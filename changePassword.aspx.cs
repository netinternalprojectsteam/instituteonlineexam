﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class changePassword : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(',');
                DataTable dt = new DataTable();
                dt = ob.getData("select * from admin_user where userid= " + userDetails[0].ToString() + "");
                lblPass.Text = dt.Rows[0]["password"].ToString();
                lblID.Text = userDetails[0].ToString();
            }

        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (txtOld.Text == lblPass.Text)
            {
                if (txtNew.Text == txtRetype.Text)
                {
                    ob.updateAdminPass(txtNew.Text, lblID.Text);
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Password changed successfully !");
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "New password and Retype password not match !");
                }
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter your old password correctly !");
            }
        }
    }
}
