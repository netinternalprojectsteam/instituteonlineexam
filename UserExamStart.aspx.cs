﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class UserExamStart : System.Web.UI.Page
    {
        string[,] queAns;
        //string[] QuestionArray;
        //string QuestionList = "";
        int i = 1;
        //DataSet ds = new DataSet();
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Timer1.Enabled = false;
            }   //Session["time"] = DateTime.Now.AddMilliseconds(3600000);
                //Label3.Text = "Started on" + DateTime.Now.ToString("h:mmtt");
                //Label3.Visible = true;
                ////Response.Write((DateTime)Session["time"]);
                //TimeSpan time1 = new TimeSpan();
                //time1 = ((DateTime)Session["time"]) - DateTime.Now;
                //Label2.Text = String.Format("Time remaining: {0}:{1}:{2}", time1.Hours, time1.Minutes.ToString(), time1.Seconds.ToString());
                //Label2.Visible = true;


                //   exm_questions.questid, exm_questions.question, exm_questions.option1, exm_questions.option2, exm_questions.option3, exm_questions.option4,  exm_questions.answer, exm_questions.subid, exm_questions.difflevel, exm_questions.userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber], case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '2' then exm_questions.option4 end as CorectAns FROM exm_questions INNER JOIN exm_subject ON exm_questions.subid = exm_subject.subid INNER JOIN exm_course ON exm_subject.courseid = exm_course.courseid WHERE (exm_course.courseid = " + ddlCourse.SelectedValue + ") order by questid, newid() 

                //xm_questions.subid, exm_questions.difflevel, exm_questions.userid


            //    try
            //    {
            //        if (Session["resExam"].ToString() == "Yes")
            //        {

            //            Session.Remove("resExam");

            //            lblExamID.Text = Session["examID"].ToString();
            //            lbluserID.Text = Session["userID"].ToString();
            //            Panel3.Visible = true;
            //            Panel2.Visible = false;


            //            DataTable questions = new DataTable();
            //            questions.Columns.Add("questid", typeof(string));
            //            questions.Columns.Add("question", typeof(string));
            //            questions.Columns.Add("option1", typeof(string));
            //            questions.Columns.Add("option2", typeof(string));
            //            questions.Columns.Add("option3", typeof(string));
            //            questions.Columns.Add("option4", typeof(string));
            //            questions.Columns.Add("answer", typeof(string));
            //            questions.Columns.Add("subid", typeof(string));
            //            questions.Columns.Add("difflevel", typeof(string));
            //            questions.Columns.Add("userid", typeof(string));
            //            questions.Columns.Add("serialnumber", typeof(string));
            //            questions.Columns.Add("CorectAns", typeof(string));
            //            questions.Columns.Add("infoID", typeof(string));


            //            DataTable dtGivenAns = new DataTable();
            //            dtGivenAns = obj.getData("SELECT userid, exmid, questno, answer,CASE WHEN answer = '1' THEN (SELECT option1 FROM exm_questions WHERE questid = questno) WHEN answer = '2' THEN (SELECT option2 FROM exm_questions WHERE questid = questno) WHEN answer = '3' THEN (SELECT option3 FROM exm_questions WHERE questid = questno) WHEN answer = '4' THEN (SELECT option4 FROM exm_questions WHERE questid = questno) WHEN answer = '0' THEN 'Not Solved' END AS Answer1 FROM exm_userAnswers where exmid = " + lblExamID.Text + " and userid = " + lbluserID.Text + "");

            //            DataTable dtQueDetails = new DataTable();
            //            for (int i = 0; i < dtGivenAns.Rows.Count; i++)
            //            {
            //                DataRow dtrow = questions.NewRow();
            //                dtQueDetails = obj.getData("SELECT questid, question, option1, option2, option3, option4, answer,subid,difflevel,userid, case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '2' then exm_questions.option4 end as CorectAns, infoID FROM exm_questions where questid = " + dtGivenAns.Rows[i]["questno"].ToString() + "");
            //                dtrow["questid"] = dtQueDetails.Rows[0]["questid"].ToString();
            //                dtrow["question"] = dtQueDetails.Rows[0]["question"].ToString();
            //                dtrow["option1"] = dtQueDetails.Rows[0]["option1"].ToString();
            //                dtrow["option2"] = dtQueDetails.Rows[0]["option2"].ToString();
            //                dtrow["option3"] = dtQueDetails.Rows[0]["option3"].ToString();
            //                dtrow["option4"] = dtQueDetails.Rows[0]["option4"].ToString();
            //                dtrow["answer"] = dtQueDetails.Rows[0]["answer"].ToString();
            //                dtrow["subid"] = dtQueDetails.Rows[0]["subid"].ToString();
            //                dtrow["difflevel"] = dtQueDetails.Rows[0]["difflevel"].ToString();
            //                dtrow["userid"] = dtQueDetails.Rows[0]["userid"].ToString();
            //                dtrow["serialnumber"] = (i + 1).ToString();
            //                dtrow["CorectAns"] = dtQueDetails.Rows[0]["CorectAns"].ToString();
            //                dtrow["infoID"] = dtQueDetails.Rows[0]["infoID"].ToString();
            //                questions.Rows.Add(dtrow);
            //            }


            //            int remTime = Convert.ToInt32(obj.getValue("select min(remainingtime) from exm_userAnswers where exmID= " + lblExamID.Text + ""));


            //            Session["time"] = DateTime.Now.AddMilliseconds(remTime);
            //            Label3.Text = "Started at &nbsp;" + DateTime.Now.ToString("h:mm tt");
            //            Label3.Visible = true;
            //            //Response.Write((DateTime)Session["time"]);
            //            TimeSpan time1 = new TimeSpan();
            //            time1 = ((DateTime)Session["time"]) - DateTime.Now;
            //            Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
            //            Timer1.Enabled = true;


            //            ViewState["myTable"] = questions;
            //            Session.Add("localQuest", Convert.ToInt16(Session["questno"]) + 1);
            //            ViewState["in"] = 1;
            //            Session["ques"] = ddlno.Text;
            //            lblQuestionNo.Text = questions.Rows[0][0].ToString();
            //            lblQue.Text = questions.Rows[0][1].ToString();
            //            lblOptionA.Text = questions.Rows[0][2].ToString();
            //            lblOptionB.Text = questions.Rows[0][3].ToString();
            //            lblOptionC.Text = questions.Rows[0][4].ToString();
            //            lblOptionD.Text = questions.Rows[0][5].ToString();
            //            Repeater1.DataSource = questions;
            //            Repeater1.DataBind();

            //            string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //            if (givenAnswer != "0")
            //            {
            //                if (givenAnswer == "1")
            //                { rb1.Checked = true; }
            //                if (givenAnswer == "2")
            //                { rb2.Checked = true; }
            //                if (givenAnswer == "3")
            //                { rb3.Checked = true; }
            //                if (givenAnswer == "4")
            //                { rb4.Checked = true; }
            //            }

            //            if (questions.Rows[0]["infoID"].ToString() != "0")
            //            {
            //                Panel5.Visible = true;
            //                DataTable dtqueData = new DataTable();
            //                dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + questions.Rows[0]["infoID"].ToString() + " ");
            //                repQueInfo.DataSource = dtqueData;
            //                repQueInfo.DataBind();
            //            }
            //            else
            //            {
            //                Panel5.Visible = false;
            //            }

            //            //sourceAttempts.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            //            //sourceAttempts.SelectCommand = "SELECT attemptDate FROM exm_userAttempts where userid= " + lbluserID.Text + " and examid=" + lblExamID.Text + "";
            //            //gridAttempts.DataBind();
            //        }
            //    }
            //    catch { }

            //}
            //try
            //{

            //    if (!IsPostBack)
            //    {
            //        ddlCourse.DataBind();
            //        Session.Remove("ques");
            //        rb1.Attributes.Add("onmousedown", "clickCheck(this)");
            //        string[] st = HttpContext.Current.User.Identity.Name.Split(',');
            //        lbluserID.Text = st[0].ToString();

            //    }

            //}
            //catch { }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            //string ans = (rblOptions.SelectedIndex + 1).ToString();
            //if (lblGivenAns.Text.Length > 0)
            //{
            //    lblGivenAns.Text = lblGivenAns.Text + "," + ans;
            //}
            //else { lblGivenAns.Text = ans; }

            //// Access dt back from viewstate
            //DataTable dt = (DataTable)ViewState["myTable"];
            //int j = (int)ViewState["in"];
            //if (dt.Rows.Count > j)
            //{

            //    lblQuestion.Text = dt.Rows[j][1].ToString();
            //    rblOptions.Items[0].Text = dt.Rows[j][2].ToString();
            //    rblOptions.Items[1].Text = dt.Rows[j][3].ToString();
            //    rblOptions.Items[2].Text = dt.Rows[j][4].ToString();
            //    rblOptions.Items[3].Text = dt.Rows[j][5].ToString();

            //}
            //else
            //{
            //    Session["dtable"] = dt;
            //    Session["ans"] = lblGivenAns.Text;
            //    //Response.Redirect("ExamResult.aspx");

            //}
            //j++;
            //ViewState["in"] = j;
            //rblOptions.ClearSelection();

            if (Session["rTime"].ToString().Length > 0)
            {
                Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime"].ToString()));
                TimeSpan time1 = new TimeSpan();
                time1 = ((DateTime)Session["time"]) - DateTime.Now;
                Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                Timer1.Enabled = true;
                Session.Remove("rTime");
            }


            try
            {

                if (Session["JumpTo"].ToString() == "Yes")
                {

                    //Button btn = (Button)sender;
                    string qID = Session["qID"].ToString();
                    int z = Convert.ToInt16(Session["qNo"].ToString());

                    Session.Remove("JumpTo"); Session.Remove("qID"); Session.Remove("qNo");


                    ViewState["in"] = z;

                    lblQuestionNo.Text = qID;
                    DataTable dt = new DataTable();
                    dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + qID + "");
                    lblQue.Text = dt.Rows[0][1].ToString();
                    lblOptionA.Text = dt.Rows[0][2].ToString();
                    lblOptionB.Text = dt.Rows[0][3].ToString();
                    lblOptionC.Text = dt.Rows[0][4].ToString();
                    lblOptionD.Text = dt.Rows[0][5].ToString();

                    clearAllRadio();
                    string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
                    if (givenAnswer != "0")
                    {
                        if (givenAnswer == "1")
                        { rb1.Checked = true; }
                        if (givenAnswer == "2")
                        { rb2.Checked = true; }
                        if (givenAnswer == "3")
                        { rb3.Checked = true; }
                        if (givenAnswer == "4")
                        { rb4.Checked = true; }
                    }

                    try
                    {
                        if (dt.Rows[0]["infoID"].ToString() != "0")
                        {
                            Panel5.Visible = true;
                            DataTable dtqueData = new DataTable();
                            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                            {
                                repQueInfo.DataSource = dtqueData;
                                repQueInfo.DataBind();
                            }
                            else { Panel5.Visible = false; }
                        }
                        else
                        {
                            Panel5.Visible = false;
                        }
                    }
                    catch { }

                    DataTable markedQuestion = new DataTable();
                    markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
                    if (markedQuestion.Rows.Count > 0)
                    {
                        btnMark.Text = "Unmark";
                    }
                    else { btnMark.Text = "Mark"; }


                }
            }
            catch { }


        }

        protected void btnSub_Click(object sender, EventArgs e)
        {
            string[] st = HttpContext.Current.User.Identity.Name.Split(',');
            lbluserID.Text = st[0].ToString();
            string maxExamID = obj.getValue("select max(newexm)+1 from exm_newexam");
            lblExamID.Text = maxExamID;
            //Panel3.Visible = true;
            //Panel2.Visible = true;

            Session["resExam"] = "Yes";

            DataTable dt = new DataTable();
            //dt = obj.GetAllQuestion();
            // dt = obj.getData("SELECT top 5  * FROM exm_questions where ORDER BY newid()");
            dt = obj.getData("SELECT top " + ddlno.Text + " exm_questions.questid, exm_questions.question, exm_questions.option1, exm_questions.option2, exm_questions.option3, exm_questions.option4,  exm_questions.answer, exm_questions.subid, exm_questions.difflevel, exm_questions.userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber], case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '2' then exm_questions.option4 end as CorectAns,exm_questions.infoID  FROM exm_questions INNER JOIN exm_subject ON exm_questions.subid = exm_subject.subid INNER JOIN exm_course ON exm_subject.courseid = exm_course.courseid WHERE (exm_course.courseid = " + ddlCourse.SelectedValue + ") order by questid, newid() ");

            //for(int i=0;i<dt.Rows.Count;i

            //   Timer1.Enabled = true;


            //GridView1.DataSource = dt;
            //GridView1.DataBind();



            Session["time"] = DateTime.Now.AddMilliseconds(3600000);
            Label3.Text = "Started at &nbsp;" + DateTime.Now.ToString("h:mm tt");
            //Label3.Visible = true;
            //Response.Write((DateTime)Session["time"]);
            TimeSpan time1 = new TimeSpan();
            time1 = ((DateTime)Session["time"]) - DateTime.Now;
            Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
            Timer1.Enabled = false;


            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }





            string queNos = "";
            for (int n = 0; n < dt.Rows.Count; n++)
            {
                if (n == 0)
                {
                    queNos = dt.Rows[0][0].ToString();
                    obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, dt.Rows[0][0].ToString(), "0", rTimeinMill);
                }
                else
                {
                    queNos = queNos + "," + dt.Rows[n][0].ToString();
                    obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, dt.Rows[n][0].ToString(), "0", rTimeinMill);
                }
            }
           // obj.InsertUserQuestionList(maxExamID, ddlno.Text, queNos, lbluserID.Text, rTimeinMill, ddlCourse.SelectedValue, "Course");
            // obj.insertUserExamAttempt(lbluserID.Text, maxExamID);



            //for (intj = 0; j < queAns.Length; j++)
            //{


            //}


            ViewState["myTable"] = dt;
            Session.Add("localQuest", Convert.ToInt16(Session["questno"]) + 1);
            ViewState["in"] = i;
            Session["ques"] = ddlno.Text;
            lblQuestionNo.Text = dt.Rows[0][0].ToString();
            lblQue.Text = dt.Rows[0][1].ToString();
            lblOptionA.Text = dt.Rows[0][2].ToString();
            lblOptionB.Text = dt.Rows[0][3].ToString();
            lblOptionC.Text = dt.Rows[0][4].ToString();
            lblOptionD.Text = dt.Rows[0][5].ToString();
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            try
            {
                if (dt.Rows[0]["infoID"].ToString() != "0")
                {
                    Panel5.Visible = true;
                    DataTable dtqueData = new DataTable();
                    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                    if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                    {
                        repQueInfo.DataSource = dtqueData;
                        repQueInfo.DataBind();
                    }
                    else { Panel5.Visible = false; }
                }
                else
                {
                    Panel5.Visible = false;
                }
            }
            catch { }
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;
            //Timer1.Enabled = false;
            string script = "<script type=\"text/javascript\">openNewWindow();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            //try
            //{
            string ans = "0";
            if (rb1.Checked == true)
            {
                ans = "1";
            }
            if (rb2.Checked == true)
            {
                ans = "2";
            }
            if (rb3.Checked == true)
            {
                ans = "3";
            }
            if (rb4.Checked == true)
            {
                ans = "4";
            }
            // = (rblOptions.SelectedIndex + 1).ToString();
            if (lblGivenAns.Text.Length > 0)
            {
                lblGivenAns.Text = lblGivenAns.Text + "," + ans;
            }
            else { lblGivenAns.Text = ans; }
            DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }


            if (dt2.Rows.Count > 0)
            {
                obj.updateUserAnswer(dt2.Rows[0]["id"].ToString(), ans, rTimeinMill);
            }
            else
            {
                obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text, ans, rTimeinMill);
            }

            //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");

            //DataTable questionList = obj.getData("select questionlist from exm_newexam where newexm = " + lblExamID.Text + " ");




            // Access dt back from viewstate
            DataTable dt = (DataTable)ViewState["myTable"];
            int j = (int)ViewState["in"];
            if (dt.Rows.Count > j)
            {
                lblQuestionNo.Text = dt.Rows[j][0].ToString();
                lblQue.Text = dt.Rows[j][1].ToString();
                lblOptionA.Text = dt.Rows[j][2].ToString();
                lblOptionB.Text = dt.Rows[j][3].ToString();
                lblOptionC.Text = dt.Rows[j][4].ToString();
                lblOptionD.Text = dt.Rows[j][5].ToString();




                //if (dt.Rows[j]["infoID"].ToString() != "0")
                //{
                //    Panel5.Visible = true;
                //    DataTable dtqueData = new DataTable();
                //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
                //    repQueInfo.DataSource = dtqueData;
                //    repQueInfo.DataBind();
                //}
                //else
                //{
                //    Panel5.Visible = false;
                //}


                try
                {
                    if (dt.Rows[0]["infoID"].ToString() != "0")
                    {
                        Panel5.Visible = true;
                        DataTable dtqueData = new DataTable();
                        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                        {
                            repQueInfo.DataSource = dtqueData;
                            repQueInfo.DataBind();
                        }
                        else { Panel5.Visible = false; }
                    }
                    else
                    {
                        Panel5.Visible = false;
                    }
                }
                catch { }
                j++;
                ViewState["in"] = j;
            }
            else
            {
                //Session["dtable"] = dt;
                //Session["ans"] = lblGivenAns.Text;
                ////Response.Redirect("ExamResult.aspx");
                j = 0;

                lblQuestionNo.Text = dt.Rows[j][0].ToString();
                lblQue.Text = dt.Rows[j][1].ToString();
                lblOptionA.Text = dt.Rows[j][2].ToString();
                lblOptionB.Text = dt.Rows[j][3].ToString();
                lblOptionC.Text = dt.Rows[j][4].ToString();
                lblOptionD.Text = dt.Rows[j][5].ToString();


                //if (dt.Rows[j]["infoID"].ToString() != "0")
                //{
                //    Panel5.Visible = true;
                //    DataTable dtqueData = new DataTable();
                //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
                //    repQueInfo.DataSource = dtqueData;
                //    repQueInfo.DataBind();
                //}
                //else
                //{
                //    Panel5.Visible = false;
                //}

                try
                {
                    if (dt.Rows[0]["infoID"].ToString() != "0")
                    {
                        Panel5.Visible = true;
                        DataTable dtqueData = new DataTable();
                        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                        {
                            repQueInfo.DataSource = dtqueData;
                            repQueInfo.DataBind();
                        }
                        else { Panel5.Visible = false; }
                    }
                    else
                    {
                        Panel5.Visible = false;
                    }
                }
                catch { }
                j++;
                ViewState["in"] = j;

                string script = "<script type=\"text/javascript\">showMessage();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }

            //   j = (int)ViewState["in"];




            clearAllRadio();

            Repeater1.DataSource = dt;
            Repeater1.DataBind();


            string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            if (givenAnswer != "0")
            {
                if (givenAnswer == "1")
                { rb1.Checked = true; }
                if (givenAnswer == "2")
                { rb2.Checked = true; }
                if (givenAnswer == "3")
                { rb3.Checked = true; }
                if (givenAnswer == "4")
                { rb4.Checked = true; }
            }

            DataTable markedQuestion = new DataTable();
            markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            if (markedQuestion.Rows.Count > 0)
            {
                btnMark.Text = "Unmark";
            }
            else { btnMark.Text = "Mark"; }
            //}
            //catch { }

        }

        protected void btnRep_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            int z = Convert.ToInt16(btn.Text);

            ViewState["in"] = z;

            lblQuestionNo.Text = qID;
            DataTable dt = new DataTable();
            dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + qID + "");
            lblQue.Text = dt.Rows[0][1].ToString();
            lblOptionA.Text = dt.Rows[0][2].ToString();
            lblOptionB.Text = dt.Rows[0][3].ToString();
            lblOptionC.Text = dt.Rows[0][4].ToString();
            lblOptionD.Text = dt.Rows[0][5].ToString();

            clearAllRadio();
            string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            if (givenAnswer != "0")
            {
                if (givenAnswer == "1")
                { rb1.Checked = true; }
                if (givenAnswer == "2")
                { rb2.Checked = true; }
                if (givenAnswer == "3")
                { rb3.Checked = true; }
                if (givenAnswer == "4")
                { rb4.Checked = true; }
            }

            //if (dt.Rows[0]["infoID"].ToString() != "0")
            //{
            //    Panel5.Visible = true;
            //    DataTable dtqueData = new DataTable();
            //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
            //    repQueInfo.DataSource = dtqueData;
            //    repQueInfo.DataBind();
            //}
            //else
            //{
            //    Panel5.Visible = false;
            //}

            try
            {
                if (dt.Rows[0]["infoID"].ToString() != "0")
                {
                    Panel5.Visible = true;
                    DataTable dtqueData = new DataTable();
                    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                    if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                    {
                        repQueInfo.DataSource = dtqueData;
                        repQueInfo.DataBind();
                    }
                    else { Panel5.Visible = false; }
                }
                else
                {
                    Panel5.Visible = false;
                }
            }
            catch { }

            DataTable markedQuestion = new DataTable();
            markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            if (markedQuestion.Rows.Count > 0)
            {
                btnMark.Text = "Unmark";
            }
            else { btnMark.Text = "Mark"; }

        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void clearAllRadio()
        {
            rb1.Checked = false;
            rb2.Checked = false;
            rb3.Checked = false;
            rb4.Checked = false;
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["myTable"];
            int j = (int)ViewState["in"];
            if (dt.Rows.Count > j)
            {
                lblQuestionNo.Text = dt.Rows[j][0].ToString();
                lblQue.Text = dt.Rows[j][1].ToString();
                lblOptionA.Text = dt.Rows[j][2].ToString();
                lblOptionB.Text = dt.Rows[j][3].ToString();
                lblOptionC.Text = dt.Rows[j][4].ToString();
                lblOptionD.Text = dt.Rows[j][5].ToString();

                //if (dt.Rows[j]["infoID"].ToString() != "0")
                //{
                //    Panel5.Visible = true;
                //    DataTable dtqueData = new DataTable();
                //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
                //    repQueInfo.DataSource = dtqueData;
                //    repQueInfo.DataBind();
                //}
                //else
                //{
                //    Panel5.Visible = false;
                //}

                try
                {
                    if (dt.Rows[0]["infoID"].ToString() != "0")
                    {
                        Panel5.Visible = true;
                        DataTable dtqueData = new DataTable();
                        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                        {
                            repQueInfo.DataSource = dtqueData;
                            repQueInfo.DataBind();
                        }
                        else { Panel5.Visible = false; }
                    }
                    else
                    {
                        Panel5.Visible = false;
                    }
                }
                catch { }

                j++;
                ViewState["in"] = j;
            }
            else
            {
                Session["dtable"] = dt;
                Session["ans"] = lblGivenAns.Text;
                //Response.Redirect("ExamResult.aspx");

                j = 0;

                lblQuestionNo.Text = dt.Rows[j][0].ToString();
                lblQue.Text = dt.Rows[j][1].ToString();
                lblOptionA.Text = dt.Rows[j][2].ToString();
                lblOptionB.Text = dt.Rows[j][3].ToString();
                lblOptionC.Text = dt.Rows[j][4].ToString();
                lblOptionD.Text = dt.Rows[j][5].ToString();


                //if (dt.Rows[j]["infoID"].ToString() != "0")
                //{
                //    Panel5.Visible = true;
                //    DataTable dtqueData = new DataTable();
                //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
                //    repQueInfo.DataSource = dtqueData;
                //    repQueInfo.DataBind();
                //}
                //else
                //{
                //    Panel5.Visible = false;
                //}

                try
                {
                    if (dt.Rows[0]["infoID"].ToString() != "0")
                    {
                        Panel5.Visible = true;
                        DataTable dtqueData = new DataTable();
                        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                        {
                            repQueInfo.DataSource = dtqueData;
                            repQueInfo.DataBind();
                        }
                        else { Panel5.Visible = false; }
                    }
                    else
                    {
                        Panel5.Visible = false;
                    }
                }
                catch { }

                j++;
                ViewState["in"] = j;

                string script = "<script type=\"text/javascript\">showMessage();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }

            clearAllRadio();
            string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            if (givenAnswer != "0")
            {
                if (givenAnswer == "1")
                { rb1.Checked = true; }
                if (givenAnswer == "2")
                { rb2.Checked = true; }
                if (givenAnswer == "3")
                { rb3.Checked = true; }
                if (givenAnswer == "4")
                { rb4.Checked = true; }
            }

            DataTable markedQuestion = new DataTable();
            markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            if (markedQuestion.Rows.Count > 0)
            {
                btnMark.Text = "Unmark";
            }
            else { btnMark.Text = "Mark"; }
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (Repeater1.Items.Count > 0)
            {
                DataTable dt = new DataTable();
                dt = obj.getData("SELECT  questno, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ")");
                if (dt.Rows.Count > 0)
                {
                    //Label lbl = (Label)Repeater1.FindControl("");
                    Button btn = (Button)Repeater1.FindControl("Button1");
                    string qID = btn.Text;
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    if (Convert.ToInt16(qID) == Convert.ToInt16(dt.Rows[0]["questno"].ToString()))
                    //    {
                    //        btn.BackColor = System.Drawing.Color.Green;
                    //    }
                    //}
                }
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["myTable"];
            Session["dtable"] = dt;
            Session["ans"] = lblGivenAns.Text;


            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;

            obj.updateExamStatus(lblExamID.Text);
            Response.Redirect("exmResult.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                TimeSpan time1 = new TimeSpan();
                time1 = ((DateTime)Session["time"]) - DateTime.Now;
                if (time1.Minutes <= 0 && time1.Seconds <= 0)
                {
                    Label2.Text = "TimeOut!";
                    DataTable dt = (DataTable)ViewState["myTable"];
                    Session["dtable"] = dt;
                    Session["ans"] = lblGivenAns.Text;


                    Session["examID"] = lblExamID.Text;
                    Session["userID"] = lbluserID.Text;

                    obj.updateExamStatus(lblExamID.Text);
                    Response.Redirect("exmResult.aspx");
                }
                else
                {
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours, time1.Minutes.ToString(), time1.Seconds.ToString());
                }
            }
            catch { }

        }

        protected void btnMark_Click(object sender, EventArgs e)
        {
            //int i = 0;
            //if (btnMark.Text == "Mark")
            //{
            //    obj.insertMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
            //    btnMark.Text = "Unmark";
            //    i = i + 1;
            //}
            //if (btnMark.Text == "Unmark" && i == 0)
            //{
            //    obj.deleteMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
            //    btnMark.Text = "Mark";
            //}
        }

        protected void btnReview_Click(object sender, EventArgs e)
        {
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;

            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            //int x = rTime;
            Session["rTime"] = rTimeinMill.ToString();
            Timer1.Enabled = false;
            modalPopupReview.Show();
        }

    }
}
