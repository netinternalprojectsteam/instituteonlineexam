﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace OnlineExam
{
    public partial class mpayconfirm : System.Web.UI.Page
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);
        public void openConnection()
        {
            try { cn.Open(); }
            catch { cn.Close(); cn.Open(); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    string uid = Request.QueryString["uid"].ToString();
                    string transID = Request.QueryString["transID"].ToString();
                    //  messages11.Visible = false;
                    if (uid != null)
                    {


                        DataTable dt = new DataTable();
                        //dt = ob.GetUserInfoByID(val[0].ToString());
                        openConnection();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "SELECT  uid,  uemail, uname, mobileNo,  imageName, password, city, edate, country, state, extraEmail, Add1, Add2, aboutU, exam_Institute.instituteId, institute FROM exam_users INNER JOIN exam_Institute on exam_users.InstituteId=exam_Institute.InstituteId WHERE uid = @uid";
                        cmd.Connection = cn;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.Parameters.AddWithValue("uid", uid);
                        da.Fill(dt);
                        cn.Close();
                        lblUserName.Text = dt.Rows[0]["uname"].ToString();

                        string htmltext = returnHTML(transID);
                        Label3.Text = htmltext;

                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                // messages11.Visible = true;
                // messages11.setMessage(0, ex.Message);
            }

        }


        public string returnHTML(string transId)
        {
            //create object for get checksum

            IntegrationKit.libfuncs myUtility = new IntegrationKit.libfuncs();
            string htmltext = "";
            try
            {
                DataTable dt = new DataTable();
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * from view_paidExmUserDetails where transID=" + transId + "";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
                cn.Close();
                //code for runtime html 
                htmltext = "<table class='membersvs'><tr>";
                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Name  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["uname"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Mobile No.  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["mobileNo"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Email ID  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["uemail"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Address ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["address"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "City  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["city"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Transaction ID  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["transID"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Total Amount  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["exmAmt"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Transaction Date :";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["tDate"].ToString() + "</td>";
                htmltext = htmltext + "</tr></table>";

                htmltext = htmltext + "<br />";

                string totalc = dt.Rows[0]["exmAmt"].ToString();
                string transID = dt.Rows[0]["transID"].ToString() + "A";
                string name = dt.Rows[0]["uname"].ToString();
                string address = dt.Rows[0]["address"].ToString();
                string city = dt.Rows[0]["city"].ToString();
                string email = dt.Rows[0]["uemail"].ToString();
                string Mobile = dt.Rows[0]["mobileNo"].ToString();
                string country = dt.Rows[0]["country"].ToString();
                string state = dt.Rows[0]["state"].ToString();

                htmltext = htmltext + string.Format("<input type=\"hidden\" name=\"merchant_id\" id=\"merchant_id\" value=\"99555\" /><input type=\"hidden\" name=\"order_id\" value=\"" + transID + "\" /><input type=\"hidden\" name=\"amount\" value=\"" + totalc + "\" /><input type=\"hidden\" name=\"currency\" value=\"INR\" /><input type=\"hidden\" name=\"redirect_url\" value=\"http://pvkexams.com/mccavResponseHandler.aspx\" /><input type=\"hidden\" name=\"cancel_url\" value=\"http://pvkexams.com/mccavCancel.aspx\" /><input type=\"hidden\" name=\"billing_name\" value=\"" + name + "\" /><input type=\"hidden\" name=\"billing_address\" value=\"" + address + "\" /><input type=\"hidden\" name=\"billing_city\" value=\"" + city + "\" /><input type=\"hidden\" name=\"billing_state\" value=\"" + state + "\" /><input type=\"hidden\" name=\"billing_zip\" value=\"425001\" /><input type=\"hidden\" name=\"billing_country\" value=\"" + country + "\" /><input type=\"hidden\" name=\"billing_tel\" value=\"" + Mobile + "\" /><input type=\"hidden\" name=\"billing_email\" value=\"" + email + "\" /><input type=\"hidden\" name=\"delivery_name\" value=\"" + name + "\" /><input type=\"hidden\" name=\"delivery_address\" value=\"" + address + "\" /><input type=\"hidden\" name=\"delivery_city\" value=\"" + city + "\" /><input type=\"hidden\" name=\"delivery_state\" value=\"" + state + "\" /><input type=\"hidden\" name=\"delivery_zip\" value=\"444444\" /><input type=\"hidden\" name=\"delivery_country\" value=\"" + country + "\" /><input type=\"hidden\" name=\"delivery_tel\" value=\"" + Mobile + "\" /><input type=\"hidden\" name=\"merchant_param1\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param2\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param3\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param4\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param5\" value=\"additional Info.\" /><input type=\"hidden\" name=\"promo_code\" /><input type=\"hidden\" name=\"customer_identifier\" /><input type=\"submit\" value=\"Process Payment\" class=\"simplebtn\" style=\"font-size: 15px; font-weight: bold; width: 150px; height: 40px;\" />");
            }
            catch (Exception)
            {
                return "<h3> Error while processing payment request!!!</h3>";
            }
            return htmltext;
        }
    }
}
