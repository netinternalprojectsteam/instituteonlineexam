﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ManageInstitute : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsubmit.Text == "Add New")
                {
                    obj.AddNewInstitute(txtInstitute.Text);
                    Messages11.setMessage(1, "Institute Added Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    txtInstitute.Text = "";
                }

                if (btnsubmit.Text == "Update")
                {
                    obj.UpdateInstitute(txtInstitute.Text, chkActive.Checked.ToString(), lblInstituteId.Text);
                    Messages11.setMessage(1, "Institute Updated Successfully !!!");
                    Messages11.Visible = true;
                    btnsubmit.Text = "Add New";
                    GridView1.DataBind();
                    txtInstitute.Text = "";
                    btnCancel.Visible = false;
                    lblActive.Visible = false;
                    chkActive.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtInstitute.Text = "";
            btnCancel.Visible = false;
            lblActive.Visible = false;
            chkActive.Visible = false;
            btnsubmit.Text = "Add New";
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[0].Visible = false;
            }
            catch { }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowno = Convert.ToInt16(e.CommandArgument);
            if (e.CommandName == "change")
            {
                lblInstituteId.Text = GridView1.Rows[rowno].Cells[0].Text;
                txtInstitute.Text = GridView1.Rows[rowno].Cells[1].Text;
                chkActive.Checked = Convert.ToBoolean(GridView1.Rows[rowno].Cells[2].Text);
                btnsubmit.Text = "Update";
                btnCancel.Visible = true;
                lblActive.Visible = true;
                chkActive.Visible = true;
            }

            if (e.CommandName == "remove")
            {
                try
                {
                    obj.DeleteInstitute(GridView1.Rows[rowno].Cells[0].Text);
                    Messages11.setMessage(1, "Institute Deleted Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                }
                catch (Exception ex)
                {
                    Messages11.setMessage(0, ex.Message);
                    Messages11.Visible = true;
                }
            }

            if (e.CommandName == "showByInstitute")
            {
                lblInstituteId.Text = GridView1.Rows[rowno].Cells[0].Text;
                Session["InstituteID"] = lblInstituteId.Text;
                Response.Redirect("/ManageCategory.aspx");
            }
        }
    }
}
