﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="editData.aspx.cs" Inherits="OnlineExam.editData" Title="Edit Common Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
        document.getElementById("ctl00_ContentPlaceHolder1_Button2").click();     
       }
   
   
 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <div style="display: none">
        <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" /></div>
    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    Edit Common Data</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblInsId" Visible="false" runat="server" Text="Label"></asp:Label>
                &nbsp;
                <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:SqlDataSource ID="sourceInfoHead" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                    SelectCommand="SELECT Distinct [infID], [infHeading] FROM [exm_queInfo] where instituteId=@instituteId">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Label ID="Label5" runat="server" Text="Select Information Heading" Font-Bold="true"></asp:Label>
                <asp:DropDownList ID="ddlInfoHead" runat="server" DataSourceID="sourceInfoHead" DataTextField="infHeading"
                    DataValueField="infID" ToolTip="Left blank if question is without information."
                    AutoPostBack="True" CausesValidation="false" OnSelectedIndexChanged="ddlInfoHead_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;
                <asp:ImageButton ID="imgSearchInfo" ImageUrl="~/images/search.png" runat="server" />
                <br />
                <br />
                <asp:ModalPopupExtender ID="modalSearchContents" runat="server" TargetControlID="imgSearchInfo"
                    BackgroundCssClass="ModalPopupBG" PopupControlID="searchContents" Drag="true"
                    CancelControlID="btnCanSearchInfo">
                </asp:ModalPopupExtender>
                <div id="searchContents" style="display: none;" class="popupConfirmation">
                    <iframe id="Iframe2" frameborder="0" src="searchInfoContents.aspx" height="450px"
                        width="800px"></iframe>
                    <div class="popup_Buttons" style="display: none">
                        <input id="btnOkSearchInfo" value="Done" type="button" />
                        <input id="btnCanSearchInfo" value="Cancel" type="button" />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <FCKeditorV2:FCKeditor ID="CKEditorControl1" runat="server" Height="400px" Width="100%">
                </FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
            </td>
        </tr>
    </table>
    <%-- </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
