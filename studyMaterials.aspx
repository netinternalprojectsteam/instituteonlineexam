﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="studyMaterials.aspx.cs" Inherits="OnlineExam.studyMaterials" Title="Study Materials" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblIcon" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblID" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblDoc" runat="server" Text="" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Study Materials
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Title" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtTitle"
                                            runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" Text="Icon Image" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:FileUpload ID="fileUploadIcon" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label4" runat="server" Text="Document File" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:FileUpload ID="fileUploadDoc" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="Is Active" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label5" runat="server" Text="Description" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDesc" runat="server" Width="600px" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDesc"
                                            runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="simplebtn" />
                                        <asp:Button ID="btnCancel" CausesValidation="false" runat="server" Text="Cancel"
                                            Visible="false" CssClass="simplebtn" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceMaterials" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT id, title, description, isActive, '/UploadedDocs/' + iconImage AS iconImage, '/UploadedDocs/' + downloadFile AS downloadFile FROM site_studyMaterials">
                        </asp:SqlDataSource>
                        <asp:GridView ID="gridMaterials" runat="server" DataSourceID="sourceMaterials" AutoGenerateColumns="False"
                            DataKeyNames="id" OnRowCommand="gridMaterials_RowCommand" CssClass="gridview"
                            OnRowCreated="gridMaterials_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr.No.">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                    SortExpression="id" />
                                <asp:BoundField DataField="title" HeaderText="Title" SortExpression="title" />
                                <asp:BoundField DataField="description" HeaderText="Description" 
                                    SortExpression="description" />
                                <asp:BoundField DataField="iconImage" HeaderText="iconImage" SortExpression="iconImage" />
                                <asp:BoundField DataField="downloadFile" HeaderText="Download File" SortExpression="downloadFile" />
                                <asp:ImageField DataImageUrlField="iconImage" HeaderText="Icon Image">
                                    <ControlStyle Height="50px" Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:ImageField>
                                <asp:CheckBoxField DataField="isActive" HeaderText="isActive" 
                                    SortExpression="isActive" >
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CheckBoxField>
                                <asp:ButtonField ButtonType="Image" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                    CommandName="change" Text="Button" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
