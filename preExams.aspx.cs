﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;

namespace OnlineExam
{
    public partial class preExams : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');

                DataTable dt = new DataTable();
                dt = obj.GetUserInfoByID(val[0].ToString());
                //DateTime datetime = DateTime.Now.Date;
                //datetime = datetime.AddDays(-1);
                string str = "Select * from exm_existingExams ";
                //string str = "Select * from exm_existingExams where coursrPackageID = " + dt.Rows[0]["courseID"].ToString() + " ORDER BY onDate";
                DataTable dt1 = new DataTable();
                dt1 = obj.getData(str);
                ddlExams.DataSource = dt1;
                ddlExams.DataBind();
            }
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            string[] val = HttpContext.Current.User.Identity.Name.Split(',');

            //DataTable dt1 = new DataTable();
            //dt1 = obj.getData("SELECT * FROM exam_userCompleted WHERE uID = " + val[0].ToString() + " AND examID = " + ddlExams.SelectedValue + "");
            //if (dt1.Rows.Count == 0)
            //{

            //obj.insertExamCompleted(val[0].ToString(), ddlExams.SelectedValue);
            Session["resExam"] = "Yes";

            lblExamID.Text = ddlExams.SelectedValue;
            lblUserID.Text = val[0].ToString();

            DataTable dt = obj.getData("SELECT examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, examType FROM exm_existingExams where examID = " + ddlExams.SelectedValue + "");
            ViewState["examDetails"] = dt;


            string maxExamID = obj.getValue("select max(newexm)+1 from exm_newexam");
            obj.InsertUserQuestionList(maxExamID, dt.Rows[0]["noofQuestions"].ToString(), dt.Rows[0]["questionNos"].ToString(), val[0].ToString(), Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()), dt.Rows[0]["courseID"].ToString(), "Pre Exam", dt.Rows[0]["noOfSections"].ToString(), ddlExams.SelectedValue);
            string[] queNos = dt.Rows[0]["questionNos"].ToString().Split(',');
            for (int i = 0; i < queNos.Length; i++)
            {
                if (queNos[i].Length > 0)
                {
                    obj.insertUseAnswer(val[0].ToString(), maxExamID, queNos[i].ToString(), "0", Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()));
                }
            }
            Session["resPreExam"] = "No";
            Session["examID"] = maxExamID;
            Session["userID"] = val[0].ToString();
            string script = "<script type=\"text/javascript\">openNewWindow();</script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            //}
            //else
            //{
            //    string script = "<script type=\"text/javascript\">showAlert();</script>";
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            //}
        }

        protected void btnCanPass_Click(object sender, EventArgs e)
        {
            pnlPass.Visible = false;
            pnlStart.Visible = true;
        }

        protected void btnStartExam_Click(object sender, EventArgs e)
        {
            if (lblPassword.Text == txtPass.Text)
            {
                string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                DataTable dt = (DataTable)ViewState["examDetails"];
                string maxExamID = obj.getValue("select max(newexm)+1 from exm_newexam");
                obj.InsertUserQuestionList(maxExamID, dt.Rows[0]["noofQuestions"].ToString(), dt.Rows[0]["questionNos"].ToString(), val[0].ToString(), Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()), dt.Rows[0]["courseID"].ToString(), "Pre Exam", dt.Rows[0]["noOfSections"].ToString(), ddlExams.SelectedValue);
                string[] queNos = dt.Rows[0]["questionNos"].ToString().Split(',');
                for (int i = 0; i < queNos.Length; i++)
                {
                    if (queNos[i].Length > 0)
                    {
                        obj.insertUseAnswer(val[0].ToString(), maxExamID, queNos[i].ToString(), "0", Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()));
                    }
                }
                Session["resPreExam"] = "No";
                Session["examID"] = maxExamID;
                Session["userID"] = val[0].ToString();
                pnlStart.Visible = true;
                pnlPass.Visible = false;
                pnlPayNow.Visible = false;
                string script = "<script type=\"text/javascript\">openNewWindow();</script>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
            else
            {
                lblMsg.Text = "Your Password not match.Please provide correct Details !";
            }
        }

        protected void btnCanPay_Click(object sender, EventArgs e)
        {
            pnlPayNow.Visible = false;
            pnlStart.Visible = true;
        }
    }
}
