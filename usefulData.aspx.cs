﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class usefulData : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ob.insertUsefulLink(txtHeading.Text, txtLink.Text, ddlCategory.Text);
            txtHeading.Text = "";
            txtLink.Text = "";
            gridDetails.DataBind();
            Messages11.setMessage(1, "Data saved successfully !");
            Messages11.Visible = true;
        }

        protected void gridDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "remove")
            {
                row = Convert.ToInt16(e.CommandArgument);
                string id = gridDetails.Rows[row].Cells[4].Text;
                ob.deleteUsefulLink(id);
                gridDetails.DataBind();
                Messages11.setMessage(1, "Data deleted successfully !");
                Messages11.Visible = true;
            }
        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[4].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[4].Visible = false;
            }
        }
    }
}
