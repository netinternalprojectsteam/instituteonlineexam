﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="logDetails.aspx.cs" Inherits="OnlineExam.logDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Details</title>
</head>
<body>
    <form id="form1" runat="server">
      <div>
        <br />
        <%--this page is for LABS admin page to view & delete all folders and files at root directory of published site.--%>
        <asp:Label ID="lblMsg" runat="server" Text="Processing Data....."></asp:Label><center>
            <b>File & Folder Details at root directory</b><br />
            <br />
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Height="500px" Width="900px">
                <center>
                    <table>
                        <tr>
                            <td align="left" valign="top">
                                <asp:GridView ID="gridFiles" runat="server" AutoGenerateColumns="False" OnRowCommand="gridFiles_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr. No">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="fileName" HeaderText="File Name" />
                                        <asp:ButtonField ButtonType="Button" HeaderText="Delete" Text="Delete" CommandName="remove" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td style="width: 5px">
                                &nbsp;
                            </td>
                            <td align="left" valign="top">
                                <asp:GridView ID="gridDirs" runat="server" AutoGenerateColumns="False" OnRowCommand="gridDirs_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr. No">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="dirName" HeaderText="Directory Name" />
                                        <asp:ButtonField ButtonType="Button" HeaderText="Delete" Text="Delete" CommandName="remove" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </center>
            </asp:Panel>
        </center>
    </div>
    </form>
</body>
</html>
