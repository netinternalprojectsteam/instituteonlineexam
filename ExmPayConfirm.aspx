﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="ExmPayConfirm.aspx.cs" Inherits="OnlineExam.ExmPayConfirm" Title="Untitled Page" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .membersvs
        {
            width: 400px;
            margin: 0 auto;
            margin-bottom: 40px;
        }
        .membersvs tr th
        {
            padding: 10px;
            border: 1px solid #272727;
            background: #2d2d2d;
            color: #fff;
        }
        .membersvs tr th:first-child
        {
            width: 60px;
        }
        .membersvs tr td
        {
            padding: 10px;
            border: 1px solid #ccc;
        }
        .membersvs tr td a
        {
            color: #444;
            font-weight: bold;
        }
        .membersvs tr td a:hover
        {
            color: #99CC00;
            text-decoration: none;
        }
        .membersvs tr td table
        {
            width: auto;
        }
        .membersvs tr td td
        {
            padding: 5px;
            border: 1px solid #eee;
            width: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="DivToHide">
        <uc1:Messages1 ID="messages11" runat="server" />
    </div>
    <div>
        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
    </div>    
</asp:Content>
