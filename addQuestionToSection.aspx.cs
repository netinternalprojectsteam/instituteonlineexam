﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class addQuestionToSection : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["InstituteID"] != null)
                    {
                        lblInsId.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblInsId.Text = val[3];
                    }
                    lblSectionID.Text = Session["examSection"].ToString(); ;
                    lblExamID.Text = Session["EditExamID"].ToString(); ;

                    //lblSectionID.Text = "section1Ques";
                    //lblExamID.Text = "20";


                    ddlCourse.DataBind();

                    sourceSubjects.SelectCommand = "SELECT subjectinfo,subid from exm_subject WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                    sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlSubjects.DataBind();
                    DataTable dtExam = new DataTable();
                    dtExam = ob.getExamInfoByID(lblExamID.Text);
                    lblHeading.Text = "Add question to section ";
                    string quesAddedToExam = "";

                    if (lblSectionID.Text == "section1Ques")
                    { lblQueAdded.Text = dtExam.Rows[0]["section1Ques"].ToString(); lblHeading.Text = lblHeading.Text + "1"; quesAddedToExam = dtExam.Rows[0]["section2Ques"].ToString() + "," + dtExam.Rows[0]["section3Ques"].ToString() + "," + dtExam.Rows[0]["section4Ques"].ToString() + "," + dtExam.Rows[0]["section5Ques"].ToString(); }
                    if (lblSectionID.Text == "section2Ques")
                    { lblQueAdded.Text = dtExam.Rows[0]["section2Ques"].ToString(); lblHeading.Text = lblHeading.Text + "2"; quesAddedToExam = dtExam.Rows[0]["section1Ques"].ToString() + "," + dtExam.Rows[0]["section3Ques"].ToString() + "," + dtExam.Rows[0]["section4Ques"].ToString() + "," + dtExam.Rows[0]["section5Ques"].ToString(); }
                    if (lblSectionID.Text == "section3Ques")
                    { lblQueAdded.Text = dtExam.Rows[0]["section3Ques"].ToString(); lblHeading.Text = lblHeading.Text + "3"; quesAddedToExam = dtExam.Rows[0]["section2Ques"].ToString() + "," + dtExam.Rows[0]["section1Ques"].ToString() + "," + dtExam.Rows[0]["section4Ques"].ToString() + "," + dtExam.Rows[0]["section5Ques"].ToString(); }
                    if (lblSectionID.Text == "section4Ques")
                    { lblQueAdded.Text = dtExam.Rows[0]["section4Ques"].ToString(); lblHeading.Text = lblHeading.Text + "4"; quesAddedToExam = dtExam.Rows[0]["section2Ques"].ToString() + "," + dtExam.Rows[0]["section3Ques"].ToString() + "," + dtExam.Rows[0]["section1Ques"].ToString() + "," + dtExam.Rows[0]["section5Ques"].ToString(); }
                    if (lblSectionID.Text == "section5Ques")
                    { lblQueAdded.Text = dtExam.Rows[0]["section5Ques"].ToString(); lblHeading.Text = lblHeading.Text + "5"; quesAddedToExam = dtExam.Rows[0]["section2Ques"].ToString() + "," + dtExam.Rows[0]["section3Ques"].ToString() + "," + dtExam.Rows[0]["section4Ques"].ToString() + "," + dtExam.Rows[0]["section1Ques"].ToString(); }

                    lblQueAddedToExam.Text = quesAddedToExam;
                    int n = 0;
                    int addedTo1 = 0; int addedTo2 = 0; int addedTo3 = 0; int addedTo4 = 0; int addedTo5 = 0;
                    if (dtExam.Rows[0]["section1Ques"].ToString().Length > 0)
                    { string[] addedQues = dtExam.Rows[0]["section1Ques"].ToString().Split(','); n = addedQues.Length; addedTo1 = n; }
                    if (dtExam.Rows[0]["section2Ques"].ToString().Length > 0)
                    { string[] addedQues = dtExam.Rows[0]["section2Ques"].ToString().Split(','); n = n + addedQues.Length; addedTo2 = addedQues.Length; }
                    if (dtExam.Rows[0]["section3Ques"].ToString().Length > 0)
                    { string[] addedQues = dtExam.Rows[0]["section3Ques"].ToString().Split(','); n = n + addedQues.Length; addedTo3 = addedQues.Length; }
                    if (dtExam.Rows[0]["section4Ques"].ToString().Length > 0)
                    { string[] addedQues = dtExam.Rows[0]["section4Ques"].ToString().Split(','); n = n + addedQues.Length; addedTo4 = addedQues.Length; }
                    if (dtExam.Rows[0]["section5Ques"].ToString().Length > 0)
                    { string[] addedQues = dtExam.Rows[0]["section5Ques"].ToString().Split(','); n = n + addedQues.Length; addedTo5 = addedQues.Length; }


                    lblExamName.Text = "Exam Name : " + dtExam.Rows[0]["examName"].ToString() + "";
                    if (Session["MaxQueNo"] != null)
                    {
                        lblMax.Text = "Max Questions : " + Session["MaxQueNo"].ToString() + "";
                    }
                    else
                    {
                        lblMax.Text = "Max Questions : " + dtExam.Rows[0]["queNos"].ToString() + "";
                    }
                    lblAlreadyAdd.Text = n.ToString();
                    int remaining = Convert.ToInt16(dtExam.Rows[0]["quenos"].ToString()) - n;
                    //MAX questions to add for current Section = previously added questions + remaining questions;
                    if (lblSectionID.Text == "section1Ques")
                    { lblMaxQuestion.Text = (remaining + addedTo1).ToString(); }
                    if (lblSectionID.Text == "section2Ques")
                    { lblMaxQuestion.Text = (remaining + addedTo2).ToString(); }
                    if (lblSectionID.Text == "section3Ques")
                    { lblMaxQuestion.Text = (remaining + addedTo3).ToString(); }
                    if (lblSectionID.Text == "section4Ques")
                    { lblMaxQuestion.Text = (remaining + addedTo4).ToString(); }
                    if (lblSectionID.Text == "section5Ques")
                    { lblMaxQuestion.Text = (remaining + addedTo5).ToString(); }



                    if (lblQueAdded.Text.Length > 0)
                    {
                        string[] ques = lblQueAdded.Text.Split(',');
                        DataTable dt = new DataTable();
                        dt.Columns.Add("questid", typeof(string));
                        for (int i = 0; i < ques.Length; i++)
                        {
                            if (ques[i].ToString().Length > 0)
                            {
                                DataRow dtrow = dt.NewRow();
                                dtrow["questId"] = ques[i].ToString();
                                dt.Rows.Add(dtrow);
                                if (i == 0)
                                {
                                    lblQueAdded.Text = ques[0].ToString();
                                }

                                else { lblQueAdded.Text = lblQueAdded.Text + "," + ques[i].ToString(); }
                            }
                        }
                        repAddedQues.DataSource = dt;
                        repAddedQues.DataBind();
                        repAddedQues.Visible = true;
                    }
                    else { repAddedQues.Visible = false; }
                }
            }
            catch { }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid FROM dbo.view_examQuestions WHERE subid = " + ddlSubjects.SelectedValue + " and courseid = " + ddlCourse.SelectedValue + " and instituteId=" + lblInsId.Text + "");
            if (dt.Rows.Count > 0)
            {
                repQueNos.DataSource = dt;
                repQueNos.DataBind();

            }
            else
            {
                repQueNos.Visible = false;

            }
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourse.SelectedItem.Text.Length > 0)
            {
                sourceSubjects.SelectCommand = "SELECT subjectinfo,subid from exm_subject WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                ddlSubjects.DataBind();
            }

        }

        protected void btnRep_Click(object sender, EventArgs e)
        {

            Button btn = (Button)sender;

            string qID = btn.CommandArgument;
            queHolder.Visible = true;
            //int z = Convert.ToInt16(btn.Text);
            lblQuestionNumber.Text = "Question No : " + btn.Text;
            lblQuestionNumber.Visible = true;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext, mark, negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Info"].ToString().Length > 0)
                {
                    repQueInfo.DataSource = dt;
                    repQueInfo.DataBind();
                    Panel3.Visible = true;
                }
                else { Panel3.Visible = false; }
            }
            else { Panel3.Visible = false; }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            savedChanges.InnerHtml = "0";
            lblerror.Visible = false;
            Button btn = (Button)sender;
            if (btn.Text == "Add to Exam")
            {
                int queLength = 0;
                string[] quenos = null;
                if (lblQueAdded.Text.Length > 0)
                {
                    quenos = lblQueAdded.Text.Split(',');
                    queLength = quenos.Length;
                }
                else
                {
                    queLength = 0;
                }
                if (Convert.ToInt16(lblMaxQuestion.Text) > queLength)
                {


                    string qID = btn.CommandArgument;
                    //int z = Convert.ToInt16(btn.Text);

                    if (lblQueAdded.Text.Length == 0)
                    {
                        string add = "yes";
                        string[] que = lblQueAddedToExam.Text.Split(',');
                        for (int x = 0; x < que.Length; x++)
                        {
                            if (qID == que[x].ToString())
                            {
                                add = "no";

                            }
                        }

                        if (add == "yes")
                        {
                            lblQueAdded.Text = qID;
                            DataTable dt = new DataTable();
                            dt.Columns.Add("questid", typeof(string));
                            DataRow dtrow = dt.NewRow();
                            dtrow["questId"] = lblQueAdded.Text;
                            dt.Rows.Add(dtrow);
                            repAddedQues.DataSource = dt;
                            repAddedQues.DataBind();
                            repAddedQues.Visible = true;
                            DataTable dt1 = new DataTable();
                            dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " and InstituteId=" + lblInsId.Text + "");
                            Repeater1.DataSource = dt1;
                            Repeater1.DataBind();
                            lblAlreadyAdd.Text = (Convert.ToInt16(lblAlreadyAdd.Text) + 1).ToString();
                        }
                        if (add == "no")
                        {
                            string script = "<script type=\"text/javascript\">showMsg();</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                        }
                    }
                    else
                    {
                        string foundIn = "";
                        string add = "yes";
                        string[] que = lblQueAdded.Text.Split(',');
                        for (int x = 0; x < que.Length; x++)
                        {
                            if (qID == que[x].ToString())
                            {
                                add = "no";
                                foundIn = "Section";
                            }
                        }

                        que = lblQueAddedToExam.Text.Split(',');
                        for (int x = 0; x < que.Length; x++)
                        {
                            if (qID == que[x].ToString())
                            {
                                add = "no";
                                foundIn = "OtherSection";
                            }
                        }

                        if (add == "no")
                        {
                            if (foundIn == "Section")
                            {
                                string script = "<script type=\"text/javascript\">showAlert1();</script>";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                            }
                            else
                            {
                                string script = "<script type=\"text/javascript\">showMsg();</script>";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                            }
                        }

                        if (add == "yes")
                        {
                            lblQueAdded.Text = lblQueAdded.Text + "," + qID;
                            string[] ques = lblQueAdded.Text.Split(',');
                            DataTable dt = new DataTable();
                            dt.Columns.Add("questid", typeof(string));
                            for (int i = 0; i < ques.Length; i++)
                            {
                                if (ques[i].ToString().Length > 0)
                                {
                                    DataRow dtrow = dt.NewRow();
                                    dtrow["questId"] = ques[i].ToString();
                                    dt.Rows.Add(dtrow);
                                    if (i == 0)
                                    {
                                        lblQueAdded.Text = ques[0].ToString();
                                    }

                                    else { lblQueAdded.Text = lblQueAdded.Text + "," + ques[i].ToString(); }
                                }
                            }
                            repAddedQues.DataSource = dt;
                            repAddedQues.DataBind();
                            repAddedQues.Visible = true;
                            DataTable dt1 = new DataTable();
                            dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");

                            Repeater1.DataSource = dt1;
                            Repeater1.DataBind();
                            lblAlreadyAdd.Text = (Convert.ToInt16(lblAlreadyAdd.Text) + 1).ToString();
                        }
                    }
                }
                else
                {
                    string script = "<script type=\"text/javascript\">showAlert(" + lblMaxQuestion.Text + " );</script>";

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                }

                //if (Convert.ToInt16(lblMaxQuestion.Text) == quenos.Length)
                if (Convert.ToInt16(lblMaxQuestion.Text) == queLength)
                {
                    //  btnFinish.Enabled = true;
                }
            }

            if (btn.Text == "Delete from Exam")
            {
                string qID = btn.CommandArgument;
                string toReplace = qID + ",";

                string newQue = lblQueAdded.Text.Replace(toReplace, "");

                lblQueAdded.Text = "";
                string[] quenos = newQue.Split(',');
                DataTable dt = new DataTable();
                dt.Columns.Add("questid", typeof(string));
                for (int i = 0; i < quenos.Length; i++)
                {
                    if (quenos[i].ToString().Length > 0)
                    {
                        if (quenos[i].ToString() != qID)
                        {
                            DataRow dtrow = dt.NewRow();
                            dtrow["questId"] = quenos[i].ToString();
                            dt.Rows.Add(dtrow);
                            if (i == 0)
                            {
                                lblQueAdded.Text = quenos[0].ToString();
                            }

                            else { lblQueAdded.Text = lblQueAdded.Text + "," + quenos[i].ToString(); }
                        }
                    }
                }
                repAddedQues.DataSource = dt;
                repAddedQues.DataBind();

                repAddedQues.Visible = true;
                DataTable dt1 = new DataTable();
                dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");

                Repeater1.DataSource = dt1;
                Repeater1.DataBind();
                lblAlreadyAdd.Text = (Convert.ToInt16(lblAlreadyAdd.Text) - 1).ToString();
            }
        }
        protected void btnRep1_Click(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            //int z = Convert.ToInt16(btn.Text);
            lblQuestionNumber.Text = "Question No : " + btn.Text;
            lblQuestionNumber.Visible = true;
            queHolder.Visible = true;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext, mark, negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            if (dt.Rows[0]["Info"].ToString().Length > 0)
            {
                repQueInfo.DataSource = dt;
                repQueInfo.DataBind();
                Panel3.Visible = true;
            }
            else { Panel3.Visible = false; }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            string query = "UPDATE exm_existingExams SET " + lblSectionID.Text + "=";
            ob.setSectionQue(query, lblExamID.Text, lblQueAdded.Text);

            savedChanges.InnerHtml = "1";
            if (lblSectionID.Text == "section1Ques")
            { Session["examSection"] = "1"; }
            if (lblSectionID.Text == "section2Ques")
            { Session["examSection"] = "2"; }
            if (lblSectionID.Text == "section3Ques")
            { Session["examSection"] = "3"; }
            if (lblSectionID.Text == "section4Ques")
            { Session["examSection"] = "4"; }
            if (lblSectionID.Text == "section5Ques")
            { Session["examSection"] = "5"; }
            string script = "<script type=\"text/javascript\">callfun();</script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            ddlFrom.Items.Clear();
            ddlTo.Items.Clear();
            ddlFrom.Items.Add("0");
            string[] que = lblQueAdded.Text.Split(',');
            for (int x = 0; x < que.Length; x++)
            {
                if (que[x].ToString().Length > 0)
                {
                    ddlFrom.Items.Add(que[x].ToString());
                    ddlTo.Items.Add(que[x].ToString());
                }
            }
            ddlTo.DataBind();
            ddlFrom.DataBind();
            Panel4.Visible = true;
            Panel2.Visible = false;
            lblInsertQueError.Visible = false;
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            string qID = txtUniqueID.Text;

            string addInBetween = ddlFrom.Text + "," + ddlTo.Text;
            //search addInBetween in added question list to add que between it.



            if (lblQueAdded.Text.Contains(addInBetween))
            {
                string[] que = lblQueAdded.Text.Split(',');
                if (Convert.ToInt16(lblMaxQuestion.Text) > que.Length)
                {
                    string foundIn = "";
                    string add = "yes";

                    for (int x = 0; x < que.Length; x++)
                    {
                        if (qID == que[x].ToString())
                        {
                            add = "no";
                            foundIn = "Section";
                        }
                    }

                    que = lblQueAddedToExam.Text.Split(',');
                    for (int x = 0; x < que.Length; x++)
                    {
                        if (qID == que[x].ToString())
                        {
                            add = "no";
                            foundIn = "OtherSection";
                        }
                    }


                    if (add == "no")
                    {
                        if (foundIn == "Section")
                        {
                            string script = "<script type=\"text/javascript\">showAlert1();</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                        }
                        else
                        {
                            string script = "<script type=\"text/javascript\">showMsg();</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                        }
                    }


                    if (add == "yes")
                    {

                        string[] ques = lblQueAdded.Text.Split(',');
                        lblQueAdded.Text = "";
                        DataTable dt = new DataTable();
                        dt.Columns.Add("questid", typeof(string));
                        for (int i = 0; i < ques.Length; i++)
                        {
                            if (ques[i].ToString().Length > 0)
                            {
                                if (i == 0)
                                {
                                    lblQueAdded.Text = ques[0].ToString();
                                    if (ques[i].ToString() == ddlTo.Text)
                                    {
                                        lblQueAdded.Text = qID + "," + ques[i].ToString();
                                    }
                                    else
                                    {
                                        lblQueAdded.Text = ques[i].ToString();
                                    }
                                }

                                else
                                {
                                    if (ques[i].ToString() == ddlTo.Text)
                                    {
                                        lblQueAdded.Text = lblQueAdded.Text + "," + qID + "," + ques[i].ToString();
                                    }
                                    else
                                    {
                                        lblQueAdded.Text = lblQueAdded.Text + "," + ques[i].ToString();
                                    }
                                    //lblQueAdded.Text = lblQueAdded.Text + "," + ques[i].ToString();
                                }

                            }
                        }
                        string[] quenos = lblQueAdded.Text.Split(',');

                        ddlFrom.Items.Clear(); ;
                        ddlTo.Items.Clear();
                        ddlFrom.Items.Add("0");
                        for (int i = 0; i < quenos.Length; i++)
                        {
                            if (quenos[i].ToString().Length > 0)
                            {
                                DataRow dtrow = dt.NewRow();
                                dtrow["questId"] = quenos[i].ToString();
                                dt.Rows.Add(dtrow);
                                ddlFrom.Items.Add(quenos[i].ToString());
                                ddlTo.Items.Add(quenos[i].ToString());
                            }
                        }
                        repAddedQues.DataSource = dt;
                        repAddedQues.DataBind();

                        lblAlreadyAdd.Text = (Convert.ToInt16(lblAlreadyAdd.Text) + 1).ToString();
                    }


                }
                else
                {
                    string script = "<script type=\"text/javascript\">showAlert(" + lblMaxQuestion.Text + " );</script>";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                }
            }
            else
            {
                string script = "<script type=\"text/javascript\">queSelectionError();</script>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }

        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            lblInsertQueError.Visible = false;
            Panel4.Visible = false;
            Panel2.Visible = true; ;
        }
    }
}
