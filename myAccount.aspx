﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="myAccount.aspx.cs" Inherits="OnlineExam.myAccount" Title="My Profile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/accordion.css" rel="stylesheet" type="text/css" />
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
  function showResult()
        {
        window.open("exmResult.aspx", "Exam Result", "location=1, width=auto, height=auto");
        }
    </script>

    <script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
        document.getElementById("ctl00_ContentPlaceHolder1_Button1").click();     
       }
   
  function openNewWindow()
        {
        window.open("Exam.aspx", "Exam", "location=1, width=auto, height=auto");
        }
    </script>

    <style type="text/css">
        .linkBtnCSS
        {
            font-weight: bold;
            color: Blue;
            text-decoration: underline;
        }
        table.hovertable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
            width: 100%;
        }
        table.hovertable th
        {
            background-color: #c3dde0;
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
        }
        table.hovertable tr
        {
            background-color: #d4e3e5;
        }
        table.hovertable td
        {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            </div>
            <asp:Label ID="lblUID" Visible="false" runat="server" Text="Label"></asp:Label>
            <table width="100%">
                <tr>
                    <td style="padding: 20px 20px 20px 20px" align="left">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <div class="pageHeading">
                                        My Profile</div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Accordion ID="Accordion1" runat="server" FadeTransitions="true" TransitionDuration="250"
                                        FramesPerSecond="40" AutoSize="None" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                                        ContentCssClass="accordionContent" RequireOpenedPane="false">
                                        <Panes>
                                            <asp:AccordionPane ID="AccPan1" runat="server">
                                                <Header>
                                                    Personal Details</Header>
                                                <Content>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100px">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="center" width="100px">
                                                                            <asp:Image ID="Image1" runat="server" Width="80px" Height="90px" ImageUrl="~/stdImages/default.jpg" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:LinkButton ID="linkRemoveImage" runat="server" Style="text-decoration: none"
                                                                                CausesValidation="false" Visible="false" OnClick="linkRemoveImage_Click">Remove Image</asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="left" style="width: 10px">
                                                                &nbsp;
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="lblInstitute" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="lblName" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="lblEmail" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Label ID="lblMo" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:LinkButton ID="linkUpdatePer" OnClick="linkUpdatePer_Click" CausesValidation="false"
                                                                                CssClass="linkBtnCSS" runat="server"><img src="images/editInfo.png" width="20px" height="20px"   alt=""/>Edit Info</asp:LinkButton>
                                                                            <br />
                                                                            <asp:LinkButton ID="changePassword" CausesValidation="false" runat="server" CssClass="linkBtnCSS"><img src="images/editInfo.png" width="20px" height="20px"  alt=""/>Change Password</asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="display: none">
                                                        <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                                                    </div>
                                                    <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="LinkButton1"
                                                        BackgroundCssClass="ModalPopupBG" PopupControlID="changeInfo" Drag="true" CancelControlID="btn3">
                                                    </asp:ModalPopupExtender>
                                                    <div id="changeInfo" style="display: none;" class="popupConfirmation">
                                                        <iframe id="Iframe1" frameborder="0" src="editPersonalInfo.aspx" height="300px" width="400px">
                                                        </iframe>
                                                        <div class="popup_Buttons" style="display: none">
                                                            <input id="Button2" value="Done" type="button" />
                                                            <input id="btn3" value="Cancel" type="button" />
                                                        </div>
                                                    </div>
                                                    <asp:ModalPopupExtender ID="ModalPopupExtender4" runat="server" TargetControlID="changePassword"
                                                        BackgroundCssClass="ModalPopupBG" PopupControlID="changePass" Drag="true" CancelControlID="btnCancelPass">
                                                    </asp:ModalPopupExtender>
                                                    <div id="changePass" style="display: none;" class="popupConfirmation">
                                                        <iframe id="Iframe4" frameborder="0" src="editPassword.aspx" height="300px" width="400px">
                                                        </iframe>
                                                        <div class="popup_Buttons" style="display: none">
                                                            <input id="Button5" value="Done" type="button" />
                                                            <input id="btnCancelPass" value="Cancel" type="button" />
                                                        </div>
                                                    </div>
                                                </Content>
                                            </asp:AccordionPane>
                                            <asp:AccordionPane ID="AccPan2" runat="server">
                                                <Header>
                                                    Contact Information
                                                </Header>
                                                <Content>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 150px">
                                                                <asp:Label ID="Label1" runat="server" Text="Additional Email" CssClass="myLabelHead"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEEmail" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <asp:Label ID="Label2" runat="server" Text="Permanent Address" CssClass="myLabelHead"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAdd" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="linkeditInfo_Click" CssClass="linkBtnCSS"><img src="images/editInfo.png" width="20px" height="20px"  alt=""/>Edit</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="display: none">
                                                        <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="linkChangeContact" CausesValidation="false" runat="server" CssClass="linkBtnCSS"><img src="images/editInfo.png" width="20px" height="20px"  alt=""/>Edit</asp:LinkButton>
                                                    </div>
                                                    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkChangeContact"
                                                        BackgroundCssClass="ModalPopupBG" PopupControlID="changeContactInfo" Drag="true"
                                                        CancelControlID="btnCinfoCancel">
                                                    </asp:ModalPopupExtender>
                                                    <div id="changeContactInfo" style="display: none;" class="popupConfirmation">
                                                        <iframe id="Iframe2" frameborder="0" src="editContactInfo.aspx" height="350px" width="500px">
                                                        </iframe>
                                                        <div class="popup_Buttons" style="display: none">
                                                            <input id="Button3" value="Done" type="button" />
                                                            <input id="btnCinfoCancel" value="Cancel" type="button" />
                                                        </div>
                                                    </div>
                                                </Content>
                                            </asp:AccordionPane>
                                            <asp:AccordionPane ID="AccPan3" runat="server">
                                                <Header>
                                                    About You
                                                </Header>
                                                <Content>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <asp:Label ID="lblAbtyou" runat="server" Text="-" CssClass="myLabel"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:LinkButton ID="linkChangeAbtYou" CssClass="linkBtnCSS" runat="server"><img src="images/editInfo.png" width="20px" height="20px"  alt=""/>Edit</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="linkChangeAbtYou"
                                                        BackgroundCssClass="ModalPopupBG" PopupControlID="changeAboutYou" Drag="true"
                                                        CancelControlID="btnAboutCancel">
                                                    </asp:ModalPopupExtender>
                                                    <div id="changeAboutYou" style="display: none;" class="popupConfirmation">
                                                        <iframe id="Iframe3" frameborder="0" src="editAboutu.aspx" height="400px" width="650px">
                                                        </iframe>
                                                        <div class="popup_Buttons" style="display: none">
                                                            <input id="Button4" value="Done" type="button" />
                                                            <input id="btnAboutCancel" value="Cancel" type="button" />
                                                        </div>
                                                    </div>
                                                </Content>
                                            </asp:AccordionPane>
                                            <asp:AccordionPane ID="AccordionPane2" runat="server" Visible="false">
                                                <Header>
                                                    Testimony
                                                </Header>
                                                <Content>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="right" valign="top">
                                                                <asp:LinkButton ID="linkAddTetimony" CausesValidation="false" runat="server" OnClick="linkAddTetimony_Click"
                                                                    CssClass="linkBtnCSS">Add Testimony</asp:LinkButton>
                                                                <asp:Panel ID="Panel1" runat="server" Visible="false">
                                                                    <center>
                                                                        <table>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <asp:Label ID="lblMsg" runat="server" Text="Label" Visible="false"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    Testimony
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtTestimony" TextMode="MultiLine" Width="500px" Height="150px"
                                                                                        runat="server"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                                                                                        CssClass="simplebtn" OnClick="btnCancel_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </center>
                                                                </asp:Panel>
                                                                <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand"
                                                                    DataSourceID="SqlDataSource1">
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblTestimony" runat="server" Text='<%#Eval("testimony") %>' CssClass="myLabel"></asp:Label>
                                                                                    <asp:Label ID="lblTID" Visible="false" runat="server" Text='<%#Eval("id") %>' CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:LinkButton ID="LinkButton3" CssClass="linkBtnCSS" CommandName="editItem" runat="server"><img src="images/editInfo.png" width="20px" height="20px"  alt=""/>Edit</asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Label ID="lblTestimonyID" runat="server" Text="Label" Visible="false"></asp:Label>
                                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                                                </Content>
                                            </asp:AccordionPane>
                                            <asp:AccordionPane ID="AccordionPane3" runat="server" Visible="false">
                                                <Header>
                                                    Expert's Answers
                                                </Header>
                                                <Content>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="right" valign="top">
                                                                <asp:LinkButton ID="linkAskToExpert" CausesValidation="false" runat="server" OnClick="linkAskToExpert_Click"
                                                                    CssClass="linkBtnCSS">Ask to Expert</asp:LinkButton>
                                                                <asp:Panel ID="Panel2" runat="server" Visible="false">
                                                                    <center>
                                                                        <table>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <asp:Label ID="lblMsg1" runat="server" Text="Label" Visible="false"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    Question
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtExpertQue" TextMode="MultiLine" Width="500px" Height="150px"
                                                                                        runat="server"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:Button ID="btnAskToExpert" runat="server" Text="Submit" CssClass="simplebtn"
                                                                                        OnClick="btnAskToExpert_Click" />
                                                                                    <asp:Button ID="btnCancel1" runat="server" Text="Cancel" CausesValidation="false"
                                                                                        CssClass="simplebtn" OnClick="btnCancel1_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </center>
                                                                </asp:Panel>
                                                                <asp:Repeater ID="rptExpert" runat="server" DataSourceID="sourceExpert">
                                                                    <HeaderTemplate>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" style="width: 40px">
                                                                                    <b>Que : </b>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("question") %>' Font-Bold="true"></asp:Label><br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 40px">
                                                                                    <b>Ans : </b>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("answer") %>'></asp:Label><br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <hr style="width: 100%; border-bottom: solid 2px white" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label>
                                                    <asp:SqlDataSource ID="sourceExpert" runat="server"></asp:SqlDataSource>
                                                </Content>
                                            </asp:AccordionPane>
                                            <asp:AccordionPane ID="AccordionPane1" runat="server">
                                                <Header>
                                                    Exam Details
                                                </Header>
                                                <Content>
                                                    <asp:TabContainer ID="TabContainer1" runat="server">
                                                        <asp:TabPanel runat="server" ID="tab1">
                                                            <HeaderTemplate>
                                                                <b>Unfinished Exams</b>
                                                            </HeaderTemplate>
                                                            <ContentTemplate>
                                                                <asp:GridView ID="gridUnFinished" runat="server" Caption="<b>Unfinished Exams</b>"
                                                                    CssClass="hovertable" AutoGenerateColumns="false" OnRowCommand="gridUnFinished_RowCommand"
                                                                    OnRowCreated="gridUnFinished_RowCreated">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="newexm" HeaderText="ExamID" SortExpression="newexm" />
                                                                        <asp:BoundField DataField="examName" HeaderText="Exam Name" SortExpression="examName" />
                                                                        <asp:BoundField DataField="noquestion" HeaderText="No of Questions" SortExpression="noquestion" />
                                                                        <asp:BoundField DataField="onDate" HeaderText="On Date" SortExpression="onDate" />
                                                                        <asp:BoundField DataField="remTime" HeaderText="Remaining Time(Hr:Min:Sec)" SortExpression="remTime" />
                                                                        <asp:ButtonField ButtonType="Image" CommandName="restart" HeaderText="Restart" ImageUrl="/images/restart.png">
                                                                            <ItemStyle HorizontalAlign="Center" Width="50px" Height="15px" />
                                                                        </asp:ButtonField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="sourceUnFinished" runat="server"></asp:SqlDataSource>
                                                            </ContentTemplate>
                                                        </asp:TabPanel>
                                                        <asp:TabPanel HeaderText="<b>Purchase Summary</b>" runat="server" ID="TabPanel1">
                                                            <HeaderTemplate>
                                                                <b>Finished Exams</b>
                                                            </HeaderTemplate>
                                                            <ContentTemplate>
                                                                <asp:GridView ID="gridFinishedExam" runat="server" DataSourceID="sourceFinishedExam"
                                                                    Caption="<b>Finished Exams</b>" CssClass="hovertable" AutoGenerateColumns="false"
                                                                    OnRowCreated="gridUnFinished_RowCreated" OnRowCommand="gridFinishedExam_RowCommand">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="newexm" HeaderText="ExamID" SortExpression="catid" />
                                                                        <asp:BoundField DataField="examName" HeaderText="Exam Name" SortExpression="examName" />
                                                                        <asp:BoundField DataField="noquestion" HeaderText="No of Questions" SortExpression="noquestion" />
                                                                        <asp:BoundField DataField="onDate" HeaderText="On Date" SortExpression="onDate" />
                                                                        <asp:ButtonField ButtonType="Image" CommandName="details" HeaderText="Details" ImageUrl="/images/Details.png">
                                                                            <ItemStyle HorizontalAlign="Center" Width="20px" Height="20px" />
                                                                        </asp:ButtonField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="sourceFinishedExam" runat="server"></asp:SqlDataSource>
                                                            </ContentTemplate>
                                                        </asp:TabPanel>
                                                    </asp:TabContainer>
                                                </Content>
                                            </asp:AccordionPane>
                                        </Panes>
                                    </asp:Accordion>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
