﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class thankYou : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                    messages11.Visible = false;
                    //IntegrationKit.libfuncs myUtility = new IntegrationKit.libfuncs();
                    string Order_Id; //, //WorkingKey, Merchant_Id, Amount, AuthDesc, Checksum, checksum, username, Tranzaction_type;

                    //Assign following values to send it to verifychecksum function.
                    //WorkingKey = "F1284481122F02FA703EF7CFC70A6707";  // put in the 32 bit working key in the quotes provided here
                    //Merchant_Id = Request.Form["Merchant_Id"];
                    Order_Id = Session["order_id"].ToString();  //Request.Form["Order_Id"];
                    //Amount = Request.Form["Amount"];
                    //AuthDesc = Request.Form["AuthDesc"];
                    //checksum = Request.Form["Checksum"];
                    //Tranzaction_type = Request.Form["billing_cust_notes"];
                    //username = Request.Form["billing_cust_email"];
                    if (Order_Id != null)
                    {
                        //Checksum = myUtility.verifychecksum(Merchant_Id, Order_Id, Amount, AuthDesc, WorkingKey, checksum);

                        int j = ob.updateTransStatus(Order_Id.Replace("A", ""));
                        if (j == 1)
                        {
                            lblMsg.Text = "Thank You!!! Your transaction is Successful!!!";

                            DataTable dtExm = ob.getData("Select examID,exmAttempt from exm_paidExmUsers where transId=" + Order_Id.Replace("A", ""));

                            Session["resExam"] = "Yes";

                            string userID = val[0].ToString();

                            DataTable dt = ob.getData("SELECT examID, examName,isnull(section1Ques,'') +','+ isnull(section2Ques,'') +','+ isnull(section3Ques,'') +','+ isnull(section4Ques,'' ) +','+isnull(section5Ques,'' ) as questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, examType FROM exm_existingExams where examID = " + dtExm.Rows[0][0].ToString() + "");
                            ViewState["examDetails"] = dt;
                            string maxExamID = "";
                            for (int k = 0; k < Convert.ToInt32(dtExm.Rows[0][1].ToString()); k++)
                            {
                                maxExamID = ob.getValue("select max(newexm)+1 from exm_newexam");
                                ob.InsertUserQuestionList(maxExamID, dt.Rows[0]["noofQuestions"].ToString(), dt.Rows[0]["questionNos"].ToString(), val[0].ToString(), Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()), dt.Rows[0]["courseID"].ToString(), "Pre Exam", dt.Rows[0]["noOfSections"].ToString(), dtExm.Rows[0][0].ToString());
                                string[] queNos = dt.Rows[0]["questionNos"].ToString().Split(',');
                                int noOfQuestions = Convert.ToInt32(dt.Rows[0]["noofQuestions"].ToString());
                                for (int i = 0; i < noOfQuestions; i++)
                                {
                                    if (queNos[i].Length > 0)
                                    {
                                        ob.insertUseAnswer(val[0].ToString(), maxExamID, queNos[i].ToString(), "0", Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()));
                                    }
                                }
                            }
                            Session["resPreExam"] = "No";
                            Session["examID"] = maxExamID;
                            Session["userID"] = val[0].ToString();
                            string script = "<script type=\"text/javascript\">openNewWindow()</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

                        }
                        else
                        {
                            lblMsg.Text = "Something went wrong!!! Transaction Not successful! Please try again!";
                        }
                    }
                    else
                    {
                        Response.Redirect("/testsList.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                messages11.Visible = true;
                messages11.setMessage(0, ex.Message);
            }
        }
    }
}
