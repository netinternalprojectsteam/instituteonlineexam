﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="createUserAccount.aspx.cs"
    Inherits="OnlineExam.createUserAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Login</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnMasterCanAccount').click();
        }
        
         function callfun()
    {
      parent.CallMasterAlert();
        return false;
    }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Sign Up Now
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:Panel ID="Panel1" runat="server">
            <center>
                <table>
                    <tr>
                        <td align="center" colspan="2">
                            <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label1" runat="server" Text="Name" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtbox" Width="250px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="Email ID" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmailID" runat="server" CssClass="txtbox" Width="250px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailID"
                                ErrorMessage="*"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                    runat="server" ErrorMessage="Invalid email !" ControlToValidate="txtEmailID"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Mobile" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMo" runat="server" CssClass="txtbox" Width="250px" MaxLength="12"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMo"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" Text="Password" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="txtbox" Width="250px" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                                ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Button ID="Button1" CssClass="simplebtn" runat="server" Text="Sign Up Now" OnClick="Button1_Click" />
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
