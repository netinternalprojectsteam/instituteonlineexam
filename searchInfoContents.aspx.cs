﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class searchInfoContents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
            }
            //System.Threading.Thread.Sleep(4000);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {//
            soureceSearch.SelectCommand = "SELECT  infID, CASE WHEN len(infheading) > 50 THEN LEFT(infHeading, 50) + '...' ELSE infHeading END AS infHeading, CASE WHEN len(infContents) > 100 THEN LEFT(infContents, 100) + '...' ELSE infContents END AS infContents FROM exm_queinfo where ({ fn LENGTH('" + txtInfoHeading.Text + "') } = 0 OR infHeading  LIKE '%' + '" + txtInfoHeading.Text + "' + '%') AND ({ fn LENGTH('" + txtInfo.Text + "') } = 0 OR infContents  LIKE '%' + '" + txtInfo.Text + "' + '%') AND (instituteId=" + lblInsId.Text + ")";
            soureceSearch.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridResult.DataBind();
        }

        protected void gridResult_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gridResult_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "details")
            {
                row = Convert.ToInt32(e.CommandArgument);
                sourceInfo.SelectCommand = "SELECT infID, infHeading, infContents FROM exm_queinfo WHERE (infID = " + gridResult.Rows[row].Cells[0].Text + ")";
                sourceInfo.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                repQueInfo.DataBind();
                Panel2.Visible = true;


                Button btn = (Button)repQueInfo.Items[0].FindControl("Button1");
                btn.Focus();

            }
            if (e.CommandName == "select")
            {
                row = Convert.ToInt32(e.CommandArgument);
                string infID = gridResult.Rows[row].Cells[0].Text;
                Session["InfID"] = infID;
                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }


        protected void btnRep_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string infID = btn.CommandArgument;
            Session["InfID"] = infID;
            string script = "<script type=\"text/javascript\">callfun();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void gridResult_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
            }
        }
    }
}
