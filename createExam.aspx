<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="createExam.aspx.cs" Inherits="OnlineExam.createExam" Title="Create New Exam" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript"> 
        function showAlert()
        {       
            alert('Can not add. Maximum Questions reached ! ');
        }
        
         function showAlert1()
        {       
            alert('Question already added ! ');
        }
        
        
    </script>

    <script type="text/javascript" language="javascript"> 
        function ConfirmOnFinish()
        {
        if (confirm("Max questions not reach?")==true)
        return true;
        else 
        return false;
        }
    </script>

    <script type="text/javascript" language="javascript">
    function alertForQueUpdate()
    {
    alert("Please update previous questions! Questions limit exceeds!");
    }
    </script>

    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .pnlScroll
        {
            overflow-y: scroll;
            max-height: 500px;
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblQues" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblInstituteId" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblQuesNoOld" runat="server" Text="" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Manage Exam
                        </h3>
                    </td>
                    <asp:Label ID="lblExamID" Visible="false" runat="server" Text="Label"></asp:Label>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server">
                            <center>
                                <table>
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lblerror" Visible="false" runat="server" Text="" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label21" runat="server" Text="Scheduled Date" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars"
                                                ValidChars="0123456789./-" TargetControlID="txtDate">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Exam Name" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="Max Time(in Mins)" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTime" runat="server" MaxLength="3"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtTime"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTime"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Course Name" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <%--<asp:TextBox ID="txtType" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtType"></asp:RequiredFieldValidator>--%>
                                            <asp:DropDownList ID="ddlCourses" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                                DataValueField="courseid" AutoPostBack="True" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="No of Questions" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtQueNo" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtQueNo"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtQueNo"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label9" runat="server" Text="No of Sections" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSections" runat="server" MaxLength="1"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtSections"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Value between 1 to 5"
                                                ControlToValidate="txtSections" MinimumValue="1" MaximumValue="5"></asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label20" runat="server" Text="Under Course (Package)" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlUnderCourse" runat="server">
                                                <asp:ListItem>Bank PO</asp:ListItem>
                                                <asp:ListItem>Clerk</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="sourceClasses" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                SelectCommand="SELECT id, className FROM site_courses WHERE (sessionID = (SELECT id FROM site_Sessions WHERE (isActive = 1)))">
                                            </asp:SqlDataSource>
                                            <%--
                                            <asp:DropDownList ID="ddlUnderCourse" runat="server">
                                                <asp:ListItem>FIRST YEAR</asp:ListItem>
                                                <asp:ListItem>SECOND YEAR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MINOR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MAJOR</asp:ListItem>
                                                <asp:ListItem>INTERNS</asp:ListItem>
                                            </asp:DropDownList>
                                       
                                            <asp:DropDownList ID="ddlUnderCourse" runat="server" 
                                                DataSourceID="sourceCoursePackage" DataTextField="courseName" 
                                                DataValueField="id">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="sourceCoursePackage" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>" 
                                                onselecting="sourceCoursePackage_Selecting" 
                                                SelectCommand="SELECT [id], [courseName] FROM [site_courses]"></asp:SqlDataSource>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label19" runat="server" Text="Exam Type" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlType" runat="server">
                                                <asp:ListItem>Free</asp:ListItem>
                                                <asp:ListItem>Paid</asp:ListItem>
                                                <asp:ListItem>Demo</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label8" runat="server" Text="Is Available" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkAvailable" runat="server" Checked="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label16" runat="server" Text="Negative Marking" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkNegMark" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label23" runat="server" Text="Exam Description" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Width="200px" Height="100px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update & Continue" OnClick="btnUpdate_Click"
                                                CssClass="simplebtn" Visible="false" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                                                OnClick="btnCancel_Click" Visible="false" CssClass="simplebtn" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="Panel4" runat="server">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="txtLast" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLast"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:Button ID="btnLatest" runat="server" Text="Search Exam" OnClick="btnLatest_Click"
                                                    CausesValidation="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Panel ID="pnlGrid" runat="server" CssClass="pnlScroll">
                                                    <asp:GridView ID="gridExam" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                                        AllowPaging="True" PageSize="20" OnRowCommand="gridExam_RowCommand" OnRowCreated="gridExam_RowCreated">
                                                        <Columns>
                                                            <asp:BoundField DataField="examID" HeaderText="examID" />
                                                            <asp:TemplateField HeaderText="Sr.No." HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex+1 %></ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="examName" HeaderText="Exam Name" />
                                                            <asp:BoundField DataField="maxTime" HeaderText="Max Time(Hr:Min:Sec)" />
                                                            <asp:BoundField DataField="noofQuestions" HeaderText="Total Questions" />
                                                            <asp:BoundField DataField="courseinfo" HeaderText="Course Name" />
                                                            <asp:BoundField DataField="createdDate" HeaderText="Created Date" />
                                                            <asp:BoundField DataField="noOfSections" HeaderText="Sections" />
                                                            <asp:BoundField DataField="isAvailable" HeaderText="Available" />
                                                            <asp:BoundField DataField="negativeMarking" HeaderText="Negative Mark" />
                                                            <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                                                <ControlStyle CssClass="simplebtn" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                                                <ControlStyle CssClass="simplebtn" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                            <table width="100%">
                                <tr>
                                    <td align="left" colspan="2">
                                        <center>
                                            <div style="border: 1px dotted black;">
                                                <b>Note :</b> Please add questions sectionwise to avoid interruption while solving
                                                exams.
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="lblMsg" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Panel ID="Panel5" runat="server" Visible="false">
                                            <asp:Repeater ID="repAddedQues" runat="server">
                                                <ItemTemplate>
                                                    <div style="float: left; width: 40px">
                                                        <asp:Button ID="Button1" runat="server" Text='<%# Eval("questid") %>' CommandArgument='<%#Eval("questid") %>'
                                                            Width="35px" OnClick="btnRep1_Click" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSections" runat="server">
                                            <asp:ListItem Value="section1Ques" Enabled="false">Section 1</asp:ListItem>
                                            <asp:ListItem Value="section2Ques" Enabled="false">Section 2</asp:ListItem>
                                            <asp:ListItem Value="section3Ques" Enabled="false">Section 3</asp:ListItem>
                                            <asp:ListItem Value="section4Ques" Enabled="false">Section 4</asp:ListItem>
                                            <asp:ListItem Value="section5Ques" Enabled="false">Section 5</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;
                                        <div style="display: none">
                                            <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" />
                                            <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                                        </div>
                                        <asp:LinkButton ID="linkAddRemoveQues" CausesValidation="false" runat="server" OnClick="linkAddRemoveQues_Click">Add/Remove Questions</asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="display: none">
                                        <asp:Label ID="Label7" Font-Bold="true" runat="server" Text="Course"></asp:Label>
                                        <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                            DataValueField="courseid" AutoPostBack="True" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Label ID="Label22" Font-Bold="true" runat="server" Text="Subject"></asp:Label>
                                        <asp:DropDownList ID="ddlSubjects" runat="server" DataSourceID="sourceSubjects" DataTextField="subjectinfo"
                                            DataValueField="subid">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                        <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                            SelectCommand="SELECT Distinct courseid, courseinfo FROM view_catCourseSub where instituteId=@instituteId and courseid is not null and subid is not null ORDER BY courseinfo">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lblInstituteId" Name="instituteId" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <asp:SqlDataSource ID="sourceSubjects" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>">
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="simplebtn" OnClick="btnFinish_Click"
                                            ToolTip="Finish" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="lblNoQue" Visible="false" ForeColor="Red" runat="server" Text="No Questions Found !"></asp:Label>
                                        <asp:Repeater ID="repQueNos" runat="server" Visible="false">
                                            <ItemTemplate>
                                                <div style="float: left; width: 40px">
                                                    <asp:Button ID="Button1" runat="server" Text='<%# Eval("questid") %>' CommandArgument='<%#Eval("questid") %>'
                                                        Width="35px" OnClick="btnRep_Click" />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:Label ID="lblQuestionNumber" Font-Bold="true" Visible="false" runat="server"
                                            Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="border: solid 1px black">
                                    <td>
                                        <div style="border: solid 2px black" runat="server" id="queHolder" visible="false">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <div style="max-width: 500px; position: relative">
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <asp:Panel ID="Panel3" runat="server" Visible="false" Height="400" ScrollBars="Auto">
                                                                <asp:Repeater ID="repQueInfo" runat="server">
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("Info") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </asp:Panel>
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <div style="width: 99%; min-width: 500px; float: left; vertical-align: top">
                                                            <asp:Label ID="lblQueAdded" runat="server" Text="" Visible="false"></asp:Label>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Repeater ID="Repeater1" runat="server">
                                                                            <ItemTemplate>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:Panel ID="Panel1" runat="server">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left" style="width: 100px">
                                                                                                            <asp:Button ID="Button1" runat="server" Text='<%#Eval("btntext") %>' CommandArgument='<%#Eval("questid") %>'
                                                                                                                OnClick="btnAdd_Click" CssClass="simplebtn" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" style="width: 100px" align="right">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:Label ID="Label17" runat="server" Text="Correct Marks :"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <asp:Label ID="lblMarks" runat="server" Text='<%#Eval("mark") %>'></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="left">
                                                                                                                        <asp:Label ID="Label18" runat="server" Text="Incorrect Deduction :"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <asp:Label ID="lblDeduction" runat="server" Text='<%#Eval("negativeDeduction") %>'></asp:Label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" style="width: 100px" align="left">
                                                                                                            <asp:Label ID="Label10" runat="server" Text="Question" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="lblQuestion" runat="server" CssClass="myLabel" Text='<%# Eval("Question") %>'></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label11" runat="server" Text="Option 1" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="lblYourAns" runat="server" Text='<%# Eval("option1") %>' CssClass="myLabel"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label12" runat="server" Text="Option 2" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="lblCorrect" runat="server" CssClass="myLabel" Text='<%# Eval("option2") %>'></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label13" runat="server" Text="Option 3" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("option3") %>' CssClass="myLabel"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label5" runat="server" Text="Option 4" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("option4") %>' CssClass="myLabel"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <asp:Label ID="Label14" runat="server" Text="Option 5" CssClass="myLabelHead"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="left">
                                                                                                            <asp:Label ID="Label15" runat="server" Text='<%# Eval("option5") %>' CssClass="myLabel"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:Panel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblMaxQuestion" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="AddRemoveQues" Drag="true"
                CancelControlID="linkAddRemoveQuesCancel">
            </asp:ModalPopupExtender>
            <div id="AddRemoveQues" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe3" frameborder="0" src="addQuestionToSection.aspx" height="500px"
                    width="950px"></iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="AddRemoveQuesOK" value="Done" type="button" />
                    <input id="linkAddRemoveQuesCancel" value="Cancel" type="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
