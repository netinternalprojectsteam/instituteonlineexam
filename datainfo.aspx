﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="datainfo.aspx.cs" Inherits="OnlineExam.datainfo" Title="Manage Category,Course,Subject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td align="left">
                        <asp:TreeView ID="TreeView1" runat="server" OnTreeNodeExpanded="TreeView1_TreeNodeExpanded1"
                            OnSelectedNodeChanged="TreeView1_SelectedNodeChanged">
                            <ParentNodeStyle Font-Bold="True" Font-Names="Georgia" />
                            <NodeStyle Font-Names="Arial" Font-Size="Small" ForeColor="Black" HorizontalPadding="5px"
                                NodeSpacing="0px" VerticalPadding="1px" />
                        </asp:TreeView>
                    </td>
                    <td align="left">
                        <asp:Panel ID="Panel1" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnCourse" Visible="false" runat="server" Text="Add new Course" OnClick="btnCourse_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlCourse" runat="server" Visible="false">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtCourse" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSaveCourse" runat="server" Text="Save" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCancelCourse" runat="server" Text="Cancel" OnClick="btnCancelCourse_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSubject" runat="server" Text="Add New Subject" OnClick="btnSubject_Click"
                                            Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gridsub" runat="server" CssClass="gridview">
                                        </asp:GridView>
                                        <asp:Panel ID="pnlSubject" runat="server" Visible="false">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtSub" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSaveSub" runat="server" Text="Save" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCancelSub" runat="server" Text="Cancel" OnClick="btnCancelSub_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
