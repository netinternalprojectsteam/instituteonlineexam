﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class userDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            classes.DataLogic ob = new OnlineExam.classes.DataLogic();
            try
            {
                if (!IsPostBack)
                {
                    string ID = Session["DetailsID"].ToString();
                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT uid, uname, uemail, password, mobileNo, imageName, edate, extraEmail, address1, address2, city, state, country, aboutU FROM view_userDetails WHERE (uid = " + ID + ") ");
                    lblName.Text = dt.Rows[0]["uname"].ToString();
                    lblEmail.Text = "Email : " + dt.Rows[0]["uemail"].ToString();
                    if (dt.Rows[0]["extraEmail"].ToString().Length > 0)
                    {

                        lblExtraEmail.Text = "Extra Email : " + dt.Rows[0]["extraEmail"].ToString();
                    }
                    else { lblExtraEmail.Text = "Extra Email :  -"; }
                    if (dt.Rows[0]["mobileNo"].ToString().Length > 0)
                    {
                        lblMobile.Text = "Mobile No : " + dt.Rows[0]["mobileNo"].ToString();
                    }
                    else
                    {
                        lblMobile.Text = "Mobile No :  -";
                    }
                    if (dt.Rows[0]["imageName"].ToString().Length > 0)
                    {
                        Image1.ImageUrl = "/stdImages/" + dt.Rows[0]["imageName"].ToString();
                    }
                    else { Image1.ImageUrl = "/stdImages/default.jpg"; }
                    string add = "";
                    if (dt.Rows[0]["address1"].ToString().Length > 0)
                    {
                        add = dt.Rows[0]["address1"].ToString();
                    }
                    if (dt.Rows[0]["address2"].ToString().Length > 0)
                    {
                        if (add.Length > 0)
                        {
                            add = add + "," + dt.Rows[0]["address2"].ToString();
                        }
                        else
                        {
                            add = dt.Rows[0]["address2"].ToString();
                        }
                    }
                    if (add.Length > 0)
                    {
                        add = add + "<br>" + dt.Rows[0]["city"].ToString();
                    }
                    else { add = dt.Rows[0]["city"].ToString(); }
                    lblAdd.Text = add + "<br>" + dt.Rows[0]["state"].ToString() + "," + dt.Rows[0]["country"].ToString();
                    lblExtraInfo.Text = dt.Rows[0]["aboutU"].ToString();
                    lblHeading.Text = lblName.Text + "'S Details";
                }
            }
            catch { }
        }
    }
}
