﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="siteUsers.aspx.cs" Inherits="OnlineExam.site.siteUsers" Title="Registered Users" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/popup.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript"> 
        function ConfirmOnDelete()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:SqlDataSource ID="sourceUser" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT top(20) uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where instituteId=@instituteId ORDER BY uid DESC">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            Registered Users</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc2:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td align="left">
                                    Search site users registered :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    between :&nbsp;
                                    <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp; and :&nbsp;
                                    <asp:TextBox ID="txtToDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp;
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnSearch_Click" />&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        &nbsp;<asp:Button ID="btnToExcel" runat="server" Text="Export to Excel" OnClick="btnToExcel_Click"
                            CssClass="simplebtn" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server">
                            <asp:GridView ID="gridUsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="uid" DataSourceID="sourceUser" CssClass="gridview" PageSize="25"
                                EmptyDataText="<b>No Details Found !</b>" OnRowCommand="gridUsers_RowCommand"
                                OnRowCreated="gridUsers_RowCreated" OnPageIndexChanging="gridUsers_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField DataField="uid" HeaderText="uid" SortExpression="uid" InsertVisible="False"
                                        ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                    <asp:BoundField DataField="uemail" HeaderText="Email" SortExpression="uemail" />
                                    <asp:BoundField DataField="mobileNo" HeaderText="Mobile No" SortExpression="mobileNo" />
                                    <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                                    <asp:BoundField DataField="edate" HeaderText="Reg Date" ReadOnly="True" SortExpression="edate" />
                                    <asp:ButtonField ButtonType="Button" CommandName="INFO" HeaderText="Info" Text="Info">
                                        <ControlStyle CssClass="simplebtn" />
                                    </asp:ButtonField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:Button ID="gridButton1" runat="server" Text="Delete" OnClick="Button2_Click"
                                                CssClass="simplebtn" OnClientClick="javascript:return ConfirmOnDelete()" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="uid"
                                DataSourceID="sourceUser" CssClass="gridview">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                    <asp:BoundField DataField="uemail" HeaderText="Email" SortExpression="uemail" />
                                    <asp:BoundField DataField="mobileNo" HeaderText="Mobile No" SortExpression="mobileNo" />
                                    <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                                    <asp:BoundField DataField="edate" HeaderText="Reg Date" ReadOnly="True" SortExpression="edate" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate">
            </asp:CalendarExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
            </asp:CalendarExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFromDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtToDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFromDate"
                WatermarkText="From Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtToDate"
                WatermarkText="To Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
            </div>
            <br />
            <asp:Panel ID="Panel2" runat="server">
                <center>
                    <div class="popup_Container">
                        <div class="popup_Titlebar" id="Div1">
                            <div class="TitlebarLeft">
                                User Info
                            </div>
                            <div class="TitlebarRight" onclick="cancel();">
                            </div>
                        </div>
                        <asp:Panel ID="Panel5" runat="server">
                            <center>
                                <table>
                                    <tr>
                                        <td rowspan="4" align="left">
                                            <asp:Image ID="Image1" runat="server" Width="80px" Height="90px" />
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblInstitute" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblMobile" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="padding: 5px">
                                            <asp:Label ID="lblAdd" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Button ID="btn32" runat="server" Text="Close" CssClass="simplebtn" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </div>
                </center>
            </asp:Panel>
            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="LinkButton2"
                BackgroundCssClass="ModalPopupBG" PopupControlID="Panel2" Drag="true" CancelControlID="btn32">
            </asp:ModalPopupExtender>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
