﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="msgSetting.aspx.cs" Inherits="OnlineExam.site.msgSetting" Title="Set Email Messages" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /*css for table*/table.hovertable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
            width: 100%;
        }
        table.hovertable th
        {
            background-color: #c3dde0;
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
        }
        table.hovertable tr
        {
            background-color: #d4e3e5;
        }
        table.hovertable td
        {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Email Messages</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp; &nbsp; &nbsp; &nbsp;Email For &nbsp; &nbsp;<asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="ddlCategory_SelectedIndexChanged">
                            <asp:ListItem>Sign up for Site</asp:ListItem>
                            <asp:ListItem>Sign up for Online Exam</asp:ListItem>
                            <asp:ListItem>Sign up for Offline Exam</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" style="width: 600px">
                                        <cc1:Editor ID="editorMsg" runat="server" />
                                    </td>
                                    <td align="left" valign="top">
                                        <table class="hovertable">
                                            <tr>
                                                <th colspan="2" align="left">
                                                    You can use variable as
                                                </th>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    #Name#
                                                </td>
                                                <td align="left">
                                                    to mention Name
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    #Email#
                                                </td>
                                                <td align="left">
                                                    to mention Email
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    #Password#
                                                </td>
                                                <td align="left">
                                                    to mention Password
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                        <asp:Button ID="btnCancle" runat="server" Text="Cancel" CausesValidation="false"
                                            OnClick="btnCancle_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblID" runat="server" Text="Label" Visible="false"></asp:Label>
                        <asp:Repeater ID="rptDetails" runat="server" DataSourceID="sourceDetails">
                            <ItemTemplate>
                                <div style="border: solid 2px black; width: 80%; padding: 10px">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("emailMsg") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                    OnClick="LinkButton2_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                    color: Blue">Edit</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT [id], [emailMsg], [category] FROM [site_emailMessages]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
