﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="center.aspx.cs" Inherits="OnlineExam.site.center" Title="Test Centers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Test Centers</h3>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnNew_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" Visible="false" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <center>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Institute Name"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtInstitute" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtInstitute"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtMail" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtMail"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid ID!"
                                                ControlToValidate="txtMail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Address"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAdd" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtAdd"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="City"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtCity"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label5" runat="server" Text="Contact No"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtContact"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtContact"
                                                FilterMode="ValidChars" ValidChars="0123456789,">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label6" runat="server" Text="Is Active"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkIsActive" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSave" CssClass="simplebtn" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" CausesValidation="false"
                                                OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:GridView ID="gridCenter" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                DataSourceID="sourceCenters" CssClass="gridview" 
                                OnRowCommand="gridCenter_RowCommand" onrowcreated="gridCenter_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" Width="25px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                        SortExpression="id" />
                                    <asp:BoundField DataField="centerName" HeaderText="Institute Name" SortExpression="centerName" />
                                    <asp:BoundField DataField="address" HeaderText="Address" SortExpression="address" />
                                    <asp:BoundField DataField="contactNo" HeaderText="Contact No(s)" SortExpression="contactNo" />
                                    <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                                    <asp:BoundField DataField="mail" HeaderText="Email" SortExpression="mail" />
                                    <asp:CheckBoxField DataField="isActive" HeaderText="Is Active" 
                                        SortExpression="isActive" >
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CheckBoxField>
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                        Text="Button">
                                        <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                                        Text="Button">
                                        <ItemStyle Height="20px" Width="25px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceCenters" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id], [centerName], [address], [contactNo], [city], [mail], [isActive] FROM [site_centers]">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
