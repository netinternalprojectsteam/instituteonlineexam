﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;

namespace OnlineExam.site
{
    public partial class pollQue : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = true;
            Panel2.Visible = false;
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            txtDate.Text = "";
            txtOpt1.Text = "";
            txtOpt2.Text = "";
            txtOpt3.Text = "";
            txtOpt4.Text = "";
            txtQue.Text = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;

            int result = DateTime.Compare(Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), DateTime.Now.Date);
            if (result != 0 && result < 0)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Question date already passed !");
            }
            if (result == 0 || result > 0)
            {
                try
                {
                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT  id, question, option1, option2, option3, option4, answer, queDate FROM  site_pollingQue where queDate = '" + Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "'");
                    //q = "SELECT  id, question, option1, option2, option3, option4, answer, queDate FROM  site_pollingQue where queDate = '" + Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "'";
                    if (dt.Rows.Count > 0)
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Question for selected date is already saved !");
                    }
                    else
                    {
                        ob.insertPollingQue(txtQue.Text, txtOpt1.Text, txtOpt2.Text, txtOpt3.Text, txtOpt4.Text, ddlAns.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")));
                        Messages11.Visible = true;
                        Messages11.setMessage(1, "Data saved successfully !");
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        gridQue.DataBind();
                        btnNew.Visible = true;
                    }
                }
                catch (Exception ee)
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, ee.Message);
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ob.updatePollingQue(txtQue.Text, txtOpt1.Text, txtOpt2.Text, txtOpt3.Text, txtOpt4.Text, ddlAns.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), lblID.Text);
            Messages11.Visible = true;
            Messages11.setMessage(1, "Data updated succesfully !");
            Panel1.Visible = false;
            Panel2.Visible = true;
            gridQue.DataBind();
            btnNew.Visible = true;
        }

        protected void gridQue_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridQue.Rows[row].Cells[0].Text;
                DataTable dt = ob.getData("SELECT id, question, option1, option2, option3, option4, answer AS asnwerOption, CONVERT (varchar(10), queDate, 103) AS qDate FROM site_pollingQue where id=" + lblID.Text + "");

                int result = DateTime.Compare(Convert.ToDateTime(dt.Rows[0]["qDate"].ToString(), CultureInfo.GetCultureInfo("hi-IN")), DateTime.Now.Date);

                if (result == 0)
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Today's question cannot be edited !");
                }
                if (result > 0)
                {
                    Messages11.Visible = false;
                    txtQue.Text = dt.Rows[0]["question"].ToString();
                    txtOpt1.Text = dt.Rows[0]["option1"].ToString();
                    txtOpt2.Text = dt.Rows[0]["option2"].ToString();
                    txtOpt3.Text = dt.Rows[0]["option3"].ToString();
                    txtOpt4.Text = dt.Rows[0]["option4"].ToString();
                    ddlAns.Text = dt.Rows[0]["asnwerOption"].ToString();
                    txtDate.Text = dt.Rows[0]["qDate"].ToString();
                    Panel1.Visible = true;
                    Panel2.Visible = true;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnCancel.Visible = true;
                    btnNew.Visible = false;
                }
                if (result != 0 && result < 0)
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Question date already passed !");
                }
            }
            if (e.CommandName == "remove")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridQue.Rows[row].Cells[0].Text;
                ob.deletePollingQue(lblID.Text);
                gridQue.DataBind();
                Messages11.Visible = true;
                Messages11.setMessage(1, "Deleted successfully !");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
            Messages11.Visible = false;
        }

        protected void gridQue_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;

            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;

            }
        }
    }
}
