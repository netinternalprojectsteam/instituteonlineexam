﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class center : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = true;
            Panel2.Visible = false;
            txtAdd.Text = "";
            txtCity.Text = "";
            txtContact.Text = "";
            txtInstitute.Text = "";
            txtMail.Text = "";
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnNew.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = true;
            ob.insertCenter(txtInstitute.Text, txtAdd.Text, txtContact.Text, txtCity.Text, txtMail.Text, chkIsActive.Checked);
            Messages11.Visible = true; Messages11.setMessage(1, "Data saved successfully !");
            Panel1.Visible = false;
            Panel2.Visible = true;
            gridCenter.DataBind();
            btnNew.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ob.updateCenter(txtInstitute.Text, txtAdd.Text, txtContact.Text, txtCity.Text, txtMail.Text, chkIsActive.Checked, lblID.Text);
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
            Messages11.Visible = true;
            Messages11.setMessage(1, "Data updated Successfully !");
            gridCenter.DataBind();
        }

        protected void gridCenter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int row = 0;
                if (e.CommandName == "change")
                {
                    Messages11.Visible = false;
                    row = Convert.ToInt16(e.CommandArgument);
                    lblID.Text = gridCenter.Rows[row].Cells[1].Text;
                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT [id], [centerName], [address], [contactNo], [city], [mail], [isActive] FROM [site_centers] where [id] = " + lblID.Text + "");
                    txtAdd.Text = dt.Rows[0]["address"].ToString();
                    txtCity.Text = dt.Rows[0]["city"].ToString();
                    txtContact.Text = dt.Rows[0]["contactNo"].ToString();
                    txtInstitute.Text = dt.Rows[0]["centerName"].ToString();
                    txtMail.Text = dt.Rows[0]["mail"].ToString();
                    chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["isActive"].ToString());
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                    btnNew.Visible = false;
                    btnUpdate.Visible = true;
                    btnSave.Visible = false;
                    btnCancel.Visible = true;
                }
                if (e.CommandName == "remove")
                {
                    row = Convert.ToInt16(e.CommandArgument);
                    ob.deleteCenter(gridCenter.Rows[row].Cells[1].Text);
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Data deleted Successfully !");
                    gridCenter.DataBind();
                }
            }
            catch { }
        }

        protected void gridCenter_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }


    }
}
