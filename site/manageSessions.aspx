﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="manageSessions.aspx.cs" Inherits="OnlineExam.site.manageSessions"
    Title="Manage Sessions" %>

<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Sessions</h3>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                        <asp:Label ID="lblID" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" Visible="false" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" align="center">
                        <asp:Label ID="Label1" runat="server" Text="Session Name" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtSessions" Width="200px" runat="server"></asp:TextBox>&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtSessions"></asp:RequiredFieldValidator>&nbsp;
                        <asp:Button ID="btnSave" CssClass="simplebtn" runat="server" Text="Save" OnClick="btnSave_Click" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" Visible="false"
                            CausesValidation="false" OnClick="btnCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:SqlDataSource ID="sourceSessions" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT [id], [sName], [isActive] FROM [site_Sessions] WHERE sName <> '-Select-'"></asp:SqlDataSource>
                        <asp:GridView ID="gridSessions" runat="server" AutoGenerateColumns="False" DataSourceID="sourceSessions"
                            CssClass="gridview" OnRowCommand="gridSessions_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr. NO.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                                <asp:BoundField DataField="sName" HeaderText="sName" SortExpression="sName" />
                                <asp:CheckBoxField DataField="isActive" HeaderText="isActive" SortExpression="isActive">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:CheckBoxField>
                                <asp:ButtonField ButtonType="Image" CommandName="setSession" HeaderText="Set" ImageUrl="~/images/apply2.png"
                                    Text="Button">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                    Text="Button">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
