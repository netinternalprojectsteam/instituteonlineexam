﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.Globalization;
using System.IO;

namespace OnlineExam.site
{
    public partial class siteUsers : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                gridUsers.DataBind();
                GridView1.DataBind();
                if (gridUsers.Rows.Count > 0)
                {
                    btnToExcel.Visible = true;
                }
                else { btnToExcel.Visible = false; }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceUser.SelectCommand = "SELECT uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where ((CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "') and (instituteId=" + lblInsId.Text + ") ORDER BY edate DESC";
                gridUsers.DataBind();
                GridView1.DataBind();
                if (gridUsers.Rows.Count > 0)
                {
                    btnToExcel.Visible = true;
                }
                else { btnToExcel.Visible = false; }
            }
            catch (Exception ee)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter proper Dates");
                Messages11.setMessage(0, ee.Message);
            }
        }

        protected void gridUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            int row = 0;
            if (e.CommandName == "INFO")
            {
                Messages11.Visible = false;
                row = Convert.ToInt32(e.CommandArgument);
                string ID = gridUsers.Rows[row].Cells[0].Text;
                DataTable dt = new DataTable();
                dt = ob.GetUserInfoByID(ID);
                lblName.Text = dt.Rows[0]["uname"].ToString();
                lblInstitute.Text = dt.Rows[0]["institute"].ToString();
                lblEmail.Text = dt.Rows[0]["uemail"].ToString();
                lblMobile.Text = dt.Rows[0]["mobileno"].ToString();

                if (dt.Rows[0]["imageName"].ToString().Length > 0)
                {
                    Image1.ImageUrl = "/stdImages/" + dt.Rows[0]["imageName"].ToString() + "";

                }
                string details = "";
                if (dt.Rows[0]["Add1"].ToString().Length > 0)
                {
                    details = dt.Rows[0]["Add1"].ToString() + ",<br>";
                }

                if (dt.Rows[0]["Add2"].ToString().Length > 0)
                {
                    details = dt.Rows[0]["Add2"].ToString() + ",<br>";
                }

                if (dt.Rows[0]["city"].ToString().Length > 0)
                {
                    if (details.Length > 0)
                    { details = details + dt.Rows[0]["city"].ToString() + ","; }
                    else { details = dt.Rows[0]["city"].ToString() + ","; }
                }

                details = details + "<br>" + dt.Rows[0]["state"].ToString();
                details = details + "<br>" + dt.Rows[0]["country"].ToString();
                lblAdd.Text = details;
                ModalPopupExtender1.Show();
            }
        }

        protected void gridUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
            }
        }

        protected void gridUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Messages11.Visible = false;
            sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            try
            {
                sourceUser.SelectCommand = "SELECT uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where ((CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "') and (instituteId=" + lblInsId.Text + ") ORDER BY uid DESC";
            }
            catch { sourceUser.SelectCommand = "SELECT Top 20 uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where instituteId=" + lblInsId.Text + " ORDER BY uid DESC"; }
            gridUsers.DataBind();
            gridUsers.PageIndex = e.NewPageIndex;

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Button btn = (Button)sender;
            GridViewRow gvrow = (GridViewRow)btn.NamingContainer;
            string id = gridUsers.DataKeys[gvrow.RowIndex].Value.ToString();
            if (btn.Text == "Delete")
            {
                ob.deleteUser(id);
                Messages11.Visible = true;
                Messages11.setMessage(1, "User Details Deleted Successfully !");
                try
                {
                    if (txtFromDate.Text.Length > 0 && txtToDate.Text.Length > 0)
                    {

                        sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        sourceUser.SelectCommand = "SELECT uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where ((CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "') AND (instituteId=" + lblInsId.Text + ") ORDER BY uid DESC";
                        gridUsers.DataBind();
                    }
                    else
                    {
                        sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        sourceUser.SelectCommand = "SELECT TOP(15) uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate FROM exam_users where instituteId=" + lblInsId.Text + " ORDER BY uid DESC";
                        gridUsers.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void btnToExcel_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            string dtime = DateTime.Now.ToString("ddMMyyyy_HHmmss");
            Response.AddHeader("content-disposition", "attachment;filename=regUsers_" + dtime + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            GridView1.AllowPaging = false;


            GridView1.RenderControl(hw); GridView1.DataBind();
            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }
        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }
    }
}
