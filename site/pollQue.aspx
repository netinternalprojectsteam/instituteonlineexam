﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="pollQue.aspx.cs" Inherits="OnlineExam.site.pollQue" Title="Polling Que" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Polling Question</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnNew_Click" />
                        <asp:Label ID="lblID" runat="server" Visible="false" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <center>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label6" runat="server" Text="For Date" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDate"
                                                FilterMode="ValidChars" ValidChars="0123456789./-">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Question" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtQue" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtQue"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="Option 1" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtOpt1" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Option 2" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtOpt2" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="Option 3" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtOpt3" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label5" runat="server" Text="Option 4" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtOpt4" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label7" runat="server" Text="Answer" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlAns" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" CausesValidation="false" runat="server" Text="Cancel"
                                                CssClass="simplebtn" OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:GridView ID="gridQue" runat="server" CssClass="gridview" DataSourceID="sourceQue"
                                PageSize="20" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id"
                                OnRowCommand="gridQue_RowCommand" OnRowCreated="gridQue_RowCreated">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
                                    <asp:BoundField DataField="question" HeaderText="Question" SortExpression="question" />
                                    <asp:BoundField DataField="option1" HeaderText="Option1" SortExpression="option1" />
                                    <asp:BoundField DataField="option2" HeaderText="Option2" SortExpression="option2" />
                                    <asp:BoundField DataField="option3" HeaderText="Option3" SortExpression="option3" />
                                    <asp:BoundField DataField="option4" HeaderText="Option4" SortExpression="option4" />
                                    <asp:BoundField DataField="asnwerOption" HeaderText="Answer Option" SortExpression="asnwerOption" />
                                    <asp:BoundField DataField="qDate" HeaderText="For Date" ReadOnly="True" SortExpression="qDate" />
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                        Text="Button" />
                                    <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Edit" ImageUrl="~/images/cross.png"
                                        Text="Button" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceQue" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT id, question, option1, option2, option3, option4, answer AS asnwerOption, CONVERT (varchar(10), queDate, 103) AS qDate FROM site_pollingQue order by queDate desc">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
