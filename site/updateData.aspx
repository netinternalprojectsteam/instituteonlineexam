﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="updateData.aspx.cs" Inherits="OnlineExam.site.updateData" Title="Latest Data" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/tabContainer.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Update Data</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblID" runat="server" Text="Label" Visible="false"></asp:Label>
                        <uc1:Messages1 ID="Messages12" runat="server" Visible="false" />
                    </td>
                </tr>
            </table>
            <%--<a href='download.aspx?file=" + flname[i] + "&path=/Solutions/" + company + "/'>--%>
            <%--<a href='/site/downloads.aspx?file=4.jpg'>image</a>--%>
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme">
                <asp:TabPanel runat="server" ID="tab1">
                    <HeaderTemplate>
                        <b>About Us</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <center>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblIDAbout" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Panel ID="Panel1" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <h3>
                                                        About US</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Panel ID="Panel2" runat="server" Visible="False">
                                                        <center>
                                                            <table>
                                                                <tr>
                                                                    <td align="left">
                                                                        <cc1:Editor ID="editorAbout" runat="server" CssClass="" Width="600px" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Button ID="btnUpdateAbout" runat="server" Text="Update" OnClick="btnUpdateAbout_Click"
                                                                            CssClass="simplebtn" />
                                                                        <asp:Button ID="btnCancel1" CausesValidation="False" runat="server" Text="Cancel"
                                                                            CssClass="simplebtn" OnClick="btnCancel1_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </center>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <%--<asp:GridView ID="gridAbout" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                                        DataSourceID="sourceAboutUs" CssClass="gridview" OnRowCommand="gridAbout_RowCommand"
                                                        OnRowCreated="gridAbout_RowCreated">
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                                                SortExpression="id" />
                                                            <asp:BoundField DataField="aboutUs" HeaderText="About Us" SortExpression="aboutUs" />
                                                            <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/editInfo.png"
                                                                Text="Button">
                                                                <ItemStyle Height="20px" Width="20px" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                    </asp:GridView>--%>
                                                    <asp:Repeater ID="rptAbout" runat="server" DataSourceID="sourceAboutUs">
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("aboutUs") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                            OnClick="LinkButton2_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                            color: Blue">Edit</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <asp:SqlDataSource ID="sourceAboutUs" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                        SelectCommand="SELECT [id], [aboutUs] FROM [site_aboutUs]"></asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </center>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1">
                    <HeaderTemplate>
                        <b>Why HMT</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:Panel ID="Panel3" runat="server">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <h3>
                                            Why HMT
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lblIDWhyHMT" runat="server" Text="Label" Visible="false"></asp:Label>
                                                <asp:Panel ID="Panel4" runat="server" Visible="false">
                                                    <center>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <cc1:Editor ID="editorWhyHMT" runat="server" CssClass="" Width="600px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpdateWhyHMT" runat="server" Text="Update" CssClass="simplebtn"
                                                                        OnClick="btnUpdateWhyHMT_Click" />
                                                                    <asp:Button ID="btnCancel2" CausesValidation="false" runat="server" Text="Cancel"
                                                                        CssClass="simplebtn" OnClick="btnCancel2_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <%--<asp:GridView ID="gridWhyHMT" CssClass="gridview" runat="server" AutoGenerateColumns="False"
                                            DataKeyNames="id" DataSourceID="source" OnRowCommand="gridWhyHMT_RowCommand"
                                            OnRowCreated="gridWhyHMT_RowCreated">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                                    SortExpression="id" />
                                                <asp:BoundField DataField="whyHmt" HeaderText="Why HMT" SortExpression="whyHmt" />
                                                <asp:ButtonField ButtonType="Image" CommandName="change" ImageUrl="~/images/editInfo.png"
                                                    Text="Button">
                                                    <ItemStyle Height="20px" Width="20px" />
                                                </asp:ButtonField>
                                            </Columns>
                                        </asp:GridView>--%>
                                        <asp:Repeater ID="rptWhyHMT" runat="server" DataSourceID="source">
                                            <ItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("whyHmt") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                OnClick="LinkButton3_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                color: Blue">Edit</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:SqlDataSource ID="source" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                            SelectCommand="SELECT [id], [whyHmt] FROM [site_whyHMT]"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel2">
                    <HeaderTemplate>
                        <b>Scrolling New</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblIDNews" runat="server" Text="Label" Visible="false"></asp:Label>
                                <asp:Panel ID="Panel5" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <h3>
                                                    Scrolling Data</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Panel ID="Panel6" runat="server" Visible="false">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <cc1:Editor ID="editorNews" runat="server" CssClass="" Width="600px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpdateNews" runat="server" Text="Update" OnClick="btnUpdateNews_Click"
                                                                        CssClass="simplebtn" />
                                                                    <asp:Button ID="btnCancel3" CausesValidation="false" runat="server" Text="Cancel"
                                                                        CssClass="simplebtn" OnClick="btnCancel3_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <%--<asp:GridView ID="gridNews" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                                    DataKeyNames="id" DataSourceID="sourceNews" OnRowCommand="gridNews_RowCommand"
                                                    OnRowCreated="gridNews_RowCreated">
                                                    <Columns>
                                                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                                            SortExpression="id" />
                                                        <asp:BoundField DataField="news" HeaderText="Scrolling News" SortExpression="news" />
                                                        <asp:ButtonField ButtonType="Image" CommandName="change" ImageUrl="~/images/editInfo.png"
                                                            Text="Button">
                                                            <ItemStyle Height="20px" Width="20px" />
                                                        </asp:ButtonField>
                                                    </Columns>
                                                </asp:GridView>--%>
                                                <asp:Repeater ID="rptNews" runat="server" DataSourceID="sourceNews">
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("news") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                        OnClick="LinkButton4_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                        color: Blue">Edit</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="sourceNews" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                    SelectCommand="SELECT [id], [news] FROM [site_scrollingNew]"></asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel3">
                    <HeaderTemplate>
                        <b>Upload Documents</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="Panel7" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <h3>
                                                    Upload Documents</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Panel ID="Panel8" runat="server">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <asp:Label ID="lblMsg" runat="server" Text="Label" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Label ID="Label1" runat="server" Text="Select file" Font-Bold="true"></asp:Label>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click"
                                                                        CssClass="simplebtn" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:GridView ID="gridFiles" runat="server" CssClass="gridview" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="File" HeaderText="Document Name" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUpload" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel4">
                    <HeaderTemplate>
                        <b>Latest News</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblIDLatestNews" runat="server" Text="Label" Visible="false"></asp:Label>
                                <asp:Panel ID="Panel9" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <h3>
                                                    Latest News</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Panel ID="Panel10" runat="server" Visible="false">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <cc1:Editor ID="editorLatest" runat="server" CssClass="" Width="600px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpdateLatestNews" runat="server" Text="Update" OnClick="btnUpdateLatestNews_Click"
                                                                        CssClass="simplebtn" />
                                                                    <asp:Button ID="btnCancelLatestNew" CausesValidation="false" runat="server" Text="Cancel"
                                                                        CssClass="simplebtn" OnClick="btnCancel4_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Repeater ID="rptLatest" runat="server" DataSourceID="sourceLatestNews">
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("lNews") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                        OnClick="LinkButton5_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                        color: Blue">Edit</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="sourceLatestNews" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                    SelectCommand="SELECT [id], [lNews] FROM [site_latestNews]"></asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel5">
                    <HeaderTemplate>
                        <b>Online Test Instructions</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
                                <asp:Panel ID="Panel11" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <h3>
                                                    Instructions for Joining Online Test
                                                </h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Panel ID="Panel12" runat="server" Visible="false">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <cc1:Editor ID="editorIns" runat="server" CssClass="" Width="600px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpdateIns" runat="server" Text="Update" OnClick="btnUpdateIns_Click"
                                                                        CssClass="simplebtn" />
                                                                    <asp:Button ID="btnCanIns" CausesValidation="false" runat="server" Text="Cancel"
                                                                        CssClass="simplebtn" OnClick="btnCancelrptOnline_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblIDIns" runat="server" Text="Label" Visible="false"></asp:Label>
                                                <asp:Repeater ID="repIns" runat="server" DataSourceID="sourceIns">
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("instructions") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                        OnClick="LinkButton6_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                        color: Blue">Edit</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="sourceIns" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                    SelectCommand="SELECT [id], [instructions] FROM [site_instructionsOnline]"></asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel6">
                    <HeaderTemplate>
                        <b>Offline Test Instructions</b>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label>
                                <asp:Panel ID="Panel13" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <h3>
                                                    Instructions for Joining Offline Test</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Panel ID="Panel14" runat="server" Visible="false">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <cc1:Editor ID="editorInsOffline" runat="server" CssClass="" Width="600px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnUpdateInsOffline" runat="server" Text="Update" CssClass="simplebtn"
                                                                        OnClick="btnUpdateInsOffline_Click" />
                                                                    <asp:Button ID="Button2" CausesValidation="false" runat="server" Text="Cancel" CssClass="simplebtn"
                                                                        OnClick="btnCancelrptOffline_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblIDInsOffline" runat="server" Text="Label" Visible="false"></asp:Label>
                                                <asp:Repeater ID="rptInsOffline" runat="server" DataSourceID="sourceInsOffline">
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("instructions") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                                        OnClick="LinkButton7_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                                        color: Blue">Edit</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="sourceInsOffline" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                    SelectCommand="SELECT [id], [instructions] FROM [site_instructionsOffline]">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
