﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class regUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gridDetails.DataBind();
                gridDetails.Caption = "<b>Regisered for Online Test</b>";
            }
        }

        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSelect.Text == "Online")
            {
                sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceDetails.SelectCommand = "SELECT className, uname, uemail, mobileNo, city, convert(varchar(10), edate,103) as edate,' ' as examCenter FROM view_ActiveSessionOnlineUsers";
                gridDetails.DataBind();
                gridDetails.Caption = "<b>Regisered for Online Test</b>";
            }
            if (ddlSelect.Text == "Live Offline")
            {
                sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceDetails.SelectCommand = "SELECT className, uname, uemail, mobileNo, city, examCenter,convert(varchar(10), edate,103) as edate FROM view_ActiveSessionOfflineUsers";
                gridDetails.DataBind();
                gridDetails.Caption = "<b>Regisered for Live Offline Test</b>";
            }
        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (ddlSelect.Text == "Online")
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[6].Visible = false;
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[6].Visible = false;
                }
            }   
        }
    }
}
