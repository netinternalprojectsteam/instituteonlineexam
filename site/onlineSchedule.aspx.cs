﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class onlineSchedule : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sourceSeries.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceSeries.SelectCommand = "SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where category = '" + ddlClasses.Text + "'";
                gridSeries.DataBind();
            }

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtSub.Text = "";
            txtTestNo.Text = "";
            txtTopic.Text = "";
            Messages11.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ob.insertOnlineSchedule(txtSub.Text, txtTestNo.Text, Convert.ToDateTime(txtDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN")), txtTopic.Text, ddlClasses.Text);
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
            sourceSeries.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceSeries.SelectCommand = "SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where category = '" + ddlClasses.Text + "'";
            gridSeries.DataBind();
            Messages11.Visible = true;
            Messages11.setMessage(1, "Data saved successfully !");
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ob.updateOnlineSchedule(txtSub.Text, txtTestNo.Text, Convert.ToDateTime(txtDate.Text, System.Globalization.CultureInfo.GetCultureInfo("hi-IN")), txtTopic.Text, lblID.Text, ddlClasses.Text);
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
            Messages11.Visible = true;
            Messages11.setMessage(1, "Data saved successfully !");
            sourceSeries.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceSeries.SelectCommand = "SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where category = '" + ddlClasses.Text + "'";
            gridSeries.DataBind();

        }

        protected void gridSeries_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridSeries.Rows[row].Cells[1].Text;
                DataTable dt = new DataTable();
                dt = ob.getData("SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where id= " + lblID.Text + "");
                txtDate.Text = dt.Rows[0]["sDate"].ToString();
                txtSub.Text = dt.Rows[0]["subject"].ToString();
                txtTestNo.Text = dt.Rows[0]["testNo"].ToString();
                txtTopic.Text = dt.Rows[0]["topics"].ToString();
                Panel1.Visible = true;
                Panel2.Visible = false;
                btnNew.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;


            }
            if (e.CommandName == "remove")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                ob.deleteOnlineSchedule(gridSeries.Rows[row].Cells[1].Text);

                Messages11.setMessage(1, "Data deleted successfully !");
                sourceSeries.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceSeries.SelectCommand = "SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where category = '" + ddlClasses.Text + "'";
                gridSeries.DataBind();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel2.Visible = true;
            Panel1.Visible = false;
            btnNew.Visible = true;
        }

        protected void gridSeries_RowCreated(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }

        protected void ddlClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            sourceSeries.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceSeries.SelectCommand = "SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline] where category = '" + ddlClasses.Text + "'";
            gridSeries.DataBind();
        }

    }
}
