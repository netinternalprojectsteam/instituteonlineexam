﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="manageTests.aspx.cs" Inherits="OnlineExam.site.manageTests" Title="Upcoming Tests" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Upcoming Tests</h3>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnNew_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <center>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Date" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                                Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars"
                                                TargetControlID="txtDate" ValidChars="0123456789./-">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Test Name" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label5" runat="server" Text="Exam Mode" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddltype" runat="server">
                                                <asp:ListItem>-SELECT-</asp:ListItem>
                                                <asp:ListItem>ONLINE</asp:ListItem>
                                                <asp:ListItem>LIVE OFFLINE</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="For Batch" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlBatch" runat="server">
                                                <asp:ListItem>-SELECT-</asp:ListItem>
                                                <asp:ListItem>FIRST YEAR</asp:ListItem>
                                                <asp:ListItem>SECOND YEAR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MAJOR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MINOR</asp:ListItem>
                                                <asp:ListItem>INTERNS</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label6" runat="server" Text="Is Active" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkIsActive" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="Topic(s)"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTopics" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtTopics"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSave" CssClass="simplebtn" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" CausesValidation="false"
                                                OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:GridView ID="gridTests" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                DataSourceID="sourceTests" CssClass="gridview" OnRowCommand="gridCenter_RowCommand"
                                OnRowCreated="gridCenter_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" Width="25px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                        SortExpression="id" />
                                    <asp:BoundField DataField="batch" HeaderText="Batch" SortExpression="bacth" />
                                    <asp:BoundField DataField="examMode" HeaderText="Offline/Online" SortExpression="examMode" />
                                    <asp:BoundField DataField="testDate" HeaderText="Date" SortExpression="testDate" />
                                    <asp:BoundField DataField="testName" HeaderText="Test Name" SortExpression="testName" />
                                    <asp:BoundField DataField="testTopics" HeaderText="Topics" SortExpression="testTopics" />
                                    <asp:CheckBoxField DataField="isActive" HeaderText="Is Active" SortExpression="isActive">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CheckBoxField>
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                        Text="Button">
                                        <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                                        Text="Button">
                                        <ItemStyle Height="20px" Width="25px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceTests" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id], [testName], [testTopics], convert(varchar(10),[testDate],103) as[testDate],[isActive],[batch],[examMode] FROM [site_tests]">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
