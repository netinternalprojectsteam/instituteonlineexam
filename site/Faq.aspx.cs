﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class Faq : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sourceFaq.SelectCommand = "Select id,questions,answer from site_FAQ where FaqType='" + ddlType.Text + "'";
                sourceFaq.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                gridFaq.DataBind();
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            txtAns.Text = "";
            txtQue.Text = "";
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            lblMsg.Visible = false; ;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ob.insertFaq(txtQue.Text, txtAns.Text, ddlType.Text);
            Panel2.Visible = false;
            btnNew.Visible = true;
            sourceFaq.SelectCommand = "Select id,questions,answer from site_FAQ where FaqType='" + ddlType.Text + "'";
            sourceFaq.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridFaq.DataBind();
            lblMsg.Text = "Data saved successfully !";
            lblMsg.Visible = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ob.updateFaq(txtQue.Text, txtAns.Text, lblID.Text);
            Panel2.Visible = false;
            btnNew.Visible = true;
            sourceFaq.SelectCommand = "Select id,questions,answer from site_FAQ where FaqType='" + ddlType.Text + "'";
            sourceFaq.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridFaq.DataBind();
            gridFaq.Visible = true;
            lblMsg.Text = "Data updated successfully !";
            lblMsg.Visible = true;
            btnNew.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            gridFaq.Visible = true;
            btnNew.Visible = true;
        }

        protected void gridFaq_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }

        protected void gridFaq_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            lblMsg.Visible = false;
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt32(e.CommandArgument);
                lblID.Text = gridFaq.Rows[row].Cells[1].Text;
                DataTable dt = new DataTable();
                dt = ob.getData("Select questions , answer,id from site_FAQ where id= " + lblID.Text + "");
                txtQue.Text = dt.Rows[0]["questions"].ToString();
                txtAns.Text = dt.Rows[0]["answer"].ToString();
                Panel2.Visible = true;
                btnNew.Visible = false;
                btnUpdate.Visible = true;
                btnSave.Visible = false;
                gridFaq.Visible = false;
            }
            if (e.CommandName == "remove")
            {
                row = Convert.ToInt32(e.CommandArgument);
                ob.deleteFaq(gridFaq.Rows[row].Cells[1].Text);
                lblMsg.Text = "Data deleted successfully !";
                lblMsg.Visible = true;
                sourceFaq.SelectCommand = "Select id,questions,answer from site_FAQ where FaqType='" + ddlType.Text + "'";
                sourceFaq.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                gridFaq.DataBind();
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceFaq.SelectCommand = "Select id,questions,answer from site_FAQ where FaqType='" + ddlType.Text + "'";
            sourceFaq.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridFaq.DataBind();
        }


    }
}
