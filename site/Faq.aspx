﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="Faq.aspx.cs" Inherits="OnlineExam.site.Faq" Title="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <h3>
                                    FAQ</h3>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                    <asp:ListItem>Online Exam</asp:ListItem>
                                    <asp:ListItem>Live Offline Exam</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                                    OnClick="btnNew_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblMsg" Visible="false" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Panel ID="Panel2" runat="server" Visible="false">
                                    <center>
                                        <table>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="Label3" runat="server" Text="Question" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtQue" runat="server" TextMode="MultiLine" Width="800" Height="60"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                        ControlToValidate="txtQue"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="Label2" runat="server" Text="Answer" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:TextBox ID="txtAns" runat="server" TextMode="MultiLine" Width="800" Height="60"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                        ControlToValidate="txtAns"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSave" CssClass="simplebtn" runat="server" Text="Save" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                                    <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" CausesValidation="false"
                                                        OnClick="btnCancel_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </center>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Panel ID="Panel3" runat="server">
                                    <asp:SqlDataSource ID="sourceFaq" runat="server"></asp:SqlDataSource>
                                    <asp:GridView ID="gridFaq" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                        DataSourceID="sourceFaq" CssClass="gridview" EmptyDataText="No results found !"
                                        OnRowCommand="gridFaq_RowCommand" OnRowCreated="gridFaq_RowCreated">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                                SortExpression="id" />
                                            <asp:BoundField DataField="questions" HeaderText="Question" SortExpression="questions" />
                                            <asp:BoundField DataField="answer" HeaderText="Answer" SortExpression="answer" />
                                            <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                                Text="Button">
                                                <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                            </asp:ButtonField>
                                            <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                                                Text="Button">
                                                <ItemStyle Height="20px" Width="25px" />
                                            </asp:ButtonField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content> 