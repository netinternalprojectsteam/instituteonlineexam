﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;

namespace OnlineExam.site
{
    public partial class manageTests : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            btnNew.Visible = true;
            txtDate.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            txtTopics.Text = "";
            txtName.Text = "";
            ddlBatch.Text = "-SELECT-";
            ddltype.Text = "-SELECT-";
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlBatch.Text != "-SELECT-" && ddltype.Text != "-SELECT-")
                {
                    Messages11.Visible = false;
                    ob.insertTests(txtName.Text, txtTopics.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), chkIsActive.Checked, ddlBatch.Text, ddltype.Text);
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    btnNew.Visible = true;
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Data saved successfully !");
                    gridTests.DataBind();
                }
                else
                {
                    string msg = "";
                    if (ddlBatch.Text == "-SELECT-")
                    {
                        msg = "Select proper batch !";
                    }
                    if (ddltype.Text == "-SELECT-")
                    {
                        msg = "Select proper Exam mode !";
                    }


                    Messages11.Visible = true;
                    Messages11.setMessage(0, msg);
                }
            }
            catch { }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                ob.updateTest(txtName.Text, txtTopics.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), chkIsActive.Checked, lblID.Text, ddlBatch.Text, ddltype.Text);
                Panel1.Visible = false;
                Panel2.Visible = true;
                btnNew.Visible = true;
                Messages11.Visible = true;
                Messages11.setMessage(1, "Data updated successfully !");
                gridTests.DataBind();
            }
            catch { }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
            Messages11.Visible = false;
            gridTests.DataBind();
        }

        protected void gridCenter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridTests.Rows[row].Cells[1].Text;
                DataTable dt = new DataTable();
                dt = ob.getData("SELECT [id], [testName], [testTopics],convert(varchar(10),[testDate],103) as[testDate],[isActive], [batch],[examMode] FROM [site_tests] where id=" + lblID.Text + "");
                txtDate.Text = dt.Rows[0]["testDate"].ToString();
                txtName.Text = dt.Rows[0]["testName"].ToString();
                txtTopics.Text = dt.Rows[0]["testTopics"].ToString();
                chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["isActive"].ToString());
                ddlBatch.Text = dt.Rows[0]["batch"].ToString();
                ddltype.Text = dt.Rows[0]["examMode"].ToString();
                Panel1.Visible = true;
                Panel2.Visible = false;
                btnNew.Visible = false;
                // gridTests.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnCancel.Visible = true;
            }
            if (e.CommandName == "remove")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridTests.Rows[0].Cells[1].Text;
                ob.deleteTest(lblID.Text);
                Messages11.Visible = true;
                Messages11.setMessage(1, "Data deleted successfully !");
                gridTests.DataBind();
            }
        }

        protected void gridCenter_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }
    }
}
