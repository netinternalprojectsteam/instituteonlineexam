﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class msgSetting : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceDetails.SelectCommand = "SELECT [id], [emailMsg], [category] FROM [site_emailMessages] WHERE [category] = '" + ddlCategory.Text + "' ";
                rptDetails.DataBind();
            }
        }



        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (editorMsg.Content.Trim().Length > 0)
            {
                if (editorMsg.Content.Contains("#Password#") && editorMsg.Content.Contains("#Email#"))
                {
                    ob.updateEmailMsg(editorMsg.Content, lblID.Text);
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Message Updated Successfully !");
                    Panel1.Visible = false;
                    sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    sourceDetails.SelectCommand = "SELECT [id], [emailMsg], [category] FROM [site_emailMessages] WHERE [category] = '" + ddlCategory.Text + "' ";
                    rptDetails.DataBind();

                    rptDetails.Visible = true;
                    ddlCategory.Enabled = true;
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Password & Email both should be mentioned in message !");
                }
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter proper message !");
            }
        }

        protected void btnCancle_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = false;

            rptDetails.Visible = true;
        }


        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblID.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptDetails.Items[0].FindControl("Label2");
            editorMsg.Content = lbl.Text;

            Panel1.Visible = true;
            rptDetails.Visible = false;
            ddlCategory.Enabled = false;
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceDetails.SelectCommand = "SELECT [id], [emailMsg], [category] FROM [site_emailMessages] WHERE [category] = '" + ddlCategory.Text + "' ";
            rptDetails.DataBind();
        }
    }
}
