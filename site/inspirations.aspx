﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="inspirations.aspx.cs" Inherits="OnlineExam.site.inspirations" Title="Inspirations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="lblFileName" Visible="false" runat="server" Text="Label"></asp:Label>
    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    Our Inspirations
                </h3>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="btnNew" CausesValidation="false" runat="server" Text="Add New" OnClick="btnNew_Click" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel1" runat="server" Visible="false">
                    <table>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label1" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtName" runat="server" Width="300px"></asp:TextBox>
                            </td>
                            <td rowspan="4" align="left" valign="top">
                                <asp:Image ID="img" Width="50px" Height="50px" runat="server" />
                            </td>
                        </tr>
                         <tr>
                            <td align="left">
                                <asp:Label ID="Label5" runat="server" Text="Degree" Font-Bold="true"></asp:Label>
                            </td>
                            <td align="left">
                                  <asp:TextBox ID="txtDegree" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label3" runat="server" Text="Image" Font-Bold="true"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="FileUpload1" runat="server" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label4" runat="server" Text="IsActive" Font-Bold="true"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <asp:Label ID="Label2" runat="server" Text="Message" Font-Bold="true" ></asp:Label>
                            </td>
                            <td align="left" colspan="2">
                                <cc1:Editor ID="Editor1" runat="server" Width="600px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" colspan="2">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCan" runat="server" Text="Cancel" CausesValidation="false" CssClass="simplebtn"
                                    OnClick="btnCan_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gridIns" CssClass="gridview" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="Iname" DataSourceID="sourceIns" 
                    OnRowCommand="gridIns_RowCommand" onrowcreated="gridIns_RowCreated">
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                            SortExpression="ID" />
                        <asp:TemplateField HeaderText="Sr.No.">
                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Iname" HeaderText="Name" ReadOnly="True" SortExpression="Iname" />
                        <asp:BoundField DataField="degree" HeaderText="Degree" SortExpression="degree" />
                        <asp:ImageField DataImageUrlField="ImageName" HeaderText="Image Preview">
                            <ControlStyle Height="100px" Width="100px" />
                        </asp:ImageField>
                        <asp:BoundField DataField="message" HeaderText="message" SortExpression="message" />
                        <asp:CheckBoxField DataField="isActive" HeaderText="isActive" SortExpression="isActive" />
                        <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                            Text="Button">
                            <ItemStyle Height="20px" Width="25px" />
                        </asp:ButtonField>
                         <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                            Text="Button">
                            <ItemStyle Height="20px" Width="25px" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sourceIns" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                    SelectCommand="SELECT id, Iname, '/inspirations/' + ImageName AS ImageName, message, isActive,degree FROM site_inspirations">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
