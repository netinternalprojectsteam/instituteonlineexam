﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class examDetails : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt = ob.getData("Select Edetails,id from site_ExamDetails where Etype='" + ddlType.Text + "'");
                rptDetails.DataSource = dt;
                rptDetails.DataBind();
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ob.getData("Select Edetails,id from site_ExamDetails where Etype='" + ddlType.Text + "'");
            rptDetails.DataSource = dt;
            rptDetails.DataBind();
            editorDetails.Content = dt.Rows[0]["Edetails"].ToString();
            lblID.Text = dt.Rows[0]["id"].ToString();
        }

        protected void linkBtnEdit_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblID.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptDetails.Items[0].FindControl("Label2");
            editorDetails.Content = lbl.Text;

            Panel1.Visible = true;

            rptDetails.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (editorDetails.Content.Length > 0)
            {
                ob.updateDetails(editorDetails.Content, lblID.Text);
                Panel1.Visible = false;


                DataTable dt = new DataTable();
                dt = ob.getData("Select Edetails,id from site_ExamDetails where Etype='" + ddlType.Text + "'");
                rptDetails.DataSource = dt;
                rptDetails.DataBind();
                rptDetails.Visible = true;
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false; rptDetails.Visible = true;
        }
    }
}
