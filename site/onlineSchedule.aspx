﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="onlineSchedule.aspx.cs" Inherits="OnlineExam.site.onlineSchedule"
    Title="Online Test Schedule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Online Test Schedule</h3>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="style1">
                        <asp:DropDownList ID="ddlClasses" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="ddlClasses_SelectedIndexChanged">
                            <asp:ListItem>FIRST YEAR</asp:ListItem>
                            <asp:ListItem>SECOND YEAR</asp:ListItem>
                            <asp:ListItem>THIRD YEAR MINOR</asp:ListItem>
                            <asp:ListItem>THIRD YEAR MAJOR</asp:ListItem>
                            <asp:ListItem>INTERNS</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnNew_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <center>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label1" runat="server" Text="Date"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate">
                                            </asp:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="Test No"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTestNo" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtTestNo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Subject"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSub" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtSub"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="Topic"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTopic" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtTopic"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSave" CssClass="simplebtn" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                            <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" CausesValidation="false"
                                                OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:GridView ID="gridSeries" runat="server" AutoGenerateColumns="False" DataKeyNames="id"
                                DataSourceID="sourceSeries" CssClass="gridview" OnRowCommand="gridSeries_RowCommand"
                                OnRowCreated="gridSeries_RowCreated"  EmptyDataText="No results found !">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" Width="25px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                        SortExpression="id" />
                                    <asp:BoundField DataField="sDate" HeaderText="Date" SortExpression="sDate" />
                                    <asp:BoundField DataField="testNo" HeaderText="Test No" SortExpression="testNo" />
                                    <asp:BoundField DataField="subject" HeaderText="Subject" SortExpression="subject" />
                                    <asp:BoundField DataField="topics" HeaderText="Topics" SortExpression="topics" />
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                        Text="Button">
                                        <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                                        Text="Button">
                                        <ItemStyle Height="20px" Width="25px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceSeries" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id],convert(varchar(10),[sDate],103) as [sDate], [testNo], [subject], [topics] FROM [site_scheduleOnline]">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
