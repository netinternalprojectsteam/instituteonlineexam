﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="courseInfoOffline.aspx.cs" Inherits="OnlineExam.site.courseInfoOffline"
    Title="Course Live Offline" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Course/Package Information(Live Offline)</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnNew_Click" />
                        <asp:Label ID="lblID" runat="server" Visible="false" Text="Label"></asp:Label>
                        <asp:Label ID="lblImage" runat="server" Visible="false" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <center>
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label6" runat="server" Text="Select Class/Course" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlClasses" runat="server">
                                                <asp:ListItem>FIRST YEAR</asp:ListItem>
                                                <asp:ListItem>SECOND YEAR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MINOR</asp:ListItem>
                                                <asp:ListItem>THIRD YEAR MAJOR</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="top" rowspan="7">
                                            <asp:Image ID="Image1" runat="server" Width="100px" Height="100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label10" runat="server" Text="Select Session" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSessions" runat="server" DataSourceID="sourceSessions" DataTextField="sName"
                                                DataValueField="id">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text="No of MCQ" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtNo" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNo"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtNo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="No of Exam/Test" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtENo" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtENo"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label4" runat="server" Text="Fees" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFees" runat="server"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFees"
                                                FilterMode="ValidChars" ValidChars="0123456789.">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtFees"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="Label5" runat="server" Text="Duration" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="Label1" runat="server" Text="From Date" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtFromDate" runat="server" Width="65px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                            ControlToValidate="txtFromDate"></asp:RequiredFieldValidator>
                                                        <asp:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" runat="server" TargetControlID="txtFromDate">
                                                        </asp:CalendarExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtFromDate"
                                                            FilterMode="ValidChars" ValidChars="0123456789./-">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="Label8" runat="server" Text="To Date" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtToDate" runat="server" Width="65px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                                            ControlToValidate="txtToDate"></asp:RequiredFieldValidator>
                                                        <asp:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy" runat="server" TargetControlID="txtToDate">
                                                        </asp:CalendarExtender>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtToDate"
                                                            FilterMode="ValidChars" ValidChars="0123456789./-">
                                                        </asp:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Photo"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Details"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine" Width="300px" Height="80px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="btnSave" runat="server" CssClass="simplebtn" OnClick="btnSave_Click"
                                                Text="Save" />
                                            <asp:Button ID="btnUpdate" runat="server" CssClass="simplebtn" OnClick="btnUpdate_Click"
                                                Text="Update" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CssClass="simplebtn"
                                                OnClick="btnCancel_Click" Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:GridView ID="gridCoures" runat="server" CssClass="gridview" DataSourceID="sourceCourses"
                                PageSize="20" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id"
                                OnRowCommand="gridCoures_RowCommand" OnRowCreated="gridCoures_RowCreated">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id"
                                        InsertVisible="False" />
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="className" HeaderText="Course Name" SortExpression="className" />
                                    <asp:BoundField DataField="noofMCQs" HeaderText="No of MCQs" SortExpression="noofMCQs" />
                                    <asp:BoundField DataField="noofExam" HeaderText="No of Exam" SortExpression="noofExam" />
                                    <asp:BoundField DataField="fees" HeaderText="Fees" SortExpression="fees" />
                                    <asp:BoundField DataField="duration" HeaderText="Duration" SortExpression="duration" />
                                    <asp:BoundField DataField="imageName" HeaderText="imageName" SortExpression="imageName" />
                                    <asp:ImageField DataImageUrlField="ImageName" HeaderText="Image Preview">
                                        <ControlStyle Height="100px" Width="100px" />
                                    </asp:ImageField>
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/edit123.gif"
                                        Text="Button" />
                                    <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/cross.png"
                                        Text="Button" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceCourses" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id],[className], [courseName], [noofMCQs], [noofExam], [fees],convert(varchar(10),fromDate,103) + ' to ' +  convert(varchar(10),toDate,103) AS [duration],'/courseImages/'+ [imageName] as imageName FROM [site_coursesOffline] WHERE sessionID = (select id from site_sessions where isActive='true')">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sourceSessions" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id], [isActive], [sName] FROM [site_Sessions] order by sName">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
