﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class feesDeduction : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblAmount.Text = ob.getValue("Select fees from site_feesDeduction");
                txtAmount.Text = lblAmount.Text;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtAmount.Text.Length > 0)
            {
                ob.updateDeductionFees(Convert.ToInt16(txtAmount.Text));
                lblAmount.Text = txtAmount.Text;
                string script = "<script type=\"text/javascript\">cancelEdit();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }
    }
}
