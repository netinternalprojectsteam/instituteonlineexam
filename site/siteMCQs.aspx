﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="siteMCQs.aspx.cs" Inherits="OnlineExam.site.siteMCQs" Title="Site MCQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:SqlDataSource ID="sourceQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT [id], [question], [optA], [optB], [optC], [optD], [correctAns], [refBook], [explanation], [Answer], [entryDate] FROM [view_SiteMCQ] order by entryDate desc">
                            </asp:SqlDataSource>
                            Site MCQs</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Repeater ID="rptMCQs" runat="server" DataSourceID="sourceQuestions">
                            <AlternatingItemTemplate>
                                <div style="color: white; font-size: medium">
                                    <table style="background-color: Gray" width="100%">
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Que
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("question") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option A
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optA") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option B
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optB") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option C
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optC") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option D
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optD") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Answer
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("Answer") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Ref
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("refBook") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Exp
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("explanation")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                    Font-Bold="true" ForeColor="White" OnClick="LinkButton2_Click">Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </AlternatingItemTemplate>
                            <ItemTemplate>
                                <div style="font-size: medium">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Que
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("question") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option A
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optA") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option B
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optB") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option C
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optC") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Option D
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("optD") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Answer
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("Answer") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Ref
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("refBook") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 70px" align="left">
                                                Exp
                                            </td>
                                            <td style="width: 15px" align="left">
                                                :
                                            </td>
                                            <td align="left">
                                                <%#Eval("explanation")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <asp:LinkButton ID="LinkButton1" Font-Bold="true" CommandArgument='<%#Eval("id") %>'
                                                    runat="server" OnClick="LinkButton2_Click">Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
