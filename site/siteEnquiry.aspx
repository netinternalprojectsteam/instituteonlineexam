﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="siteEnquiry.aspx.cs" Inherits="OnlineExam.site.siteEnquiry" Title="Site Enquiries" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center" colspan="2">
                        <h3>
                            <asp:SqlDataSource ID="sourceEnq" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT TOP (15) id, name, email, mobile, city, preference, query,convert(varchar(10), eDate,103) as eDate FROM site_enquiry ORDER BY eDate DESC">
                            </asp:SqlDataSource>
                            Site Enquiries</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" style="display: none">
                        <table>
                            <tr>
                                <td align="left">
                                    Search site users registered :
                                    <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    between :&nbsp;
                                    <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp; and :&nbsp;
                                    <asp:TextBox ID="txtToDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp;
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnSearch_Click" />&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Height="350px">
                            <asp:GridView ID="gridUsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="id" DataSourceID="sourceEnq" CssClass="gridview" PageSize="25">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False"
                                        ReadOnly="True" />
                                    <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
                                    <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                                    <asp:BoundField DataField="mobile" HeaderText="Mobile" SortExpression="mobile" />
                                    <asp:BoundField DataField="query" HeaderText="Query" SortExpression="query" />
                                    <asp:BoundField DataField="eDate" HeaderText="On Date" SortExpression="eDate" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate">
            </asp:CalendarExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
            </asp:CalendarExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFromDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtToDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFromDate"
                WatermarkText="From Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtToDate"
                WatermarkText="To Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <asp:ModalPopupExtender ID="modalPopupUDetails" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="userDetails" Drag="true" CancelControlID="btnUerDetailsCancel">
            </asp:ModalPopupExtender>
            <div id="userDetails" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe4" frameborder="0" src="userDetails.aspx" height="500px" width="450px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="Button5" value="Done" type="button" />
                    <input id="btnUerDetailsCancel" value="Cancel" type="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
