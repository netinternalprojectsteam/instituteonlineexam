﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class inspirations : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            btnSave.Text = "Save";
            Panel1.Visible = true;
            txtName.Text = "";
            Editor1.Content = "";
            gridIns.Visible = false;
            btnNew.Visible = false;
            chkActive.Checked = true;
        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = false;
            btnNew.Visible = true;
            gridIns.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            string fileName = "default.jpg";
            if (FileUpload1.HasFile)
            {
                int maxID = Convert.ToInt16(ob.getValue("Select max(ID) as ID from site_inspirations"));
                maxID = maxID + 1;
                string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                string extToUpper = ext.ToUpper();
                if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
                {
                    if (FileUpload1.FileBytes.Length >= 4194304)
                    {
                        Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                        Messages11.Visible = true;
                    }
                    else
                    {
                        if (btnSave.Text == "Save")
                        {
                            fileName = maxID.ToString() + ext;
                        }
                        else { fileName = lblID.Text + ext; }
                        FileUpload1.SaveAs(Server.MapPath("~\\inspirations\\" + fileName));
                        if (btnSave.Text == "Save")
                        {
                            ob.saveInpirations(txtName.Text, fileName, Editor1.Content, chkActive.Checked, txtDegree.Text);
                            Messages11.setMessage(1, "Data saved Succesfully !");
                            Messages11.Visible = true;
                        }
                        if (btnSave.Text == "Update")
                        {

                            ob.updateInpirations(txtName.Text, fileName, Editor1.Content, chkActive.Checked, lblID.Text, txtDegree.Text);
                            Messages11.setMessage(1, "Data updated Succesfully !");
                            Messages11.Visible = true;
                        }
                    }
                }
            }
            else
            {
                if (btnSave.Text == "Save")
                {
                    ob.saveInpirations(txtName.Text, fileName, Editor1.Content, chkActive.Checked, txtDegree.Text);
                    Messages11.setMessage(1, "Data saved Succesfully !");
                    Messages11.Visible = true;
                }
                if (btnSave.Text == "Update")
                {

                    ob.updateInpirations(txtName.Text, lblFileName.Text, Editor1.Content, chkActive.Checked, lblID.Text, txtDegree.Text);
                    Messages11.setMessage(1, "Data updated Succesfully !");
                    Messages11.Visible = true;
                }
            }
            Panel1.Visible = false;
            btnNew.Visible = true;
            gridIns.Visible = true;
            gridIns.DataBind();
        }

        protected void gridIns_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            int row = 0;
            if (e.CommandName == "change")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridIns.Rows[row].Cells[0].Text;
                DataTable dt = ob.getData("SELECT id, Iname, '/inspirations/' + ImageName AS ImagePath, message, isActive, ImageName, degree FROM site_inspirations where id=" + lblID.Text + " ");
                txtName.Text = dt.Rows[0]["Iname"].ToString();
                chkActive.Checked = Convert.ToBoolean(dt.Rows[0]["isActive"].ToString());
                img.ImageUrl = dt.Rows[0]["ImagePath"].ToString();
                Editor1.Content = dt.Rows[0]["message"].ToString();
                Panel1.Visible = true;
                btnNew.Visible = false;
                btnSave.Text = "Update";
                gridIns.Visible = false;
                lblFileName.Text = dt.Rows[0]["ImageName"].ToString();
                txtDegree.Text = dt.Rows[0]["degree"].ToString();
            }

            if (e.CommandName == "remove")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridIns.Rows[row].Cells[0].Text;
                ob.deleteInspiration(lblID.Text);
                gridIns.DataBind();
            }
        }

        protected void gridIns_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
        }
    }
}
