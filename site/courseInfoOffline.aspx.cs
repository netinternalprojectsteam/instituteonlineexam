﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;


namespace OnlineExam.site
{
    public partial class courseInfoOffline : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = true;
            Panel2.Visible = false;
            gridCoures.Visible = false;
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;

            txtENo.Text = "";
            txtFees.Text = "";

            txtNo.Text = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSessions.SelectedItem.Text != "-Select-")
                {
                    Messages11.Visible = false;
                    string filename = "default.jpg";
                    DataTable dt = new DataTable();
                    dt = ob.getData("select * from site_coursesOffline where  className = '" + ddlClasses.Text + "'");
                    if (dt.Rows.Count == 0)
                    {
                        if (FileUpload1.HasFile)
                        {
                            string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                            string extToUpper = ext.ToUpper();
                            if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
                            {
                                if (FileUpload1.FileBytes.Length >= 4194304)
                                {
                                    Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                                    Messages11.Visible = true;
                                }
                                else
                                {
                                    int id = 0;
                                    try
                                    {
                                        id = Convert.ToInt32(ob.getValue("Select max(id) from site_courses"));
                                        id = id + 1;
                                    }
                                    catch { id = id + 1; }
                                    filename = id.ToString() + FileUpload1.PostedFile.FileName;
                                    FileUpload1.SaveAs(Server.MapPath("~\\courseImages\\" + filename));
                                    ob.insertCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), filename, ddlClasses.Text, Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")), Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDetails.Text, ddlSessions.SelectedValue);
                                }

                            }

                            else
                            {
                                Messages11.setMessage(0, "Allowd files are .jpg, .jpeg, .png");
                                Messages11.Visible = true;
                            }
                        }
                        else
                        {
                            ob.insertCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), filename, ddlClasses.Text, Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")), Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDetails.Text, ddlSessions.SelectedValue);
                            //ob.insertCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), txtDuration.Text, filename, ddlClasses.Text);
                        }
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        btnNew.Visible = true;
                        gridCoures.DataBind();
                        gridCoures.Visible = true;
                        Messages11.Visible = true;
                        Messages11.setMessage(1, "Data saved successfully !");
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Course Name already exists !");
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Select proper Session");
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("String was not recognized as a valid DateTime"))
                {

                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Enter proper dates !");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSessions.SelectedItem.Text != "-Select-")
                {
                    Messages11.Visible = false;
                    string filename = "default.jpg";
                    DataTable dt = new DataTable();
                    dt = ob.getData("select * from site_coursesOffline where  className = '" + ddlClasses.Text + "' and id != " + lblID.Text + "");
                    if (dt.Rows.Count == 0)
                    {
                        if (FileUpload1.HasFile)
                        {
                            string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                            string extToUpper = ext.ToUpper();
                            if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
                            {
                                if (FileUpload1.FileBytes.Length >= 4194304)
                                {
                                    Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                                    Messages11.Visible = true;
                                }
                                else
                                {

                                    filename = lblID.Text + FileUpload1.PostedFile.FileName;
                                    FileUpload1.SaveAs(Server.MapPath("~\\courseImages\\" + filename));
                                    ob.updateCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), filename, lblID.Text, ddlClasses.Text, Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")), Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDetails.Text, ddlSessions.SelectedValue);

                                }

                            }

                            else
                            {
                                Messages11.setMessage(0, "Allowd files are .jpg, .jpeg, .png");
                                Messages11.Visible = true;
                            }
                        }
                        else
                        {
                            ob.updateCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), lblImage.Text, lblID.Text, ddlClasses.Text, Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")), Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDetails.Text, ddlSessions.SelectedValue);
                            // ob.updateCoursesOffline(txtNo.Text, txtENo.Text, Convert.ToDouble(txtFees.Text), txtDuration.Text, lblImage.Text, lblID.Text, ddlClasses.Text);
                        }
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        btnNew.Visible = true;
                        gridCoures.DataBind();
                        gridCoures.Visible = true;
                        Messages11.Visible = true;
                        Messages11.setMessage(1, "Data updated successfully !");
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Course Name already exists !");
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Select proper Session");
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("String was not recognized as a valid DateTime"))
                {

                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Enter proper dates !");
                }
            }
        }

        protected void gridCoures_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt32(e.CommandArgument);
                lblID.Text = gridCoures.Rows[row].Cells[0].Text;
                DataTable dt = new DataTable();
                dt = ob.getData(" SELECT [id], [noofMCQs], [noofExam], [fees], [duration],'/courseImages/'+ [imageName] as imagePath,imageName,className,convert(varchar(10),fromDate,103) as fromDate,convert(varchar(10),toDate,103) as toDate,Details,sessionID FROM [site_coursesOffline] where id = " + lblID.Text + "");

                txtNo.Text = dt.Rows[0]["noofMCQs"].ToString();
                txtENo.Text = dt.Rows[0]["noofExam"].ToString();
                txtFees.Text = dt.Rows[0]["fees"].ToString();
                Image1.ImageUrl = dt.Rows[0]["imagePath"].ToString();
                lblImage.Text = dt.Rows[0]["imageName"].ToString();
                ddlClasses.Text = dt.Rows[0]["className"].ToString();
                txtFromDate.Text = dt.Rows[0]["fromDate"].ToString();
                txtToDate.Text = dt.Rows[0]["toDate"].ToString();
                txtDetails.Text = dt.Rows[0]["Details"].ToString();
                ddlSessions.SelectedValue = dt.Rows[0]["sessionID"].ToString();
                Panel1.Visible = true;
                Panel2.Visible = false;
                btnNew.Visible = false;
                btnUpdate.Visible = true;
                btnCancel.Visible = true;
                btnSave.Visible = false;
                Messages11.Visible = false;
            }
            if (e.CommandName == "remove")
            {

                row = Convert.ToInt32(e.CommandArgument);
                lblID.Text = gridCoures.Rows[row].Cells[0].Text;
                ob.deleteOfflineCourse(lblID.Text);
                Messages11.Visible = true;
                Messages11.setMessage(1, "Deleted successfully !");
                gridCoures.DataBind();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            btnNew.Visible = true;
            Panel1.Visible = false;
            Panel2.Visible = true;
            Messages11.Visible = false;
        }

        protected void gridCoures_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[7].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[7].Visible = false;
            }
        }
    }
}
