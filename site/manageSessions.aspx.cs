﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam.site
{
    public partial class manageSessions : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                if (btnSave.Text == "Save")
                {
                    ob.insertSession(txtSessions.Text);
                }
                else
                {
                    ob.updateSession(txtSessions.Text, lblID.Text);
                    btnCancel.Visible = false;
                    btnSave.Text = "Save";
                    gridSessions.Visible = true;

                }
                txtSessions.Text = "";
                gridSessions.DataBind();

            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("Violation of PRIMARY KEY constraint 'PK_site_Sessions'"))
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Session Name already exists !");
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            txtSessions.Text = "";
            gridSessions.Visible = true;
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void gridSessions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Messages11.Visible = false;
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridSessions.Rows[row].Cells[1].Text;
                txtSessions.Text = gridSessions.Rows[row].Cells[2].Text;
                btnSave.Text = "Update";
                btnCancel.Visible = true;
                gridSessions.Visible = false;
            }
            if (e.CommandName == "setSession")
            {
                row = Convert.ToInt16(e.CommandArgument);
                ob.setSession(gridSessions.Rows[row].Cells[1].Text);
                gridSessions.DataBind();
            }
        }
    }
}
