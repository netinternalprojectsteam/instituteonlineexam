﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="regUsers.aspx.cs" Inherits="OnlineExam.site.regUsers" Title="Registered Users For Offline & Online Test Series" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Registered Users For Offline & Online Test Series</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:DropDownList ID="ddlSelect" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSelect_SelectedIndexChanged">
                            <asp:ListItem>Online</asp:ListItem>
                            <asp:ListItem>Live Offline</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT className, uname, uemail, mobileNo, city, convert(varchar(10), edate,103) as edate,' ' as examCenter FROM view_ActiveSessionOnlineUsers">
                        </asp:SqlDataSource>
                        <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                            DataKeyNames="uemail" DataSourceID="sourceDetails" AllowPaging="true" PageSize="20" EmptyDataText="No details found !"
                            OnRowCreated="gridDetails_RowCreated">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr.No">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                <asp:BoundField DataField="uemail" HeaderText="Email" ReadOnly="True" SortExpression="uemail" />
                                <asp:BoundField DataField="mobileNo" HeaderText="Mobile" SortExpression="mobileNo" />
                                <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" />
                                <asp:BoundField DataField="className" HeaderText="Course Name" SortExpression="className" />
                                <asp:BoundField DataField="examCenter" HeaderText="Exam Center" SortExpression="examCenter" />
                                <asp:BoundField DataField="edate" HeaderText="Join Date" SortExpression="edate" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
