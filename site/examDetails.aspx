﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="examDetails.aspx.cs" Inherits="OnlineExam.site.examDetails" Title="Exam Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Exam Details</h3>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;&nbsp
                        <asp:Label ID="Label1" runat="server" Text="Exam Type"></asp:Label>
                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                            <asp:ListItem>Online Exam</asp:ListItem>
                            <asp:ListItem>Live Offline Exam</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Repeater ID="rptDetails" runat="server">
                            <ItemTemplate>
                                <table width="700px">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("Edetails") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("id") %>'
                                                OnClick="linkBtnEdit_Click" Font-Bold="true" Font-Size="Large" Style="text-decoration: underline;
                                                color: Blue">Edit</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="sourceDetails" runat="server"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                                        <cc1:Editor ID="editorDetails" runat="server" Width="800px" Height="500px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                                        <asp:Button ID="btnCancel" CausesValidation="false" runat="server" Text="Cancel"
                                            OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
