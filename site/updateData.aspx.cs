﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;

namespace OnlineExam.site
{
    public partial class updateData : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            //string[] a = Directory.GetFiles(Server.MapPath("~\\admin\\ContactImages\\" + gridCompanyName.Rows[row].Cells[1].Text + "\\"), "*.*");
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("File", typeof(String)));


                string[] a = Directory.GetFiles(Server.MapPath("~\\UploadedDocs\\"), "*.*");
                for (int i = 0; i < a.Length; i++)
                {

                    DataRow dr = dt.NewRow();

                    string just_file = System.IO.Path.GetFileName((string)a[i]);
                    dr[0] = just_file;

                    //dr[0] = a[i].ToString();


                    dt.Rows.Add(dr);
                }

                gridFiles.DataSource = dt;
                gridFiles.DataBind();
            }
            rptAbout.DataBind();
        }

        protected void btnUpdateWhyHMT_Click(object sender, EventArgs e)
        {
            if (editorWhyHMT.Content.Length > 0)
            {
                ob.updateWhyHMT(editorWhyHMT.Content, lblIDWhyHMT.Text);
                rptWhyHMT.DataBind();
                Panel4.Visible = false;
                // gridWhyHMT.Visible = true;
                rptWhyHMT.Visible = true;
            }
        }




        //protected void gridAbout_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //}


        protected void btnUpdateAbout_Click(object sender, EventArgs e)
        {
            if (editorAbout.Content.Length > 0)
            {
                ob.updateAboutUs(editorAbout.Content, lblIDAbout.Text);

                Panel2.Visible = false;
                rptAbout.DataBind();
                rptAbout.Visible = true;
            }
        }

        //protected void gridAbout_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    //int row = 0;
        //    //if (e.CommandName == "change")
        //    //{
        //    //    row = Convert.ToInt16(e.CommandArgument);
        //    //    lblID.Text = gridAbout.Rows[row].Cells[0].Text;
        //    //    editorAbout.Content = gridAbout.Rows[row].Cells[1].Text;
        //    //    Panel2.Visible = true;
        //    //    gridAbout.Visible = false;
        //    //}
        //}





        //protected void gridNews_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //}

        //protected void gridNews_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    //int row = 0;
        //    //if (e.CommandName == "change")
        //    //{
        //    //    row = Convert.ToInt16(e.CommandArgument);
        //    //    lblID.Text = gridNews.Rows[row].Cells[0].Text;
        //    //    editorNews.Content = gridNews.Rows[row].Cells[1].Text;
        //    //    Panel6.Visible = true;
        //    //    gridNews.Visible = false;
        //    //}
        //}

        protected void btnUpdateNews_Click(object sender, EventArgs e)
        {
            if (editorNews.Content.Length > 0)
            {
                ob.updateScrollinData(editorNews.Content, lblIDNews.Text);
                Panel6.Visible = false;
                rptNews.Visible = true;
                rptNews.DataBind();
            }
        }


        //protected void gridWhyHMT_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    //int row = 0;
        //    //if (e.CommandName == "change")
        //    //{
        //    //    row = Convert.ToInt16(e.CommandArgument);
        //    //    lblID.Text = gridWhyHMT.Rows[row].Cells[0].Text;

        //    //    editorWhyHMT.Content = gridWhyHMT.Rows[row].Cells[1].Text;
        //    //    Panel4.Visible = true;
        //    //    gridWhyHMT.Visible = false;
        //    //}
        //}



        //protected void gridWhyHMT_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        e.Row.Cells[0].Visible = false;
        //    }
        //}

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            rptAbout.Visible = true;
        }
        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            Panel4.Visible = false;
            rptWhyHMT.Visible = true;
        }
        protected void btnCancel3_Click(object sender, EventArgs e)
        {
            Panel6.Visible = false;
            rptNews.Visible = true;
        }

        protected void btnCancel4_Click(object sender, EventArgs e)
        {
            Panel10.Visible = false;
            rptLatest.Visible = true;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            if (FileUpload1.HasFile)
            {
                string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                string extToUpper = ext.ToUpper();
                if (extToUpper == ".PDF")
                {
                    if (FileUpload1.FileBytes.Length >= 4194304)
                    {
                        //Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                        //Messages11.Visible = true;
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add(new DataColumn("File", typeof(String)));
                        string[] a = Directory.GetFiles(Server.MapPath("~\\UploadedDocs\\"), "*.*");
                        for (int i = 0; i < a.Length; i++)
                        {

                            DataRow dr = dt.NewRow();
                            string just_file = System.IO.Path.GetFileName((string)a[i]);
                            dr[0] = just_file;
                            dt.Rows.Add(dr);
                        }

                        string IsAvailable = "no";
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (dt.Rows[j][0].ToString() == FileUpload1.PostedFile.FileName)
                            {
                                IsAvailable = "yes";
                            }
                        }
                        if (IsAvailable == "no")
                        {
                            FileUpload1.SaveAs(Server.MapPath("~\\UploadedDocs\\" + FileUpload1.PostedFile.FileName));
                            ob.insertFileName(FileUpload1.PostedFile.FileName);
                        }
                        else
                        {
                            lblMsg.Text = "File already exists. Use another name for file.";
                            lblMsg.Visible = true;
                        }

                       


                       

                        gridFiles.DataSource = dt;
                        gridFiles.DataBind();
                    }
                }
            }



        }

        //For edit about Us
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDAbout.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptAbout.Items[0].FindControl("Label2");
            editorAbout.Content = lbl.Text;

            Panel2.Visible = true;
            //gridAbout.Visible = false;
            rptAbout.Visible = false;
        }

        //For edit why HMT
        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDWhyHMT.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptWhyHMT.Items[0].FindControl("Label2");
            editorWhyHMT.Content = lbl.Text;


            Panel4.Visible = true;
            rptWhyHMT.Visible = false;
        }


        //For edit News
        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDNews.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptNews.Items[0].FindControl("Label2");
            editorNews.Content = lbl.Text;


            Panel6.Visible = true;
            rptNews.Visible = false;
        }
        // For edit latest news
        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDLatestNews.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptLatest.Items[0].FindControl("Label2");
            editorLatest.Content = lbl.Text;


            Panel10.Visible = true;
            rptLatest.Visible = false;
        }

        protected void btnUpdateLatestNews_Click(object sender, EventArgs e)
        {
            if (editorLatest.Content.Length > 0)
            {
                ob.updateLatestNews(editorLatest.Content, lblIDLatestNews.Text);
                Panel10.Visible = false;
                rptLatest.Visible = true;
                rptLatest.DataBind();
            }
        }

        //for edit instructions to show when user click on online exam.
        protected void LinkButton6_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDIns.Text = lbtn.CommandArgument;

            Label lbl = (Label)repIns.Items[0].FindControl("Label2");
            editorIns.Content = lbl.Text;


            Panel12.Visible = true;
            repIns.Visible = false;
        }

        protected void btnUpdateIns_Click(object sender, EventArgs e)
        {
            if (editorIns.Content.Length > 0)
            {
                ob.updateInstructions(editorIns.Content, lblIDIns.Text);
                Panel12.Visible = false;
                repIns.Visible = true;
                repIns.DataBind();
            }
        }


        protected void btnCancelrptOnline_Click(object sender, EventArgs e)
        {
            Panel12.Visible = false;
            repIns.Visible = true; 
        }

        //for edit instructions to show when user click on offline exam.
        protected void LinkButton7_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            lblIDInsOffline.Text = lbtn.CommandArgument;

            Label lbl = (Label)rptInsOffline.Items[0].FindControl("Label2");
            editorInsOffline.Content = lbl.Text;


            Panel14.Visible = true;
            rptInsOffline.Visible = false;
        }

        protected void btnUpdateInsOffline_Click(object sender, EventArgs e)
        {
            if (editorInsOffline.Content.Length > 0)
            {
                ob.updateInstructionsOffline(editorInsOffline.Content, lblIDInsOffline.Text);
                Panel14.Visible = false;
                rptInsOffline.Visible = true;
                rptInsOffline.DataBind();
            }
        }

        protected void btnCancelrptOffline_Click(object sender, EventArgs e)
        {
            Panel14.Visible = false;
            rptInsOffline.Visible = true;
        }

     
    }
}
