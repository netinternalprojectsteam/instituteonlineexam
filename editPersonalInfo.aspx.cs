﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

namespace OnlineExam
{
    public partial class editPersonalInfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    lblID.Text = Session["editUserID"].ToString();
                    DataTable dt = new DataTable();
                    dt = ob.GetUserInfoByID(lblID.Text);
                    //uid, uemail, uname, mobileno,imageName

                    DataTable dtInst = new DataTable();
                    dtInst = ob.getInstitute();
                    ddlInstitute.DataSource = dtInst;
                    ddlInstitute.DataBind();
                    ddlInstitute.Items.Insert(0, new ListItem("--Select--", "0"));
                    try
                    {
                        ddlInstitute.SelectedValue = dt.Rows[0]["instituteId"].ToString();
                    }
                    catch { }
                    txtName.Text = dt.Rows[0]["uname"].ToString();
                    txtEmail.Text = dt.Rows[0]["uemail"].ToString();
                    txtMo.Text = dt.Rows[0]["mobileno"].ToString();
                    if (dt.Rows[0]["imageName"].ToString().Length > 0)
                    {
                        lblImage.Text = dt.Rows[0]["imageName"].ToString();
                    }
                    else
                    {
                        lblImage.Text = "default.jpg";
                    }
                }
                catch { }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            string fileName = lblImage.Text;
            if (FileUpload1.HasFile)
            {
                string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                string extToUpper = ext.ToUpper();
                if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
                {
                    if (FileUpload1.FileBytes.Length >= 4194304)
                    {
                        Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                        Messages11.Visible = true;
                    }
                    else
                    {
                        fileName = lblID.Text + ext;
                        FileUpload1.SaveAs(Server.MapPath("~\\stdImages\\" + fileName));
                        ob.updatePersonalInfo(txtName.Text, txtMo.Text, fileName, lblID.Text);
                        Session["imgName"] = fileName;
                        //Session.Remove("editUserID");
                        string script = "<script type=\"text/javascript\">callfun();</script>";
                        //Page page = HttpContext.Current.CurrentHandler as Page;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    }
                }
            }
            else
            {
                ob.updatePersonalInfo(txtName.Text, txtMo.Text, fileName, lblID.Text);

                Session["imgName"] = fileName;
                //Session.Remove("editUserID");
                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

            }

            string details = "";
            details = txtName.Text + "#" + txtMo.Text;
            Session["UDetails"] = details;
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static List<string> GetInstitute(string prefixText)
        {
            classes.DataLogic obj = new classes.DataLogic();
            DataTable dt = new DataTable();
            dt = obj.getData("Select InstituteName from admin_user where InstituteName LIKE '%'+'" + prefixText + "'+'%'");
            List<string> items = new List<string>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strName = dt.Rows[i][0].ToString();
                items.Add(strName);
            }
            return items;
        }

    }
}
