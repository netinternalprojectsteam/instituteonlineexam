﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="queWithAnswers.aspx.cs"
    Inherits="OnlineExam.queWithAnswers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Questions With Answers</title>
    <style type="text/css">
        .paperLabel
        {
            color: black;
            font-size: medium;
        }
        .queNo
        {
            font-weight: bold;
            color: black;
            font-size: medium;
        }
    </style>

    <script type="text/javascript">
  function CallPrint(strid)
  {
      var prtContent = document.getElementById(strid);
      var WinPrint = window.open('','','letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
      WinPrint.document.write(prtContent.innerHTML);
      WinPrint.document.close();
      WinPrint.focus();
      WinPrint.print();
      WinPrint.close();

}
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblExamID" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="lblUserID" runat="server" Text="Label"></asp:Label>
        <asp:Panel ID="Panel1" runat="server">
            <table>
                <tr>
                    <td align="right">
                        <input type="image" value="Print " id="Button1" onclick="javascript:CallPrint('divPrint')"
                            src="/images/printico.png" title="Print" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="divPrint">
                            <asp:Repeater ID="repQueWithAnswer" runat="server">
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="left" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" valign="top" style="width: 130px;">
                                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("srNo") %>' CssClass="queNo"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>' CssClass="paperLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="width: 100px;">
                                                            <asp:Label ID="Label3" runat="server" Text="Correct Answer" CssClass="queNo"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("CorrectAns") %>' CssClass="paperLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="width: 100px;">
                                                            <asp:Label ID="Label5" runat="server" Text="Your Answer" CssClass="queNo"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("yourAns") %>' CssClass="paperLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="width: 100px;">
                                                            <asp:Label ID="Label7" runat="server" Text="Explanation" CssClass="queNo"></asp:Label>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label9" runat="server" Text='<%#Eval("questioninfo") %>' CssClass="paperLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <hr style="border-bottom: solid 1 px black" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
