﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class editPassword : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblID.Text = Session["editUserID"].ToString();
                }
            }
            catch { }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT uid, password, uname FROM exam_users where uid = " + lblID.Text + "");
            if (dt.Rows[0]["password"].ToString() == base64Encode(txtOld.Text))
            {
                if (txtNew.Text == txtConfirm.Text)
                {
                    ob.updatePassword(lblID.Text, base64Encode(txtNew.Text));
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Password Changed Successfully !");
                    txtNew.Text = "";
                    txtOld.Text = "";
                    txtConfirm.Text = "";
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "New password and confirm password not match !");
                }
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Old password not match !");
            }
        }


        private string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }
    }
}
