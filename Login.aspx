﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs"
    Inherits="OnlineExam.Login" Title="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="height: 126px; width: 981px;">
        <tr>
            <td align="center" colspan="2">
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="Label1" runat="server" Style="font-weight: 700" Text="Enter UserName :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="UserName" runat="server" Width="190px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="Label2" runat="server" Style="font-weight: 700" Text="Enter Password :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="Password" runat="server" Width="190px" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:CheckBox ID="RememberMe" runat="server" Text="Remember Me" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Login" />
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="Button" />
            </td>
        </tr>
    </table>
</asp:Content>
