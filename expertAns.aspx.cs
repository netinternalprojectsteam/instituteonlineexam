﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class expertAns : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gridDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt16(e.CommandArgument);
                lblQue.Text = gridDetails.Rows[row].Cells[1].Text;
                if (gridDetails.Rows[row].Cells[2].Text.Length > 0)
                {
                    txtAns.Text = gridDetails.Rows[row].Cells[2].Text;
                }
                else { txtAns.Text = ""; }
                if (txtAns.Text == "Not Answered !")
                {
                    txtAns.Text = "";
                }
                lblID.Text = gridDetails.Rows[row].Cells[6].Text;
                lblEID.Text = gridDetails.Rows[row].Cells[4].Text;
                Panel1.Visible = true;
                gridDetails.Visible = false;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ob.updateAnswer(txtAns.Text, lblID.Text);
            Panel1.Visible = false;
            gridDetails.Visible = true;
            gridDetails.DataBind();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            gridDetails.Visible = true;
        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[6].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[6].Visible = false;
            }
        }
    }
}
