﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="ExamResult.aspx.cs" Inherits="OnlineExam.ExamResult" Title="Exam Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td align="left">
                <h4>
                    Exam Result</h4>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server" 
                    onrowdatabound="GridView1_RowDataBound">
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblResult" runat="server"></asp:Label>
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
</asp:Content>
