﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace OnlineExam
{
    public partial class userMaster : System.Web.UI.MasterPage
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string bID = Request.Browser.Id;
                if (bID.ToLower().Contains("chrome") || bID.ToLower().Contains("safari1plus"))
                {
                    showBrowserMsg.Visible = false;

                }
                else { showBrowserMsg.Visible = true; }

            }
            try
            {
                if (!IsPostBack)
                {
                    if (HttpContext.Current.User.Identity.Name.ToString().Length > 0)
                    {

                        DataTable dt = new DataTable();
                        string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                        if (val[2].ToString() == "NXGOnlineExam")
                        {
                            if (val[1].ToString() == "User")
                            {
                                Panel1.Visible = false;
                                linkLogout.Visible = true;
                                dt = ob.GetUserInfoByID(val[0].ToString());
                                lblUserName.Text = dt.Rows[0]["uname"].ToString();
                                Panel4.Visible = true;
                                Panel3.Visible = true;
                                Panel5.Visible = false;
                                Panel2.Visible = true;
                            }
                            if (val[1].ToString() == "Admin")
                            {
                                Response.Redirect("/Default.aspx");
                            }

                        }
                        else { Panel2.Visible = false; }
                        if (Request.Url.LocalPath.ToString().Contains("/getInstitute.aspx") || Request.Url.LocalPath.ToString().Contains("/LoginDetails.aspx"))
                        {
                            Response.Redirect("testsList.aspx");
                        }
                    }

                    if (Request.Url.LocalPath.ToString().Contains("exmResult.aspx"))
                    {
                        Panel2.Visible = false;
                        //Panel3.Visible = false;
                        linkLogout.Visible = false;
                    }

                }
            }
            catch { }
        }

        protected void linkLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Default.aspx");

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //if (txtName.Text.Length > 0 && txtMobile.Text.Length > 0 && txtMail.Text.Length > 0)
            //{
            //    ob.insertContactUs(txtName.Text, txtMail.Text, txtMobile.Text, txtQuery.Text);
            //    lblContactusMsg.Visible = true;
            //    lblContactusMsg.Text = "Your Details Saved Successfully !";
            //    lblContactusMsg.ForeColor = System.Drawing.Color.Green;
            //    txtMail.Text = "";
            //    txtMobile.Text = "";
            //    txtName.Text = "";
            //    txtQuery.Text = "";
            //}
            //else
            //{
            //    lblContactusMsg.Text = "Name,Email,Mobile Required !";
            //    lblContactusMsg.ForeColor = System.Drawing.Color.Red;
            //    lblContactusMsg.Visible = true;
            //}
        }
    }
}
