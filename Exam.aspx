﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam.aspx.cs" Inherits="OnlineExam.Exam" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Exam</title>
    <link href="/css/slideOutPanel.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.3.2.js" type="text/javascript"></script>

    <script src="js/timer.js" type="text/javascript"></script>

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

    <%--<title>Exam</title>
    <link href="/css/slideOutPanel.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.3.2.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
  function closeIt()
  {
   return "Any string value here forces a dialog box to \n" + 
         "appear before closing the window.";
  } 
  
  window.onbeforeunload = closeIt;
    </script>

    <script type="text/javascript">
 function getData()
{
var data=document.getElementById('Label1').innerHTML;
document.getElementById('queDivHidden').innerHTML=data;
}
    </script>

    <script type="text/javascript">
    //to save user result in div
    function saveResult(value)
    {
     // alert(value);lblQuestionID
    //alert(value);
  updateAnswerSheet(value);
  
  if(value=="0")
  { 
  setAns(value);
  }
  else{
  
  }
  
    var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
  
     var queNo=document.getElementById('queID').innerHTML;
     var questid=document.getElementById('lblQuestionID').innerHTML;
    //alert('x');
    
     var selectedOption="selectedOption";
     selectedOption=selectedOption+queNo;
     //alert(selectedOption);
     //get old answer
     var oldAns=document.getElementById(selectedOption).innerHTML;
     //alert(oldAns);
     //set new answer
     document.getElementById(selectedOption).innerHTML=value;
     var x=document.getElementById(selectedOption);
     //alert(x.innerHTML);
    
     var data=document.getElementById('Label1').innerHTML;
     document.getElementById('Label1').value=data;
     var data1=document.getElementById('Label1').value;
     //alert(data1);
   // document.getElementById('LatestData').innerHTML=data1;
   
     //var str=document.getElementById("demo").innerHTML; 
var n=oldAns.replace(oldAns,value);
document.getElementById(selectedOption).innerHTML=n;
//alert(n +':nn ');

var hours=document.getElementById('hourLeft').innerHTML;
var minutes=document.getElementById('minLeft').innerHTML;
var secs=document.getElementById('secLeft').innerHTML;

var totalTime=(hours*3600000)+(minutes*60000)+(secs*1000);

//var preTime=document.getElementById('lblPreTime').innerHTML;
//var timeSpent=(preTime-totalTime)/1000;
//document.getElementById('lblPreTime').innerHTML=totalTime;
    var status =  navigator.onLine;;
    //alert(status);
    if (status) 
    {  //alert('xxxxx');
     var divID123=document.getElementById('showInternetLostMsg');
     divID123.innerHTML="Yes";
    
      var xmlhttp;
      //alert(xmlhttp); 
      if(value!=0)
      {   
      if (value=="")
      {
        document.getElementById("txtHint").innerHTML="";
         return;
       }
       if (window.XMLHttpRequest)
       {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
        }
       else
        {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
       }
        //alert('xxxxx');
        xmlhttp.open("GET","saveexamstate.aspx?q="+questid+"&ans="+value+"&act=saveAns&rTime="+totalTime+"&spentTime=0&examID="+examID+"&uid="+uid+"",true);
        xmlhttp.send();
        }
        else
        {
         //alert('ans : 0');
         if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
          }
          else
          {// code for IE6, IE5
           xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function()
          {
           if (xmlhttp.readyState==4 && xmlhttp.status==200)
           {
             document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
            }
           }
        xmlhttp.open("GET","saveexamstate.aspx?q="+questid+"&ans="+value+"&act=saveAns&rTime="+totalTime+"&spentTime=0&examID="+examID+"&uid="+uid+"",true);
        xmlhttp.send();
        }
     }
     else
     {
     var divID=document.getElementById('answeredQue');
      var oldValue= divID.innerHTML;
      divID.innerHTML=oldValue+"@"+questid+"X"+value+"X"+totalTime;
      //alert(divID.innerHTML);
     //document.getElementById('HiddenField2').value=oldHiddenfield
     // alert(questid);
     //var sssss=  document.getElementById('HiddenField2').value;
     //alert(sssss);
     var divID123=document.getElementById('showInternetLostMsg');
      var intMsg= divID123.innerHTML;
     if(intMsg == "Yes")
     {divID123.innerHTML="No";
     alert('Internet connection is lost but you can continue with your test.\n Submit the result only when internet connection is back');
     }
     }
    }
    
    </script>

    <script type="text/javascript" language="javascript">
    function markQue()
    {
    
     var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
  
    
       var queNo=document.getElementById('queID').innerHTML;
      //alert(queNo);
       var questid=document.getElementById('lblQuestionID').innerHTML;
       var Ismarked="marked"+queNo;
      // alert(Option)
       var div1=document.getElementById(Ismarked);
       
       //to reflect in answersheet
       div1.innerHTML='<img src=/images/mark.png  width=20px height=20px />';
       
       //to reflect in div
       var Ismarked="Ismarked"+queNo;
       var div1=document.getElementById(Ismarked);
       div1.innerHTML='marked';
      var btnToChange="btn"+queNo;
       //alert(btnToChange);
       var divtoChange=document.getElementById(btnToChange);
       divtoChange.className = "markedBtn";
      
       
//       }
      document.getElementById('mark').style.display='none';
       document.getElementById('unmark').style.display='block';
       
    var status = navigator.onLine;
    if (status) 
    {
       var xmlhttp;
       //alert(xmlhttp);    
       if (questid=="")
       {
       document.getElementById("txtHint").innerHTML="";
       return;
       }
       if (window.XMLHttpRequest)
       {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
       }
       else
       {// code for IE6, IE5
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       }
       xmlhttp.onreadystatechange=function()
       {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
         {
          document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
         }
       }
     xmlhttp.open("GET","saveexamstate.aspx?q="+questid+"&ans=mark&act=mark&examID="+examID+"&uid="+uid+"",true);
     xmlhttp.send();
    }
    else
    {
    var divID=document.getElementById('markedQue1');
      var oldValue=divID.innerHTML;
      divID.innerHTML=oldValue+"@"+questid;
      //alert(divID.innerHTML);
      
       //document.getElementById('HiddenField4').value=oldHiddenfield+"#"+questid;
       // alert(questid);
        //var sssss=  document.getElementById('HiddenField4').value;
       //alert(sssss);
    }
   
     
    }
    </script>

    <script type="text/javascript" language="javascript">
    function unmarkQue()
    {
    
     
     var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
       
       var queNo=document.getElementById('queID').innerHTML;
      //alert(queNo);
       var questid=document.getElementById('lblQuestionID').innerHTML;
       var Ismarked="marked"+queNo;
      // alert(Option)
       var div1=document.getElementById(Ismarked);
       div1.innerHTML='';
       Ismarked="Ismarked"+queNo;
       div1=document.getElementById(Ismarked);
       div1.innerHTML='unmarked';
       var btnToChange="btn"+queNo;
       //alert(btnToChange);
       var divtoChange=document.getElementById(btnToChange);
       divtoChange.className = "btn1";
       document.getElementById('mark').style.display='block';
       document.getElementById('unmark').style.display='none';
 
     var status = navigator.onLine;
     if (status) 
     {
        var xmlhttp;
        //alert(xmlhttp);    
        if (questid=="")
         {
          document.getElementById("txtHint").innerHTML="";
          return;
         }
         if (window.XMLHttpRequest)
         {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
         }
         else
         {// code for IE6, IE5
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
         xmlhttp.onreadystatechange=function()
        {
         if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
         }
       }

      xmlhttp.open("GET","saveexamstate.aspx?q="+questid+"&act=unmark&examID="+examID+"uid="+uid+"",true);
      xmlhttp.send();
      } 
      
      else
      {
   // alert("Your internet connection is lost..."); return false;
   var divID=document.getElementById('unmarkedQue');
      var oldValue= divID.innerHTML;
      divID.innerHTML=oldValue+"@"+questid;
      //alert(divID.innerHTML);
       // alert(questid);
        //var sssss=  document.getElementById('HiddenField5').value;
       //alert(sssss);
    }
    }
    </script>

    <script type="text/javascript">
    // hide/show Next Previou Buttons
    function setNextPre(value)
    {
    var pre=document.getElementById('queID').innerHTML;
    var max=document.getElementById('lblMaxQue').innerHTML;
    document.getElementById('btnNext').style.display='block';
    document.getElementById('btnPre').style.display='block';
    
    if(Number(pre)==Number(1))
    {
    //alert(pre);
    document.getElementById('btnNext').style.display='block';
    document.getElementById('btnPre').style.display='none';
    }
    if(Number(pre)==Number(max))
    {
    //alert(max);
    document.getElementById('btnNext').style.display='none';
    document.getElementById('btnPre').style.display='block';
    }
    
    
    }
    
    function changeData()
    {
    
    var div=document.getElementById('queDiv');
    
   var data=document.getElementById(div).innerHTML;
   //alert(data);
  document.getElementById('Label2').value=data;
    }
    </script>

    <script type="text/javascript">  
  //to load question 
     function Getdivdata(value) 
    {
        //alert(value);
    
        document.getElementById('HiddenField1').value=value;
        //alert(document.getElementById('HiddenField1').value);
    
        document.getElementById('queID').innerHTML=value;
        //alert(document.getElementById('queID').innerHTML);
    
        document.getElementById('no').innerHTML=value;
        //alert(document.getElementById('no').innerHTML);
    
        var btn="q";
        var div = document.getElementById(value);
        //alert(div);
    
        btn=btn+value;
        var que="que";
        que=que+value;
        div = document.getElementById(que);
        document.getElementById('lblQue').innerHTML=div.innerHTML;
        var optA="optA";
        optA=optA+value;
        div = document.getElementById(optA);
   
        if(div.innerHTML.length>0)
        {
            document.getElementById('lblOpt1').innerHTML=div.innerHTML;
            div.style.display='block';
            document.getElementById('rdOptA').style.display='block';
            document.getElementById('lblOpt1').style.display='block'
        }
        else
        {
            div.style.display='none';
            document.getElementById('rdOptA').style.display='none';
            document.getElementById('lblOpt1').style.display='none'
        }
 
        var optB="optB";
        optB=optB+value;
        div = document.getElementById(optB);
   
        if(div.innerHTML.length>0)
        {
            document.getElementById('lblOpt2').innerHTML=div.innerHTML;
            div.style.display='block';
            document.getElementById('rdOptB').style.display='block';
            document.getElementById('lblOpt2').style.display='block'
        }
        else
        {
            div.style.display='none';
            document.getElementById('rdOptB').style.display='none';
            document.getElementById('lblOpt2').style.display='none'
        }
  
        var optC="optC";
        optC=optC+value;
        div = document.getElementById(optC);
      
        if(div.innerHTML.length>0)
        {
            document.getElementById('lblOpt3').innerHTML=div.innerHTML;
            div.style.display='block';
            document.getElementById('rdOptC').style.display='block';
            document.getElementById('lblOpt3').style.display='block'
        }
        else
        {
            div.style.display='none';
            document.getElementById('rdOptC').style.display='none';
            document.getElementById('lblOpt3').style.display='none'
        }
    
        var optD="optD";
        optD=optD+value;
        div = document.getElementById(optD);
  
        if(div.innerHTML.length>0)
        {
            document.getElementById('lblOpt4').innerHTML=div.innerHTML;
            div.style.display='block';
            document.getElementById('rdOptD').style.display='block';
            document.getElementById('lblOpt4').style.display='block'
        }
        else
        {
            div.style.display='none';
            document.getElementById('rdOptD').style.display='none';
            document.getElementById('lblOpt4').style.display='none'
        }
   
           var optE="optE";
            optE=optE+value;
            div = document.getElementById(optE);
           
        if(div.innerHTML.length>0)
        {
            document.getElementById('lblOpt5').innerHTML=div.innerHTML;
            div.style.display='block';
            document.getElementById('lblOpt5').style.display='block'
            document.getElementById('rdOptE').style.display='block';
        }
        else
        {
            div.style.display='none';
            document.getElementById('lblOpt5').style.display='none';
            document.getElementById('rdOptE').style.display='none';
        }
    
        var infContents="infContents";
        infContents=infContents+value;
        div = document.getElementById(infContents);
        var content=div.innerHTML;
        if(content.length>1)
        {
            document.getElementById('Panel1').style.display='block';
            document.getElementById('lblQueData').innerHTML=content;
            document.getElementById('hrli').style.display='block'; 
        }
        else
        {
            document.getElementById('hrli').style.display='none';
            document.getElementById('Panel1').style.display='none';
        }    
        
        var Ismarked="Ismarked";
        Ismarked=Ismarked+value;
        div = document.getElementById(Ismarked);
       
        if(div.innerHTML=="marked")
        {
            document.getElementById('mark').style.display='none'
            document.getElementById('unmark').style.display='block'
        }
        else
        {
            document.getElementById('unmark').style.display='none'
            document.getElementById('mark').style.display='block';
        }

        var selectedOption="selectedOption";
        selectedOption=selectedOption+value;
        div = document.getElementById(selectedOption);
        
        var option=div.innerHTML;
       
        var questid="questid";
        questid=questid+value;
        div = document.getElementById(questid);
        
        setAns(option);
        setNextPre(value);
        document.getElementById('lblQuestionID').innerHTML=div.innerHTML;   
     
        var sections=document.getElementById('lblNoOfsections').innerHTML;
        if (sections > 1)
        {
            setSection();
        }
        else
        {
            generateButtons();
        }    
     
        var labelID=document.getElementById('lblShowInstructions');
        closepopup('popup2');//it is given because the popuped question list was not closing after click on question in list
        if(labelID.innerHTML=="show")
        {
            labelID.innerHTML='';   
            
            openpopup('popup1');
            getAllQue(); 
        }
    }
    
    function showAlreadySolved()
    {
   
     openpopup('popup5');
    }
    
    function startExams()
    {
        startTimer();
        document.getElementById('Panel6').style.display='block';
        closepopup('popup1');
       
    }
    
    </script>

    <script type="text/javascript">
 //to select proper radio button
function setAns(option)
{
    if(option !=0)
    {
        if(option==1)
        {
        document.getElementById('rdOptA').checked=true;
        }

        if(option==2)
        {
        document.getElementById('rdOptB').checked=true;
        }

        if(option==3)
        {
        document.getElementById('rdOptC').checked=true;
        }

        if(option==4)
        {
        document.getElementById('rdOptD').checked=true;
        }

        if(option==5)
        {
        document.getElementById('rdOptE').checked=true;
        }
   }
   else
   { 
        document.getElementById('rdOptA').checked=false;
        document.getElementById('rdOptB').checked=false;
        document.getElementById('rdOptC').checked=false;
        document.getElementById('rdOptD').checked=false;
        document.getElementById('rdOptE').checked=false;
   }
}
    </script>

    <script type="text/javascript">
    function GetNext()
    {
    var pre=document.getElementById('queID').innerHTML;
    var value=Number(pre)+ Number(1);
    document.getElementById('queID').innerHTML=value;
    Getdivdata(value);
    
    
     var sections=document.getElementById('lblNoOfsections').innerHTML;
     if (sections > 1)
     {
     setSection();
    }
  
    }

    function GetPre()
    {
    var pre=document.getElementById('queID').innerHTML;
    var value=Number(pre)-1;
    document.getElementById('queID').innerHTML=value;
    Getdivdata(value);
    
     var sections=document.getElementById('lblNoOfsections').innerHTML;
     if (sections > 1)
     {
     setSection();
    }
    }

    </script>

    <script language="Javascript" type="text/javascript">
      function saveData()
       {       
        document.getElementById("btnSubmit").click();     
       }
   
 
    </script>

    <script language="javascript" type="text/javascript">
 function GetViewState()
 {
 var value= document.getElementById('__VIEWSTATE').value;
 //alert(value);
 }
    </script>

    <script type="text/javascript">
$(document).ready(function(){
	$(".trigger").click(function(){
		$(".panel123").toggle("fast");
		$(this).toggleClass("active");
		return false;
	});
});
</script>

    <script language="javascript" type="text/javascript">
    
    function updateAnswerSheet(value)
    {
      var queNo=document.getElementById('queID').innerHTML;
      //alert(queNo);
       var Option="Option1"+queNo;
      // alert(Option)
       var div1=document.getElementById(Option);
       
       
       div1.innerHTML='<img src=/images/radio.png  width=12px height=12px />';
       
       Option="Option2"+queNo;
       div1=document.getElementById(Option);
       div1.innerHTML='<img src=/images/radio.png  width=12px height=12px />';
       
       Option="Option3"+queNo;
       div1=document.getElementById(Option);
       div1.innerHTML='<img src=/images/radio.png  width=12px height=12px />';
       
       Option="Option4"+queNo;
       div1=document.getElementById(Option);
       div1.innerHTML='<img src=/images/radio.png  width=12px height=12px />';
       
       
       Option="Option5"+queNo;
       div1=document.getElementById(Option);
       div1.innerHTML='<img src=/images/radio.png  width=12px height=12px />';   
       
     if(value!=0)
     {
       if(value==1)
       {
       
        Option="Option1"+queNo;
        div1=document.getElementById(Option);
        div1.innerHTML='<img src=/images/radioAns.png  width=12px height=12px />';
       }
        if(value==2)
       {
       
        Option="Option2"+queNo;
        div1=document.getElementById(Option);
        div1.innerHTML='<img src=/images/radioAns.png  width=12px height=12px />';
       }
        if(value==3)
       {
       
        Option="Option3"+queNo;
        div1=document.getElementById(Option);
        div1.innerHTML='<img src=/images/radioAns.png  width=12px height=12px />';
       }
        if(value==4)
       {
       
        Option="Option4"+queNo;
        div1=document.getElementById(Option);
        div1.innerHTML='<img src=/images/radioAns.png  width=12px height=12px />';
       }
        if(value==5)
       {
       
        Option="Option5"+queNo;
        div1=document.getElementById(Option);
        div1.innerHTML='<img src=/images/radioAns.png  width=12px height=12px />';
       }
      }
     }
    </script>

    <style type="text/css">
        .hLinkNew
        {
            background-color: #6666FF;
            font-family: 'Times New Roman' , Times, serif;
            color: #FFFFFF;
            font-weight: bold;
            border: solid 1px black;
            width: auto;
            padding: 5px;
            float: left;
        }
        .OhLink
        {
            font-family: 'Times New Roman' , Times, serif;
            color: #800000;
            font-weight: bold;
            width: auto;
        }
        .OtimerLink
        {
            font-family: 'Times New Roman' , Times, serif;
            color: #800000;
            width: auto;
            font-size: small;
        }
        .btn
        {
            float: left;
        }
        .sections
        {
            float: left;
        }
        .btn1
        {
            background-color: whitesmoke;
            color: Black;
            width: 40px;
            margin: 2px;
        }
        .markedBtn
        {
            background-color: Red;
            color: White;
            width: 40px;
            margin: 2px;
        }
        .unmarkedBtn
        {
            background-color: Green;
            color: White;
            width: 40px;
            margin: 2px;
        }
        .selectedbtn
        {
            background-color: Blue;
            color: White;
            width: 40px;
            margin: 2px;
        }
        .answeredBtn
        {
            background-color: Green;
            color: White;
            width: 40px;
            margin: 2px;
        }
    </style>

    <script type="text/javascript" language="javascript">
    
    function GetSectionQue(section,idValue)
    {
 // alert('start GetSectionQue');
 //alert(section);
      var fromQue="";
      var toQue="";
      var sectionQues="";
     
      
      if(section==1)
      {
  
     sectionQues = document.getElementById('lblSection1Ques').innerHTML;

      }
      if(section==2)
      {

 sectionQues = document.getElementById('lblSection2Ques').innerHTML;
      }
       if (section == 3)
      {

 sectionQues = document.getElementById('lblSection3Ques').innerHTML;
      }
      if (section == 4)
      {

 sectionQues = document.getElementById('lblSection4Ques').innerHTML;
      }
        if (section == 5)
      {

 sectionQues = document.getElementById('lblSection5Ques').innerHTML;
       }
       
       var n=sectionQues.split(",");
       fromQue=n[0];
       toQue=n[1];
       //alert(fromQue+','+toQue);
       
      var butttons="<div id='button' class='btn'>";
      
      
      for( var i = parseInt(fromQue);i<= parseInt(toQue); i++)
      {   
      //alert(i);
           var selectedOption="selectedOption"+i;
           var option=document.getElementById(selectedOption).innerHTML;
           var selectedQue=document.getElementById('queID').innerHTML;
           var Ismarked="Ismarked"+i;
           var marked=document.getElementById(Ismarked).innerHTML;
           var btnClass="";
           if(marked=="marked")
           {btnClass="markedBtn";}
           if(marked=="unmarked")
           {btnClass="btn1";}
           if(option!=0)
           {btnClass="answeredBtn";}
           if(i==selectedQue)
           {btnClass="selectedbtn";}
  
          if(butttons=="")
          {     
          butttons = "<input type='button' class='"+btnClass+"' id='btn" + (i) + "' value='" + i + "' onclick='JavaScript:Getdivdata(this.value)'/>";
          }
          else
          {
          butttons = butttons + "<input id='btn" + (i) + "' class='"+btnClass+"' type='button' value='" +i + "' onclick='JavaScript:Getdivdata(this.value)' />";
          }

      }
      
      butttons=butttons+"</div>";
     
      document.getElementById('lblButtons').innerHTML=butttons;
    selectSection(section);
    //alert('end GetSectionQue');
    if(idValue==1)
    {
     Getdivdata(fromQue);}
    }
    
    </script>

    <script type="text/javascript">
    
    function generateButtons()
    {
      var totalQuestions = document.getElementById('lblMaxQue').innerHTML;
      var butttons="<div id='button' class='btn'>";
      for(var i=1;i<=parseInt(totalQuestions);i++)
      {
           var selectedOption="selectedOption"+i;
           var option=document.getElementById(selectedOption).innerHTML;
           var selectedQue=document.getElementById('queID').innerHTML;
           var Ismarked="Ismarked"+i;
           var marked=document.getElementById(Ismarked).innerHTML;
           var btnClass="";
           
           if(marked=="marked")
           {btnClass="markedBtn";}
           if(marked=="unmarked")
           {btnClass="btn1";}
           if(option!=0)
           {btnClass="answeredBtn";}
           if(i==selectedQue)
           {btnClass="selectedbtn";}
           
           
          
           
           
          if(butttons=="")
          {     
          butttons = "<input type='button' class='"+btnClass+"' id='btn" + (i) + "' value='" + i + "' onclick='JavaScript:Getdivdata(this.value)'/>";
          }
          else
          {
          butttons = butttons + "<input id='btn" + (i) + "' class='"+btnClass+"' type='button' value='" +i + "' onclick='JavaScript:Getdivdata(this.value)' />";
          }
          
      }
      butttons=butttons+"</div>";
      //alert(butttons);
      document.getElementById('lblButtons').innerHTML=butttons;
    
    }
    </script>

    <script type="text/javascript" language="javascript">
    function setSection()
    {
    
    var sections=document.getElementById('lblNoOfsections').innerHTML;
    
    if (parseInt(sections) > 1)
     {
        var newQue= document.getElementById('queID').innerHTML;
  
        var sec1Max="";var sec2Max="";var sec3Max="";var sec4Max=""; var sec5Max="";
        var secFound="No";
      
        sec1Max = document.getElementById('lblSection1Max').innerHTML;
        sec2Max= document.getElementById('lblSection2Max').innerHTML;
        sec3Max= document.getElementById('lblSection3Max').innerHTML;
        sec4Max= document.getElementById('lblSection4Max').innerHTML;
        sec5Max= document.getElementById('lblSection5Max').innerHTML;
        //alert(newQue);
        //alert(newQue +"<="+sec1Max);
        
        if(parseInt(newQue) <= parseInt( sec1Max))
        {
        secFound="1";
        }
        else
        {
        if(parseInt(newQue) <= parseInt(sec2Max))
        {
        secFound="2";
        //alert("newQue:"+newQue+"Sec1Max:"+sec1Max+"sec2Max:"+sec2Max);
        }
        else
        {        
        if(parseInt(newQue) <=parseInt(sec3Max))
        {
        secFound="3";
        }
        else
        {
        if(parseInt(newQue) <= parseInt(sec4Max))
        {
        secFound="4";
        }
        else
        {
        if(parseInt(newQue )<= parseInt(sec5Max))
        {
        secFound="5";
        }
        }
        }
        }
        }
        
        //alert(secFound);
           
       GetSectionQue(secFound,0);
        selectSection(secFound);
       
      }
    }
    </script>

    <script type="text/javascript" language="javascript">
    
    
    function selectSection(secFound)
    {
    //alert('start Select section');
      var sections=document.getElementById('lblNoOfsections').innerHTML;
      if ( parseInt(sections) > 1)
      {
       for(var i=1;i<= parseInt(sections);i++)
        {
          var section="Section"+i;
          if(i==  parseInt(secFound))
          {
          var divtoChange=document.getElementById(section);
          divtoChange.className = "selectedbtn";
          }
          else
          {
          var divtoChange=document.getElementById(section);
          divtoChange.className = "btn1";
          }
        }
      }
      // alert('end Select section');
    }
    
    
    
    </script>

    <script language="javascript" type="text/javascript"> 
function openpopup(id){
      //Calculate Page width and height 
//      var pageWidth = window.innerWidth; 
//      var pageHeight = window.innerHeight; 
//      if (typeof pageWidth != "number"){ 
//      if (document.compatMode == "CSS1Compat"){ 
//            pageWidth = document.documentElement.clientWidth; 
//            pageHeight = document.documentElement.clientHeight; 
//      } else { 
//            pageWidth = document.body.clientWidth; 
//            pageHeight = document.body.clientHeight; 
//      } 
//      } 
      //Make the background div tag visible...
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "visible"; 
        
      var divobj = document.getElementById(id); 
      //divobj.style.visibility = "visible"; 
      divobj.style.display='block';
      if (navigator.appName=="Microsoft Internet Explorer") 
      computedStyle = divobj.currentStyle; 
      else computedStyle = document.defaultView.getComputedStyle(divobj, null); 
      //Get Div width and height from StyleSheet 
      var divWidth = computedStyle.width.replace('px', ''); 
      var divHeight = computedStyle.height.replace('px', ''); 
      var divLeft = (pageWidth - divWidth) / 2; 
      var divTop = (pageHeight - divHeight) / 2; 
      //Set Left and top coordinates for the div tag 
      divobj.style.left = divLeft + "px"; 
      divobj.style.top = divTop + "px"; 
      //Put a Close button for closing the popped up Div tag 
      if(divobj.innerHTML.indexOf("closepopup('" + id +"')") < 0 ) 
      divobj.innerHTML = "<a href=\"#\" onclick=\"closepopup('" + id +"')\"><span class=\"close_button\">X</span></a>" + divobj.innerHTML; 
}
function closepopup(id){
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "hidden"; 
      var divobj = document.getElementById(id); 
divobj.style.display='none';
}
    </script>

    <style type="text/css">
        .popup
        {
            background-color: #DDD;
            height: 500px;
            width: 700px;
            border: 5px solid #666;
            position: fixed;
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            text-align: justify;
            padding: 5px;
            overflow: auto;
            z-index: 20000;
            display: none;
        }
        #popup1, #popup2, #popup3, #popup4, #popup5, #popup6, #popup7, #popup14
        {
            background-color: #DDD;
            height: 500px;
            width: 900px;
            border: 5px solid #666;
            position: fixed;
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            text-align: justify;
            padding: 5px;
            overflow: auto;
            z-index: 20000;
            display: none;
            left: 100px;
            top: 100px;
        }
        .popup_bg
        {
            position: absolute;
            visibility: hidden;
            height: 100%;
            width: 100%;
            left: 0px;
            top: 0px;
            filter: alpha(opacity=100); /* for IE */
            opacity: 1.9; /* CSS3 standard */
            background-color: #999;
            z-index: 1;
            display: block;
        }
        .close_button
        {
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            font-weight: bold;
            float: left;
            color: #666;
            display: block;
            text-decoration: none;
            border: 2px solid #666;
            padding: 0px 3px 0px 3px;
        }
        a.simplebtn
        {
            padding: 2px 10px 2px 10px;
            border: #143857 solid 1px;
            color: #fff;
            cursor: pointer;
            background-color: #6666FF;
            display: inline-block;
            height: 16px;
        }
        a.simplebtn:hover
        {
            color: #fff;
            background-color: #6666FF;
            background-image: none;
        }
        input.simplebtn
        {
            padding: 0px 10px 2px 10px;
            border: #143857 solid 1px;
            color: #fff;
            cursor: pointer;
            background-color: #6666FF;
            display: inline-block;
            height: 25px;
        }
        input.simplebtn:hover
        {
            color: #fff;
            background-color: #6666FF;
            background-image: none;
        }
    </style>

    <script type="text/javascript">
    // Timer library 1.0
function _timer(callback)
{
    var time = 0;   //  The default time of the timer
    var mode = 1;   //  Mode: count up or count down
    var status = 0; //  Status: timer is running or stoped
    var timer_id;   //  This is used by setInterval function
     
    // this will start the timer ex. start the timer with 1 second interval timer.start(1000) 
    this.start = function(interval)
    {
        interval = (typeof(interval) !== 'undefined') ? interval : 1000;
 
        if(status == 0)
        {
            status = 1;
            timer_id = setInterval(function()
            {
                switch(mode)
                {
                    default:
                    if(time)
                    {
                        time--;
                        generateTime();
                        if(typeof(callback) === 'function') callback(time);
                    }
                    break;
                     
                    case 1:
                    if(time < 86400)
                    {
                        time++;
                        generateTime();
                        if(typeof(callback) === 'function') callback(time);
                    }
                    break;
                }
            }, interval);
        }
    }
     
    //  Same as the name, this will stop or pause the timer ex. timer.stop()
    this.stop =  function()
    {
        if(status == 1)
        {
            status = 0;
            clearInterval(timer_id);
        }
    }
     
    // Reset the timer to zero or reset it to your own custom time ex. reset to zero second timer.reset(0)
    this.reset =  function(sec)
    {
        sec = (typeof(sec) !== 'undefined') ? sec : 0;
        time = sec;
        generateTime(time);
    }
     
    // Change the mode of the timer, count-up (1) or countdown (0)
    this.mode = function(tmode)
    {
        mode = tmode;
    }
     
    // This methode return the current value of the timer
    this.getTime = function()
    {
        return time;
    }
     
    // This methode return the current mode of the timer count-up (1) or countdown (0)
    this.getMode = function()
    {
        return mode;
    }
     
    // This methode return the status of the timer running (1) or stoped (1)
    this.getStatus
    {
        return status;
    }
     
    // This methode will render the time variable to hour:minute:second format
    function generateTime()
    {
        var second = time % 60;
        var minute = Math.floor(time / 60) % 60;
        var hour = Math.floor(time / 3600) % 60;
         
        second = (second < 10) ? '0'+second : second;
        minute = (minute < 10) ? '0'+minute : minute;
        hour = (hour < 10) ? '0'+hour : hour;
         
        $('div.timer span.second').html(second);
        $('div.timer span.minute').html(minute);
        $('div.timer span.hour').html(hour);
    }
}
 
// example use
var timer;
 
$(document).ready(function(e) 
{
    timer = new _timer
    (
        function(time)
        {
            if(time == 0)
            {
                timer.stop();
                //alert('time out');
                 //document.getElementById("Button1").click(); 
                // document.getElementById('Panel6').style.display='none'; 
                
                
              // alert('examFinished');Panel6
              
                document.getElementById('cancleFinish').style.display='none';
                document.getElementById('divFeedBack').style.display='block';
                 document.getElementById('finishExam').style.display='none';
                 
                openpopup('popup3');
                //finish();
            }
        }
    );
    timer.reset(60);
    timer.mode(0);
});
    </script>

    <script type="text/javascript" language="javascript">
  function closeWindow()
        {
        window.close();
        }
    </script>

    <style type="text/css">
        /* Lock Screen-related CSS Classes */.LockOff
        {
            visibility: hidden;
            display: none;
            position: absolute;
            top: -100px;
            left: -100px;
        }
        .LockBackground
        {
            position: absolute;
            top: 0px;
            left: 0px;
            visibility: visible;
            display: block;
            width: 105%;
            height: 105%;
            background-color: #666;
            z-index: 999;
            filter: alpha(opacity=75);
            opacity: 0.75;
            padding-top: 20%;
        }
        .LockPane
        {
            z-index: 1000;
            position: absolute;
            top: 0px;
            left: 0px;
            padding-top: 25%;
            visibility: visible;
            display: block;
            text-align: center;
            border-radius: 10px;
            width: 100%;
            vertical-align: top;
        }
        .LockPane div
        {
            border-radius: 10px;
            background-color: #FFFFFF;
            color: #000000;
            font-size: large;
            border: dotted 1px White;
            padding: 9px;
            margin-left: auto;
            margin-right: auto;
            width: 5%;
            text-align: center;
        }
        .StatusMessage
        {
            color: Red;
            font-size: 2.0em;
            font-weight: bold;
        }
        div.timer
        {
            border: 1px #666666 solid;
            width: 100px;
            height: 30px;
            line-height: 30px;
            font-size: 20px;
            font-family: 'Comic Sans MS';
            text-align: center;
            margin: 2px;
        }
    </style>

    <script type="text/javascript">
    
   function startTimer()
   {
   
   var sTime=document.getElementById('lblTime').innerHTML;
   var nTime=Number(sTime)/Number(1000);
   //alert(nTime);
   timer.reset(nTime);
   document.getElementById('lblPreTime').innerHTML=sTime;
   timer.start(1000)
   }
    
//    function finishExam()
//    {
//     document.getElementById("Button1").click();     
//                
//    }
    
    </script>

    <script type="text/javascript">
    
    function hideTimer()
    {
    document.getElementById('hideTimer').style.display='none';
    document.getElementById('showTimer').style.display='block';
    document.getElementById('timer1').style.display='none';
    }
    
    
    function showTimer()
    {
    document.getElementById('hideTimer').style.display='block';
    document.getElementById('showTimer').style.display='none';
    document.getElementById('timer1').style.display='block';
    }
    
    
  
    
    
    
  
    </script>

    <script type="text/javascript">
          
      function finish1()
    {  
    var status = navigator.onLine;
    if (status) 
    { 
    
       var divID=document.getElementById('answeredQue');
       var AnsweredQue = divID.innerHTML;
        //       divID=document.getElementById('markedQue');
       //       var markedQue = divID.innerHTML;
      //       
      //        divID=document.getElementById('unmarkedQue');
     //       var unmarkedQue =divID.innerHTML;
       var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
       var xmlhttp;
         if (window.XMLHttpRequest)
         {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
         }
         else
         {// code for IE6, IE5
           xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         }
        xmlhttp.onreadystatechange=function()
        {
         if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
             document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
             }
        }
        //var xxxxxxx="saveexamstate.aspx?answered="+AnsweredQue+"&marked="+markedQue+"&act=finishExam&unmarked="+unmarkedQue+"&spentTime=0&examID="+examID+"&uid="+uid+"";
        //alert(xxxxxxx);="+AnsweredQue+"&
       //xmlhttp.open("GET","saveexamstate.aspx?act=finishExam&examID="+examID+"&uid="+uid+"&answered="+AnsweredQue+"",true);
       
       xmlhttp.open("GET","saveexamstate.aspx?uid="+uid+"&act=finishExam&examID="+examID+"&answered="+AnsweredQue+"",true);
        xmlhttp.send();//"saveexamstate.aspx?uid="+uid+"&act=finishExam&examID="+examID+"&answered="+AnsweredQue+""
         location.href = '<%= Page.ResolveUrl("~/exmResult.aspx") %>';
    }
    else
    {
    alert('No internet connection!');
    }
    }
    
    
    
    </script>

    <style type="text/css">
        table.gridtable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #FFFFFF;
            border-collapse: collapse; /*background-image: url(../images/botm.gif);*/
            width: 100%;
        }
        table.gridtable th
        {
            border-width: 0px;
            padding: 0px; /*background: #666 url(../images/breadcrumb_bg.gif) repeat-x top;*/
            text-align: center;
        }
        table.gridtable td
        {
            border-width: 1px;
            padding: 0px;
            border-style: solid;
            border-color: #FFFFFF;
            color: #FFFFFF;
        }
        table.gridtable1
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-collapse: collapse;
            background-color: White;
        }
        table.gridtable1 th
        {
            border-width: 0px;
            padding: 0px;
            background: #666 url(../images/breadcrumb_bg.gif) repeat-x top;
            text-align: center;
        }
        table.gridtable1 td
        {
            border-width: 1px;
            padding: 0px;
            border-style: solid;
            border-color: Black;
            color: #FFFFFF;
        }
    </style>
    <style type="text/css">
        table.hovertable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
        }
        table.hovertable th
        {
            background-color: #c3dde0;
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
        }
        table.hovertable tr
        {
            background-color: #d4e3e5;
        }
        table.hovertable td
        {
            border-width: 1px;
            padding: 3px;
            border-style: solid;
            border-color: #a9c6c9;
        }
        .anshyperLink
        {
            text-decoration: none;
            color: Black;
        }
        .anshyperLink:hover
        {
            color: White;
        }
    </style>
    <%-- <script language="JavaScript"> var message = 'Right Click is disabled'; 
function clickIE() { if (event.button == 2) { alert(message); return false; } } 
function clickNS(e) { 
if (document.layers || (document.getElementById && !document.all)) { 
if (e.which == 2 || e.which == 3) { alert(message); return false; } 
} 
} 
if (document.layers) { document.captureEvents(Event.MOUSEDOWN); document.onmousedown = clickNS; } 
else if (document.all && !document.getElementById) { document.onmousedown = clickIE; } 
document.oncontextmenu = new Function('alert(message);return false') 



    </script>--%>

    <script type="text/javascript">

function getAllQue()
{
 var totalQuestions = document.getElementById('lblMaxQue').innerHTML;
 //alert(totalQuestions);
 var AllQue="<table>";
for (var i=1;i<=totalQuestions;i++)
{
var question="que"+i;
var que=document.getElementById(question).innerHTML;
if(AllQue=="")
{
 AllQue="<tr><td align='center' valign='top'  style='width:25px'>"+i+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";
}
else{
AllQue=AllQue+"<tr><td align='center' valign='top'  style='width:25px'>"+i+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";
}
AllQue= AllQue+"<tr><td colspan='2'><hr style='border-bottom: dotted 1px blue; background-color: white' /></td></tr>";

}
AllQue=AllQue+"</table>";
//alert(AllQue);

document.getElementById('lblQuestions').innerHTML=AllQue;
//var btnToChange="btn"+queNo;
//alert(btnToChange);selectedbtn
     var divtoChange=document.getElementById('allQuestions');
     divtoChange.className = "selectedbtn";
     
     divtoChange =  document.getElementById('AnsQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('UnansweredQue');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('MarkedQue');
     divtoChange.className = "btn1";

document.getElementById('lblTotalQues').innerHTML="Total Questions : "+totalQuestions+"";
}




function getAnsweredQue()
{
 var totalQuestions = document.getElementById('lblMaxQue').innerHTML;
 //alert(totalQuestions);
 var j=1;
 var AllQue="<table>";
   for (var i=1;i<=totalQuestions;i++)
   {
   var question="que"+i;//selectedOption
   var que=document.getElementById(question).innerHTML;
   var selectedOption="selectedOption"+i;
   var options=document.getElementById(selectedOption).innerHTML;
   //alert(options);
   
    if(options!=0)
    {
      if(AllQue=="")
      {
        AllQue="<tr><td align='center' valign='top' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";  
       
      }
      else
      {
       AllQue=AllQue+"<tr><td valign='top' align='center' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";
      
      }
      AllQue=AllQue+"<tr><td colspan='2'><hr style='border-bottom: dotted 1px blue; background-color: white' /></td></tr>";
      j++;
    }

   }
  AllQue=AllQue+"</table>";
  //alert(AllQue);
  document.getElementById('lblQuestions').innerHTML=AllQue;
  
     var divtoChange=document.getElementById('allQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('AnsQuestions');
     divtoChange.className = "selectedbtn";
     
     divtoChange =  document.getElementById('UnansweredQue');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('MarkedQue');
     divtoChange.className = "btn1";
     
     document.getElementById('lblTotalQues').innerHTML="Answered Questions : "+(j-1)+"";
  }





function getUnAnsweredQue()
{
 var totalQuestions = document.getElementById('lblMaxQue').innerHTML;
 //alert(totalQuestions);
 var j=1;
 var AllQue="<table>";
   for (var i=1;i<=totalQuestions;i++)
   {
   var question="que"+i;//selectedOption
   var que=document.getElementById(question).innerHTML;
   var selectedOption="selectedOption"+i;
   var options=document.getElementById(selectedOption).innerHTML;
   //alert(options);
   
    if(options==0)
    {
      if(AllQue=="")
      {
        AllQue="<tr><td align='center' valign='top' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";  
       
      }
      else
      {
       AllQue=AllQue+"<tr><td valign='top' align='center' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";
      
      }
      AllQue=AllQue+"<tr><td colspan='2'><hr style='border-bottom: dotted 1px blue; background-color: white' /></td></tr>";
      j++;
    }

   }
  AllQue=AllQue+"</table>";
  //alert(AllQue);
  document.getElementById('lblQuestions').innerHTML=AllQue;
  
  
     var divtoChange=document.getElementById('allQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('AnsQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('UnansweredQue');
     divtoChange.className = "selectedbtn";
     
     divtoChange =  document.getElementById('MarkedQue');
     divtoChange.className = "btn1";
     document.getElementById('lblTotalQues').innerHTML="Unanswered Questions : "+(j-1)+"";
  }


function getMarkedQue()
{

var totalQuestions = document.getElementById('lblMaxQue').innerHTML;
 //alert(totalQuestions);
 var j=1;
 var AllQue="<table>";
   for (var i=1;i<=totalQuestions;i++)
   {
   var question="que"+i;//selectedOption
   var que=document.getElementById(question).innerHTML;
   var Ismarked="Ismarked"+i;
   var options=document.getElementById(Ismarked).innerHTML;
   //alert(options);
   
    if(options=="marked")
    {
      if(AllQue=="")
      {
        AllQue="<tr><td align='center' valign='top' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";  
       
      }
      else
      {
       AllQue=AllQue+"<tr><td valign='top' align='center' style='width:25px'>"+j+"</td><td><a onclick='closepopup('popup2')' href='JavaScript:Getdivdata("+i+")'>"+que+"</a></td></tr>";
      
      }
      AllQue=AllQue+"<tr><td colspan='2'><hr style='border-bottom: dotted 1px blue; background-color: white' /></td></tr>";
      j++;
    }

   }
  AllQue=AllQue+"</table>";
  //alert(AllQue);
  document.getElementById('lblQuestions').innerHTML=AllQue;
  
   var divtoChange=document.getElementById('allQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('AnsQuestions');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('UnansweredQue');
     divtoChange.className = "btn1";
     
     divtoChange =  document.getElementById('MarkedQue');
     divtoChange.className = "selectedbtn";
     document.getElementById('lblTotalQues').innerHTML="Marked Questions : "+(j-1)+"";
}

    </script>

    <script type="text/javascript">
function checkconnection() {
var status = navigator.onLine;
if (status) {
return true;} else {
alert("Internet connection is lost..."); return false;
}
}
    </script>

    <style type="text/css">
        .protected
        {
            -moz-user-select: none;
            -webkit-user-select: none;
            user-select: none;
            selection: none;
        }
    </style>

    <script type="text/javascript">
         
         function CancelFinish()
         {
         closepopup('popup3');
  
         timer.start(1000);
       
         }
         
         
         
         
         
         
         
         
         
          
      function finish()
    {  
    
   
    
    var status = navigator.onLine;
    if (status) 
    { 
    
       var divID=document.getElementById('answeredQue');
       var AnsweredQue = divID.innerHTML;
       var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
       
       var xmlhttp;
       if (window.XMLHttpRequest)
       {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
        }
       else
        {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
       }
        xmlhttp.open("GET","saveexamstate.aspx?uid="+uid+"&act=finishExam&spentTime=0&examID="+examID+"&answered="+AnsweredQue+"",true);
        xmlhttp.send();
    var divid=  document.getElementById('finishExam');
    divid.style.display='none';
    divid= document.getElementById('divFeedBack');
    divid.style.display='block';
    }
    else
    {
    alert('No internet connection!');
    }
    }
    
    
    
    </script>

    <script type="text/javascript">
          
      function AddFeedback()
    {  
     var divID=document.getElementById('showAlertMsg');
       divID.innerHTML = "No";
    var examID=document.getElementById('lblExamID').innerHTML;
       var uid=document.getElementById('lblUserID').innerHTML;
    
    var status = navigator.onLine;
   //alert(status);
    if (status) 
    { 
    
    var xyz=document.getElementById("txtFeedBack");
    
     var feedback = xyz.value;//alert(feedback);
   //alert(feedback.length);
    if(feedback.length >0)
    {
       var xmlhttp;
       if (window.XMLHttpRequest)
       {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
        }
       else
        {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function()
        {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
       }
        xmlhttp.open("GET","saveexamstate.aspx?uid="+uid+"&act=addFeedback&spentTime=0&examID="+examID+"&answered="+feedback+"",true);
        xmlhttp.send();
        
         var divid=  document.getElementById('divFeedBack');
    divid.style.display='none';
    divid= document.getElementById('divResult');
    divid.style.display='block';
    
    
    }
    else{
     alert('Give your valuable feedback !');
    }
    }
    else
    {
    alert('No internet connection!');
    }
    }
    
   function showResult()
    
    {
    
     location.href = '<%= Page.ResolveUrl("~/exmResult.aspx") %>';
    }
    
    function showWait()
    {
     $get('UpdateProgress1').style.display = 'block';
    }
    function endWait()
    {
     $get('UpdateProgress1').style.display = 'none';
    }
    </script>

</head>
<body onkeypress="javascript:onKeyDown(this.event)">
    <form id="form1" runat="server">
    <asp:Label ID="tempLabel" runat="server" Visible="false" Text="Label"></asp:Label>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="display: none">
                <asp:Label ID="lblSection1Ques" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection2Ques" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection3Ques" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection4Ques" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection5Ques" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection1Max" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection2Max" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection3Max" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection4Max" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblSection5Max" runat="server" Text="Label"></asp:Label></div>
            <span class="protected">
                <asp:Label ID="lblErrorMessage" runat="server" Text=""></asp:Label>
                <span id="timerDisplaySpan"></span>
                <input type="hidden" id="timeAllocated" name="timeAllocated" value="30" runat="server" />
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                <div style="display: none">
                    <div id="showInternetLostMsg">
                        Yes</div>
                    <div id="showAlertMsg">
                        Yes</div>
                    <div id="answeredQue">
                    </div>
                    <div id="markedQue1">
                    </div>
                    <asp:Label ID="lblSelectedQue" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblPreTime" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblTime" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblRes" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblMaxQue" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="queID" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblQuestionID" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblUserID" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblExamID" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblquestionsInSection" runat="server" Text="Label"></asp:Label>
                    <input type="text" id="txtHint" />
                    <asp:Label ID="lblNoOfsections" runat="server" Text="Label"></asp:Label>
                </div>
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <%-- HiddenField2 : storing ans when internet connection lost--%>
                <asp:HiddenField ID="HiddenField3" runat="server" />
                <asp:HiddenField ID="HiddenField4" runat="server" />
                <%-- HiddenField4 : storing marked when internet connection lost--%>
                <asp:HiddenField ID="HiddenField5" runat="server" />
                <%-- HiddenField5 : storing unmarked when internet connection lost--%>
                <asp:Panel ID="Panel6" runat="server" Style="display: none">
                    <table width="100%">
                        <tr>
                            <td align="left" colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                        </td>
                                        <td rowspan="2" align="right" valign="top">
                                            <table>
                                                <tr>
                                                    <td valign="baseline">
                                                        <a href="#" onclick="openpopup('popup14')" class="OhLink">If net connection goes off..</a>
                                                        <span>|</span>
                                                    </td>
                                                    <td valign="baseline">
                                                        <a href="JavaScript:timer.stop()" onclick="openpopup('popup4')" class="OhLink">Pause
                                                            Exam</a> <span>|</span>
                                                    </td>
                                                    <td valign="baseline">
                                                        <a href="#" onclick="openpopup('popup7')" class="OhLink">Save for Later</a> <span>|</span>
                                                    </td>
                                                    <td valign="baseline">
                                                        <a href="JavaScript:timer.stop()" onclick="openpopup('popup3')" class="OhLink">Finish
                                                            Exam</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="right" style="padding-right: 100px">
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <a href="#" id="hideTimer" onclick="JavaScript:hideTimer()" class="OtimerLink">Hide
                                                                        Timer</a> <a href="#" id="showTimer" onclick="JavaScript:showTimer()" class="OtimerLink"
                                                                            style="display: none">
                                                                            <img src="images/clockTime.jpg" alt="Show" width="15px" height="15px" />Show Timer</a>
                                                                </td>
                                                                <td align="left">
                                                                    <div class="timer" id='timer1'>
                                                                        <span class="hour" id="hourLeft">00</span>:<span class="minute" id="minLeft">00</span>:<span
                                                                            class="second" id="secLeft">00</span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblExamName" runat="server" Text="Label" Font-Bold="true"></asp:Label><br />
                                            <a href="#" onclick="openpopup('popup6')" class="OhLink">Directions and Instructions</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>--%>
                                <table width="100%">
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:Panel ID="Panel3" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblSectionsLabel" runat="server" Text="Sections : "></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Repeater ID="repSections" runat="server">
                                                                <ItemTemplate>
                                                                    <div style="float: left; width: 45px">
                                                                        <input type="button" value='<%# Eval("number") %>' name='<%# Eval("serialNo") %>'
                                                                            onclick="JavaScript:GetSectionQue(this.name,1)" style="width: 35px" id='<%#Eval("SectioNo") %>' />
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <asp:Label ID="lblShowInstructions" runat="server" Text="Label"></asp:Label>
                                        <td align="left" colspan="2">
                                            <asp:Repeater ID="Repeater2" runat="server" Visible="false">
                                                <ItemTemplate>
                                                    <div style="float: left; width: 45px">
                                                        <input type="button" id="btnClick" value='<%# Eval("serialnumber") %>' onclick="JavaScript:Getdivdata(this.value)" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblButtons" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:Panel ID="Panel4" runat="server" Height="350">
                                                <div id="questions">
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:Panel ID="Panel1" runat="server" Height="340px" ScrollBars="Auto">
                                                                    <asp:HyperLink ID="hrli" runat="server" Style="color: Black; text-decoration: none;
                                                                        cursor: pointer; display: none">
                                                                        <asp:Image ID="Image1" runat="server" Width="10px" Height="10px" />
                                                                        &nbsp; Common Data</asp:HyperLink>
                                                                    <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="Server" TargetControlID="Panel11"
                                                                        CollapsedSize="2" Collapsed="False" ExpandControlID="hrli" CollapseControlID="hrli"
                                                                        AutoCollapse="False" AutoExpand="False" ImageControlID="Image1" ExpandedImage="/images/arrow-up.gif"
                                                                        CollapsedImage="/images/arrow-dn.gif" ExpandDirection="Vertical" />
                                                                    <asp:Panel ID="Panel11" runat="server">
                                                                        <label id="lblQueData">
                                                                        </label>
                                                                    </asp:Panel>
                                                                </asp:Panel>
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <div style="width: 99%; min-width: 600px; float: left; vertical-align: top">
                                                                    <asp:Panel ID="Panel5" runat="server" Height="340px" ScrollBars="Auto">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="left" valign="top" colspan="2">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="width: 8px" align="left" valign="top">
                                                                                                Q
                                                                                            </td>
                                                                                            <td style="width: 25px" align="left" valign="top">
                                                                                                <div id="no">
                                                                                                </div>
                                                                                            </td>
                                                                                            <td align="left" valign="top">
                                                                                                <label id="lblQue">
                                                                                                </label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <input type="button" value="M" id='mark' onclick="JavaScript:markQue()" />
                                                                                                <input type="button" value="U" id='unmark' onclick="JavaScript:unmarkQue()" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 10px">
                                                                                    <input id="rdOptA" name="answer" type="radio" value="1" onclick="saveResult(this.value)" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <label id="lblOpt1">
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 10px">
                                                                                    <input id="rdOptB" type="radio" name="answer" value="2" onclick="saveResult(this.value)" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <label id="lblOpt2">
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 10px">
                                                                                    <input id="rdOptC" type="radio" name="answer" value="3" onclick="saveResult(this.value)" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <label id="lblOpt3">
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 10px">
                                                                                    <input id="rdOptD" type="radio" name="answer" value="4" onclick="saveResult(this.value)" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <label id="lblOpt4">
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 10px">
                                                                                    <input id="rdOptE" type="radio" name="answer" value="5" onclick="saveResult(this.value)" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <label id="lblOpt5">
                                                                                    </label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <div id='clearAnswer' style="display: none">
                                                                                    <td align="left" style="width: 10px">
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        <input type="button" id="Button3" value="Clear Answer" onclick="saveResult(0)" class="hLinkNew" />
                                                                                    </td>
                                                                                </div>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <a href="#" onclick="openpopup('popup2')" class="hLinkNew">Question List</a>
                                        </td>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <%--  <input type="button" id="Button2" value="updateanswer" onclick="updateAnswerSheet(2)" />--%>
                                                        <%-- <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />--%>
                                                    </td>
                                                    <td align="left">
                                                        <input type="button" id="btnNext" value="Next" onclick="JavaScript:GetNext()" class="hLinkNew" />
                                                    </td>
                                                    <td align="left">
                                                        <input type="button" id="btnPre" value="Previous" onclick="JavaScript:GetPre()" class="hLinkNew" />
                                                        <%-- <input type="button" id="Button1" value="View state value" onclick="JavaScript:GetViewState()" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div class="panel123">
                                    <p>
                                        &nbsp;<asp:Panel ID="Panel2" runat="server" Height="400px" ScrollBars="Auto">
                                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                                        </asp:Panel>
                                        <p>
                                        </p>
                                        <p>
                                        </p>
                                        <p>
                                        </p>
                                    </p>
                                </div>
                                <a class="trigger" href="#">Answer Sheet</a>
                                <div id="popup2" class="popup">
                                    <table style="background-color: White; width: 100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="Label5" runat="server" Text="Questions"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTotalQues" runat="server" Text="Label" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <input type="button" id="allQuestions" onclick="javascript:getAllQue()" value="All" />
                                                <input type="button" id="AnsQuestions" onclick="javascript:getAnsweredQue()" value="Answered" />
                                                <input type="button" id="UnansweredQue" onclick="javascript:getUnAnsweredQue()" value="Unsanswered" />
                                                <input type="button" id="MarkedQue" onclick="javascript:getMarkedQue()" value="Marked" />
                                                <input type="button" id="Button1" onclick="javascript:closepopup('popup2')" value="Close" />
                                                <%--  <a onclick="closepopup('popup2')" href="#">Close</a>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblQuestions" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="popup3" class="popup">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <div id="finishExam">
                                                    <table>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                Are you sure you want to finish exam?
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <input type="button" onclick="javascript:finish()" value="Proceed" class="simplebtn" />
                                                            </td>
                                                            <td align="center">
                                                                <input type="button" onclick="javascript:CancelFinish()" class="simplebtn" value="Cancel"
                                                                    id="cancleFinish" />
                                                                <input type="button" style="display: none" id="Button2" onclick="javascript:closepopup('popup3')"
                                                                    value="Close" class="simplebtn" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divFeedBack" style="display: none">
                                                    <table>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                Feedback
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtFeedBack" runat="server" TextMode="MultiLine" MaxLength="150"
                                                                    Width="300px" Height="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <input type="button" onclick="javascript:AddFeedback();" value="Submit" class="simplebtn" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divResult" style="display: none">
                                                    <table>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                Thank you for your Feedback !
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <input type="button" onclick="javascript:showResult();" value="Click to View Result"
                                                                    class="simplebtn" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="popup4" class="popup">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <table>
                                                    <tr>
                                                        <td align="center">
                                                            <a onclick="closepopup('popup4')" href='JavaScript:timer.start(1000)' class="hLinkNew">
                                                                Resume Exam</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="popup6" class="popup">
                                    <table>
                                        <tr>
                                            <td align="left" valign="top">
                                                <input type="button" onclick="closepopup('popup6')" style="display: none" value="Restart" />
                                                <asp:Label ID="lblIns" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <a onclick="closepopup('popup6')" href='JavaScript:timer.start(1000)' class="hLinkNew">
                                                    Resume Exam</a>
                                                <%--  <input type="button" onclick="closepopup('popup1')" style="display: none" value="Restart" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="popup7" class="popup">
                                    <table width="100%" height="100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <table>
                                                    <tr>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="Label3" runat="server" Text="Are you sure?"></asp:Label><br />
                                                            <asp:Label ID="Label4" runat="server" Text="To proceed and close this window,click proceed."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <input type="button" onclick="JavaScript:closeWindow()" value="Proceed" class="simplebtn" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="center">
                                                            <input type="button" onclick="closepopup('popup7')" value="Cancel" class="simplebtn" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="popup14" class="popup">
                                    <table width="100%" height="100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <table>
                                                    <tr>
                                                        <td align="left" valign="top" colspan="2">
                                                            You can continue with your test, just make sure that you have a working net connection
                                                            before you click on finish test.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <input type="button" onclick="closepopup('popup14')" class="simplebtn" value="Continue" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Label ID="Label1" runat="server" Text="Label" Style="display: none"></asp:Label>
                                <input type="hidden" id="queDivHidden" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="popup5" class="popup">
                    <table style="width: 100%; height: 100%">
                        <tr>
                            <td valign="middle" align="center">
                                <table>
                                    <tr>
                                        <td align="center">
                                            You have already finished this exam !
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <input type="button" onclick="closepopup('popup5')" style="display: none" value="Restart"
                                                class="simplebtn" />
                                            <input type="button" onclick="closeWindow()" value="Close" class="simplebtn" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="popup1" class="popup">
                    <table>
                        <tr>
                            <td align="left" valign="top">
                                <input type="button" onclick="closepopup('popup1')" style="display: none" value="Restart" />
                                <asp:Label ID="lblInstructions" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a onclick="startExams()" href='#' class="hLinkNew">Start Exam</a>
                                <%--  <input type="button" onclick="closepopup('popup1')" style="display: none" value="Restart" />--%>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="bg" class="popup_bg">
                </div>
            </span>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
