﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="regUsers.aspx.cs" Inherits="OnlineExam.regUsers" Title="Registered Users" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center" colspan="2">
                        <h3>
                            Registered Users</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td align="left">
                                    Search users registered :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    between :&nbsp;
                                    <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp; and :&nbsp;
                                    <asp:TextBox ID="txtToDate" runat="server" MaxLength="10"></asp:TextBox>
                                    &nbsp;
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnSearch_Click" />&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Height="350px">
                            <asp:GridView ID="gridUsers" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="uid" DataSourceID="sourceUsers" CssClass="gridview" PageSize="25"
                                OnRowCommand="gridUsers_RowCommand" OnRowCreated="gridUsers_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="uid" HeaderText="uid" SortExpression="uid" />
                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                    <asp:BoundField DataField="uemail" HeaderText="Email" SortExpression="uemail" />
                                    <asp:BoundField DataField="mobileno" HeaderText="Mobile No" SortExpression="mobileno" />
                                    <asp:BoundField DataField="edate" HeaderText="Entry Date" SortExpression="edate" />
                                    <asp:ButtonField ButtonType="Image" CommandName="uDetails" ImageUrl="~/images/Details.png"
                                        Text="Edit">
                                        <ItemStyle Width="20px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="sourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="select * from(SELECT top (15) [uid], [uname], [uemail],[mobileno],convert(varchar(10),edate,103) as edate FROM [exam_users] order by uid desc) e order by uname asc">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel2" runat="server" Height="350px">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="uid" DataSourceID="sourceUsers" CssClass="gridview" 
                                OnRowCommand="gridUsers_RowCommand" OnRowCreated="gridUsers_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="uid" HeaderText="uid" SortExpression="uid" />
                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                    <asp:BoundField DataField="uemail" HeaderText="Email" SortExpression="uemail" />
                                    <asp:BoundField DataField="mobileno" HeaderText="Mobile No" SortExpression="mobileno" />
                                    <asp:BoundField DataField="edate" HeaderText="Entry Date" SortExpression="edate" />
                                    <asp:ButtonField ButtonType="Image" CommandName="uDetails" ImageUrl="~/images/Details.png"
                                        Text="Edit">
                                        <ItemStyle Width="20px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="select * from(SELECT top (15) [uid], [uname], [uemail],[mobileno],convert(varchar(10),edate,103) as edate FROM [exam_users] order by uid desc) e order by uname asc">
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate">
            </asp:CalendarExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
            </asp:CalendarExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFromDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtToDate"
                ValidChars="0123456789./" FilterMode="ValidChars">
            </asp:FilteredTextBoxExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFromDate"
                WatermarkText="From Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtToDate"
                WatermarkText="To Date (DD/MM/YYYY)">
            </asp:TextBoxWatermarkExtender>
            <asp:ModalPopupExtender ID="modalPopupUDetails" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="userDetails" Drag="true" CancelControlID="btnUerDetailsCancel">
            </asp:ModalPopupExtender>
            <div id="userDetails" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe4" frameborder="0" src="userDetails.aspx" height="500px" width="450px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="Button5" value="Done" type="button" />
                    <input id="btnUerDetailsCancel" value="Cancel" type="button" />
                </div>
            </div>
            <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
