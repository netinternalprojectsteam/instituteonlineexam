﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editContactInfo.aspx.cs"
    Inherits="OnlineExam.editContactInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Queston Details</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCinfoCancel').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

    <%--<link href="css/style.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Personal Information
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:Panel ID="Panel1" runat="server">
            <center>
                <table>
                    <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
                    <tr>
                        <td colspan="2" align="center">
                            <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label1" runat="server" Text="Extra Email" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txt1" runat="server" Width="300px" CssClass="txtbox"></asp:TextBox>
                            <%-- <asp:TextBox ID="txtEEmail" runat="server" CssClass="txtbox"> </asp:TextBox>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email !"
                                ControlToValidate="txt1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="Label2" runat="server" Text="Address" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <asp:TextBox ID="txtAdd1" runat="server" Width="300px" CssClass="txtbox"></asp:TextBox>
                                        <%--    <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkText="Address Line 1"
                                            TargetControlID="txtAdd1">
                                        </asp:TextBoxWatermarkExtender>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:TextBox ID="txtAdd2" runat="server" Width="300px" CssClass="txtbox"></asp:TextBox>
                                        <%--<asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkText="Address Line 2"
                                            TargetControlID="txtAdd2">
                                        </asp:TextBoxWatermarkExtender>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                     <asp:Label ID="Label4" runat="server" Text="City" CssClass="myLabel"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="txtCity" runat="server" CssClass="txtbox" Width="120px"></asp:TextBox>
                                        <%--<asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" WatermarkText="City Name"
                                            TargetControlID="txtCity">
                                        </asp:TextBoxWatermarkExtender>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label5" runat="server" Text="Country" CssClass="myLabel"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCountry" runat="server" DataSourceID="sourceCountry" DataTextField="countryName"
                                            DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                                            Width="120px" CssClass="txtbox">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label6" runat="server" Text="State" CssClass="myLabel"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlState" runat="server" DataSourceID="sourceState" DataTextField="stateName"
                                            DataValueField="ID" Width="120px" CssClass="txtbox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update Info"
                                OnClick="btnUpdate_Click" />
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </div>
    <asp:SqlDataSource ID="sourceCountry" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT DISTINCT [ID], [countryName] FROM [countryList] order by countryName ASC">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceState" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] order by statename asc">
    </asp:SqlDataSource>
    </form>
</body>
</html>
