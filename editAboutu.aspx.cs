﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class editAboutu : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblID.Text = Session["editUserID"].ToString();
                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT aboutU,userID FROM exam_users WHERE (uid = " + lblID.Text + ")");
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["aboutU"].ToString().Length > 0)
                        {
                            Editor1.Content = dt.Rows[0][0].ToString();
                            btnSubmit.Text = "Update";
                            Label1.Text = dt.Rows[0][1].ToString();
                        }
                        else { btnSubmit.Text = "Save"; }

                    }
                    else { btnSubmit.Text = "Save"; }


                }
            }
            catch { }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Editor1.Content.Length > 0)
            {

                ob.updateAboutYou(Editor1.Content, lblID.Text);

                Session["AbtYou"] = Editor1.Content;
                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }
    }
}
