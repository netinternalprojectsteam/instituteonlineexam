﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="abtSpecification.aspx.cs" Inherits="OnlineExam.abtSpecification"
    Title="Manage POP Up and Javascript for Browsers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/accordion.css" rel="stylesheet" type="text/css" />
    <link href="/css/tabContainer.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .imgClass
        {
            border-radius: 20px;
            border: solid 2px black;
        }
        .OhLink
        {
            font-family: 'Times New Roman' , Times, serif;
            color: #46b1e1;
            font-weight: bold;
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td align="left" valign="top">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <div class="pageHeading">
                                Manage Pop-ups and Javascripts
                            </div>
                        </td>
                        <td align="right">
                            <asp:HyperLink ID="hyperBack" runat="server" CssClass="OhLink">Back to Start Exam</asp:HyperLink>&nbsp;&nbsp;
                            <a href='/myAccount.aspx' class="OhLink">My Profile</a>&nbsp;&nbsp; <a href='/testsList.aspx'
                                class="OhLink">Test List</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr>
            <td align="right">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:TabContainer ID="TabContainer1" runat="server" CssClass="ajax__tab_yuitabview-theme">
                    <asp:TabPanel runat="server" ID="tab1">
                        <HeaderTemplate>
                            <b>For Google Chrome</b>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="700px">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <b>To enable JavaScript and Popups in Google Chrome perform the following steps:</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H1.gif" />&nbsp;Click the Chrome menu on the browser toolbar.<br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc1.png" width="400px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H2.gif" />
                                            &nbsp;Select Settings.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc2.png" width="400px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H3.gif" />
                                            &nbsp;Click Show advanced settings.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc4.png" width="400px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H4.gif" />
                                            &nbsp;In the "Privacy" section, click the Content settings button.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H5.gif" />
                                            In the Dialog Box Under Javascript Click the radio button for "Allow all sites run
                                            javascript"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc5.png" width="400px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H6.gif" />
                                            In the same Dialog Box Under Pop-ups Click the radio button for "Allow all sites
                                            to run pop-ups"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc6.png" width="400px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H7.gif" />
                                            In the same Dialog Box Click Done & Relaunch your browser.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Gc7.png" width="400px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel HeaderText="<b>Purchase Summary</b>" runat="server" ID="TabPanel1">
                        <HeaderTemplate>
                            <b>For Mozilla Firefox</b>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="Panel5" runat="server" Height="700px">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <b>To enable JavaScript and Popups in Mozilla Firefox perform the following steps:</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H1.gif" />&nbsp;From the Tools menu, select Options.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <img src="HelpDocs/Mf1.png" width="300px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H2.gif" />
                                            &nbsp;From the Content tab, uncheck Block Popup Windows and Enable Javascript.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            <img src="HelpDocs/Mf2.png" width="300px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H3.gif" />
                                            &nbsp;Click OK
                                        </td>
                                    </tr>
                                    <td valign="top" align="left">
                                        <img src="HelpDocs/H4.gif" />
                                        &nbsp;Relaunch your browser.
                                    </td>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel2">
                        <HeaderTemplate>
                            <b>For Internet Explorer</b>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="Panel2" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <b>To enable JavaScript and Popups in Microsoft Internet Explorer perform the following
                                                steps:</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H1.gif" alt="" />&nbsp;From the Tools menu, click Internet Options.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <img src="HelpDocs/Ie1.png" alt="" width="300px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H2.gif" />
                                            &nbsp;From the Security tab, click Custom Level.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Ie2.png" width="300px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <img src="HelpDocs/H3.gif" />
                                            &nbsp;Scroll to Java permissions, click to Enable under Scripting tag.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <img src="HelpDocs/Ie3.png" width="300px" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H4.gif" />
                                            &nbsp;Click OK
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H5.gif" />
                                            &nbsp;Now Select the Privacy tab & uncheck the 'Pop-up Blocker'.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <img src="HelpDocs/Ie4.png" width="300px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H6.gif" />
                                            &nbsp;Click OK
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left">
                                            <img src="HelpDocs/H7.gif" />
                                            &nbsp;Relaunch your browser.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </td>
        </tr>
    </table>
    <%--  <asp:Accordion ID="Accordion1" runat="server" FadeTransitions="true" TransitionDuration="250"
        FramesPerSecond="40" AutoSize="None" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
        ContentCssClass="accordionContent" RequireOpenedPane="false" Width="100%">
        <Panes>
            <asp:AccordionPane ID="AccPan1" runat="server" Width="100%">
                <Header>
                    For Internet Explorer</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server" Height="700px">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b>To enable JavaScript and Popups in Microsoft Internet Explorer perform the following
                                        steps:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H1.gif" alt="" />&nbsp;From the Tools menu, click Internet Options.<br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="HelpDocs/Ie1.png" alt="" width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H2.gif" />
                                    &nbsp;From the Security tab, click Custom Level.
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Ie2.png" width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" valign="top">
                                    <img src="HelpDocs/H3.gif" />
                                    &nbsp;Scroll to Java permissions, click to Enable under Scripting tag.<br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Ie3.png" width="300px" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H4.gif" />
                                    &nbsp;Click OK
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H5.gif" />
                                    &nbsp;Now Select the Privacy tab & uncheck the 'Pop-up Blocker'.<br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="HelpDocs/Ie4.png" width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H6.gif" />
                                    &nbsp;Click OK<br />
                                    <br />
                                    <img src="HelpDocs/H7.gif" />
                                    &nbsp;Relaunch your browser.
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </Content>
            </asp:AccordionPane>
            <asp:AccordionPane ID="AccPan2" runat="server">
                <Header>
                    For Mozilla Firefox
                </Header>
                <Content>
                    <asp:Panel ID="Panel2" runat="server" Height="700px">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b>To enable JavaScript and Popups in Mozilla Firefox perform the following steps:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H1.gif" />&nbsp;From the Tools menu, select Options.
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <img src="HelpDocs/Mf1.png" width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H2.gif" />
                                    &nbsp;From the Content tab, uncheck Block Popup Windows and Enable Javascript.
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <img src="HelpDocs/Mf2.png" width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <img src="HelpDocs/H3.gif" />
                                    &nbsp;Click OK
                                </td>
                            </tr>
                            <td valign="top" align="left">
                                <img src="HelpDocs/H4.gif" />
                                &nbsp;Relaunch your browser.
                            </td>
                        </table>
                    </asp:Panel>
                </Content>
            </asp:AccordionPane>
            <asp:AccordionPane ID="AccPan3" runat="server">
                <Header>
                    For Google Chrome
                </Header>
                <Content>
                    <asp:Panel ID="Panel3" runat="server" Width="700px">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b>To enable JavaScript and Popups in Google Chrome perform the following steps:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H1.gif" />&nbsp;Click the Chrome menu
                                    <img src="HelpDocs/cMenu.png" />
                                    on the browser toolbar.<br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc1.png" width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H2.gif" />
                                    &nbsp;Select Settings.
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc2.png" width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H3.gif" />
                                    &nbsp;Click Show advanced settings.
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc4.png" width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H4.gif" />
                                    &nbsp;In the "Privacy" section, click the Content settings button.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H5.gif" />
                                    In the Dialog Box Under Javascript Click the radio button for "Allow all sites run
                                    javascript"
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc5.png" width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H6.gif" />
                                    In the same Dialog Box Under Pop-ups Click the radio button for "Allow all sites
                                    to run pop-ups"
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc6.png" width="400px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="HelpDocs/H7.gif" />
                                    In the same Dialog Box Click Done & Relaunch your browser.
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="HelpDocs/Gc7.png" width="400px" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </Content>
            </asp:AccordionPane>
        </Panes>
    </asp:Accordion>--%>
</asp:Content>
