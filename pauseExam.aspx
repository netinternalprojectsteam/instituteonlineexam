﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pauseExam.aspx.cs" Inherits="OnlineExam.pauseExam" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pause Exam</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkPause').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCanPause').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Exam Paused
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <center>
                        <table>
                            <tr>
                                <td>
                                    <%--<asp:ImageButton ID="imgRestart" runat="server" ImageUrl="~/images/restartExam.png"
                                        Height="50px" OnClick="imgRestart_Click" Width="50px" />--%>
                                    <asp:Button ID="btnRestart" runat="server" Text="Resume" CssClass="simplebtn" OnClick="btnRestart_Click" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
