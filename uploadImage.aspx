﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uploadImage.aspx.cs" Inherits="OnlineExam.uploadImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>

    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

    <script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>

    <script type="text/javascript">
    $(function () {
        CKEDITOR.replace('<%=txtCkEditor.ClientID %>', { filebrowserImageUploadUrl: '/Upload.ashx' });
    });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:TextBox ID="txtCkEditor" TextMode="MultiLine" runat="server"></asp:TextBox>
    </div>
    </form>
</body>
</html>
