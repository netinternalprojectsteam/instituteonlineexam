﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class insertQueInfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string qID = Session["QID"].ToString();
                    if (Session["InstituteID"] != null)
                    {
                        lblIns.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblIns.Text = val[3];
                    }

                    DataTable dt = new DataTable();
                    dt = ob.getData("select questid, questionInfo from view_examQuestions where questid = " + qID + " and instituteId=" + lblIns.Text + "");
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["questionInfo"].ToString() != "")
                        {
                            FCKeditor1.Value = dt.Rows[0]["questionInfo"].ToString(); btnSave.Text = "Update";
                        }
                    }
                }
                catch { }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (FCKeditor1.Value.Length > 0)
                {
                    if (btnSave.Text == "Save")
                    {
                        ob.insertQueInfo(Session["QID"].ToString(), FCKeditor1.Value);
                    }
                    if (btnSave.Text == "Update")
                    {
                        ob.updateQueInfo(Session["QID"].ToString(), FCKeditor1.Value);
                    }

                    string script = "<script type=\"text/javascript\">cancel();</script>";
                    //Page page = HttpContext.Current.CurrentHandler as Page;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("duplicate"))
                {
                    Messages11.setMessage(0, "Data already exists.Please avoid duplicates !");
                    Messages11.Visible = true;
                }
            }
        }

        protected void txtInfo_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
