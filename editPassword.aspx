﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editPassword.aspx.cs" Inherits="OnlineExam.editPassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Password</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCancelPass').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Change Password
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:Panel ID="Panel1" runat="server">
            <center>
                <table>
                    <tr>
                        <td colspan="2" align="center">
                            <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblImage" runat="server" Visible="false" Text="Label"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Old Password" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtOld" runat="server" TextMode="Password" CssClass="txtbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtOld"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="New Password" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNew" runat="server" TextMode="Password" CssClass="txtbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                ControlToValidate="txtNew"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Confirm Password" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password" CssClass="txtbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                ControlToValidate="txtConfirm"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                            <asp:Button ID="btnCancel" CssClass="simplebtn" runat="server" Text="Cancel" OnClientClick="callfun();"
                                CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
