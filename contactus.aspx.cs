﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Net.Mail;
using System.Net;

namespace OnlineExam
{
    public partial class contactus : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] valuser = HttpContext.Current.User.Identity.Name.Split(',');


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInstituteId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInstituteId.Text = valuser[3];
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text.Length > 0 && txtMobile.Text.Length > 0 && txtMail.Text.Length > 0)
                {
                    ob.insertContactUs(txtName.Text, txtMail.Text, txtMobile.Text, txtQuery.Text);
                    lblContactusMsg.Visible = true;
                    lblContactusMsg.Text = "Your Details Saved Successfully !";
                    lblContactusMsg.ForeColor = System.Drawing.Color.Green;
                    string Query = "No query.";
                    if (txtQuery.Text.Length > 0)
                    {
                        Query = txtQuery.Text;
                    }
                    string mail = "<b><u>Enquiry From</u></b><br><br><b>Name :</b> " + txtName.Text + "<br><b>Email : </b>" + txtMail.Text + "<br><b>Mobile : </b>" + txtMobile.Text + "<br><b>Query : </b><br>" + Query + "";
                    lblMail.Text = mail;

                    //string contactEID = ConfigurationManager.AppSettings["ContactEmailID"].ToString();
                    //string contactPass = ConfigurationManager.AppSettings["ContactEmailPass"].ToString();

                    ////System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("info@pgfunda.com", "contact@pgfunda.com", "New Enquiry at pgfunda.com", lblMail.Text);
                    ////mm.IsBodyHtml = true;
                    ////System.Net.Mail.SmtpClient client = new SmtpClient();
                    ////client.Send(mm);

                    //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(contactEID, contactEID, "New Enquiry at pgfunda.com", lblMail.Text);
                    //mm.IsBodyHtml = true;
                    //System.Net.Mail.SmtpClient client = new SmtpClient();
                    //client.Credentials = new System.Net.NetworkCredential(contactEID, contactPass);
                    //client.Send(mm);

                    string emailID = "";
                    string password = "";
                    string smtpHost = "";
                    string smtpPort = "";
                    bool smtpEnable;

                    DataTable dtEmail = new DataTable();
                    dtEmail = ob.getData("Select id, emailID, password, smtpHost, smtpPort, smtpEnable from email_Config where instituteId=" + lblInstituteId.Text + "");
                    if (dtEmail.Rows.Count > 0)
                    {
                        emailID = dtEmail.Rows[0]["emailID"].ToString();
                        password = base64Decode(dtEmail.Rows[0]["password"].ToString());
                        smtpHost = dtEmail.Rows[0]["smtpHost"].ToString();
                        smtpPort = dtEmail.Rows[0]["smtpPort"].ToString();
                        smtpEnable = Convert.ToBoolean(dtEmail.Rows[0]["smtpEnable"].ToString());
                    }
                    else
                    {
                        emailID = ConfigurationManager.AppSettings["InfoEmailID"];
                        password = ConfigurationManager.AppSettings["InfoEmailPass"];
                        smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                        smtpPort = ConfigurationManager.AppSettings["smtpPort"];
                        smtpEnable = true;
                    }

                    System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                    //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                    SmtpClient Sc = new SmtpClient(smtpHost);
                    //string port = ConfigurationManager.AppSettings["smtpPort"];
                    Sc.Port = Convert.ToInt32(smtpPort);
                    //string infoEID = ConfigurationManager.AppSettings["ContactEmailID"];
                    //string infoPass = ConfigurationManager.AppSettings["ContactEmailPass"];
                    Sc.Credentials = new NetworkCredential(emailID, password);
                    Sc.EnableSsl = smtpEnable;
                    ms.From = new MailAddress(emailID);
                    ms.To.Add(emailID);

                    ms.Subject = "New Enquiry at pvkexams.com";
                    ms.Body = mail;
                    ms.IsBodyHtml = true;
                    Sc.Send(ms);



                    //DataTable dtConfig = new DataTable();
                    //dtConfig = ob.getData("select id,emailid,password,smtphost,smtpport,smtpenable from email_config");
                    //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(dtConfig.Rows[0]["emailID"].ToString(), "padiya.ashish@gmail.com", "New Enquiry at onlinebankingexam.com", lblMail.Text);//
                    //mm.IsBodyHtml = true;
                    //System.Net.Mail.SmtpClient client = new SmtpClient();
                    //client.Host = dtConfig.Rows[0]["smtpHost"].ToString();
                    //client.EnableSsl = true;
                    //client.Port = Convert.ToInt32(dtConfig.Rows[0]["smtpPort"].ToString());
                    //NetworkCredential ntc = new NetworkCredential(dtConfig.Rows[0]["emailID"].ToString(), dtConfig.Rows[0]["password"].ToString());
                    //client.Credentials = ntc;
                    //client.Send(mm);

                    txtMail.Text = "";
                    txtMobile.Text = "";
                    txtName.Text = "";
                    txtQuery.Text = "";
                }
                else
                {
                    lblContactusMsg.Text = "Name,Email,Mobile Required !";
                    lblContactusMsg.ForeColor = System.Drawing.Color.Red;
                    lblContactusMsg.Visible = true;
                }
            }
            catch { }
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }

    }
}
