﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="printPaper.aspx.cs" Inherits="OnlineExam.printPaper" Title="Print Paper" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .paperLabel
        {
            color: black;
            font-size: small;
        }
        .queNo
        {
            font-weight: bold;
            color: black;
            font-size: medium;
        }
    </style>

    <script type="text/javascript">
  function CallPrint(strid)
  {
      var prtContent = document.getElementById(strid);
      var WinPrint = window.open('','','letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
      WinPrint.document.write(prtContent.innerHTML);
      WinPrint.document.close();
      WinPrint.focus();
      WinPrint.print();
      WinPrint.close();

}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="GridView1" runat="server" Visible="false">
            </asp:GridView>
            <asp:Label ID="lblInfoID" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Label ID="lblUsedInfoID" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Print Paper</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblError" Font-Bold="true" ForeColor="Red" runat="server" Text="No Questions Found in Exam !"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="Label10" runat="server" Text="Exam Name"></asp:Label>
                        <asp:DropDownList ID="ddlExams" runat="server" DataSourceID="sourceExams" DataTextField="examName"
                            DataValueField="examID">
                        </asp:DropDownList>
                        <asp:Button ID="btnShow" runat="server" Text="Question Paper" OnClick="btnShow_Click"
                            CssClass="simplebtn" />
                        <asp:Button ID="btnShowQueAns" runat="server" Text="Questions with Answers" CssClass="simplebtn"
                            OnClick="btnShowQueAns_Click" />
                        <asp:Button ID="btnShowQue" runat="server" Text="Questions Only" CssClass="simplebtn"
                            OnClick="btnShowQue_Click" />
                        <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT Distinct examID, examName FROM view_examDetails where instituteId=@instituteId">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="right">
                                        <input type="image" value="Print " id="Button1" onclick="javascript:CallPrint('divPrint')"
                                            src="/images/printico.png" title="Print" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="divPrint">
                                            <asp:Repeater ID="repQuestions" runat="server">
                                                <ItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top" style='border: solid <%#Eval("borderInfo")%>px black'>
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("info") %>' CssClass="paperLabel"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("srNo") %>' CssClass="queNo"></asp:Label>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                &nbsp;&nbsp;
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label3" runat="server" Text="1)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption1" runat="server" Text='<%#Eval("option1") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label4" runat="server" Text="2)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption2" runat="server" Text='<%#Eval("option2") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label5" runat="server" Text="3)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption3" runat="server" Text='<%#Eval("option3") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label6" runat="server" Text="4)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption4" runat="server" Text='<%#Eval("option4") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label7" runat="server" Text="5)" CssClass="paperLabel"> </asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption5" runat="server" Text='<%#Eval("option5") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <hr style="border-bottom: solid 1 px black" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <asp:GridView ID="GridView2" runat="server">
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="right">
                                        <input type="image" value="Print " id="Image1" onclick="javascript:CallPrint('divPrint1')"
                                            src="/images/printico.png" title="Print" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <div id="divPrint1">
                                            <asp:Repeater ID="repQueWithAnswer" runat="server">
                                                <ItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("srNo") %>' CssClass="queNo"></asp:Label>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                &nbsp;&nbsp;
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label3" runat="server" Text="1)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption1" runat="server" Text='<%#Eval("option1") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label4" runat="server" Text="2)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption2" runat="server" Text='<%#Eval("option2") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label5" runat="server" Text="3)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption3" runat="server" Text='<%#Eval("option3") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label6" runat="server" Text="4)" CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption4" runat="server" Text='<%#Eval("option4") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 10px">
                                                                            <asp:Label ID="Label7" runat="server" Text="5)" CssClass="paperLabel"> </asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOption5" runat="server" Text='<%#Eval("option5") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            &nbsp;&nbsp;
                                                            <td align="left" valign="top">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 70px">
                                                                            <asp:Label ID="Label11" runat="server" Text="Answer : " Font-Bold="true" CssClass="paperLabel"> </asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="Label12" runat="server" Text='<%#Eval("CorrectAns") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <hr style="border-bottom: solid 1 px black" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <table width="100%">
                                <tr>
                                    <td align="right">
                                        <input type="image" value="Print " id="Image2" onclick="javascript:CallPrint('divPrint2')"
                                            src="/images/printico.png" title="Print" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <div id="divPrint2">
                                            <asp:Repeater ID="repQuestionsOnly" runat="server">
                                                <ItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td align="left" valign="top" style="width: 80px;">
                                                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("srNo") %>' CssClass="queNo"></asp:Label>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>' CssClass="paperLabel"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <hr style="border-bottom: solid 1 px black" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
