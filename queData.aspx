﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="queData.aspx.cs" Inherits="OnlineExam.queData" Title="Add Common Data" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td align="center" colspan="2">
                <h3>
                    Information related to Questions</h3>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lblInst" runat="server" Text="" Visible="false"></asp:Label>
                <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 150px" valign="top">
                <asp:Label ID="Label1" runat="server" Text="Information Heading" Font-Bold="true"></asp:Label>
            </td>
            <td valign="top">
                <asp:TextBox ID="txtHeading" runat="server" Width="300px" OnTextChanged="txtHeading_TextChanged"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtHeading"
                    runat="server" ErrorMessage="Enter Heading"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <asp:Label ID="Label2" runat="server" Text="Information" Font-Bold="true"></asp:Label>
            </td>
            <td valign="top">
                <FCKeditorV2:FCKeditor ID="FCKeditor1" runat="server" Width="750px">
                </FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:Button ID="btnNew" runat="server" Text="Add New" CssClass="simplebtn" OnClick="btnNew_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
