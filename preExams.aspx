﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="preExams.aspx.cs" Inherits="OnlineExam.preExams" Title="Predifined Exams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
  function openNewWindow()
        {
        window.open("Exam.aspx", "Exam", "location=1, width=auto, height=auto");
        }
    </script>

    <script type="text/javascript" language="javascript"> 
        function showAlert()
        {
       
            alert('You have already attempted this exam !');
        }
        
 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblPassword" runat="server" Visible="false" Text="Label"></asp:Label>
            <asp:Label ID="lblExamID" runat="server" Visible="false" Text="Label"></asp:Label>
            <table width="100%">
                <tr>
                    <td style="padding: 20px 20px 20px 20px" align="left">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <h2>
                                        Predifined Exams
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                    <asp:Label ID="lblUserID" runat="server" EnableViewState="False" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                ( Insure that POP UP has been allowed to start the exam )
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td align="center">
                                    <asp:Panel ID="pnlStart" runat="server">
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="Label1" runat="server" Text="Choose Exam Name" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlExams" runat="server" DataTextField="examName" DataValueField="examID">
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                        SelectCommand="SELECT examID, examName, courseID FROM exm_existingExams WHERE (isAvailable = 1) AND (questionNos IS NOT NULL) AND (examType != 'Demo')">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                </td>
                                                <td align="left">
                                                    <asp:Button ID="btnStart" runat="server" Text="Start" CssClass="simplebtn" OnClick="btnStart_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Panel ID="pnlPayNow" runat="server" Visible="false">
                                        <table>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblError" runat="server" Font-Bold="true" Font-Size="Larger" Text="You have not paid amount for this exam !"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btnPayNow" runat="server" Text="Pay Now !" CssClass="simplebtn" />
                                                    <asp:Button ID="btnCanPay" runat="server" Text="Cancel" CssClass="simplebtn" CausesValidation="false"
                                                        OnClick="btnCanPay_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Panel ID="pnlPass" runat="server" Visible="false">
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblMsg" Font-Bold="true" Font-Size="Larger" runat="server" Text="Please provide your password !"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="Label3" runat="server" Text="Password" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td style="width: 15px">
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                            <tr>
                                                                <td colspan="2">
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Button ID="btnStartExam" CssClass="simplebtn" runat="server" Text="Start" OnClick="btnStartExam_Click" />
                                                                    <asp:Button ID="btnCanPass" runat="server" Text="Cancel" CssClass="simplebtn" CausesValidation="false"
                                                                        OnClick="btnCanPass_Click" />
                                                                </td>
                                                            </tr>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
