﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class studyDetails : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string id = Request.QueryString["ID"];
            //string id = "1";
            dt = ob.getStudyMaterialDetails(id);
            if (dt.Rows.Count > 0)
            {
                //title, description, iconImage, downloadFile,

                lblHeading.Text = dt.Rows[0]["Mheading"].ToString();
                lblDesc.Text = dt.Rows[0]["Mdetails"].ToString();

            }
            else
            {

            }


        }
    }
}
