﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class feedback : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                ddlExams.DataBind();
                if (ddlExams.Text != "-Select-")
                {

                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT id, uname AS Name, CONVERT(varchar(10), onDate, 103) AS [On Date], feedBack AS Feedback, Case WHEN IsPublish ='False' THEN 'Add' ELSE 'Remove' end as publish FROM view_feedback WHERE preExamID = " + ddlExams.SelectedValue + " order by onDate DESC");

                    if (dt.Rows.Count > 0)
                    {
                        gridDetails.DataSource = dt;
                        gridDetails.DataBind();
                        Label1.Visible = false;
                        Panel1.Visible = true;
                        if (gridDetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < gridDetails.Rows.Count; i++)
                            {
                                Button btn = (Button)gridDetails.Rows[i].FindControl("gridButton");
                                btn.Text = dt.Rows[i]["publish"].ToString();
                            }
                        }
                    }
                    else
                    {
                        Panel1.Visible = false;
                        Label1.Visible = true;

                    }
                }
            }
        }
        protected void ddlExams_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlExams.Text != "-Select-")
            {

                DataTable dt = new DataTable();
                dt = ob.getData("SELECT id, uname AS Name, CONVERT(varchar(10), onDate, 103) AS [On Date], feedBack AS Feedback, Case WHEN IsPublish ='False' THEN 'Add' ELSE 'Remove' end as publish FROM view_feedback WHERE preExamID = " + ddlExams.SelectedValue + " order by onDate DESC");

                if (dt.Rows.Count > 0)
                {
                    gridDetails.DataSource = dt;
                    gridDetails.DataBind();
                    Label1.Visible = false;
                    Panel1.Visible = true;
                    if (gridDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < gridDetails.Rows.Count; i++)
                        {
                            Button btn = (Button)gridDetails.Rows[i].FindControl("gridButton");
                            btn.Text = dt.Rows[i]["publish"].ToString();
                        }
                    }
                }
                else
                {
                    Panel1.Visible = false;
                    Label1.Visible = true;

                }
            }
        }

        protected void gridDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow gvrow = (GridViewRow)btn.NamingContainer;
            string id = gridDetails.DataKeys[gvrow.RowIndex].Value.ToString();
            if (btn.Text == "Add")
            {
                ob.setFeedback(true, id);
            }
            else
            {
                ob.setFeedback(false, id);
            }
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT id, uname AS Name, CONVERT(varchar(10), onDate, 103) AS [On Date], feedBack AS Feedback, Case WHEN IsPublish ='False' THEN 'Add' ELSE 'Remove' end as publish FROM view_feedback WHERE preExamID = " + ddlExams.SelectedValue + " order by onDate DESC");

            if (dt.Rows.Count > 0)
            {
                gridDetails.DataSource = dt;
                gridDetails.DataBind();
                Label1.Visible = false;
                Panel1.Visible = true;
                if (gridDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < gridDetails.Rows.Count; i++)
                    {
                        Button btn1 = (Button)gridDetails.Rows[i].FindControl("gridButton");
                        btn1.Text = dt.Rows[i]["publish"].ToString();
                    }
                }
            }
            else
            {
                Panel1.Visible = false;
                Label1.Visible = true;

            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow gvrow = (GridViewRow)btn.NamingContainer;
            string id = gridDetails.DataKeys[gvrow.RowIndex].Value.ToString();
            if (btn.Text == "Delete")
            {
                ob.deleteFeedback(id);
                DataTable dt = new DataTable();
                dt = ob.getData("SELECT id, uname AS Name, CONVERT(varchar(10), onDate, 103) AS [On Date], feedBack AS Feedback, Case WHEN IsPublish ='False' THEN 'Add' ELSE 'Remove' end as publish FROM view_feedback WHERE preExamID = " + ddlExams.SelectedValue + " order by onDate DESC");

                if (dt.Rows.Count > 0)
                {
                    gridDetails.DataSource = dt;
                    gridDetails.DataBind();
                    Label1.Visible = false;
                    Panel1.Visible = true;
                    if (gridDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < gridDetails.Rows.Count; i++)
                        {
                            Button btn1 = (Button)gridDetails.Rows[i].FindControl("gridButton");
                            btn1.Text = dt.Rows[i]["publish"].ToString();
                        }
                    }
                }
                else
                {
                    Panel1.Visible = false;
                    Label1.Visible = true;

                }
            }
        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
            }
        }
    }
}
