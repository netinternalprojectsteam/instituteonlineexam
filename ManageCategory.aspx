﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="ManageCategory.aspx.cs" Inherits="OnlineExam.ManageCategory" Title="Manage Category" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 336px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table class="style1">
                <tr>
                    <td align="center" colspan="2">
                        <h3>
                            Manage Category</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Category :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCategory"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblCatID" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="lblInstituteId" runat="server" Visible="False"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnsubmit" runat="server" OnClick="Button1_Click" Text="Add New"
                            CssClass="simplebtn" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="simplebtn" OnClick="btnCancel_Click"
                            Visible="False" CausesValidation="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            DataKeyNames="catid" DataSourceID="SqlDataSource1" OnRowCommand="GridView1_RowCommand"
                            OnRowCreated="GridView1_RowCreated" CssClass="gridview">
                            <Columns>
                                <asp:BoundField DataField="catid" HeaderText="catid" InsertVisible="False" ReadOnly="True"
                                    SortExpression="catid" />
                                <asp:BoundField DataField="category" HeaderText="Category" SortExpression="category" />
                                <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="simplebtn" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT [catid], [category] FROM [exm_category] where InstituteId=@InstituteId">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblInstituteId" Name="InstituteId" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
