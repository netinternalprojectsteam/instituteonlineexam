﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class createnewaccount : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //string questionList = "";
                obj.CreateNewUserAccount(txtFirstName.Text, txtEmailID.Text, txtPassword.Text, txtMo.Text);
                string n = obj.getValue("Select max(uid) + 1 from exam_users");
                //DataTable dt = new DataTable();
                //dt = obj.GetUserLoginInfo(txtEmailID.Text);
                //Session.Add("useremail", dt.Rows[0][0].ToString());
                //DataTable dtQuestion = new DataTable();
                //dtQuestion = obj.GetUserExamQuestionID(Convert.ToInt16(ddlQNO.SelectedValue));
                //for (int i = 0; i < dtQuestion.Rows.Count; i++)
                //{
                //    if (questionList == "")
                //    {
                //        questionList = dtQuestion.Rows[i][0].ToString();
                //    }
                //    else
                //    {
                //        questionList = questionList + "," + dtQuestion.Rows[i][0].ToString();
                //    }
                //}
                //Session.Add("questionlist", questionList);
                //Session.Add("questno", 0);

                ////  obj.InsertUserQuestionList(ddlQNO.SelectedValue, questionList);
                //Response.Redirect("Default.aspx");


                FormsAuthentication.RedirectFromLoginPage(n + "," + "User" + "," + "NXGOnlineExam", false);
            }
            catch (Exception)
            {
                Response.Redirect("/default.aspx");
            }
        }
    }
}
