﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="queInfo.aspx.cs" Inherits="OnlineExam.queInfo" Title="Question Information" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Question Information</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td valign="top">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Course :"></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:DropDownList ID="ddlCourse" runat="server" AutoPostBack="True" DataSourceID="sourceCourse"
                                        DataTextField="courseinfo" DataValueField="courseid" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Subject :"></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:DropDownList ID="ddlSub" runat="server" DataSourceID="sourceSub" DataTextField="subjectinfo"
                                        DataValueField="subid">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top">
                                    <asp:Button ID="btnShow" runat="server" Text="Show" OnClick="btnShow_Click" CssClass="simplebtn" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:SqlDataSource ID="sourceQueNos" runat="server"></asp:SqlDataSource>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div style="float: left; width: 40px">
                                    <asp:Button ID="Button1" runat="server" Text='<%# Eval("serialnumber") %>' CommandArgument='<%#Eval("questid") %>'
                                        OnClick="btnRep_Click" Width="35px" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Repeater ID="Repeater2" runat="server">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                            Question :
                                            <asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("question") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Option A :
                                            <asp:Label ID="lblOptionA" runat="server" Text='<%# Eval("option1") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Option B :
                                            <asp:Label ID="lblOptionB" runat="server" Text='<%# Eval("option2") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Option C :
                                            <asp:Label ID="lblOptionC" runat="server" Text='<%# Eval("option3") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Option D :
                                            <asp:Label ID="lblOptionD" runat="server" Text='<%# Eval("option4") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Option E :
                                            <asp:Label ID="lblOptionE" runat="server" Text='<%#Eval("option5") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            Answer :
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Answer") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("questid") %>'
                                                OnClick="LinkButton2_Click" Style="text-decoration: underline; color: Blue">Add Details</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceQuestions" runat="server"></asp:SqlDataSource>
                        <asp:GridView ID="gridQuestions" runat="server" DataSourceID="sourceQuestions" Visible="false">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblInstId" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                SelectCommand="SELECT DISTINCT [courseid], [catid], [courseinfo] FROM [view_catCourseSub] where instituteId=@instituteId and courseid is not null and subid is not null">
                <SelectParameters>
                    <asp:ControlParameter ControlID="lblInstId" Name="instituteId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sourceSub" runat="server"></asp:SqlDataSource>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
            </div>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="LinkButton2"
                BackgroundCssClass="ModalPopupBG" PopupControlID="queDetails" Drag="true" CancelControlID="Button3">
            </asp:ModalPopupExtender>
            <div id="queDetails" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe1" frameborder="0" src="insertQueInfo.aspx" height="600px" width="800px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="Button2" value="Done" type="button" />
                    <input id="Button3" value="Cancel" type="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
