﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserExamStartNew.aspx.cs"
    Inherits="OnlineExam.UserExamStartNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Exam</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <link href="css/slideOutPanel.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="jqueryShowHide.js"></script>

    <script type="text/javascript">
$(document).ready(function(){
	$(".trigger").click(function(){
		
$(".panel123").toggle("fast");
		$(this).toggleClass("active");
		return false;
	});
});
</script>

    <script type="text/javascript">
function checkconnection() {
var status = navigator.onLine;
if (status) {
return true;} else {
alert("Your internet connection is lost..."); return false;
}
}
    </script>

    <script language="javascript" type="text/javascript">
    
   
    
    
function hideLink()
{
document.getElementById("linkCleraOptions").innerHTML="";
document.getElementById("rb2").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;

document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb5.ClientID %>").checked = false;
}

function showLink()
{document.getElementById("linkCleraOptions").innerHTML="Clear";
}

    </script>

    <script language="javascript" type="text/javascript">



function CheckOnOff()
{
if(document.getElementById("<%= rb1.ClientID %>").checked == true)
{
document.getElementById("rb2").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
document.getElementById("linkCleraOptions").innerHTML="Clear";

document.getElementById("<%= rb5.ClientID %>").checked = false;

}

}


function CheckOnOff1()
{
if(document.getElementById("<%= rb2.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
document.getElementById("linkCleraOptions").innerHTML="Clear";

document.getElementById("<%= rb5.ClientID %>").checked = false;
}

}


function CheckOnOff2()
{
if(document.getElementById("<%= rb3.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
document.getElementById("linkCleraOptions").innerHTML="Clear";
document.getElementById("<%= rb5.ClientID %>").checked = false;

}

}


function CheckOnOff3()
{
if(document.getElementById("<%= rb4.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("linkCleraOptions").innerHTML="Clear";
document.getElementById("<%= rb5.ClientID %>").checked = false;
}

}


function CheckOnOff4()
{
if(document.getElementById("<%= rb5.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("linkCleraOptions").innerHTML="Clear";
document.getElementById("<%= rb4.ClientID %>").checked = false;
}

}



    </script>

    <%--alert('Call JavaScript function from codebehind');--%>

    <script type="text/javascript" language="javascript"> 
        function showMessage()
        {
//        if (confirm("Exam completed ! Displaying Question No. 1...")==true)
//        return true;
//        else ()
//        return false;
alert('Exam completed ! Displaying Question No. 1...');
        }
        
        
        function showErrorMsg()
        {
         alert('Please select an option !');
        }
    </script>

    <script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
        document.getElementById("Button3").click();     
       }
   
 
 
 
    </script>

    <script type="text/javascript" language="javascript"> 
        function ConfirmForFinish()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
        
        function openNewWindow()
        {
        window.open("userexamstartNew.aspx", "Exam", "location=1, width=1000px, height=800%");
        }
        
    </script>

    <style type="text/css">
        table.gridtable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-collapse: collapse;
            background-image: url(../images/botm.gif);
        }
        table.gridtable th
        {
            border-width: 0px;
            padding: 0px;
            background: #666 url(../images/breadcrumb_bg.gif) repeat-x top;
            text-align: center;
        }
        table.gridtable td
        {
            border-width: 1px;
            padding: 0px;
            border-style: solid;
            border-color: #FFFFFF;
            color: #FFFFFF;
        }
        table.gridtable1
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-collapse: collapse;
            background-color: White;
        }
        table.gridtable1 th
        {
            border-width: 0px;
            padding: 0px;
            background: #666 url(../images/breadcrumb_bg.gif) repeat-x top;
            text-align: center;
        }
        table.gridtable1 td
        {
            border-width: 1px;
            padding: 0px;
            border-style: solid;
            border-color: Black;
            color: #FFFFFF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <table width="100%">
        <tr>
            <td align="right">
                <%-- <asp:Button ID="btnPause" runat="server" Text="Pause Exam" CssClass="simplebtn" CausesValidation="false"
                    OnClick="btnPause_Click" />
                <asp:Button ID="btnsaveForLater" CssClass="simplebtn" CausesValidation="false" runat="server"
                    Text="Save For Later" OnClientClick="return ConfirmForFinish();" OnClick="btnsaveForLater_Click" />
                <asp:Button ID="btnFinish" runat="server" Text="Finish Exam" OnClick="btnFinish_Click"
                    CssClass="simplebtn" OnClientClick="return ConfirmForFinish();" />--%>
                <table>
                    <tr>
                        <td valign="baseline">
                            <asp:LinkButton ID="LinkButton5" runat="server" OnClick="btnPause_Click" ForeColor="Black"
                                OnClientClick="return checkconnection();">Pause Exam</asp:LinkButton>
                            <span>|</span>
                        </td>
                        <td valign="baseline">
                            <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="return ConfirmForFinish();"
                                OnClick="btnsaveForLater_Click" ForeColor="Black">Save For Later</asp:LinkButton>
                            <span>|</span>
                        </td>
                        <td valign="baseline">
                            <asp:LinkButton ID="LinkButton6" runat="server" OnClientClick="return ConfirmForFinish();"
                                OnClick="btnFinish_Click" ForeColor="Black">Finish Exam</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel4" runat="server">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblYourName" runat="server" Text="Label" CssClass="myLabelHead"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Panel ID="Panel7" runat="server">
                                <asp:ImageButton ID="imgHideClock" runat="server" ToolTip="Hide Clock" Width="10px"
                                    Height="10px" ImageUrl="~/images/cross.png" OnClick="imgHideClock_Click" />
                                <asp:LinkButton ID="lnkHideClock" runat="server" OnClick="lnkHideClock_Click" Font-Size="X-Small"
                                    Style="text-decoration: none" ToolTip="Hide Clock">Hide Clock</asp:LinkButton>
                                <asp:ImageButton ID="imgShowClock" runat="server" Visible="false" ToolTip="Show Clock"
                                    Width="20px" Height="20px" ImageUrl="~/images/clockTime.jpg" OnClick="imgShowClock_Click" />
                                <asp:LinkButton ID="lnkShowClock" runat="server" Visible="false" OnClick="lnkShowClock_Click"
                                    Font-Size="X-Small" Style="text-decoration: none" ToolTip="Show Clock">Show Clock</asp:LinkButton>
                                <asp:Label ID="Label4" runat="server" Text="Remaining Time :"></asp:Label>&nbsp;&nbsp;<asp:Label
                                    ID="Label2" runat="server" />
                                <asp:Timer ID="Timer1" runat="server" Interval="3600" OnTick="Timer1_Tick">
                                </asp:Timer>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblPreviousTime" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Label ID="lblSectionNo" runat="server" Text="Label" Visible="false"></asp:Label>
            <asp:Panel ID="Panel6" runat="server">
                <%--<asp:Panel ID="Panel2" runat="server" Visible="false">
                    <center>
                        <table>
                            <tr>
                                <td>
                                    <h4>
                                        Course Name :</h4>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                        DataValueField="courseid">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblSolvedQuestions" runat="server" Visible="False"></asp:Label>
                                    <asp:Label ID="lblQueAns" runat="server" Visible="False"></asp:Label>
                                    <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                        SelectCommand="SELECT [courseid], [catid], [courseinfo] FROM [exm_course]"></asp:SqlDataSource>
                                </td>
                                <td>
                                    <h4>
                                        No of Questions :</h4>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlno" runat="server">
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>20</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Button ID="btnSub" runat="server" Text="Submit" OnClick="btnSub_Click" />
                                    <asp:Button ID="Button4" runat="server" Text="Button" OnClientClick="openNewWindow();" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>--%>
                <asp:Label ID="lblQuestionList" runat="server" Text="Label" Visible="False"></asp:Label>
                <asp:Label ID="lblGivenAns" Visible="false" runat="server" Text=""></asp:Label>
                <asp:Panel ID="Panel3" runat="server" Visible="false">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:Panel ID="Panel8" runat="server">
                                    <table>
                                        <tr>
                                            <td align="left" style="width: 15px">
                                                <asp:Label ID="lblSectionsLabel" runat="server" Text="Sections"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:Repeater ID="Repeater2" runat="server">
                                                    <ItemTemplate>
                                                        <div style="float: left; width: 45px">
                                                            <asp:Button ID="Button1" runat="server" Text='<%# Eval("number") %>' CommandArgument='<%#Eval("serialNo") %>'
                                                                Width="40px" OnClick="btnRep2_Click" OnClientClick="return checkconnection();" />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lblSections" runat="server" Text="" Visible="false"></asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="linkShowDirections" runat="server" CausesValidation="false" OnClick="linkShowDirections_Click">Exam Directions/Instructions</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                                    <ItemTemplate>
                                        <div style="float: left; width: 40px">
                                            <asp:Button ID="Button1" runat="server" Text='<%# Eval("serialnumber") %>' CommandArgument='<%#Eval("questid") %>'
                                                Width="35px" OnClick="btnRep_Click" OnClientClick="return checkconnection();" />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <hr style="border-bottom: solid 1px black" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel1" runat="server">
                        <table width="100%">
                            <tr>
                                <td align="left" valign="top">
                                    <%-- <div style="max-width: 500px; position: relative; float: left">--%>
                                    <asp:HyperLink ID="hrli" runat="server" Style="color: Black; text-decoration: none;
                                        cursor: pointer">
                                        <asp:Image ID="Image1" runat="server" Width="10px" Height="10px" />
                                        &nbsp; Common Data</asp:HyperLink>
                                    <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Visible="false">
                                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="Server" TargetControlID="Panel11"
                                            CollapsedSize="2" Collapsed="False" ExpandControlID="hrli" CollapseControlID="hrli"
                                            AutoCollapse="False" AutoExpand="False" ImageControlID="Image1" ExpandedImage="/images/arrow-up.gif"
                                            CollapsedImage="/images/arrow-dn.gif" ExpandDirection="Vertical" />
                                        <asp:Panel ID="Panel11" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Repeater ID="repQueInfo" runat="server">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("infContents") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <%-- </div>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <div style="width: 99%; min-width: 500px; float: left; vertical-align: top">
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" style="float: left">
                                                        <asp:Label ID="lblQuestionNo" runat="server" Text="Label" Visible="false"></asp:Label>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table style="float: left">
                                                                    <tr>
                                                                        <td valign="top" style="width: 55px" align="left">
                                                                            Question<br />
                                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <asp:ImageButton ID="iBtnMark" runat="server" ImageUrl="~/images/mark.png" Width="30px"
                                                                                        Height="25px" OnClick="iBtnMark_Click" Visible="false" ToolTip="Mark" />
                                                                                    <asp:ImageButton ID="iBtnUnMark" runat="server" ImageUrl="~/images/unmark.png" Width="30px"
                                                                                        Height="25px" OnClick="iBtnUnMark_Click" Visible="false" ToolTip="Unmark" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                        <td valign="top" style="width: 15px" align="left">
                                                                            <asp:Label ID="lblSelectedQuesNo" runat="server" Text="Label"></asp:Label>:
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblQue" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" style="float: left">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:RadioButton ID="rb1" runat="server" OnClick="javascript:CheckOnOff();" />
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:Label ID="lblQue1" runat="server" Text="1)"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOptionA" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" style="float: left">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:RadioButton ID="rb2" runat="server" OnClick="javascript:CheckOnOff1();" />
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:Label ID="lblQue2" runat="server" Text="2)"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOptionB" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" style="float: left">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:RadioButton ID="rb3" runat="server" OnClick="javascript:CheckOnOff2();" />
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:Label ID="lblQue3" runat="server" Text="3)"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOptionC" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" style="float: left">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:RadioButton ID="rb4" runat="server" OnClick="javascript:CheckOnOff3();" />
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:Label ID="lblQue4" runat="server" Text="4)"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOptionD" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" style="float: left">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:RadioButton ID="rb5" runat="server" OnClick="javascript:CheckOnOff4();" />
                                                                        </td>
                                                                        <td valign="top" align="left" style="width: 5px">
                                                                            <asp:Label ID="lblQue5" runat="server" Text="5)"></asp:Label>
                                                                        </td>
                                                                        <td valign="top" align="left">
                                                                            <asp:Label ID="lblOptionE" runat="server" Text="Label" CssClass="myLabel22"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:LinkButton ID="linkCleraOptions" runat="server" OnClientClick="javascript:hideLink();"></asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton7" Visible="false" runat="server" OnClientClick="javascript:hideLink1();">Clear</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table width="100%">
                        <tr>
                            <td>
                                <hr style="border-bottom: solid 1px black" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <%--<asp:Button ID="Button2" runat="server" Text="Submit And Next" CausesValidation="False"
                                    OnClick="Button2_Click" CssClass="simplebtn" />--%>
                                <table width="100%" style="background-color: White">
                                    <tr>
                                        <td align="left">
                                            <asp:Button ID="btnMark" runat="server" Text="Mark" CssClass="simplebtn" OnClick="btnMark_Click"
                                                Visible="false" />
                                            <asp:Button ID="btnReview" runat="server" Text="Exam Review" CssClass="simplebtn"
                                                OnClick="btnReview_Click" Visible="false" />
                                            <%--  <asp:Button ID="btnSwitchView" runat="server" Text="Question List" CssClass="simplebtn"
                                                CausesValidation="false" OnClick="btnSwitchView_Click" />--%>
                                            <asp:LinkButton ID="linkBtnQList" runat="server" OnClick="btnSwitchView_Click" ForeColor="Black"
                                                Font-Bold="true">Question List</asp:LinkButton>
                                        </td>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <%-- <asp:Button ID="btnPre" runat="server" Text="Previous" CssClass="simplebtn" OnClick="btnPre_Click"
                                                            Enabled="false" />--%>
                                                        <asp:LinkButton ID="lBtnPre" runat="server" OnClick="btnPre_Click" ForeColor="Black"
                                                            Enabled="false" Visible="false" Style="text-decoration: none" Font-Bold="true"><img src="images/imgPre.jpg" height="15px" width="15px" alt="" /> &nbsp;Previous</asp:LinkButton>
                                                        <span>|</span>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="queNo" runat="server" Text="Label" CssClass="myLabel1" Font-Bold="true"></asp:Label>
                                                        <span>|</span>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lBtnNext" runat="server" OnClick="btnNext_Click" ForeColor="Black"
                                                            Style="text-decoration: none" Font-Bold="true">Next &nbsp; <img src="images/imgNext.jpg" alt="" height="15px" width="15px" /></asp:LinkButton>
                                                        <%--  <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="simplebtn" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <%--   <asp:GridView ID="GridView1" runat="server" Visible="false">
                                </asp:GridView>--%>
                                <asp:Label ID="lblExamID" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lbluserID" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <%--  <asp:SqlDataSource ID="sourceAttempts" runat="server"></asp:SqlDataSource>
               <asp:GridView ID="gridAttempts" CssClass="gridview" DataSourceID="sourceAttempts"
                    Width="250px" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="attemptDate" HeaderText="Date Attempted" />
                    </Columns>
                </asp:GridView>--%></asp:Panel>
                <div style="display: none">
                    <asp:LinkButton ID="linkReview" runat="server">LinkButton</asp:LinkButton>
                    <asp:LinkButton ID="linkSwitchView" runat="server">LinkButton</asp:LinkButton>
                </div>
                <asp:ModalPopupExtender ID="modalPopupReview" runat="server" TargetControlID="linkReview"
                    BackgroundCssClass="ModalPopupBG" PopupControlID="examReview" Drag="true" CancelControlID="btnReviewCan">
                </asp:ModalPopupExtender>
                <div id="examReview" style="display: none;" class="popupConfirmation">
                    <iframe id="Iframe4" frameborder="0" src="examReview.aspx" height="600px" width="600px">
                    </iframe>
                    <div class="popup_Buttons" style="display: none">
                        <input id="btnreviewOk" value="Done" type="button" />
                        <input id="btnReviewCan" value="Cancel" type="button" />
                    </div>
                </div>
                <asp:ModalPopupExtender ID="ModalPopupSwitchView" runat="server" TargetControlID="linkSwitchView"
                    BackgroundCssClass="ModalPopupBG" PopupControlID="switchView" Drag="true" CancelControlID="btnReviewCan">
                </asp:ModalPopupExtender>
                <div id="switchView" style="display: none;" class="popupConfirmation">
                    <iframe id="Iframe2" frameborder="0" src="allQuestions.aspx" height="600px" width="700px">
                    </iframe>
                    <div class="popup_Buttons" style="display: none">
                        <input id="btnOkSView" value="Done" type="button" />
                        <input id="btnCanSView" value="Cancel" type="button" />
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="modalPauseExam" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="pauseExam" Drag="true" CancelControlID="btnCanPause">
            </asp:ModalPopupExtender>
            <div id="pauseExam" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe8" frameborder="0" src="pauseExam.aspx" height="150px" width="200px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="btnOkPause" value="Done" type="button" />
                    <input id="btnCanPause" value="Cancel" type="button" />
                </div>
            </div>
            <asp:ModalPopupExtender ID="modalShowDirections" runat="server" TargetControlID="LinkButton2"
                BackgroundCssClass="ModalPopupBG" PopupControlID="examDirections" Drag="true"
                CancelControlID="btnCanDirPage">
            </asp:ModalPopupExtender>
            <div id="examDirections" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe1" frameborder="0" src="examDirections.aspx" height="600px" width="800px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="btnOkDirPage" value="Done" type="button" />
                    <input id="btnCanDirPage" value="Cancel" type="button" />
                </div>
            </div>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
                <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
                <asp:Button ID="Button3" runat="server" Text="Button" CausesValidation="false" OnClick="Button1_Click" /></div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="panel123">
        <p>
            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="Panel9" runat="server" ScrollBars="Auto" Height="500px">
                        <center>
                            <asp:Label ID="lblAnswerSheet" runat="server" Text="Label" Visible="false" Font-Bold="true"
                                ForeColor="white"></asp:Label>
                            <asp:Repeater ID="repAnsSheet" runat="server">
                                <HeaderTemplate>
                                    <table class="gridtable" width="100%">
                                        <tr>
                                            <th style="width: 30px" align="left">
                                                Que No
                                            </th>
                                            <th style="width: 50px" align="left">
                                                Marked
                                            </th>
                                            <th style="width: 20px" align="left">
                                                1
                                            </th>
                                            <th style="width: 20px" align="left">
                                                2
                                            </th>
                                            <th style="width: 20px" align="left">
                                                3
                                            </th>
                                            <th style="width: 20px" align="left">
                                                4
                                            </th>
                                            <th style="width: 20px" align="left">
                                                5
                                            </th>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table class="gridtable1" width="100%">
                                        <tr>
                                            <td style="width: 30px" align="left">
                                                &nbsp;&nbsp;<asp:LinkButton ID="LinkButton3" runat="server" CommandArgument='<%#Eval("questid") %>'
                                                    Text='<%#Eval("serialnumber")%>' OnClick="btnToQue_Click" ToolTip='<%#Eval("serialnumber") %>'
                                                    ForeColor="Black" Style="text-decoration: none" OnClientClick="return checkconnection();"></asp:LinkButton>
                                            </td>
                                            <td style="width: 50px" align="center">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("IsMarked")%>'></asp:Label>
                                                <asp:Label ID="lblQid" Visible="false" runat="server" Text='<%#Eval("questid") %>'></asp:Label>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%--<asp:RadioButton ID="RadioButton1" runat="server" Enabled="false" ForeColor="Black"
                                                    Style="color: black; width: 25px; height: 25px;" />--%>
                                                <asp:Label ID="lblOption1" runat="server" Text="Label"></asp:Label>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%-- <asp:RadioButton ID="RadioButton2" runat="server" Enabled="false" />--%>
                                                <asp:Label ID="lblOption2" runat="server" Text="Label"></asp:Label>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%-- <asp:RadioButton ID="RadioButton3" runat="server" Enabled="false" />--%>
                                                <asp:Label ID="lblOption3" runat="server" Text="Label"></asp:Label>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%--   <asp:RadioButton ID="RadioButton4" runat="server" Enabled="false" />--%>
                                                <asp:Label ID="lblOption4" runat="server" Text="Label"></asp:Label>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%--<asp:RadioButton ID="RadioButton5" runat="server" Enabled="false" />--%>
                                                <asp:Label ID="lblOption5" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%--    <asp:Repeater ID="repAnswerSheet" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <table class="gridtable">
                                        <tr>
                                            <th style="width: 30px" align="left">
                                                Que No
                                            </th>
                                            <th style="width: 50px" align="left">
                                                Marked
                                            </th>
                                            <th style="width: 20px" align="left">
                                                1
                                            </th>
                                            <th style="width: 20px" align="left">
                                                2
                                            </th>
                                            <th style="width: 20px" align="left">
                                                3
                                            </th>
                                            <th style="width: 20px" align="left">
                                                4
                                            </th>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table class="gridtable">
                                        <tr>
                                            <td style="width: 30px" align="left">
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandArgument='<%#Eval("questid") %>'
                                                    Text='<%#Eval("serialnumber")%>' OnClick="btnToQue_Click" ToolTip='<%#Eval("serialnumber") %>'></asp:LinkButton>
                                            </td>
                                            <td style="width: 50px" align="center">
                                                <%#Eval("IsMarked")%>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%#Eval("option1")%>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%#Eval("option2")%>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%#Eval("option3")%>
                                            </td>
                                            <td style="width: 20px" align="center">
                                                <%#Eval("option4")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>--%>
                        </center>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </p>
    </div>
    <asp:GridView ID="GridView2" runat="server" Visible="false">
    </asp:GridView>
    <asp:Panel ID="Panel10" runat="server">
        <a class="trigger" href="#">Answer Sheet</a>
    </asp:Panel>
    </form>
</body>
</html>
