﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="study.aspx.cs" Inherits="OnlineExam.study" Title="Study Material" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="pageHeading" id="pageHeading">
                    Study Material
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <%--<div style="border-style: solid; border-width: 1px; padding-left: 4px; padding-right: 4px;
        padding-top: 1px; padding-bottom: 1px; background-color: #800000">
        <h2 align="center">
            <font color="#FFFFFF">Computer Knowledge (MCQ )</font></h2>
    </div>--%>
    <div id="comp" runat="server" visible="false">
        <p align="center" style="font-size: large; background-color: Red">
            <u><b><font color="blue">Sample MCQ for Computer</font></b></u>
        </p>
        <br />
        <p>
            <b>1. A high level programming language named after Ada Augusta, coworker with Charles
                Babbage–</b></p>
        <p>
            1. Augustan
            <br />
            2. Babbage<br />
            3. Ada
            <br />
            4. Charlie<br />
            5. All of the above</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. Ada')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>2. Acknowledgement from a computer that a packet of data has been received and verified
                is known as–</b></p>
        <p>
            1. ACK
            <br />
            2. BCK<br />
            3. ECK
            <br />
            4. All of the above<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. ACK')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>3. The following computer’s memory is characterized by low cost per bit stored–</b></p>
        <p align="justify">
            1. Primary
            <br />
            2. Secondary<br />
            3. Hard disk
            <br />
            4. All of the above<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 2. Secondary')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>4. The following is true for Auxiliary Storage–</b></p>
        <p align="justify">
            1. It has an operating speed far slower than that of the primary storage.<br />
            2. It has an operating speed faster than that of the primary storage.<br />
            3. It has an operating speed equivalent than that of the primary storage.<br />
            4. All of the above
            <br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. It has an operating speed far slower than that of the primary storage.')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>5. Following is true for Bandwidth–</b></p>
        <p align="justify">
            1. The narrow the bandwidth of a communications system the less data it can transmit
            in a given period of time.<br />
            2. The narrow then bandwidth of a communications system the more data it can transmit
            in a given period of time.<br />
            3. The wider the bandwidth of a communications system the less data it can transmit
            in a given period of time.<br />
            4. All of the above
            <br />
            5. None is true</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. The narrow the bandwidth of a communications system the less data it can transmit in a given period of time.')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>6. To identify particular location in storage area one have a–</b>
        </p>
        <p>
            1. Address
            <br />
            2. Password<br />
            3. Logic
            <br />
            4. Mouse<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Address ')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>7. The following is a communications device (modem) which allows an ordinary telephone
                to be used with a computer device for data transmission–</b></p>
        <p align="justify">
            1. Keyboard
            <br />
            2. Acoustic coupler<br />
            3. Mobile phone
            <br />
            4. All of the above<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 2. Acoustic coupler')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>8. ALGOL is the </b>
        </p>
        <p>
            1. High-level language
            <br />
            2. Low level language<br />
            3. Machine language
            <br />
            4. All of the above<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. High-level language')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>9. Following is a logic circuit capable of forming the sum of two or more quantities–</b></p>
        <p>
            1. Adder
            <br />
            2. Multiplier<br />
            3. Address
            <br />
            4. Access<br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Adder')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>10. Assembly language is–</b></p>
        <p>
            1. Low-level programming language
            <br />
            2. High level programming language
            <br />
            3. Machine language<br />
            4. All of the above
            <br />
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Low-level programming language ')"
            class="simplebtn" />
    </div>
    <div id="english" runat="server" visible="false">
        <p align="center" style="font-size: large; background-color: Red">
            <u><b><font color="blue">Sample MCQ for English</font></b></u>
        </p>
        <p>
            <b><u>One-Word Substitution</u></b>
        </p>
        <p>
            <b>1. A house for storing grains</b></p>
        <p>
            (1) Cellar
            <br />
            (2) Store<br />
            (3) Godown
            <br />
            (4) Granary
            <br />
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) Granary ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>2. A name adopted by an author in his writings </b>
        </p>
        <p>
            (1) Title
            <br />
            (2) Nomenclature<br />
            (3) Nickname
            <br />
            (4) Pseudonym
            <br />
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) Pseudonym')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>3. Through which light cannot pass </b>
        </p>
        <p>
            (1) Dull
            <br />
            (2) Dark<br />
            (3) Obscure
            <br />
            (4) Opaque<br />
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) Opaque')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b><u>Sentence Correction</u></b>
        </p>
        <p>
            <b>4. The <u>people of Orleans</u>, <u>when they first saw</u> her <u>in their city</u>
                thought <u>she was an</u> angel. <u>No error</u></p>
        </b>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;(1)
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            (2)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;(3)&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (4)&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;(5)
        </p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (3) in their city ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b><u>Analogy</u></b></p>
        <p>
            <b>5. CRIME : PUNISHMENT<br>
            </b>(1) lawyer : judge
            <br>
            (2) court : room<br>
            (3) accused : defendant<br>
            (4) homicide : penalty<br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) homicide : penalty')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>6. NUTS : BOLTS<br>
            </b>(1) nitty : gritty
            <br>
            (2) bare : feet<br>
            (3) naked : clothed<br>
            (4) hard : soft<br>
            (5) None of these</p>
        <form name="myform3">
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (1) nitty : gritty ')"
            class="simplebtn" /><br />
        <br />
        </form>
        <p>
            <b>7. BEE : HONEY
                <br>
            </b>(1) wolf : cub
            <br>
            (2) spider : web<br>
            (3) goat : cheese<br>
            (4) ant : hill<br>
            (5) None of these
        </p>
        <form name="myform3">
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) ant : hill')"
            class="simplebtn" /><br />
        <br />
    </div>
    <div id="reasoning" runat="server" visible="false">
        <p align="center" style="font-size: large; background-color: Red">
            <u><b><font color="blue">Sample MCQ for Reasoning</font></b></u>
        </p>
        <p>
            <b>1. If the order of letters of each of the follwoing words is reversed, then which
                of the following will be the meaningful word? If more than one such word can be
                formed, mark 'S' as the answer and if no such word can be formed, mark 'X' as the
                answer. NAIL, PAIL, RAIL, MADAM, REST.</b></font><br>
            (1) PAIL
            <br>
            (2) RAIL
            <br>
            (3) MADAM<br>
            (4) S
            <br>
            (5) X</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (4) S ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>2. If the letters in the word UNDERTAKING are rearranged in the alphabetical order,
                which one will be in the middle in order after the rearrangement?<br>
            </b>(1) G
            <br>
            (2) I
            <br>
            (3) K<br>
            (4) N
            <br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (3) K')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>3. Which letter in the word CYBERNETICS occupies the same position as it does in
                the English alphabet?<br>
            </b>(1) C
            <br>
            (2) E
            <br>
            (3) I<br>
            (4) T
            <br>
            (5) None of these
        </p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (3) I')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>4. If the first and second letters in the word DEPRESSION were interchanged, also
                the third and the fourth letters, the fifth and the sixth letters and so on, which
                of the following would be the seventh letter from the right?<br>
            </b>(1) R
            <br>
            (2) O
            <br>
            (3) S<br>
            (4) I
            <br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (5) None of these ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>5. If the positions of the first and the sixth letters in the word DISTRIBUTE are
                interchanged; similarly the positions of the second and the seventh, the third and
                the eighth and so on, which of the following letters will be the fifth from left
                after interchanging the positions?</b><br>
            (1) E
            <br>
            (2) I
            <br>
            (3) S<br>
            (4) T
            <br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (1) E ')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>6. The positions of the first and the eighth letters in the word WORKINGS are interchanged.
                Similarly the positions of the second and the seventh letters are interchanged and
                the positions of the third letter and the sixth letter are interchanged and the
                positions of the remaining two letters are interchanged with each other, which of
                the following will be the third letter to the left of R after rearrangement?</b><br>
            (1) G
            <br>
            (2) I
            <br>
            (3) N<br>
            (4) S
            <br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (3) N')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>7. The positions of how many letters in the word WONDERFUL will remain unchanged
                when the letters within the word are arranged alphabetically?<br>
            </b>(1) None
            <br>
            (2) One
            <br>
            (3) Two<br>
            (4) Three
            <br>
            (5) More than three</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (2) One')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>8. If the letters of the word TRANSFORM are rearranged as they appear in the English
                alphabet, then the position of how many letters will remain unchanged after such
                rearrangement?<br>
            </b>(1) None
            <br>
            (2) One
            <br>
            (3) Two<br>
            (4) Three
            <br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (2) One')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>9. The position of how many letters in the word BRAKES remains unchanged when they
                are arranged in alphabetical order?<br>
            </b>(1) One
            <br>
            (2) Two<br>
            (3) Three
            <br>
            (4) More than three<br>
            (5) None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (2) Two')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>10. Two letters in the word COUPLE have as many letters between them as in the alphabet.
                The letter which appears first in the alphabet is the answer. If there is no such
                pair of letters in the work, then mark your answer as X.</b><br>
            (1) L
            <br>
            (2) O
            <br>
            (3) P<br>
            (4) X
            <br>
            (5) C</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: (1) L ')"
            class="simplebtn" /><br />
        <br />
    </div>
    <div id="aptitude" runat="server" visible="false">
        <p align="center" style="font-size: large; background-color: Red">
            <u><b><font color="blue">Sample MCQ for Aptitude</font></b></u>
        </p>
        <p>
            <b>1. Five-eighth of three-tenth of four-ninth of a number is 45. What is the number?<br>
            </b>1. 470
            <br>
            2. 550
            <br>
            3. 560<br>
            4. 540
            <br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 4. 540')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>2. Which of the following numbers should be added to 11158 to make it exactly divisible
                by 77?<br>
            </b>1. 9
            <br>
            2. 8
            <br>
            3. 6<br>
            4. 5
            <br>
            5. 7</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 5. 7 ')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>3. If n is odd, (11)<sup>n</sup> + 1 is divisible by:<br>
            </b>1. 11 + 1
            <br>
            2. 11 – 1
            <br>
            3. 11<br>
            4. 10 + 1
            <br>
            5. 10 – 1
        </p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. 11 + 1')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>4. A sum of money is divided among 160 males and some females in the ratio 16 : 21.
                Individually each male gets Rs. 4 and each female Rs. 3. The number of females is:</b></p>
        <p>
            1. 280
            <br>
            2. 298
            <br>
            3. 292<br>
            4. 293
            <br>
            5. 300
        </p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. 280')"
            class="simplebtn" /><br />
        <p>
            <b>5. In a factory men, women and children were employed in the ratio 8 : 5 : 1 to finish
                a job-and their individual wages were in the ratio 5 : 2 : 3. Total daily wages
                of all amount to Rs. 318. Find the total daily wages paid to each category.</b>
        </p>
        <p>
            1. Rs. 240, 60, 18
            <br>
            2. Rs. 210, 70, 38<br>
            3. Rs. 190, 95, 33
            <br>
            4. Rs. 240,45,15<br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Rs. 240, 60, 18 ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>6. Ram, Shiv and Ganesh assemble for a contributory party. Ram brings 3 apples while
                Shiv brings 5. Since Ganesh did not have any apples, he contributed Rs. 8. How many
                rupees should Ram and Shiv respectively get, assuming each of the three consumes
                an equal portion of the apples?<b></p>
        <p>
            1. 1, 7
            <br>
            2. 2, 5
            <br>
            3. 5, 3<br>
            4. 2, 6
            <br>
            5. 2,7</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. 1, 7')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>7. A factory employs skilled workers, unskilled workers and clerks in the proportion
                8 : 5 : 1 and the wages of a skilled worker as unskilled worker and a clerk are
                in the ratio 5 : 2 : 3. When 20 unskilled workers are employed, the total daily
                wages of all, amount to Rs. 318. Find the daily wages paid to each category of employees
                (Rs.):</b></p>
        <p>
            1. 240,57,19
            <br>
            2. 210,70,13
            <br>
            3. 230,65,12
            <br>
            4. 210,70,15<br>
            5. 240,60,18</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 5. 240,60,18')"
            class="simplebtn" /><br />
        <br />
        <p align="justify">
            <b>8. If the area of a rectangular plot increases by 30% while its breadth remains same,
                what will be the ratio of the areas of new and old figures?</b></p>
        <p align="justify">
            1. 4 : 3
            <br>
            2. 5 : 1
            <br>
            3. 4 : 7<br>
            4. 11 : 13
            <br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer:5. None of these')"
            class="simplebtn" /><br />
        <p>
            <b>9. A typist uses a sheet measuring 20 cm by 30 cm lengthwise. If a margin of 2 cm
                is left on each side and a 3 cm margin on top and bottom, then percent of the page
                used for typing is:</b>
        </p>
        <p>
            1. 40
            <br>
            2. 70
            <br>
            3. 64<br>
            4. 75
            <br>
            5. 36</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. 64 ')"
            class="simplebtn" /><br />
        <p>
            <b>10. What will be the cost of gardening 1 metre broad boundary around a rectangular
                plot having perimeter of 340 metres at the rate of Rs. 10 per square metre?</b></p>
        <p>
            1. Rs. 1900 2. Rs. 3600<br>
            3. Rs. 3440
            <br>
            4. cannot be determined<br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. Rs. 3440')"
            class="simplebtn" /><br />
    </div>
    <div id="marketing" visible="false" runat="server">
        <p align="center" style="font-size: large; background-color: Red">
            <u><b><font color="blue">Sample MCQ for Marketing</font></b></u>
        </p>
        <p>
            <b>1. Bancassurance is a relationship between Bank and–</b></p>
        <p>
            1. Education
            <br>
            2. Insurance Company<br>
            3. Employee
            <br>
            4. Customer<br>
            5. All of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 2. Insurance Company ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>2. BIM stands for–</b>
        </p>
        <p>
            1. Bank Insurance Model<br>
            2. Book In Management<br>
            3. Bank In Money<br>
            4. Bank Investment Model<br>
            5. All of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Bank Insurance Model ')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>3. Effective Communication is–</b></p>
        <p>
            1. Good Vocabulary<br>
            2. A sine–qua–non for marketing<br>
            3. Not required of demand exceeds supply<br>
            4. All of these<br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 2. A sine–qua–non for marketing')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>4. Which is not a part of 7 ps of marketing?</b></p>
        <p>
            1. Price
            <br>
            2. Policy<br>
            3. Product
            <br>
            4. People<br>
            5. Process</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. Product')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>5. Market Plan can be for­</b></p>
        <p>
            1. A brand
            <br>
            2. A product<br>
            3. Product line
            <br>
            4. All of these<br>
            5. None of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 4. All of these')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>6. Solid marketing strategy is the fundation of a–</b></p>
        <p>
            1. Sales
            <br>
            2. Budget<br>
            3. Well–written marketing plan<br>
            4. Market
            <br>
            5. All of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. Well–written marketing plan')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>7. Which is/are promotional content?</b></p>
        <p>
            1. Sales Promotion
            <br>
            2. Branding
            <br>
            3. Direct Marketing
            <br>
            4. Advertising
            <br>
            5. All of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 3. Direct Marketing')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>8. Marketing strategy means–</b></p>
        <p>
            1. To introduce in sales promotion scheme<br>
            2. Population
            <br>
            3. Perseverance<br>
            4. Demands
            <br>
            5. Networth</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. To introduce in sales promotion scheme')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>9. Customisation means–</b></p>
        <p>
            1. Making Few change according to client requirement to an already existing product<br>
            2. Test
            <br>
            3. Production<br>
            4. Costing
            <br>
            5. All of these</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 1. Making Few change according to client requirement to an already existing product')"
            class="simplebtn" /><br />
        <br />
        <p>
            <b>10. Marketing should be resorted–</b></p>
        <p>
            1. Only among rich person<br>
            2. Depends on income
            <br>
            3. Only in crowded areas<br>
            4. Depends on the product
            <br>
            5. Only among the poor</p>
        <input type="button" value="Click Here for Answer" onclick="alert('Answer: 4. Depends on the product')"
            class="simplebtn" /><br />
        <br />
    </div>
</asp:Content>
