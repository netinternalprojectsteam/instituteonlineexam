﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class printPaper : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                ddlExams.DataBind();
            }
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Visible = false;
                string examid = ddlExams.SelectedValue;
                string queNos = ob.getValue("SELECT questionNos FROM view_examDetails WHERE (examID = " + examid + " and instituteId=" + lblInsId.Text + ") ");
                if (queNos.Length > 0)
                {
                    string[] questions = queNos.Split(',');
                    string q1 = "";

                    for (int i = 0; i < questions.Length; i++)
                    {
                        if (i == 0)
                        {
                            q1 = q1 + Convert.ToInt32(questions[i].ToString());
                        }
                        else
                        {
                            q1 = q1 + "," + Convert.ToInt32(questions[i].ToString());
                        }
                    }
                    DataTable dt = ob.getData("SELECT questid, question, option1, option2, option3, option4, option5, infoID, case when infoID=0 then '0' else '1' end as borderInfo FROM view_examQuestions WHERE (questid IN (" + q1 + ") and instituteId=" + lblInsId.Text + ")");
                    GridView1.DataSource = dt;
                    GridView1.DataBind();


                    DataTable dtQuestions = new DataTable();
                    dtQuestions.Columns.Add("questid", typeof(string));
                    dtQuestions.Columns.Add("question", typeof(string));
                    dtQuestions.Columns.Add("option1", typeof(string));
                    dtQuestions.Columns.Add("option2", typeof(string));
                    dtQuestions.Columns.Add("option3", typeof(string));
                    dtQuestions.Columns.Add("option4", typeof(string));
                    dtQuestions.Columns.Add("option5", typeof(string));
                    dtQuestions.Columns.Add("infoID", typeof(string));
                    dtQuestions.Columns.Add("info", typeof(string));
                    dtQuestions.Columns.Add("srNo", typeof(string));
                    dtQuestions.Columns.Add("borderInfo", typeof(string));

                    for (int j = 0; j < GridView1.Rows.Count; j++)
                    {
                        if (Convert.ToInt32(GridView1.Rows[j].Cells[7].Text) > 1)
                        {

                            lblInfoID.Text = GridView1.Rows[j].Cells[7].Text;

                            if (lblInfoID.Text != lblUsedInfoID.Text)
                            {
                                DataRow dtrow = dtQuestions.NewRow();
                                dtrow["questid"] = dt.Rows[j][0].ToString(); ;
                                dtrow["question"] = dt.Rows[j][1].ToString(); ;
                                dtrow["option1"] = dt.Rows[j][2].ToString(); ;
                                dtrow["option2"] = dt.Rows[j][3].ToString(); ;
                                dtrow["option3"] = dt.Rows[j][4].ToString(); ;
                                dtrow["option4"] = dt.Rows[j][5].ToString(); ;
                                dtrow["option5"] = dt.Rows[j][6].ToString(); ;
                                dtrow["infoID"] = dt.Rows[j][7].ToString(); ;
                                DataTable dtInfo = new DataTable();
                                dtInfo = ob.getData("SELECT infContents FROM exm_queinfo WHERE (infID = " + GridView1.Rows[j].Cells[7].Text + ")");
                                dtrow["info"] = dtInfo.Rows[0][0].ToString();
                                dtrow["srNo"] = "Que " + (j + 1).ToString();
                                dtrow["borderInfo"] = dt.Rows[j][8].ToString();
                                dtQuestions.Rows.Add(dtrow);
                                lblUsedInfoID.Text = GridView1.Rows[j].Cells[7].Text;
                                continue;
                            }
                            if (lblInfoID.Text == lblUsedInfoID.Text)
                            {

                                DataRow dtrow = dtQuestions.NewRow();
                                dtrow["questid"] = dt.Rows[j][0].ToString();
                                dtrow["question"] = dt.Rows[j][1].ToString();
                                dtrow["option1"] = dt.Rows[j][2].ToString();
                                dtrow["option2"] = dt.Rows[j][3].ToString(); ;
                                dtrow["option3"] = dt.Rows[j][4].ToString();
                                dtrow["option4"] = dt.Rows[j][5].ToString();
                                dtrow["option5"] = dt.Rows[j][6].ToString();
                                dtrow["infoID"] = dt.Rows[j][7].ToString();
                                dtrow["info"] = "";
                                dtrow["srNo"] = "Que " + (j + 1).ToString();
                                dtrow["borderInfo"] = dt.Rows[j][8].ToString();
                                dtQuestions.Rows.Add(dtrow);
                            }
                        }
                        else
                        {
                            DataRow dtrow = dtQuestions.NewRow();
                            dtrow["questid"] = dt.Rows[j][0].ToString();
                            dtrow["question"] = dt.Rows[j][1].ToString();
                            dtrow["option1"] = dt.Rows[j][2].ToString();
                            dtrow["option2"] = dt.Rows[j][3].ToString();
                            dtrow["option3"] = dt.Rows[j][4].ToString();
                            dtrow["option4"] = dt.Rows[j][5].ToString();
                            dtrow["option5"] = dt.Rows[j][6].ToString();
                            dtrow["infoID"] = dt.Rows[j][7].ToString();
                            dtrow["srNo"] = "Que " + (j + 1).ToString();
                            dtrow["info"] = "";
                            dtrow["borderInfo"] = dt.Rows[j][8].ToString();
                            dtQuestions.Rows.Add(dtrow);

                        }
                    }
                    GridView2.DataSource = dtQuestions;
                    GridView2.DataBind();
                    repQuestions.DataSource = dtQuestions;
                    repQuestions.DataBind();
                    GridView2.Visible = false;
                    GridView1.Visible = false;

                    for (int x = 0; x < repQuestions.Items.Count; x++)
                    {
                        if (GridView1.Rows[x].Cells[2].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label3");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label3");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[3].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label4");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label4");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[4].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label5");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label5");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[5].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label6");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label6");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[6].Text.Trim().Length == 0 || GridView1.Rows[x].Cells[6].Text == "&nbsp;")
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label7");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQuestions.Items[x].FindControl("Label7");
                            lbl.Visible = true;
                        }
                    }

                    Panel1.Visible = true; Panel2.Visible = false; Panel3.Visible = false;
                }
                else
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("input string"))
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
        }

        protected void btnShowQueAns_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Visible = false;
                string examid = ddlExams.SelectedValue;
                string queNos = ob.getValue("SELECT questionNos FROM view_examDetails WHERE (examID = " + examid + " and instituteId=" + lblInsId.Text + ") ");
                if (queNos.Length > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Panel3.Visible = false;
                    string[] questions = queNos.Split(',');
                    string q1 = "";

                    for (int i = 0; i < questions.Length; i++)
                    {
                        if (i == 0)
                        {
                            q1 = q1 + Convert.ToInt32(questions[i].ToString());
                        }
                        else
                        {
                            q1 = q1 + "," + Convert.ToInt32(questions[i].ToString());
                        }
                    }
                    DataTable dt = ob.getData("SELECT  questid, question, option1, option2, option3, option4, option5, infoID,CorrectAns,'Que ' + Convert(varchar(200), row_number() over(order by questid)) as srNo FROM view_QuestionDetails WHERE (questid IN (" + q1 + ") and instituteId=" + lblInsId.Text + ")");
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    repQueWithAnswer.DataSource = dt;
                    repQueWithAnswer.DataBind();

                    for (int x = 0; x < repQueWithAnswer.Items.Count; x++)
                    {
                        if (GridView1.Rows[x].Cells[2].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label3");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label3");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[3].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label4");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label4");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[4].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label5");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label5");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[5].Text.Trim().Length == 0)
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label6");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label6");
                            lbl.Visible = true;
                        }
                        if (GridView1.Rows[x].Cells[6].Text.Trim().Length == 0 || GridView1.Rows[x].Cells[6].Text == "&nbsp;")
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label7");
                            lbl.Visible = false;
                        }
                        else
                        {
                            Label lbl = (Label)repQueWithAnswer.Items[x].FindControl("Label7");
                            lbl.Visible = true;
                        }
                    }

                }
                else
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("input string"))
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
        }

        protected void btnShowQue_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Visible = false;
                string examid = ddlExams.SelectedValue;
                string queNos = ob.getValue("SELECT questionNos FROM view_examDetails WHERE (examID = " + examid + " and instituteId=" + lblInsId.Text + ") ");
                if (queNos.Length > 0)
                {
                    Panel2.Visible = true;
                    Panel1.Visible = false;
                    Panel3.Visible = false;
                    string[] questions = queNos.Split(',');
                    string q1 = "";

                    for (int i = 0; i < questions.Length; i++)
                    {
                        if (i == 0)
                        {
                            q1 = q1 + Convert.ToInt32(questions[i].ToString());
                        }
                        else
                        {
                            q1 = q1 + "," + Convert.ToInt32(questions[i].ToString());
                        }
                    }
                    DataTable dt = ob.getData("SELECT  questid, question, option1, option2, option3, option4, option5, infoID,CorrectAns,'Que ' + Convert(varchar(200), row_number() over(order by questid)) as srNo FROM view_QuestionDetails WHERE (questid IN (" + q1 + ") and instituteId=" + lblInsId.Text + ")");
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    repQuestionsOnly.DataSource = dt;
                    repQuestionsOnly.DataBind();
                    Panel3.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false;
                }
                else
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("input string"))
                {
                    lblError.Visible = true;
                    Panel1.Visible = false; Panel2.Visible = false; Panel3.Visible = false;
                }
            }
        }
    }
}
