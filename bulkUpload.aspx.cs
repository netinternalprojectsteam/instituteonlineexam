﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.OleDb;
using System.IO;

namespace OnlineExam
{
    public partial class bulkUpload : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                ddlCourse.DataBind();
                try
                {
                    sourceSubjects.SelectCommand = "SELECT Distinct subjectinfo,subid from view_catCourseSub WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                    sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlSubjects.DataBind();
                }
                catch
                {
                    Response.Redirect("/ManageCourse.aspx");
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Visible = false;
                Messages11.Visible = false;
                if (FileUpload1.HasFile)
                {
                    if (FileUpload1.FileName.EndsWith(".xls") || FileUpload1.FileName.EndsWith(".xlsx"))
                    {

                        string path = FileUpload1.PostedFile.FileName;
                        FileUpload1.SaveAs(Server.MapPath("~\\UploadedDocs\\" + FileUpload1.FileName));
                        string xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Server.MapPath("~\\UploadedDocs\\" + FileUpload1.FileName) + ";" + "Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
                        OleDbConnection oledbcon = new OleDbConnection(xConnStr);
                        try
                        {
                            oledbcon.Open();
                            OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", oledbcon);
                            OleDbDataAdapter adptr = new OleDbDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            adptr.Fill(dt);

                            oledbcon.Close();
                            System.IO.File.Delete(Server.MapPath("~\\UploadedDocs\\" + FileUpload1.FileName));
                            if (dt.Rows.Count > 0)
                            {

                                string srCol = dt.Columns[0].ColumnName.ToLower();
                                string questionCol = dt.Columns[1].ColumnName.ToLower();
                                string option1Col = dt.Columns[2].ColumnName.ToLower();
                                string option2Col = dt.Columns[3].ColumnName.ToLower();
                                string option3Col = dt.Columns[4].ColumnName.ToLower();
                                string option4Col = dt.Columns[5].ColumnName.ToLower();
                                string option5Col = dt.Columns[6].ColumnName.ToLower();
                                string answerCol = dt.Columns[7].ColumnName.ToLower();
                                string markCol = dt.Columns[8].ColumnName.ToLower();
                                string negCol = dt.Columns[9].ColumnName.ToLower();
                                int fromQueNo = 0; int x = 0;


                                try
                                {
                                    if (srCol.ToLower() == "sr no" && questionCol.ToLower() == "question" && option1Col.ToLower() == "option1" && option2Col.ToLower() == "option2" && option3Col.ToLower() == "option3" && option4Col.ToLower() == "option4" && option5Col.ToLower() == "option5" && answerCol.ToLower() == "answer" && markCol.ToLower() == "mark" && negCol.ToLower() == "negative deduction")
                                    {

                                        int success = checkForProperDataType(dt);
                                        if (success == 1)
                                        {
                                            string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                                            for (int i = 0; i < dt.Rows.Count; i++)
                                            {
                                                if (dt.Rows[i][0].ToString().Length > 0 && dt.Rows[i][1].ToString().Length > 0 && dt.Rows[i][2].ToString().Length > 0 && dt.Rows[i][3].ToString().Length > 0)
                                                {
                                                    DataTable dtQueExists = new DataTable();
                                                    dtQueExists = obj.getQueByInstitute(dt.Rows[i][1].ToString().Trim().Replace("'", "''"), lblInsId.Text);
                                                    if (dtQueExists.Rows.Count > 0)
                                                    {
                                                        x = x + 1;
                                                    }
                                                    else
                                                    {
                                                        //option1 result = ob.insertStd(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(), dt.Rows[i][3].ToString());
                                                        int max = obj.AddNewQuestion(dt.Rows[i][1].ToString().Replace("'", "''").Trim(), dt.Rows[i][2].ToString().Trim(), dt.Rows[i][3].ToString().Trim(), dt.Rows[i][4].ToString().Trim(), dt.Rows[i][5].ToString().Trim(), dt.Rows[i][6].ToString().Trim(), dt.Rows[i][7].ToString().Trim(), ddlSubjects.SelectedValue, "Basic", val[0].ToString(), "0", Convert.ToDouble(dt.Rows[i][8].ToString().Trim()), Convert.ToDouble(dt.Rows[i][9].ToString().Trim()), lblInsId.Text);
                                                        if (max == 0)
                                                        {
                                                            x = x + 1;
                                                        }

                                                        if (i == 0)
                                                        {
                                                            fromQueNo = max;
                                                        }
                                                    }
                                                }
                                                else { x = x + 1; }
                                            }
                                            if (x > 0)
                                            {

                                                lblError.Visible = true;
                                                //Messages11.Visible = true;
                                                //Messages11.setMessage(0, x.ToString() + " Records not inserted !");
                                                lblError.Text = x.ToString() + " Records not inserted !";
                                            }

                                            int top = 0;
                                            top = dt.Rows.Count - x;
                                            DataTable dtab = new DataTable();
                                            dtab = obj.getQuestionsTable(fromQueNo);
                                            gridData.DataSource = dtab;
                                            gridData.DataBind();

                                            Messages11.Visible = true;
                                            Messages11.setMessage(1, "Data uploaded successfully");
                                        }

                                    }
                                    else
                                    {
                                        Messages11.Visible = true;
                                        Messages11.setMessage(0, "Uploaded Excel Sheet is not in proper format !");

                                    }
                                }
                                catch (Exception ee)
                                {
                                    Messages11.Visible = true;
                                    Messages11.setMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                                }

                            }

                        }
                        catch (Exception ee)
                        {
                            Messages11.Visible = true;
                            Messages11.setMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                        }
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Select proper Excel sheet !");
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Select proper Excel sheet !");
                }
            }
            catch (Exception ee)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
            }
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourse.SelectedItem.Text.Length > 0)
            {
                sourceSubjects.SelectCommand = "SELECT Distinct subjectinfo,subid from view_catCourseSub WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                ddlSubjects.DataBind();
            }

        }

        public int checkForProperDataType(DataTable dt)
        {
            int i = 0;
            int answer = 0; double mark = 0; double negmarks = 0;
            try
            {
                for (i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][1].ToString().Length > 0)
                    {
                        answer = Convert.ToInt32(dt.Rows[i]["answer"].ToString());
                        mark = Convert.ToInt32(dt.Rows[i]["mark"].ToString());
                        negmarks = Convert.ToDouble(dt.Rows[i]["negative Deduction"].ToString());
                    }
                    if (i == 75)
                    {

                    }
                }
                return 1;
            }
            catch
            {

                Messages11.Visible = true;
                Messages11.setMessage(0, "Check for proper Answer, Mark & Negative Deduction Column " + i + " !");
                //string a = dt.Rows[0][1].ToString();
                //answer = Convert.ToInt32(dt.Rows[i]["answer"].ToString());
                //mark = Convert.ToInt32(dt.Rows[i]["mark"].ToString());
                //negmarks = Convert.ToDouble(dt.Rows[i]["negative Deduction"].ToString());
                return 0;
            }
        }
    }
}
