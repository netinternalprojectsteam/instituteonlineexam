﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class emailConfig : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInstituteId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInstituteId.Text = val[3];
                }

                DataTable dt = new DataTable();
                dt = ob.getData("SELECT id, emailID, password, smtpHost, smtpPort, smtpEnable FROM email_Config where instituteId=" + lblInstituteId.Text + "");
                if (dt.Rows.Count > 0)
                {
                    lblID.Text = dt.Rows[0]["id"].ToString();
                    lblEmail.Text = dt.Rows[0]["emailID"].ToString();
                    txtAdd.Text = dt.Rows[0]["emailID"].ToString();
                    lblHost.Text = dt.Rows[0]["smtpHost"].ToString();
                    txtHost.Text = dt.Rows[0]["smtpHost"].ToString();
                    lblPort.Text = dt.Rows[0]["smtpPort"].ToString();
                    txtPort.Text = dt.Rows[0]["smtpPort"].ToString();
                    chkEnable.Checked = Convert.ToBoolean(dt.Rows[0]["smtpEnable"].ToString());
                    chkEnabled.Checked = Convert.ToBoolean(dt.Rows[0]["smtpEnable"].ToString());
                    Panel3.Visible = true;
                    Panel1.Visible = false;
                    btnUpdate.Text = "Update";
                    btnCan.Visible = true;
                }
                else
                {
                    Panel1.Visible = true;
                    Panel3.Visible = false;
                    btnUpdate.Text = "Add";
                    btnCan.Visible = false;
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt = ob.getData("SELECT id, emailID, password, smtpHost, smtpPort, smtpEnable FROM email_Config where instituteId=" + lblInstituteId.Text + "");
            if (dt.Rows.Count > 0)
            {
                lblID.Text = dt.Rows[0]["id"].ToString();
                lblEmail.Text = dt.Rows[0]["emailID"].ToString();
                txtAdd.Text = dt.Rows[0]["emailID"].ToString();
                lblHost.Text = dt.Rows[0]["smtpHost"].ToString();
                txtHost.Text = dt.Rows[0]["smtpHost"].ToString();
                lblPort.Text = dt.Rows[0]["smtpPort"].ToString();
                txtPort.Text = dt.Rows[0]["smtpPort"].ToString();
                chkEnable.Checked = Convert.ToBoolean(dt.Rows[0]["smtpEnable"].ToString());
                chkEnabled.Checked = Convert.ToBoolean(dt.Rows[0]["smtpEnable"].ToString());
                Panel3.Visible = false;
                Panel1.Visible = true;
                btnUpdate.Text = "Update";
                btnCan.Visible = true;
            }
            else
            {
                Panel1.Visible = false;
                Panel3.Visible = true;
                btnUpdate.Text = "Add";
                btnCan.Visible = false;
            }
        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            Panel3.Visible = true;
            Panel1.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (btnUpdate.Text == "Add")
            {
                ob.insertEmailConfig(txtAdd.Text, base64Encode(txtPass.Text), txtHost.Text, chkEnable.Checked, txtPort.Text, lblInstituteId.Text);
                Panel3.Visible = true;
                Panel1.Visible = false;
                lblEmail.Text = txtAdd.Text;
                lblHost.Text = txtHost.Text;
                lblPort.Text = txtPort.Text;
                chkEnabled.Checked = chkEnable.Checked;
            }
            else
            {
                ob.updateEmailConfig(txtAdd.Text, base64Encode(txtPass.Text), txtHost.Text, chkEnable.Checked, lblID.Text, txtPort.Text, lblInstituteId.Text);
                Panel3.Visible = true;
                Panel1.Visible = false;
                lblEmail.Text = txtAdd.Text;
                lblHost.Text = txtHost.Text;
                lblPort.Text = txtPort.Text;
                chkEnabled.Checked = chkEnable.Checked;
            }
        }

        private string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

    }
}
