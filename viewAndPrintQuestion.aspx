﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="viewAndPrintQuestion.aspx.cs"
    Inherits="OnlineExam.viewAndPrintQuestion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkQueAns12').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCanQueAns12').click();
        }
        
           function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
        
    </script>

    <style type="text/css">
        .paperLabel
        {
            color: black;
            font-size: medium;
        }
        .queNo
        {
            font-weight: bold;
            color: black;
            font-size: medium;
        }
    </style>

    <script type="text/javascript">
  function CallPrint(strid)
  {
      var prtContent = document.getElementById(strid);
      var WinPrint = window.open('','','letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
      WinPrint.document.write(prtContent.innerHTML);
      WinPrint.document.close();
      WinPrint.focus();
      WinPrint.print();
      WinPrint.close();

}
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Questions With Answers
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <div style="color: White">
                    X</div>
            </div>
        </div>
        <asp:Label ID="lblExamID" runat="server" Visible="false" Text="Label"></asp:Label>
        <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <table>
                        <tr>
                            <td align="right">
                                <input type="image" value="Print " id="Button1" onclick="window.print();" src="/images/printico.png"
                                    title="Print" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divPrint123">
                                    <asp:Repeater ID="repQueWithAnswer" runat="server">
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 130px;">
                                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("srNo") %>' CssClass="queNo"></asp:Label>
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>' CssClass="paperLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 100px;">
                                                                    <asp:Label ID="Label3" runat="server" Text="Correct Answer" CssClass="queNo"></asp:Label>
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("CorrectAns") %>' CssClass="paperLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 100px;">
                                                                    <asp:Label ID="Label5" runat="server" Text="Your Answer" CssClass="queNo"></asp:Label>
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("yourAns") %>' CssClass="paperLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="top" style="width: 100px;">
                                                                    <asp:Label ID="Label7" runat="server" Text="Explanation" CssClass="queNo"></asp:Label>
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="Label9" runat="server" Text='<%#Eval("questioninfo") %>' CssClass="paperLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <hr style="border-bottom: solid 1 px black" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
