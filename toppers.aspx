﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="toppers.aspx.cs" Inherits="OnlineExam.toppers" Title="Exam Toppers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td align="center">
                <h2>
                    Exm Toppers</h2>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:GridView ID="gridToppers" runat="server" AutoGenerateColumns="false" CssClass="gridview">
                    <Columns>
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="ExamName" DataField="examName" />
                        <asp:BoundField HeaderText="Candidate Name" DataField="uname" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
