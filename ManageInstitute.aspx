﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="ManageInstitute.aspx.cs" Inherits="OnlineExam.ManageInstitute" Title="Manage Institute" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 336px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table class="style1">
                <tr>
                    <td align="center" colspan="2">
                        <h3>
                            Manage Institute</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Institute :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtInstitute" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInstitute"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style2">
                        <br />
                        <asp:Label ID="lblActive" runat="server" Font-Bold="True" Text="Is Active :" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <br />
                        <asp:CheckBox ID="chkActive" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblInstituteId" runat="server" Visible="False"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnsubmit" runat="server" OnClick="Button1_Click" Text="Add New"
                            CssClass="simplebtn" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="simplebtn" OnClick="btnCancel_Click"
                            Visible="False" CausesValidation="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            DataKeyNames="InstituteId" DataSourceID="SqlDataSource1" OnRowCommand="GridView1_RowCommand"
                            OnRowCreated="GridView1_RowCreated" CssClass="gridview">
                            <Columns>
                                <asp:BoundField DataField="InstituteId" HeaderText="InstituteId" InsertVisible="False"
                                    ReadOnly="True" SortExpression="InstituteId" />
                                <asp:BoundField DataField="Institute" HeaderText="Institute" SortExpression="Institute" />
                                <asp:BoundField DataField="isActive" HeaderText="Is Active" SortExpression="isActive" />
                                <asp:ButtonField ButtonType="Button" CommandName="showByInstitute" HeaderText="Show Details"
                                    Text="Show Details">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="simplebtn" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT [InstituteId], [Institute], [isActive] FROM [exam_Institute]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
