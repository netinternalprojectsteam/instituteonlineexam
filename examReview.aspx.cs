﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class examReview : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblExamID.Text = Session["examID"].ToString();
                    lblUserID.Text = Session["userID"].ToString();
                    Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)Session["time"]) - DateTime.Now;
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    Timer1.Enabled = true;

                    //lblExamID.Text = "220";
                    //lblUserID.Text = "44";
                    DataTable dt = new DataTable();

                    dt = ob.GetUserInfoByID(lblUserID.Text);
                    lblName.Text = dt.Rows[0]["uname"].ToString();

                    //string questionList = ob.getValue("select questionlist from exm_newexam where newexm=" + lblExamID.Text + "");
                    //string[] questions = questionList.Split(',');

                    DataTable dtCompIncompQues = new DataTable();
                    //dtCompIncompQues = ob.getData("select questNo,case when answer='0' then 'Yes' else ' ' end as Incomplete, case when answer !='0' then 'Yes' else ' ' end as Complete from exm_useranswers where exmid=" + lblExamID.Text + "");
                    //dtCompIncompQues = ob.getData("select questNo,exmid,case when (select id from exm_markedQuestions where questionno=questno and examid=" + lblExamID.Text + " and userid = " + lblUserID.Text + ") is not null then 'Yes' else ' ' end as Marked ,case when answer='0' then 'Yes' else ' ' end as Incomplete, case when answer !='0' then 'Yes' else ' ' end as Complete from exm_useranswers where exmid=" + lblExamID.Text + " and userid = " + lblUserID + "");

                    dtCompIncompQues = ob.getData("select questNo,exmid,case when (select id from exm_markedQuestions where questionno=questno and examid=exmid and userid = userid) is not null then 'Yes' else ' ' end as Marked ,case when answer='0' then 'Yes' else ' ' end as Incomplete, case when answer !='0' then 'Yes' else ' ' end as Complete from exm_useranswers where exmid=" + lblExamID.Text + " and userid = " + lblUserID.Text + "");
                    gridReview.DataSource = dtCompIncompQues;

                    gridReview.DataBind();
                }
            }
            catch { }
        }

        protected void gridReview_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }

        }

        protected void btnMarked_Click(object sender, EventArgs e)
        {
            DataTable dtCompIncompQues = new DataTable();
            dtCompIncompQues = ob.getData("select questNo,exmid,case when (select id from exm_markedQuestions where questionno=questno and examid=exmid) is not null then 'Yes' else ' ' end as Marked  from exm_useranswers where exmid=" + lblExamID.Text + "");
            gridReview.DataSource = dtCompIncompQues;
            gridReview.DataBind();
        }

        protected void btnInc_Click(object sender, EventArgs e)
        {
            DataTable dtCompIncompQues = new DataTable();
            dtCompIncompQues = ob.getData("select questNo,exmid,case when answer='0' then 'Yes' else ' ' end as Incomplete from exm_useranswers where exmid=" + lblExamID.Text + "");
            gridReview.DataSource = dtCompIncompQues;
            gridReview.DataBind();
        }

        protected void totalReview_Click(object sender, EventArgs e)
        {
            DataTable dtCompIncompQues = new DataTable();
            dtCompIncompQues = ob.getData("select questNo,exmid,case when (select id from exm_markedQuestions where questionno=questno and examid=exmid and userid = userid) is not null then 'Yes' else ' ' end as Marked ,case when answer='0' then 'Yes' else ' ' end as Incomplete, case when answer !='0' then 'Yes' else ' ' end as Complete from exm_useranswers where exmid=" + lblExamID.Text + " and userid = " + lblUserID.Text + "");
            gridReview.DataSource = dtCompIncompQues;
            gridReview.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            Session["rTime"] = rTimeinMill;
            Timer1.Enabled = false;
            string script = "<script type=\"text/javascript\">callfun();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                TimeSpan time1 = new TimeSpan();
                time1 = ((DateTime)Session["time"]) - DateTime.Now;
                if (time1.Minutes <= 0 && time1.Seconds <= 0)
                {
                    Label2.Text = "TimeOut!";
                    DataTable dt = (DataTable)ViewState["myTable"];
                    Session["dtable"] = dt;
                    //  Session["ans"] = lblGivenAns.Text;


                    Session["examID"] = lblExamID.Text;
                    Session["userID"] = lblUserID.Text;

                    ob.updateExamStatus(lblExamID.Text);
                    Response.Redirect("exmResult.aspx");
                }
                else
                {
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours, time1.Minutes.ToString(), time1.Seconds.ToString());
                }
            }
            catch { }

        }



        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            try
            {
                foreach (GridViewRow row in gridReview.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        row.Attributes["onmouseover"] =
                           "this.style.cursor='hand';this.style.textDecoration='underline';";
                        row.Attributes["onmouseout"] =
                           "this.style.textDecoration='none';";
                        // Set the last parameter to True 
                        // to register for event validation. 
                        row.Attributes["onclick"] =
                         ClientScript.GetPostBackClientHyperlink(gridReview,
                            "Select$" + row.DataItemIndex, true);
                    }

                }


                base.Render(writer);
            }
            catch (Exception ee) { Response.Write(ee.Message); }

        }

        protected void gridReview_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gridReview_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                int row = Convert.ToInt16(e.CommandArgument);
                Session["qID"] = gridReview.Rows[row].Cells[1].Text;

                Session["JumpTo"] = "Yes";
                // string x = gridReview.Rows[row].Cells[0].Text;

                Session["qNo"] = (row + 1).ToString();

                //Session.Remove("JumpTo"); Session.Remove("qID"); Session.Remove("qNo");


                int rTimeinMill = 0;
                string[] rTime = Label2.Text.Split(':');

                if (rTime[0].Length > 0)
                {
                    if (Convert.ToInt32(rTime[0].ToString()) > 0)
                    {
                        rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                    }
                }

                if (rTime[1].Length > 0)
                {
                    if (Convert.ToInt32(rTime[1].ToString()) > 0)
                    {
                        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                    }
                }

                if (rTime[2].Length > 0)
                {
                    if (Convert.ToInt32(rTime[2].ToString()) > 0)
                    {
                        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                    }
                }
                Session["rTime"] = rTimeinMill;
                Timer1.Enabled = false;









                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }
    }
}
