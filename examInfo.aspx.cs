﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class examInfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        static string eID = "";
        static string eType = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    eID = Request.QueryString["eID"].ToString();
                    eType = Request.QueryString["etype"].ToString();
                    Session["examInfo"] = eID;
                    lblExamID.Text = eID;
                    DataTable dt = ob.getExamDetailsByID(eID);
                    if (dt.Rows.Count > 0)
                    {
                        DataTable dtExams = new DataTable();
                        dtExams.Columns.Add("examID", typeof(string));
                        dtExams.Columns.Add("ImageName", typeof(string));
                        dtExams.Columns.Add("examName", typeof(string));
                        dtExams.Columns.Add("maxTime", typeof(string));
                        dtExams.Columns.Add("noOfSections", typeof(string));
                        dtExams.Columns.Add("coursrPackageID", typeof(string));


                        dtExams.Columns.Add("negativeMarking", typeof(string));
                        dtExams.Columns.Add("queNos", typeof(string));
                        dtExams.Columns.Add("eDesc", typeof(string));

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dtrow = dtExams.NewRow();
                            dtrow["examID"] = dt.Rows[i]["examID"].ToString();
                            dtrow["examName"] = dt.Rows[i]["examName"].ToString();
                            dtrow["noOfSections"] = dt.Rows[i]["noOfSections"].ToString();
                            dtrow["coursrPackageID"] = dt.Rows[i]["coursrPackageID"].ToString();
                            //SELECT examID, examName, negativeMarking, coursrPackageID, noOfSections, eDesc, noofQuestions as queNos, coursrPackageID
                            dtrow["ImageName"] = dt.Rows[i]["ImageName"].ToString();
                            dtrow["negativeMarking"] = dt.Rows[i]["negativeMarking"].ToString();
                            dtrow["queNos"] = dt.Rows[i]["queNos"].ToString();
                            dtrow["eDesc"] = dt.Rows[i]["eDesc"].ToString();
                            DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt.Rows[i]["maxTime"].ToString()));
                            TimeSpan time1 = new TimeSpan();
                            time1 = ((DateTime)remTime1) - DateTime.Now;
                            string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                            dtrow["maxTime"] = r1;

                            dtExams.Rows.Add(dtrow);
                        }

                        rptExamInfo.DataSource = dtExams;
                        rptExamInfo.DataBind();
                        lblMessage.Visible = false;
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = false;
                        lblMessage.Visible = true;
                    }
                }
            }
            catch { Response.Redirect("/Default.aspx"); }
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                bool ISrdEmpty = true;
                string paidId = "";
                string fees = "";
                string attempt = "";
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                    if (eType == "Paid")
                    {
                        for (int i = 0; i < rptExamInfo.Items.Count; i++)
                        {
                            RadioButtonList rdb = (RadioButtonList)rptExamInfo.Items[i].FindControl("rdbPaid");
                            foreach (ListItem listItem in rdb.Items)
                            {
                                if (listItem.Selected)
                                {
                                    ISrdEmpty = false;
                                    string[] tmpData = listItem.Value.Split(':');
                                    paidId = tmpData[0];
                                    fees = tmpData[2];
                                    attempt = tmpData[1];

                                    break;
                                }
                                else
                                {
                                    ISrdEmpty = true;
                                }
                            }
                        }

                        if (ISrdEmpty == false)
                        {
                            lblError.Visible = false;
                            int i = ob.insertPaidExmUserDetails(val[0], eID, paidId, fees, attempt);
                            if (i > 0)
                            {
                                DataTable dtTransId = ob.getData("Select max(transID) from exm_paidExmUsers");
                                Session["transID"] = dtTransId.Rows[0][0].ToString();
                                Response.Redirect("/payConfirm.aspx");
                            }
                            else
                            {
                                lblError.Text = "Error Occurred!Please try again!";
                                lblError.Visible = true;
                            }
                        }
                        else
                        {
                            lblError.Text = "Please select Exam Package to Continue!!!";
                            lblError.Visible = true;
                        }
                    }
                    else
                    {
                        Session["resExam"] = "Yes";

                        lblUserID.Text = val[0].ToString();

                        DataTable dt = ob.getData("SELECT examID, examName,isnull(section1Ques,'') +','+ isnull(section2Ques,'') +','+ isnull(section3Ques,'') +','+ isnull(section4Ques,'' ) +','+isnull(section5Ques,'' ) as questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, examType FROM exm_existingExams where examID = " + lblExamID.Text + "");
                        ViewState["examDetails"] = dt;

                        string maxExamID = ob.getValue("select max(newexm)+1 from exm_newexam");
                        ob.InsertUserQuestionList(maxExamID, dt.Rows[0]["noofQuestions"].ToString(), dt.Rows[0]["questionNos"].ToString(), val[0].ToString(), Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()), dt.Rows[0]["courseID"].ToString(), "Pre Exam", dt.Rows[0]["noOfSections"].ToString(), lblExamID.Text);
                        string[] queNos = dt.Rows[0]["questionNos"].ToString().Split(',');
                        int noOfQuestions = Convert.ToInt32(dt.Rows[0]["noofQuestions"].ToString());
                        for (int i = 0; i < noOfQuestions; i++)
                        {
                            if (queNos[i].Length > 0)
                            {
                                ob.insertUseAnswer(val[0].ToString(), maxExamID, queNos[i].ToString(), "0", Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()));
                            }
                        }
                        Session["resPreExam"] = "No";
                        Session["examID"] = maxExamID;
                        Session["userID"] = val[0].ToString();
                        string script = "<script type=\"text/javascript\">openNewWindow()</script>";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    }
                }
                else
                {

                }
            }
            catch (Exception)
            { }
        }

        protected void rptExamInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (eType == "Paid")
                {
                    Panel pnlPaid = (Panel)e.Item.FindControl("pnlPaid");
                    DataTable dtPaidExams = new DataTable();
                    dtPaidExams = ob.getPaidExamDetailsByExamID(eID);
                    if (dtPaidExams.Rows.Count > 0)
                    {
                        RadioButtonList rdbPaid = (RadioButtonList)e.Item.FindControl("rdbPaid");
                        rdbPaid.DataSource = dtPaidExams;
                        rdbPaid.DataBind();
                        pnlPaid.Visible = true;
                    }
                    else
                    {
                        pnlPaid.Visible = false;
                    }
                }
            }
        }
    }
}
