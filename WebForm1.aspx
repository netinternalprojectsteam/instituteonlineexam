﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="OnlineExam.WebForm1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="obout_Editor" Namespace="OboutInc.Editor" TagPrefix="obout" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
    <form id="form1" runat="server">
    <span id="timerDisplaySpan"></span>
    <input type="hidden" id="timeAllocated" name="timeAllocated" value="30" runat="server" />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                SelectCommand="SELECT [examName], [noofQuestions], [coursrPackageID] FROM [exm_existingExams]">
            </asp:SqlDataSource>
            <fieldset style="width: 310px">
                <legend>Custom paging in Repeater</legend>
                <asp:Repeater ID="rptBooks" runat="server">
                    <HeaderTemplate>
                        <table style="border: 1px solid #c1650f; width: 300px" cellpadding="0">
                            <tr style="background-color: #ffa100; color: White">
                                <td colspan="2">
                                    <b>
                                        <center>
                                            Book Details</center>
                                    </b>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="background-color: #ffa100">
                            <td>
                                <table style="background-color: #f7f7f7; border-top: 1px dotted #c1650f; width: 300px">
                                    <tr>
                                        <td>
                                            <b>Name:</b>
                                            <asp:Label ID="lblBookName" runat="server" Text='<%#Eval("examName") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Questions:</b>
                                            <asp:Label ID="lblAuthor" runat="server" Text='<%#Eval("noofQuestions") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Category:</b>
                                            <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("coursrPackageID") %>' />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div style="overflow: hidden;">
                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnPage" Style="padding: 8px; margin: 2px; background: #ffa100;
                                border: solid 1px #666; font: 8pt tahoma;" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                runat="server" ForeColor="White" Font-Bold="True"><%# Container.DataItem %>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </fieldset>
            <br />
            <asp:Repeater ID="Repeater1" runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <b>Name:</b>
                            <asp:Label ID="lblBookName" runat="server" Text='<%#Eval("examName") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Questions:</b>
                            <asp:Label ID="lblAuthor" runat="server" Text='<%#Eval("noofQuestions") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Category:</b>
                            <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("coursrPackageID") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <table>
                <tr>
                    <td>
                        <asp:PlaceHolder ID="plcPaging" runat="server" />
                        <br />
                        <asp:Label runat="server" ID="lblPageName" />
                    </td>
                </tr>
            </table>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
