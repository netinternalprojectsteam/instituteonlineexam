﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class getInstitute : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["loginEmail"] != null)
                {
                    DataTable dt = new DataTable();
                    dt = ob.getUserInstituteByEmail(Session["loginEmail"].ToString(), base64Encode(Session["logPass"].ToString()));
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count > 1)
                        {
                            rptInstitute.DataSource = dt;
                            rptInstitute.DataBind();
                        }
                        else
                        {
                            Boolean chrem = Convert.ToBoolean(Session["remember"]);
                            if (chrem == true)
                            {
                                Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                            }
                            else
                            {
                                Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                            }
                            Response.Cookies["UserName"].Value = Session["loginEmail"].ToString().Trim();
                            Response.Cookies["Password"].Value = base64Encode(Session["logPass"].ToString().Trim());

                            FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "User" + "," + "NXGOnlineExam" + "," + dt.Rows[0]["instituteId"].ToString(), true);

                            Session["loginEmail"] = null;
                            Session["InstituteID"] = dt.Rows[0]["instituteId"].ToString();
                        }
                    }
                }
                else
                {
                    Response.Redirect("/LoginDetails.aspx");
                }
            }
        }

        protected void rptInstitute_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "select")
            {
                string[] insSplit = e.CommandArgument.ToString().Split(',');
                DataTable dt = ob.getInstituteByID(insSplit[0]);
                if (dt.Rows.Count > 0)
                {
                    Boolean chremm = Convert.ToBoolean(Session["remember"]);
                    if (chremm == true)
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                    }
                    else
                    {
                        Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                    }
                    Response.Cookies["UserName"].Value = Session["loginEmail"].ToString().Trim();
                    Response.Cookies["Password"].Value = base64Encode(Session["logPass"].ToString().Trim());

                    FormsAuthentication.RedirectFromLoginPage(insSplit[1].ToString() + "," + "User" + "," + "NXGOnlineExam" + "," + insSplit[0].ToString(), true);
                    Session["InstituteID"] = insSplit[0];
                    Session["loginEmail"] = null;
                }
                else
                {
                    string script = "<script>alert('Selected Institute is not in the list! Please select another!!')</script>";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", "alert('Selected Institute is not in the list! Please select another!');", true);
                }
            }
        }

        private string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

    }
}
