﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt = ob.GetUsernameAndPassword(txtUName.Text, txtPass.Text);
            if (dt.Rows.Count > 0)
            {
                FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + dt.Rows[0]["adminType"].ToString() + "," + "NXGOnlineExam" + "," + dt.Rows[0]["instituteId"].ToString(), true);
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Invalid Login details !");
            }

        }
    }
}
