﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string n = "abcd";
            //char[] ar = n.ToCharArray();
            //Array.Reverse(ar);
            //string s = new string(ar);.aspx
            try
            {
                if (!IsPostBack)
                {
                    if (HttpContext.Current.User.Identity.Name.ToString().Length > 0)
                    {
                        string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                        if (val[2].ToString() == "NXGOnlineExam")
                        {
                            if (val[1].ToString() == "Admin")
                            {
                                // Panel1.Visible = false;
                                //linkLogout.Visible = true;
                            }

                        }

                    }
                    string Lpath = Request.Url.LocalPath.ToString();
                    if (Lpath.Contains("testsList.aspx") || Lpath.Contains("LoginDetails.aspx") || Lpath.Contains("adminlogin.aspx"))
                    {
                        //  Panel2.Visible = false;
                    }

                    if (Lpath.Contains("studyDetails.aspx"))
                    {
                         Panel1.Visible = false;
                    }
                }
            }
            catch { }
        }

        protected void linkLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cookies.Clear();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void btnMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["LoginDetails"].ToString().Length > 0)
                {
                    string[] val1 = Session["LoginDetails"].ToString().Split('#');
                    //dt.Rows[0][0].ToString() + "#" + "User" + "#" + chkRem.Checked;
                    if (val1[2].ToString() == "true")
                    {
                        FormsAuthentication.RedirectFromLoginPage(val1[0].ToString() + "," + val1[1].ToString() + "," + "NXGOnlineExam", true);
                    }
                    else { FormsAuthentication.RedirectFromLoginPage(val1[0].ToString() + "," + val1[1].ToString() + "," + "NXGOnlineExam", false); }

                }
            }
            catch { }
        }
    }
}
