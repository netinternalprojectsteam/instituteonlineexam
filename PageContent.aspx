﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true" CodeBehind="PageContent.aspx.cs" Inherits="OnlineExam.PageContent" Title="Edit Page Content" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
        document.getElementById("ctl00_ContentPlaceHolder1_Button2").click();     
       }
   
   
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div style="display: none">
        </div>
    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    Edit Page Content</h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                &nbsp;
                <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                &nbsp;
            </td>
        </tr>
         <tr>
            <td align="left">
                <asp:SqlDataSource ID="sourceInfoHead" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                    SelectCommand="SELECT [pageid], [PageName] FROM [tbl_FrontPagesDetails]"></asp:SqlDataSource>
                <asp:Label ID="Label5" runat="server" Text="Menu Name" Font-Bold="true"></asp:Label>
                <asp:DropDownList ID="ddlInfoHead" runat="server" DataSourceID="sourceInfoHead" DataTextField="PageName"
                    DataValueField="pageid" 
                    AutoPostBack="True" CausesValidation="false" OnSelectedIndexChanged="ddlInfoHead_SelectedIndexChanged" AppendDataBoundItems="true">
               <asp:ListItem>--Select--</asp:ListItem>
                </asp:DropDownList>
                
                <br />
                <br />
               
            </td>
        </tr>
        <tr>
        <td>
            <asp:Panel ID="Panel2" runat="server" Visible="false">
            
            <asp:Label ID="lblcontent" runat="server" Text=""></asp:Label><br />
            <strong style="text-align:center;">
                <asp:LinkButton ID="lnkedit" runat="server" onclick="lnkedit_Click">Edit</asp:LinkButton>
            </strong>
            </asp:Panel>
        </td>
        </tr>
        <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
           
        <table width="100%">
        
        
       
        <tr>
            <td align="left">
               
                <FCKeditorV2:FCKeditor runat="server" id="CKEditorControl1" Height="400" Width="100%"></FCKeditorV2:FCKeditor>
            </td>
        </tr>
         <tr>
            <td align="center">
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
            </td>
        </tr>
        </table> </asp:Panel>
        </td>
        </tr>
       
    </table>
</asp:Content>
