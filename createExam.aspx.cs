﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;
using System.Web.Services;
using System.Text;
using System.Net;
using System.IO;

namespace OnlineExam
{
    public partial class createExam : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        public string finishType = "create";
        static string examName="";
        static string type = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(',');
                if (Session["InstituteID"] != null)
                {
                    lblInstituteId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInstituteId.Text = userDetails[3];
                }

                //repQuestions.DataBind();
                ddlCourse.DataBind();
                ddlCourses.DataBind();
                try
                {
                    sourceSubjects.SelectCommand = "SELECT Distinct subjectinfo,subid from view_catCourseSub WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                    sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlSubjects.DataBind();
                }
                catch
                {
                    Response.Redirect("/ManageCourse.aspx");
                }

                DataTable dt = new DataTable();
                //if (userDetails[1].ToLower() == "super admin")
                //{
                //    dt = ob.getData("SELECT TOP (10) examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseinfo, isAvailable, negativeMarking, noOfSections FROM view_examDetails ORDER BY examID DESC");
                //}
                //else
                //{
                //DataTable dt = ob.getData("SELECT TOP (10) examID, examName, case when len(questionnos)>0 then  questionNos else 'No Questions Added' end as questionNos, maxTime, userID, noofQuestions, examType,createdDate FROM exm_existingExams ORDER BY examID DESC");
                dt = ob.getData("SELECT  TOP (10) examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseinfo, isAvailable, negativeMarking, noOfSections FROM view_examDetails where instituteId=" + lblInstituteId.Text + " ORDER BY examID DESC");
                //}
                DataTable dtExams = new DataTable();
                dtExams.Columns.Add("examID", typeof(string));
                dtExams.Columns.Add("examName", typeof(string));
                dtExams.Columns.Add("questionNos", typeof(string));
                dtExams.Columns.Add("maxTime", typeof(string));
                dtExams.Columns.Add("noofQuestions", typeof(string));
                dtExams.Columns.Add("courseinfo", typeof(string));
                dtExams.Columns.Add("createdDate", typeof(string));
                dtExams.Columns.Add("isAvailable", typeof(string));
                dtExams.Columns.Add("negativeMarking", typeof(string));
                dtExams.Columns.Add("noOfSections", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dtrow = dtExams.NewRow();
                    dtrow["examID"] = dt.Rows[i]["examID"].ToString();
                    dtrow["examName"] = dt.Rows[i]["examName"].ToString();
                    dtrow["questionNos"] = dt.Rows[i]["questionNos"].ToString();
                    dtrow["noofQuestions"] = dt.Rows[i]["noofQuestions"].ToString();
                    dtrow["courseinfo"] = dt.Rows[i]["courseinfo"].ToString();
                    dtrow["createdDate"] = dt.Rows[i]["createdDate"].ToString();

                    //int remTime = 3600000;
                    DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt.Rows[i]["maxTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)remTime1) - DateTime.Now;
                    string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    dtrow["maxTime"] = r1;
                    dtrow["isAvailable"] = dt.Rows[i]["isAvailable"].ToString();
                    dtrow["negativeMarking"] = dt.Rows[i]["negativeMarking"].ToString();
                    dtrow["noOfSections"] = dt.Rows[i]["noOfSections"].ToString();
                    dtExams.Rows.Add(dtrow);
                }

                gridExam.DataSource = dtExams;
                gridExam.DataBind();
                gridExam.Caption = "Latest 10 Exam details";
            }
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //if (txtTime.Text != 0)
            //{

            //    string ids = "";
            //    for (int i = 0; i < repQuestions.Items.Count; i++)
            //    {
            //        CheckBox chb = (CheckBox)repQuestions.Items[i].FindControl("chkSelect");
            //        if (chb.Checked == true)
            //        {
            //            Label lbl = (Label)repQuestions.Items[i].FindControl("Label7");
            //            if (ids.Length == 0)
            //            {
            //                ids = lbl.Text;
            //            }
            //            else
            //            {
            //                ids = ids + lbl.Text;
            //            }
            //        }
            //    }

            //    lblQues.Text = ids;
            //    ob.createdExam(txtName.Text,lblQues.Text,Convert.ToInt16(txtTime.Text)*60000,
            //}
            //else
            //{
            //    lblMsg.Text = "Enter valid max time !";
            //    lblMsg.Visible = true;
            //}


        }

        protected void repQuestions_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCourse.SelectedItem.Text.Length > 0)
            {
                sourceSubjects.SelectCommand = "SELECT Distinct subjectinfo,subid from view_catCourseSub WHERE courseid = " + ddlCourse.SelectedValue + " and subid is not null";
                sourceSubjects.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                ddlSubjects.DataBind();
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt16(txtSections.Text) > 0)
            {
                lblerror.Visible = false;
                try
                {
                    if (Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")) > DateTime.Now.Date)
                    {
                        string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        lblMaxQuestion.Text = txtQueNo.Text;
                        ob.createExam(txtName.Text, txtQueNo.Text, Convert.ToInt16(txtTime.Text) * 60000, val[0].ToString(), ddlCourses.SelectedValue, chkAvailable.Checked, txtSections.Text, chkNegMark.Checked, ddlType.Text, ddlUnderCourse.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDesc.Text, lblInstituteId.Text);
                        lblExamID.Text = ob.getValue("SELECT MAX(examID) AS Expr1 FROM exm_existingExams");

                        examName = txtName.Text;
                        type = ddlType.Text;
                        lblMsg.Text = "Exam Created Add Questions to exam &nbsp;&nbsp;&nbsp;&nbsp;Max Questions to add " + txtQueNo.Text;
                        int sections = Convert.ToInt16(txtSections.Text);
                        for (int i = 0; i < sections; i++)
                        {
                            ddlSections.Items[i].Enabled = true;
                        }
                    }
                    else
                    {
                        lblerror.Visible = true;
                        lblerror.Text = "Please select date greater than present date!!!";
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("duplicate"))
                    {
                        lblerror.Text = "Exam Name Already Exists!";
                    }
                    else
                    {
                        lblerror.Text = ex.Message;
                    }
                    lblerror.Visible = true;
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                }
            }
        }

        protected void btnRep_Click(object sender, EventArgs e)
        {
            string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
            lblerror.Visible = false;
            Button btn = (Button)sender;
            lblQuestionNumber.Text = "Question No : " + btn.Text;
            lblQuestionNumber.Visible = true;
            string qID = btn.CommandArgument;
            queHolder.Visible = true;
            //int z = Convert.ToInt16(btn.Text);

            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext, mark, negativeDeduction FROM view_examQuestions where questid = " + qID + " AND instituteId=" + lblInstituteId.Text + "");

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            if (dt.Rows[0]["Info"].ToString().Length > 0)
            {
                repQueInfo.DataSource = dt;
                repQueInfo.DataBind();
                Panel3.Visible = true;
            }
            else { Panel3.Visible = false; }
        }

        protected void btnRep1_Click(object sender, EventArgs e)
        {
            string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
            lblerror.Visible = false;
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            //int z = Convert.ToInt16(btn.Text);
            lblQuestionNumber.Text = "Question No : " + btn.Text;
            lblQuestionNumber.Visible = true;
            queHolder.Visible = true;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext, mark, negativeDeduction FROM view_examQuestions where questid = " + qID + " AND instituteId=" + lblInstituteId.Text + "");

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            if (dt.Rows[0]["Info"].ToString().Length > 0)
            {
                repQueInfo.DataSource = dt;
                repQueInfo.DataBind();
                Panel3.Visible = true;
            }
            else { Panel3.Visible = false; }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            DataTable dt = new DataTable();
            //dt = ob.getData("SELECT exm_questions.questid FROM exm_questions INNER JOIN exm_subject ON exm_questions.subid = exm_subject.subid INNER JOIN exm_course ON exm_subject.courseid = exm_course.courseid WHERE (exm_course.courseid = " + ddlCourse.SelectedValue + ")");
            dt = ob.getData("SELECT dbo.exm_questions.questid FROM dbo.exm_questions INNER JOIN  dbo.exm_subject ON dbo.exm_questions.subid = dbo.exm_subject.subid INNER JOIN  dbo.exm_course ON dbo.exm_subject.courseid = dbo.exm_course.courseid WHERE  dbo.exm_subject.subid = " + ddlSubjects.SelectedValue + "  and  dbo.exm_course.courseid = " + ddlCourse.SelectedValue + "");
            if (dt.Rows.Count > 0)
            {
                repQueNos.DataSource = dt;
                repQueNos.DataBind();
                lblNoQue.Visible = false;
                repQueNos.Visible = true;
            }
            else
            {
                repQueNos.Visible = false;
                lblNoQue.Visible = true;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            Button btn = (Button)sender;
            if (btn.Text == "Add to Exam")
            {
                string[] quenos = lblQueAdded.Text.Split(',');
                if (Convert.ToInt16(lblMaxQuestion.Text) > quenos.Length)
                {
                    string qID = btn.CommandArgument;
                    //int z = Convert.ToInt16(btn.Text);

                    if (lblQueAdded.Text.Length == 0)
                    {
                        lblQueAdded.Text = qID;
                        DataTable dt = new DataTable();
                        dt.Columns.Add("questid", typeof(string));
                        DataRow dtrow = dt.NewRow();
                        dtrow["questId"] = lblQueAdded.Text;
                        dt.Rows.Add(dtrow);
                        repAddedQues.DataSource = dt;
                        repAddedQues.DataBind();
                        repAddedQues.Visible = true;
                        DataTable dt1 = new DataTable();
                        dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " AND instituteId=" + lblInstituteId.Text + "");
                        Repeater1.DataSource = dt1;
                        Repeater1.DataBind();
                    }
                    else
                    {
                        string add = "yes";
                        string[] que = lblQueAdded.Text.Split(',');
                        for (int x = 0; x < que.Length; x++)
                        {
                            if (qID == que[x].ToString())
                            {
                                add = "no";
                            }
                        }

                        if (add == "yes")
                        {
                            lblQueAdded.Text = lblQueAdded.Text + "," + qID;
                            string[] ques = lblQueAdded.Text.Split(',');
                            DataTable dt = new DataTable();
                            dt.Columns.Add("questid", typeof(string));
                            for (int i = 0; i < ques.Length; i++)
                            {
                                if (ques[i].ToString().Length > 0)
                                {
                                    DataRow dtrow = dt.NewRow();
                                    dtrow["questId"] = ques[i].ToString();
                                    dt.Rows.Add(dtrow);
                                    if (i == 0)
                                    {
                                        lblQueAdded.Text = ques[0].ToString();
                                    }

                                    else { lblQueAdded.Text = lblQueAdded.Text + "," + ques[i].ToString(); }
                                }
                            }
                            repAddedQues.DataSource = dt;
                            repAddedQues.DataBind();
                            repAddedQues.Visible = true;
                            DataTable dt1 = new DataTable();
                            dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Delete from Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInstituteId.Text + "");

                            Repeater1.DataSource = dt1;
                            Repeater1.DataBind();
                        }
                        if (add == "no")
                        {
                            string script = "<script type=\"text/javascript\">showAlert1();</script>";
                            //Page page = HttpContext.Current.CurrentHandler as Page;
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                        }
                    }
                }
                else
                {
                    string script = "<script type=\"text/javascript\">showAlert(" + lblMaxQuestion.Text + " );</script>";
                    //Page page = HttpContext.Current.CurrentHandler as Page;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                }

                if (Convert.ToInt16(lblMaxQuestion.Text) == quenos.Length)
                {
                    //  btnFinish.Enabled = true;
                }
            }

            if (btn.Text == "Delete from Exam")
            {
                string qID = btn.CommandArgument;
                string toReplace = qID + ",";

                string newQue = lblQueAdded.Text.Replace(toReplace, " ");

                lblQueAdded.Text = "";
                string[] quenos = newQue.Split(',');
                DataTable dt = new DataTable();
                dt.Columns.Add("questid", typeof(string));
                for (int i = 0; i < quenos.Length; i++)
                {
                    if (quenos[i].ToString().Length > 0)
                    {
                        if (quenos[i].ToString() != qID)
                        {
                            DataRow dtrow = dt.NewRow();
                            dtrow["questId"] = quenos[i].ToString();
                            dt.Rows.Add(dtrow);
                            if (i == 0)
                            {
                                lblQueAdded.Text = quenos[0].ToString();
                            }

                            else { lblQueAdded.Text = lblQueAdded.Text + "," + quenos[i].ToString(); }
                        }
                    }
                }
                repAddedQues.DataSource = dt;
                repAddedQues.DataBind();

                repAddedQues.Visible = true;
                DataTable dt1 = new DataTable();
                dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext,mark,negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInstituteId.Text + "");

                Repeater1.DataSource = dt1;
                Repeater1.DataBind();
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            int count = 0;
            lblQueAdded.Text = "";
            lblerror.Visible = false;
            DataTable dtQuesNo = new DataTable();
            dtQuesNo = ob.getSectionwiseQuesNo(lblExamID.Text);
            string q1 = "";
            string load = "yes";
            string[] queSplit = null;

            if (dtQuesNo.Rows.Count > 0)
            {
                for (int i = 0; i < dtQuesNo.Columns.Count; i++)
                {
                    if (dtQuesNo.Rows[0][i].ToString() != "")
                    {
                        if (i == 0)
                        {
                            q1 = q1 + dtQuesNo.Rows[0][i].ToString();
                        }
                        else
                        {
                            q1 = q1 + "," + dtQuesNo.Rows[0][i].ToString();
                        }
                    }
                }

                if (q1.Length > 0)
                {
                    queSplit = q1.Split(',');
                    count = queSplit.Length;
                }
                else
                {
                    count = 0;
                }

                if (finishType == "update")
                {
                    if (Convert.ToInt32(lblQuesNoOld.Text) > Convert.ToInt32(lblMaxQuestion.Text))
                    {
                        if (count > Convert.ToInt32(lblMaxQuestion.Text))
                        {
                            string script = "<script type=\"text/javascript\">alertForQueUpdate();</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                            load = "no";
                        }
                        else
                        {
                            load = "yes";
                            lblQueAdded.Text = q1;
                            ob.updateExamDetails(lblQueAdded.Text, lblExamID.Text);
                            ob.updateQueNo(lblMaxQuestion.Text, lblExamID.Text);
                        }
                    }
                    else
                    {
                        lblQueAdded.Text = q1;
                        ob.updateExamDetails(lblQueAdded.Text, lblExamID.Text);
                        ob.updateQueNo(lblMaxQuestion.Text, lblExamID.Text);
                    }
                }
                else
                {
                    lblQueAdded.Text = q1;
                    ob.updateExamDetails(lblQueAdded.Text, lblExamID.Text);
                    ob.updateQueNo(lblMaxQuestion.Text, lblExamID.Text);
                    sendGCMForJob(lblInstituteId.Text, examName,type);
                    examName = "";
                    type = "";
                }
            }
            else
            { }
            if (load == "yes")
            {
                Response.Redirect("/createexam.aspx");
            }

            //string[] queAdded = lblQueAdded.Text.Split(',');
            //if (Convert.ToInt32(lblMaxQuestion.Text) == queAdded.Length)
            //{


            //}
            //else
            //{
            //    string script = "<script type=\"text/javascript\">ConfirmOnFinish();</script>";
            //    //Page page = HttpContext.Current.CurrentHandler as Page;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            //}
        }

        protected void gridExam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnLatest_Click(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            if (txtLast.Text.Length > 0)
            {
                DataTable dt = new DataTable();
                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(',');
                //if (userDetails[1].ToLower() == "super admin")
                //{
                //dt = ob.getData("SELECT  TOP (" + txtLast.Text + ") exm_existingExams.examID, exm_existingExams.examName, CASE WHEN len(questionnos)  > 0 THEN questionNos ELSE 'No Questions Added' END AS questionNos, exm_existingExams.maxTime, exm_existingExams.userID,  exm_existingExams.noofQuestions, exm_existingExams.createdDate, exm_course.courseinfo as examType, dbo.exm_existingExams.isAvailable, dbo.exm_existingExams.negativeMarking, dbo.exm_existingExams.noOfSections  FROM  exm_existingExams INNER JOIN exm_course ON exm_existingExams.courseID = exm_course.courseid ORDER BY exm_existingExams.examID DESC");
                //}
                //else
                //{
                //DataTable dt = ob.getData("SELECT TOP (" + txtLast.Text + ") examID, examName, case when len(questionnos)>0 then  questionNos else 'No Questions Added' end as questionNos, maxTime, userID, noofQuestions, examType,createdDate FROM exm_existingExams ORDER BY examID DESC");
                //dt = ob.getData("SELECT  TOP (" + txtLast.Text + ") exm_existingExams.examID, exm_existingExams.examName, CASE WHEN len(questionnos)  > 0 THEN questionNos ELSE 'No Questions Added' END AS questionNos, exm_existingExams.maxTime, exm_existingExams.userID,  exm_existingExams.noofQuestions, exm_existingExams.createdDate, exm_course.courseinfo as examType, dbo.exm_existingExams.isAvailable, dbo.exm_existingExams.negativeMarking, dbo.exm_existingExams.noOfSections  FROM  exm_existingExams INNER JOIN exm_course ON exm_existingExams.courseID = exm_course.courseid where exm_existingExams.userID=" + userDetails[0] + " ORDER BY exm_existingExams.examID DESC");
                //}
                dt = ob.getData("SELECT  TOP (" + txtLast.Text + ") examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseinfo, isAvailable, negativeMarking, noOfSections FROM view_examDetails where instituteId=" + lblInstituteId.Text + " ORDER BY examID DESC");
                DataTable dtExams = new DataTable();
                dtExams.Columns.Add("examID", typeof(string));
                dtExams.Columns.Add("examName", typeof(string));
                dtExams.Columns.Add("questionNos", typeof(string));
                dtExams.Columns.Add("maxTime", typeof(string));
                dtExams.Columns.Add("noofQuestions", typeof(string));
                dtExams.Columns.Add("courseinfo", typeof(string));
                dtExams.Columns.Add("createdDate", typeof(string));
                dtExams.Columns.Add("isAvailable", typeof(string));
                dtExams.Columns.Add("negativeMarking", typeof(string));
                dtExams.Columns.Add("noOfSections", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dtrow = dtExams.NewRow();
                    dtrow["examID"] = dt.Rows[i]["examID"].ToString();
                    dtrow["examName"] = dt.Rows[i]["examName"].ToString();
                    dtrow["questionNos"] = dt.Rows[i]["questionNos"].ToString();
                    dtrow["noofQuestions"] = dt.Rows[i]["noofQuestions"].ToString();
                    dtrow["courseinfo"] = dt.Rows[i]["courseinfo"].ToString();
                    dtrow["createdDate"] = dt.Rows[i]["createdDate"].ToString();

                    //int remTime = 3600000;
                    DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt.Rows[i]["maxTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)remTime1) - DateTime.Now;
                    string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    dtrow["maxTime"] = r1;
                    dtrow["isAvailable"] = dt.Rows[i]["isAvailable"].ToString();
                    dtrow["negativeMarking"] = dt.Rows[i]["negativeMarking"].ToString();
                    dtrow["noOfSections"] = dt.Rows[i]["noOfSections"].ToString();
                    dtExams.Rows.Add(dtrow);
                }

                gridExam.DataSource = dtExams;
                gridExam.DataBind();
                gridExam.Caption = "Latest " + txtLast.Text + " Exam details";
            }
            else
            {
                lblerror.Text = "Enter proper number to search !";
                lblerror.Visible = true;
            }
        }

        protected void gridExam_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                lblerror.Visible = false;
                row = Convert.ToInt32(e.CommandArgument);
                lblExamID.Text = gridExam.Rows[row].Cells[0].Text;
                DataTable dt = ob.getData("SELECT examName, case when len(questionnos)>0 then questionNos else 'No Questions Added' end as questionNos, maxTime, userID, noofQuestions, courseID, createdDate,noOfSections, isAvailable, negativeMarking, examType,coursrPackageID , convert(varchar(10),onDate,103) as onDate,eDesc FROM exm_existingExams where examID = " + lblExamID.Text + "");

                txtName.Text = dt.Rows[0]["examName"].ToString();

                //DateTime newDate = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()));
                //TimeSpan t1 = new TimeSpan();
                //t1 = ((DateTime)newDate) - DateTime.Now;
                // txtTime.Text = String.Format("{0}", t1.Minutes.ToString());
                txtTime.Text = (Convert.ToInt32(dt.Rows[0]["maxTime"].ToString()) / 60000).ToString();
                ddlCourses.SelectedValue = dt.Rows[0]["courseID"].ToString();
                txtQueNo.Text = lblQuesNoOld.Text = dt.Rows[0]["noofQuestions"].ToString();
                txtSections.Text = dt.Rows[0]["noOfSections"].ToString();
                txtDate.Text = dt.Rows[0]["onDate"].ToString();
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnCancel.Visible = true;
                Panel4.Visible = false;
                lblQueAdded.Text = dt.Rows[0]["questionNos"].ToString();
                chkAvailable.Checked = Convert.ToBoolean(dt.Rows[0]["isAvailable"].ToString());
                chkNegMark.Checked = Convert.ToBoolean(dt.Rows[0]["negativeMarking"].ToString());
                ddlType.Text = dt.Rows[0]["examType"].ToString();
                ddlUnderCourse.Text = dt.Rows[0]["coursrPackageID"].ToString();
                txtDesc.Text = dt.Rows[0]["eDesc"].ToString();
                finishType = "update";
            }
            if (e.CommandName == "remove")
            {
                row = Convert.ToInt32(e.CommandArgument);
                lblExamID.Text = gridExam.Rows[row].Cells[0].Text;

                DataTable dt1 = new DataTable();
                dt1 = ob.getData("SELECT * from exm_newExam where preexamid=" + lblExamID.Text + "");
                if (dt1.Rows.Count == 0)
                {
                    lblerror.Visible = false;
                    ob.DeleteEDetails(lblExamID.Text);
                    ob.deletePaidExmDetails(lblExamID.Text);

                    // DataTable dt = ob.getData("SELECT TOP (10) examID, examName,case when len(questionnos)>0 then  questionNos else 'No Questions Added' end as questionNos , maxTime, userID, noofQuestions, examType,createdDate FROM exm_existingExams ORDER BY examID DESC");
                    DataTable dt = new DataTable();
                    string[] userDetails = HttpContext.Current.User.Identity.Name.Split(',');
                    //if (userDetails[0].ToLower() == "super admin")
                    //{
                    //    dt = ob.getData("SELECT  TOP (10) exm_existingExams.examID, exm_existingExams.examName, CASE WHEN len(questionnos)  > 0 THEN questionNos ELSE 'No Questions Added' END AS questionNos, exm_existingExams.maxTime, exm_existingExams.userID,  exm_existingExams.noofQuestions, exm_existingExams.createdDate, exm_course.courseinfo as examType, dbo.exm_existingExams.isAvailable, dbo.exm_existingExams.negativeMarking, dbo.exm_existingExams.noOfSections  FROM  exm_existingExams INNER JOIN exm_course ON exm_existingExams.courseID = exm_course.courseid ORDER BY exm_existingExams.examID DESC");
                    //}
                    //else
                    //{
                    dt = ob.getData("SELECT  TOP (10) examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseinfo, isAvailable, negativeMarking, noOfSections FROM view_examDetails where instituteId=" + lblInstituteId.Text + " ORDER BY examID DESC");
                    //}
                    DataTable dtExams = new DataTable();
                    dtExams.Columns.Add("examID", typeof(string));
                    dtExams.Columns.Add("examName", typeof(string));
                    dtExams.Columns.Add("questionNos", typeof(string));
                    dtExams.Columns.Add("maxTime", typeof(string));
                    dtExams.Columns.Add("noofQuestions", typeof(string));
                    dtExams.Columns.Add("courseinfo", typeof(string));
                    dtExams.Columns.Add("createdDate", typeof(string));
                    dtExams.Columns.Add("isAvailable", typeof(string));
                    dtExams.Columns.Add("negativeMarking", typeof(string));
                    dtExams.Columns.Add("noOfSections", typeof(string));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dtrow = dtExams.NewRow();
                        dtrow["examID"] = dt.Rows[i]["examID"].ToString();
                        dtrow["examName"] = dt.Rows[i]["examName"].ToString();
                        dtrow["questionNos"] = dt.Rows[i]["questionNos"].ToString();
                        dtrow["noofQuestions"] = dt.Rows[i]["noofQuestions"].ToString();
                        dtrow["courseinfo"] = dt.Rows[i]["courseinfo"].ToString();
                        dtrow["createdDate"] = dt.Rows[i]["createdDate"].ToString();

                        //int remTime = 3600000;
                        DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt.Rows[i]["maxTime"].ToString()));
                        TimeSpan time1 = new TimeSpan();
                        time1 = ((DateTime)remTime1) - DateTime.Now;
                        string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                        dtrow["maxTime"] = r1;
                        dtrow["isAvailable"] = dt.Rows[i]["isAvailable"].ToString();
                        dtrow["negativeMarking"] = dt.Rows[i]["negativeMarking"].ToString();
                        dtrow["noOfSections"] = dt.Rows[i]["noOfSections"].ToString();
                        dtExams.Rows.Add(dtrow);
                    }

                    gridExam.DataSource = dtExams;
                    gridExam.DataBind();
                    gridExam.Caption = "Latest 10 Exam details";
                    Messages11.setMessage(1, "Exam Details deleted successfully! ");
                    Messages11.Visible = true;
                }
                else
                {
                    lblerror.Text = "Can not delete details !";
                    lblerror.Visible = true;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            lblerror.Visible = false;
            try
            {
                if (txtTime.Text.Length > 0 && txtTime.Text != "0")
                {
                    if (Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")) > DateTime.Now.Date)
                    {
                        string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        lblMaxQuestion.Text = txtQueNo.Text;
                        if (Convert.ToInt32(lblQuesNoOld.Text) > Convert.ToInt32(lblMaxQuestion.Text))
                        {
                            Session["MaxQueNo"] = txtQueNo.Text;
                            ob.updateEDetails(txtName.Text, Convert.ToInt32(txtTime.Text) * 60000, lblQuesNoOld.Text, ddlCourses.SelectedValue, lblExamID.Text, chkAvailable.Checked, txtSections.Text, chkNegMark.Checked, ddlType.Text, ddlUnderCourse.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDesc.Text);
                        }
                        else
                        {
                            Session["MaxQueNo"] = null;
                            //ob.createExam(txtName.Text, txtQueNo.Text, Convert.ToInt16(txtTime.Text) * 60000, val[0].ToString(), txtType.Text);
                            ob.updateEDetails(txtName.Text, Convert.ToInt32(txtTime.Text) * 60000, txtQueNo.Text, ddlCourses.SelectedValue, lblExamID.Text, chkAvailable.Checked, txtSections.Text, chkNegMark.Checked, ddlType.Text, ddlUnderCourse.Text, Convert.ToDateTime(txtDate.Text, CultureInfo.GetCultureInfo("hi-IN")), txtDesc.Text);
                        }
                        examName = txtName.Text;
                        type = ddlType.Text;
                        lblMsg.Text = lblMsg.Text + "&nbsp;&nbsp;&nbsp;&nbsp;Max Questions to add " + txtQueNo.Text;
                        int sections = Convert.ToInt16(txtSections.Text);
                        for (int i = 0; i < sections; i++)
                        {
                            ddlSections.Items[i].Enabled = true;
                        }

                        if (lblQueAdded.Text != "No Questions Added")
                        {
                            string[] ques = lblQueAdded.Text.Split(',');
                            DataTable dt = new DataTable();
                            dt.Columns.Add("questid", typeof(string));
                            for (int i = 0; i < ques.Length; i++)
                            {
                                DataRow dtrow = dt.NewRow();
                                dtrow["questId"] = ques[i].ToString();
                                dt.Rows.Add(dtrow);
                            }
                            repAddedQues.DataSource = dt;
                            repAddedQues.DataBind();
                            repAddedQues.Visible = true;
                        }
                        else { lblQueAdded.Text = ""; repAddedQues.Visible = false; }

                        string[] queAdded = lblQueAdded.Text.Split(',');
                        if (Convert.ToInt32(lblMaxQuestion.Text) == queAdded.Length)
                        {
                            //btnFinish.Enabled = true;
                        }
                        else
                        {
                            // btnFinish.Enabled = false;
                        }
                    }
                    else
                    {
                        lblerror.Visible = true;
                        lblerror.Text = "Please select date greater than present date!!!";
                    }
                }
                else
                {
                    lblerror.Visible = true;
                    lblerror.Text = "Enter proper Max Time !";
                }
            }
            catch
            {
                lblerror.Visible = true;
                lblerror.Text = "Test name already exists !";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtDate.Text = "";
            txtName.Text = "";
            txtTime.Text = "";
            txtSections.Text = "";
            chkAvailable.Checked = false;
            txtDesc.Text = "";

            txtQueNo.Text = "";
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            Panel4.Visible = true; ;
            Messages11.Visible = false;
        }

        protected void gridExam_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
            }
        }

        protected void linkAddRemoveQues_Click(object sender, EventArgs e)
        {
            Session["examSection"] = ddlSections.SelectedValue;
            Session["EditExamID"] = lblExamID.Text;
            Messages11.Visible = false;
            ModalPopupExtender3.Show();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            // Session["examSection"]
            Messages11.Visible = true;
            Messages11.setMessage(1, "Questions added successfully to Section " + Session["examSection"].ToString() + " !");
        }


        [WebMethod]
        public void sendGCMForJob(string institudeId, string examNm,string examtype)
        {
            try
            {
                string authkey = ConfigurationManager.AppSettings["authkey"].ToString();

                DataTable dtBatch = new DataTable();
                dtBatch = ob.getUsersbyinstitut(institudeId);
                if (dtBatch.Rows.Count > 0)
                {
                    StringBuilder buildGCM = new StringBuilder();
                    foreach (DataRow row in dtBatch.Rows)
                    {
                        if (row["GCMId"].ToString() != "")
                        {
                            buildGCM.Append("\"").Append(row["GCMId"].ToString()).Append("\"").Append(",");
                        }
                    }
                    string finalGCM = buildGCM.ToString(0, buildGCM.Length - 1);

                    HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");

                    Request.Method = "POST";
                    Request.KeepAlive = false;
                    string registrationId = finalGCM;
                    string cmessage = "Dear user, " + examNm + " Exam has been created.So,hurry up.This Exam is "+examtype+"";
                    string postData = "{\"registration_ids\": [ " + registrationId + " ], \"data\": {\"title\":\" Online Exam Notification\",\"subtitle\": \"" + cmessage + "\",\"instituteID\": \"" + institudeId + "\",\"time_to_live\":\"2160000\"} }";

                    Console.WriteLine(postData);
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                    Request.ContentType = "application/json";
                    Request.Headers.Add("Authorization", "key=" + authkey);

                    //-- Create Stream to Write Byte Array --// 
                    Stream dataStream = Request.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    //-- Post a Message --//
                    WebResponse Response = Request.GetResponse();
                    HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
                    if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                    {
                        Console.WriteLine("Unauthorized - need new token");

                    }
                    else if (!ResponseCode.Equals(HttpStatusCode.OK))
                    {
                        Console.WriteLine("Response from web service isn't OK");
                    }

                    StreamReader Reader = new StreamReader(Response.GetResponseStream());
                    string responseLine = Reader.ReadLine();
                    Reader.Close();
                }

            }
            catch (Exception) { }
        }
    }
}
