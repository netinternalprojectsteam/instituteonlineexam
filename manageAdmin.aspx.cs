﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Net.Mail;
using System.Net;

namespace OnlineExam
{
    public partial class manageAdmin : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblInsID.Text = val[3];

                DataTable dt = new DataTable();
                dt = ob.getInstitute();
                ddlInstitute.DataSource = dt;
                ddlInstitute.DataBind();
                ddlInstitute.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Panel1.Visible = true;
            Panel2.Visible = false;
            btnNew.Visible = false;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            txtEmail.Text = "";
            txtMobile.Text = "";
            txtName.Text = "";
            chkUpdatePass.Visible = false;
            ddlInstitute.ClearSelection();
            //lblPass.Visible = true;
            //lblRePass.Visible = true;
            //txtPass.Visible = true;
            //txtRePass.Visible = true;
            //txtEmail.Enabled = true;

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;

            try
            {
                if (txtPass.Text.Length > 0 && txtRePass.Text.Length > 0)
                {
                    if (txtPass.Text == txtRePass.Text)
                    {
                        if (ddlInstitute.SelectedItem.Text != "--Select--")
                        {
                            ob.insertAdmin(txtEmail.Text, txtPass.Text, txtName.Text, txtMobile.Text, ddlType.Text, ddlInstitute.SelectedValue);
                            gridDetails.DataBind();
                            Panel1.Visible = false;
                            Panel2.Visible = true;
                            btnNew.Visible = true;
                        }
                        else
                        {
                            Messages11.setMessage(2, "Please select proper Institute");
                            Messages11.Visible = true;
                        }
                        //try
                        //{
                        //    DataTable dtConfig = new DataTable();
                        //    dtConfig = ob.getData("select id,emailid,password,smtphost,smtpport,smtpenable from email_config");

                        //    string mail = "Hello " + txtName.Text + "<br> <br> Your Admin Login details <br><b>Email : </b>" + txtEmail.Text + "<br><b>Pasword : </b>" + txtPass.Text + "<br><br>Regards<br><a href='#'>Online Banking Exams Team</a>'";
                        //    lblMail.Text = mail;
                        //    System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(dtConfig.Rows[0]["emailID"].ToString(), txtEmail.Text, "Profile Details at Online Banking Exam", lblMail.Text);
                        //    mm.IsBodyHtml = true;

                        //    System.Net.Mail.SmtpClient client = new SmtpClient();

                        //    client.Host = dtConfig.Rows[0]["smtpHost"].ToString();

                        //    client.EnableSsl = true;

                        //    client.Port = Convert.ToInt32(dtConfig.Rows[0]["smtpPort"].ToString());

                        //    NetworkCredential ntc = new NetworkCredential(dtConfig.Rows[0]["emailID"].ToString(), dtConfig.Rows[0]["password"].ToString());


                        //    client.Credentials = ntc;

                        //    client.Send(mm);

                        //}
                        //catch (Exception ee)
                        //{
                        //    Messages11.Visible = true;
                        //    Messages11.setMessage(0, ee.Message);
                        //}
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Password & Confirm Password not match !");
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Password in required !");
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("Violation of PRIMARY KEY constraint 'PK_admin_user'. Cannot insert duplicate key in object 'dbo.admin_user'"))
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Email id already registered !");
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ddlInstitute.Enabled = true;
            Panel1.Visible = false;
            Panel2.Visible = true;
            btnNew.Visible = true;
        }

        protected void gridDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                Messages11.Visible = false;
                row = Convert.ToInt16(e.CommandArgument);
                lblID.Text = gridDetails.Rows[row].Cells[1].Text;
                DataTable dt = new DataTable();
                dt = ob.getData("select * from admin_user where userid = " + lblID.Text + "");

                DataTable dtIns = new DataTable();
                dtIns = ob.getInstitute();
                ddlInstitute.DataSource = dtIns;
                ddlInstitute.DataBind();
                ddlInstitute.Items.Insert(0, new ListItem("--Select--", "0"));

                try
                {
                    ddlInstitute.SelectedValue = dt.Rows[0]["instituteId"].ToString();
                    ddlInstitute.Enabled = false;
                }
                catch
                {
                    ddlInstitute.Enabled = true;
                }
                txtName.Text = dt.Rows[0]["name"].ToString();
                txtEmail.Text = dt.Rows[0]["username"].ToString();
                txtMobile.Text = dt.Rows[0]["mobile"].ToString();
                ddlType.Text = dt.Rows[0]["adminType"].ToString();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
                btnCancel.Visible = true;
                Panel1.Visible = true;
                Panel2.Visible = false;
                btnNew.Visible = false;
                chkUpdatePass.Visible = true;
                //lblPass.Visible = false;
                //lblRePass.Visible = false;
                //txtPass.Visible = false;
                //txtRePass.Visible = false;
                //txtEmail.Enabled = false;
                chkUpdatePass.Checked = false;
            }
            if (e.CommandName == "remove")
            {

            }
        }

        protected void btnGrid_Click(object sender, EventArgs e)
        {
            ImageButton btndetails = sender as ImageButton;

            GridViewRow gvrow = (GridViewRow)btndetails.NamingContainer;
            lblID.Text = gridDetails.DataKeys[gvrow.RowIndex].Value.ToString();
            string[] np = HttpContext.Current.User.Identity.Name.ToString().Split(',');
            if (lblID.Text == np[0].ToString())
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Can not delete !");
            }
            else
            {
                ob.deleteAdmin(lblID.Text);
                gridDetails.DataBind();
                Messages11.Visible = true;
                Messages11.setMessage(1, "Admin Info Deleted !");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                if (chkUpdatePass.Checked == true)
                {
                    if (txtPass.Text.Length > 0 && txtRePass.Text.Length > 0)
                    {
                        if (txtPass.Text == txtRePass.Text)
                        {
                            ob.updateAdminWithPass(txtEmail.Text, txtName.Text, txtMobile.Text, lblID.Text, txtPass.Text, ddlType.Text, ddlInstitute.SelectedValue);
                            gridDetails.DataBind();
                            Panel1.Visible = false;
                            Panel2.Visible = true;
                            btnNew.Visible = true;
                            Messages11.Visible = true;
                            Messages11.setMessage(1, "Details updated successfully !");
                        }
                        else
                        {
                            Messages11.Visible = true;
                            Messages11.setMessage(0, "Password & Confirm Password not match !");
                        }
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Password is required !");
                    }
                }
                if (chkUpdatePass.Checked == false)
                {
                    ob.updateAdmin(txtEmail.Text, txtName.Text, txtMobile.Text, lblID.Text, ddlType.Text, ddlInstitute.SelectedValue);
                    gridDetails.DataBind();
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    btnNew.Visible = true;
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Details updated successfully !");
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("Violation of PRIMARY KEY constraint 'PK_admin_user'. Cannot insert duplicate key in object 'dbo.admin_user'"))
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Email id already registered !");
                }
            }
        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
        }

    }
}
