﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class payconfirm : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    messages11.Visible = false;
                    if (Session["transID"] != null)
                    {
                        string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                        if (val[2].ToString() == "NXGOnlineExam")
                        {
                            if (val[1].ToString() == "User")
                            {
                                DataTable dt = new DataTable();
                                dt = ob.GetUserInfoByID(val[0].ToString());
                                lblUserName.Text = dt.Rows[0]["uname"].ToString();

                                string transId = Session["transID"].ToString();
                                string htmltext = ob.returnHTML(transId);
                                Label3.Text = htmltext;
                            }
                            else
                            {
                                Response.Redirect("/Default.aspx");
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("/testsList.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                messages11.Visible = true;
                messages11.setMessage(0, ex.Message);
            }
        }
    }
}
