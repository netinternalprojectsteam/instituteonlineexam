﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="paidExamDetails.aspx.cs" Inherits="OnlineExam.paidExamDetails" Title="Paid Exam Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Manage Paid Exams
                        </h3>
                    </td>
                    <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblExamID" Visible="false" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lblInst" Visible="false" runat="server" Text="Label"></asp:Label>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <center>
                            <asp:Panel ID="pnlAdd" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lblerror" Visible="false" runat="server" Text="" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label21" runat="server" Text="Select Exam :" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPaidExams" runat="server" DataTextField="examName" DataValueField="examID">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label24" runat="server" Text="Fees :" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtfees" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtfees" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Amount!"
                                                ControlToValidate="txtfees" Display="Dynamic" SetFocusOnError="true" ValidationExpression="^[1-9]\d*(\.\d+)?$"></asp:RegularExpressionValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtfees"
                                                FilterType="Custom" ValidChars="0123456789.">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label1" runat="server" Text="Allowed Attempts :" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAttempt" runat="server" MaxLength="1"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtAttempt" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAttempt"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click"
                                                CssClass="simplebtn" Visible="false" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                                                OnClick="btnCancel_Click" CssClass="simplebtn" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlShow" runat="server">
                                <table>
                                    <tr>
                                        <td align="right">
                                            <asp:DropDownList ID="ddlExams" runat="server" DataTextField="examName" DataValueField="examID"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlExams_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;&nbsp;<asp:Button ID="btnnew" runat="server" Text="Add New" CssClass="simplebtn"
                                                OnClick="btnnew_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:GridView ID="gridExam" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                                AllowPaging="True" PageSize="20" OnRowCommand="gridExam_RowCommand" OnRowCreated="gridExam_RowCreated">
                                                <Columns>
                                                    <asp:BoundField DataField="ID" HeaderText="ID" />
                                                    <asp:TemplateField HeaderText="Sr.No." HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="examID" HeaderText="examID" />
                                                    <asp:BoundField DataField="examName" HeaderText="Exam Name" />
                                                    <asp:BoundField DataField="userID" HeaderText="userID" />
                                                    <asp:BoundField DataField="examFees" HeaderText="Exam Fees" />
                                                    <asp:BoundField DataField="allowedAttempt" HeaderText="Allowed Attempt" />
                                                    <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                                        <ControlStyle CssClass="simplebtn" />
                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                    </asp:ButtonField>
                                                    <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                                        <ControlStyle CssClass="simplebtn" />
                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                    </asp:ButtonField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Details Found!!!
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </center>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
