﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="howTo.aspx.cs" Inherits="OnlineExam.howTo" Title="How To Use" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .imgClass
        {
            border-radius: 20px;
            border: solid 2px black;
        }
        .OhLink
        {
            font-family: 'Times New Roman' , Times, serif;
            color: #46b1e1;
            font-weight: bold;
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <div class="pageHeading">
                                How To Use Exam Page
                            </div>
                        </td>
                        <td align="right">
                            <asp:HyperLink ID="hyperBack" runat="server" CssClass="OhLink">Back to Start Exams</asp:HyperLink>&nbsp;&nbsp; <a
                                href='/myAccount.aspx' class="OhLink">My Profile</a> &nbsp;&nbsp; <a href='/testsList.aspx' class="OhLink">
                                    Test List</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                On successful Login you are able to attempt the no of tests available on Test List
                page. Before starting any test please read the setting requirments for your browser
                <a href='abtSpecification.aspx' target="_blank">here</a>.
                <br />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <img src="HelpDocs/H1.gif" />&nbsp; <b>Test Page :</b>
                <br />
                First you will see the instructins for the test you selected. On Click of 'Start
                Exam' you are provided with your Test Paper.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam1.png" class="imgClass" alt="" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <img src="HelpDocs/H2.gif">&nbsp; <b>Place for Answering Question :</b>
                <br />
                You can submit your answer with radio buttons selection of your choice.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam2.png" class="imgClass" alt="" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <img src="HelpDocs/H3.gif">&nbsp; <b>Common Data for Question :</b>
                <br />
                If the question is based on some information then you will be provided with the
                "Common Data" for question. You can minimize or maximize the "Common Data" information.
                Scrolling facility is provided to read the "Common Data" without any disturbance
                to other side of your exam page.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam3.png" class="imgClass" alt="" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <img src="HelpDocs/H4.gif">&nbsp; <b>Question List :</b>
                <br />
                You can quickly Review your exam status on click of "Question List".Where you can
                find the questions as
                <br />
                1) All questions list (Answered,Unanswered,Marked)<br />
                2) Answered questions list<br />
                3) Unanswered questions list<br />
                4)Marked questions list<br />
                You can jump to question for answer on click of question in Question List.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam4.png" alt="" class="imgClass" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <img src="HelpDocs/H5.gif">&nbsp; <b>Answer Sheet :</b>
                <br />
                Answer Sheet provides the you the details about your answer for question and also
                it shows the marked questions.<br />
                You can directly jump to question by clicking on the Question Number from Answer
                Sheet.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam5.png" class="imgClass" alt="" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <img src="HelpDocs/H6.gif">
                <b>At any time in test you can,</b><br />
                1) move to any Question number in the test by clicking on the numbers provided.<br />
                2) go to any Section with section numbers (if present).<br />
                3) Also you can go to Next and Previous question with the help of the provided buttons.<br />
                4) Mark the question for your review.<br />
                5) Pause the exam.
                <br />
                6) Save the exam for later (Can resume from Unfinished Exam section provided on
                My Account Page ).<br />
                7) Finish the exam to see result (Can not be ressumed).<br />
                8) Hide/Show the Timer.<br />
                9) See the Directions and Instructions for test.
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <img src="HelpDocs/exam7.png" alt="" class="imgClass" title="Exam Screen" width="700px"
                    height="300px" />
            </td>
        </tr>
    </table>
</asp:Content>
