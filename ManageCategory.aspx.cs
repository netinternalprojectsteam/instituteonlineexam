﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ManageCategory : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInstituteId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInstituteId.Text = val[3];
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnsubmit.Text == "Add New")
                {
                    obj.AddNewCategory(txtCategory.Text, lblInstituteId.Text);
                    Messages11.setMessage(1, "Category Added Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    txtCategory.Text = "";
                }

                if (btnsubmit.Text == "Update")
                {
                    obj.UpdateCategory(txtCategory.Text, lblCatID.Text, lblInstituteId.Text);
                    Messages11.setMessage(1, "Category Updated Successfully !!!");
                    Messages11.Visible = true;
                    btnsubmit.Text = "Add New";
                    GridView1.DataBind();
                    txtCategory.Text = "";
                    btnCancel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowno = Convert.ToInt16(e.CommandArgument);
            if (e.CommandName == "change")
            {
                lblCatID.Text = GridView1.Rows[rowno].Cells[0].Text;
                txtCategory.Text = GridView1.Rows[rowno].Cells[1].Text;
                btnsubmit.Text = "Update";
                btnCancel.Visible = true;
            }

            if (e.CommandName == "remove")
            {
                try
                {
                    obj.DeleteCategory(GridView1.Rows[rowno].Cells[0].Text, lblInstituteId.Text);
                    Messages11.setMessage(1, "Category Deleted Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("The DELETE Statement Conflicted"))
                    {
                        Messages11.setMessage(0, "Cannot Delete Category! The related records exists in another Table");
                        Messages11.Visible = true;
                    }
                    else
                    {
                        Messages11.setMessage(0, ex.Message);
                        Messages11.Visible = true;
                    }
                }
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[0].Visible = false;
            }
            catch { }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtCategory.Text = "";
            btnCancel.Visible = false;
            btnsubmit.Text = "Add New";
        }
    }
}
