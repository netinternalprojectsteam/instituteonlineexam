﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ManageQuestion : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                lbluserId.Text = val[0];
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                ddlCourse.DataBind();
                try
                {
                    SqlDataSource1.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlSubject.DataBind();
                }
                catch
                {
                    Response.Redirect("/ManageCourse.aspx");
                }

                ddlHeading.DataBind();
                ddlHeading.Items.Insert(0, new ListItem("-Select-", "0"));

                Button1.Text = "Save";

                try
                {
                    ddlInfoHead.DataBind();
                    ddlInfoHead.Items.Insert(0, new ListItem("-Select-", "0"));

                    if (Session["editQuestion"].ToString() == "Yes")
                    {
                        string[] Qdata = Session["QData"].ToString().Split(',');
                        ddlCourse.SelectedValue = Qdata[1].ToString();
                        SqlDataSource1.SelectCommand = "SELECT [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                        SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        ddlSubject.DataBind();
                        ddlSubject.SelectedValue = Qdata[2].ToString();
                        lblQid.Text = Qdata[0].ToString();
                        DataTable dt = new DataTable();
                        dt = obj.getData("SELECT  questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid, infoID, mark, negativeDeduction, instituteId FROM view_examQuestions where questid= " + lblQid.Text + " AND instituteId=" + lblInsId.Text + "");
                        CKEditorControl1.Value = dt.Rows[0]["question"].ToString();
                        CKEOptionA.Value = dt.Rows[0]["option1"].ToString();
                        CKEOptionB.Value = dt.Rows[0]["option2"].ToString();
                        CKEOptionC.Value = dt.Rows[0]["option3"].ToString();
                        CKEOptionD.Value = dt.Rows[0]["option4"].ToString();
                        CKEOptionE.Value = dt.Rows[0]["option5"].ToString();
                        edAnswer.Text = dt.Rows[0]["answer"].ToString();
                        txtMarks.Text = dt.Rows[0]["mark"].ToString();
                        txtNegMarks.Text = dt.Rows[0]["negativeDeduction"].ToString();
                        txtDiffLevel.Text = dt.Rows[0]["difflevel"].ToString();
                        Button1.Text = "Update";
                        if (Convert.ToInt32(dt.Rows[0]["infoID"].ToString()) > 1)
                        {
                            ddlInfoHead.SelectedValue = dt.Rows[0]["infoID"].ToString();
                        }
                        Session.Remove("editQuestion");
                    }
                }
                catch
                {

                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string infoID = "0";
            string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
            Messages11.Visible = false;
            try
            {
                if (CKEditorControl1.Value.Length > 0 && CKEOptionA.Value.Length > 0 && CKEOptionB.Value.Length > 0 && CKEOptionC.Value.Length > 0 && CKEOptionD.Value.Length > 0 && txtMarks.Text.Length > 0 && txtNegMarks.Text.Length > 0)
                {
                    string que = ""; string opt1 = ""; string opt2 = ""; string opt3 = ""; string opt4 = ""; string opt5 = "";
                    if (CKEditorControl1.Value.StartsWith("<p>"))
                    {
                        que = CKEditorControl1.Value.Substring(3, CKEditorControl1.Value.Length - 7);
                    }
                    else { que = CKEditorControl1.Value; }
                    if (CKEOptionA.Value.StartsWith("<p>"))
                    {
                        opt1 = CKEOptionA.Value.Substring(3, CKEOptionA.Value.Length - 7);
                    }
                    else { opt1 = CKEOptionA.Value; }
                    if (CKEOptionB.Value.StartsWith("<p>"))
                    {
                        opt2 = CKEOptionB.Value.Substring(3, CKEOptionB.Value.Length - 7);
                    }
                    else { opt2 = CKEOptionB.Value; }

                    if (CKEOptionC.Value.StartsWith("<p>"))
                    {
                        opt3 = CKEOptionC.Value.Substring(3, CKEOptionC.Value.Length - 7);
                    }
                    else { opt3 = CKEOptionC.Value; }
                    if (CKEOptionD.Value.StartsWith("<p>"))
                    {
                        opt4 = CKEOptionD.Value.Substring(3, CKEOptionD.Value.Length - 7);
                    }
                    else { opt4 = CKEOptionD.Value; }

                    if (CKEOptionE.Value.StartsWith("<p>"))
                    {
                        opt5 = CKEOptionE.Value.Substring(3, CKEOptionE.Value.Length - 7);
                    }
                    else { opt5 = CKEOptionE.Value; }



                    if (Button1.Text == "Save")
                    {
                        try
                        {

                            if (ddlInfoHead.SelectedItem.Text.Length > 0)
                            {
                                infoID = ddlInfoHead.SelectedValue;
                            }
                        }
                        catch { }
                        DataTable dtQueExists = new DataTable();
                        dtQueExists = obj.getQueByInstitute(que.Replace("'", "''"), lblInsId.Text);
                        if (dtQueExists.Rows.Count > 0)
                        {
                            Messages11.setMessage(0, "Question already exists!");
                            Messages11.Visible = true;
                        }
                        else
                        {
                            //int max = obj.AddNewQuestion(CKEditorControl1.Value, CKEOptionA.Value, CKEOptionB.Value, CKEOptionC.Value, CKEOptionD.Value, CKEOptionE.Value, edAnswer.Text, ddlSubject.SelectedValue, txtDiffLevel.Text, val[0].ToString(), infoID, txtMarks.Text, Convert.ToDouble(txtNegMarks.Text));
                            int max = obj.AddNewQuestion(que.Replace("'", "''").Trim(), opt1.Trim(), opt2.Trim(), opt3.Trim(), opt4.Trim(), opt5.Trim(), edAnswer.Text.Trim(), ddlSubject.SelectedValue, txtDiffLevel.Text.Trim(), val[0].ToString(), infoID, Convert.ToDouble(txtMarks.Text), Convert.ToDouble(txtNegMarks.Text), lblInsId.Text);
                            Messages11.setMessage(1, "Question Added Successfully !!!");
                            Messages11.Visible = true;
                            GridView1.DataBind();

                            ddlHeading.ClearSelection();
                            CKEditorControl1.Value = "";
                            CKEOptionA.Value = "";
                            CKEOptionB.Value = "";
                            CKEOptionC.Value = "";
                            CKEOptionD.Value = "";
                            CKEOptionE.Value = "";
                            txtDiffLevel.ClearSelection();
                            txtMarks.Text = "";
                            txtNegMarks.Text = "0";

                            string script = "<script type=\"text/javascript\">showMsg(" + max + ")</script>";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                        }
                    }

                    if (Button1.Text == "Update")
                    {
                        // string infoID = "0";
                        try
                        {
                            if (ddlInfoHead.SelectedItem.Text.Length > 0)
                            {
                                infoID = ddlInfoHead.SelectedValue;
                            }
                        }
                        catch { }
                        DataTable dtexists = new DataTable();
                        dtexists = obj.getQueByInsForUpdate(que, lblInsId.Text, lblQid.Text);
                        if (dtexists.Rows.Count > 0)
                        {
                            Messages11.setMessage(0, "Question already exists!");
                            Messages11.Visible = true;
                        }
                        else
                        {
                            //obj.UpdateQuestion(CKEditorControl1.Value, CKEOptionA.Value, CKEOptionB.Value, CKEOptionC.Value, CKEOptionD.Value,CKEOptionE.Value, edAnswer.Text, ddlSubject.SelectedValue, txtDiffLevel.Text, val[0].ToString(), lblQid.Text, infoID, txtMarks.Text, Convert.ToDouble(txtNegMarks.Text));
                            obj.UpdateQuestion(que, opt1, opt2, opt3, opt4, opt5, edAnswer.Text, ddlSubject.SelectedValue, txtDiffLevel.Text, val[0].ToString(), lblQid.Text, infoID, Convert.ToDouble(txtMarks.Text), Convert.ToDouble(txtNegMarks.Text));
                            Messages11.setMessage(1, "Question updated Successfully !!!");
                            Messages11.Visible = true;
                            Session["cancleQueEdit"] = "Yes";
                            Response.Redirect("editQuestions.aspx");
                        }
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Enter all fields properly !");
                    if (txtMarks.Text.Length == 0)
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Enter marks for question !");
                    }


                }
            }
            catch (Exception ex)
            {

                string q1 = CKEditorControl1.Value + "," + CKEOptionA.Value + "," + CKEOptionB.Value + "," + CKEOptionC.Value + "," + CKEOptionD.Value + "," + CKEOptionE.Value + "," + infoID;

                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowno = Convert.ToInt16(e.CommandArgument);
            if (e.CommandName == "change")
            {
                lblQid.Text = GridView1.Rows[rowno].Cells[0].Text;
                DataTable dt = new DataTable();
                dt = obj.GetQuestionByID(lblQid.Text);
                //question, option1, option2, option3, option4, answer, subid, difflevel, userid
                CKEditorControl1.Value = dt.Rows[0][0].ToString();
                CKEOptionA.Value = dt.Rows[0][1].ToString();
                CKEOptionB.Value = dt.Rows[0][2].ToString();
                CKEOptionC.Value = dt.Rows[0][3].ToString();
                CKEOptionD.Value = dt.Rows[0][4].ToString();
                edAnswer.Text = dt.Rows[0][5].ToString();
                ddlSubject.SelectedValue = dt.Rows[0][6].ToString();
                txtDiffLevel.Text = dt.Rows[0][7].ToString();
                Button1.Text = "Update";
            }

            if (e.CommandName == "remove")
            {
                try
                {
                    obj.DeleteQuestion(GridView1.Rows[rowno].Cells[0].Text);
                    Messages11.setMessage(1, "Question Deleted Successfully !!!");
                    Messages11.Visible = true;
                }
                catch (Exception ex)
                {
                    Messages11.setMessage(0, ex.Message);
                    Messages11.Visible = true;
                }
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[0].Visible = false;
            }
            catch { }
        }

        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataSource1.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
            SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            ddlSubject.DataBind();
        }

        protected void ddlHeading_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlHeading_SelectedIndexChanged1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = obj.getData("select infContents from exm_queInfo where infID = " + ddlHeading.SelectedValue + "");
            repQueInfo.DataSource = dt;
            repQueInfo.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ddlHeading.DataBind();
            ddlInfoHead.SelectedValue = Session["InfID"].ToString();
            Session.Remove("InfID");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["cancleQueEdit"] = "Yes";
            Response.Redirect("editQuestions.aspx");
        }


    }
}
