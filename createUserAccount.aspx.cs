﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class createUserAccount : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //string questionList = "";
                obj.CreateNewUserAccount(txtFirstName.Text, txtEmailID.Text, txtPassword.Text, txtMo.Text);
                string n = obj.getValue("Select max(uid) from exam_users");

                Session["LoginDetails"] = n + "#" + "User" + "#false";
                string script = "<script type=\"text/javascript\">callfun()();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

                //FormsAuthentication.RedirectFromLoginPage(n + "," + "User" + "," + "NXGOnlineExam", false);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Violation of PRIMARY KEY constraint 'PK_exam_users'. Cannot insert duplicate key in object 'dbo.exam_users'"))
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Email ID already exists !");
                }
            }
        }
    }
}
