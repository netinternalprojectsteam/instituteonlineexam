﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class Exam : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Response.Cache.SetNoStore();
                    timeAllocated.Value = "30";
                    //lblExamID.Text = "9";
                    //lblUserID.Text = "1";

                    //Session["userID"] = "1";
                    //Session["examID"] = "9";javascript:showWait();endWait()



                    //string scripts = "<script type=\"text/javascript\">showWait()</script>";
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", scripts, false);

                    //System.Threading.Thread.Sleep(5000);


                    lblExamID.Text = Session["examID"].ToString();
                    lblUserID.Text = Session["userID"].ToString();
                    //Entry is maintained to table for user is attempted the exam User can attempt an exam only once.




                    DataTable questions = new DataTable();
                    questions = ob.getData("SELECT questid, question, option1, option2, option3, option4, option5,infoID,infContents,selectedOption,Uname,noOfSections,isFinish,Ismarked,examName,section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM view_examQuestionsDetails where exmid = " + lblExamID.Text + " and uid = " + lblUserID.Text);
                    tempLabel.Text = "SELECT questid, question, option1, option2, option3, option4, option5,infoID,infContents,selectedOption,Uname,noOfSections,isFinish,Ismarked,examName,section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM view_examQuestionsDetails where exmid = " + lblExamID.Text + " and uid = " + lblUserID.Text + "   " + questions.Rows.Count;

                    if (questions.Rows[0]["isFinish"].ToString() != "1")
                    {
                        lblName.Text = questions.Rows[0]["Uname"].ToString();
                        try { lblExamName.Text = questions.Rows[0]["examName"].ToString(); }
                        catch { }

                        string queDiv = "<div id='queDiv' style='display:none'>";
                        for (int i = 0; i < questions.Rows.Count; i++)
                        {
                            queDiv = queDiv + "<div id='" + (i + 1) + "'>";
                            queDiv = queDiv + "<div id='que" + (i + 1) + "'>" + questions.Rows[i]["question"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='optA" + (i + 1) + "'>" + questions.Rows[i]["option1"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='optB" + (i + 1) + "'>" + questions.Rows[i]["option2"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='optC" + (i + 1) + "'>" + questions.Rows[i]["option3"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='optD" + (i + 1) + "'>" + questions.Rows[i]["option4"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='optE" + (i + 1) + "'>" + questions.Rows[i]["option5"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='infContents" + (i + 1) + "'>" + questions.Rows[i]["infContents"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='selectedOption" + (i + 1) + "'>" + questions.Rows[i]["selectedOption"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='questid" + (i + 1) + "'>" + questions.Rows[i]["questid"].ToString() + "</div>";
                            queDiv = queDiv + "<div id='Ismarked" + (i + 1) + "'>" + questions.Rows[i]["Ismarked"].ToString() + "</div>";
                            queDiv = queDiv + "</div>";
                        }
                        queDiv = queDiv + "</div>";
                        queID.Text = "1";//queID refers to traverse through div where questions are stored. 
                        lblMaxQue.Text = (questions.Rows.Count).ToString();

                        string butttons = "<div id='button' class='btn'>";
                        for (int x = 0; x < questions.Rows.Count; x++)
                        {
                            if (x == 0)
                            {
                                butttons = "<input type='button' id='btn" + (x + 1) + "' value='" + (x + 1) + "' onclick='JavaScript:Getdivdata(this.value)' />";
                            }
                            else
                            {
                                butttons = butttons + "<input type='button' id='btn" + (x + 1) + "' value='" + (x + 1) + "' class='btn1' onclick='JavaScript:Getdivdata(this.value)' />";
                            }
                        }
                        butttons = butttons + "</div>";
                        lblButtons.Text = butttons;

                        //Repeater2.DataSource = questions;
                        //Repeater2.DataBind();
                        Label1.Text = queDiv;
                        //ViewState["myTable"] = questions;
                        //  

                        DataTable dt1 = new DataTable();
                        dt1 = ob.getData("select questid,selectedOption,Case when selectedOption=1 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png  width=12px height=12px />' end as Option1,Case when selectedOption=2 then '<img src=/images/radioAns.png  width=12px height=12px />' else '<img src=/images/radio.png   width=12px height=12px />' end as Option2,Case when selectedOption=3 then '<img src=/images/radioAns.png  width=12px height=12px />' else '<img src=/images/radio.png width=12px height=12px />' end as Option3,Case when selectedOption=4 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png width=12px height=12px />' end as Option4,Case when selectedOption=5 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png  width=12px height=12px />' end as Option5,case when Ismarked='marked' then '<img src=/images/mark.png width=20px height=20px />' else '' end as Ismarked from view_examQuestionsDetails where exmid=" + lblExamID.Text + " and uid=" + lblUserID.Text + "");
                        string res = "<table class='hovertable'>";
                        res = res + "<tr><th align='center'>Sr.No.</th><th align='center'>M/U</th><th align='center'>1</th><th align='center'>2</th><th align='center'>3</th><th align='center'>4</th><th align='center'>5</th>";
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            res = res + "<tr onmouseover='this.style.backgroundColor='#ffff66';' onmouseout='this.style.backgroundColor='#d4e3e5';'><td align='center' ><a href='JavaScript:Getdivdata(" + (i + 1) + ")'>" + (i + 1) + "</a></td><td id='marked" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Ismarked"].ToString() + "</td><td align='center' id='Option1" + (i + 1) + "'>" + dt1.Rows[i]["Option1"].ToString() + "</td><td align='center' id='Option2" + (i + 1) + "' >" + dt1.Rows[i]["Option2"].ToString() + "</td><td id='Option3" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option3"].ToString() + "</td><td id='Option4" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option4"].ToString() + "</td><td id='Option5" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option5"].ToString() + "</td></tr>";
                        }
                        res = res + "</table>";
                        // HiddenField3.Value = res;
                        Label2.Text = res;



                        string sections = questions.Rows[0]["noOfSections"].ToString();//obj.getValue("select noOfSections from exm_newExam where newExm = " + lblExamID.Text + "");


                        if (Session["resPreExam"].ToString() == "No")
                        {
                            lblTime.Text = ob.getValue("select maxTime from exm_newExam where newExm = " + lblExamID.Text + "");

                        }
                        else
                        {
                            lblTime.Text = ob.getValue("select min(remainingtime) from exm_userAnswers where exmid = " + lblExamID.Text + "");
                        }


                        if (Convert.ToInt16(sections) > 1)
                        {
                            lblSelectedQue.Text = "1";
                            lblNoOfsections.Text = sections;
                            DataTable dtSections = new DataTable();
                            dtSections.Columns.Add("serialNo", typeof(string));
                            dtSections.Columns.Add("SectioNo", typeof(string));
                            dtSections.Columns.Add("number", typeof(string));

                            for (int z = 1; z <= Convert.ToInt16(sections); z++)
                            {
                                DataRow dtrow = dtSections.NewRow();
                                dtrow["serialNo"] = z.ToString();
                                dtrow["SectioNo"] = "Section" + z.ToString();
                                if (z == 1) { dtrow["number"] = "I"; }
                                if (z == 2) { dtrow["number"] = "II"; }
                                if (z == 3) { dtrow["number"] = "III"; }
                                if (z == 4) { dtrow["number"] = "IV"; }
                                if (z == 5) { dtrow["number"] = "V"; }
                                dtSections.Rows.Add(dtrow);
                            }

                            repSections.DataSource = dtSections;
                            repSections.DataBind();

                            int queSum = 0;
                            int ques = 0;
                            int sectionLength = 0;

                            //string[] section1Ques = questions.Rows[0]["section1Ques"].ToString().Split(',');
                            string[] section1Ques = null;
                            if (questions.Rows[0]["section1Ques"].ToString().Length > 0)
                            {
                                section1Ques = questions.Rows[0]["section1Ques"].ToString().Split(',');
                                sectionLength = section1Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            //for (int i = 0; i < section1Ques.Length; i++)
                            for (int i = 0; i < section1Ques.Length; i++)
                            {
                                if (section1Ques[i].Trim().Length > 0)
                                {
                                    ques = ques + 1;
                                }
                            }

                            int questionInSection = ques;// section 1 questions.
                            lblSection1Ques.Text = "1," + ques.ToString() + "";
                            queSum = ques;
                            lblSection1Max.Text = ques.ToString();
                            ques = 0;
                            sectionLength = 0;

                            //string[] section2Ques = questions.Rows[0]["section2Ques"].ToString().Split(',');
                            string[] section2Ques = null;
                            if (questions.Rows[0]["section2Ques"].ToString().Length > 0)
                            {
                                section2Ques = questions.Rows[0]["section2Ques"].ToString().Split(',');
                                sectionLength = section2Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            //for (int i = 0; i < section2Ques.Length; i++)
                            for (int i = 0; i < sectionLength; i++)
                            {
                                if (section2Ques[i].Trim().Length > 0)
                                {
                                    ques = ques + 1;
                                }
                            }
                            if (sectionLength > 0)
                            {
                                lblSection2Ques.Text = "" + (queSum + 1).ToString() + "," + (queSum + ques).ToString() + "";
                                lblSection2Max.Text = (queSum + ques).ToString();
                                queSum = queSum + ques;
                            }
                            else
                            {
                                lblSection2Ques.Text = lblSection2Max.Text = "";
                            }
                            ques = 0;
                            sectionLength = 0;

                            //string[] section3Ques = questions.Rows[0]["section3Ques"].ToString().Split(',');
                            string[] section3Ques = null;
                            if (questions.Rows[0]["section3Ques"].ToString().Length > 0)
                            {
                                section3Ques = questions.Rows[0]["section3Ques"].ToString().Split(',');
                                sectionLength = section3Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            //for (int i = 0; i < section3Ques.Length; i++)
                            for (int i = 0; i < sectionLength; i++)
                            {
                                if (section3Ques[i].Trim().Length > 0)
                                {
                                    ques = ques + 1;
                                }

                            }

                            if (sectionLength > 0)
                            {
                                lblSection3Ques.Text = "" + (queSum + 1).ToString() + "," + (queSum + ques).ToString() + "";
                                lblSection3Max.Text = (queSum + ques).ToString();
                                queSum = queSum + ques;
                            }
                            else
                            {
                                lblSection3Ques.Text = lblSection3Max.Text = "";
                            }

                            ques = 0;
                            sectionLength = 0;

                            //string[] section4Ques = questions.Rows[0]["section4Ques"].ToString().Split(',');
                            string[] section4Ques = null;
                            if (questions.Rows[0]["section4Ques"].ToString().Length > 0)
                            {
                                section4Ques = questions.Rows[0]["section4Ques"].ToString().Split(',');
                                sectionLength = section4Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            //for (int i = 0; i < section4Ques.Length; i++)
                            for (int i = 0; i < sectionLength; i++)
                            {
                                if (section4Ques[i].Trim().Length > 0)
                                {
                                    ques = ques + 1;
                                }

                            }

                            if (sectionLength > 0)
                            {
                                lblSection4Ques.Text = "" + (queSum + 1).ToString() + "," + (queSum + ques).ToString() + "";
                                lblSection4Max.Text = (queSum + ques).ToString();
                                queSum = queSum + ques;
                            }
                            else
                            {
                                lblSection4Ques.Text = lblSection4Max.Text = "";
                            }

                            ques = 0;
                            sectionLength = 0;

                            //string[] section5Ques = questions.Rows[0]["section5Ques"].ToString().Split(',');
                            string[] section5Ques = null;
                            if (questions.Rows[0]["section5Ques"].ToString().Length > 0)
                            {
                                section5Ques = questions.Rows[0]["section5Ques"].ToString().Split(',');
                                sectionLength = section5Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            //for (int i = 0; i < section5Ques.Length; i++)
                            for (int i = 0; i < sectionLength; i++)
                            {
                                if (section5Ques[i].Trim().Length > 0)
                                {
                                    ques = ques + 1;
                                }
                            }

                            if (sectionLength > 0)
                            {
                                lblSection5Ques.Text = "" + (queSum + 1).ToString() + "," + (queSum + ques).ToString() + "";
                                lblSection5Max.Text = (queSum + ques).ToString();
                                queSum = queSum + ques;
                            }
                            else
                            {
                                lblSection5Ques.Text = lblSection5Max.Text = "";
                            }

                            //load section 1 questions
                            DataTable dtLoadQuestionNos = new DataTable();
                            dtLoadQuestionNos.Columns.Add("serialnumber", typeof(string));
                            dtLoadQuestionNos.Columns.Add("questid", typeof(string));

                            for (int x = 0; x < questionInSection; x++)
                            {
                                try
                                {
                                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                                    dtrow["serialnumber"] = (x + 1).ToString();
                                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                                    dtLoadQuestionNos.Rows.Add(dtrow);
                                }
                                catch
                                { }
                            }
                            butttons = "";
                            butttons = "<div id='button'>";
                            for (int x = 0; x < questionInSection; x++)
                            {
                                if (x == 0)
                                {
                                    butttons = butttons + "<div id='btn" + (x + 1) + "' class='btn' style='width: 35px' ><input type='button' value='" + (x + 1).ToString() + "' onclick='JavaScript:Getdivdata(this.value)'></div>";
                                }
                                else
                                {
                                    butttons = butttons + "<div id='btn" + (x + 1) + "'  style='width: 35px'  class='btn'><input type='button' value='" + (x + 1).ToString() + "' onclick='JavaScript:Getdivdata(this.value)'></div>";
                                }
                            }
                            butttons = butttons + "</div>";
                            lblButtons.Text = butttons;

                            Repeater2.DataSource = dtLoadQuestionNos;
                            Repeater2.DataBind();


                            //selectSection("1");
                            //lblSectionNo.Text = "1";
                            lblSectionsLabel.Visible = true;

                        }
                        else { lblSectionsLabel.Visible = false; }


                        lblInstructions.Text = "";
                        lblInstructions.Text = ob.getValue("SELECT description from exam_startPages where coursePreExamID = (select preexamid from exm_newexam where newexm=  " + lblExamID.Text + ") ");
                        if (lblInstructions.Text.Length == 1)
                        {
                            lblInstructions.Text = "Directions not set !";
                        }
                        lblIns.Text = lblInstructions.Text;
                        //repQuestins.DataSource = questions;
                        //repQuestins.DataBind();
                        lblShowInstructions.Text = "show";


                        string script = "<script type=\"text/javascript\">Getdivdata(1)</script>";
                        //Page page = HttpContext.Current.CurrentHandler as Page;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

                    }
                    else
                    {
                        string script = "<script type=\"text/javascript\">showAlreadySolved()</script>";
                        //Page page = HttpContext.Current.CurrentHandler as Page;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    }
                    //string scripts1 = "<script type=\"text/javascript\">endWait()</script>";
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", scripts1, false);
                }
            }
            catch (Exception ee) { lblErrorMessage.Text = ee.Message; }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //string questid = "";
            //string ans = "";
            //string id = HiddenField1.Value;
            ////string data = LatestData.InnerHtml;
            //string[] saveQuestions = HiddenField2.Value.Split('#');

            //for (int i = 1; i < saveQuestions.Length; i++)
            //{
            //    string[] queAns = saveQuestions[i].ToString().Split(',');
            //    questid = queAns[0].ToString();
            //    ans = queAns[1].ToString();
            //    ob.updateQueAnswer(questid, lblExamID.Text, lblUserID.Text, ans, 0);
            //}
            //HiddenField2.Value = "";

            //DataTable questions = new DataTable();
            //questions = ob.getData("SELECT questid, question, option1, option2, option3, option4, option5, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber],infoID,infContents,selectedOption,Uname FROM view_examQuestionsDetails where exmid = " + lblExamID.Text + " and uid = " + lblUserID.Text + "");
            //lblName.Text = questions.Rows[0]["Uname"].ToString();
            //string queDiv = "<div id='queDiv'>";
            //for (int i = 0; i < questions.Rows.Count; i++)
            //{
            //    queDiv = queDiv + "<div id='" + (i + 1) + "'>";
            //    queDiv = queDiv + "<div id='que" + (i + 1) + "'>" + questions.Rows[i]["question"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='optA" + (i + 1) + "'>" + questions.Rows[i]["option1"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='optB" + (i + 1) + "'>" + questions.Rows[i]["option2"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='optC" + (i + 1) + "'>" + questions.Rows[i]["option3"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='optD" + (i + 1) + "'>" + questions.Rows[i]["option4"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='optE" + (i + 1) + "'>" + questions.Rows[i]["option5"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='infContents" + (i + 1) + "'>" + questions.Rows[i]["infContents"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='selectedOption" + (i + 1) + "'>" + questions.Rows[i]["selectedOption"].ToString() + "</div>";
            //    queDiv = queDiv + "<div id='questid" + (i + 1) + "'>" + questions.Rows[i]["questid"].ToString() + "</div>";
            //    queDiv = queDiv + "</div>";
            //}
            //queDiv = queDiv + "</div>";
            //queID.Text = "1";//queID refers to traverse through div where questions are stored. 
            //Label1.Text = queDiv;

            ////ViewState["myTable"] = questions;





            //DataTable dt1 = new DataTable();
            //dt1 = ob.getData("select questid,selectedOption,Case when selectedOption=1 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png  width=12px height=12px />' end as Option1,Case when selectedOption=2 then '<img src=/images/radioAns.png  width=12px height=12px />' else '<img src=/images/radio.png   width=12px height=12px />' end as Option2,Case when selectedOption=3 then '<img src=/images/radioAns.png  width=12px height=12px />' else '<img src=/images/radio.png width=12px height=12px />' end as Option3,Case when selectedOption=4 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png width=12px height=12px />' end as Option4,Case when selectedOption=5 then '<img src=/images/radioAns.png width=12px height=12px />' else '<img src=/images/radio.png  width=12px height=12px />' end as Option5,case when Ismarked='marked' then '<img src=/images/mark.png width=20px height=20px />' else '' end as Ismarked,ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber] from view_examQuestionsDetails where exmid=" + lblExamID.Text + " and userid=" + lblUserID.Text + "");
            //string res = "<table class='gridtable'>";
            //res = res + "<tr><td align='center'>Sr.No.</td><td align='center'>Marked</td><td align='center'>1</td><td align='center'>2</td><td align='center'>3</td><td align='center'>4</td><td align='center'>5</td>";
            //for (int i = 0; i < dt1.Rows.Count; i++)
            //{
            //    res = res + "<tr><td align='center'><a href='JavaScript:Getdivdata(" + (i + 1) + ")'>" + (i + 1) + "</a></td><td id='marked" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Ismarked"].ToString() + "</td><td align='center' id='Option1" + (i + 1) + "'>" + dt1.Rows[i]["Option1"].ToString() + "</td><td align='center' id='Option2" + (i + 1) + "' >" + dt1.Rows[i]["Option2"].ToString() + "</td><td id='Option3" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option3"].ToString() + "</td><td id='Option4" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option4"].ToString() + "</td><td id='Option5" + (i + 1) + "' align='center'>" + dt1.Rows[i]["Option5"].ToString() + "</td></tr>";
            //}
            //res = res + "</table>";
            //Label2.Text = res;


            //string script = "<script type=\"text/javascript\">Getdivdata('" + id + "')</script>";
            ////Page page = HttpContext.Current.CurrentHandler as Page;
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }


        //to show answersheet to user...
        public void generateAnswerSheet()
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lblUserID.Text;

            ob.updateExamStatus(lblExamID.Text);
            Response.Redirect("exmResult.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ob.updateExamStatus(lblExamID.Text);
        }

        protected void btnSubmitFeedback_Click(object sender, EventArgs e)
        {
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lblUserID.Text;

            Response.Redirect("exmResult.aspx");
        }
    }
}
