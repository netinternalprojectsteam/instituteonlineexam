﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class editData : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            ddlInfoHead.DataBind();
            ddlInfoHead.SelectedValue = Session["InfID"].ToString();

            DataTable dt = new DataTable();
            dt = ob.getCommonData(Session["InfID"].ToString());
          
            lblID.Text = Session["InfID"].ToString();  Session.Remove("InfID");
            CKEditorControl1.Value = dt.Rows[0]["infContents"].ToString();

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (CKEditorControl1.Value.Length > 0)
            {
                ob.updateCommonData(lblID.Text, ddlInfoHead.SelectedItem.Text, CKEditorControl1.Value);
                Messages11.Visible = true;
                Messages11.setMessage(1, "Data Updated Successfully !");
                CKEditorControl1.Value = "";
                ddlInfoHead.DataBind();
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter Proper Text !");
            }
        }

        protected void ddlInfoHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            DataTable dt = new DataTable();
            lblID.Text = ddlInfoHead.SelectedValue;
            dt = ob.getCommonData(ddlInfoHead.SelectedValue);
            CKEditorControl1.Value = dt.Rows[0]["infContents"].ToString();
        }
    }
}
