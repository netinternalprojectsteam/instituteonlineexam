﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OnlineExam.Web.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>JCF Academy</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/screens.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/jquery-ui.css" />
    <link href="/css/unite-gallery.css" rel="stylesheet" type="text/css" />

    <script src="/js/jquery-1.9.1.min.js"></script>

    <script type="text/javascript" src="/js/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="/css/woco-accordion.min.css" />

    <script type="text/javascript">
        jQuery(document).ready(function($) { 
            $(window).load(function(){
        	    $('#preloader').fadeOut('slow',function(){$(this).remove();});
       	    });
       });
    </script>

    <script type="text/javascript">
        function categoryHideShow(id){
            var screenWidth = parseInt(screen.width);
            if(screenWidth < 982){
                $("#"+id).find('ul').toggle();
            }}
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(window).scroll(function(){
                var window_top = $(window).scrollTop() + 0; 
                var div_top = $('#nav-anchor').offset().top;
                if (window_top > div_top) {
                    $('.nav_fixed').addClass('stick');
                }
                else {
                    $('.nav_fixed').removeClass('stick');
                }
            });
       });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <header>
<!--            <div class="top_strip"><div class="lgt_rd"></div></div>-->
  
            <div class="main_wrapper">
                <a href="" class="logo">
                    <img src="/images/logo.png">
                </a>
                <div class="hdr_rgt">
                    <ul>

                        <li>
                            <a target="_blank" href="http://onlinetestseriesmadeeasy.in/">
                                <span class="span"><i class="sprite icn2"></i></span>
                                <div class="ln1">Online test</div>
                                <div class="ln2">series <span class="red">2017</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="home/postal_study_course">
                                <span class="span"><i class="sprite icn3"></i></span>
                                <div class="ln1">postal study</div>
                                <div class="ln2">course</div>
                            </a>
                        </li>
                        <li>
                            <a href="home/StudentOnlineRegistration" >
                                <span class="span"><i class="sprite icn4"></i></span>
                                <div class="ln1">Online </div>
                                  <div class="ln2">Admissions</div>
                            </a>
                        </li>
                        <li>
                            <a href="home/ContactUs">
                                <span class="span"><i class="sprite icn5"></i></span>
                                <div class="ln1">Contact us:</div>
                                <div class="ln2 red">011-45124612</div>
                            </a>
                        </li>
                        <li>
                            <a href="partPayment">
                                <span class="span"><i class="sprite icn6"></i></span>
                                <div class="ln1">Make Payment</div>
                                <div class="ln1">(Registered user)</div>
                                
                            </a>
                        </li>
                    </ul>
                </div>
            </div><nav>
                <div class="main-wrap1" >
                    <a class="toggleMenu" href="#">
                        <span class="tgl_mnu_txt">Menu</span>
                        <span class="menu_icon">
                            <i></i>
                            <i class="two"></i>
                            <i></i>
                        </span>
                    </a>
                    <div class="qklnk"></div>
                    <ul class="nav bold_font">
                         
                        
                        <li class="test"><a href="/Web/Default.aspx"><span class="brdrnav first"> Home </span> </a></li>

<li class="test"><a href="home/IES"><span class="brdrnav"> About Us </span> </a></li>

<li class="test"><a href="home/IES"><span class="brdrnav"> GATE </span> </a></li>

<li class="test"><a href="home/IES"><span class="brdrnav"> IES </span> </a></li>

<li class="test"><a href="home/IES"><span class="brdrnav"> MPSC </span> </a></li>

<li class="test"><a href="/Web/Cources.aspx"><span class="brdrnav"> COURSES </span> </a></li>

<li class="test"><a href="home/IES"><span class="brdrnav"> ADMISSIONS </span> </a></li>
                        
         
    </ul>
    
    
                    <ul class="nav_cntr">
                        <li class="centers"><a  href="javascript:void(0)">Centres<i class="sprite"></i></a>
                            <ul class="centerslist">
                                <!-- for dynamic centre list -->
                                                                        <li><a href="home/centre/Delhi" class="border-none">Delhi</a></li>
                                                                            <li><a href="home/centre/Patna">Patna</a></li>
                                            <li><a href="home/centre/Lucknow">Lucknow</a></li>
                                            <li><a href="home/centre/Jaipur">Jaipur</a></li>
                                            <li><a href="home/centre/Bhopal">Bhopal</a></li>
                                            <li><a href="home/centre/Indore">Indore</a></li>
                                            <li><a href="home/centre/Pune">Pune</a></li>
                                            <li><a href="home/centre/Hyderabad">Hyderabad</a></li>
                                            <li><a href="home/centre/Bhubaneswar">Bhubaneswar</a></li>
                                            <li><a href="home/centre/Kolkata">Kolkata</a></li>
                                            <li><a href="home/centre/Noida">Noida</a></li>
                                </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            
        </header>
    <section>
            <div class="main_wrapper">
<style>
    a
    {
        cursor: pointer;
    }
    .tabb .tabing li
    {
        list-style: none;
        background: url(/images/Earth.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: left;
        color: #000;
        padding: 60px 0 0;
        margin-top: 15px;
        font-size: 13px;
        width: 23.5%;
        line-height: 15px;
        text-align: center;
    }
    .tabb .tabing li p
    {
        color: #666;
    }
    .postid-1426 .tabb .tabing li
    {
        width: 25% !important;
        margin-right: 18px !important;
    }
    .postid-1645 .tabb .tabing li, .postid-1647 .tabb .tabing li, .postid-1649 .tabb .tabing li
    {
        width: 30.5% !important;
    }
    .postid-1646 .tabb .tabing li, .postid-1648 .tabb .tabing li
    {
        width: 30.5% !important;
        min-height: 80px !important;
    }
    .tabb .tabing li.books
    {
        list-style: none;
        background: url(/images/books.png) left top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: left;
        color: #000;
        padding: 0 0 0 60px;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.tests
    {
        list-style: none;
        background: url(/images/tests.png) left top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: left;
        color: #000;
        padding: 0 0 0 60px;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.e-lectures
    {
        list-style: none;
        background: url(/images/electures.png) left top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: left;
        color: #000;
        padding: 0 0 0 60px;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.classrooms
    {
        list-style: none;
        background: url(/images/classrooms.png) left top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: left;
        color: #000;
        padding: 0 0 0 60px;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    /*Remove these after sometime*/.tabb .tabing li.books1, .area1 .center_con li.books1
    {
        list-style: none;
        background: url(/images/books_1.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.tests1, .area1 .center_con li.tests1
    {
        list-style: none;
        background: url(/images/tests_1.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.e-lectures1, .area1 .center_con li.e-lectures1
    {
        list-style: none;
        background: url(/images/electures_1.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.classrooms1, .area1 .center_con li.classrooms1
    {
        list-style: none;
        background: url(/images/classrooms_1.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.books2
    {
        list-style: none;
        background: url(/images/books_2.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.tests2
    {
        list-style: none;
        background: url(/images/tests_2.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.e-lectures2
    {
        list-style: none;
        background: url(/images/electures_2.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    .tabb .tabing li.classrooms2
    {
        list-style: none;
        background: url(/images/classrooms_2.png) center top no-repeat;
        float: left;
        margin-right: 20px;
        text-align: center;
        color: #000;
        padding: 60px 0 0 0;
        margin-top: 15px;
        font-size: 13px;
        line-height: 15px;
    }
    /*Remove these after sometime*/.tabb .tabing li h5
    {
        font-size: 16px;
        margin: 0 0 2px 0;
    }
    .tabing
    {
        width: 100%;
        float: left;
        margin-left: 0;
    }
    .tabb
    {
        width: 75%;
        float: left;
    }
    .tabb1
    {
        width: 23.8%;
        float: right;
    }
    .tabb-vedio
    {
        /*height: 200px; margin-top: 10px; width: 269px;*/
        position: relative;
        padding-bottom: 62.25%; /* ratio for youtube embed */
        padding-top: 30px;
        height: auto;
        overflow: hidden;
    }
    .tabb-vedio iframe, .tabb-vedio object, .tabb-vedio embed
    {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .tabb-img img
    {
        height: 200px;
    }
    .hovereffect
    {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
    }
    .hovereffect .overlay
    {
        width: 100%;
        height: 100%;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        background-color: rgba(75,75,75,0.7);
        -webkit-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .hovereffect:hover .overlay
    {
        background-color: rgba(48, 152, 157, 0.4);
    }
    .hovereffect img
    {
        width: 320px;
        height: 270px;
        display: block;
        position: relative;
    }
    .hovereffect h2
    {
        color: #fff;
        text-align: center;
        position: relative;
        font-size: 17px;
        padding: 10px;
        background: rgba(0, 0, 0, 0.6);
        -webkit-transform: translateY(225px);
        -ms-transform: translateY(225px);
        transform: translateY(225px);
        -webkit-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
    }
    .hovereffect:hover h2
    {
        -webkit-transform: translateY(5px);
        -ms-transform: translateY(5px);
        transform: translateY(5px);
    }
    .hovereffect a.info
    {
        display: inline-block;
        text-decoration: none;
        padding: 14px;
        text-transform: uppercase;
        color: #fff;
        border: 1px solid #fff;
        background-color: transparent;
        opacity: 0;
        filter: alpha(opacity=0);
        -webkit-transform: scale(0);
        -ms-transform: scale(0);
        transform: scale(0);
        -webkit-transition: all 0.4s ease-in-out;
        transition: all 0.4s ease-in-out;
        font-weight: normal;
        margin: -52px 0 0 0;
        padding: 62px 10px;
    }
    .hovereffect:hover a.info
    {
        opacity: 1;
        filter: alpha(opacity=100);
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
    }
    .hovereffect a.info:hover
    {
        box-shadow: 0 0 5px #fff;
    }
</style>
 
    <div  class="ltst_updtscntr">
        <div class="ltst_updttl bold">Latest Update</div>
        <div class="latest_updt">
                                <span>GATE - 2016 Top 100 Rankers from MADE EASY                        <div class="updtshow_cntr">                            <a href="http://madeeasy.in/admin/ESEGroup/GATE_2016_top_500_116.pdf" target="_blank"><img src="/images/GATE-toppers1_17.jpg" title="" alt="Topper Image"c></a>
                        </div>
                    </span>
                                    <span>ESE 2015 Selections from MADE EASY                        <div class="updtshow_cntr">                            <a href="achievement" target="_blank"><img src="/images/ESE-2015_101.jpg" title="" alt="Topper Image"c></a>
                        </div>
                    </span>
                                    <span class="last">&nbsp;Revised Scheme and Syllabi for ESE-2017                        <div class="updtshow_cntr" style="background:#ccffff;">
                            <a href="http://madeeasy.in/admin/WhatsNew/ESE-2017_syllabi_55.pdf" target="_blank"><img src="/images/1RevisedBanner_83.jpg" title="" alt="Topper Image"></a>
                        </div>
                    </span>
                        </div>
    </div>

    <div style="display:none" class="sld_cntr">
        <div class="box_skitter box_skitter_large desktop_show" style="margin:0;padding:0; height: 161px !important; ">
            <ul>
                                    <li>
                                                        <a target="_blank" href="http://cbt.onlinetestseriesmadeeasy.in/">
                                                                <img src="/images/1Online-Banner-design_373.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                      
                        <li>
                                                        <a target="_blank" href="http://onlinetestseriesmadeeasy.in/">
                                                                <img src="/images/4APPSCOTS_375.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                        <li>
                                                        <a target="_blank" href="home/fee-structure">
                                                                <img src="/images/Offline_Test_377.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                        <li>
                                                        <a target="_blank" href="admin/MarqueeDoc/2ME_bhopal_259_5.pdf">
                                                                <img src="/images/4Bhopal_359.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                        <li>
                                                        <a target="_blank" href="1PostalStudyCourse.pdf">
                                                                <img src="/images/7postal_226.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                        <li>
                                                        <a target="_blank" href="PuneSeminar15January.pdf">
                                                                <img src="/images/Pune_Seminar_15January_391.jpg" title="" alt="Gate Topper Image" class="block" />
                            </a></li>
                </ul>
        </div>
    </div>


    <div class="sld_cntr mob_show">
        <div id="owl-demo" class="owl-carousel">
                            <div class="item">
                                                <a target="_blank" href="http://cbt.onlinetestseriesmadeeasy.in/">
                                                    <img src="/images/1Online-Banner-design_373.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                <a target="_blank" href="http://nst.onlinetestseriesmadeeasy.in/">
                                                    <img src="/images/NST_Banner_1_376.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                <a target="_blank" href="http://onlinetestseriesmadeeasy.in/">
                                                    <img src="/images/4APPSCOTS_375.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                <a target="_blank" href="home/fee-structure">
                                                    <img src="/images/Offline_Test_377.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                <a target="_blank" href="admin/MarqueeDoc/2ME_bhopal_259_5.pdf">
                                                    <img src="/images/4Bhopal_359.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                 <a target="_blank" href="images/upldPDF/1PostalStudyCourse_226.pdf">       
                                                  <img src="/images/7postal_226.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
                    <div class="item">
                                                 <a target="_blank" href="images/upldPDF/PuneSeminar15January_391.pdf">       
                                                  <img src="/images/Pune_Seminar_15January_391.jpg" title="" alt="Gate Topper Image" width="100%">
                   </a></div>
            </div>
    </div>
 
 
  </div>
  
  
  
  <div style="width:80%; margin:20px auto;">
  <div style="width:100%;float:left;">
  
  <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/homeGATE.jpg" alt="">
        <div class="overlay">
           <h2>homeGATE</h2>
           <a class="info" href="#">Prepare for GATE at your home. Get trained by best faculties for 600 hours of online LIVE classes, 800 hours of recorded lectures, books and tests. Get recordings of missed classes</a>
        </div>
    </div>
</div>
  
  <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/Subject-page356x265.jpg" alt="">
        <div class="overlay">
           <h2>Subject Pack</h2>
           <a class="info" href="#">

Choose your important subjects & pay only for those</a>
        </div>
    </div>
</div>


  <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/samsung-tabGATE_homepage.jpg" alt="">
        <div class="overlay">
           <h2>tabGATE</h2>
           <a class="info" href="#">

GATE preparation, anytime, anywhere. High Quality GATE Content preloaded on Android based tablet. E-books, e-lectures Tests & Quizzes prepared by IIT/IISc graduates</a>
        </div>
    </div>
</div>
 
 
 
  <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/CPP3.jpg" alt="">
        <div class="overlay">
           <h2>Correspondence Programs</h2>
           <a class="info" href="#">



Get the GATE preparation material delivered to your doorstep. Books, refresher guides, e-lectures, question banks, mock test & more.</a>
        </div>
    </div>
</div>


 <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/classroom.png" alt="">
        <div class="overlay">
           <h2>Classroom Courses</h2>
           <a class="info" href="#">


Strengthen your fundamentals through focused classroom sessions of up to 500 hrs, conducted by GATE toppers and alumni of IITs /IISc.</a>
        </div>
    </div>
</div>

 <div  style="width:310px;float:left;margin:20px;">
    <div class="hovereffect">
        <img class="img-responsive" src="/images/test-series.png" alt="">
        <div class="overlay">
           <h2>GATE Test Series</h2>
           <a class="info" href="#">



Prepare for GATE with All India mock tests, with detailed performance analysis and All-India percentile ranking. Also get subject wise tests and sectional tests.</a>
        </div>
    </div>
</div>
 
 
 
 
  
    </div>
</div>
  
  
  <br/>
  
    
  <br/>  
  <br/>
 <!-- <div class="tabb">
<ul class="tabing">
<li class="classrooms1">
<h5>Live Online Classes</h5>
<p>You receive 1400 hours of (Live + VoD)sessions from IIT/IISc alumni via the internet.</p></li>
<li class="books1">
<h5>Books</h5>
<p>12 to 16 GATE Focused Books. Theory, Questions, Solutions.</p></li>
<li class="books1">
<h5>Refresher Guide</h5>
<p>Important concepts and formulae Book for last minute revisions.</p></li>
<li class="tests1">
<h5>Subject Wise Tests</h5>
<p>500+ Problems &amp; Solutions Grouped under 10 Subjects.</p></li>
<li class="tests1">
<h5>Chapter Wise Tests</h5>
<p>800+ Problems &amp; Solutions Grouped under 40 Chapters.</p></li>
<li class="tests1">
<h5>All India Mock Tests</h5>
<p>12 National Level Online Tests with Video Solutions.</p></li>
<li class="tests1">
<h5>Full Length Tests</h5>
<p>8 Online Test Based on latest GATE Syllabus available from day one.</p></li>
<li class="classrooms1">
<h5>Post GATE Guidance</h5>
<p>Online Guidance on how to best use GATE Score for Admissions.</p></li>
</ul>-->
</div>
  
  
  
  
  
  
  
  
 <!--home Pop Up end Here -->
 <!--home popup js start dhiraj*/--->
<script type="text/javascript">
function hide1(){ $("#onloadPopup,#blackPopup1").fadeOut(700)();
//$("#onloadPopup,#blackPopup")fadeOut(2000);
//$(".ui").hide(1000);
}
$(document).ready(function(){
    $("#left").addClass('appicon_show');
	$("#left").removeClass("appicon_hide");
	
    });
  function hideRight(){
	$("#left").removeClass("appicon_show");
	$("#left").addClass("appicon_hide");
	}
    function fireEvent(obj,evt,link){
 var fireOnThis = obj;
 obj.href=link;
 if(document.createEvent) {
   var evObj = document.createEvent('MouseEvents');
   evObj.initEvent( evt, true, false );
   fireOnThis.dispatchEvent(evObj);
 } else if( document.createEventObject){
   fireOnThis.fireEvent('on'+evt);
 }
}      
</script>
 <!--home popup  js start dhiraj*/--->
<script>
    function setGid(id,action){
           $('#gId').val(id);
           $('#phototabForm').attr('action',action);
           $('#phototabForm').submit();
       }
</script>
<script>
    $(document).ready(function () {

        var owl = $("#owl-demo55");

        owl.owlCarousel({
            
            itemsCustom: [
                [0, 3],
                [320, 2.5],
                [360, 2.5],
                [480, 4.4],
                [540, 5],
                [600, 5],
                [640, 5],
                [768, 7],
                [800, 7],
                [920, 7],
                [980, 9],
                [1024, 9],
                [1200, 11],
                [1400, 13],
                [1600, 15]
            ],
            navigation: true

        });



    });
</script>
<script>
    $(document).ready(function () {

        var owl = $("#owl-demo60");

        owl.owlCarousel({
            

            itemsCustom: [
                [0, 1],
                [450, 1],
                [600, 1],
                [700, 1],
                [1000, 1],
                [1200, 1],
                [1400, 1],
                [1600, 1]
            ],
            navigation: true

        });



    });
</script>

 
</section>
<footer>
    
 <div class="main_wrapper footer_first">
  
                  <div class="ftr_prt1 first" style="width:280px;">
                <h2 class="bold">About us</h2>
               <p class="foot_add">We provide comprehensive and rigorous coaching for the GATE exams. Our student-centered guidance focuses on the strengths and weaknesses of each student. This has enabled us to achieve a proven track record of GATE toppers from our institute.</p>
                </div>
                           <div class="ftr_prt1" style="width:200px;">
                <h2 class="bold">Quick Links</h2>
                <ul>
                                          <li><a href="#">ONLINE TEST SERIES</a></li>

 <li><a href="#">PHOTO GALLERY</a></li>

 <li><a href="#">FAQs</a></li>

 <li><a href="#">TESTIMONIALS</a></li>

 <li><a href="#">DOWNLOAD</a></li>

 <li><a href="#">GATE CIVIL ENGG.</a></li>

 <li><a href="#">IES CIVIL ENGG.</a></li>

 <li><a href="#">MPSC CIVIL ENGG.</a></li>
                                      </ul>
                </div>
                 <div class="ftr_prt1" style="width:280px;">
            <h2>Contact Details</h2>
      <p class="foot_add">
      Gulshan Plaza, <br />3rd Floor, <br />at CADDESK Institute,<br>
         Mobile:9730883984<br>
        Email :vaibhav.mahalle1111@gmail.com
      </p>
                </div>
    
    <div class="ftr_prt1 last" style="width:200px;">
         <h2 class="bold">RESOURCES</h2>
                <ul>
                                          <li><a href="#">Linkdin</a></li>

<li><a href="#">Facebook</a></li>

<li><a href="#">Twitter</a></li>

<li><a href="#">Blog</a></li>
                                      </ul> 
   
   </div>
      </div>
    <a href="#" class="scrollToTop"></a>
</footer>
 <div class="ftr_cprit">
     <div class="main_wrapper follow_on">
       
         <div class="cop_icn">
           
        <div class="socil_links">
              <i style="  float: left;margin-top: -4px;margin-right: 17px;">Developed by</i> <a target="_blank" href="https://www.nxglabs.in/">NXG Labs Pvt. Ltd.</a> 


        </div>

                 
            
   </div>
           <p class="copy">JCF Academy <span style="font-size:16px">©</span> 2016. All Rights Reserved.</p>
     </div>
 </div>
<script>
$(document).ready(function(){
	
	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
});
 function displaySpan(){
          $("#open_facegate").toggle();
            }
            
</script>



<script type="text/javascript">
$(document).ready(function(fadeLoop) {
  
  var fad = $('.fader');
  var counter = 0;
  var divs = $('.fader').hide();
  var dur = 500;
  
  fad.children().filter('.fader').each(function(fad) {
    
    function animator(currentItem) {
      
      animator(fad.children(":first"));
      
      fad.mouseenter(function(){
        $(".fader").stop(); 
      });
      fad.mouseleave(function(){
        animator(fad.children(":first"));
      });
    };
    
  });
  
  function showDiv() {
    divs.fadeOut(dur)
    .filter(function(index) {
      return index == counter % divs.length;
    }) 
    .delay(dur) 
    .fadeIn(dur);
    counter++;
  };
  
  showDiv();
  
  return setInterval(function() { 
    showDiv(); 
  }, 2 * 1000);
  
  adfade();
  
});
 adfade();
function adfade(){
		$('#picOne').fadeIn(500).delay(5000).fadeOut(500);
		adfadetwo();
	}
	function adfadetwo(){
	    adfade();
	}
</script>



<!--<script src="js/jquery-1.10.2.min.js"></script>--> 

 <script>
    $(document).ready(function() {

      $("#owl-demo2").owlCarousel({
        items : 1,
        lazyLoad : true,
        navigation : true
      });

    });
</script>


<script>
    $(document).ready(function() {

      var owl = $("#owl-demo1");

      owl.owlCarousel({

        
        itemsCustom : [
          [0, 2],
          [450, 4],
          [600, 7],
          [700, 9],
          [1000, 10],
          [1200, 12],
          [1400, 13],
          [1600, 15]
        ],
        navigation : true

      });



    });
</script>
<script>
    $(document).ready(function() {
      $("#owl-demo3,#owl-demo4").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      });
    });
    </script>
    <script>
    $(document).ready(function() {

      $("#owl-demo5").owlCarousel({
        items : 5,
        lazyLoad : true,
        navigation : true
      });

    });
    </script>
    
<script>
    $(document).ready(function() {

      var time = 7; 
      var timeSlot  = 100 ;
      var $progressBar,
          $bar, 
          $elem, 
          isPause, 
          tick,
          percentTime;


        $("#owl-demo").owlCarousel({
          slideSpeed : 500,
          paginationSpeed : 500,
          singleItem : true,
          afterInit : progressBar,
          afterMove : moved,
          startDragging : pauseOnDragging
        });

    
        function progressBar(elem){
          $elem = elem;
    
          buildProgressBar();
    
          start();
        }

    
        function buildProgressBar(){
          $progressBar = $("<div>",{
            id:"progressBar"
          });
          $bar = $("<div>",{
            id:"bar"
          });
          $progressBar.append($bar).prependTo($elem);
        }

        function start() {
        
          percentTime = 0;
          isPause = false;
        
          tick = setInterval(interval, 10);
        };

        function interval() {
          if(isPause === false){
            percentTime += 1 / time;
            $bar.css({
               width: percentTime+"%"
             });
         
            //if(percentTime >= 100){
            if(percentTime >= timeSlot){
              $elem.trigger('owl.next')
            }
          }
        }

 
        function pauseOnDragging(){
          isPause = true;
        }

 
        function moved(){
 
          clearTimeout(tick);
 
          start();
        }
        // dpkia 
        $("#owl-demo").mouseover(function(){
            timeSlot  = 1000000 ;
        });
        
        $("#owl-demo").mouseout(function(){
            timeSlot  = 100 ;
        });
        // dpkia  end
    });
    </script>
    
 
<script type="text/javascript">
var ul;
var li_items;
var imageNumber;
var imageWidth;
var prev, next;
var currentPostion = 0;
var currentImage = 0;


function init(){
	ul = document.getElementById('image_slider');                
	li_items = ul.children;
	imageNumber = li_items.length;
	imageWidth = li_items[0].children[0].clientWidth;
	ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
	prev = document.getElementById("prev");
	next = document.getElementById("next");
	prev.onclick = function(){ onClickPrev();};
	next.onclick = function(){ onClickNext();};
}

function animate(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	//return id;
}

function slideTo(imageToGo){
	var direction;
	var numOfImageToGo = Math.abs(imageToGo - currentImage);
	

	direction = currentImage > imageToGo ? 1 : -1;
	currentPostion = -1 * currentImage * imageWidth;
	var opts = {
		duration:500,
		delta:function(p){return p;},
		step:function(delta){
			ul.style.left = parseInt(currentPostion + direction * delta * imageWidth * numOfImageToGo) + 'px';
		},
		callback:function(){currentImage = imageToGo;}	
	};
	animate(opts);
}

function onClickPrev(){
	if (currentImage == 0){
		slideTo(imageNumber - 1);
	} 		
	else{
		slideTo(currentImage - 1);
	}		
}

function onClickNext(){
	if (currentImage == imageNumber - 1){
		slideTo(0);
	}		
	else{
		slideTo(currentImage + 1);
	}		
}

window.onload = init;
</script>

    



	
	<!--<script type='text/javascript' src='js/jquery-11.0.min.js'></script>-->	
	<script type='text/javascript' src='js/unitegallery.min.js'></script>
	<script type='text/javascript' src='js/ug-theme-compact.js'></script>

<script src="js/responsiveTabs.js"></script>
<script>
$(document).ready(function() {
	RESPONSIVEUI.responsiveTabs();
        var CentreName       = $.trim($("#getCntrFrmUrl").val());
        if(CentreName!=''){
           
            var tabActiveId      = $.trim($(".responsive-tabs-wrapper").find("li.responsive-tabs__list__item--active").attr('id'));
            var tabPanelActiveId = $.trim($('.responsive-tabs-wrapper').find('.responsive-tabs__panel--active').attr('id'));
            var tabId            = $.trim($('#feeStructure li:contains("'+ CentreName+'")').attr('id'));
            var tabIdLen         = tabId.length;
            var currtabcnt       = tabId.substring(tabIdLen-1,tabIdLen);
            $("#"+tabActiveId).removeClass("responsive-tabs__list__item--active");
            $("#"+tabPanelActiveId).removeClass(".responsive-tabs__panel--active");
            $("#"+tabPanelActiveId).hide();
            $("#"+tabId).addClass("responsive-tabs__list__item--active");
            $("#tablist1-panel"+currtabcnt).show();
            $("#tablist1-panel"+currtabcnt).addClass(".responsive-tabs__panel--active");
        }
});
   
</script>
<script src="js/jquery-ui.js"></script>
<script src="js/owl.carousel.js"></script>
    <script>
        $(document).ready(function () {
            $("#owl-demo66,#owl-demo67").owlCarousel({
                navigation: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
            });
    //            alert(sessId);
        });
    </script>
<script type="text/javascript" src="js/woco.accordion.min.js"></script>
<link rel="stylesheet" href="js/calender/jquery.datetimepicker.css">
<script type="text/javascript" src="js/calender/jquery.datetimepicker.js"></script> 
        
<script>
    $(".accordion").accordion();
   $(".accordion1").accordion1();
   $(".accordion2").accordion2();
</script>

<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script>
   $(document).ready(function() {    
       $("#scroller").niceScroll(".nicescroll",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller1").niceScroll(".nicescroll1",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller2").niceScroll(".nicescroll2",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller3").niceScroll(".nicescroll3",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller4").niceScroll(".nicescroll4",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller5").niceScroll(".nicescroll5",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller6").niceScroll(".nicescroll6",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller7").niceScroll(".nicescroll7",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller8").niceScroll(".nicescroll8",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller9").niceScroll(".nicescroll9",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller10").niceScroll(".nicescroll10",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller11").niceScroll(".nicescroll11",{cursorcolor:"#770f18",boxzoom:true}); 
       $("#scroller12").niceScroll(".nicescroll12",{cursorcolor:"#770f18",boxzoom:true});
       $("#scroller16").niceScroll(".nicescroll16",{cursorcolor:"#770f18",boxzoom:true,railalign:"left"});     
    });
</script>



<script src="js/advertisment/jquery.transform-0.8.0.min.js"></script>
<script src="js/advertisment/jquery.banner.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/centreData.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript" src="js/slider/jquery.easing.1.3.js"></script>
<script type="text/javascript" language="javascript" src="js/slider/jquery.skitter.min.js"></script>
<script type="text/javascript" language="javascript"  src="js/jquery.horizontal.scroll.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function(){
		$('.box_skitter_large').skitter({fullscreen:true});
       	});
    
</script>
    </form>
</body>
</html>
