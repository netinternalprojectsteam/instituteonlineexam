﻿<%@ Page Language="C#" MasterPageFile="~/NewHome.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs"
    Inherits="OnlineExam.Web.AboutUs" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main_wrapper">
        <div style="width: 80%; margin: 20px auto;">
            <h2 style="border-bottom: 2px solid #fec907; padding: 10px 0; margin-bottom: 10px;">
                Our Speciality</h2>
            <ul>
                <li style="padding: 10px;">
                    <img src="/images/restartExam.png" alt="" width="15px" />
                    Get Personal attentation from Experienced Faculty </li>
                <li style="padding: 10px;">
                    <img src="/images/restartExam.png" alt="" width="15px" />
                    Get E-Lecturers, E-Books, Detailed Notes, Short Notes, Pictorial Notes, Theory Books</li>
                <li style="padding: 10px;">
                    <img src="/images/restartExam.png" alt="" width="15px" />
                    Get Solved Papers of GATE, IES and MPSC and more</li>
                <li style="padding: 10px;">
                    <img src="/images/restartExam.png" alt="" width="15px" />
                    Get 24 Hours Library Facility Query Sessions Doubts clearing Special Session</li>
            </ul>
        </div>
    </div>
</asp:Content>
