﻿<%@ Page Language="C#" MasterPageFile="~/NewHome.Master" AutoEventWireup="true" CodeBehind="onlineTestSeries.aspx.cs"
    Inherits="OnlineExam.Web.onlineTestSeries" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main_wrapper">
        <div style="width: 80%; margin: 20px auto;">
            <div style="width: 100%; float: left;">
                <h2 style="border-bottom: 2px solid #fec907; padding: 10px 0; margin-bottom: 10px;">
                    Online Exam Packages Offered</h2>
                <ul>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        ESE (IES) 2017 Online Test Series</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        GATE 2018 Online Test Series</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        ESE (IES+Gate) 2018 Online Test Series </li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        MPSC 2017 Online Test Series</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        MPSC and IES Online Interview Guidance Classes Batch</li>
                </ul>
                <br />
                <br />
                <h2 style="border-bottom: 2px solid #fec907; padding: 10px 0; margin-bottom: 10px;">
                    Online Exam Package</h2>
                <ul>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        CBT Mock Test – 1 400/-</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        CBT Mock Test – 2 400/- </li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        CBT Mock Test 1 +2 700/- </li>
                </ul>
                <br />
                <br />
                <h2 style="border-bottom: 2px solid #fec907; padding: 10px 0; margin-bottom: 10px;">
                    Online Exam Features</h2>
                <ul>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        Experienced Real time Exam Environment</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        Video Solutions by senior faculties</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        Test papers developed by experienced faculties</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        Includes MCQs</li>
                    <li style="padding: 10px;">
                        <img src="/images/restartExam.png" alt="" width="15px" />
                        Comprehensive Result Analysis </li>
                </ul>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
