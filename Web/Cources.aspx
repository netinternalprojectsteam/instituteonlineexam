﻿<%@ Page Language="C#" MasterPageFile="~/NewHome.Master" AutoEventWireup="true" CodeBehind="Cources.aspx.cs"
    Inherits="OnlineExam.Web.Cources" Title="Cources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        img.alignnone
        {
            float: left;
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main_wrapper">
        <div style="width: 80%; margin: 20px auto;">
            <div style="width: 100%; float: left;">
                <h2 style="border-bottom: 2px solid #fec907;">
                    EXAMS</h2>
                <br />
                <br />
                <h3>
                    <a href="gate.html">
                        <img class="alignnone" src="/images/graduate_aptitude_test_in_engineering.png.pagespeed.ce.aNdcYGYG8_.png"
                            alt="">GATE</a></h3>
                <p>
                    GATE picks out the best Engineers in India by testing students’ understanding of
                    Engineering undergraduate subjects.</p>
                <br />
                <br />
                <h3>
                    <a href="#">
                        <img class="alignnone" src="/images/engineering_services_examination.png.pagespeed.ce.E1BUC3MiMA.png"
                            alt="">ESE</a></h3>
                <p>
                    The Engineering Services Exam tests eligibility for recruitment into the Indian
                    Engineering Services.</p>
                <br />
                <br />
                <br />
                <h2 style="border-bottom: 2px solid #fec907;">
                    COURSES</h2>
                <br />
                <br />
                <h3>
                    <a href="#">
                        <img class="alignnone" src="/images/intensive_cassroom_program.png.pagespeed.ce.0CDfVujyHa.png"
                            alt="">Intensive Classroom Program</a></h3>
                <p>
                    For Perfectly Planned Study. Our flagship program, the ICP delivers interactive,
                    high quality training designed to yield the best results in the GATE exam.</p>
                <br />
                <br />
                <h3>
                    <a href="#">
                        <img class="alignnone" src="/images/Online-test-Series.png.pagespeed.ce.61gwnwmkwB.png"
                            alt="">Online test Series</a></h3>
                <p>
                    For Pure Practice. Practice high quality questions and assess yourself online with
                    a comparative birds-eye view on performance indicators like All-India Rank, Percentile,
                    Score, Section-wise Performance Analysis etc.</p>
                <br />
                <br />
                <h3>
                    <a href="#">
                        <img class="alignnone" src="/images/Distance-Learning-Program.png.pagespeed.ce.k6ZKYaW9IB.png"
                            alt="">Distance Learning Program</a></h3>
                <p>
                    Invaluable study, right at home. Get the best of both worlds with a blended course
                    that combines self-learning (video lectures &amp; study material) and faculty-assisted
                    learning (chat &amp; voice-based interaction with faculty).</p>
                <br />
                <br />
                <h3>
                    <a href="#">
                        <img class="alignnone" src="/images/Live-Online-Classes.png.pagespeed.ce.cOCfx0JARF.png"
                            alt="">Live Online Classes</a></h3>
                <p>
                    Live online courses from your home or our center. Get access to live classes, our
                    study material through GDrive (pendrive) and our online test series.</p>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
