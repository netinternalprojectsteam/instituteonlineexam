﻿<%@ Page Language="C#" MasterPageFile="~/NewHome.Master" AutoEventWireup="true" CodeBehind="Gate.aspx.cs"
    Inherits="OnlineExam.Web.Gate" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        a
        {
            cursor: pointer;
        }
        .tabb .tabing li
        {
            list-style: none;
            background: url(/images/Earth.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 60px 0 0;
            margin-top: 15px;
            font-size: 13px;
            width: 23.5%;
            line-height: 15px;
            text-align: center;
        }
        .tabb .tabing li p
        {
            color: #666;
        }
        .postid-1426 .tabb .tabing li
        {
            width: 25% !important;
            margin-right: 18px !important;
        }
        .postid-1645 .tabb .tabing li, .postid-1647 .tabb .tabing li, .postid-1649 .tabb .tabing li
        {
            width: 30.5% !important;
        }
        .postid-1646 .tabb .tabing li, .postid-1648 .tabb .tabing li
        {
            width: 30.5% !important;
            min-height: 80px !important;
        }
        .tabb .tabing li.books
        {
            list-style: none;
            background: url(/images/books.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests
        {
            list-style: none;
            background: url(/images/tests.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures
        {
            list-style: none;
            background: url(/images/electures.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms
        {
            list-style: none;
            background: url(/images/classrooms.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        /*Remove these after sometime*/.tabb .tabing li.books1, .area1 .center_con li.books1
        {
            list-style: none;
            background: url(/images/books_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests1, .area1 .center_con li.tests1
        {
            list-style: none;
            background: url(/images/tests_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures1, .area1 .center_con li.e-lectures1
        {
            list-style: none;
            background: url(/images/electures_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms1, .area1 .center_con li.classrooms1
        {
            list-style: none;
            background: url(/images/classrooms_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.books2
        {
            list-style: none;
            background: url(/images/books_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests2
        {
            list-style: none;
            background: url(/images/tests_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures2
        {
            list-style: none;
            background: url(/images/electures_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms2
        {
            list-style: none;
            background: url(/images/classrooms_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        /*Remove these after sometime*/.tabb .tabing li h5
        {
            font-size: 16px;
            margin: 0 0 2px 0;
            color: #000;
            font-weight: bold;
        }
        .tabing
        {
            width: 100%;
            float: left;
            margin-left: 0;
        }
        .tabb
        {
            width: 90%;
            float: left;
        }
        .tabb1
        {
            width: 23.8%;
            float: right;
        }
        .tabb-vedio
        {
            /*height: 200px; margin-top: 10px; width: 269px;*/
            position: relative;
            padding-bottom: 62.25%; /* ratio for youtube embed */
            padding-top: 30px;
            height: auto;
            overflow: hidden;
        }
        .tabb-vedio iframe, .tabb-vedio object, .tabb-vedio embed
        {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        .tabb-img img
        {
            height: 200px;
        }
        .tabing li
        {
            height: 110px;
            margin: 30px;
        }
    </style>
    <style>
        img.alignnone
        {
            float: left;
            margin-right: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main_wrapper">
        <div style="width: 80%; margin: 20px auto;">
            <div style="width: 100%; float: left;">
                <h2 style="border-bottom: 2px solid #fec907;">
                    Gate Course Details</h2>
                <br />
                <br />
                <div class="tabb">
                    <ul class="tabing">
                        <li class="classrooms1">
                            <h5>
                                Live Online Classes</h5>
                            <p>
                                You receive 1400 hours of (Live + VoD)sessions from IIT/IISc alumni via the internet.</p>
                        </li>
                        <li class="books1">
                            <h5>
                                Books</h5>
                            <p>
                                12 to 16 GATE Focused Books. Theory, Questions, Solutions.</p>
                        </li>
                        <li class="books1">
                            <h5>
                                Refresher Guide</h5>
                            <p>
                                Important concepts and formulae Book for last minute revisions.</p>
                        </li>
                        <li class="tests1">
                            <h5>
                                Subject Wise Tests</h5>
                            <p>
                                500+ Problems &amp; Solutions Grouped under 10 Subjects.</p>
                        </li>
                        <li class="tests1">
                            <h5>
                                Chapter Wise Tests</h5>
                            <p>
                                800+ Problems &amp; Solutions Grouped under 40 Chapters.</p>
                        </li>
                        <li class="tests1">
                            <h5>
                                All India Mock Tests</h5>
                            <p>
                                12 National Level Online Tests with Video Solutions.</p>
                        </li>
                        <li class="tests1">
                            <h5>
                                Full Length Tests</h5>
                            <p>
                                8 Online Test Based on latest GATE Syllabus available from day one.</p>
                        </li>
                        <li class="classrooms1">
                            <h5>
                                Post GATE Guidance</h5>
                            <p>
                                Online Guidance on how to best use GATE Score for Admissions.</p>
                        </li>
                    </ul>
                </div>
                <br />
                <br />
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>
</asp:Content>
