﻿<%@ Page Language="C#" MasterPageFile="~/NewHome.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs"
    Inherits="OnlineExam.Web.Home" Title="Home Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        a
        {
            cursor: pointer;
        }
        .tabb .tabing li
        {
            list-style: none;
            background: url(/images/Earth.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 60px 0 0;
            margin-top: 15px;
            font-size: 13px;
            width: 23.5%;
            line-height: 15px;
            text-align: center;
        }
        .tabb .tabing li p
        {
            color: #666;
        }
        .postid-1426 .tabb .tabing li
        {
            width: 25% !important;
            margin-right: 18px !important;
        }
        .postid-1645 .tabb .tabing li, .postid-1647 .tabb .tabing li, .postid-1649 .tabb .tabing li
        {
            width: 30.5% !important;
        }
        .postid-1646 .tabb .tabing li, .postid-1648 .tabb .tabing li
        {
            width: 30.5% !important;
            min-height: 80px !important;
        }
        .tabb .tabing li.books
        {
            list-style: none;
            background: url(/images/books.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests
        {
            list-style: none;
            background: url(/images/tests.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures
        {
            list-style: none;
            background: url(/images/electures.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms
        {
            list-style: none;
            background: url(/images/classrooms.png) left top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: left;
            color: #000;
            padding: 0 0 0 60px;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        /*Remove these after sometime*/.tabb .tabing li.books1, .area1 .center_con li.books1
        {
            list-style: none;
            background: url(/images/books_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests1, .area1 .center_con li.tests1
        {
            list-style: none;
            background: url(/images/tests_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures1, .area1 .center_con li.e-lectures1
        {
            list-style: none;
            background: url(/images/electures_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms1, .area1 .center_con li.classrooms1
        {
            list-style: none;
            background: url(/images/classrooms_1.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.books2
        {
            list-style: none;
            background: url(/images/books_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.tests2
        {
            list-style: none;
            background: url(/images/tests_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.e-lectures2
        {
            list-style: none;
            background: url(/images/electures_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        .tabb .tabing li.classrooms2
        {
            list-style: none;
            background: url(/images/classrooms_2.png) center top no-repeat;
            float: left;
            margin-right: 20px;
            text-align: center;
            color: #000;
            padding: 60px 0 0 0;
            margin-top: 15px;
            font-size: 13px;
            line-height: 15px;
        }
        /*Remove these after sometime*/.tabb .tabing li h5
        {
            font-size: 16px;
            margin: 0 0 2px 0;
        }
        .tabing
        {
            width: 100%;
            float: left;
            margin-left: 0;
        }
        .tabb
        {
            width: 75%;
            float: left;
        }
        .tabb1
        {
            width: 23.8%;
            float: right;
        }
        .tabb-vedio
        {
            /*height: 200px; margin-top: 10px; width: 269px;*/
            position: relative;
            padding-bottom: 62.25%; /* ratio for youtube embed */
            padding-top: 30px;
            height: auto;
            overflow: hidden;
        }
        .tabb-vedio iframe, .tabb-vedio object, .tabb-vedio embed
        {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        .tabb-img img
        {
            height: 200px;
        }
        .hovereffect
        {
            width: 100%;
            height: 100%;
            float: left;
            overflow: hidden;
            position: relative;
            text-align: center;
            cursor: default;
        }
        .hovereffect .overlay
        {
            width: 100%;
            height: 100%;
            position: absolute;
            overflow: hidden;
            top: 0;
            left: 0;
            background-color: rgba(75,75,75,0.7);
            -webkit-transition: all 0.4s ease-in-out;
            transition: all 0.4s ease-in-out;
        }
        .hovereffect:hover .overlay
        {
            background-color: rgba(48, 152, 157, 0.4);
        }
        .hovereffect img
        {
            width: 320px;
            height: 270px;
            display: block;
            position: relative;
        }
        .hovereffect h2
        {
            color: #fff;
            text-align: center;
            position: relative;
            font-size: 17px;
            padding: 10px;
            background: rgba(0, 0, 0, 0.6);
            -webkit-transform: translateY(225px);
            -ms-transform: translateY(225px);
            transform: translateY(225px);
            -webkit-transition: all 0.4s ease-in-out;
            transition: all 0.4s ease-in-out;
        }
        .hovereffect:hover h2
        {
            -webkit-transform: translateY(5px);
            -ms-transform: translateY(5px);
            transform: translateY(5px);
        }
        .hovereffect a.info
        {
            display: inline-block;
            text-decoration: none;
            padding: 14px;
            text-transform: uppercase;
            color: #fff;
            border: 1px solid #fff;
            background-color: transparent;
            opacity: 0;
            filter: alpha(opacity=0);
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
            transform: scale(0);
            -webkit-transition: all 0.4s ease-in-out;
            transition: all 0.4s ease-in-out;
            font-weight: normal;
            margin: -52px 0 0 0;
            padding: 62px 10px;
        }
        .hovereffect:hover a.info
        {
            opacity: 1;
            filter: alpha(opacity=100);
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
        }
        .hovereffect a.info:hover
        {
            box-shadow: 0 0 5px #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main_wrapper">
        <div style="display: none" class="sld_cntr">
            <div class="box_skitter box_skitter_large desktop_show" style="margin: 0; padding: 0;
                height: 161px !important;">
                <ul>
                    <li><a target="_blank" href="http://cbt.onlinetestseriesmadeeasy.in/">
                        <img src="/images/1Online-Banner-design_373.jpg" title="" alt="Gate Topper Image"
                            class="block" />
                    </a></li>
                    <li><a target="_blank" href="http://onlinetestseriesmadeeasy.in/">
                        <img src="/images/4APPSCOTS_375.jpg" title="" alt="Gate Topper Image" class="block" />
                    </a></li>
                    <li><a target="_blank" href="home/fee-structure">
                        <img src="/images/Offline_Test_377.jpg" title="" alt="Gate Topper Image" class="block" />
                    </a></li>
                    <li><a target="_blank" href="admin/MarqueeDoc/2ME_bhopal_259_5.pdf">
                        <img src="/images/4Bhopal_359.jpg" title="" alt="Gate Topper Image" class="block" />
                    </a></li>
                    <li><a target="_blank" href="1PostalStudyCourse.pdf">
                        <img src="/images/7postal_226.jpg" title="" alt="Gate Topper Image" class="block" />
                    </a></li>
                    <li><a target="_blank" href="PuneSeminar15January.pdf">
                        <img src="/images/Pune_Seminar_15January_391.jpg" title="" alt="Gate Topper Image"
                            class="block" />
                    </a></li>
                </ul>
            </div>
        </div>
        <div class="sld_cntr mob_show">
            <div id="owl-demo" class="owl-carousel">
                <div class="item">
                    <a target="_blank" href="http://cbt.onlinetestseriesmadeeasy.in/">
                        <img src="/images/1Online-Banner-design_373.jpg" title="" alt="Gate Topper Image"
                            width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="http://nst.onlinetestseriesmadeeasy.in/">
                        <img src="/images/NST_Banner_1_376.jpg" title="" alt="Gate Topper Image" width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="http://onlinetestseriesmadeeasy.in/">
                        <img src="/images/4APPSCOTS_375.jpg" title="" alt="Gate Topper Image" width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="home/fee-structure">
                        <img src="/images/Offline_Test_377.jpg" title="" alt="Gate Topper Image" width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="admin/MarqueeDoc/2ME_bhopal_259_5.pdf">
                        <img src="/images/4Bhopal_359.jpg" title="" alt="Gate Topper Image" width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="images/upldPDF/1PostalStudyCourse_226.pdf">
                        <img src="/images/7postal_226.jpg" title="" alt="Gate Topper Image" width="100%">
                    </a>
                </div>
                <div class="item">
                    <a target="_blank" href="images/upldPDF/PuneSeminar15January_391.pdf">
                        <img src="/images/Pune_Seminar_15January_391.jpg" title="" alt="Gate Topper Image"
                            width="100%">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div style="width: 80%; margin: 20px auto;">
        <div style="width: 100%; float: left;">
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/homeGATE.jpg" alt="" />
                    <div class="overlay">
                        <h2>
                            homeGATE</h2>
                        <a class="info" href="#">Prepare for GATE at your home. Get trained by best faculties
                            for 600 hours of online LIVE classes, 800 hours of recorded lectures, books and
                            tests. Get recordings of missed classes </a>
                    </div>
                </div>
            </div>
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/Subject-page356x265.jpg" alt="">
                    <div class="overlay">
                        <h2>
                            Subject Pack</h2>
                        <a class="info" href="#">Choose your important subjects & pay only for those </a>
                    </div>
                </div>
            </div>
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/samsung-tabGATE_homepage.jpg" alt="">
                    <div class="overlay">
                        <h2>
                            tabGATE</h2>
                        <a class="info" href="#">GATE preparation, anytime, anywhere. High Quality GATE Content
                            preloaded on Android based tablet. E-books, e-lectures Tests & Quizzes prepared
                            by IIT/IISc graduates </a>
                    </div>
                </div>
            </div>
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/CPP3.jpg" alt="">
                    <div class="overlay">
                        <h2>
                            Correspondence Programs</h2>
                        <a class="info" href="#">Get the GATE preparation material delivered to your doorstep.
                            Books, refresher guides, e-lectures, question banks, mock test & more. </a>
                    </div>
                </div>
            </div>
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/classroom.png" alt="">
                    <div class="overlay">
                        <h2>
                            Classroom Courses</h2>
                        <a class="info" href="#">Strengthen your fundamentals through focused classroom sessions
                            of up to 500 hrs, conducted by GATE toppers and alumni of IITs /IISc. </a>
                    </div>
                </div>
            </div>
            <div style="width: 310px; float: left; margin: 20px;">
                <div class="hovereffect">
                    <img class="img-responsive" src="/images/test-series.png" alt="">
                    <div class="overlay">
                        <h2>
                            GATE Test Series</h2>
                        <a class="info" href="#">Prepare for GATE with All India mock tests, with detailed performance
                            analysis and All-India percentile ranking. Also get subject wise tests and sectional
                            tests. </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
