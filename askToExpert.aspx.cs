﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class askToExpert : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptExpert.DataBind();
                try
                {
                    if (Session["ID"].ToString() == "Yes")
                    {
                        Session.Remove("ID");
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        lblMsg.Text = "Ask your question, you will be notified by mail.";
                    }
                }
                catch { }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
            }
            else
            {
                Session["openPage"] = "askToExpert.aspx";
                Response.Redirect("LoginDetails.aspx?ReturnUrl=askToExpert.aspx");

            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.Page.MasterPageFile = "~/userMaster.master";
            }
            else
            {
                this.Page.MasterPageFile = "~/site1.master";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string[] np = HttpContext.Current.User.Identity.Name.Split(',');
            ob.askToExpert(txtQue.Text, np[0].ToString());
            Panel1.Visible = true;
            Panel2.Visible = false;
            lblMsg.Text = "Your question is submitted successfully";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            lblMsg.Text = "";
        }

    }
}
