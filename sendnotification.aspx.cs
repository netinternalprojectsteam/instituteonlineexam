﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;
using System.Web.Services;
using System.IO;
using System.Text;
using System.Net;

namespace OnlineExam
{
    public partial class sendnotification : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CalendarExtender1.SelectedDate = DateTime.Today;
                CalendarExtender2.SelectedDate = DateTime.Today;
                
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                gridUsers.DataBind();
                // GridView1.DataBind();
                if (gridUsers.Rows.Count > 0)
                {
                    //btnToExcel.Visible = true;
                }
                else
                { //btnToExcel.Visible = false;
                }
            }

        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceUser.SelectCommand = "SELECT uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate,GCMId FROM exam_users where ((CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "') and (instituteId=" + lblInsId.Text + ") ORDER BY edate DESC";
                gridUsers.DataBind();
                //GridView1.DataBind();
                if (gridUsers.Rows.Count > 0)
                {
                    // btnToExcel.Visible = true;
                }
                else
                {// btnToExcel.Visible = false; }
                }
            }
            catch (Exception ee)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter proper Dates");
                Messages11.setMessage(0, ee.Message);
            }
        }

        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkSelectAll.Checked == true)
                {
                    if (gridUsers.Rows.Count > 0)
                    {
                        for (int i = 0; i < gridUsers.Rows.Count; i++)
                        {
                            GridViewRow row = gridUsers.Rows[i];
                            CheckBox chk = (CheckBox)row.FindControl("chkSelect");
                            chk.Checked = true;
                        }
                    }
                }
                else
                {
                    if (gridUsers.Rows.Count > 0)
                    {
                        for (int i = 0; i < gridUsers.Rows.Count; i++)
                        {
                            GridViewRow row = gridUsers.Rows[i];
                            CheckBox chk = (CheckBox)row.FindControl("chkSelect");
                            chk.Checked = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ////messages1.setMessage(0, ex.Message);
                ////messages1.Visible = true;
                ////messages1.Focus();
                Messages11.Visible = true;
                // Messages11.setMessage(0, "Enter proper Dates");
                Messages11.setMessage(0, ex.Message);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridUsers.Rows.Count > 0)
                {
                    for (int i = 0; i < gridUsers.Rows.Count; i++)
                    {
                        GridViewRow row = gridUsers.Rows[i];
                        CheckBox chk = (CheckBox)row.FindControl("chkSelect");
                        if (chk.Checked == true)
                        {
                            if (gridUsers.Rows[i].Cells[8].Text != "&nbsp;")
                            {
                                if (lblNos.Text != "")
                                {
                                    lblNos.Text = lblNos.Text + ", " + gridUsers.Rows[i].Cells[8].Text;
                                }
                                else
                                {
                                    lblNos.Text = gridUsers.Rows[i].Cells[8].Text;
                                }
                            }


                        }
                    }
                    sendGCMForJob(txtmsg.Text, lblNos.Text);
                    lblNos.Text = "";
                    sourceUser.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    sourceUser.SelectCommand = "SELECT uname, uemail, mobileNo, city, uid, convert(varchar(10),edate,103) as edate,GCMId FROM exam_users where ((CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "') and (instituteId=" + lblInsId.Text + ") ORDER BY edate DESC";
                    gridUsers.DataBind();
                    chkSelectAll.Checked = false;
                    txtmsg.Text = "";
                }
                else
                {

                }

            }
            catch (Exception ex)
            {


                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
                Messages11.Focus();
            }


        }

        [WebMethod]
        public void sendGCMForJob(string msg, string GCMid)
        {
            try
            {

                string authkey = ConfigurationManager.AppSettings["authkey"].ToString();
                string[] GCM = GCMid.Split(',');

                //for (int j = 0; j < GCM.Length; j++)
                // {
                //   //DataTable dtBatch = new DataTable();
                //  // dtBatch = ob.getUsersbyid(uid[j]);
                //   if (dtBatch.Rows.Count > 0)
                //    {
                StringBuilder buildGCM = new StringBuilder();
                foreach (var row in GCM)
                {
                    if (row.ToString() != "")
                    {
                        buildGCM.Append("\"").Append(row.ToString()).Append("\"").Append(",");
                    }
                }
                string finalGCM = buildGCM.ToString(0, buildGCM.Length - 1);

                HttpWebRequest Request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");

                Request.Method = "POST";
                Request.KeepAlive = false;
                string registrationId = finalGCM;
                string cmessage = msg;
                string postData = "{\"registration_ids\": [ " + registrationId + " ], \"data\": {\"title\":\" Online Exam Notification Custom\",\"subtitle\": \"" + cmessage + "\",\"instituteID\": \"" + lblInsId.Text + "\",\"time_to_live\":\"2160000\"} }";

                Console.WriteLine(postData);
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                Request.ContentType = "application/json";
                Request.Headers.Add("Authorization", "key=" + authkey);

                //-- Create Stream to Write Byte Array --// 
                Stream dataStream = Request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                //-- Post a Message --//
                WebResponse Response = Request.GetResponse();
                HttpStatusCode ResponseCode = ((HttpWebResponse)Response).StatusCode;
                if (ResponseCode.Equals(HttpStatusCode.Unauthorized) || ResponseCode.Equals(HttpStatusCode.Forbidden))
                {
                    Console.WriteLine("Unauthorized - need new token");

                }
                else if (!ResponseCode.Equals(HttpStatusCode.OK))
                {
                    Console.WriteLine("Response from web service isn't OK");
                }

                StreamReader Reader = new StreamReader(Response.GetResponseStream());
                string responseLine = Reader.ReadLine();
                Reader.Close();
            }
            //    }

            //}
            catch (Exception) { }
        }

        protected void gridUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[8].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[8].Visible = false;
            }

        }
    }
}
