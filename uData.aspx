﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="uData.aspx.cs" Inherits="OnlineExam.uData" Title="Useful Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceMaterials" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT Mheading, id FROM site_StudyMaterialTable WHERE Mcategory = 'FREE STUDY MATERIAL' ORDER BY MAddDate DESC">
                        </asp:SqlDataSource>
                        <asp:Repeater ID="rptMaterials" runat="server" DataSourceID="sourceMaterials">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <div class="pageHeading">
                                                FREE STUDY MATERIAL
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="800px">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <%#Eval("Mheading") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <a href='studyDetails.aspx?ID=<%#Eval("id") %>' class='tLink1' target='_blank'>Read
                                                            More>></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceRec" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            runat="server" SelectCommand="SELECT Mheading, id FROM site_StudyMaterialTable WHERE Mcategory = 'CURRENT JOBS & RESULTS' ORDER BY MAddDate DESC">
                        </asp:SqlDataSource>
                        <asp:Repeater ID="rptRec" runat="server" DataSourceID="sourceRec">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <div class="pageHeading">
                                                CURRENT JOBS & RESULTS
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="800px">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <%#Eval("Mheading") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <a href='studyDetails.aspx?ID=<%#Eval("id") %>' target='_blank' class='tLink1'>Read
                                                            More>></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceBAwareness" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            runat="server" SelectCommand="SELECT Mheading, id FROM site_StudyMaterialTable WHERE Mcategory = 'BANKING AWARENESS' ORDER BY MAddDate DESC">
                        </asp:SqlDataSource>
                        <asp:Repeater ID="rptAwareness" runat="server" DataSourceID="sourceBAwareness">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <div class="pageHeading">
                                                BANKING AWARENESS
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="800px">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <%#Eval("Mheading") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <a href='studyDetails.aspx?ID=<%#Eval("id") %>' target='_blank' class='tLink1'>Read
                                                            More>></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceLinks" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT Mheading, id FROM site_StudyMaterialTable WHERE Mcategory = 'IMPORTANT LINKS' ORDER BY MAddDate DESC">
                        </asp:SqlDataSource>
                        <asp:Repeater ID="rptLinks" runat="server" DataSourceID="sourceLinks">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <div class="pageHeading">
                                                IMPORTANT LINKS
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="800px">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <%#Eval("Mheading") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <a href='studyDetails.aspx?ID=<%#Eval("id") %>' class='tLink1' target='_blank'>Read
                                                            More>></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
