﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true" CodeBehind="studyMaterial.aspx.cs"
    Inherits="OnlineExam.studyMaterial" Title="Study Material" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:SqlDataSource ID="sourceMaterials" runat="server" SelectCommand="SELECT id, title, description,'/UploadedDocs/'+ iconImage as iconImage, downloadFile, isActive, CASE WHEN len(description)>100 THEN left(description,100) + '...' ELSE description END as SDesc  FROM site_studyMaterials WHERE isActive = 1 ORDER BY id Desc"
                            ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"></asp:SqlDataSource>
                        <asp:Repeater ID="rptMaterials" runat="server" DataSourceID="sourceMaterials">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <div class="pageHeading">
                                                Study Material</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="800px">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td rowspan='3' style="width: 80px" align="center">
                                                        <img alt="" src='<%#Eval("iconImage") %>' width='50px' height='50px' />
                                                    </td>
                                                    <td align='left' valign='top' style='font-size: medium; color: Red; width: 600px'>
                                                        <b>
                                                            <%#Eval("title") %></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign='top' align='left'>
                                                        <%#Eval("SDesc")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align='right' style="width: 120px">
                                                        <a href='studyDetails.aspx?ID=<%#Eval("id")%>' target="_blank">Read More</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2'>
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
