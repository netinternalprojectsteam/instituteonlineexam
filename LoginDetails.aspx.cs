﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Globalization;
using System.Net.Mail;
using System.Web.Mail;

namespace OnlineExam
{
    public partial class LoginDetails : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                    
                    if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                    {
                        txtEmail.Text = Request.Cookies["UserName"].Value;
                        txtPassword.Attributes["value"] = base64Decode(Request.Cookies["Password"].Value);
                        chkUserRem.Checked = true;
                    }
                    else
                    {
                        txtEmail.Text = "";
                        txtPassword.Text = "";
                        chkUserRem.Checked = false;
                    }

                    sourceCountry.SelectCommand = "SELECT DISTINCT [ID], [countryName] FROM [countryList] order by countryName ASC";
                    sourceCountry.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlCountry.DataBind();

                    sourceState.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    sourceState.SelectCommand = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = " + ddlCountry.SelectedValue + " order by statename asc";
                    ddlState.DataBind();

                    DataTable dt = new DataTable();
                    dt = obj.getInstitute();
                    ddlInstitute.DataSource = dt;
                    ddlInstitute.DataBind();
                    ddlInstitute.Items.Insert(0, new ListItem("--Select--", "0"));
                }
                catch { }

                try
                {
                    lblErrorMessage.Text = Request.QueryString["eID"].ToString();
                }
                catch { }

                try
                {
                    lblRedirectTo.Text = Request.QueryString["ReturnUrl"].ToString();
                }
                catch { }

                //lblErrorMessage.Text = "3";
            }

        }

        protected void btnUserLogin_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (txtEmail.Text.Length > 0 && txtPassword.Text.Length > 0)
            {
                lblUserError.Visible = false;
                try
                {
                    DataTable dt = new DataTable();
                    dt = obj.GetActiveUserLoginInfo(txtEmail.Text, base64Encode(txtPassword.Text));

                    if (dt.Rows.Count > 0)
                    {

                        if (lblRedirectTo.Text.Contains("askToExpert.aspx"))
                        {
                            //Session["ID"] = "Yes";
                            //FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "User" + "," + "NXGOnlineExam", chkUserRem.Checked);

                        }
                        else
                        {
                            if (dt.Rows.Count > 1)
                            {
                                Session["loginEmail"] = txtEmail.Text;
                                Session["logPass"] = txtPassword.Text;
                                Session["remember"] = chkUserRem.Checked;
                                Response.Redirect("getInstitute.aspx", false);
                            }
                            else
                            {
                                Session["loginEmail"] = null;
                                if (chkUserRem.Checked)
                                {
                                    Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                                    Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                                }
                                else
                                {
                                    Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                                }
                                Response.Cookies["UserName"].Value = txtEmail.Text.Trim();
                                Response.Cookies["Password"].Value = base64Encode(txtPassword.Text.Trim());
                                FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "User" + "," + "NXGOnlineExam" + "," + dt.Rows[0]["instituteId"].ToString(), true);

                                Session["InstituteID"] = dt.Rows[0]["instituteId"].ToString();
                            }
                        }
                    }
                    else
                    {
                        //lblUserError.Visible = true;
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Invalid Login Details !");
                    }
                }
                catch (Exception ee) { lblErrorMessage.Text = ee.Message; }
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Invalid Login Details !");
            }
        }

        protected void btnNewAccount_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (txtFirstName.Text.Length > 0 && txtEmailID.Text.Length > 0 && txtMo.Text.Length > 0)
            {
                if (ddlInstitute.SelectedItem.Text != "--Select--")
                {
                    if (ddlState.SelectedItem.Text != "-Select-" && ddlCountry.SelectedItem.Text != "-Select-")
                    {
                        if (txtCity.Text.Length > 0)
                        {
                            if (txtAdd1.Text.Length > 0 && txtAdd2.Text.Length > 0)
                            {
                                DataTable dt = new DataTable();
                                dt = obj.GetUserInfoByMail(txtEmailID.Text, ddlInstitute.SelectedValue);
                                if (dt.Rows.Count == 0)
                                {
                                    lblSignUpError.Visible = false;
                                    try
                                    {
                                        // to generate random alphanumeric linkID for sending in link to set password
                                        Random random = new Random();
                                        string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                        for (int i = 0; i < 6; i++)
                                        {
                                            int x = random.Next(0, 53);
                                            sb.Append(array[x]);
                                        }
                                        string linkID = sb.ToString();

                                        int maxID = obj.CreateUserAccount(txtFirstName.Text, txtEmailID.Text, "", txtMo.Text, txtAdd1.Text, txtAdd2.Text, ddlCountry.SelectedItem.Text, ddlState.SelectedItem.Text, txtCity.Text, "Default.jpg", linkID, ddlInstitute.SelectedValue);

                                        try
                                        {
                                            string link = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/setPassword.aspx";
                                            string mail = "Hello " + txtFirstName.Text + "<br> Thank you for using the pvkexams.com.<br>To complete the registration process, please <a href='" + link + "?regID=" + linkID + "@" + maxID + "'>Click Here</a><br><br>Regards<br><a href='http://pvkexams.com'>Team pvkExams</a>'<br><br><b>ALL THE BEST</b>";
                                            lblMail.Text = mail;

                                            //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"].ToString();
                                            //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"].ToString();

                                            //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(infoEID, txtEmailID.Text, "Profile Details at pgfunda.com", lblMail.Text);
                                            //mm.IsBodyHtml = true;
                                            //System.Net.Mail.SmtpClient client = new SmtpClient();
                                            //client.Credentials = new System.Net.NetworkCredential(infoEID, infoPass);
                                            //client.Send(mm);

                                            string emailID = "";
                                            string password = "";
                                            string smtpHost = "";
                                            string smtpPort = "";
                                            bool smtpEnable;

                                            DataTable dtEmail = new DataTable();
                                            dtEmail = obj.getData("Select id, emailID, password, smtpHost, smtpPort, smtpEnable from email_Config where instituteId=" + ddlInstitute.SelectedValue + "");
                                            if (dtEmail.Rows.Count > 0)
                                            {
                                                emailID = dtEmail.Rows[0]["emailID"].ToString();
                                                password = base64Decode(dtEmail.Rows[0]["password"].ToString());
                                                smtpHost = dtEmail.Rows[0]["smtpHost"].ToString();
                                                smtpPort = dtEmail.Rows[0]["smtpPort"].ToString();
                                                smtpEnable = Convert.ToBoolean(dtEmail.Rows[0]["smtpEnable"].ToString());
                                            }
                                            else
                                            {
                                                emailID = ConfigurationManager.AppSettings["InfoEmailID"];
                                                password = ConfigurationManager.AppSettings["InfoEmailPass"];
                                                smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                                                smtpPort = ConfigurationManager.AppSettings["smtpPort"];
                                                smtpEnable = true;
                                            }

                                            System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                                            //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                                            SmtpClient Sc = new SmtpClient(smtpHost);
                                            //string port = ConfigurationManager.AppSettings["smtpPort"];
                                            Sc.Port = Convert.ToInt32(smtpPort);
                                            //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                                            //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                                            Sc.Credentials = new NetworkCredential(emailID, password);
                                            Sc.EnableSsl = smtpEnable;
                                            ms.From = new MailAddress(emailID);
                                            ms.To.Add(txtEmailID.Text);

                                            ms.Subject = "Profile Details at pvkexams.com";
                                            ms.Body = mail;
                                            ms.IsBodyHtml = true;
                                            Sc.Send(ms);

                                            Messages11.Visible = true;
                                            Messages11.setMessage(1, "Please check your Email....!");

                                        }
                                        catch (Exception ee)
                                        {
                                            Messages11.Visible = true;
                                            Messages11.setMessage(0, ee.Message);
                                        }

                                        txtFirstName.Text = "";
                                        txtEmailID.Text = "";
                                        txtMo.Text = "";
                                        txtAdd1.Text = ""; txtAdd2.Text = "";
                                        txtCity.Text = "";
                                        ddlInstitute.ClearSelection();


                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message.Contains("Violation of PRIMARY KEY constraint 'PK_exam_users'. Cannot insert duplicate key in object 'dbo.exam_users'"))
                                        {

                                            Messages11.Visible = true;
                                            Messages11.setMessage(0, "Email Id already registered !");

                                        }
                                        if (ex.Message.Contains("Cannot insert duplicate key row in object 'dbo.exam_users' with unique index 'IX_exam_users'"))
                                        {
                                            Messages11.Visible = true;
                                            Messages11.setMessage(0, "Mobile No. already registered !");
                                        }
                                    }
                                }
                                else
                                {
                                    if (dt.Rows[0]["IsActive"].ToString() == "False")
                                    {
                                        try
                                        {
                                            string link = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/setPassword.aspx";
                                            string mail = "Hello " + txtFirstName.Text + "<br> Thank you for using the pvkexams.com<br>To complete the registration process, please <a href='" + link + "?regID=" + dt.Rows[0]["linkID"].ToString() + "@" + dt.Rows[0]["uid"].ToString() + "'>Click Here</a><br><br>Regards<br><a href='http://pvkexams.com'>Team pvkExams</a>'";
                                            lblMail.Text = mail;

                                            //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"].ToString();
                                            //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"].ToString();

                                            //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(infoEID, txtEmailID.Text, "Profile Details at pgfunda.com", lblMail.Text);
                                            //mm.IsBodyHtml = true;
                                            //System.Net.Mail.SmtpClient client = new SmtpClient();
                                            //client.Credentials = new System.Net.NetworkCredential(infoEID, infoPass);
                                            //client.Send(mm);

                                            string emailID = "";
                                            string password = "";
                                            string smtpHost = "";
                                            string smtpPort = "";
                                            bool smtpEnable;

                                            DataTable dtEmail = new DataTable();
                                            dtEmail = obj.getData("Select id, emailID, password, smtpHost, smtpPort, smtpEnable from email_Config where instituteId=" + ddlInstitute.SelectedValue + "");
                                            if (dtEmail.Rows.Count > 0)
                                            {
                                                emailID = dtEmail.Rows[0]["emailID"].ToString();
                                                password = base64Decode(dtEmail.Rows[0]["password"].ToString());
                                                smtpHost = dtEmail.Rows[0]["smtpHost"].ToString();
                                                smtpPort = dtEmail.Rows[0]["smtpPort"].ToString();
                                                smtpEnable = Convert.ToBoolean(dtEmail.Rows[0]["smtpEnable"].ToString());
                                            }
                                            else
                                            {
                                                emailID = ConfigurationManager.AppSettings["InfoEmailID"];
                                                password = ConfigurationManager.AppSettings["InfoEmailPass"];
                                                smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                                                smtpPort = ConfigurationManager.AppSettings["smtpPort"];
                                                smtpEnable = true;
                                            }

                                            System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                                            //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                                            SmtpClient Sc = new SmtpClient(smtpHost);
                                            //string port = ConfigurationManager.AppSettings["smtpPort"];
                                            Sc.Port = Convert.ToInt32(smtpPort);
                                            // string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                                            //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                                            Sc.Credentials = new NetworkCredential(emailID, password);
                                            Sc.EnableSsl = smtpEnable;
                                            ms.From = new MailAddress(emailID);
                                            ms.To.Add(txtEmailID.Text);

                                            ms.Subject = "Profile Details at pvkexams.com";
                                            ms.Body = mail;
                                            ms.IsBodyHtml = true;
                                            Sc.Send(ms);

                                            Messages11.Visible = true;
                                            Messages11.setMessage(1, "Please check your Email....!");
                                            txtFirstName.Text = "";
                                            ddlInstitute.ClearSelection();
                                            txtEmailID.Text = "";
                                            txtMo.Text = "";
                                            txtAdd1.Text = ""; txtAdd2.Text = "";
                                            txtCity.Text = "";


                                        }
                                        catch (Exception ee) { lblMessage.Text = ee.Message; }
                                    }
                                    else
                                    {

                                        //txtFirstName.Text = "";
                                        //txtEmailID.Text = "";
                                        //txtMo.Text = "";
                                        //txtAdd1.Text = ""; txtAdd2.Text = "";
                                        //txtCity.Text = "";
                                        //ddlInstitute.ClearSelection();
                                        Messages11.Visible = true;
                                        Messages11.setMessage(0, "Email ID already registered for selected Institute!<br> Please try again using different Email!"); //Click Forgot Password to get Password !");
                                    }
                                }
                            }
                            else
                            {
                                Messages11.Visible = true;
                                Messages11.setMessage(0, "Enter Proper Address !");
                            }
                        }
                        else
                        {
                            Messages11.Visible = true;
                            Messages11.setMessage(0, "Enter Proper City Name !");

                        }
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "Select Proper Country and State Name !");
                    }
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Select Proper Institute Name !");
                }
            }
            else
            {
                //lblSignUpError.Visible = true;
                //lblSignUpError.Text = "Enter Proper Details !";
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter Proper Details !");

            }
        }

        protected void btnAdminLogin_Click(object sender, EventArgs e)
        {
            if (txtUName.Text.Length > 0 && txtPass.Text.Length > 0)
            {
                lblAdminError.Visible = false;
                DataTable dt = new DataTable();
                dt = obj.GetUsernameAndPassword(txtUName.Text, txtPass.Text);
                if (dt.Rows.Count > 0)
                {
                    FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "Admin" + "," + "NXGOnlineExam", chkAdminRem.Checked);

                }
                else
                {
                    lblAdminError.Visible = true;
                }
            }
            else { lblAdminError.Visible = true; }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            sourceState.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceState.SelectCommand = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = " + ddlCountry.SelectedValue + " order by statename asc";
            ddlState.DataBind();
        }

        protected void linkUserForget_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Session["UType"] = "User";
            modalForgetPass.Show();
        }

        protected void linkAdminForget_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Session["UType"] = "Admin";
            modalForgetPass.Show();
        }

        private string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }


        //[System.Web.Script.Services.ScriptMethod]
        //[System.Web.Services.WebMethod]
        //public static List<string> GetInstitute(string prefixText)
        //{
        //    classes.DataLogic obj = new classes.DataLogic();
        //    DataTable dt = new DataTable();
        //    dt = obj.getData("Select InstituteName from admin_user where InstituteName LIKE '%'+'" + prefixText + "'+'%'");
        //    List<string> items = new List<string>();

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        string strName = dt.Rows[i][0].ToString();
        //        items.Add(strName);
        //    }
        //    return items;
        //}
    }
}
