﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Globalization;

namespace OnlineExam
{
    public partial class regUsers : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gridUsers.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                sourceUsers.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceUsers.SelectCommand = "SELECT [uid], [uname], [uemail],[mobileno],convert(varchar(10),edate,103) as edate FROM [exam_users] where (CAST(FLOOR(CAST(edate AS float)) AS datetime)) between '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' and '" + Convert.ToDateTime(txtToDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' or (CAST(FLOOR(CAST(edate AS float)) AS datetime)) = '" + Convert.ToDateTime(txtFromDate.Text, CultureInfo.GetCultureInfo("hi-IN")) + "' order by uname asc";
                gridUsers.DataBind();
            }
            catch (Exception)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter proper dates !");
            }
        }

        protected void gridUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "uDetails")
            {
                row = Convert.ToInt32(e.CommandArgument);
                Session["DetailsID"] = gridUsers.Rows[row].Cells[1].Text;
                modalPopupUDetails.Show();
            }
        }

        protected void gridUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }
    }
}
