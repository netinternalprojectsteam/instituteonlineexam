﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace OnlineExam
{
    public partial class forgetPassword : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblUType.Text = Session["UType"].ToString();
                    Session.Remove("UType");

                }
            }
            catch { }
        }

        protected void btnGetPass_Click(object sender, EventArgs e)
        {
            //if (txtEmail.Text.Length > 0)
            //{
            //string query = "";
            DataTable dt = new DataTable();
            //if (lblUType.Text == "Admin")
            //{
            //    query = "SELECT username, password, name FROM admin_user where username = '" + txtEmail.Text + "'";
            //}
            //if (lblUType.Text == "User")
            //{
            //    query = "SELECT uname, uemail, password FROM  exam_users where uemail = '" + txtEmail.Text + "'";
            //}
            //dt = ob.getData(query);
            //if (dt.Rows.Count > 0)
            //{

            dt = ob.GetUserInfoByMail(txtEmail.Text, ddlInstitute.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                string mail = "Hello " + dt.Rows[0]["uname"].ToString() + "<br> <br> Your Login details <br><b>Email : </b>" + dt.Rows[0]["uemail"].ToString() + "<br><b>Pasword : </b>" + base64Decode(dt.Rows[0]["password"].ToString()) + "<br><br>Regards<br><a href='http://pvkexams.com'>Team pvkExams</a>'";
                lblMail.Text = mail;

                //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"].ToString();
                //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"].ToString();

                //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(infoEID, dt.Rows[0]["uemail"].ToString(), "Profile Details at pgfunda.com", lblMail.Text);
                //mm.IsBodyHtml = true;
                //System.Net.Mail.SmtpClient client = new SmtpClient();
                //client.Credentials = new System.Net.NetworkCredential(infoEID, infoPass);
                //client.Send(mm);


                string emailID = "";
                string password = "";
                string smtpHost = "";
                string smtpPort = "";
                bool smtpEnable;

                DataTable dtEmail = new DataTable();
                dtEmail = ob.getData("Select id, emailID, password, smtpHost, smtpPort, smtpEnable from email_Config where instituteId=" + ddlInstitute.SelectedValue + "");
                if (dtEmail.Rows.Count > 0)
                {
                    emailID = dtEmail.Rows[0]["emailID"].ToString();
                    password = base64Decode(dtEmail.Rows[0]["password"].ToString());
                    smtpHost = dtEmail.Rows[0]["smtpHost"].ToString();
                    smtpPort = dtEmail.Rows[0]["smtpPort"].ToString();
                    smtpEnable = Convert.ToBoolean(dtEmail.Rows[0]["smtpEnable"].ToString());
                }
                else
                {
                    emailID = ConfigurationManager.AppSettings["InfoEmailID"];
                    password = ConfigurationManager.AppSettings["InfoEmailPass"];
                    smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                    smtpPort = ConfigurationManager.AppSettings["smtpPort"];
                    smtpEnable = true;
                }


                System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                // string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                SmtpClient Sc = new SmtpClient(smtpHost);
                //string port = ConfigurationManager.AppSettings["smtpPort"];
                Sc.Port = Convert.ToInt32(smtpPort);
                //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                Sc.Credentials = new NetworkCredential(emailID, password);
                Sc.EnableSsl = smtpEnable;
                ms.From = new MailAddress(emailID);
                ms.To.Add(dt.Rows[0]["uemail"].ToString());

                ms.Subject = "Profile Details at pvkexams.com";
                ms.Body = mail;
                ms.IsBodyHtml = true;
                Sc.Send(ms);

                lblMsg.Text = "Please check your email...!";
                lblMsg.ForeColor = System.Drawing.Color.Black;
                txtEmail.Text = "";
                lblMsg.Visible = true;

                txtEmail.Enabled = btnNext.Visible = true;
                Label2.Visible = ddlInstitute.Visible = btnGetPass.Visible = false;
            }
            //}
            //else
            //{
            //    lblMsg.Text = "No user details found with this email id !";
            //    lblMsg.Visible = true;
            //    lblMsg.ForeColor = System.Drawing.Color.Red;
            //}
            //}
            //else
            //{
            //    lblMsg.Text = "Enter proper email id !";
            //    lblMsg.Visible = true;
            //    lblMsg.ForeColor = System.Drawing.Color.Red;
            //}
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text.Length > 0)
            {
                DataTable dt = new DataTable();
                if (lblUType.Text == "User")
                {
                    dt = ob.getUserInstituteByEmailOnly(txtEmail.Text);
                }
                if (dt.Rows.Count > 0)
                {
                    ddlInstitute.DataSource = dt;
                    ddlInstitute.DataBind();

                    txtEmail.Enabled = btnNext.Visible = false;
                    Label2.Visible = ddlInstitute.Visible = btnGetPass.Visible = true;
                }
                else
                {
                    txtEmail.Enabled = btnNext.Visible = true;
                    Label2.Visible = ddlInstitute.Visible = btnGetPass.Visible = false;

                    lblMsg.Text = "No user details found with this email id !";
                    lblMsg.Visible = true;
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                txtEmail.Enabled = btnNext.Visible = true;
                Label2.Visible = ddlInstitute.Visible = btnGetPass.Visible = false;

                lblMsg.Text = "Enter proper email id !";
                lblMsg.Visible = true;
                lblMsg.ForeColor = System.Drawing.Color.Red;
            }
        }

    }
}
