﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class myAccount : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    if (HttpContext.Current.User.Identity.Name.ToString().Length > 0)
                    {//uemail, uname, mobileno
                        string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                        if (val[2].ToString() == "NXGOnlineExam")
                        {
                            if (val[1].ToString() == "User")
                            {
                                DataTable dt = new DataTable();
                                dt = ob.GetUserInfoByID(val[0].ToString());
                                try
                                {
                                    lblInstitute.Text = dt.Rows[0]["institute"].ToString();
                                }
                                catch { }
                                lblName.Text = dt.Rows[0]["uname"].ToString();
                                lblEmail.Text = dt.Rows[0]["uemail"].ToString();
                                lblMo.Text = dt.Rows[0]["mobileno"].ToString();
                                lblUID.Text = val[0].ToString();
                                if (dt.Rows[0]["imageName"].ToString().Length > 0)
                                {
                                    Image1.ImageUrl = "/stdImages/" + dt.Rows[0]["imageName"].ToString() + "";
                                    linkRemoveImage.Visible = true;
                                }

                                if (Image1.ImageUrl.Contains("default.jpg"))
                                {
                                    linkRemoveImage.Visible = false;
                                }
                                else { linkRemoveImage.Visible = true; }
                                Session["editUserID"] = val[0].ToString();


                                //rptCourseDetails.DataSource = dt;
                                //rptCourseDetails.DataBind();


                                string details = "";
                                if (dt.Rows[0]["Add1"].ToString().Length > 0)
                                {
                                    details = dt.Rows[0]["Add1"].ToString() + ",<br>";
                                }

                                //if (details.Trim().Length > 0)
                                //{ details = details + ",<br>"; }

                                if (dt.Rows[0]["Add2"].ToString().Length > 0)
                                {
                                    details = dt.Rows[0]["Add2"].ToString() + ",<br>";
                                }

                                if (dt.Rows[0]["city"].ToString().Length > 0)
                                {
                                    if (details.Length > 0)
                                    { details = details + dt.Rows[0]["city"].ToString() + ","; }
                                    else { details = dt.Rows[0]["city"].ToString() + ","; }
                                }

                                details = details + "<br>" + dt.Rows[0]["state"].ToString();
                                details = details + "<br>" + dt.Rows[0]["country"].ToString();
                                lblAdd.Text = details;
                                lblEEmail.Text = dt.Rows[0]["extraEmail"].ToString();
                                lblAbtyou.Text = dt.Rows[0]["aboutU"].ToString();

                                sourceFinishedExam.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                                sourceFinishedExam.SelectCommand = "SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, exm_newExam.onDate, exm_newExam.maxTime,  exm_existingExams.examName FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid INNER JOIN exm_existingExams ON exm_newExam.preexamID = exm_existingExams.examID WHERE (exm_newExam.userID = " + lblUID.Text + ") AND (exm_newExam.isFinish = 1) ORDER BY exm_newExam.newexm DESC";
                                gridFinishedExam.DataBind();

                                SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                                SqlDataSource1.SelectCommand = "SELECT id,testimony from site_Testimony WHERE usreID= " + lblUID.Text + "";
                                Repeater1.DataBind();


                                sourceExpert.SelectCommand = "select question,case when isnull(answer,0) =0 then 'Not Answered !' else answer end as answer from site_asktoexpert WHERE userid = " + lblUID.Text + "";
                                sourceExpert.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                                rptExpert.DataBind();

                                if (Repeater1.Items.Count >= 3)
                                {
                                    linkAddTetimony.Visible = false;
                                }

                                try
                                {
                                    DataTable dt1 = new DataTable();
                                    //dt1 = ob.getData("SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, exm_newExam.onDate, exm_newExam.maxTime, (select min(remainingtime) from exm_userAnswers where exmID=exm_newExam.newexm) as remTime FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid where userID = " + lblUID.Text + " and isFinish = 0");
                                    dt1 = ob.getData("SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, exm_newExam.onDate, exm_newExam.maxTime, isnull((SELECT     MIN(remainingTime) AS Expr1 FROM exm_userAnswers WHERE  (exmid = exm_newExam.newexm)),0) AS remTime, exm_existingExams.examName FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid INNER JOIN exm_existingExams ON exm_newExam.preexamID = exm_existingExams.examID WHERE (exm_newExam.userID = " + lblUID.Text + ") AND (exm_newExam.isFinish = 0) ORDER BY exm_newExam.newexm DESC");

                                    DataTable dtUnfinished = new DataTable();
                                    dtUnfinished.Columns.Add("newexm", typeof(string));
                                    dtUnfinished.Columns.Add("examName", typeof(string));
                                    dtUnfinished.Columns.Add("noquestion", typeof(string));
                                    dtUnfinished.Columns.Add("onDate", typeof(string));
                                    dtUnfinished.Columns.Add("remTime", typeof(string));

                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        DataRow dtrow = dtUnfinished.NewRow();
                                        dtrow["newexm"] = dt1.Rows[i]["newexm"].ToString();
                                        dtrow["examName"] = dt1.Rows[i]["examName"].ToString();
                                        dtrow["noquestion"] = dt1.Rows[i]["noquestion"].ToString();
                                        dtrow["onDate"] = dt1.Rows[i]["onDate"].ToString();
                                        string s11111 = dt1.Rows[i]["remTime"].ToString();
                                        //int remTime = 3600000;
                                        DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt1.Rows[i]["remTime"].ToString()));
                                        TimeSpan time1 = new TimeSpan();
                                        time1 = ((DateTime)remTime1) - DateTime.Now;
                                        string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                                        dtrow["remTime"] = r1;
                                        dtUnfinished.Rows.Add(dtrow);
                                    }

                                    gridUnFinished.DataSource = dtUnfinished;
                                    gridUnFinished.DataBind();
                                }
                                catch { }



                            }

                        }

                    }
                    else { Response.Redirect("/Default.aspx"); }

                }
            }
            catch { }

        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            //Panel1.Visible = true; ;
            //btnChange.Visible = false;
        }

        protected void btnChangeImg_Click(object sender, EventArgs e)
        {
            //string fileName = "";
            //if (FileUpload1.HasFile)
            //{
            //    string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
            //    string extToUpper = ext.ToUpper();
            //    if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
            //    {
            //        if (FileUpload1.FileBytes.Length >= 4194304)
            //        {
            //            Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
            //            Messages11.Visible = true;
            //        }
            //        else
            //        {
            //            fileName = lblUID.Text + ext;
            //            FileUpload1.SaveAs(Server.MapPath("~\\stdImages\\" + fileName));
            //            ob.updateStdImage(fileName, lblUID.Text);
            //            Panel1.Visible = false;
            //            btnChange.Visible = true;
            //            Image1.ImageUrl = "/stdImages/" + fileName + "";
            //        }
            //    }
            //}
        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            //Panel1.Visible = false;
            //btnChange.Visible = true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["imgName"].ToString().Length > 0)
                {
                    Image1.ImageUrl = "/stdImages/" + Session["imgName"].ToString() + "";
                    Session.Remove("imgName");
                    if (Image1.ImageUrl.Contains("default.jpg"))
                    {
                        linkRemoveImage.Visible = false;
                    }
                    else { linkRemoveImage.Visible = true; }
                    string[] val = Session["UDetails"].ToString().Split('#');
                    lblMo.Text = val[1].ToString();
                    lblName.Text = val[0].ToString();
                    Session.Remove("UDetails");
                }
            }
            catch { }

            try
            {
                if (Session["Contact"].ToString().Length > 0)
                {
                    string[] val = Session["Contact"].ToString().Split('#');
                    lblAdd.Text = val[0].ToString();
                    lblEEmail.Text = val[1].ToString();
                    Session.Remove("Contact");
                }
            }
            catch { }

            try
            {
                if (Session["AbtYou"].ToString().Length > 0)
                {
                    lblAbtyou.Text = Session["AbtYou"].ToString();
                    Session.Remove("AbtYou");
                }
            }
            catch { }
        }

        protected void linkUpdatePer_Click(object sender, EventArgs e)
        {
            try
            {
                Session["editUserID"] = lblUID.Text;
                ModalPopupExtender2.Show();
            }
            catch { }


        }


        protected void linkeditInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Session["editUserID"] = lblUID.Text;
                ModalPopupExtender1.Show();
            }
            catch { }


        }

        protected void gridFinishedExam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gridFinishedExam_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "details")
            {
                row = Convert.ToInt32(e.CommandArgument);
                Session["examID"] = gridFinishedExam.Rows[row].Cells[0].Text;
                Session["userID"] = lblUID.Text;
                //Response.Redirect("exmResult.aspx");
                string script = "<script type=\"text/javascript\">showResult();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

            }
        }

        protected void gridUnFinished_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "details")
            {
                row = Convert.ToInt32(e.CommandArgument);
                Session["examID"] = gridUnFinished.Rows[row].Cells[0].Text;
                Session["userID"] = lblUID.Text;
                Response.Redirect("exmResult.aspx");
                //string script = "<script type=\"text/javascript\">showResult();</script>";
                ////Page page = HttpContext.Current.CurrentHandler as Page;
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

            }
            if (e.CommandName == "restart")
            {
                row = Convert.ToInt32(e.CommandArgument);
                Session["examID"] = gridUnFinished.Rows[row].Cells[0].Text;
                Session["userID"] = lblUID.Text;
                Session["resExam"] = "Yes";
                Session["resPreExam"] = "Yes";
                //  ob.insertUserExamAttempt(lblUID.Text, gridUnFinished.Rows[row].Cells[0].Text);
                //Response.Redirect("userExamstart.aspx");
                string script = "<script type=\"text/javascript\">openNewWindow();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }

        protected void gridUnFinished_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].Visible = false;
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Visible = false;
                }
            }
            catch { }
        }


        protected void linkEditTestimony_Click(object sender, EventArgs e)
        {
            LinkButton lBtn = (LinkButton)sender;
            string tID = lBtn.CommandArgument;



        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "editItem")
            {
                Label lbl1 = (Label)e.Item.FindControl("lblTID");
                lblTestimonyID.Text = lbl1.Text;
                Label lbl = (Label)e.Item.FindControl("lblTestimony");
                txtTestimony.Text = lbl.Text; ;
                btnSave.Text = "Update";
                Panel1.Visible = true;
                Repeater1.Visible = false;
                linkAddTetimony.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            if (txtTestimony.Text.Length > 0)
            {
                if (btnSave.Text == "Save")
                {
                    ob.insertTestimony(txtTestimony.Text, lblUID.Text);
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    SqlDataSource1.SelectCommand = "SELECT id,testimony from site_Testimony WHERE usreID= " + lblUID.Text + "";
                    Repeater1.DataBind();
                    if (Repeater1.Items.Count >= 3)
                    {
                        linkAddTetimony.Visible = false;
                    }
                    Panel1.Visible = false;

                    Repeater1.Visible = true;
                }
                if (btnSave.Text == "Update")
                {
                    ob.updateTestimony(txtTestimony.Text, lblTestimonyID.Text);
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    SqlDataSource1.SelectCommand = "SELECT id,testimony from site_Testimony WHERE usreID= " + lblUID.Text + "";
                    Repeater1.DataBind();
                    if (Repeater1.Items.Count >= 3)
                    {
                        linkAddTetimony.Visible = false;
                    }
                    Panel1.Visible = false;

                    Repeater1.Visible = true;
                }
            }
            else
            {
                lblMsg.Text = "Write proper text !";
                lblMsg.Visible = true;
            }
        }

        protected void linkAddTetimony_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Repeater1.Visible = false;
            linkAddTetimony.Visible = false;
            txtTestimony.Text = "";
            btnSave.Text = "Save";
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false; ;
            Repeater1.Visible = true;
            linkAddTetimony.Visible = true;
            txtTestimony.Text = "";

        }

        protected void linkAskToExpert_Click(object sender, EventArgs e)
        {
            rptExpert.Visible = false;
            Panel2.Visible = true;
            linkAskToExpert.Visible = false;
            txtExpertQue.Text = "";
        }

        protected void btnAskToExpert_Click(object sender, EventArgs e)
        {
            if (txtExpertQue.Text.Length > 0)
            {
                ob.askToExpert(txtExpertQue.Text, lblUID.Text);
                sourceExpert.SelectCommand = "select question,case when isnull(answer,0) =0 then 'Not Answered !' else answer end as answer from site_asktoexpert WHERE userid = " + lblUID.Text + "";
                sourceExpert.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                rptExpert.DataBind();
                Panel2.Visible = false;
                rptExpert.Visible = true;
                linkAskToExpert.Visible = true;
            }
            else
            {
                lblMsg1.Text = "Write proper question !";
                lblMsg1.Visible = true;

            }
        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            rptExpert.Visible = true;
            linkAskToExpert.Visible = true;
            txtExpertQue.Text = "";
        }

        protected void linkRemoveImage_Click(object sender, EventArgs e)
        {
            ob.RemoveImage("default.jpg", lblUID.Text);
            Image1.ImageUrl = "~/stdImages/default.jpg";
            linkRemoveImage.Visible = false;
        }



    }
}
