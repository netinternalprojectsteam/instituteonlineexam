﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace OnlineExam
{
    public partial class mthankYou : System.Web.UI.Page
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);
        public void openConnection()
        {
            try { cn.Open(); }
            catch { cn.Close(); cn.Open(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //IntegrationKit.libfuncs myUtility = new IntegrationKit.libfuncs();
                    //string WorkingKey, Order_Id, Merchant_Id, Amount, AuthDesc, Checksum, checksum, username, Tranzaction_type;
                    //WorkingKey = "qcmroh555ec91wskx2xbxwdpxouy9xof";
                    //Merchant_Id = Request.Form["Merchant_Id"];
                    //Order_Id = Request.Form["Order_Id"];
                    //Amount = Request.Form["Amount"];
                    //AuthDesc = Request.Form["AuthDesc"];
                    //checksum = Request.Form["Checksum"];
                    //Tranzaction_type = Request.Form["billing_cust_notes"];
                    //username = Request.Form["billing_cust_email"];

                    string Order_Id;
                    Order_Id = Session["order_id"].ToString();

                    if (Order_Id != null)
                    {
                        string uid = "";
                        openConnection();
                        SqlCommand cmd6 = new SqlCommand("Select userId from exm_paidExmUsers where transId=" + Order_Id.Replace("A", "") + "", cn);
                        uid = cmd6.ExecuteScalar().ToString();
                        cn.Close();

                        //  Checksum = myUtility.verifychecksum(Merchant_Id, Order_Id, Amount, AuthDesc, WorkingKey, checksum);

                        openConnection();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "Update exm_paidExmUsers set tStatus='Completed' where transId=" + Order_Id.Replace("A", "") + "";
                        cmd.Connection = cn;
                        int j = cmd.ExecuteNonQuery();
                        cn.Close();
                        if (j == 1)
                        {
                            lblMsg.Text = "Thank You!!! Your transaction is Successful!!! </br>To start Exam, Click on back button";

                            DataTable dt = new DataTable();
                            openConnection();
                            SqlCommand cmd1 = new SqlCommand();
                            cmd1.CommandText = "Select examID,exmAttempt from exm_paidExmUsers where transId=" + Order_Id.Replace("A", "") + "";
                            cmd1.Connection = cn;
                            SqlDataAdapter da = new SqlDataAdapter(cmd1);

                            da.Fill(dt);
                            cn.Close();

                            Session["resExam"] = "Yes";
                            string userID = uid;


                            DataTable dt1 = new DataTable();
                            openConnection();
                            SqlCommand cmd2 = new SqlCommand();
                            cmd2.CommandText = "SELECT examID, examName,isnull(section1Ques,'') +','+ isnull(section2Ques,'') +','+ isnull(section3Ques,'') +','+ isnull(section4Ques,'' ) +','+isnull(section5Ques,'' ) as questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, examType FROM exm_existingExams where examID = " + dt.Rows[0][0].ToString() + "";
                            cmd2.Connection = cn;
                            SqlDataAdapter da1 = new SqlDataAdapter(cmd2);

                            da1.Fill(dt1);
                            cn.Close();

                            ViewState["examDetails"] = dt1;
                            string maxExamID = "";
                            for (int k = 0; k < Convert.ToInt32(dt.Rows[0][1].ToString()); k++)
                            {
                                openConnection();
                                SqlCommand cmd3 = new SqlCommand("Select max(newexm)+1 from exm_newexam", cn);
                                maxExamID = cmd3.ExecuteScalar().ToString();
                                cn.Close();


                                openConnection();
                                SqlCommand cmd4 = new SqlCommand();
                                cmd4.CommandText = "INSERT INTO exm_newExam (newexm, noquestion, questionlist, userID, onDate, maxTime, courseID, category, noOfSections, preexamID) VALUES (@newexm, @noquestion, @questionlist, @userID, getdate(), @maxTime, @courseID ,'Pre Exam', @noOfSections, @preexamID)";
                                cmd4.Parameters.AddWithValue("newexm", maxExamID);
                                cmd4.Parameters.AddWithValue("noquestion", dt1.Rows[0]["noofQuestions"].ToString());
                                cmd4.Parameters.AddWithValue("questionlist", dt1.Rows[0]["questionNos"].ToString());
                                cmd4.Parameters.AddWithValue("userID", uid);
                                cmd4.Parameters.AddWithValue("maxTime", Convert.ToInt32(dt1.Rows[0]["maxTime"].ToString()));
                                cmd4.Parameters.AddWithValue("courseID", dt1.Rows[0]["courseID"].ToString());

                                cmd4.Parameters.AddWithValue("noOfSections", dt1.Rows[0]["noOfSections"].ToString());
                                cmd4.Parameters.AddWithValue("preexamID", dt.Rows[0][0].ToString());
                                cmd4.Connection = cn;
                                string i = cmd4.ExecuteNonQuery().ToString();

                                cn.Close();

                                string[] queNos = dt1.Rows[0]["questionNos"].ToString().Split(',');
                                int noOfQuestions = Convert.ToInt32(dt1.Rows[0]["noofQuestions"].ToString());
                                for (int l = 0; l < noOfQuestions; l++)
                                {
                                    if (queNos[l].Length > 0)
                                    {

                                        openConnection();
                                        SqlCommand cmd5 = new SqlCommand();
                                        cmd5.CommandText = "INSERT INTO exm_userAnswers (userid, exmid, questno, answer, remainingTime) VALUES  (@userid, @exmid, @questno, @answer, @remainingTime)";
                                        cmd5.Parameters.AddWithValue("userid", uid);
                                        cmd5.Parameters.AddWithValue("exmid", maxExamID);
                                        cmd5.Parameters.AddWithValue("questno", queNos[l].ToString());

                                        cmd5.Parameters.AddWithValue("answer", 0);
                                        cmd5.Parameters.AddWithValue("remainingTime", Convert.ToInt32(dt1.Rows[0]["maxTime"].ToString()));
                                        cmd5.Connection = cn;
                                        string m = cmd5.ExecuteNonQuery().ToString();
                                        cn.Close();

                                    }
                                }
                            }
                            Session["resPreExam"] = "No";
                            Session["examID"] = maxExamID;
                            Session["userID"] = uid;
                        }
                        else
                        {
                            lblMsg.Text = "Something went wrong!!! Transaction Not successful! Please try again!";
                        }
                    }
                    else
                    {
                        lblMsg.Text = "Something went wrong!!! Transaction Not successful! Please try again! not get ordid";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMsg.Text = "Something went wrong!!! Transaction Not successful! Please try again! tran failed " + ex.Message;
            }
        }
    }
}