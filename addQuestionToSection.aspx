﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="addQuestionToSection.aspx.cs"
    Inherits="OnlineExam.addQuestionToSection" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('linkAddRemoveQuesCancel').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

    <script type="text/javascript" language="javascript"> 
        function showAlert(value)
        {
       
            alert('Can not add. Maximum Questions reached !'+value);
        }
        
         function showAlert1()
        {
           
            alert('Question already added  to this Section ! ');
           
        }
        
           function showMsg()
        {
           
            alert('Question already added  to other Section ! ');
           
        }
        
        function queSelectionError()
        {
          alert('Select proper questions for adding in between ! ');
        }
        
        function checkChangesSaved()
        {
        var divID=document.getElementById('savedChanges');
        var x=divID.innerHTML;
        if(x==0)
        {
        alert('Click Finish to save unsaved changes Or click Cancel Changes !');
        }
        if(x==1)
        {
        cancel();
        }
        }
    </script>

    <link href="css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" CombineScripts="True">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                <asp:Label ID="lblHeading" runat="server" Text="Add Questions to Section" meta:resourcekey="lblHeadingResource1"></asp:Label>
            </div>
            <div class="TitlebarRight">
                <%-- <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White"> <asp:Button ID="Button2" runat="server" Text="X" Font-Bold="true" /></a></b>--%>
            </div>
        </div>
        <div id="savedChanges" runat="server" style="display: none">
            1</div>
        <asp:Panel ID="Panel1" runat="server" meta:resourcekey="Panel1Resource2">
            <asp:Label ID="lblerror" runat="server" Text="Label" Visible="False" meta:resourceKey="lblerrorResource1"></asp:Label>
            <asp:Label ID="lblMaxQuestion" runat="server" Text="50" Visible="False" meta:resourceKey="lblMaxQuestionResource1"></asp:Label>
            <asp:Label ID="lblExamID" runat="server" Text="Label" Visible="False" meta:resourceKey="lblExamIDResource1"></asp:Label>
            <asp:Label ID="lblSectionID" runat="server" Text="Label" Visible="False" meta:resourceKey="lblSectionIDResource1"></asp:Label><br />
            <asp:Label ID="lblQueAdded" runat="server" Visible="false" meta:resourceKey="lblQueAddedResource1"></asp:Label>
            <asp:Label ID="lblQueAddedToExam" runat="server" Visible="False" meta:resourceKey="lblQueAddedToExamResource1"></asp:Label>
            <asp:Label ID="lblInsId" runat="server" Visible="False"></asp:Label>
            <center>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblExamName" runat="server" Text="Label" Font-Bold="True" meta:resourceKey="lblExamNameResource1"></asp:Label>&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblMax" runat="server" Text="Label" Font-Bold="True" meta:resourceKey="lblMaxResource1"></asp:Label>&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="Label2" runat="server" Text="Added Questions :" Font-Bold="True" meta:resourceKey="Label2Resource1"></asp:Label>
                            <asp:Label ID="lblAlreadyAdd" runat="server" Text="Label" Font-Bold="True" meta:resourceKey="lblAlreadyAddResource1"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnFinish" runat="server" Text="Finish" CssClass="simplebtn" OnClick="btnFinish_Click"
                                meta:resourceKey="btnFinishResource1" />
                            <asp:Button ID="Button2" runat="server" Text="Close" CssClass="simplebtn" Font-Bold="True"
                                OnClientClick="checkChangesSaved()" />
                            <asp:Button ID="Button3" runat="server" Text="Cancel Changes" CssClass="simplebtn"
                                Font-Bold="True" OnClientClick="cancel();" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Repeater ID="repAddedQues" runat="server">
                                <HeaderTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <b>Added Questions</b>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="float: left; width: 40px">
                                        <asp:Button ID="Button1" runat="server" Text='<%# Eval("questid") %>' CommandArgument='<%# Eval("questid") %>'
                                            Width="35px" OnClick="btnRep1_Click" meta:resourceKey="Button1Resource1" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Panel ID="Panel2" runat="server" meta:resourceKey="Panel2Resource1">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label7" Font-Bold="True" runat="server" Text="Course" meta:resourceKey="Label7Resource1"></asp:Label>
                                            <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                                DataValueField="courseid" AutoPostBack="True" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged"
                                                meta:resourceKey="ddlCourseResource1">
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:Label ID="Label22" Font-Bold="True" runat="server" Text="Subject" meta:resourceKey="Label22Resource1"></asp:Label>
                                            <asp:DropDownList ID="ddlSubjects" runat="server" DataSourceID="sourceSubjects" DataTextField="subjectinfo"
                                                DataValueField="subid" meta:resourceKey="ddlSubjectsResource1">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnSearch_Click"
                                                meta:resourceKey="btnSearchResource1" />
                                            <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                SelectCommand="SELECT Distinct courseid, courseinfo FROM view_catCourseSub where instituteId=@instituteId and courseid is not null and subid is not null ORDER BY courseinfo">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:SqlDataSource ID="sourceSubjects" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>">
                                            </asp:SqlDataSource>
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btn" runat="server" Text="Insert Que" CssClass="simplebtn" CausesValidation="False"
                                                OnClick="btn_Click" meta:resourceKey="btnResource1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:Label ID="lblNoQue" Visible="False" ForeColor="Red" runat="server" Text="No Questions Found !"
                                                meta:resourceKey="lblNoQueResource1"></asp:Label>
                                            <asp:Repeater ID="repQueNos" runat="server">
                                                <ItemTemplate>
                                                    <div style="float: left; width: 40px">
                                                        <asp:Button ID="Button1" runat="server" Text='<%# Eval("questid") %>' CommandArgument='<%# Eval("questid") %>'
                                                            Width="35px" OnClick="btnRep_Click" meta:resourceKey="Button1Resource2" />&nbsp;
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <table width="100%">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblQuestionNumber" runat="server" Text="Label" Visible="False" meta:resourceKey="lblQuestionNumberResource1"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="border: solid 1px black">
                                        <td colspan="2">
                                            <div style="border: solid 2px black" runat="server" id="queHolder" visible="False">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <div style="max-width: 400px; position: relative">
                                                                <br />
                                                                <br />
                                                                <br />
                                                                <asp:Panel ID="Panel3" runat="server" Visible="False" Height="400px" ScrollBars="Auto"
                                                                    meta:resourceKey="Panel3Resource1">
                                                                    <asp:Repeater ID="repQueInfo" runat="server">
                                                                        <ItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("Info") %>' meta:resourceKey="Label4Resource1"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </asp:Panel>
                                                            </div>
                                                        </td>
                                                        <td align="left">
                                                            <div style="width: 99%; min-width: 400px; float: left; vertical-align: top">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:Repeater ID="Repeater1" runat="server">
                                                                                <ItemTemplate>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <asp:Panel ID="Panel1" runat="server" meta:resourceKey="Panel1Resource1">
                                                                                                    <table width="100%">
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left" style="width: 100px">
                                                                                                                <asp:Button ID="Button1" runat="server" Text='<%# Eval("btntext") %>' CommandArgument='<%# Eval("questid") %>'
                                                                                                                    CssClass="simplebtn" OnClick="btnAdd_Click" meta:resourceKey="Button1Resource3" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" style="width: 100px" align="right">
                                                                                                                <table>
                                                                                                                    <tr>
                                                                                                                        <td align="left">
                                                                                                                            <asp:Label ID="Label17" runat="server" Text="Correct Marks :" meta:resourceKey="Label17Resource1"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:Label ID="lblMarks" runat="server" Text='<%# Eval("mark") %>' meta:resourceKey="lblMarksResource1"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td align="left">
                                                                                                                            <asp:Label ID="Label18" runat="server" Text="Incorrect Deduction :" meta:resourceKey="Label18Resource1"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td align="left">
                                                                                                                            <asp:Label ID="lblDeduction" runat="server" Text='<%# Eval("negativeDeduction") %>'
                                                                                                                                meta:resourceKey="lblDeductionResource1"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" style="width: 100px" align="left">
                                                                                                                <asp:Label ID="Label10" runat="server" Text="Question" CssClass="myLabelHead" meta:resourceKey="Label10Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="lblQuestion" runat="server" CssClass="myLabel" Text='<%# Eval("Question") %>'
                                                                                                                    meta:resourceKey="lblQuestionResource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label11" runat="server" Text="Option 1" CssClass="myLabelHead" meta:resourceKey="Label11Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="lblYourAns" runat="server" Text='<%# Eval("option1") %>' CssClass="myLabel"
                                                                                                                    meta:resourceKey="lblYourAnsResource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label12" runat="server" Text="Option 2" CssClass="myLabelHead" meta:resourceKey="Label12Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="lblCorrect" runat="server" CssClass="myLabel" Text='<%# Eval("option2") %>'
                                                                                                                    meta:resourceKey="lblCorrectResource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label13" runat="server" Text="Option 3" CssClass="myLabelHead" meta:resourceKey="Label13Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("option3") %>' CssClass="myLabel"
                                                                                                                    meta:resourceKey="Label1Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label5" runat="server" Text="Option 4" CssClass="myLabelHead" meta:resourceKey="Label5Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("option4") %>' CssClass="myLabel"
                                                                                                                    meta:resourceKey="Label6Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label14" runat="server" Text="Option 5" CssClass="myLabelHead" meta:resourceKey="Label14Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" align="left">
                                                                                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("option5") %>' CssClass="myLabel"
                                                                                                                    meta:resourceKey="Label15Resource1"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:Panel>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel4" runat="server" Visible="False" meta:resourceKey="Panel4Resource1">
                                <table>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblInsertQueError" Visible="False" runat="server" Text="Label" meta:resourceKey="lblInsertQueErrorResource1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="Label3" runat="server" Text="Unique ID" meta:resourceKey="Label3Resource1"></asp:Label>
                                            <asp:TextBox ID="txtUniqueID" runat="server" meta:resourceKey="txtUniqueIDResource1"></asp:TextBox>&nbsp;&nbsp;
                                            <asp:Label ID="Label8" runat="server" Text="In Between" meta:resourceKey="Label8Resource1"></asp:Label>&nbsp;
                                            <asp:DropDownList ID="ddlFrom" runat="server" meta:resourceKey="ddlFromResource1">
                                                <asp:ListItem meta:resourceKey="ListItemResource1">0</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:Label ID="Label9" runat="server" Text="and" meta:resourceKey="Label9Resource1"></asp:Label>&nbsp;
                                            <asp:DropDownList ID="ddlTo" runat="server" meta:resourceKey="ddlToResource1">
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;<asp:Button ID="btnInsert" runat="server" Text="Add" CssClass="simplebtn"
                                                OnClick="btnInsert_Click" meta:resourceKey="btnInsertResource1" />
                                            <asp:Button ID="btnCan" runat="server" Text="Cancel" CausesValidation="False" CssClass="simplebtn"
                                                OnClick="btnCan_Click" meta:resourceKey="btnCanResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
