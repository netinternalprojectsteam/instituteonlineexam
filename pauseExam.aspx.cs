﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class pauseExam : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRestart_Click(object sender, EventArgs e)
        {
            Session["restartExam"] = "Yes";
            string script = "<script type=\"text/javascript\">callfun();</script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void imgRestart_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}
