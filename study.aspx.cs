﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class study : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string qID = Request.QueryString["studyFor"];
                if (qID == "comp")
                {
                    comp.Visible = true;
                }
                if (qID == "english")
                {
                    english.Visible = true;
                }
                if (qID == "reasoning")
                {
                    reasoning.Visible = true;
                }
                if (qID == "aptitude")
                {
                    aptitude.Visible = true;
                }
                if (qID == "marketing")
                {
                    marketing.Visible = true;
                }


            }
        }
    }
}
