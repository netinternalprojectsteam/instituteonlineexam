﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="examDirections.aspx.cs"
    Inherits="OnlineExam.examDirections" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Directions</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnMasterCanAdmin').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Directions for Exam
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">
                    <center>
                        <table width="100%">
                            <tr>
                                <td align="left" style="background-color: White">
                                    <asp:Label ID="lblExamID" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lbluserID" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblInstructions" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnStart" runat="server" Text="Start" CssClass="simplebtn" OnClick="btnStart_Click" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
