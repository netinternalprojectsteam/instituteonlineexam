﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class Login : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                if (!Page.IsValid)
                {
                    return;
                }

                DataTable dTab = new DataTable();
                dTab = obj.GetUsernameAndPassword(UserName.Text, Password.Text);
                if (dTab.Rows.Count > 0)
                {
                    string password = dTab.Rows[0][1].ToString();
                    bool validUsername = (string.Compare(UserName.Text, dTab.Rows[0][0].ToString(), false) == 0);
                    bool validPassword = (string.Compare(Password.Text, password, false) == 0);
                    if (validUsername && validPassword)
                    {
                        FormsAuthentication.RedirectFromLoginPage(UserName.Text, RememberMe.Checked);
                    }
                    else
                    {
                        //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                        //Messages11.Visible = true;
                        //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                        //lblMsg.Visible = true;
                    }
                }
                else
                {
                    //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                    //Messages11.Visible = true;
                    //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                    //lblMsg.Visible = true;
                }
            }
            catch (FormatException)
            {
                //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                //Messages11.Visible = true;
                //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                //lblMsg.Visible = true;
            }
            catch (Exception)
            {
                //Messages11.setMessage(0, ex.Message);
                //Messages11.Visible = true;
                //lblMsg.Text = ex.Message;
                //lblMsg.Visible = true;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();
                if (!Page.IsValid)
                {
                    return;
                }

                DataTable dTab = new DataTable();
                dTab = obj.GetUsernameAndPassword(UserName.Text, Password.Text);
                if (dTab.Rows.Count > 0)
                {
                    string password = dTab.Rows[0][1].ToString();
                    bool validUsername = (string.Compare(UserName.Text, dTab.Rows[0][0].ToString(), false) == 0);
                    bool validPassword = (string.Compare(Password.Text, password, false) == 0);
                    if (validUsername && validPassword)
                    {
                        FormsAuthentication.RedirectFromLoginPage(UserName.Text, RememberMe.Checked);
                    }
                    else
                    {
                        //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                        //Messages11.Visible = true;
                        //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                        //lblMsg.Visible = true;
                    }
                }
                else
                {
                    //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                    //Messages11.Visible = true;
                    //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                    //lblMsg.Visible = true;
                }
            }
            catch (FormatException)
            {
                //Messages11.setMessage(0, "Your UserName Or Password is invalid. Please try again.");
                //Messages11.Visible = true;
                //lblMsg.Text = "Your UserName Or Password is invalid. Please try again.";
                //lblMsg.Visible = true;
            }
            catch (Exception)
            {
                //Messages11.setMessage(0, ex.Message);
                //Messages11.Visible = true;
                //lblMsg.Text = ex.Message;
                //lblMsg.Visible = true;
            }
        }
    }
}
