﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class examStartPage : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInst.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInst.Text = val[3];
                }
                sourceExams.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceExams.SelectCommand = "Select distinct examID, examName from view_examDetails where instituteId=" + lblInst.Text + "";
                ddlExams.DataBind();
            }
        }


        //protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    lblMessage.Visible = false;


        //    sourceCourseOrPreExam.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;

        //    //if (ddlCategory.Text == "Pre Exam")
        //    //{
        //    lblCategory.Text = "Pre Exam";
        //    sourceCourseOrPreExam.SelectCommand = "Select distinct courseID As ID, examName as EName from exm_existingExams";
        //    ddlCouresPreExam.DataBind();
        //    ddlCouresPreExam.Enabled = true;

        //    //}
        //    //if (ddlCategory.Text == "Course")
        //    //{
        //    //    lblCategory.Text = "Course";
        //    //    sourceCourseOrPreExam.SelectCommand = "Select distinct courseid As ID, courseInfo as EName from exm_course";
        //    //    ddlCouresPreExam.DataBind();
        //    //    ddlCouresPreExam.Enabled = true;
        //    //}
        //    DataTable dt = new DataTable();
        //    dt = ob.getData("SELECT id, coursePreExamID, category, description FROM exam_startPages where coursePreExamID = " + ddlExam.SelectedValue + "");
        //    if (dt.Rows.Count > 0)
        //    {
        //        lblID.Text = dt.Rows[0]["id"].ToString();
        //        btnSave.Text = "Update";
        //        CKEditorControl1.Text = dt.Rows[0]["description"].ToString();
        //    }
        //    else
        //    {
        //        btnSave.Text = "Save";
        //        CKEditorControl1.Text = "";
        //    }


        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Visible = false;
                if (FCKeditor1.Value.Length > 0)
                {
                    if (btnSave.Text == "Save")
                    {
                        ob.saveStartInfo(ddlExams.SelectedValue, FCKeditor1.Value);
                        FCKeditor1.Value = "";
                        lblMessage.Visible = true;
                        lblMessage.Text = "Information Saved Successfully !";
                        lblMessage.ForeColor = System.Drawing.Color.Green; ;
                    }
                    if (btnSave.Text == "Update")
                    {
                        ob.updateStartInfo(lblID.Text, FCKeditor1.Value);
                        lblMessage.Visible = true;
                        lblMessage.Text = "Information Updated Successfully !";
                        lblMessage.ForeColor = System.Drawing.Color.Green; ;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("duplicate"))
                {
                    lblMessage.Text = "Information already exists for exam!";
                }
            }
        }



        protected void ddlExam_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT id, coursePreExamID, description FROM exam_startPages where coursePreExamID = " + ddlExams.SelectedValue + " ");
            if (dt.Rows.Count > 0)
            {
                lblID.Text = dt.Rows[0]["id"].ToString();
                btnSave.Text = "Update";
                FCKeditor1.Value = dt.Rows[0]["description"].ToString();

            }
            else
            {
                btnSave.Text = "Save";
                FCKeditor1.Value = "";
            }
        }

        protected void ddlExams_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT id, coursePreExamID, description FROM exam_startPages where coursePreExamID = " + ddlExams.SelectedValue + " ");
            if (dt.Rows.Count > 0)
            {
                lblID.Text = dt.Rows[0]["id"].ToString();
                btnSave.Text = "Update";
                FCKeditor1.Value = dt.Rows[0]["description"].ToString();
            }
            else
            {
                btnSave.Text = "Save";
                FCKeditor1.Value = "";
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT id, coursePreExamID, description FROM exam_startPages where coursePreExamID = " + ddlSelectAs.SelectedValue + " ");
            if (dt.Rows.Count > 0)
            {
                lblID.Text = ddlExams.SelectedValue;
                btnSave.Text = "Update";
                FCKeditor1.Value = dt.Rows[0]["description"].ToString();
                try
                {
                    ob.saveStartInfo(ddlExams.SelectedValue, dt.Rows[0]["description"].ToString());
                }
                catch
                {
                    ob.updateStartInfo(dt.Rows[0]["id"].ToString(), dt.Rows[0]["description"].ToString());
                }
                lblMessage.Visible = true;
                lblMessage.Text = "Exam Description set !";
            }
            else
            {
                btnSave.Text = "Save";
                FCKeditor1.Value = "";
            }

        }
    }
}
