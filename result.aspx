﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="result.aspx.cs" Inherits="OnlineExam.result" Title="Exam Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Exam Result</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Select Exam Name :
                        <asp:DropDownList ID="ddlExams" runat="server" AutoPostBack="True" DataSourceID="sourceExams"
                            DataTextField="examName" DataValueField="examID" OnSelectedIndexChanged="ddlExams_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Button ID="btnToPDF" runat="server" OnClick="btnToPDF_Click" Text="Export to PDF"
                            Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" Text="No Details Found" Font-Bold="true" ForeColor="Red"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblExamName" runat="server" Text="Exam Name : " Visible="false"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblDate" runat="server" Text="Label" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblTotalQue" runat="server" Text="Total Questions : " Visible="false"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblIsNegative" runat="server" Text="Negative Marking : " Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <div id="dvText" runat="server">
                            <asp:Panel ID="Panel1" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="gridDetails" runat="server" AutoGenerateColumns="false" CssClass="gridview">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr.No.">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                                    <asp:BoundField DataField="onDate" HeaderText="On Date" SortExpression="onDate" />
                                                    <asp:BoundField DataField="solvedQue" HeaderText="Solved Que" SortExpression="solvedQue" />
                                                    <asp:BoundField DataField="correctQue" HeaderText="Correct Que" SortExpression="correctQue" />
                                                    <asp:BoundField DataField="totalMarks" HeaderText="Marks" SortExpression="totalMarks" />
                                                    <asp:BoundField DataField="percentile" HeaderText="Percentile" SortExpression="percentile" />
                                                    <asp:BoundField DataField="accuracy" HeaderText="Accuracy" SortExpression="accuracy" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
            </table>
            <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                SelectCommand="SELECT DISTINCT [examID], [examName] FROM [view_examDetails] WHERE ([isAvailable] = @isAvailable) and instituteId=@instituteId">
                <SelectParameters>
                    <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                    <asp:Parameter DefaultValue="true" Name="isAvailable" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sourceDetails" runat="server"></asp:SqlDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnToPDF" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
