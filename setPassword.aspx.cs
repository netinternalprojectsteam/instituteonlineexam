﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class setPassword : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string qString = Request.QueryString["regID"].ToString();
                    string[] qPara = qString.Split('@');
                    string linkID = qPara[0].ToString();
                    string ID = qPara[1].ToString();
                    DataTable dt = new DataTable();
                    dt = ob.getUserDetailsByLinkID(linkID,ID);
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "False")
                        {
                            txtEmail.Text = dt.Rows[0]["uemail"].ToString();
                            lblID.Text = dt.Rows[0]["uid"].ToString();
                            Panel1.Visible = true;
                        }
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Messages11.Visible = true;
                            Messages11.setMessage(0, "This registration link is already used !");
                            Panel1.Visible = false;
                        }
                    }
                    else
                    {
                        Messages11.Visible = true;
                        Messages11.setMessage(0, "No details found !");
                        Panel1.Visible = false;
                    }
                }
                catch { }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (txtPass.Text == txtRePass.Text)
            {
                ob.updateUserRegistration(lblID.Text, base64Encode(txtPass.Text));
                Messages11.Visible = true;
                Messages11.setMessage(1, "Registration Successful ! To Login <a href='/LoginDetails.aspx'>Click Here</a>");
                txtEmail.Text = "";
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Password & Retype Password not match !");
            }
        }
        private string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }
    }
}
