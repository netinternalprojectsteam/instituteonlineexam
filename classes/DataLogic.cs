﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Net.Mail;

namespace OnlineExam.classes
{
    public class DataLogic
    {
        CodeStore.CodeStore obj = new CodeStore.CodeStore(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);

        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);

        #region Get Username and Password
        /// <summary>
        /// Get Username and Password
        /// </summary>
        /// <param name="username">string username</param>
        /// <param name="password">string password</param>
        /// <returns>DataTable</returns>
        public DataTable GetUsernameAndPassword(string username, string password)
        {
            string strSql = "SELECT userid, username, password, adminType, instituteId FROM admin_user WHERE (username = @username) AND (password = @password)";
            SqlParameter[] para ={
                                     new SqlParameter("username",username),
                                     new SqlParameter("password",password)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        #endregion

        public int AddNewCategory(string category, string instituteId)
        {
            string strSql = "INSERT INTO exm_category (category, instituteId) VALUES (@category, @instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("category",category),
                                     new SqlParameter("instituteId",instituteId)
                                };
            return obj.Insert(strSql, para);
        }

        public int UpdateCategory(string category, string catid, string instituteId)
        {
            string strSql = "UPDATE exm_category SET category = @category WHERE (catid = @catid and instituteId=@instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("category",category),
                                     new SqlParameter("catid",catid),
                                     new SqlParameter("instituteId",instituteId)
                                };
            return obj.Update(strSql, para);
        }

        public int DeleteCategory(string catid, string instituteId)
        {
            string strSql = "DELETE FROM exm_category WHERE (catid = @catid and instituteId=@instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("catid",catid),
                                     new SqlParameter("instituteId",instituteId)
                                };
            return obj.Delete(strSql, para);
        }

        public int AddNewCourse(string catid, string courseinfo)
        {
            string strSql = "INSERT INTO exm_course (catid, courseinfo) VALUES (@catid,@courseinfo)";
            SqlParameter[] para ={
                                     new SqlParameter("catid",catid),
                                     new SqlParameter("courseinfo",courseinfo)
                                 };
            return obj.Insert(strSql, para);
        }

        public int UpdateCourseInfo(string catid, string courseinfo, string courseid)
        {
            string strSql = "UPDATE exm_course SET catid = @catid, courseinfo = @courseinfo WHERE (courseid = @courseid)";
            SqlParameter[] para ={
                                     new SqlParameter("catid",catid),
                                     new SqlParameter("courseinfo",courseinfo),
                                     new SqlParameter("courseid",courseid)
                                };
            return obj.Update(strSql, para);
        }

        public int DeleteCourseInfo(string courseid)
        {
            string strSql = "DELETE FROM exm_course WHERE (courseid = @courseid)";
            SqlParameter[] para ={
                                     new SqlParameter("courseid",courseid)
                                };
            return obj.Delete(strSql, para);
        }

        public DataTable GetCourseInfoByID(string courseid)
        {
            string strSql = "SELECT catid, courseinfo FROM exm_course WHERE (courseid = @courseid)";
            SqlParameter[] para ={
                                     new SqlParameter("courseid",courseid)
                                };
            return obj.SelectMultiple(strSql, para);
        }


        public DataTable getUsersbyinstitut(string instituteId)
        {
            string strSql = "SELECT uname, uemail, mobileNo, city, uid, instituteId,GCMId FROM exam_users WHERE (instituteId = @instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("instituteId",instituteId)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getUsersbyid(string uid)
        {
            string strSql = "SELECT uname, uemail, mobileNo, city, uid, instituteId,GCMId FROM exam_users WHERE (uid = @uid)";
            SqlParameter[] para ={
                                     new SqlParameter("uid",uid)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public int AddNewSubjectInfo(string courseid, string subjectinfo)
        {
            string strSql = "INSERT INTO exm_subject (courseid, subjectinfo) VALUES (@courseid, @subjectinfo)";
            SqlParameter[] para ={
                                     new SqlParameter("courseid",courseid),
                                     new SqlParameter("subjectinfo",subjectinfo)
                                };
            return obj.Insert(strSql, para);
        }

        public int UpdateSubjectInfo(string courseid, string subjectinfo, string subid)
        {
            string strSql = "UPDATE exm_subject SET courseid = @courseid, subjectinfo = @subjectinfo WHERE (subid = @subid)";
            SqlParameter[] para ={
                                     new SqlParameter("courseid",courseid),
                                     new SqlParameter("subjectinfo",subjectinfo),
                                     new SqlParameter("subid",subid)
                                };
            return obj.Update(strSql, para);
        }

        public int DeleteSubjectInfo(string subid)
        {
            string strSql = "DELETE FROM exm_subject WHERE (subid = @subid)";
            SqlParameter[] para ={
                                     new SqlParameter("subid",subid)
                                };
            return obj.Delete(strSql, para);
        }

        public DataTable GetSubjectInfoByID(string subid)
        {
            string strSql = "SELECT courseid, subjectinfo FROM exm_subject WHERE (subid = @subid)";
            SqlParameter[] para ={
                                     new SqlParameter("subid",subid)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getQueByInstitute(string que, string institute)
        {
            string strSql = "Select * from view_examQuestions where question='" + que + "' and instituteId=" + institute + "";
            SqlParameter[] para = { 
                                    new SqlParameter("que",que),
                                    new SqlParameter("institute",institute)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getQueByInsForUpdate(string que, string institute, string qid)
        {
            string strSql = "Select * from view_examQuestions where question='" + que + "' and instituteId=" + institute + " and questid!=" + qid + "";
            SqlParameter[] para = { 
                                    new SqlParameter("que",que),
                                    new SqlParameter("institute",institute),
                                    new SqlParameter("qid",qid)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public int AddNewQuestion(string question, string option1, string option2, string option3, string option4, string option5, string answer, string subid, string difflevel, string userid, string infoID, double mark, double negativeDeduction, string instituteId)
        {
            try
            {
                string strSql = "INSERT INTO exm_questions (question, option1, option2, option3, option4,option5, answer, subid, difflevel, userid, infoID, mark, negativeDeduction, instituteId) VALUES (@question,@option1,@option2,@option3,@option4,@option5,@answer,@subid,@difflevel,@userid, @infoID, @mark, @negativeDeduction, @instituteId)";
                SqlParameter[] para ={
                                    new SqlParameter("question",question),
                                    new SqlParameter("option1",option1),
                                    new SqlParameter("option2",option2),
                                    new SqlParameter("option3",option3),
                                    new SqlParameter("option4",option4),
                                    new SqlParameter("option5",option5),
                                    new SqlParameter("answer",answer),
                                    new SqlParameter("subid",subid),
                                    new SqlParameter("difflevel",difflevel),
                                    new SqlParameter("userid",userid),
                                    new SqlParameter("infoID",infoID),
                                    new SqlParameter("mark",mark),
                                    new SqlParameter("negativeDeduction",negativeDeduction),
                                    new SqlParameter("instituteId",instituteId)
                                };
                //return obj.Insert(strSql, para);
                obj.Insert(strSql, para);
                string strsql = "SELECT max(questid) from exm_questions";
                return Convert.ToInt32(obj.SelectScalar(strsql));
            }
            catch { return 0; }
        }

        public int UpdateQuestion(string question, string option1, string option2, string option3, string option4, string option5, string answer, string subid, string difflevel, string userid, string qid, string infoID, double mark, double negativeDeduction)
        {
            string strSql = "UPDATE exm_questions SET question = @question, option1 = @option1, option2 = @option2, option3 = @option3, option4 = @option4, option5 = @option5, answer = @answer, subid = @subid, difflevel = @difflevel, userid = @userid, negativeDeduction = @negativeDeduction,mark = @mark, infoID = @infoID WHERE (questid = @qid)";
            SqlParameter[] para ={
                                    new SqlParameter("question",question),
                                    new SqlParameter("option1",option1),
                                    new SqlParameter("option2",option2),
                                    new SqlParameter("option3",option3),
                                    new SqlParameter("option4",option4),
                                     new SqlParameter("option5",option5),
                                    new SqlParameter("answer",answer),
                                    new SqlParameter("subid",subid),
                                    new SqlParameter("difflevel",difflevel),
                                    new SqlParameter("userid",userid),
                                    new SqlParameter("qid",qid),
                                     new SqlParameter("infoID",infoID),
                                      new SqlParameter("mark",mark),
                                      new SqlParameter("negativeDeduction",negativeDeduction)
                                };
            return obj.Update(strSql, para);
        }

        public DataTable GetQuestionByID(string qid)
        {
            string strSql = "SELECT question, option1, option2, option3, option4, answer, subid, difflevel, userid FROM exm_questions WHERE (questid = @qid)";
            SqlParameter[] para ={
                                     new SqlParameter("qid",qid)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public int DeleteQuestion(string qid)
        {
            string strSql = "DELETE FROM exm_questions WHERE (questid = @qid)";
            SqlParameter[] para ={
                                     new SqlParameter("qid",qid)
                                 };
            return obj.Delete(strSql, para);
        }

        public int CreateNewUserAccount(string uname, string uemail, string password, string mobileNo)
        {
            string strSql = "INSERT INTO exam_users (uname, uemail, password, mobileNo) VALUES (@uname,@uemail,@password, @mobileNo)";
            SqlParameter[] para ={
                                     new SqlParameter("mobileNo",mobileNo),
                                     new SqlParameter("uname",uname),
                                     new SqlParameter("uemail",uemail),
                                     new SqlParameter("password",password),
                                };
            return obj.Insert(strSql, para);
        }

        public DataTable GetUserLoginInfo(string uemail)
        {
            string strSq = "SELECT uid, uemail FROM exam_users WHERE uemail=@uemail and IsActive='True'";
            SqlParameter[] para ={
                                     new SqlParameter("uemail",uemail)
                                };
            return obj.SelectMultiple(strSq, para);
        }

        public DataTable GetUserLoginInfo(string uemail, string password)
        {
            string strSq = "SELECT uid, uemail FROM exam_users WHERE uemail=@uemail AND password=@password";
            SqlParameter[] para ={
                                     new SqlParameter("uemail",uemail),
                                     new SqlParameter("password",password)
                                };
            return obj.SelectMultiple(strSq, para);
        }

        public DataTable GetActiveUserLoginInfo(string uemail, string password)
        {
            //string strSq = "SELECT uid, uemail FROM view_ActiveSessionOnlineUsers WHERE uemail=@uemail AND password=@password";
            //string strSq = "SELECT uid, uemail, password, IsActive, instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail AND password=@password AND IsActive = 'True'";
            string strSq = "SELECT uid, uemail, password, IsActive, instituteId, institute as instituteName From view_userByInstitute where uemail=@uemail and password=@password and isActive='True'";
            SqlParameter[] para ={
                                     new SqlParameter("uemail",uemail),
                                     new SqlParameter("password",password)
                                };
            return obj.SelectMultiple(strSq, para);
        }

        public DataTable GetUserInfoByMail(string uemail, string instituteId)
        {
            string strSq = "SELECT uid, uemail, password, IsActive,linkID, uname FROM exam_users WHERE uemail=@uemail and instituteId=@instituteId";
            SqlParameter[] para ={
                                     new SqlParameter("uemail",uemail),
                                     new SqlParameter("instituteId",instituteId)
                                };
            return obj.SelectMultiple(strSq, para);
        }

        public DataTable GetUserInfoByID(string uid)
        {
            //string strSq = "SELECT uid, uemail, uname, mobileno,imageName FROM exam_users WHERE uid = @uid";
            string strSq = "SELECT  uid,  uemail, uname, mobileNo,  imageName, password, city, edate, country, state, extraEmail, Add1, Add2, aboutU, exam_Institute.instituteId, institute FROM exam_users INNER JOIN exam_Institute on exam_users.InstituteId=exam_Institute.InstituteId WHERE uid = @uid";
            //string strSq = "SELECT  Tid, amount, className, transactionDate, noofMCQs, noofExam, fees, CourseDuration, Details,uid, uemail, uname, mobileno,imageName,city ,state,country,extraEmail,Add1,Add2,aboutU,courseID FROM view_ActiveSessionOnlineUsers WHERE uid = @uid";
            SqlParameter[] para ={
                                     new SqlParameter("uid",uid) };
            return obj.SelectMultiple(strSq, para);
        }

        public DataTable GetUserExamQuestionID(int qno)
        {
            string strSql = "SELECT TOP (@qno) questid FROM exm_questions ORDER BY NEWID()";
            SqlParameter[] para ={
                                     new SqlParameter("qno",qno)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public int InsertUserQuestionList(string newexm, string noquestion, string questionlist, string userID, int maxTime, string courseID, string category, string noOfSections, string preexamID)
        {
            string strSql = "INSERT INTO exm_newExam (newexm, noquestion, questionlist, userID, onDate, maxTime, courseID, category, noOfSections, preexamID) VALUES (@newexm, @noquestion, @questionlist, @userID, getdate(), @maxTime, @courseID ,@category, @noOfSections, @preexamID)";
            SqlParameter[] para ={
                                     new SqlParameter("newexm",newexm),
                                     new SqlParameter("noquestion",noquestion),
                                     new SqlParameter("questionlist",questionlist),
                                      new SqlParameter("userID",userID),
                                       new SqlParameter("maxTime",maxTime),
                                        new SqlParameter("courseID",courseID),
                                          new SqlParameter("category",category),
                                              new SqlParameter("noOfSections",noOfSections),
                                               new SqlParameter("preexamID",preexamID)
                                };
            return obj.Insert(strSql, para);
        }

        public DataTable GetUserQuestions(string questid)
        {
            string strSql = "SELECT questid, question, option1, option2, option3, option4, answer FROM exm_questions WHERE (questid = @questid)";
            SqlParameter[] para ={
                                     new SqlParameter("questid",questid)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable GetAllQuestion()
        {
            string strSql = "SELECT questid, question, option1, option2, option3, option4, answer FROM exm_questions";

            return obj.SelectMultiple(strSql);
        }

        public DataTable getData(string query)
        {
            return obj.SelectMultiple(query);
        }

        public string getValue(string query)
        {
            try
            {
                return obj.SelectScalar(query).ToString();
            }
            catch { return "0"; }
        }

        public int insertQueInfo(string questid, string questionInfo)
        {
            string strSql = "insert into exm_questioninfo (questid, questionInfo) values(@questid, @questionInfo)";
            SqlParameter[] para ={
                                     new SqlParameter("questid",questid),
                                     new SqlParameter("questionInfo",questionInfo),
                                       };
            return obj.Insert(strSql, para);
        }

        public int updateQueInfo(string questid, string questionInfo)
        {
            string strSql = "Update exm_questioninfo set questionInfo = @questionInfo where  questid = @questid";
            SqlParameter[] para ={
                                     new SqlParameter("questid",questid),
                                     new SqlParameter("questionInfo",questionInfo),
                                       };
            return obj.Update(strSql, para);
        }

        public int insertUseAnswer(string userid, string exmid, string questno, string answer, int remainingTime)
        {
            string strSql = "INSERT INTO exm_userAnswers (userid, exmid, questno, answer, remainingTime) VALUES  (@userid, @exmid, @questno, @answer, @remainingTime)";
            SqlParameter[] para ={
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("exmid",exmid),
                                      new SqlParameter("questno",questno),
                                     new SqlParameter("answer",answer),
                                      new SqlParameter("remainingTime",remainingTime)
                                       };
            return obj.Insert(strSql, para);
        }

        public void updateTimespentAndUserAnswer(string answer, string remainingTime, int timeSpent, string userID, string examid, string questno)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);
            SqlCommand command = new SqlCommand("updateData", connection);

            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@answer", answer);
            command.Parameters.AddWithValue("@remainingTime", remainingTime);
            command.Parameters.AddWithValue("@timeSpent", timeSpent);
            command.Parameters.AddWithValue("@userID", userID);
            command.Parameters.AddWithValue("@examid", examid);
            command.Parameters.AddWithValue("@questno", questno);

            //command.Parameters[0].Value = strXML;
            connection.Open();
            int recordsInserted = command.ExecuteNonQuery();
            connection.Close();
            // return recordsInserted;
        }

        public int updateUserAnswer(string id, string answer, int remainingTime)
        {
            string strSql = "Update exm_userAnswers set answer = @answer , remainingTime = @remainingTime where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("remainingTime",remainingTime)
                                       };
            return obj.Update(strSql, para);
        }

        public int updateTimeRem(string id, string remainingTime)
        {
            string strSql = "Update exm_userAnswers set remainingTime = @remainingTime where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                     new SqlParameter("remainingTime",remainingTime)
                                       };
            return obj.Update(strSql, para);
        }

        public int setTimeSpent(string timeSpent, string userid, string exmid, string questno)
        {
            string strSql = "UPDATE exm_userAnswers SET timeSpent = @timespent where userid = @userid and exmid = @exmid and questno = @questno";
            SqlParameter[] para ={
                                     new SqlParameter("timeSpent",timeSpent),
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("exmid",exmid),
                                     new SqlParameter("questno",questno),                                           
                               };
            return obj.Update(strSql, para);

        }

        public int updateExamStatus(string newexm)
        {
            string strSql = "UPDATE exm_newExam SET  isFinish = '1' WHERE (newexm = @newexm)";
            SqlParameter[] para ={
                                     new SqlParameter("newexm",newexm)};
            return obj.Update(strSql, para);
        }

        public int updateStdImage(string imageName, string uid)
        {
            string strSql = "UPDATE exam_users SET  imageName = @imageName WHERE (uid = @uid)";
            SqlParameter[] para ={
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("uid",uid)
                                 };
            return obj.Update(strSql, para);
        }

        public int updatePersonalInfo(string uname, string mobileNo, string imageName, string uid)
        {
            string strSql = "UPDATE exam_users SET uname = @uname, mobileNo = @mobileNo, imageName = @imageName where uid = @uid";
            SqlParameter[] para ={
                                     new SqlParameter("uname",uname),
                                     new SqlParameter("mobileNo",mobileNo),
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("uid",uid)
                                 };
            return obj.Update(strSql, para);
        }

        public int RemoveImage(string imageName, string uid)
        {
            string strSql = "UPDATE exam_users SET imageName = @imageName where uid = @uid";
            SqlParameter[] para ={
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("uid",uid)
                                 };
            return obj.Update(strSql, para);
        }

        public int insertContactDetails(string extraEmail, string address1, string address2, string city, string state, string country, string userID)
        {
            string strSql = "INSERT INTO exam_usersDetails (extraEmail, address1, address2, city, state, country, userID) VALUES (@extraEmail, @address1, @address2, @city, @state, @country, @userID)";
            SqlParameter[] para ={
                                     new SqlParameter("extraEmail",extraEmail),
                                     new SqlParameter("address1",address1),
                                     new SqlParameter("address2",address2),
                                     new SqlParameter("city",city),
                                     new SqlParameter("state",state),
                                     new SqlParameter("country",country),
                                     new SqlParameter("userID",userID)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateContactDetails(string extraEmail, string Add1, string Add2, string city, string state, string country, string uid)
        {
            string strSql = "UPDATE exam_users SET extraEmail = @extraEmail, Add1 = @Add1, Add2 = @Add2, city = @city, state = @state, country = @country where uid = @uid";
            SqlParameter[] para ={
                                     new SqlParameter("extraEmail",extraEmail),
                                     new SqlParameter("Add1",Add1),
                                     new SqlParameter("Add2",Add2),
                                     new SqlParameter("city",city),
                                     new SqlParameter("state",state),
                                     new SqlParameter("country",country),
                                     new SqlParameter("uid",uid)
                                 
                                 };
            return obj.Update(strSql, para);
        }

        public int insertAboutYou(string aboutU, string userID)
        {
            string strSql = "INSERT INTO exam_usersDetails (aboutU, userID) VALUES (@aboutU, @userID)";
            SqlParameter[] para ={
                                     new SqlParameter("aboutU",aboutU),
                                     new SqlParameter("userID",userID)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateAboutYou(string aboutU, string uid)
        {
            string strSql = "UPDATE exam_users SET aboutU = @aboutU where uid = @uid";
            SqlParameter[] para ={
                                    new SqlParameter("aboutU",aboutU),
                                     new SqlParameter("uid",uid)
                                 };
            return obj.Update(strSql, para);
        }

        public int updatePassword(string uid, string password)
        {
            string strSql = "UPDATE exam_users SET password = @password where uid = @uid ";
            SqlParameter[] para ={
                                    new SqlParameter("password",password),
                                     new SqlParameter("uid",uid)
                                 };
            return obj.Update(strSql, para);
        }

        public int insertUserExamAttempt(string userID, string examid)
        {

            string strSql = "INSERT INTO exm_userAttempts (userID, examid, attemptDate) VALUES (@userID, @examid,getdate())";
            SqlParameter[] para ={
                                    new SqlParameter("userID",userID),
                                     new SqlParameter("examid",examid)
                                 };
            return obj.Insert(strSql, para);
        }

        public int insertQuestionInformation(string infHeading, string infContents, string instituteId)
        {
            string strSql = "Insert into exm_queInfo(infHeading,infContents, instituteId) values (@infHeading, @infContents, @instituteId)";
            SqlParameter[] para ={
                                    new SqlParameter("infHeading",infHeading),
                                    new SqlParameter("infContents",infContents),
                                    new SqlParameter("instituteId",instituteId)
                                 };
            return obj.Insert(strSql, para);
        }

        //public int createExam(string examName, string noofQuestions, int maxTime, string userID, string courseID, bool isAvailable, string noOfSections, bool negativeMarking, string examType, string coursrPackageID, DateTime onDate, string eDesc, string instituteId)
        //{
        //    string strSql = "INSERT INTO exm_existingExams (examName, maxTime, userID, noofQuestions, courseID, createdDate, isAvailable, noOfSections, negativeMarking, examType, coursrPackageID, onDate, eDesc, instituteId) VALUES (@examName, @maxTime, @userID, @noofQuestions, @courseID, getdate(), @isAvailable, @noOfSections, @negativeMarking, @examType, @coursrPackageID, CONVERT(datetime,@onDate,105), @eDesc, @instituteId)";
        //    //   string strSql = "INSERT INTO exm_existingExams (examName, questionNos, maxTime, userID) VALUES  (@examName, @questionNos, @maxTime, @userID)";
        //    SqlParameter[] para ={
        //                             new SqlParameter("examName",examName),
        //                             new SqlParameter("noofQuestions",noofQuestions),
        //                             new SqlParameter("maxTime",maxTime),
        //                             new SqlParameter("userID",userID),
        //                             new SqlParameter("courseID",courseID),
        //                             new SqlParameter("isAvailable",isAvailable),
        //                             new SqlParameter("noOfSections",noOfSections),
        //                             new SqlParameter("negativeMarking",negativeMarking),
        //                             new SqlParameter("examType",examType),
        //                             new SqlParameter("coursrPackageID",coursrPackageID),
        //                             new SqlParameter("onDate",onDate),
        //                             new SqlParameter("eDesc",eDesc),
        //                             new SqlParameter("instituteId",instituteId)
        //                         };
        //    return obj.Insert(strSql, para);
        //}

        public int createExam(string examName, string noofQuestions, int maxTime, string userID, string courseID, bool isAvailable, string noOfSections, bool negativeMarking, string examType, string coursrPackageID, DateTime onDate, string eDesc, string instituteId)
        {
            SqlCommand cmd = new SqlCommand("createExam", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("examName", examName);
            cmd.Parameters.AddWithValue("noofQuestions", noofQuestions);
            cmd.Parameters.AddWithValue("maxTime", maxTime);
            cmd.Parameters.AddWithValue("userID", userID);
            cmd.Parameters.AddWithValue("courseID", courseID);
            cmd.Parameters.AddWithValue("isAvailable", isAvailable);
            cmd.Parameters.AddWithValue("noOfSections", noOfSections);
            cmd.Parameters.AddWithValue("negativeMarking", negativeMarking);
            cmd.Parameters.AddWithValue("examType", examType);
            cmd.Parameters.AddWithValue("coursrPackageID", coursrPackageID);
            cmd.Parameters.AddWithValue("onDate", onDate);
            cmd.Parameters.AddWithValue("eDesc", eDesc);
            cmd.Parameters.AddWithValue("instituteId", instituteId);
            cn.Open();
            int i = cmd.ExecuteNonQuery();
            cn.Close();
            return i;
        }

        public DataTable getSectionwiseQuesNo(string examID)
        {
            string strSql = "Select section1Ques, section2Ques, section3Ques, section4Ques, section5Ques from exm_existingExams where examID=@examID";
            SqlParameter[] para ={
                                    new SqlParameter("examID",examID)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public int updateExamDetails(string questionNos, string examID)
        {
            string strSql = "UPDATE exm_existingExams SET questionNos = @questionNos where examID = @examID";
            SqlParameter[] para ={
                                     new SqlParameter("questionNos",questionNos),
                                     new SqlParameter("examID",examID)
                                 };
            return obj.Update(strSql, para);

        }

        public int updateQueNo(string noOfQuestions, string examID)
        {
            string strSql = "UPDATE exm_existingExams SET noOfQuestions=" + noOfQuestions + " where examID=" + examID;
            SqlParameter[] para ={
                                    new SqlParameter("noOfQuestions",noOfQuestions),
                                    new SqlParameter("examID",examID)
                                };
            return obj.Update(strSql, para);
        }

        public int updateEDetails(string examName, int maxTime, string noofQuestions, string courseID, string examID, bool isAvailable, string noOfSections, bool negativeMarking, string examType, string coursrPackageID, DateTime onDate, string eDesc)
        {
            string strSql = "UPDATE exm_existingExams SET examName = @examName,  maxTime = @maxTime,  noofQuestions = @noofQuestions , courseID = @courseID, isAvailable = @isAvailable ,noOfSections=@noOfSections, negativeMarking=@negativeMarking, examType = @examType, coursrPackageID=@coursrPackageID , onDate = @onDate, eDesc = @eDesc where examID = @examID";
            SqlParameter[] para ={
                                     new SqlParameter("examName",examName),
                                     new SqlParameter("examID",examID),
                                     new SqlParameter("maxTime",maxTime),
                                     new SqlParameter("noofQuestions",noofQuestions),
                                     new SqlParameter("courseID",courseID),
                                     new SqlParameter("isAvailable",isAvailable),
                                     new SqlParameter("noOfSections",noOfSections),
                                     new SqlParameter("negativeMarking",negativeMarking),
                                     new SqlParameter("examType",examType),
                                     new SqlParameter("coursrPackageID",coursrPackageID),
                                     new SqlParameter("onDate",onDate),
                                     new SqlParameter("eDesc",eDesc)
                               };
            return obj.Update(strSql, para);

        }

        public int DeleteEDetails(string examID)
        {
            string strSql = "DELETE from exm_existingExams where examID = @examID";
            SqlParameter[] para ={
                                    new SqlParameter("examID",examID),
                                                };
            return obj.Delete(strSql, para);
        }

        public int insertMarked(string userid, string examid, string questionNo)
        {
            string strSql = "INSERT into exm_markedQuestions (userid,examid,questionNo) VALUES (@userid,@examid,@questionNo)";
            SqlParameter[] para ={
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("examid",examid),
                                     new SqlParameter("questionNo",questionNo),
                                    };
            return obj.Insert(strSql, para);
        }

        public int deleteMarked(string userid, string examid, string questionNo)
        {
            string strSql = "DELETE from exm_markedQuestions where userid = @userid and examid = @examid and questionNo = @questionNo";
            SqlParameter[] para ={
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("examid",examid),
                                     new SqlParameter("questionNo",questionNo),
                                    };
            return obj.Delete(strSql, para);
        }

        public int saveStartInfo(string coursePreExamID, string description)
        {
            string strSql = "INSERT INTO exam_startPages (coursePreExamID,  description) VALUES (@coursePreExamID, @description)";
            SqlParameter[] para ={
                                     new SqlParameter("coursePreExamID",coursePreExamID),
                                    
                                     new SqlParameter("description",description),
                                    };
            return obj.Insert(strSql, para);
        }

        public int updateStartInfo(string id, string description)
        {
            string strSql = "UPDATE exam_startPages set description = @description where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                     new SqlParameter("description",description),
                                    };
            return obj.Insert(strSql, para);
        }

        public int insertUserResult(string userid, string examid, string totalMarks, string totalQue, string solvedQue, string correctQue, string preexamID)
        {
            string strSql = "INSERT INTO exam_userResult (userid, examid, totalMarks, totalQue, solvedQue, correctQue, preexamID) VALUES (@userid, @examid, @totalMarks, @totalQue, @solvedQue, @correctQue, @preexamID)";
            SqlParameter[] para ={
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("examid",examid),
                                     new SqlParameter("totalMarks",totalMarks),
                                     new SqlParameter("totalQue",totalQue),
                                      new SqlParameter("solvedQue",solvedQue),
                                       new SqlParameter("correctQue",correctQue),
                                         new SqlParameter("preexamID",preexamID),
                                     //    
                                 
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateUserPercentile(string id, double percentile, double accuracy, int totalTimeSpent)
        {
            string strSql = "UPDATE exam_userResult set percentile = @percentile , accuracy = @accuracy, totalTimeSpent=@totalTimeSpent where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                    new SqlParameter("percentile",percentile),
                                    new SqlParameter("accuracy",accuracy),
                                     new SqlParameter("totalTimeSpent",totalTimeSpent)
                                    };
            return obj.Update(strSql, para);
        }

        public int insertSectionwiseDetails(string examID, string userID, int sectionNo, int totalQue, int solvedQue, int correctQue, int incorrectQue, double marks, string preExamID)
        {
            string strSql = "INSERT INTO exam_sectionWiseDetails (examID, userID, sectionNo, totalQue, solvedQue, correctQue, incorrectQue, marks, preExamID ) VALUES(@examID, @userID, @sectionNo, @totalQue, @solvedQue, @correctQue, @incorrectQue, @marks, @preExamID)";
            SqlParameter[] para ={
                                     new SqlParameter("examID",examID),
                                     new SqlParameter("userID",userID),
                                     new SqlParameter("sectionNo",sectionNo),
                                     new SqlParameter("totalQue",totalQue),
                                      new SqlParameter("solvedQue",solvedQue),
                                       new SqlParameter("correctQue",correctQue),
                                         new SqlParameter("incorrectQue",incorrectQue),
                                          new SqlParameter("marks",marks),
                                          new SqlParameter("preExamID",preExamID)
                                 };
            return obj.Insert(strSql, para);// accuracy, percentile 
        }

        public int updateSectionwiseDetails(string id, double percentile, double accuracy, int sessionTime)
        {
            string strSql = "UPDATE exam_sectionWiseDetails set percentile = @percentile , accuracy = @accuracy, sessionTime = @sessionTime where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                    new SqlParameter("percentile",percentile),
                                    new SqlParameter("accuracy",accuracy),
                                    new SqlParameter("sessionTime",sessionTime)
                                    };
            return obj.Update(strSql, para);
        }

        public int insertEmailConfig(string emailID, string password, string smtpHost, bool smtpEnable, string smtpPort, string instituteId)
        {
            string strSql = "Insert into email_Config(emailID, password, smtpHost, smtpPort, smtpEnable, instituteId) values(@emailID, @password, @smtpHost, @smtpPort, @smtpEnable, @instituteId)";
            SqlParameter[] para = { 
                                    new SqlParameter("emailID",emailID),
                                    new SqlParameter("password",password),
                                    new SqlParameter("smtpHost",smtpHost),
                                    new SqlParameter("smtpPort",smtpPort),
                                    new SqlParameter("smtpEnable",smtpEnable),
                                    new SqlParameter("instituteId",instituteId)
                                  };
            return obj.Insert(strSql, para);
        }

        public int updateEmailConfig(string emailID, string password, string smtpHost, bool smtpEnable, string id, string smtpPort, string instituteId)
        {
            string strSql = "UPDATE  email_Config SET emailID = @emailID, password = @password, smtpHost = @smtpHost, smtpPort = @smtpPort, smtpEnable = @smtpEnable where id=@id and instituteId=@instituteId";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                    new SqlParameter("emailID",emailID),
                                    new SqlParameter("password",password),
                                    new SqlParameter("smtpHost",smtpHost),
                                    new SqlParameter("smtpPort",smtpPort),
                                    new SqlParameter("smtpEnable",smtpEnable),
                                    new SqlParameter("instituteId",instituteId)
                                    };
            return obj.Update(strSql, para);
        }

        //public void SendMailMessage(string from, string to, string subject, string body, string pass)
        //{
        //    System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(from, to, subject, body);
        //    mm.IsBodyHtml = true;
        //    System.Net.Mail.SmtpClient client = new SmtpClient();
        //    client.Host = "smtp.gmail.com";
        //    client.EnableSsl = true;
        //    client.Port = 587;
        //    NetworkCredential ntc = new NetworkCredential(from, pass);
        //    client.Credentials = ntc;
        //    client.Send(mm);
        //}

        public int insertAdmin(string username, string password, string name, string mobile, string adminType, string instituteId)
        {
            string strSql = "INSERT INTO admin_user (username, password, name, mobile, adminType, instituteId) VALUES (@username, @password, @name, @mobile, @adminType, @instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("username",username),
                                     new SqlParameter("password",password),
                                     new SqlParameter("name",name),
                                     new SqlParameter("mobile",mobile),
                                     new SqlParameter("adminType",adminType),
                                     new SqlParameter("instituteId",instituteId)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateAdmin(string username, string name, string mobile, string userid, string adminType, string instituteId)
        {
            string strSql = "Update admin_user set username = @username,  name= @name, mobile = @mobile, adminType = @adminType, instituteId=@instituteId where userid=@userid";
            SqlParameter[] para ={
                                     new SqlParameter("username",username),
                                     new SqlParameter("adminType",adminType),
                                     new SqlParameter("name",name),
                                     new SqlParameter("mobile",mobile),
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("instituteId",instituteId)
                                 };
            return obj.Update(strSql, para);
        }

        public int updateAdminWithPass(string username, string name, string mobile, string userid, string password, string adminType, string instituteId)
        {
            string strSql = "Update admin_user set username = @username,  name= @name, mobile = @mobile, password = @password, adminType = @adminType, instituteId=@instituteId where userid=@userid";
            SqlParameter[] para ={
                                     new SqlParameter("adminType",adminType),
                                     new SqlParameter("username",username),
                                     new SqlParameter("password",password),
                                     new SqlParameter("name",name),
                                     new SqlParameter("mobile",mobile),
                                     new SqlParameter("instituteId",instituteId),
                                     new SqlParameter("userid",userid)
                                 };
            return obj.Update(strSql, para);
        }

        public int updateAdminPass(string password, string userid)
        {
            string strSql = "UPDATE admin_user SET password = @password where userid = @userid";
            SqlParameter[] para ={
                                     new SqlParameter("password",password),
                                     new SqlParameter("userid",userid),
                              };
            return obj.Update(strSql, para);
        }

        public int deleteAdmin(string userid)
        {
            string strSql = "DELETE FROM admin_user where userid = @userid";
            SqlParameter[] para ={
                                     
                                     new SqlParameter("userid",userid),
                              };
            return obj.Delete(strSql, para);
        }

        public int saveInpirations(string Iname, string ImageName, string message, bool isActive, string degree)
        {
            string strSql = "INSERT INTO site_inspirations(Iname, ImageName, message, isActive,degree) VALUES (@Iname, @ImageName, @message, @isActive, @degree)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("Iname",Iname),
                                     new SqlParameter("ImageName",ImageName),
                                      new SqlParameter("message",message),
                                     new SqlParameter("isActive",isActive),
                                       new SqlParameter("degree",degree)
                              };
            return obj.Update(strSql, para);
        }

        public int updateInpirations(string Iname, string ImageName, string message, bool isActive, string ID, string degree)
        {
            string strSql = "UPDATE site_inspirations SET Iname = @Iname, ImageName = @ImageName, message = @message, isActive = @isActive, degree=@degree where ID = @ID";
            SqlParameter[] para =
                              {
                                     new SqlParameter("Iname",Iname),
                                     new SqlParameter("ImageName",ImageName),
                                      new SqlParameter("message",message),
                                     new SqlParameter("isActive",isActive),
                                     new SqlParameter("ID",ID),
                                      new SqlParameter("degree",degree)
                              };
            return obj.Update(strSql, para);
        }

        public int deleteInspiration(string ID)
        {
            string strSql = "Delete from site_inspirations where ID=@ID";
            SqlParameter[] para =
                              { 
                                  new SqlParameter("ID",ID),
                               };
            return obj.Delete(strSql, para);
        }

        public int updateWhyHMT(string whyHmt, string id)
        {
            string strSql = "UPDATE site_whyHMT SET whyHmt = @whyHmt WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("whyHmt",whyHmt),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int updateScrollinData(string news, string id)
        {
            string strSql = " UPDATE site_scrollingNew SET news = @news WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("news",news),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int updateAboutUs(string aboutUs, string id)
        {
            string strSql = "UPDATE site_aboutUs SET aboutUs = @aboutUs WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("aboutUs",aboutUs),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int insertCenter(string centerName, string address, string contactNo, string city, string mail, bool isActive)
        {
            string strSql = "INSERT INTO site_centers  (centerName, address, contactNo, city, mail, isActive) VALUES (@centerName, @address, @contactNo, @city, @mail, @isActive)";
            SqlParameter[] para ={
                                     new SqlParameter("centerName",centerName),
                                     new SqlParameter("address",address),
                                     new SqlParameter("contactNo",contactNo),
                                     new SqlParameter("city",city),
                                      new SqlParameter("mail",mail),
                                     new SqlParameter("isActive",isActive)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateCenter(string centerName, string address, string contactNo, string city, string mail, bool isActive, string id)
        {
            string strSql = "Update site_centers set centerName = @centerName, address = @address, contactNo = @contactNo, city = @city, mail = @mail, isActive = @isActive where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("centerName",centerName),
                                     new SqlParameter("address",address),
                                     new SqlParameter("contactNo",contactNo),
                                     new SqlParameter("city",city),
                                      new SqlParameter("mail",mail),
                                     new SqlParameter("isActive",isActive),
                                      new SqlParameter("id",id)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteCenter(string id)
        {
            string strSql = "DELETE from site_centers  where id = @id";
            SqlParameter[] para ={
                                         new SqlParameter("id",id)
                                 };
            return obj.Delete(strSql, para);
        }

        public int insertTests(string testName, string testTopics, DateTime testDate, bool isActive, string batch, string examMode)
        {
            string strSql = "INSERT INTO site_tests  (testName, testTopics, testDate, isActive, batch, examMode) VALUES (@testName, @testTopics, @testDate, @isActive,@batch , @examMode)";
            SqlParameter[] para ={
                                     new SqlParameter("testName",testName),
                                     new SqlParameter("testTopics",testTopics),
                                     new SqlParameter("testDate",testDate),
                                      new SqlParameter("isActive",isActive),
                                        new SqlParameter("batch",batch),
                                      new SqlParameter("examMode",examMode)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateTest(string testName, string testTopics, DateTime testDate, bool isActive, string id, string batch, string examMode)
        {
            string strSql = "Update site_tests set testName = @testName, testTopics = @testTopics, testDate = @testDate, isActive = @isActive, batch = @batch, examMode = @examMode where id = @id";
            SqlParameter[] para ={
                                  new SqlParameter("testName",testName),
                                     new SqlParameter("testTopics",testTopics),
                                     new SqlParameter("testDate",testDate),
                                      new SqlParameter("isActive",isActive),
                                      new SqlParameter("id",id),
                                        new SqlParameter("batch",batch),
                                      new SqlParameter("examMode",examMode)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteTest(string id)
        {
            string strSql = "DELETE from site_tests  where id = @id";
            SqlParameter[] para ={
                                         new SqlParameter("id",id)
                                 };
            return obj.Delete(strSql, para);
        }

        public int insertLiveSchedule(string subject, string testNo, DateTime sDate, string topics, string category)
        {
            string strSql = "INSERT INTO site_scheduleLive  (sDate, testNo, subject, topics, category) VALUES (@sDate, @testNo, @subject, @topics, @category)";
            SqlParameter[] para ={
                                     new SqlParameter("sDate",sDate),
                                     new SqlParameter("testNo",testNo),
                                     new SqlParameter("subject",subject),
                                      new SqlParameter("topics",topics),
                                        new SqlParameter("category",category)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateLiveSchedule(string subject, string testNo, DateTime sDate, string topics, string id, string category)
        {
            string strSql = "Update site_scheduleLive set subject = @subject, testNo = @testNo, sDate = @sDate, topics = @topics, category = @category where id = @id";
            SqlParameter[] para ={
                                  new SqlParameter("subject",subject),
                                     new SqlParameter("testNo",testNo),
                                     new SqlParameter("sDate",sDate),
                                      new SqlParameter("topics",topics),
                                      new SqlParameter("id",id),
                                         new SqlParameter("category",category)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteLiveSchedule(string id)
        {
            string strSql = "DELETE from site_scheduleLive  where id = @id";
            SqlParameter[] para ={
                                         new SqlParameter("id",id)
                                 };
            return obj.Delete(strSql, para);
        }

        public int insertOnlineSchedule(string subject, string testNo, DateTime sDate, string topics, string category)
        {
            string strSql = "INSERT INTO site_scheduleOnline  (sDate, testNo, subject, topics, category) VALUES (@sDate, @testNo, @subject, @topics, @category)";
            SqlParameter[] para ={
                                     new SqlParameter("sDate",sDate),
                                     new SqlParameter("testNo",testNo),
                                     new SqlParameter("subject",subject),
                                      new SqlParameter("topics",topics),
                                       new SqlParameter("category",category)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateOnlineSchedule(string subject, string testNo, DateTime sDate, string topics, string id, string category)
        {
            string strSql = "Update site_scheduleOnline set subject = @subject, testNo = @testNo, sDate = @sDate, topics = @topics, category=@category where id = @id";
            SqlParameter[] para ={
                                  new SqlParameter("subject",subject),
                                     new SqlParameter("testNo",testNo),
                                     new SqlParameter("sDate",sDate),
                                      new SqlParameter("topics",topics),
                                      new SqlParameter("id",id),
                                       new SqlParameter("category",category)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteOnlineSchedule(string id)
        {
            string strSql = "DELETE from site_scheduleOnline  where id = @id";
            SqlParameter[] para ={
                                         new SqlParameter("id",id)
                                 };
            return obj.Delete(strSql, para);
        }

        public int updateLatestNews(string lNews, string id)
        {
            string strSql = "UPDATE site_latestNews SET lNews = @lNews WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("lNews",lNews),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int updateInstructions(string instructions, string id)
        {
            string strSql = "UPDATE site_instructionsOnline SET instructions = @instructions WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("instructions",instructions),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int updateInstructionsOffline(string instructions, string id)
        {
            string strSql = "UPDATE site_instructionsOffline SET instructions = @instructions WHERE (id = @id)";
            SqlParameter[] para =
                              {
                                     new SqlParameter("instructions",instructions),
                                     new SqlParameter("id",id)
                             };
            return obj.Update(strSql, para);
        }

        public int insertPollingQue(string question, string option1, string option2, string option3, string option4, string answer, DateTime queDate)
        {
            string strSql = "INSERT INTO site_pollingQue( question, option1, option2, option3, option4, answer, queDate) VALUES (@question, @option1, @option2, @option3, @option4, @answer, @queDate)";
            SqlParameter[] para ={
                                     new SqlParameter("question",question),
                                     new SqlParameter("option1",option1),
                                     new SqlParameter("option2",option2),
                                     new SqlParameter("option3",option3),
                                      new SqlParameter("option4",option4),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("queDate",queDate)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updatePollingQue(string question, string option1, string option2, string option3, string option4, string answer, DateTime queDate, string id)
        {
            string strSql = "UPDATE site_pollingQue set question = @question, option1 =@option1, option2 = @option2, option3 = @option3, option4 = @option4, answer = @answer, queDate = @queDate where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("question",question),
                                     new SqlParameter("option1",option1),
                                     new SqlParameter("option2",option2),
                                     new SqlParameter("option3",option3),
                                      new SqlParameter("option4",option4),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("queDate",queDate),
                                      new SqlParameter("id",id)
                                 };
            return obj.Update(strSql, para);
        }

        public int deletePollingQue(string id)
        {
            string strSql = "Delete from site_pollingQue where id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id)};
            return obj.Delete(strSql, para);
        }

        public int insertCourses(string noofMCQs, string noofExam, double fees, string imageName, string className, DateTime fromDate, DateTime toDate, string Details, string sessionID)
        {
            string strSql = "INSERT INTO site_courses  ( noofMCQs, noofExam, fees, imageName, className, fromDate, toDate, Details, sessionID) VALUES   (@noofMCQs, @noofExam, @fees,  @imageName, @className, @fromDate, @toDate, @Details, @sessionID)";
            SqlParameter[] para ={
                                 
                                     new SqlParameter("noofMCQs",noofMCQs),
                                     new SqlParameter("noofExam",noofExam),
                                     new SqlParameter("fees",fees),
                                  
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("className",className),
                                      new SqlParameter("fromDate",fromDate),
                                     new SqlParameter("toDate",toDate),
                                     new SqlParameter("Details",Details),
                                     new SqlParameter("sessionID",sessionID)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateCourses(string noofMCQs, string noofExam, double fees, string imageName, string id, string className, DateTime fromDate, DateTime toDate, string Details, string sessionID)
        {
            string strSql = "UPDATE site_courses set  noofMCQs = @noofMCQs, noofExam = @noofExam, fees =@fees,  imageName = @imageName, className = @className, fromDate = @fromDate, toDate = @toDate, Details = @Details, sessionID = @sessionID  where id = @id";
            SqlParameter[] para ={
                                     
                                     new SqlParameter("noofMCQs",noofMCQs),
                                     new SqlParameter("noofExam",noofExam),
                                     new SqlParameter("fees",fees),
                                  new SqlParameter("sessionID",sessionID),
                                     new SqlParameter("imageName",imageName),
                                      new SqlParameter("id",id),
                                      new SqlParameter("className",className),
                                       new SqlParameter("fromDate",fromDate),
                                     new SqlParameter("toDate",toDate),
                                     new SqlParameter("Details",Details)
                                 };
            return obj.Update(strSql, para);
        }

        public int insertCoursesOffline(string noofMCQs, string noofExam, double fees, string imageName, string className, DateTime fromDate, DateTime toDate, string Details, string sessionID)
        {
            string strSql = "INSERT INTO site_coursesOffline  ( noofMCQs, noofExam, fees,  imageName, className, fromDate, toDate, Details, sessionID) VALUES   (@noofMCQs, @noofExam, @fees,  @imageName, @className,@fromDate, @toDate, @Details, @sessionID)";
            SqlParameter[] para ={
                                 
                                     new SqlParameter("noofMCQs",noofMCQs),
                                     new SqlParameter("noofExam",noofExam),
                                     new SqlParameter("sessionID",sessionID),
                                    new SqlParameter("fees",fees),
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("className",className),
                                      new SqlParameter("fromDate",fromDate),
                                     new SqlParameter("toDate",toDate),
                                     new SqlParameter("Details",Details)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateCoursesOffline(string noofMCQs, string noofExam, double fees, string imageName, string id, string className, DateTime fromDate, DateTime toDate, string Details, string sessionID)
        {
            string strSql = "UPDATE site_coursesOffline set  noofMCQs = @noofMCQs, noofExam = @noofExam, fees =@fees,  imageName = @imageName, className = @className, fromDate = @fromDate, toDate = @toDate, Details = @Details, sessionID = @sessionID where id = @id";
            SqlParameter[] para ={
                                     
                                     new SqlParameter("noofMCQs",noofMCQs),
                                     new SqlParameter("noofExam",noofExam),
                                     new SqlParameter("fees",fees),
                                   new SqlParameter("sessionID",sessionID),
                                     new SqlParameter("imageName",imageName),
                                      new SqlParameter("id",id),
                                      new SqlParameter("className",className),
                                        new SqlParameter("fromDate",fromDate),
                                     new SqlParameter("toDate",toDate),
                                     new SqlParameter("Details",Details)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteOfflineCourse(string id)
        {
            string strSql = "Delete from site_coursesOffline where id = @id";
            SqlParameter[] para ={
                                    new SqlParameter("id",id),
                                 };
            return obj.Delete(strSql, para);
        }

        public int deleteOnlineCourse(string id)
        {
            string strSql = "Delete from site_courses where id = @id";
            SqlParameter[] para ={
                                    new SqlParameter("id",id),
                                 };
            return obj.Delete(strSql, para);
        }

        public int insertFileName(string fileName)
        {
            string strSql = "INSERT INTO site_downloads( fileName) values (@fileName)";
            SqlParameter[] para ={
                                     new SqlParameter("fileName",fileName)
                                 
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateQueAnswer(string questNo, string exmid, string userid, string answer, int remainingTime, int timeSpent)
        {
            string strSql = "Update exm_userAnswers set answer = @answer , remainingTime = @remainingTime, timeSpent=timeSpent+@timeSpent where questNo = @questNo and userid = @userid and exmid = @exmid";
            SqlParameter[] para ={ 
                                     new SqlParameter("questNo",questNo),
                                     new SqlParameter("exmid",exmid),
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("remainingTime",remainingTime),
                                      new SqlParameter("timeSpent",timeSpent)
                                       };
            return obj.Update(strSql, para);
        }

        public int updateDetails(string Edetails, string id)
        {
            string strSql = "Update site_ExamDetails set Edetails = @Edetails where id=@id ";
            SqlParameter[] para ={ 
                                     new SqlParameter("Edetails",Edetails),
                                     new SqlParameter("id",id),
                                 };
            return obj.Update(strSql, para);

        }

        public int insertFaq(string questions, string answer, string FaqType)
        {
            string strSql = "INSERT INTO site_FAQ  ( questions, answer, FaqType) VALUES   (@questions, @answer, @FaqType)";
            SqlParameter[] para ={
                                 
                                     new SqlParameter("questions",questions),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("FaqType",FaqType),
                                 
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateFaq(string questions, string answer, string id)
        {
            string strSql = "Update site_FAQ set questions = @questions, answer = @answer where id=@id ";
            SqlParameter[] para ={ 
                                    new SqlParameter("questions",questions),
                                     new SqlParameter("answer",answer),
                                     new SqlParameter("id",id),
                                 };
            return obj.Update(strSql, para);

        }

        public int deleteFaq(string id)
        {
            string strSql = "Delete from site_FAQ  where id=@id ";
            SqlParameter[] para ={ 
                                  
                                     new SqlParameter("id",id),
                                 };
            return obj.Update(strSql, para);

        }

        public int insertResult(string fileName, string forClass, string ExamName)
        {
            string strSql = "INSERT INTO site_offlineResult( fileName, forClass, ExamName) values (@fileName, @forClass, @ExamName)";
            SqlParameter[] para ={
                                     new SqlParameter("fileName",fileName),
                                   new SqlParameter("forClass",forClass),
                                   new SqlParameter("ExamName",ExamName)
                                 };
            return obj.Insert(strSql, para);
        }

        public int deleteResult(string id)
        {
            string strSql = "DELETE from site_offlineResult WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                   
                                 };
            return obj.Delete(strSql, para);
        }

        public int updateEmailMsg(string emailMsg, string id)
        {
            string strSql = "UPDATE site_emailMessages set emailMsg = @emailMsg where id = @id";
            SqlParameter[] para ={ 
                                     new SqlParameter("id",id),
                                     new SqlParameter("emailMsg",emailMsg)
                                 };
            return obj.Update(strSql, para);
        }

        public int deleteMCQ(string id)
        {
            string strSql = "DELETE from site_contributeMCQ where id= @id";
            SqlParameter[] para = { 
                                  
                                  new SqlParameter("id",id)
                                  };
            return obj.Delete(strSql, para);
        }

        public int insertSession(string sName)
        {
            string strSql = "INSERT INTO site_Sessions (sName) values(@sName)";
            SqlParameter[] para = { 
                                  new SqlParameter("sName",sName)
                                  };
            return obj.Insert(strSql, para);
        }

        public int updateSession(string sName, string id)
        {
            string strSql = "UPDATE site_sessions set sName=@sName where id= @id";
            SqlParameter[] para = { 
                                  new SqlParameter("id",id),
                                  new SqlParameter("sName",sName)
                                  };
            return obj.Update(strSql, para);
        }

        public int deleteSession(string id)
        {
            string strSql = "Delete from site_sessions where id = @id";
            SqlParameter[] para = { 
                                   new SqlParameter("id",id),
                                  };
            return obj.Delete(strSql, para);
        }

        public int setSession(string id)
        {
            string strSql = "";
            strSql = "Update site_sessions set isActive='false'";
            obj.Update(strSql);
            strSql = "UPDATE site_sessions set isActive = 'true' WHERE id = @id";
            SqlParameter[] para = { 
                                   new SqlParameter("id",id),
                                  };
            return obj.Update(strSql, para);
        }

        public int updateDeductionFees(int fees)
        {
            string strSql = "UPDATE site_feesDeduction set fees = @fees ";
            SqlParameter[] para = { 
                                   new SqlParameter("fees",fees),
                                  };
            return obj.Update(strSql, para);
        }

        public int insertExamCompleted(string uID, string examID)
        {
            string strSql = "INSERT INTO exam_userCompleted (uID,examID) VALUES (@uID, @examID)";
            SqlParameter[] para = { 
                                  new SqlParameter("uID",uID),
                                  new SqlParameter("examID",examID)
                                  };
            return obj.Insert(strSql, para);
        }

        public int CreateUserAccount(string uname, string uemail, string password, string mobileNo, string Add1, string Add2, string country, string state, string city, string imageName, string linkID, string instituteId)
        {
            string strSql = "INSERT INTO exam_users (uname, uemail, password, mobileNo, Add1, Add2, country, state, city, imageName, linkID, instituteId) VALUES (@uname, @uemail, @password, @mobileNo, @Add1, @Add2, @country, @state, @city, @imageName, @linkID, @instituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("mobileNo",mobileNo),
                                     new SqlParameter("uname",uname),
                                     new SqlParameter("uemail",uemail),
                                     new SqlParameter("password",password),
                                     new SqlParameter("Add1",Add1),
                                     new SqlParameter("Add2",Add2),
                                     new SqlParameter("city",city),
                                     new SqlParameter("country",country),
                                     new SqlParameter("state",state),
                                     new SqlParameter("imageName",imageName),
                                     new SqlParameter("linkID",linkID),
                                     new SqlParameter("instituteId",instituteId)
                                };
            obj.Insert(strSql, para);
            string str = "SELECT MAX(uid) FROM exam_users";
            return Convert.ToInt32(obj.SelectScalar(str));


        }

        public DataTable getAllExams()
        {
            string strSql = "SELECT examID, examName, 'Total Questions : ' + CONVERT(varchar(5), noofQuestions) AS noofQuestions, negativeMarking, coursrPackageID, noOfSections,CASE WHEN coursrPackageID = 'Computer' THEN 'comp.jpg' WHEN coursrPackageID = 'English' THEN 'english.jpg' WHEN coursrPackageID = 'General Awareness' THEN 'GenAwareness.jpg' WHEN coursrPackageID = 'Model Paper' THEN 'modal.jpg' WHEN coursrPackageID = 'Numerical' THEN 'numerical.jpg' WHEN coursrPackageID = 'Reasoning' THEN 'reasoning.jpg' END AS ImageName, eDesc, noofQuestions as queNos FROM exm_existingExams WHERE IsAvailable = 'true'";
            return obj.SelectMultiple(strSql);
        }

        public DataTable getExamDetailsByID(string examID)
        {
            string strSql = "SELECT examID, examName, 'Total Questions : ' + CONVERT(varchar(5), noofQuestions) AS noofQuestions,CASE WHEN negativeMarking = 'True' THEN 'Yes' Else 'No' end as  negativeMarking, coursrPackageID, noOfSections,CASE WHEN coursrPackageID = 'Computer' THEN 'comp.jpg' WHEN coursrPackageID = 'English' THEN 'english.jpg' WHEN coursrPackageID = 'General Awareness' THEN 'GenAwareness.jpg' WHEN coursrPackageID = 'Model Paper' THEN 'modal.jpg' WHEN coursrPackageID = 'Numerical' THEN 'numerical.jpg' WHEN coursrPackageID = 'Reasoning' THEN 'reasoning.jpg' END AS ImageName, eDesc, noofQuestions as queNos, coursrPackageID, maxTime FROM exm_existingExams WHERE IsAvailable = 'true' and examID = @examID";
            SqlParameter[] para ={
                                     new SqlParameter("examID",examID)};
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getTopTestimony()
        {
            string strSql = "SELECT top(5) dbo.site_testimony.testimony, dbo.exam_users.imageName, exam_users.uname FROM dbo.site_testimony INNER JOIN  dbo.exam_users ON dbo.site_testimony.usreID = dbo.exam_users.uid order by  dbo.site_testimony.id desc";
            // string strSql = "Select top(5) *, Case when len(testimony)>50 Then left(testimony,35) +'....' else testimony end as t1 from site_testimony order by id desc";
            return obj.SelectMultiple(strSql);
        }

        public int askToExpert(string question, string userid)
        {
            string strSql = "INSERT INTO site_askToExpert (question, userid) VALUES (@question, @userid)";
            SqlParameter[] para ={
                                     new SqlParameter("userid",userid),
                                     new SqlParameter("question",question)};
            return obj.Insert(strSql, para);
        }

        public int insertTestimony(string testimony, string usreID)
        {
            string strSql = "INSERT INTO site_testimony (testimony, usreID) VALUES (@testimony, @usreID)";
            SqlParameter[] para ={
                                     new SqlParameter("usreID",usreID),
                                     new SqlParameter("testimony",testimony)};
            return obj.Insert(strSql, para);
        }

        public int updateTestimony(string testimony, string id)
        {
            string strSql = "UPDATE site_testimony SET testimony = @testimony WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                     new SqlParameter("testimony",testimony)};
            return obj.Update(strSql, para);
        }

        public int insertUsefulLink(string Linkheader, string redirectLink, string linkcategory)
        {
            string strSql = "INSERT INTO exam_usefulLinksAndNews (Linkheader, redirectLink, linkcategory) VALUES (@Linkheader, @redirectLink, @linkcategory)";
            SqlParameter[] para ={
                                     new SqlParameter("Linkheader",Linkheader),
                                        new SqlParameter("linkcategory",linkcategory),
                                     new SqlParameter("redirectLink",redirectLink)};
            return obj.Insert(strSql, para);
        }

        public int deleteUsefulLink(string id)
        {
            string strSql = "DELETE FROM exam_usefulLinksAndNews WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id)
                              };
            return obj.Delete(strSql, para);
        }

        public DataTable getRecruitmentNews()
        {
            string strSql = "SELECT top(5) Linkheader, redirectLink, linkcategory, id FROM exam_usefulLinksAndNews WHERE linkcategory = 'Recruitment News' ORDER BY id desc";
            return obj.SelectMultiple(strSql);
        }

        public DataTable getUsefulLinks()
        {
            string strSql = "SELECT top(5) Linkheader, redirectLink, linkcategory, id FROM exam_usefulLinksAndNews WHERE linkcategory = 'Useful Link' ORDER BY id desc";
            return obj.SelectMultiple(strSql);
        }

        public int updateAnswer(string answer, string id)
        {
            string strSql = "UPDATE  site_askToExpert SET answer = @answer WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id),
                                     new SqlParameter("answer",answer)};
            return obj.Update(strSql, para);
        }

        public DataTable getQueAns()
        {
            string strSql = "SELECT TOP (5) question, answer, uname, id FROM view_expertQueAns WHERE (LEN(answer) > 0) AND (LEN(question) > 0) order by id desc";
            return obj.SelectMultiple(strSql);
        }

        public int insertStudyMaterial(string title, string description, string iconImage, string downloadFile, bool isActive)
        {
            string strSql = "INSERT INTO site_studyMaterials (title, description, iconImage, downloadFile, isActive) VALUES (@title, @description, @iconImage, @downloadFile, @isActive)";
            SqlParameter[] para ={
                                     new SqlParameter("title",title),
                                     new SqlParameter("description",description),
                                     new SqlParameter("iconImage",iconImage),
                                     new SqlParameter("downloadFile",downloadFile),
                                      new SqlParameter("isActive",isActive),
                               };
            return obj.Insert(strSql, para);

        }

        public int getMaxSMaterialID()
        {
            try
            {
                string strSql = "SELECT max(id) from site_studyMaterials";
                return Convert.ToInt32(obj.SelectScalar(strSql));
            }
            catch { return 0; }
        }

        public int updateStudyMaterial(string title, string description, string iconImage, string downloadFile, bool isActive, string id)
        {
            string strSql = "UPDATE site_studyMaterials SET title = @title, description = @description, iconImage = @iconImage, downloadFile = @downloadFile, isActive = @isActive WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("title",title),
                                     new SqlParameter("description",description),
                                     new SqlParameter("iconImage",iconImage),
                                     new SqlParameter("downloadFile",downloadFile),
                                      new SqlParameter("isActive",isActive),
                                      new SqlParameter("id",id)
                               };
            return obj.Update(strSql, para);
        }

        public DataTable getStudyMaterialDetails(string id)
        {
            string strSql = "SELECT Mheading, Mcategory, Mdetails, id FROM site_StudyMaterialTable WHERE id= @id";
            SqlParameter[] para = { new SqlParameter("id", id) };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getStudyMaterials(string Mcategory)
        {
            string strSql = "SELECT TOP(6) CASE WHEN LEN(Mheading) > 30 THEN LEFT(Mheading,30) + '...' ELSE Mheading END AS Mheading , Mcategory, id FROM site_StudyMaterialTable WHERE Mcategory = @Mcategory ORDER BY MAddDate DESC"; //"SELECT id, title, description, iconImage, downloadFile, isActive, CASE WHEN len(description)>30 THEN left(description,30) + '...' ELSE description END as SDesc  FROM site_studyMaterials WHERE isActive = 1 ORDER BY id Desc";
            SqlParameter[] para = { new SqlParameter("Mcategory", Mcategory) };
            return obj.SelectMultiple(strSql, para);
        }

        public int insertContactUs(string name, string email, string mobile, string query)
        {
            string strSql = "INSERT INTO site_enquiry (name, email, mobile, query, eDate) VALUES (@name, @email, @mobile, @query, getdate())";
            SqlParameter[] para = { 
                                  new SqlParameter("name",name),
                                  new SqlParameter("email",email),
                                  new SqlParameter("mobile",mobile),
                                  new SqlParameter("query",query)
                                 };
            return obj.Insert(strSql, para);
        }

        public int addImpData(string Mheading, string Mdetails, string Mcategory)
        {
            string strSql = "INSERT INTO site_StudyMaterialTable (Mheading, Mdetails, Mcategory) VALUES (@Mheading,  @Mdetails, @Mcategory)";
            SqlParameter[] para ={
                                     new SqlParameter("Mheading",Mheading),
                              
                                     new SqlParameter("Mdetails",Mdetails),
                                  new SqlParameter("Mcategory",Mcategory)
                                 };
            return obj.Insert(strSql, para);
        }

        public DataTable getdataByID(string id)
        {
            string strSql = "SELECT Mheading, Mdetails, Mcategory, MAddDate FROM site_StudyMaterialTable WHERE id = @id";
            SqlParameter[] para ={
                                     new SqlParameter("id",id)
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public int addSyllabus(string sHeading, string sDetails)
        {
            string strSql = "INSERT INTO site_syllabus (sHeading, sDetails) VALUES (@sHeading, @sDetails)";
            SqlParameter[] para ={
                                new SqlParameter("sHeading",sHeading),
                                  new SqlParameter("sDetails",sDetails)
                                 };
            return obj.Insert(strSql, para);
        }

        public DataTable getExamSyllabus()
        {
            string strSql = "SELECT CASE WHEN LEN(sHeading) > 30 THEN LEFT(sHeading, 30) + '...' ELSE sheading END AS sHeading, id FROM site_syllabus";
            return obj.SelectMultiple(strSql);
        }

        public int insertFeedback(string userID, string examID, string feedBack)
        {
            string strSql = "INSERT INTO exm_feedback (userID, examID, feedBack) VALUES (@userID, @examID, @feedBack)";
            SqlParameter[] para ={
                                  new SqlParameter("feedBack",feedBack),
                                  new SqlParameter("examID",examID),
                                  new SqlParameter("userID",userID)
                                 };
            return obj.Insert(strSql, para);
        }

        public int insertMenu(string PageName, string Preferance)
        {
            string strSql = "INSERT INTO tbl_FrontPagesDetails (PageName, Preferance) VALUES (@PageName, @Preferance)";
            SqlParameter[] para ={
                                  new SqlParameter("PageName",PageName),
                                  new SqlParameter("Preferance",Preferance)
                                 };
            return obj.Insert(strSql, para);
        }

        public DataTable getmenuPref(string Preferance)
        {
            string strSql = "SELECT Preferance FROM tbl_FrontPagesDetails WHERE (Preferance = @Preferance)";
            SqlParameter[] para ={
                                new SqlParameter("Preferance",Preferance),
                           
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getCommonData(string infID)
        {
            string strSql = "SELECT infID, infHeading, infContents FROM exm_queinfo WHERE (infID = @infID)";
            SqlParameter[] para ={
                                new SqlParameter("infID",infID),
                           
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getPageContent(string pageid)
        {
            string strSql = "SELECT * FROM tbl_FrontPagesDetails WHERE (pageid = @pageid)";
            SqlParameter[] para ={
                                new SqlParameter("pageid",pageid),
                           
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public int updateCommonData(string infID, string infHeading, string infContents)
        {
            string strSql = "UPDATE exm_queinfo SET infHeading = @infHeading, infContents = @infContents  WHERE infID = @infID";
            SqlParameter[] para ={
                                new SqlParameter("infID",infID),
                                new SqlParameter("infContents",infContents),
                                new SqlParameter("infHeading",infHeading),
                             };
            return obj.Update(strSql, para);
        }

        public int updatePageContent(string pageid, string PageDetails)
        {
            string strSql = "UPDATE tbl_FrontPagesDetails SET PageDetails = @PageDetails  WHERE pageid = @pageid";
            SqlParameter[] para ={
                                new SqlParameter("pageid",pageid),
                                new SqlParameter("PageDetails",PageDetails),
                                
                             };
            return obj.Update(strSql, para);
        }

        public DataTable getQuestionsTable(int fromQueNo)
        {
            string strSql = "SELECT questid AS [Unique ID], question AS Question, option1 AS Option1, option2 AS Option2, option3, option4, option5, answer AS Answer, mark AS Mark,  negativeDeduction AS [Negative Deduction] FROM exm_questions WHERE  questid = @fromQueNo or questid > @fromQueNo ORDER By questid ASC ";
            SqlParameter[] para = { 
                                  new SqlParameter("fromQueNo",fromQueNo)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public int getTotalQuestionsAnswers(string exmid, string userid)
        {
            string strSql = "select count (id) from exm_userAnswers where exmid= @exmid and userid =@userid ";
            SqlParameter[] para = { 
                                  new SqlParameter("exmid",exmid),
                                 new SqlParameter("userid",userid),
                                  };
            return Convert.ToInt32(obj.SelectScalar(strSql, para));
        }

        //public Boolean gmailmail(string to, string from, string sub, string message, string pass)
        //{
        //    SmtpClient client = new SmtpClient();
        //    client.EnableSsl = true;
        //    client.Host = "smtp.gmail.com";
        //    client.Port = 587;

        //    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(from, pass);
        //    client.UseDefaultCredentials = false;

        //    client.Credentials = credentials;

        //    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();

        //    msg.From = new MailAddress(from);
        //    msg.To.Add(new MailAddress(to));

        //    msg.Subject = sub;
        //    msg.IsBodyHtml = true;
        //    msg.Body = string.Format(message);

        //    //string fname = Server.MapPath("~/attach/" + FileUpload1.FileName);
        //    //FileUpload1.SaveAs(fname);

        //    //string fname2 = Server.MapPath("~/attach/" + FileUpload2.FileName);
        //    //FileUpload2.SaveAs(fname2);

        //    //System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(fname);
        //    //System.Net.Mail.Attachment attach2 = new System.Net.Mail.Attachment(fname2);

        //    //if (attach != null)
        //    //    msg.Attachments.Add(attach);
        //    //if (attach2 != null)
        //    //    msg.Attachments.Add(attach2);
        //    try
        //    {
        //        client.Send(msg);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public string sendMail(string from, string sendTo, string body, string subject)
        {
            string message = "";
            try
            {
                const string SERVER = "relay-hosting.secureserver.net";
                using (System.Net.Mail.MailMessage mess = new System.Net.Mail.MailMessage())
                {
                    System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();

                    mess.To.Add(sendTo);

                    mess.Subject = subject;
                    mess.Body = body;
                    mess.IsBodyHtml = true;
                    mess.From = new System.Net.Mail.MailAddress(from);
                    sc.Host = SERVER;
                    sc.Send(mess);
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            return message;
        }

        public DataTable getUserDetailsByLinkID(string linkID, string uid)
        {
            string strSql = "SELECT uid, uemail, uname,IsActive FROM exam_users WHERE linkID=@linkID and uid = @uid";
            SqlParameter[] para = { 
                                  new SqlParameter("linkID",linkID),
                                  new SqlParameter("uid",uid)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public int updateUserRegistration(string uid, string pass)
        {
            string strSql = "UPDATE exam_users SET password=@pass,Isactive='1' WHERE uid=@uid";
            SqlParameter[] para = { 
                                  new SqlParameter("pass",pass),
                                  new SqlParameter ("uid",uid)
                                  };
            return obj.Update(strSql, para);
        }

        public DataTable getDetails(string queID)
        {
            string strSql = "SELECT  examID, examName, isnull((questionNos),'') + isnull((questionList),'') as list FROM view_examQuestionList WHERE (questionNos LIKE '%,@queID,%') OR (questionNos LIKE '%@queID,%') OR (questionNos LIKE '%,@queID%') OR (questionList LIKE '%,@queID,%') OR (questionList LIKE '%@queID,%') OR (questionList LIKE '%,@queID%')";
            SqlParameter[] para = { new SqlParameter("queID", queID) };
            return obj.SelectMultiple(strSql, para);
        }

        public int setFeedback(bool isPublish, string id)
        {
            string strSql = "UPDATE exm_feedback SET isPublish = @isPublish WHERE id = @id";
            SqlParameter[] para = { 
                                  new SqlParameter ("isPublish",isPublish),
                                  new SqlParameter ("id",id)
                                  };
            return obj.Update(strSql, para);

        }

        public int deleteFeedback(string id)
        {
            string strSql = "DELETE FROM exm_feedback WHERE id = @id";
            SqlParameter[] para = { new SqlParameter("id", id) };
            return obj.Delete(strSql, para);
        }

        public int deleteUser(string uid)
        {
            string strSql = "DELETE FROM exam_Users WHERE uid = @uid";
            SqlParameter[] para = { new SqlParameter("uid", uid) };
            return obj.Delete(strSql, para);
        }

        public int setSectionQue(string queryPart, string examID, string queList)
        {
            string strSql = "" + queryPart + "  @queList  WHERE examID = @examID";
            SqlParameter[] para ={
                            
                                new SqlParameter("examID",examID),
                            new SqlParameter("queList",queList),
                             };
            return obj.Update(strSql, para);
        }

        public DataTable getExamInfoByID(string examID)
        {
            string strSql = "SELECT examID, examName, 'Total Questions : ' + CONVERT(varchar(5), noofQuestions) AS noofQuestions, negativeMarking, noOfSections, eDesc, noofQuestions as queNos, coursrPackageID, maxTime,section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM exm_existingExams WHERE examID = @examID";
            SqlParameter[] para ={
                                     new SqlParameter("examID",examID)};
            return obj.SelectMultiple(strSql, para);
        }

        public int setStartPage(string coursePreExamID, string description)
        {

            string strSql = "INSERT INTO exam_startPages (coursePreExamID,description) values (@coursePreExamID,@description)";
            SqlParameter[] para ={
                                  new SqlParameter("coursePreExamID",coursePreExamID),
                                  new SqlParameter("description",description)
                                 };
            return obj.Insert(strSql, para);
        }

        public DataTable getLink(int pageID)
        {
            string strSql = "select link from tbl_pagewiseLinks where pageID=@pageID";
            SqlParameter[] para ={
                                new SqlParameter("pageID",pageID),
                           
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public int getMaxID()
        {
            string strSql = "select max(id) from tblMenu";
            return Convert.ToInt32(obj.SelectScalar(strSql));
        }

        public int insertFinalMenu(string menuName, bool isParent, string pageContents, string parentID, string preference, string link, bool notvisibleheader, bool displayinfooter)
        {
            string strSql = "Insert into tblMenu (menuName,isParent,pageContents,parentID,preference,link,notvisibleheader,displayinfooter) values (@menuName,@isParent,@pageContents,@parentID,@preference,@link,@notvisibleheader,@displayinfooter)";
            SqlParameter[] para ={
                                  new SqlParameter("menuName",menuName),
                                  new SqlParameter("isParent",isParent),
                                  new SqlParameter("pageContents",pageContents),
                                  new SqlParameter("parentID",parentID),
                                  new SqlParameter("preference",preference),
                                  new SqlParameter("link",link),
                                   new SqlParameter("notvisibleheader",notvisibleheader),
                                  new SqlParameter("displayinfooter",displayinfooter)
                                 };
            return obj.Insert(strSql, para);
        }

        public int updateFinalMenu(string menuName, bool isParent, string pageContents, string parentID, string id, string preference, string link, bool notvisibleheader, bool displayinfooter)
        {
            string strSql = "UPDATE tblMenu SET menuName= @menuName,isParent=@isParent,pageContents=@pageContents,parentID=@parentID,preference=@preference,link=@link,notvisibleheader=@notvisibleheader,displayinfooter=@displayinfooter WHERE id =@id";
            SqlParameter[] para ={
                                  new SqlParameter("menuName",menuName),
                                  new SqlParameter("isParent",isParent),
                                  new SqlParameter("pageContents",pageContents),
                                  new SqlParameter("parentID",parentID),
                                   new SqlParameter("id",id),
                                  new SqlParameter("preference",preference),
                                  new SqlParameter("link",link),
                                   new SqlParameter("notvisibleheader",notvisibleheader),
                                  new SqlParameter("displayinfooter",displayinfooter)
                                 };
            return obj.Update(strSql, para);
        }

        public DataTable getFinalMenu(int id)
        {
            string strSql = "Select  * from tblMenu where id=@id ";
            SqlParameter[] para ={
                                new SqlParameter("id",id),
                           
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public int deleteFinalMenu(int id)
        {
            string strSql = " delete from  tblMenu where id=@id";
            SqlParameter[] para = { new SqlParameter("id", id) };
            return obj.Delete(strSql, para);
        }

        public DataTable getInstitute()
        {
            DataTable dt = new DataTable();
            string strSql = "Select InstituteId, Institute from exam_Institute where isActive='true' order by Institute asc";
            return obj.SelectMultiple(strSql);
        }

        public int insertPaidExmDetails(string examID, string examFees, string allowedAttempt)
        {
            string strSql = "Insert into exm_paidExams(examID, examFees, allowedAttempt) values(@examID, @examFees, @allowedAttempt)";
            SqlParameter[] para ={
                                new SqlParameter("examID",examID),
                                new SqlParameter("examFees", examFees),
                                new SqlParameter("allowedAttempt", allowedAttempt)
                                };
            return obj.Insert(strSql, para);
        }

        public int updatePaidExmDetails(string ID, string examID, string examFees, string allowedAttempt)
        {
            string strSql = "Update exm_paidExams set examID=@examID, examFees=@examFees, allowedAttempt=@allowedAttempt where ID=@ID";
            SqlParameter[] para ={
                                new SqlParameter("ID",ID),
                                new SqlParameter("examID",examID),
                                new SqlParameter("examFees", examFees),
                                new SqlParameter("allowedAttempt", allowedAttempt)
                                };
            return obj.Update(strSql, para);
        }

        public int deletePaidExmDetails(string ID)
        {
            string strSql = "DELETE from exm_paidExams where ID = @ID";
            SqlParameter[] para ={
                                    new SqlParameter("ID",ID),
                                                };
            return obj.Delete(strSql, para);
        }

        public DataTable getPaidExamsByInstitute(string instituteId)
        {
            DataTable dt = new DataTable();
            string strSql = "Select examID, examName from view_examDetails where instituteId=@instituteId and examType='Paid'";
            SqlParameter[] para = { 
                                  new SqlParameter("instituteId",instituteId)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        //public DataTable getPaidExams()
        //{
        //    DataTable dt = new DataTable();
        //    string strSql = "Select examID, examName from exm_existingExams where examType='Paid'";
        //    return obj.SelectMultiple(strSql);
        //}

        public DataTable getPaidExamDetailsByExamID(string examID)
        {
            DataTable dt = new DataTable();
            string strSql = "Select ID, examID, examName, examFees, allowedAttempt, userID, ' Rs.'+ convert(varchar(20),examFees)+' (Allowed attempts:'+convert(varchar(20),allowedAttempt)+')' as exmPackageName,  (convert(varchar(10),ID))+':'+(convert(varchar(10),allowedAttempt))+':'+(convert(varchar(10),examFees)) as totID from view_paidExams where examID=@examID";
            SqlParameter[] para = { 
                                  new SqlParameter("examID",examID)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable checkPaidExamForInsert(string examID, string examFees, string allowedAttempt)
        {
            DataTable dt = new DataTable();
            string strSql = "Select * from view_paidExams where examID=@examID and examFees=@examFees and allowedAttempt=@allowedAttempt";
            SqlParameter[] para ={
                                new SqlParameter("examID",examID),
                                new SqlParameter("examFees",examFees),
                                new SqlParameter("allowedAttempt",allowedAttempt)
                                };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable checkPaidExamForUpdate(string ID, string examID, string examFees, string allowedAttempt)
        {
            DataTable dt = new DataTable();
            string strSql = "Select * from view_paidExams where examID=@examID and examFees=@examFees and allowedAttempt=@allowedAttempt and ID!=@ID";
            SqlParameter[] para ={
                                     new SqlParameter("ID",ID),
                                     new SqlParameter("examID",examID),
                                     new SqlParameter("examFees",examFees),
                                     new SqlParameter("allowedAttempt",allowedAttempt)
                                 };
            return obj.SelectMultiple(strSql, para);
        }

        public int insertPaidExmUserDetails(string userId, string examId, string paidExmID, string exmAmt, string exmAttempt)
        {
            string strSql = "INSERT into exm_paidExmUsers(userId, examId, paidExmID, exmAmt, exmAttempt) VALUES(@userId, @examId, @paidExmID, @exmAmt, @exmAttempt)";
            SqlParameter[] para ={
                                    new SqlParameter("userId",userId), 
                                    new SqlParameter("examId", examId), 
                                    new SqlParameter("paidExmID", paidExmID), 
                                    new SqlParameter("exmAmt",exmAmt), 
                                    new SqlParameter("exmAttempt",exmAttempt)
                                };
            return obj.Insert(strSql, para);
        }

        public string returnHTML(string transId)
        {
            //create object for get checksum

            IntegrationKit.libfuncs myUtility = new IntegrationKit.libfuncs();
            string htmltext = "";
            try
            {
                DataTable dt = new DataTable();
                dt = getData("SELECT * from view_paidExmUserDetails where transID=" + transId);

                //code for runtime html 
                htmltext = "<table class='membersvs'><tr>";
                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Name  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["uname"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Mobile No.  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["mobileNo"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Email ID  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["uemail"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Address ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["address"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "City  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["city"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Transaction ID  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["transID"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Total Amount  ";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["exmAmt"].ToString() + "</td>";
                htmltext = htmltext + "</tr><tr>";

                htmltext = htmltext + "<td>";
                htmltext = htmltext + "Transaction Date :";
                htmltext = htmltext + "</td><td>" + dt.Rows[0]["tDate"].ToString() + "</td>";
                htmltext = htmltext + "</tr></table>";

                htmltext = htmltext + "<br />";

                string totalc = dt.Rows[0]["exmAmt"].ToString();
                string transID = dt.Rows[0]["transID"].ToString() + "A";
                string name = dt.Rows[0]["uname"].ToString();
                string address = dt.Rows[0]["address"].ToString();
                string city = dt.Rows[0]["city"].ToString();
                string email = dt.Rows[0]["uemail"].ToString();
                string Mobile = dt.Rows[0]["mobileNo"].ToString();
                string country = dt.Rows[0]["country"].ToString();
                string state = dt.Rows[0]["state"].ToString();

                htmltext = htmltext + string.Format("<input type=\"hidden\" name=\"merchant_id\" id=\"merchant_id\" value=\"99555\" /><input type=\"hidden\" name=\"order_id\" value=\"" + transID + "\" /><input type=\"hidden\" name=\"amount\" value=\"" + totalc + "\" /><input type=\"hidden\" name=\"currency\" value=\"INR\" /><input type=\"hidden\" name=\"redirect_url\" value=\"http://pvkexams.com/ccavResponseHandler.aspx\" /><input type=\"hidden\" name=\"cancel_url\" value=\"http://pvkexams.com/ccavCancel.aspx\" /><input type=\"hidden\" name=\"billing_name\" value=\"" + name + "\" /><input type=\"hidden\" name=\"billing_address\" value=\"" + address + "\" /><input type=\"hidden\" name=\"billing_city\" value=\"" + city + "\" /><input type=\"hidden\" name=\"billing_state\" value=\"" + state + "\" /><input type=\"hidden\" name=\"billing_zip\" value=\"425001\" /><input type=\"hidden\" name=\"billing_country\" value=\"" + country + "\" /><input type=\"hidden\" name=\"billing_tel\" value=\"" + Mobile + "\" /><input type=\"hidden\" name=\"billing_email\" value=\"" + email + "\" /><input type=\"hidden\" name=\"delivery_name\" value=\"" + name + "\" /><input type=\"hidden\" name=\"delivery_address\" value=\"" + address + "\" /><input type=\"hidden\" name=\"delivery_city\" value=\"" + city + "\" /><input type=\"hidden\" name=\"delivery_state\" value=\"" + state + "\" /><input type=\"hidden\" name=\"delivery_zip\" value=\"444444\" /><input type=\"hidden\" name=\"delivery_country\" value=\"" + country + "\" /><input type=\"hidden\" name=\"delivery_tel\" value=\"" + Mobile + "\" /><input type=\"hidden\" name=\"merchant_param1\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param2\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param3\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param4\" value=\"additional Info.\" /><input type=\"hidden\" name=\"merchant_param5\" value=\"additional Info.\" /><input type=\"hidden\" name=\"promo_code\" /><input type=\"hidden\" name=\"customer_identifier\" /><input type=\"submit\" value=\"Process Payment\" class=\"simplebtn\" style=\"font-size: 15px; font-weight: bold; width: 150px; height: 40px;\" />");
            }
            catch (Exception)
            {
                return "<h3> Error while processing payment request!!!</h3>";
            }
            return htmltext;
        }

        public int updateTransStatus(string transId)
        {
            string strSql = "Update exm_paidExmUsers set tStatus='Completed' where transId=@transId";
            SqlParameter[] para = { 
                                    new SqlParameter("transId", transId)
                                  };
            return obj.Update(strSql, para);
        }

        public int AddNewInstitute(string Institute)
        {
            string strSql = "INSERT INTO exam_Institute (Institute, isActive) VALUES (@Institute, 'true')";
            SqlParameter[] para ={
                                     new SqlParameter("Institute", Institute)
                                };
            return obj.Insert(strSql, para);
        }

        public int UpdateInstitute(string Institute, string isActive, string InstituteId)
        {
            string strSql = "UPDATE exam_Institute SET Institute = @Institute, isActive=@isActive WHERE (InstituteId=@InstituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("Institute",Institute),
                                     new SqlParameter("isActive",isActive),
                                     new SqlParameter("InstituteId",InstituteId)
                                };
            return obj.Update(strSql, para);
        }

        public int DeleteInstitute(string InstituteId)
        {
            string strSql = "DELETE FROM exam_Institute WHERE (InstituteId=@InstituteId)";
            SqlParameter[] para ={
                                     new SqlParameter("InstituteId",InstituteId)
                                };
            return obj.Delete(strSql, para);
        }

        public DataTable getUserInstituteByEmail(string uemail, string password)
        {
            //string strSql = "SELECT uid, uemail,instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail and IsActive='True'";
            string strSql = "SELECT uid, uemail, instituteId, institute as instituteName from view_userByInstitute where uemail=@uemail and password=@password and isActive='true'";
            SqlParameter[] para = { 
                                    new SqlParameter("uemail",uemail),
                                    new SqlParameter("password",password)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getUserInstituteByEmailOnly(string uemail)
        {
            //string strSql = "SELECT uid, uemail,instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail and IsActive='True'";
            string strSql = "SELECT uid, uemail, instituteId, institute as instituteName from view_userByInstitute where uemail=@uemail and isActive='true'";
            SqlParameter[] para = { 
                                    new SqlParameter("uemail",uemail)
                                  };
            return obj.SelectMultiple(strSql, para);
        }

        public DataTable getInstituteByID(string instituteId)
        {
            string strSql = "SELECT InstituteId, institute from exam_Institute where InstituteId=@instituteId";
            SqlParameter[] para = { 
                                new SqlParameter("instituteId",instituteId)
                              };
            return obj.SelectMultiple(strSql, para);
        }

    }
}
