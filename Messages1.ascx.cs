﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Drona_Store_Module
{
    public partial class Message : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void setMessage(int type, string msg)
        {

            message1.Text = msg;
            if (type == 1)
            {

                errorimage.ImageUrl = "~/images/okgreen.png";
                mypanel.CssClass = "myMessages";
                
                
                //message.CssClass = "myMessages";

            }

            else
            {

                errorimage.ImageUrl = "~/images/error.png";
                mypanel.CssClass = "myError";
                //message.CssClass = "myError";
            }
        }
    }
}