﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ExmPayConfirm : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    messages11.Visible = false;
                    //if (Session["transID"] != null)
                    //{
                    string transId = "6"; //Session["transID"].ToString();
                        string htmltext = ob.returnHTML(transId);
                        Label3.Text = htmltext;
                    //}
                    //else
                    //{
                    //    Response.Redirect("/testsList.aspx", false);
                    //}
                }
            }
            catch (Exception ex)
            {
                messages11.Visible = true;
                messages11.setMessage(0, ex.Message);
            }
        }
    }
}
