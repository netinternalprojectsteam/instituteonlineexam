﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class paidExamDetails : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Messages11.Visible = false;
                pnlShow.Visible = true;
                pnlAdd.Visible = false;

                string[] userDetails = HttpContext.Current.User.Identity.Name.Split(',');
                if (Session["InstituteID"] != null)
                {
                    lblInst.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInst.Text = userDetails[3];
                }
                DataTable dtPaidExams = new DataTable();
                //if (userDetails[1].ToLower() == "super admin")
                //{
                //    dtPaidExams = ob.getPaidExams();
                //}
                //else
                //{
                dtPaidExams = ob.getPaidExamsByInstitute(lblInst.Text);
                // }
                if (dtPaidExams.Rows.Count > 0)
                {
                    ddlPaidExams.DataSource = ddlExams.DataSource = dtPaidExams;
                    ddlPaidExams.DataBind();
                    ddlExams.DataBind();

                    DataTable dt = new DataTable();
                    dt = ob.getPaidExamDetailsByExamID(ddlExams.SelectedValue);
                    gridExam.DataSource = dt;
                    gridExam.DataBind();
                }
            }
        }

        protected void ddlExams_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                DataTable dt = new DataTable();
                dt = ob.getPaidExamDetailsByExamID(ddlExams.SelectedValue);
                gridExam.DataSource = dt;
                gridExam.DataBind();

            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void btnnew_Click(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                pnlShow.Visible = btnUpdate.Visible = false;
                pnlAdd.Visible = btnSave.Visible = btnCancel.Visible = true;
                txtfees.Text = txtAttempt.Text = "";
                ddlExams.SelectedValue = ddlPaidExams.SelectedValue;
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                DataTable dtExists = new DataTable();
                dtExists = ob.checkPaidExamForInsert(ddlExams.SelectedValue, txtfees.Text, txtAttempt.Text);
                if (dtExists.Rows.Count == 0)
                {
                    ob.insertPaidExmDetails(ddlPaidExams.SelectedValue, txtfees.Text, txtAttempt.Text);
                    lblExamID.Text = ddlPaidExams.SelectedValue;

                    ddlExams.SelectedValue = lblExamID.Text;

                    DataTable dt = new DataTable();
                    dt = ob.getPaidExamDetailsByExamID(ddlExams.SelectedValue);
                    gridExam.DataSource = dt;
                    gridExam.DataBind();

                    pnlAdd.Visible = false;
                    pnlShow.Visible = true;

                    Messages11.setMessage(1, "Paid Exam Details Saved Successfully!");
                    Messages11.Visible = true;
                }
                else
                {
                    Messages11.setMessage(0, "Record already exists!");
                    Messages11.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                DataTable dtExists = new DataTable();
                dtExists = ob.checkPaidExamForUpdate(lblID.Text, ddlPaidExams.SelectedValue, txtfees.Text, txtAttempt.Text);
                if (dtExists.Rows.Count == 0)
                {
                    ob.updatePaidExmDetails(lblID.Text, ddlPaidExams.SelectedValue, txtfees.Text, txtAttempt.Text);
                    pnlAdd.Visible = false;
                    pnlShow.Visible = true;
                    Messages11.setMessage(1, "Paid Exam Details Updated Successfully!");
                    Messages11.Visible = true;

                    ddlExams.SelectedValue = lblExamID.Text;

                    DataTable dt = new DataTable();
                    dt = ob.getPaidExamDetailsByExamID(ddlExams.SelectedValue);
                    gridExam.DataSource = dt;
                    gridExam.DataBind();

                    pnlAdd.Visible = false;
                    pnlShow.Visible = true;

                    Messages11.setMessage(1, "Paid Exam Details updated Successfully!");
                    Messages11.Visible = true;
                }
                else
                {
                    Messages11.setMessage(0, "Record already Exists!");
                    Messages11.Visible = true;
                }

            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                pnlShow.Visible = true;
                pnlAdd.Visible = false;
                txtfees.Text = txtAttempt.Text = "";
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void gridExam_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[4].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[4].Visible = false;
            }
        }

        protected void gridExam_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                int row = 0;
                if (e.CommandName == "change")
                {
                    row = Convert.ToInt32(e.CommandArgument);
                    lblID.Text = gridExam.Rows[row].Cells[0].Text;
                    lblExamID.Text = gridExam.Rows[row].Cells[2].Text;
                    DataTable dt = ob.getData("SELECT ID, examID, examName, examFees, allowedAttempt FROM view_paidExams where ID = " + lblID.Text + "");

                    ddlPaidExams.SelectedValue = lblExamID.Text;
                    txtfees.Text = dt.Rows[0]["examFees"].ToString();
                    txtAttempt.Text = dt.Rows[0]["allowedAttempt"].ToString();
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    btnCancel.Visible = true;
                    pnlShow.Visible = false;
                    pnlAdd.Visible = true;
                }
                if (e.CommandName == "remove")
                {
                    row = Convert.ToInt32(e.CommandArgument);
                    lblExamID.Text = gridExam.Rows[row].Cells[2].Text;
                    DataTable dt1 = new DataTable();
                    dt1 = ob.getData("SELECT * from exm_newExam where preexamid=" + lblExamID.Text + "");
                    if (dt1.Rows.Count == 0)
                    {
                        lblerror.Visible = false;
                        //row = Convert.ToInt32(e.CommandArgument);
                        lblID.Text = gridExam.Rows[row].Cells[0].Text;
                        ob.deletePaidExmDetails(lblID.Text);

                        DataTable dt = new DataTable();
                        dt = ob.getPaidExamDetailsByExamID(ddlExams.SelectedValue);
                        gridExam.DataSource = dt;
                        gridExam.DataBind();

                        pnlAdd.Visible = false;
                        pnlShow.Visible = true;
                        Messages11.setMessage(1, "Paid Exam Details deleted successfully!");
                        Messages11.Visible = true;
                    }
                    else
                    {
                        Messages11.setMessage(0, "Can not delete details!");
                        Messages11.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }
    }
}
