﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Net;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace OnlineExam
{
    public partial class exmResult : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] userVal = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                DataTable dtChartData = (DataTable)ViewState["chartData"];
                queDetailsChart.DataSource = dtChartData;
                queDetailsChart.DataBind();
                accuracyChart.DataSource = dtChartData;
                accuracyChart.DataBind();

                Chart1.DataSource = dtChartData;
                Chart1.DataBind();
            }
            catch { }


            if (!IsPostBack)
            {
                try
                {
                    if (Session["InstituteID"] != null)
                    {
                        lblInstituteId.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblInstituteId.Text = userVal[3];
                    }

                    lblExamID.Text = Session["examID"].ToString();
                    lblUserID.Text = Session["userID"].ToString();

                    DataTable dtUserDetails = new DataTable();
                    dtUserDetails = ob.getData("SELECT uname, uemail, password, mobileNo, imageName FROM exam_users WHERE (uid = " + lblUserID.Text + ")");
                    lblName.Text = dtUserDetails.Rows[0]["uname"].ToString();

                    //lblExamID.Text = "85";
                    //lblUserID.Text = "3";
                    DataTable questions = new DataTable();
                    questions.Columns.Add("questid", typeof(string));
                    questions.Columns.Add("question", typeof(string));
                    questions.Columns.Add("option1", typeof(string));
                    questions.Columns.Add("option2", typeof(string));
                    questions.Columns.Add("option3", typeof(string));
                    questions.Columns.Add("option4", typeof(string));
                    questions.Columns.Add("option5", typeof(string));
                    questions.Columns.Add("answer", typeof(string));
                    questions.Columns.Add("serialnumber", typeof(string));
                    questions.Columns.Add("CorectAns", typeof(string));
                    questions.Columns.Add("yourAns", typeof(string));
                    questions.Columns.Add("IsCorrect", typeof(string));
                    questions.Columns.Add("Answer1", typeof(string));
                    questions.Columns.Add("timeSpent", typeof(string));
                    questions.Columns.Add("questionNo", typeof(string));
                    questions.Columns.Add("mark", typeof(string));
                    questions.Columns.Add("negativeDeduction", typeof(string));
                    questions.Columns.Add("timespentinSecond", typeof(string));

                    DataTable dtGivenAns = new DataTable();
                    dtGivenAns = ob.getData("SELECT userid, exmid, questno, answer,CASE WHEN answer = '1' THEN (SELECT option1 FROM exm_questions WHERE questid = questno) WHEN answer = '2' THEN (SELECT option2 FROM exm_questions WHERE questid = questno) WHEN answer = '3' THEN (SELECT option3 FROM exm_questions WHERE questid = questno) WHEN answer = '4' THEN (SELECT option4 FROM exm_questions WHERE questid = questno) WHEN answer = '5' THEN (SELECT option5 FROM exm_questions WHERE questid = questno) WHEN answer = '0' THEN 'Not Solved' END AS Answer1,case when timespent > 60 then  convert(varchar(300), timespent/60) + ' min ' + convert(varchar(300), timespent%60)+' sec' else  convert(varchar(300), timespent) + ' sec' end as timeSpent, timespent as timespentinSecond FROM exm_userAnswers where exmid = " + lblExamID.Text + " and userid = " + lblUserID.Text + "");

                    DataTable dtQueDetails = new DataTable();
                    for (int i = 0; i < dtGivenAns.Rows.Count; i++)
                    {
                        DataRow dtrow = questions.NewRow();
                        dtQueDetails = ob.getData("SELECT questid, question, option1, option2, option3, option4,option5, answer, case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '4' then exm_questions.option4  when exm_questions.answer = '5' then exm_questions.option5 end as CorectAns,mark, negativeDeduction FROM exm_questions where questid = " + dtGivenAns.Rows[i]["questno"].ToString() + "");
                        dtrow["questid"] = dtQueDetails.Rows[0]["questid"].ToString();
                        dtrow["question"] = dtQueDetails.Rows[0]["question"].ToString();
                        dtrow["option1"] = dtQueDetails.Rows[0]["option1"].ToString();
                        dtrow["option2"] = dtQueDetails.Rows[0]["option2"].ToString();
                        dtrow["option3"] = dtQueDetails.Rows[0]["option3"].ToString();
                        dtrow["option4"] = dtQueDetails.Rows[0]["option4"].ToString();
                        dtrow["option5"] = dtQueDetails.Rows[0]["option5"].ToString();
                        dtrow["answer"] = dtQueDetails.Rows[0]["answer"].ToString();
                        dtrow["serialnumber"] = (i + 1).ToString();
                        dtrow["CorectAns"] = dtQueDetails.Rows[0]["CorectAns"].ToString();
                        dtrow["yourAns"] = dtGivenAns.Rows[i]["answer"].ToString();
                        dtrow["timeSpent"] = dtGivenAns.Rows[i]["timeSpent"].ToString();
                        dtrow["timespentinSecond"] = dtGivenAns.Rows[i]["timespentinSecond"].ToString();

                        if (dtQueDetails.Rows[0]["answer"].ToString() == dtGivenAns.Rows[i]["answer"].ToString())
                        {
                            dtrow["IsCorrect"] = "Correct";
                        }
                        else
                        {
                            dtrow["IsCorrect"] = "InCorrect";
                        }
                        if (dtGivenAns.Rows[i]["answer"].ToString() == "0")
                        {
                            dtrow["IsCorrect"] = "Not Solved";
                        }
                        dtrow["Answer1"] = dtGivenAns.Rows[i]["Answer1"].ToString();
                        dtrow["questionNo"] = "Question " + (i + 1).ToString();
                        dtrow["mark"] = dtQueDetails.Rows[0]["mark"].ToString();
                        dtrow["negativeDeduction"] = dtQueDetails.Rows[0]["negativeDeduction"].ToString();
                        questions.Rows.Add(dtrow);
                    }

                    GridView1.DataSource = questions;
                    GridView1.DataBind();

                    gridQueDetails.DataSource = questions;
                    gridQueDetails.DataBind();
                    string IsSolved = "No";
                    for (int i = 0; i < gridQueDetails.Rows.Count; i++)
                    {
                        if (gridQueDetails.Rows[i].Cells[1].Text != "Not Solved")
                        {
                            IsSolved = "Yes";
                        }
                    }
                    Repeater2.DataSource = questions;
                    Repeater2.DataBind();
                    DataTable dtExamDetails = ob.getData("Select preexamid from exm_newexam where newexm=" + lblExamID.Text + " union all select negativemarking from exm_existingexams where examid = (Select preexamid from exm_newexam where newexm=" + lblExamID.Text + ")");

                    DataTable dtexamInfo = new DataTable();
                    dtexamInfo = ob.getData("SELECT examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, negativeMarking, section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM  exm_existingExams WHERE (examID = " + dtExamDetails.Rows[0][0].ToString() + ")");
                    lblExamName.Text = "Exam Name : " + dtexamInfo.Rows[0]["examName"].ToString();

                    double marks = 0;
                    int correct = 0;
                    int incorrect = 0;
                    int totalTimeSpent = 0;

                    for (int item = 0; item < Repeater2.Items.Count; item++)
                    {
                        Label lbl = Repeater2.Items[item].FindControl("lblInfo") as Label;
                        //CheckBox box = Repeater2.Items[item].FindControl("CheckBoxID") as CheckBox;
                        if (lbl.Text == "Correct")
                        {
                            Button btn = Repeater2.Items[item].FindControl("Button1") as Button;
                            btn.BackColor = System.Drawing.Color.Green;
                            btn.ForeColor = System.Drawing.Color.White;
                            btn.Attributes.Add("OnClick", "btnRep_Click");
                            btn.ToolTip = "Correct";
                            correct++;
                            marks = marks + Convert.ToDouble(GridView1.Rows[item].Cells[15].Text);
                            totalTimeSpent = totalTimeSpent + Convert.ToInt32(GridView1.Rows[item].Cells[17].Text);
                        }
                        if (lbl.Text == "InCorrect")
                        {
                            Button btn = Repeater2.Items[item].FindControl("Button1") as Button;
                            btn.BackColor = System.Drawing.Color.Red;
                            btn.ForeColor = System.Drawing.Color.White;
                            btn.Attributes.Add("OnClick", "btnRep_Click");
                            btn.ToolTip = "Incorrect";
                            incorrect++;
                            totalTimeSpent = totalTimeSpent + Convert.ToInt32(GridView1.Rows[item].Cells[17].Text);
                            if (dtExamDetails.Rows[1][0].ToString() == "true" || dtExamDetails.Rows[1][0].ToString() == "1")
                            {
                                marks = marks - Convert.ToDouble(GridView1.Rows[item].Cells[16].Text);
                            }
                        }
                        if (lbl.Text == "Not Solved")
                        {
                            Button btn = Repeater2.Items[item].FindControl("Button1") as Button;
                            btn.Attributes.Add("OnClick", "btnRep_Click");
                            btn.ToolTip = "Not Attempted";
                            totalTimeSpent = totalTimeSpent + Convert.ToInt32(GridView1.Rows[item].Cells[17].Text);
                        }
                    }

                    string timeTotal = "";
                    if (totalTimeSpent > 3600)
                    {
                        timeTotal = totalTimeSpent / 3600 + "Hrs";
                        int timespent1 = totalTimeSpent - 3600;
                        if (timespent1 > 60)
                        {
                            timeTotal = timeTotal + (timespent1 / 60).ToString() + " Mins  " + (timespent1 % 60).ToString() + " Sec";
                        }
                    }
                    if (totalTimeSpent > 60 && totalTimeSpent < 3600)
                    {
                        timeTotal = timeTotal + (totalTimeSpent / 60).ToString() + " Mins  " + (totalTimeSpent % 60).ToString() + " Sec";
                    }
                    if (totalTimeSpent < 60)
                    {
                        timeTotal = totalTimeSpent.ToString() + " Sec";
                    }

                    lblTotalQue.Text = GridView1.Rows.Count.ToString();
                    lblCorrect.Text = correct.ToString();
                    lblIncorrect.Text = incorrect.ToString();
                    lblSolvedQuestions.Text = (correct + incorrect).ToString();
                    lblTMarks.Text = marks.ToString();
                    lblTimeSpent.Text = timeTotal;
                    //string preexamID = ob.getValue("Select preexamid from exm_newexam where newexm=" + lblExamID.Text + "");
                    DataTable dtExamResult = new DataTable();
                    dtExamResult = ob.getData("Select * from exam_userResult where userid = " + lblUserID.Text + " and examid = " + lblExamID.Text + "");
                    if (dtExamResult.Rows.Count == 0)
                    {
                        ob.insertUserResult(lblUserID.Text, lblExamID.Text, lblTMarks.Text, lblTotalQue.Text, lblSolvedQuestions.Text, lblCorrect.Text, dtExamDetails.Rows[0][0].ToString());
                        string id = ob.getValue("select max(id) from exam_userResult");
                        DataTable dtDetails = ob.getData("select count(*) from exam_userresult where preexamid= " + dtExamDetails.Rows[0][0].ToString() + " union all select count(id) from exam_userresult where totalmarks <= " + lblTMarks.Text + " and preexamid=" + dtExamDetails.Rows[0][0].ToString() + " and id != " + id + " ");
                        double percentile = 0;
                        string s1 = dtDetails.Rows[1][0].ToString();
                        string s2 = dtDetails.Rows[0][0].ToString();

                        if (IsSolved == "No") { percentile = 0; }
                        else
                        {
                            percentile = (Convert.ToDouble(s1) / Convert.ToDouble(s2)) * 100;
                        }
                        lblPercentile.Text = string.Format("{0:n2}", percentile);
                        Label8.Text = "Total user : " + s2 + "  and <= user = " + s1 + "";
                        double accuracy = 0;
                        try
                        {
                            if (Convert.ToInt16(lblCorrect.Text) > 0 && Convert.ToInt16(lblSolvedQuestions.Text) > 0)
                            {
                                accuracy = (Convert.ToDouble(lblCorrect.Text) / Convert.ToDouble(lblSolvedQuestions.Text)) * 100;
                                lblAccuracy.Text = string.Format("{0:n2}", accuracy);
                            }
                            else { lblAccuracy.Text = "0.0"; accuracy = 0; }
                        }
                        catch { accuracy = 0; }
                        ob.updateUserPercentile(id, percentile, accuracy, totalTimeSpent);


                        //to generate section wise details
                        try
                        {
                            if (Convert.ToInt32(dtexamInfo.Rows[0]["noOfSections"].ToString()) > 1)
                            {

                                DataTable dtSectionwiseDetails = new DataTable();
                                dtSectionwiseDetails.Columns.Add("sectionNo", typeof(string));
                                dtSectionwiseDetails.Columns.Add("totalQue", typeof(string));
                                dtSectionwiseDetails.Columns.Add("solvedQue", typeof(string));
                                dtSectionwiseDetails.Columns.Add("correctQue", typeof(string));
                                dtSectionwiseDetails.Columns.Add("incorrectQue", typeof(string));
                                dtSectionwiseDetails.Columns.Add("marks", typeof(string));
                                dtSectionwiseDetails.Columns.Add("accuracy", typeof(string));
                                dtSectionwiseDetails.Columns.Add("percentile", typeof(string));
                                dtSectionwiseDetails.Columns.Add("sessionTime", typeof(string));
                                dtSectionwiseDetails.Columns.Add("unSolved", typeof(string));
                                dtSectionwiseDetails.Columns.Add("topper1", typeof(string));
                                dtSectionwiseDetails.Columns.Add("topper2", typeof(string));

                                int questionInSection = 0;// Convert.ToInt32(lblTotalQue.Text) / Convert.ToInt32(dtexamInfo.Rows[0]["noOfSections"].ToString());

                                int sectionCorrect = 0; int sectionIncorrect = 0; int sectionNotSolved = 0;
                                double sectionMarks = 0; int totalSectionQues = 0;

                                int queSum = 0;
                                int ques = 0;

                                for (int x = 1; x <= Convert.ToInt32(dtexamInfo.Rows[0]["noOfSections"].ToString()); x++)
                                {
                                    try
                                    {
                                        questionInSection = 0;
                                        ques = 0;
                                        int fromQue = 0; int toQue = 0;
                                        if (x == 1)//for section 1
                                        {
                                            string section1Ques = dtexamInfo.Rows[0]["section1Ques"].ToString();
                                            string[] section1QuesNo = section1Ques.Split(',');
                                            for (int i = 0; i < section1QuesNo.Length; i++)
                                            {
                                                if (section1QuesNo[i].ToString().Length > 0)
                                                { ques = ques + 1; }

                                            }
                                            queSum = ques;
                                            fromQue = 0; toQue = ques;
                                            questionInSection = section1QuesNo.Length;
                                        }
                                        if (x == 2)//for section 2
                                        {
                                            string section2Ques = dtexamInfo.Rows[0]["section2Ques"].ToString();
                                            string[] section2QuesNo = section2Ques.Split(',');
                                            for (int i = 0; i < section2QuesNo.Length; i++)
                                            {
                                                if (section2QuesNo[i].ToString().Length > 0)
                                                { ques = ques + 1; }

                                            }

                                            fromQue = queSum;
                                            toQue = queSum + ques; queSum = queSum + ques;
                                            questionInSection = section2QuesNo.Length;
                                        }
                                        if (x == 3)//for section 3
                                        {
                                            string section3Ques = dtexamInfo.Rows[0]["section3Ques"].ToString();
                                            string[] section3QuesNo = section3Ques.Split(',');
                                            for (int i = 0; i < section3QuesNo.Length; i++)
                                            {
                                                if (section3QuesNo[i].ToString().Length > 0)
                                                { ques = ques + 1; }

                                            }

                                            fromQue = queSum; toQue = queSum + ques; queSum = queSum + ques;
                                            questionInSection = section3QuesNo.Length;
                                        }
                                        if (x == 4)//for section 4
                                        {
                                            string section4Ques = dtexamInfo.Rows[0]["section4Ques"].ToString();
                                            string[] section4QuesNo = section4Ques.Split(',');
                                            for (int i = 0; i < section4QuesNo.Length; i++)
                                            {
                                                if (section4QuesNo[i].ToString().Length > 0)
                                                { ques = ques + 1; }

                                            }

                                            fromQue = queSum; toQue = queSum + ques; queSum = queSum + ques;
                                            questionInSection = section4QuesNo.Length;
                                        }
                                        if (x == 5)//for section 5
                                        {
                                            string section5Ques = dtexamInfo.Rows[0]["section5Ques"].ToString();
                                            string[] section5QuesNo = section5Ques.Split(',');
                                            for (int i = 0; i < section5QuesNo.Length; i++)
                                            {
                                                if (section5QuesNo[i].ToString().Length > 0)
                                                { ques = ques + 1; }

                                            }

                                            fromQue = queSum; toQue = queSum + ques;
                                            queSum = queSum + ques;
                                            questionInSection = section5QuesNo.Length;
                                        }

                                        sectionCorrect = 0; sectionIncorrect = 0; sectionNotSolved = 0;
                                        sectionMarks = 0; int timespent = 0; totalSectionQues = 0;

                                        for (int z = fromQue; z < toQue; z++)
                                        {

                                            //totalSectionQues = totalSectionQues + 1;
                                            Label lbl = Repeater2.Items[z].FindControl("lblInfo") as Label;
                                            if (lbl.Text == "Correct")
                                            {
                                                string abcd = GridView1.Rows[z].Cells[17].Text;
                                                timespent = timespent + Convert.ToInt32(GridView1.Rows[z].Cells[17].Text);
                                                sectionCorrect++;
                                                sectionMarks = sectionMarks + Convert.ToDouble(GridView1.Rows[z].Cells[15].Text);
                                            }
                                            if (lbl.Text == "InCorrect")
                                            {
                                                string abcd = GridView1.Rows[z].Cells[17].Text;
                                                timespent = timespent + Convert.ToInt32(GridView1.Rows[z].Cells[17].Text);
                                                sectionIncorrect++;
                                                if (dtExamDetails.Rows[1][0].ToString() == "true" || dtExamDetails.Rows[1][0].ToString() == "1")
                                                {
                                                    sectionMarks = sectionMarks - Convert.ToDouble(GridView1.Rows[z].Cells[16].Text);
                                                }
                                            }
                                            if (lbl.Text == "Not Solved")
                                            {
                                                string abcd = GridView1.Rows[z].Cells[17].Text;
                                                timespent = timespent + Convert.ToInt32(GridView1.Rows[z].Cells[17].Text);
                                                sectionNotSolved = sectionNotSolved + 1;
                                            }

                                        }

                                        ob.insertSectionwiseDetails(lblExamID.Text, lblUserID.Text, x, questionInSection, sectionCorrect + sectionIncorrect, sectionCorrect, sectionIncorrect, sectionMarks, dtExamDetails.Rows[0][0].ToString());

                                        string id1 = ob.getValue("select max(id) from exam_sectionWiseDetails");
                                        DataTable dtEDetails = ob.getData("select count(*) from exam_sectionWiseDetails where sectionNo=" + x + " and preExamID=" + dtExamDetails.Rows[0][0].ToString() + " union all select count(id) from exam_sectionWiseDetails where marks <= " + sectionMarks + " and sectionNo=" + x + " and preExamID=" + dtExamDetails.Rows[0][0].ToString() + " and id != " + id1 + " ");
                                        string s12 = dtEDetails.Rows[1][0].ToString();
                                        string s13 = dtEDetails.Rows[0][0].ToString();
                                        double percentile1 = 0; double accuracy1 = 0;

                                        if (questionInSection == sectionNotSolved)
                                        {
                                            percentile1 = 0;
                                            accuracy1 = 0;
                                        }
                                        else
                                        {
                                            percentile1 = (Convert.ToDouble(s12) / Convert.ToDouble(s13)) * 100;
                                            accuracy1 = (Convert.ToDouble(sectionCorrect) / Convert.ToDouble(sectionCorrect + sectionIncorrect)) * 100;
                                        }

                                        string sessionTime = "";
                                        if (timespent > 3600)
                                        {
                                            sessionTime = timespent / 3600 + "Hrs";
                                            int timespent1 = timespent - 3600;
                                            if (timespent1 > 60)
                                            {
                                                sessionTime = sessionTime + (timespent1 / 60).ToString() + " Mins  " + (timespent1 % 60).ToString() + " Sec";
                                            }
                                        }
                                        if (timespent > 60 && timespent < 3600)
                                        {
                                            sessionTime = sessionTime + (timespent / 60).ToString() + " Mins  " + (timespent % 60).ToString() + " Sec";
                                        }
                                        if (timespent < 60)
                                        {
                                            sessionTime = timespent.ToString() + " Sec";
                                        }
                                        try
                                        {
                                            ob.updateSectionwiseDetails(id1, percentile1, accuracy1, Convert.ToInt32(timespent));
                                        }
                                        catch
                                        {
                                            ob.updateSectionwiseDetails(id1, percentile1, 0, Convert.ToInt32(timespent));
                                        }
                                        DataTable dtsectiondetails = new DataTable();
                                        dtsectiondetails = ob.getData("select id,examid,isnull((select max(percentile) from exam_sectionwisedetails where sectionno=" + x + " and preexamid=" + dtexamInfo.Rows[0]["examID"].ToString() + " ),0) as toper1,isnull((select max(percentile) from exam_sectionwisedetails where sectionno= " + x + " and preexamid=" + dtexamInfo.Rows[0]["examID"].ToString() + " and percentile<(select max(percentile) from exam_sectionwisedetails where sectionno=" + x + " and preexamid=" + dtexamInfo.Rows[0]["examID"].ToString() + " )),0) as toper2 from exam_sectionwisedetails where examid=" + lblExamID.Text + " and userid=" + lblUserID.Text + "");

                                        DataRow dtrow = dtSectionwiseDetails.NewRow();
                                        dtrow["sectionNo"] = "Section " + x.ToString();
                                        dtrow["totalQue"] = questionInSection.ToString();
                                        dtrow["solvedQue"] = (sectionCorrect + sectionIncorrect).ToString();
                                        dtrow["correctQue"] = sectionCorrect.ToString();
                                        dtrow["incorrectQue"] = sectionIncorrect.ToString();
                                        dtrow["marks"] = sectionMarks.ToString();
                                        dtrow["accuracy"] = string.Format("{0:n2}", accuracy1);//accuracy1.ToString();
                                        dtrow["percentile"] = string.Format("{0:n2}", percentile1); //percentile1.ToString();
                                        dtrow["sessionTime"] = sessionTime;
                                        dtrow["unSolved"] = (questionInSection - (correct + incorrect)).ToString();
                                        dtrow["topper1"] = dtsectiondetails.Rows[0]["toper1"].ToString();
                                        dtrow["topper2"] = dtsectiondetails.Rows[0]["toper2"].ToString();
                                        dtSectionwiseDetails.Rows.Add(dtrow);
                                    }
                                    catch { }
                                }
                                try
                                {
                                    //to add total details to show in graph queDetailsChart (overall summary)
                                    DataTable dtUserOverallPer = ob.getData("select toper1,toper2,userpercentile from view_percentiledetails where newexm=" + lblExamID.Text + " ");

                                    DataRow dtrow1 = dtSectionwiseDetails.NewRow();
                                    dtrow1["sectionNo"] = "Overall";
                                    dtrow1["totalQue"] = lblTotalQue.Text;
                                    dtrow1["solvedQue"] = lblSolvedQuestions.Text;
                                    dtrow1["correctQue"] = lblCorrect.Text;
                                    dtrow1["incorrectQue"] = lblIncorrect.Text;
                                    dtrow1["marks"] = lblTMarks.Text;
                                    dtrow1["accuracy"] = lblAccuracy.Text;
                                    dtrow1["percentile"] = lblPercentile.Text;
                                    dtrow1["sessionTime"] = lblTimeSpent.Text;
                                    dtrow1["unSolved"] = (Convert.ToInt32(lblTotalQue.Text) - (Convert.ToInt32(lblCorrect.Text) + Convert.ToInt32(lblIncorrect.Text))).ToString();
                                    dtrow1["topper1"] = dtUserOverallPer.Rows[0]["toper1"].ToString();
                                    dtrow1["topper2"] = dtUserOverallPer.Rows[0]["toper2"].ToString();
                                    dtSectionwiseDetails.Rows.Add(dtrow1);

                                    GridView2.DataSource = dtSectionwiseDetails;
                                    GridView2.DataBind();

                                    repSectionWiseDetails.DataSource = dtSectionwiseDetails;
                                    repSectionWiseDetails.DataBind();
                                    ViewState["chartData"] = dtSectionwiseDetails;

                                    queDetailsChart.DataSource = dtSectionwiseDetails;
                                    queDetailsChart.DataBind();
                                    accuracyChart.DataSource = dtSectionwiseDetails;
                                    accuracyChart.DataBind();
                                    Chart1.DataSource = dtSectionwiseDetails;
                                    Chart1.DataBind();
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    AccordionPane2.Visible = false;
                                    DataTable dtUserOverallPer = ob.getData("select toper1,toper2,userpercentile from view_percentiledetails where newexm=" + lblExamID.Text + " ");
                                    DataTable dtSectionwiseDetails = new DataTable();
                                    dtSectionwiseDetails.Columns.Add("sectionNo", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("totalQue", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("solvedQue", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("correctQue", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("incorrectQue", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("marks", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("accuracy", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("percentile", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("sessionTime", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("unSolved", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("topper1", typeof(string));
                                    dtSectionwiseDetails.Columns.Add("topper2", typeof(string));


                                    //to add total details to show in graph queDetailsChart (overall summary)
                                    DataRow dtrow1 = dtSectionwiseDetails.NewRow();
                                    dtrow1["sectionNo"] = "Overall";
                                    dtrow1["totalQue"] = lblTotalQue.Text;
                                    dtrow1["solvedQue"] = lblSolvedQuestions.Text;
                                    dtrow1["correctQue"] = lblCorrect.Text;
                                    dtrow1["incorrectQue"] = lblIncorrect.Text;
                                    dtrow1["marks"] = lblTMarks.Text;
                                    dtrow1["accuracy"] = lblAccuracy.Text;
                                    dtrow1["percentile"] = lblPercentile.Text;
                                    dtrow1["sessionTime"] = lblTimeSpent.Text;
                                    dtrow1["unSolved"] = (Convert.ToInt32(lblTotalQue.Text) - (Convert.ToInt32(lblCorrect.Text) + Convert.ToInt32(lblIncorrect.Text))).ToString();
                                    dtrow1["topper1"] = dtUserOverallPer.Rows[0]["toper1"].ToString();
                                    dtrow1["topper2"] = dtUserOverallPer.Rows[0]["toper2"].ToString();
                                    dtSectionwiseDetails.Rows.Add(dtrow1);

                                    queDetailsChart.DataSource = dtSectionwiseDetails;
                                    queDetailsChart.DataBind();
                                    ViewState["chartData"] = dtSectionwiseDetails;
                                    accuracyChart.DataSource = dtSectionwiseDetails;
                                    accuracyChart.DataBind();


                                    Chart1.DataSource = dtSectionwiseDetails;
                                    Chart1.DataBind();
                                }
                                catch { }
                            }


                            StringBuilder SB = new StringBuilder();
                            StringWriter SW = new StringWriter(SB);
                            HtmlTextWriter htmlTW = new HtmlTextWriter(SW);

                            Panel2.RenderControl(htmlTW);
                            string messagebody = SW.ToString();

                            string mailBody = "Hello " + lblName.Text + "<br> Your score for <b>" + dtexamInfo.Rows[0]["examName"].ToString() + "</b> is as follows<br><br>";
                            mailBody = mailBody + messagebody;

                            //string resultEID = ConfigurationManager.AppSettings["ResultEmailID"].ToString();
                            //string resultPass = ConfigurationManager.AppSettings["ResultEmailPass"].ToString();

                            //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(resultEID, dtUserDetails.Rows[0]["uemail"].ToString(), "Result Details at pgfunda.com", mailBody);
                            //mm.IsBodyHtml = true;
                            //System.Net.Mail.SmtpClient client = new SmtpClient();
                            //client.Credentials = new System.Net.NetworkCredential(resultEID, resultPass);
                            //client.Send(mm);

                            string emailID = "";
                            string password = "";
                            string smtpHost = "";
                            string smtpPort = "";
                            bool smtpEnable;

                            DataTable dtEmail = new DataTable();
                            dtEmail = ob.getData("Select id, emailID, password, smtpHost, smtpPort, smtpEnable from email_Config where instituteId=" + lblInstituteId.Text + "");
                            if (dtEmail.Rows.Count > 0)
                            {
                                emailID = dtEmail.Rows[0]["emailID"].ToString();
                                password = base64Decode(dtEmail.Rows[0]["password"].ToString());
                                smtpHost = dtEmail.Rows[0]["smtpHost"].ToString();
                                smtpPort = dtEmail.Rows[0]["smtpPort"].ToString();
                                smtpEnable = Convert.ToBoolean(dtEmail.Rows[0]["smtpEnable"].ToString());
                            }
                            else
                            {
                                emailID = ConfigurationManager.AppSettings["InfoEmailID"];
                                password = ConfigurationManager.AppSettings["InfoEmailPass"];
                                smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                                smtpPort = ConfigurationManager.AppSettings["smtpPort"];
                                smtpEnable = true;
                            }


                            System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                            //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                            SmtpClient Sc = new SmtpClient(smtpHost);
                            //string port = ConfigurationManager.AppSettings["smtpPort"];
                            Sc.Port = Convert.ToInt32(smtpPort);
                            //string infoEID = ConfigurationManager.AppSettings["ResultEmailID"];
                            //string infoPass = ConfigurationManager.AppSettings["ResultEmailPass"];
                            Sc.Credentials = new NetworkCredential(emailID, password);
                            Sc.EnableSsl = smtpEnable;
                            ms.From = new MailAddress(emailID);
                            ms.To.Add(dtUserDetails.Rows[0]["uemail"].ToString());

                            ms.Subject = "Result Details at pvkexams.com";
                            ms.Body = mailBody;
                            ms.IsBodyHtml = true;
                            Sc.Send(ms);
                        }
                        catch { }

                    }
                    else
                    {
                        lblPercentile.Text = dtExamResult.Rows[0]["percentile"].ToString();
                        lblAccuracy.Text = dtExamResult.Rows[0]["accuracy"].ToString();

                        lblCorrect.Text = dtExamResult.Rows[0]["correctQue"].ToString();
                        lblIncorrect.Text = (Convert.ToInt16(dtExamResult.Rows[0]["solvedQue"].ToString()) - Convert.ToInt16(dtExamResult.Rows[0]["correctQue"].ToString())).ToString();
                        lblSolvedQuestions.Text = dtExamResult.Rows[0]["solvedQue"].ToString();
                        lblTMarks.Text = dtExamResult.Rows[0]["totalMarks"].ToString();
                        lblTimeSpent.Text = dtExamResult.Rows[0]["totalTimeSpent"].ToString();
                        //'section '+convert(varchar(20),sectionno)
                        DataTable dtsectionwiseDetails = ob.getData("SELECT  id, examID, userID,'Section '+ convert(varchar(200), sectionNo) as sectionNo, totalQue, solvedQue, correctQue, incorrectQue, marks, accuracy, percentile, preExamID, case when sessionTime > 60 then  convert(varchar(300), sessionTime/60) + ' Min ' + convert(varchar(300), sessionTime%60)+' sec' else  convert(varchar(300), sessionTime) + ' sec' end as sessionTime, totalQue-solvedQue as unSolved,'0' as topper1,'0' as topper2  FROM  exam_sectionWiseDetails where examID = " + lblExamID.Text + " and  userID = " + lblUserID.Text + "");
                        if (dtsectionwiseDetails.Rows.Count > 0)
                        {
                            repSectionWiseDetails.DataSource = dtsectionwiseDetails;
                            repSectionWiseDetails.DataBind();
                            AccordionPane2.Visible = true;

                        }
                        else { AccordionPane2.Visible = false; }


                        DataTable dtUserOverallPer = ob.getData("select toper1,toper2,userpercentile from view_percentiledetails where newexm=" + lblExamID.Text + " ");
                        //to add total details to show in graph (overall summary)
                        DataRow dtrow1 = dtsectionwiseDetails.NewRow();
                        dtrow1["sectionNo"] = "Overall";
                        dtrow1["totalQue"] = lblTotalQue.Text;
                        dtrow1["solvedQue"] = lblSolvedQuestions.Text;
                        dtrow1["correctQue"] = lblCorrect.Text;
                        dtrow1["incorrectQue"] = lblIncorrect.Text;
                        dtrow1["marks"] = lblTMarks.Text;
                        dtrow1["accuracy"] = lblAccuracy.Text;
                        dtrow1["percentile"] = lblPercentile.Text;
                        dtrow1["sessionTime"] = lblTimeSpent.Text;
                        dtrow1["unSolved"] = (Convert.ToInt32(lblTotalQue.Text) - (Convert.ToInt32(lblCorrect.Text) + Convert.ToInt32(lblIncorrect.Text))).ToString();
                        dtrow1["topper1"] = dtUserOverallPer.Rows[0]["toper1"].ToString();
                        dtrow1["topper2"] = dtUserOverallPer.Rows[0]["toper2"].ToString();
                        dtsectionwiseDetails.Rows.Add(dtrow1);

                        queDetailsChart.DataSource = dtsectionwiseDetails;
                        queDetailsChart.DataBind();

                        ViewState["chartData"] = dtsectionwiseDetails;
                        accuracyChart.DataSource = dtsectionwiseDetails;
                        accuracyChart.DataBind();

                        Chart1.DataSource = dtsectionwiseDetails;
                        Chart1.DataBind();
                    }
                    // to hide the overall section from repeater under sectionwise details but show to show it in queDetailsChart
                    for (int items = 0; items < repSectionWiseDetails.Items.Count; items++)
                    {
                        Label lbl = (Label)repSectionWiseDetails.Items[items].FindControl("Label16");
                        if (lbl.Text == "Overall")
                        {
                            repSectionWiseDetails.Items[items].Visible = false;
                        }
                    }
                }
                catch { }
            }
        }

        protected void btnRep_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            string qID = btn.CommandArgument;
            //SELECT userid, exmid, questno, answer,CASE WHEN answer = '1' THEN (SELECT option1 FROM exm_questions WHERE questid = questno) WHEN answer = '2' THEN (SELECT option2 FROM exm_questions WHERE questid = questno) WHEN answer = '3' THEN (SELECT option3 FROM exm_questions WHERE questid = questno) WHEN answer = '4' THEN (SELECT option4 FROM exm_questions WHERE questid = questno) WHEN answer = '0' THEN 'Not Solved' END AS Answer1 FROM exm_userAnswers where exmid = " + lblExamID.Text + " and userid = " + lblUserID.Text + "

            DataTable dtQueDetails = new DataTable();
            dtQueDetails = ob.getData("SELECT questid, question, option1, option2, option3, option4,option5 answer, case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '4' then exm_questions.option4 when exm_questions.answer = '5' then exm_questions.option5 end as CorectAns FROM exm_questions where questid = " + qID + "");

            DataTable dtGivenAns = new DataTable();
            dtGivenAns = ob.getData("SELECT userid, exmid, questno, answer,CASE WHEN answer = '1' THEN (SELECT option1 FROM exm_questions WHERE questid = questno) WHEN answer = '2' THEN (SELECT option2 FROM exm_questions WHERE questid = questno) WHEN answer = '3' THEN (SELECT option3 FROM exm_questions WHERE questid = questno) WHEN answer = '4' THEN (SELECT option4 FROM exm_questions WHERE questid = questno) WHEN answer = '5' THEN (SELECT option5 FROM exm_questions WHERE questid = questno)  WHEN answer = '0' THEN 'Not Solved' END AS Answer1 FROM exm_userAnswers where exmid = " + lblExamID.Text + " and userid = " + lblUserID.Text + " and questno = " + qID + "");

            DataTable dt = new DataTable();
            dt.Columns.Add("Question", typeof(string));
            dt.Columns.Add("correctAns", typeof(string));
            dt.Columns.Add("Answer", typeof(string));
            dt.Columns.Add("explanation", typeof(string));
            dt.Columns.Add("timeSpent", typeof(string));
            DataTable dt1 = ob.getData("SELECT questioninfo, questid FROM exm_questioninfo where questid = " + qID + "");

            DataRow dtrow = dt.NewRow();
            dtrow["Question"] = dtQueDetails.Rows[0]["question"].ToString();
            dtrow["correctAns"] = dtQueDetails.Rows[0]["CorectAns"].ToString(); ;//Answer1
            dtrow["Answer"] = dtGivenAns.Rows[0]["Answer1"].ToString();
            if (dt1.Rows.Count > 0)
            {
                dtrow["explanation"] = dt1.Rows[0]["questioninfo"].ToString();
            }
            else { dtrow["explanation"] = "-"; }


            //DataTable dtTimeSpent = ob.getData();

            string timeSpent = ob.getValue("Select case when timespent > 60 then 'Time Spent : ' + convert(varchar(300), timespent/60) + ' min ' + convert(varchar(300), timespent%60)+' sec' else 'Time Spent : '+ convert(varchar(300), timespent) + ' sec' end as timeSpent from exm_userAnswers where userid=" + lblUserID.Text + " and exmid=" + lblExamID.Text + " and questno = " + qID + "");

            dtrow["timeSpent"] = timeSpent;

            dt.Rows.Add(dtrow);

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            Label lbl = (Label)Repeater1.Items[0].FindControl("lblQuestionNo");
            lbl.Text = "Question " + btn.Text + "";

        }

        protected void Repeater2_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void linkQueAns_Click(object sender, EventArgs e)
        {
            Session["examIDToShow"] = lblExamID.Text;
            Session["userIDToShow"] = lblUserID.Text;
            modalShowQueAns.Show();
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }


    }
}
