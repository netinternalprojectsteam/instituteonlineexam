﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class syllabus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string id = Request.QueryString["ID"].ToString();

                    sourceSyllabus.SelectCommand = "SELECT sHeading, id, sDetails FROM site_syllabus WHERE (id = " + id + ")";
                    rptSyllabus.DataBind();
                    if (rptSyllabus.Items.Count == 0)
                    {
                        rptSyllabus.Visible = false;
                        lblMsg.Visible = true;

                        Label lbl =(Label) rptSyllabus.Items[0].FindControl("Label1");
                        this.Page.Title = lbl.Text;

                    }
                    else
                    {
                        rptSyllabus.Visible = true;
                        lblMsg.Visible = false;
                    }
                }
                catch
                {
                    rptSyllabus.Visible = false;
                    lblMsg.Visible = true;
                }
            }
        }
    }
}
