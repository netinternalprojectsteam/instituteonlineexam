﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchInfoContents.aspx.cs"
    Inherits="OnlineExam.searchInfoContents" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Information</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCanSearchInfo').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Search Informations
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
              <asp:Label ID="lblInsId" runat="server" Text="" Visible="false"></asp:Label>
                <%--<asp:Panel ID="Panel1" runat="server" Width="600px" Height="700px" ScrollBars="Auto">--%>
                <center>
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label1" runat="server" Text="Information Heading" CssClass="myLabel"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtInfoHeading" runat="server" CssClass="txtbox"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label2" runat="server" Text="Information" CssClass="myLabel"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtInfo" runat="server" CssClass="txtbox"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel3" runat="server">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="gridResult" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                        OnRowCommand="gridResult_RowCommand" OnSelectedIndexChanged="gridResult_SelectedIndexChanged"
                                        DataSourceID="soureceSearch" OnRowCreated="gridResult_RowCreated">
                                        <Columns>
                                            <asp:BoundField DataField="infID" HeaderText="infID" />
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %></ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="infHeading" HeaderText="Info Heading" />
                                            <asp:ButtonField ButtonType="Image" CommandName="details" HeaderText="Details" ImageUrl="~/images/Details.png"
                                                Text="Button">
                                                <ItemStyle Height="15px" HorizontalAlign="Center" Width="20px" />
                                            </asp:ButtonField>
                                            <asp:ButtonField ButtonType="Image" CommandName="select" HeaderText="Select" ImageUrl="~/images/apply2.png"
                                                Text="Button">
                                                <ItemStyle Height="15px" HorizontalAlign="Center" Width="20px" />
                                            </asp:ButtonField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="soureceSearch" runat="server"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:SqlDataSource ID="sourceInfo" runat="server"></asp:SqlDataSource>
                                    <asp:Repeater ID="repQueInfo" runat="server" DataSourceID="sourceInfo">
                                        <ItemTemplate>
                                            <asp:Panel ID="Panel1" runat="server" Height="250px" ScrollBars="Auto">
                                                <%--<table style="background-image: url(/images/bg.gif);">--%>
                                                <table style="background-color: White">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("infContents") %>' CssClass="myLabel1"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Button ID="Button1" runat="server" Text="Select" CommandArgument='<%#Eval("infID") %>'
                                                                OnClick="btnRep_Click" CssClass="simplebtn" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </center>
                <%-- </asp:Panel>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
