﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class adminMaster : System.Web.UI.MasterPage
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    DataTable dt = new DataTable();
                    if (HttpContext.Current.User.Identity.Name.ToString().Length > 0)
                    {
                        string[] val = HttpContext.Current.User.Identity.Name.ToString().Split(',');
                        if (val[2].ToString() == "NXGOnlineExam")
                        {
                            if (val[1].ToString() == "Admin" || val[1].ToString() == "Super Admin")
                            {
                                // Panel1.Visible = false;
                                linkLogout.Visible = true;
                                dt = ob.getData("SELECT username,name, institute FROM  view_adminDetails WHERE (userid = " + val[0].ToString() + ")");
                                lblUName.Text = dt.Rows[0]["name"].ToString();
                                if (Session["InstituteID"] != null)
                                {
                                    DataTable dtIns = new DataTable();
                                    dtIns = ob.getData("Select Institute from exam_institute where InstituteId=" + Session["InstituteID"].ToString() + "");
                                    lblInstitute.Text = dtIns.Rows[0]["Institute"].ToString();
                                }
                                else
                                {
                                    lblInstitute.Text = dt.Rows[0]["institute"].ToString();
                                }
                                sAdminPanel.Visible = false;
                                if (val[1].ToString() == "Super Admin")
                                {
                                    sAdminPanel.Visible = true;
                                }
                                else { sAdminPanel.Visible = false; }

                            }
                            if (val[1].ToString() == "User")
                            {
                                Response.Redirect("/Default.aspx");
                            }

                        }
                    }
                }

            }
            catch { }
        }
        protected void linkLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Remove("InstituteID");
            Response.Redirect("/Default.aspx");
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //if (txtName.Text.Length > 0 && txtMobile.Text.Length > 0 && txtMail.Text.Length > 0)
            //{
            //    ob.insertContactUs(txtName.Text, txtMail.Text, txtMobile.Text, txtQuery.Text);
            //    lblContactusMsg.Visible = true;
            //    lblContactusMsg.Text = "Your Details Saved Successfully !";
            //    lblContactusMsg.ForeColor = System.Drawing.Color.Green;
            //    txtMail.Text = "";
            //    txtMobile.Text = "";
            //    txtName.Text = "";
            //    txtQuery.Text = "";
            //}
            //else
            //{
            //    lblContactusMsg.Text = "Name,Email,Mobile Required !";
            //    lblContactusMsg.ForeColor = System.Drawing.Color.Red;
            //    lblContactusMsg.Visible = true;
            //}

        }
    }
}
