﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class viewAndPrintQuestion : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    try
                    {
                        lblExamID.Text = Session["examIDToShow"].ToString();
                        lblUserID.Text = Session["userIDToShow"].ToString();
                        Session.Remove("examIDToShow"); Session.Remove("userIDToShow");
                    }
                    catch { }

                    //lblExamID.Text = "557";
                    // lblUserID.Text = "58";
                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT uid, uname, exmid, questid, question, CorrectAns, yourAns, questioninfo, 'Que ' + Convert(varchar(200), row_number() over(order by questid)) as srNo FROM view_examQuestionsDetails WHERE (uid = " + lblUserID.Text + ") AND (exmid = " + lblExamID.Text + ")");
                    repQueWithAnswer.DataSource = dt;
                    repQueWithAnswer.DataBind();
                }
            }
            catch { }
        }
    }
}
