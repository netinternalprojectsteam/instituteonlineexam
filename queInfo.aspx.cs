﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class queInfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInstId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInstId.Text = val[3];
                }
                try
                {
                    ddlCourse.DataBind();
                    sourceSub.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                    sourceSub.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                    ddlSub.DataBind();
                }
                catch
                {
                    Response.Redirect("/ManageCourse.aspx");
                }
            }
        }

        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                sourceSub.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                sourceSub.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                ddlSub.DataBind();
                sourceQuestions.SelectCommand = "SELECT questid, question, option1, option2, option3, option4, option5, answer, difflevel, userid FROM view_examQuestions WHERE (subid = " + ddlSub.SelectedValue + ") and (instituteId=" + lblInstId.Text + ")";
                sourceQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                gridQuestions.DataBind();


            }
            catch { }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            sourceQuestions.SelectCommand = "SELECT questid, question, option1, option2, option3, option4, option5, answer, difflevel, userid FROM view_examQuestions WHERE (subid = " + ddlSub.SelectedValue + ") and (instituteId=" + lblInstId.Text + ")";
            sourceQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridQuestions.DataBind();
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4, option5, answer, difflevel, userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber] FROM view_examQuestions  WHERE (subid = " + ddlSub.SelectedValue + ") and (instituteId=" + lblInstId.Text + ") ORDER BY questid");
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            ViewState["queTable"] = dt;
            if (dt.Rows.Count == 0)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "No Questions Added!!!");
            }
            else
            {
                Messages11.Visible = false;
            }
        }

        protected void btnRep_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4, option5, case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 when answer=5 then option5 end as Answer, difflevel, userid FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInstId.Text + "");
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            string qID = lbtn.CommandArgument;
            Session["QID"] = qID;
            ModalPopupExtender2.Show();
        }
    }
}
