﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class testsList : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //DataTable dt = new DataTable();
                //dt = ob.getAllExams();

                //RepeaterTests.DataSource = dt;
                //RepeaterTests.DataBind();

                //rptComp.DataSource = dt;
                //rptComp.DataBind();

                //rptEnglish.DataSource = dt;
                //rptEnglish.DataBind();

                //rptGeneral.DataSource = dt;
                //rptGeneral.DataBind();

                //rptModal.DataSource = dt;
                //rptModal.DataBind();

                //rptNumerical.DataSource = dt;
                //rptNumerical.DataBind();

                //rptReasoning.DataSource = dt;
                //rptReasoning.DataBind();


                //string script = "<script type=\"text/javascript\">showAllExam()</script>";
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                try
                {
                    string[] val = HttpContext.Current.User.Identity.Name.Split(',');
                    if (val[1].ToString() == "User")
                    {
                        lblInstitute.Text = val[3].ToString();
                        ddlExams.DataBind();
                    }
                    else
                    {
                        Response.Redirect("/Default.aspx");
                    }
                }
                catch {
                    Response.Redirect("/Default.aspx");
                }
                
            }
        }


        protected void btnStart_Click(object sender, EventArgs e)
        {
            Response.Redirect("examInfo.aspx?eID=" + ddlExams.SelectedValue + "&etype=" + ddlType.Text);

        }

        //protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "Computer")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}





        //protected void rptEnglish_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "English")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}

        //protected void rptGeneral_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "General Awareness")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}

        //protected void rptModal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "Model Paper")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}

        //protected void rptReasoning_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "Reasoning")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}

        //protected void rptNumerical_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        string A = (string)((DataRowView)e.Item.DataItem).Row["coursrPackageID"];
        //        if (A == "Numerical")
        //        {
        //            e.Item.Visible = true;
        //        }
        //        else { e.Item.Visible = false; }
        //    }
        //}

        //protected void linkStartAllExam_Click(object sender, EventArgs e)
        //{

        //}
    }
}
