﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="bulkUpload.aspx.cs" Inherits="OnlineExam.bulkUpload" Title="Upload Questions" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript"> 
function openpopup(id){
      //Calculate Page width and height 
      var pageWidth = window.innerWidth; 
      var pageHeight = window.innerHeight; 
      if (typeof pageWidth != "number"){ 
      if (document.compatMode == "CSS1Compat"){ 
            pageWidth = document.documentElement.clientWidth; 
            pageHeight = document.documentElement.clientHeight; 
      } else { 
            pageWidth = document.body.clientWidth; 
            pageHeight = document.body.clientHeight; 
      } 
      } 
      //Make the background div tag visible...
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "visible"; 
        
      var divobj = document.getElementById(id); 
      divobj.style.visibility = "visible"; 
      if (navigator.appName=="Microsoft Internet Explorer") 
      computedStyle = divobj.currentStyle; 
      else computedStyle = document.defaultView.getComputedStyle(divobj, null); 
      //Get Div width and height from StyleSheet 
      var divWidth = computedStyle.width.replace('px', ''); 
      var divHeight = computedStyle.height.replace('px', ''); 
      var divLeft = (pageWidth - divWidth) / 2; 
      var divTop = (pageHeight - divHeight) / 2; 
      //Set Left and top coordinates for the div tag 
      divobj.style.left = divLeft + "px"; 
      divobj.style.top = divTop + "px"; 
      //Put a Close button for closing the popped up Div tag 
      if(divobj.innerHTML.indexOf("closepopup('" + id +"')") < 0 ) 
      divobj.innerHTML = "<a href=\"#\" onclick=\"closepopup('" + id +"')\"><span class=\"close_button\">X</span></a>" + divobj.innerHTML; 
}
function closepopup(id){
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "hidden"; 
      var divobj = document.getElementById(id); 
      divobj.style.visibility = "hidden"; 
}
    </script>

    <style type="text/css">
        .popup
        {
            background-color: #DDD;
            height: 350px;
            width: 810px;
            border: 5px solid #666;
            position: absolute;
            visibility: hidden;
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            text-align: justify;
            padding: 5px;
            overflow: auto;
            z-index: 20000;
        }
        .popup_bg
        {
            position: absolute;
            visibility: hidden;
            height: 100%;
            width: 100%;
            left: 0px;
            top: 0px;
            filter: alpha(opacity=70); /* for IE */
            opacity: 0.7; /* CSS3 standard */
            background-color: #999;
            z-index: 1;
        }
        .close_button
        {
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            font-weight: bold;
            float: right;
            color: #666;
            display: block;
            text-decoration: none;
            border: 2px solid #666;
            padding: 0px 3px 0px 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/images/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblInsId" Visible="false" runat="server" Text="Label"></asp:Label>
            <center>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <div class="pageHeading">
                                Bulk Upload Questions</div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" Text="Label" Visible="false"></asp:Label>
                            <asp:RegularExpressionValidator ID="revldtrBkgrnd" runat="server" ErrorMessage="Select proper Excel file. "
                                ValidationExpression="^.+(.xls|.xlsx)$" ControlToValidate="FileUpload1" ForeColor="Red"
                                Display="Dynamic"></asp:RegularExpressionValidator>
                            <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" Text="Course"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                            DataValueField="courseid" AutoPostBack="True" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Subject"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSubjects" runat="server" DataSourceID="sourceSubjects" DataTextField="subjectinfo"
                                            DataValueField="subid">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="Choose File"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="simplebtn" OnClick="btnSubmit_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp; <a href="#" onclick="openpopup('popup1')">Excel Sheet Format & Help</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp; <a href="/download.aspx?file=excelSheetFormat.xls">Download Excel Sheet Format</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gridData" runat="server" CssClass="gridview" Caption="<b>Recent Records</b>">
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </center>
            </div>
            <div id="popup1" class="popup">
                <br />
                <br />
                <b>Excel Sheet Format</b><br />
                <br />
                Below is the format for excel sheet:
                <br />
                Create an Excel Sheet with any name but save the sheet with data on "Sheet1" only.<br />
                Questions without images and common data can be uploaded directly from this page.
                <br />
                <br />
                <img src="HelpDocs/excelFormat.png" alt="" />
            </div>
            <div id="bg" class="popup_bg">
            </div>
            <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                SelectCommand="SELECT Distinct courseid, courseinfo FROM view_catCourseSub where instituteId=@instituteId and courseid is not null and subid is not null ORDER BY courseinfo">
                <SelectParameters>
                    <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sourceSubjects" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>">
            </asp:SqlDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
