﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forgetPassword.aspx.cs"
    Inherits="OnlineExam.forgetPassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forget Password</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('Button12345').click();
        }
        function cancel() {
            window.parent.document.getElementById('getPassCancel').click();
        }
        
         function callfun()
    {
      parent.CallMasterAlert();
        return false;
    }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Recover Password
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="/ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <center>
                        <table>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Label ID="lblMsg" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblUType" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Label ID="lblMail" runat="server" Text="Label" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="Label1" runat="server" Text="Email" CssClass="myLabel"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="txtbox"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email ID !"
                                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnNext" runat="server" Text="Next >>" CssClass="simplebtn" OnClick="btnNext_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="Label2" runat="server" Text="Select Institute" CssClass="myLabel"
                                        Visible="false"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlInstitute" runat="server" CssClass="txtbox" Width="180px"
                                        DataTextField="instituteName" DataValueField="instituteId" Visible="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnGetPass" runat="server" Text="Get Password" CssClass="simplebtn"
                                        CausesValidation="false" Visible="false" OnClick="btnGetPass_Click" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
