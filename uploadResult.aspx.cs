﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;


namespace OnlineExam
{
    public partial class uploadResult : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add(new DataColumn("File", typeof(String)));
            //    dt.Columns.Add(new DataColumn("File", typeof(String)));
            //    dt.Columns.Add(new DataColumn("File", typeof(String)));

            //    string[] a = Directory.GetFiles(Server.MapPath("~\\OResultDocs\\"), "*.*");
            //    for (int i = 0; i < a.Length; i++)
            //    {

            //        DataRow dr = dt.NewRow();

            //        string just_file = System.IO.Path.GetFileName((string)a[i]);
            //        dr[0] = just_file;

            //        dt.Rows.Add(dr);
            //    }

            //    gridFiles.DataSource = dt;
            //    gridFiles.DataBind();
            //}
            if (!IsPostBack)
            {
                gridFiles.DataBind();
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            if (ddlClass.Text != "-SELECT-")
            {
                if (FileUpload1.HasFile)
                {
                    string ext = System.IO.Path.GetExtension(this.FileUpload1.PostedFile.FileName);
                    string extToUpper = ext.ToUpper();
                    if (extToUpper == ".PDF")
                    {
                        if (FileUpload1.FileBytes.Length >= 4194304)
                        {
                            //Messages11.setMessage(0, "Cannot Upload ! File Size is Larger than 4 MB.");
                            //Messages11.Visible = true;
                        }
                        else
                        {
                            DataTable dt = new DataTable();
                            dt.Columns.Add(new DataColumn("File", typeof(String)));
                            string[] a = Directory.GetFiles(Server.MapPath("~\\OResultDocs\\"), "*.*");
                            for (int i = 0; i < a.Length; i++)
                            {

                                DataRow dr = dt.NewRow();

                                string just_file = System.IO.Path.GetFileName((string)a[i]);
                                dr[0] = just_file;
                                dt.Rows.Add(dr);
                            }
                            string IsAvailable = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (dt.Rows[j][0].ToString() == FileUpload1.PostedFile.FileName)
                                {
                                    IsAvailable = "yes";
                                }
                            }
                            if (IsAvailable == "")
                            {
                                FileUpload1.SaveAs(Server.MapPath("~\\OResultDocs\\" + FileUpload1.PostedFile.FileName));

                                ob.insertResult(FileUpload1.PostedFile.FileName, ddlClass.Text, txtName.Text);
                                ddlClass.Text = "-SELECT-";
                            }
                            else
                            {
                                lblMsg.Text = "File already exists. Use another name for file.";
                                lblMsg.Visible = true;
                            }



                            gridFiles.DataBind();
                        }
                    }
                }
            }
            else
            {
                lblMsg.Text = "Select proper Class/Course.";
                lblMsg.Visible = true;
            }

        }

        protected void sourceDetails_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            string fileName = "";
            int i = 0;
            foreach (GridViewRow gvrow in gridFiles.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                if (chk != null & chk.Checked)
                {
                    ob.deleteResult(gridFiles.DataKeys[gvrow.RowIndex].Value.ToString());
                    fileName = gvrow.Cells[2].Text;

                    string path = Server.MapPath("~\\OResultDocs\\" + fileName);
                    FileInfo file = new FileInfo(path);
                    if (file.Exists)
                    {
                        file.Delete();
                    }//ids += gridFiles.DataKeys[gvrow.RowIndex].Value.ToString() + ',';
                    i++;
                }
            }
            gridFiles.DataBind();
            if (i == 0)
            {
                lblMsg.Text = "Select files to delete !";
                lblMsg.Visible = true;
            }
            if (i > 0)
            {
                lblMsg.Text = "Files deleted successfully !";
                lblMsg.Visible = true;
            }
        }

        protected void gridFiles_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }
        }
    }
}
