﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ManageSubject : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["InstituteID"] != null)
                    {
                        lblInsId.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblInsId.Text = val[3];
                    }
                }
            }
            catch
            {
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Button1.Text == "Add New")
                {
                    obj.AddNewSubjectInfo(ddlCourse.SelectedValue, txtSubject.Text);
                    Messages11.setMessage(1, "Subject Info Added Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    txtSubject.Text = "";
                }

                if (Button1.Text == "Update")
                {
                    obj.UpdateSubjectInfo(ddlCourse.SelectedValue, txtSubject.Text, lblSubID.Text);
                    Messages11.setMessage(1, "Subject Info Updated Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    txtSubject.Text = "";
                    Button1.Text = "Add New";
                    btnCan.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowno = Convert.ToInt16(e.CommandArgument);
            if (e.CommandName == "change")
            {
                lblSubID.Text = GridView1.Rows[rowno].Cells[0].Text;
                DataTable dt = new DataTable();
                dt = obj.GetSubjectInfoByID(lblSubID.Text);
                ddlCourse.SelectedValue = dt.Rows[0][0].ToString();
                txtSubject.Text = dt.Rows[0][1].ToString();
                Button1.Text = "Update";
                btnCan.Visible = true;
            }

            if (e.CommandName == "remove")
            {
                try
                {
                    obj.DeleteSubjectInfo(GridView1.Rows[rowno].Cells[0].Text);
                    GridView1.DataBind();
                    Messages11.setMessage(1, "Subject Info Deleted Successfully !!!");
                    Messages11.Visible = true;
                }
                catch (Exception ex)
                {
                    Messages11.setMessage(0, ex.Message);
                    Messages11.Visible = true;
                }
            }
        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            btnCan.Visible = false;
            Button1.Text = "Add New";
            txtSubject.Text = "";
        }
    }
}
