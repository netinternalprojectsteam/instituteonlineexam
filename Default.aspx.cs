﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Globalization;

namespace OnlineExam
{
    public partial class Default : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               

                try
                {
                    if (HttpContext.Current.User.Identity.Name.Length > 0)
                    {

                        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

                        if (val[1].ToString() == "Admin" || val[1].ToString() == "Super Admin")
                        {
                            Response.Redirect("ManageCategory.aspx");
                        }
                        if (val[1].ToString() == "User")
                        {
                            Response.Redirect("testsList.aspx");
                        }
                        //Page.MasterPageFile


                    }
                    else
                    {
                        Response.Redirect("LoginDetails.aspx");
                       // Response.Redirect("/Web/Home.aspx");
                    }
                }
                catch (Exception)
                {

                    //Response.Redirect("LoginDetails.aspx");
                }





                DataTable dtStudyMaterial = new DataTable();
                dtStudyMaterial = ob.getStudyMaterials("FREE STUDY MATERIAL");

                string sMaterial = "<table width='100%' height='225px'><tr><td valign='top' align='left'><table>";
                for (int x = 0; x < dtStudyMaterial.Rows.Count; x++)
                {
                    sMaterial = sMaterial + "<tr><td align='left'><img src='/images/arrow.jpg' width='10px' height='10px'/>&nbsp;<a href= 'studyDetails.aspx?ID=" + dtStudyMaterial.Rows[x]["id"].ToString() + "' target='_blank'>" + dtStudyMaterial.Rows[x]["Mheading"].ToString() + "</a> </td></tr>";
                }

                sMaterial = sMaterial + "</table></td></tr><tr><td align='right' valign='bottom'><a href='udata.aspx?Mcategory=FREE_STUDY_MATERIAL' class='tLink1'>Read More>></a>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>";


                lblStudyMaterial.Text = sMaterial;

                sMaterial = "<table width='100%' height='225px'><tr><td valign='top' align='left'><table>";
                DataTable dtRecNews = new DataTable();
                dtRecNews = ob.getStudyMaterials("CURRENT JOBS & RESULTS");

                for (int x = 0; x < dtRecNews.Rows.Count; x++)
                {
                    sMaterial = sMaterial + "<tr><td align='left'><img src='/images/arrow.jpg' width='10px' height='10px'/>&nbsp;<a href= 'studyDetails.aspx?ID=" + dtRecNews.Rows[x]["id"].ToString() + "' target='_blank'>" + dtRecNews.Rows[x]["Mheading"].ToString() + "</a> </td></tr>";
                }
                sMaterial = sMaterial + "</table></td></tr><tr><td align='right' valign='bottom'><a href='udata.aspx?Mcategory=CURRENT_JOBS' class='tLink1'>Read More>></a>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>";

                lblCurrentJobs.Text = sMaterial;

                sMaterial = "<table width='100%' height='225px'><tr><td valign='top' align='left'><table width='100%'>";

                DataTable dtBAwareness = new DataTable();
                dtBAwareness = ob.getStudyMaterials("BANKING AWARENESS");

                for (int x = 0; x < dtBAwareness.Rows.Count; x++)
                {
                    sMaterial = sMaterial + "<tr><td align='left'><img src='/images/arrow.jpg' width='10px' height='10px'/>&nbsp;<a href= 'studyDetails.aspx?ID=" + dtBAwareness.Rows[x]["id"].ToString() + "' target='_blank'>" + dtBAwareness.Rows[x]["Mheading"].ToString() + "</a> </td></tr>";
                }

                sMaterial = sMaterial + "</table></td></tr><tr><td align='right' valign='bottom'><a href='udata.aspx?Mcategory=BANKING_AWARENESS' class='tLink1'>Read More>></a>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>";
                lblBAwareness.Text = sMaterial;

                sMaterial = "";

                DataTable dtSyllabus = ob.getExamSyllabus();
                //sheading,id
                sMaterial = "<table width='100%'>";
                for (int x = 0; x < dtSyllabus.Rows.Count; x++)
                {
                    sMaterial = sMaterial + "<tr><td align='left'><img src='/images/arrow.jpg' width='10px' height='10px'/>&nbsp;<a href= '/syllabus.aspx?ID=" + dtSyllabus.Rows[x]["id"].ToString() + "' target='_blank'>" + dtSyllabus.Rows[x]["sHeading"].ToString() + "</a> </td></tr>";
                }
                lblSyllabus.Text = sMaterial + "</table>";


                DataTable dtIMPLInks = new DataTable();
                dtIMPLInks = ob.getStudyMaterials("IMPORTANT LINKS");

                sMaterial = "<table width='100%'>";
                for (int x = 0; x < dtIMPLInks.Rows.Count; x++)
                {
                    sMaterial = sMaterial + "<tr><td align='left'><img src='/images/arrow.jpg' width='10px' height='10px'/>&nbsp;<a href= 'studyDetails.aspx?ID=" + dtIMPLInks.Rows[x]["id"].ToString() + "' target='_blank'>" + dtIMPLInks.Rows[x]["Mheading"].ToString() + "</a> </td></tr>";
                }

                sMaterial = sMaterial + "</table>";
                lblImpLinks.Text = sMaterial;



            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.Page.MasterPageFile = "~/userMaster.master";
            }
            else
            {
                //this.Page.MasterPageFile = "~/site1.master";
                this.Page.MasterPageFile = "~/userMaster.master";
            }
        }
    }
}
