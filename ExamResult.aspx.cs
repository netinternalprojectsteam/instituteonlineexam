﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ExamResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string listarr = Session["newAnswer"].ToString();
            //string[] AnsList = listarr.Split(',');
            //for (int i = 0; i > AnsList.Length; i++)
            //{
            //    lblResult.Text = AnsList[i].ToString();
            //}
            //Session["dtable"] = dt;
            string[] ans = Session["ans"].ToString().Split(',');

            DataTable dt = (DataTable)Session["dtable"];

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("ID", typeof(string));
            dt1.Columns.Add("Question", typeof(string));
            dt1.Columns.Add("Option 1", typeof(string));
            dt1.Columns.Add("Option 2", typeof(string));
            dt1.Columns.Add("Option 3", typeof(string));
            dt1.Columns.Add("Option 4", typeof(string));
            dt1.Columns.Add("Correct Answer", typeof(string));
            dt1.Columns.Add("Your Answer", typeof(string));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dtrow = dt1.NewRow();
                dtrow["ID"] = dt.Rows[i][0].ToString();
                dtrow["Question"] = dt.Rows[i][1].ToString();
                dtrow["Option 1"] = dt.Rows[i][2].ToString();
                dtrow["Option 2"] = dt.Rows[i][3].ToString();
                dtrow["Option 3"] = dt.Rows[i][4].ToString();
                dtrow["Option 4"] = dt.Rows[i][5].ToString();
                dtrow["Correct Answer"] = dt.Rows[i][6].ToString();
                dtrow["Your Answer"] = ans[i].ToString();
                dt1.Rows.Add(dtrow);
            }

            GridView1.DataSource = dt1;
            GridView1.DataBind();

            int correct = 0; int incorrect = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (GridView1.Rows[i].Cells[6].Text == GridView1.Rows[i].Cells[7].Text)
                {
                    correct = correct + 1;
                }
                else { incorrect = incorrect + 1; }
            }

            Label1.Text = "You have given " + correct.ToString() + " correct and " + incorrect.ToString() + " incorrect answers";
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[6].Text == e.Row.Cells[7].Text)
                {
                    e.Row.BackColor = System.Drawing.Color.Green;
                    e.Row.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Red;
                    e.Row.ForeColor = System.Drawing.Color.White;
                }
            }
        }
    }
}
