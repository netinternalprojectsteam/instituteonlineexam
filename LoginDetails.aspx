﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="LoginDetails.aspx.cs" Inherits="OnlineExam.LoginDetails" Title="Login Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/tabContainer.css" rel="stylesheet" type="text/css" />
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblRedirectTo" Visible="false" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblMessage" Visible="false" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="lblMail" Visible="false" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="lblErrorMessage" runat="server" Visible="false" Text="Label"></asp:Label>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme">
                            <asp:TabPanel runat="server" HeaderText="Login to Start Exam" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <table width="100%">
                                        <tr>
                                            <asp:SqlDataSource ID="sourceCountry" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                SelectCommand="SELECT DISTINCT [ID], [countryName] FROM [countryList] order by countryName ASC">
                                            </asp:SqlDataSource>
                                            <asp:SqlDataSource ID="sourceState" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                SelectCommand="SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] order by statename asc">
                                            </asp:SqlDataSource>
                                            <td align="left" valign="top">
                                                <asp:Panel ID="Panel1" runat="server">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td colspan="3" align="left" valign="top">
                                                                    <div style="font-weight: bold; color: black">
                                                                        Please Login to Continue
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <div style="border: solid 1px black; padding: 10px;">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center" colspan="3">
                                                                                    <asp:Label ID="lblUserError" Visible="False" runat="server" Text="Invalid Login Details."
                                                                                        ForeColor="Red"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Label ID="Label1" runat="server" Text="Email" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td rowspan="5" style="width: 10px">
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email ID !"
                                                                                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Label ID="Label2" runat="server" Text="Password" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="txtbox" TextMode="Password"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <asp:CheckBox ID="chkUserRem" runat="server" />
                                                                                    <asp:Label ID="Label9" runat="server" Text="Remember Me"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <br />
                                                                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="linkUserForget" runat="server" CausesValidation="False"
                                                                                        OnClick="linkUserForget_Click" Style="text-decoration: underline">Forgot Password</asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                </td>
                                                                                <td align="left">
                                                                                    <br />
                                                                                    <asp:Button ID="btnUserLogin" CssClass="simplebtn" runat="server" Text="Login" OnClick="btnUserLogin_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                            <td valign="top">
                                                <asp:Panel ID="Panel3" runat="server">
                                                    <center>
                                                        <table>
                                                            <tr>
                                                                <td align="left" colspan="3" valign="top">
                                                                    <div style="font-weight: bold; color: black">
                                                                        DO NOT have a username & password? Register here
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <div style="border: solid 1px black; padding: 5px">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center" colspan="3">
                                                                                    <asp:Label ID="lblSignUpError" Visible="False" runat="server" ForeColor="Red" Text="Email ID already exists !"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Label ID="Label5" runat="server" Text="Name" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td rowspan="6" style="width: 10px">
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtbox" Width="250px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Label ID="Label8" runat="server" Text="Institute Name" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:DropDownList ID="ddlInstitute" runat="server" DataTextField="Institute" DataValueField="InstituteId"
                                                                                        Width="260px">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <asp:Label ID="Label6" runat="server" Text="Email ID" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtEmailID" runat="server" CssClass="txtbox" Width="250px"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid email !"
                                                                                        ControlToValidate="txtEmailID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                                                                    <div style="font-size: x-small; font-style: oblique; font-weight: lighter">
                                                                                        (log in password will be send to this email id &amp; also<br>
                                                                                        same email id u can use as user name for further log in)
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:Label ID="Label7" runat="server" Text="Mobile" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ID="txtMo" runat="server" CssClass="txtbox" Width="250px" MaxLength="12"></asp:TextBox>
                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMo"
                                                                                        FilterType="Numbers" Enabled="True">
                                                                                    </asp:FilteredTextBoxExtender>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" valign="top">
                                                                                    <asp:Label ID="Label11" runat="server" Text="Address" CssClass="myLabel"></asp:Label>
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td colspan="4">
                                                                                                <asp:TextBox ID="txtAdd1" runat="server" Width="300px" CssClass="txtbox"></asp:TextBox>
                                                                                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkText="Address Line 1"
                                                                                                    TargetControlID="txtAdd1" Enabled="True">
                                                                                                </asp:TextBoxWatermarkExtender>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4">
                                                                                                <asp:TextBox ID="txtAdd2" runat="server" Width="300px" CssClass="txtbox"></asp:TextBox>
                                                                                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkText="Address Line 2"
                                                                                                    TargetControlID="txtAdd2" Enabled="True">
                                                                                                </asp:TextBoxWatermarkExtender>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4">
                                                                                                <asp:TextBox ID="txtCity" runat="server" CssClass="txtbox"></asp:TextBox>
                                                                                                <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" WatermarkText="City Name"
                                                                                                    TargetControlID="txtCity" Enabled="True">
                                                                                                </asp:TextBoxWatermarkExtender>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <asp:Label ID="Label12" runat="server" Text="Country" CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:DropDownList ID="ddlCountry" runat="server" DataSourceID="sourceCountry" DataTextField="countryName"
                                                                                                    DataValueField="ID" AutoPostBack="True" Width="120px" CssClass="txtbox" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:Label ID="Label13" runat="server" Text="State" CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:DropDownList ID="ddlState" runat="server" DataSourceID="sourceState" DataTextField="stateName"
                                                                                                    DataValueField="ID" Width="120px" CssClass="txtbox">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:Button ID="btnNewAccount" CssClass="simplebtn" runat="server" Text="Sign Up Now"
                                                                                        OnClick="btnNewAccount_Click" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </center>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                </ContentTemplate>
                            </asp:TabPanel>
                        </asp:TabContainer>
                    </td>
                </tr>
                <asp:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme">
                    <asp:TabPanel runat="server" Visible="false" HeaderText="Login" ID="TabPanel4">
                        <ContentTemplate>
                            <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Admin Login">
                                <ContentTemplate>
                                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                                        <center>
                                            <table>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        <asp:Label ID="lblAdminError" Visible="False" runat="server" Text="Invalid Login Details."
                                                            ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="Label3" runat="server" Text="Email" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td rowspan="5" style="width: 10px">
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtUName" runat="server" CssClass="txtbox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Email ID !"
                                                            ControlToValidate="txtUName" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="Label4" runat="server" Text="Password" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtPass" TextMode="Password" runat="server" CssClass="txtbox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:CheckBox ID="chkAdminRem" runat="server" />
                                                        <asp:Label ID="Label10" runat="server" Text="Remember Me"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="left">
                                                        &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="linkAdminForget" runat="server" CausesValidation="False"
                                                            OnClick="linkUserForget_Click" Style="text-decoration: underline">Forgot Password</asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                    </td>
                                                    <td align="left">
                                                        <asp:Button ID="btnAdminLogin" runat="server" Text="Login" CssClass="simplebtn" OnClick="btnAdminLogin_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:TabPanel>
                            <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Sign Up">
                                <ContentTemplate>
                                </ContentTemplate>
                            </asp:TabPanel>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </table>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton>
            </div>
            <asp:ModalPopupExtender ID="modalForgetPass" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="getPass" Drag="true" CancelControlID="getPassCancel">
            </asp:ModalPopupExtender>
            <div id="getPass" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe1" frameborder="0" src="forgetPassword.aspx" height="300px" width="400px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="Button12345" value="Done" type="button" />
                    <input id="getPassCancel" value="Cancel" type="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
