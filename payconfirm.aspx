﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="payconfirm.aspx.cs" Inherits="OnlineExam.payconfirm" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Confirm</title>
    <!--// Style Sheet //-->
    <link href="css/contentslider.css" rel="stylesheet" type="text/css" />
    <link href="css/default.advanced.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/slider.css" rel="stylesheet" type="text/css" />
    <!--// Javascript //-->
    <link href="css/newCSS.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="js/jquery.min14.js"></script>

    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/contentslider.js"></script>

    <style type="text/css">
        .trigger
        {
            position: absolute;
            text-decoration: none;
            top: 250px;
            left: 0px;
            font-size: 12px;
            letter-spacing: -1px;
            font-family: verdana, helvetica, arial, sans-serif;
            color: #fff;
            font-weight: 700;
            background: #333333 15% 55% no-repeat;
            display: block;
        }
    </style>
    <style type="text/css">
        .membersvs
        {
            width: 400px;
            margin: 0 auto;
            margin-bottom: 40px;
        }
        .membersvs tr th
        {
            padding: 10px;
            border: 1px solid #272727;
            background: #2d2d2d;
            color: #fff;
        }
        .membersvs tr th:first-child
        {
            width: 60px;
        }
        .membersvs tr td
        {
            padding: 10px;
            border: 1px solid #ccc;
        }
        .membersvs tr td a
        {
            color: #444;
            font-weight: bold;
        }
        .membersvs tr td a:hover
        {
            color: #99CC00;
            text-decoration: none;
        }
        .membersvs tr td table
        {
            width: auto;
        }
        .membersvs tr td td
        {
            padding: 5px;
            border: 1px solid #eee;
            width: 20px;
        }
    </style>

    <script>
        window.onload = function () {
            var d = new Date().getTime();
            document.getElementById("tid").value = d;
        };
    </script>

</head>
<body>
    <div id="wrapper_sec">
        <table width="940px" style="background-image: url('images/obe.png'); height: 100px;
            width: 940px; background-repeat: no-repeat">
            <tr>
                <td align="left">
                </td>
                <td align="right" valign="top" style="padding: 5px">
                    <asp:Panel ID="Panel4" runat="server" Visible="false">
                        <div style="color: #ffb401;">
                            <b>Welcome&nbsp;&nbsp;
                                <asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label></b></div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <table width="100%">            
            <tr>
                <td align="right">
                    <div id="showBrowserMsg" runat="server">
                        Works best in Google Chrome.&nbsp;<a href="https://www.google.com/intl/en/chrome/browser/"
                            target="_blank">Download Google Chrome</a>
                    </div>
                </td>
            </tr>
        </table>
        <div class="clear">
        </div>
        <div class="clear">
        </div>
        <div class="clear">
        </div>
        <div id="content_sec">
            <div id="DivToHide">
                <uc1:Messages1 ID="messages11" runat="server" />
            </div>
            <div>
                <form method="post" name="customerData" action="ccavRequestHandler.aspx">
                <center>
                    <table width="40%" height="20" border='1' align="center">
                        <caption style="padding-top: 30px; margin-bottom: 0px;">
                            <font size="5" color="blue"><b>Confirm Payment</b></font></caption>
                    </table>
                    <input type='hidden' name='tid' id='tid' readonly />
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                </center>
                </form>
            </div>
            <br /><br />
            <hr />
            <div>
                <p style="font-weight: bold; font-size: 15px;">
                    Instructions:</p>
                <ul style="padding: 15px;">
                    <li>1. Candidate can make a payment next day after confirmed for examination. </li>
                    <li>2. For any payment related query, insurance company/candidate needs to contact the
                        respective bank.</li>
                    <li>3. Please do not close the browser till you get an appropriate message (Payment
                        Successful/Unsuccessful) and note down the Customer Reference number. from bank
                        account then kindly contact your respective bank.</li>
                    <li>5. Exam fees once paid will not be refunded under any circumstances once scheduled,
                        even if candidate chooses to remain absent for the examination. </li>
                    <li style="font-weight: bold; color: Red;">6. In case of payment confirmation not received,
                        transaction amount will be refunded back within 3-4 working days.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="FooterDiv">
        <center>
            <table width="100%">
                <tr>
                    <td align="center">
                        Copyright © 2013
                    </td>
                </tr>
            </table>
        </center>
    </div>
</body>
</html>
