﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class editContactInfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCountry.DataBind();
                sourceState.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                sourceState.SelectCommand = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = " + ddlCountry.SelectedValue + " order by statename asc";
                ddlState.DataBind();
                try
                {
                    lblID.Text = Session["editUserID"].ToString();

                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT uid, extraEmail, Add1, Add2, city, state, (Select ID from view_stateList where view_stateList.stateName=exam_users.state and view_stateList.countryName=exam_users.country) as stateID, country, (Select ID from countryList where countryList.countryName=exam_users.country) as countryID FROM exam_users where uID = " + lblID.Text + "");
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["Add1"].ToString().Length > 0 || dt.Rows[0]["Add2"].ToString().Length > 0 || dt.Rows[0]["city"].ToString().Length > 0 || dt.Rows[0]["country"].ToString().Length > 0 || dt.Rows[0]["state"].ToString().Length > 0 || dt.Rows[0]["extraEmail"].ToString().Length > 0)
                        {
                            Label3.Text = dt.Rows[0]["uid"].ToString();
                            txtAdd1.Text = dt.Rows[0]["Add1"].ToString();
                            txtAdd2.Text = dt.Rows[0]["Add2"].ToString();
                            txtCity.Text = dt.Rows[0]["city"].ToString();
                            //ddlCountry.SelectedItem.Text = dt.Rows[0]["country"].ToString();
                            //ddlState.SelectedItem.Text = dt.Rows[0]["state"].ToString();
                            ddlCountry.SelectedValue = dt.Rows[0]["countryID"].ToString();
                            sourceState.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                            sourceState.SelectCommand = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = " + ddlCountry.SelectedValue + " order by statename asc";
                            ddlState.DataBind();
                            ddlState.SelectedValue = dt.Rows[0]["stateID"].ToString();
                            txt1.Text = dt.Rows[0]["extraEmail"].ToString();
                            btnUpdate.Text = "Update";
                        }
                        else
                        {
                            btnUpdate.Text = "Save";
                        }
                    }
                    else
                    {
                        btnUpdate.Text = "Save";
                    }


                }
                catch { }
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (ddlState.Text != "-Select-" && ddlCountry.Text != "-Select-")
            {

                ob.updateContactDetails(txt1.Text, txtAdd1.Text, txtAdd2.Text, txtCity.Text, ddlState.SelectedItem.Text, ddlCountry.SelectedItem.Text, Label3.Text);


                string details = "";
                if (txtAdd1.Text.Length > 0)
                {
                    details = txtAdd1.Text;
                }
                if (details.Length > 0)
                { details = details + ",<br>" + txtAdd2.Text; }
                else { details = txtAdd2.Text; }

                if (txtCity.Text.Length > 0)
                {
                    if (details.Length > 0)
                    { details = details + ",<br>" + txtCity.Text + ","; }
                    else { details = txtCity.Text + ","; }
                }

                details = details + "<br>" + ddlState.SelectedItem.Text;
                details = details + "<br>" + ddlCountry.SelectedItem.Text;

                Session["Contact"] = details + "#" + txt1.Text;

                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Select Proper Country and State !");
            }


        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            sourceState.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            sourceState.SelectCommand = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = " + ddlCountry.SelectedValue + " order by statename asc";
            ddlState.DataBind();
        }
    }
}
