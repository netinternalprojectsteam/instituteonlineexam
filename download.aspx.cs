﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace OnlineExam
{
    public partial class download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string app_data_folder = "";
                app_data_folder += "/UploadedDocs/";

                string path = app_data_folder;
                string name = Path.GetFileName(Request["file"]);
                string ext = Path.GetExtension(Request["file"]);
                string type = "";

               

                // set known types based on file extension  
                if (ext != null)
                {
                    switch (ext.ToLower())
                    {
                        case ".htm":
                        case ".html":
                            type = "text/HTML";
                            break;
                        case ".rtf":
                            type = "Application/msword";
                            break;
                        case ".txt":
                        case ".sql":
                            type = "text/plain";
                            break;
                        case ".doc":
                            type = "application/ms-word";
                            break;
                        case ".xls":
                            type = "application/vnd.ms-excel";
                            break;
                        case ".xlsx":
                            type = "application/vnd.ms-excel";
                            break;
                        case ".gif":
                            type = "image/gif";
                            break;
                        case ".jpg":
                        case "jpeg":
                            type = "image/jpeg";
                            break;
                        case ".bmp":
                            type = "image/bmp";
                            break;
                        case ".wav":
                            type = "audio/wav";
                            break;
                        case ".ppt":
                            type = "application/mspowerpoint";
                            break;
                        case ".dwg":
                            type = "image/vnd.dwg";
                            break;
                        case ".zip":
                            type = "application/zip";
                            break;
                        case ".exe":
                            type = "application/exe";
                            break;
                        default:
                            type = "application/octet-stream";
                            break;
                    }
                }

                Response.AppendHeader("content-disposition", "attachment; filename=" + name);

                if (type != "")
                {
                    Response.ContentType = type;
                    //Response.WriteFile(path);
                    Response.TransmitFile(path + name);
                    Response.End();
                }



            }
            catch (Exception)
            { //Label1.Text = ee.Message;
            }

        }
    }
}

