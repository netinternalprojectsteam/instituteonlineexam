﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="examReview.aspx.cs" Inherits="OnlineExam.examReview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Exam Review</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnMasterCanUser').click();
        }
        
           function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Exam Review
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel4" runat="server">
                    <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label4" runat="server" Text="Remaining Time :"></asp:Label>&nbsp;&nbsp;<asp:Label
                        ID="Label2" runat="server" />
                    <asp:Timer ID="Timer1" runat="server" Interval="3600" OnTick="Timer1_Tick">
                    </asp:Timer>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <center>
                        <table width="100%">
                            <tr>
                                <td align="right">
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblName" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="Panel2" runat="server" Height="200px" ScrollBars="Auto">
                                        <asp:GridView ID="gridReview" CssClass="gridview" runat="server" OnRowCreated="gridReview_RowCreated"
                                            OnRowCommand="gridReview_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Questions">
                                                    <ItemTemplate>
                                                        Question&nbsp;
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lbltype" runat="server" Text="Label" Visible="false"></asp:Label>
                                    <asp:Button ID="btnInc" runat="server" Text="Unsolved" OnClick="btnInc_Click" CssClass="simplebtn" />
                                    <asp:Button ID="btnMarked" runat="server" Text="Review Marked" OnClick="btnMarked_Click"
                                        CssClass="simplebtn" />
                                    <asp:Button ID="totalReview" runat="server" Text="Full Review" OnClick="totalReview_Click"
                                        CssClass="simplebtn" />
                                    <asp:Button ID="Button1" runat="server" Text="Close" OnClick="Button1_Click" CssClass="simplebtn"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblExamID" runat="server" Text="Label" Visible="false"></asp:Label>
                        <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
                    </center>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
