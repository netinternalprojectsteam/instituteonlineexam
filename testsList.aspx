﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="testsList.aspx.cs" Inherits="OnlineExam.testsList" Title="Test Lists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/rolloverImages.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .btn1
        {
            background-color: whitesmoke;
            color: Black;
            padding: 4px;
            border-radius: 5px;
        }
        .btn1:hover
        {
            background-color: #46b1e1;
            color: White;
            padding: 4px;
            border-radius: 5px;
        }
        .selectedbtn
        {
            background-color: #46b1e1;
            color: White;
            padding: 4px;
            border-radius: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td align="right" style="font-weight: bold; color: Blue">
            </td>
        </tr>
        <tr>
            <td>
                <div class="pageHeading">
                    Test List</div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblInstitute" runat="server" Text="" Visible="false"></asp:Label>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" height="300px">
                <table>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="Choose Exam Type" Font-Bold="true"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true">
                                <asp:ListItem>Free</asp:ListItem>
                                <asp:ListItem>Paid</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label1" runat="server" Text="Choose Exam Name" Font-Bold="true"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlExams" runat="server" DataTextField="examName" DataValueField="examID"
                                DataSourceID="sourceExams">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT examID, examName FROM view_examDetails WHERE (isAvailable = 1) AND (section1Ques IS NOT NULL) AND (len(section1Ques)>0) AND (examType=@examType) and (instituteId=@instituteId)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlType" Name="examType" Type="String" />
                                    <asp:ControlParameter ControlID="lblInstitute" Name="instituteId" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Button ID="btnStart" runat="server" Text="Start" CssClass="simplebtn" OnClick="btnStart_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
