﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="manageAdmin.aspx.cs" Inherits="OnlineExam.manageAdmin" Title="Manage Admins" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript"> 
        function ConfirmOnDelete()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Manage Admins</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblInsID" runat="server" Text="0" Visible="false"></asp:Label>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnNew" CausesValidation="false" CssClass="simplebtn" runat="server"
                            Text="Add New Admin" OnClick="btnNew_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <asp:Label ID="lblMail" Visible="false" runat="server" Text="Label"></asp:Label>
                            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label6" runat="server" Text="Institute" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlInstitute" runat="server" DataValueField="InstituteId" DataTextField="Institute">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                                            ControlToValidate="ddlInstitute" Text="Institute required" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="Email" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid email id!"
                                            ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" Text="Mobile" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtMobile" runat="server" MaxLength="12"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMobile"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label4" runat="server" Text="Type" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlType" runat="server">
                                            <asp:ListItem Selected="True">Admin</asp:ListItem>
                                            <%-- <asp:ListItem>Super Admin</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblPass" runat="server" Text="Password" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtPass"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblRePass" runat="server" Text="Confirm Password" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtRePass" runat="server" TextMode="Password"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtRePass"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkUpdatePass" runat="server" Text="  Update Password" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                                            CssClass="simplebtn" Width="67px" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT * FROM [view_adminDetails]"></asp:SqlDataSource>
                            <asp:GridView ID="gridDetails" runat="server" DataSourceID="sourceDetails" CssClass="gridview"
                                AutoGenerateColumns="False" DataKeyNames="userid" OnRowCommand="gridDetails_RowCommand"
                                OnRowCreated="gridDetails_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No.">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="userid" HeaderText="userid" InsertVisible="False" ReadOnly="True"
                                        SortExpression="userid" />
                                    <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
                                    <asp:BoundField DataField="mobile" HeaderText="Mobile" SortExpression="mobile" />
                                    <asp:BoundField DataField="username" HeaderText="Email" SortExpression="username" />
                                    <asp:BoundField DataField="Institute" HeaderText="Institute" SortExpression="Institute" />
                                    <asp:ButtonField ButtonType="Image" CommandName="change" HeaderText="Edit" ImageUrl="~/images/editInfoo.png"
                                        Text="Button"></asp:ButtonField>
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButtonDelete" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ImageUrl="/images/delete.gif" ToolTip="Delete" CommandName="Remove" OnClick="btnGrid_Click" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="15px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="15px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
