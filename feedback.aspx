﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="feedback.aspx.cs" Inherits="OnlineExam.feedback" Title="Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript"> 
        function ConfirmOnDelete()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Feedback</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Select Exam Name :
                        <asp:DropDownList ID="ddlExams" runat="server" AutoPostBack="True" DataSourceID="sourceExams"
                            DataTextField="examName" DataValueField="examID" OnSelectedIndexChanged="ddlExams_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="Label1" runat="server" Text="No Details Found" Font-Bold="true" ForeColor="Red"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Panel ID="Panel1" runat="server">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="gridDetails" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                            OnRowCommand="gridDetails_RowCommand" DataKeyNames="id" OnRowCreated="gridDetails_RowCreated">
                                            <Columns>
                                                <asp:BoundField DataField="id" HeaderText="id" />
                                                <asp:TemplateField HeaderText="Sr.No.">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                                <asp:BoundField DataField="On Date" HeaderText="On date" />
                                                <asp:BoundField DataField="Feedback" HeaderText="Feedback" />
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="gridButton1" runat="server" Text="Delete" OnClick="Button2_Click"
                                                            CssClass="simplebtn" OnClientClick="javascript:return ConfirmOnDelete()" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Publish">
                                                    <ItemTemplate>
                                                        <asp:Button ID="gridButton" CssClass="simplebtn" runat="server" Text="Button" OnClick="Button1_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                SelectCommand="SELECT DISTINCT [examID], [examName] FROM [view_examDetails] WHERE ([isAvailable] = @isAvailable and instituteId=@instituteId)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                    <asp:Parameter DefaultValue="true" Name="isAvailable" Type="Boolean" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sourceDetails" runat="server"></asp:SqlDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
