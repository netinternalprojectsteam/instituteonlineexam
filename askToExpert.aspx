﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="askToExpert.aspx.cs"
    Inherits="OnlineExam.askToExpert" Title="Ask To Expert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Panel ID="Panel3" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </td>
                            <td align="right">
                                Have a question ? &nbsp;&nbsp;
                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Ask To Expert.</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Repeater ID="rptExpert" runat="server" DataSourceID="sourceExpert">
                        <HeaderTemplate>
                            <b>Expret's Answers<br />
                                <br />
                            </b>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td align="left" style="width: 40px">
                                        <b>Que : </b>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("question") %>' Font-Bold="true"></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 40px">
                                        <b>Ans : </b>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("answer") %>'></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <hr style="width: 100%; border-bottom: solid 2px white" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel2" runat="server" Visible="false">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Question : "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtQue" runat="server" TextMode="MultiLine" Width="600px" Height="100px"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtQue"
                                    runat="server" ErrorMessage="Question text is Required !"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                                    CausesValidation="false" onclick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sourceExpert" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT question, answer FROM site_askToExpert"></asp:SqlDataSource>
</asp:Content>
