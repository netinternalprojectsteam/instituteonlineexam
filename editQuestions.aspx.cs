﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class editQuestions : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Messages11.Visible = false;
                    if (Session["InstituteID"] != null)
                    {
                        lblInsId.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblInsId.Text = val[3];
                    }
                    ddlCourse.DataBind();
                    try
                    {
                        sourceSub.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                        sourceSub.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        ddlSub.DataBind();
                    }
                    catch
                    {
                        Response.Redirect("/ManageCourse.aspx");
                    }
                }
                catch { }
                try
                {
                    if (Session["cancleQueEdit"].ToString() == "Yes")
                    {
                        string[] Qdata = Session["QData"].ToString().Split(',');
                        ddlCourse.SelectedValue = Qdata[1].ToString();
                        sourceSub.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                        sourceSub.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        ddlSub.DataBind();
                        ddlSub.SelectedValue = Qdata[2].ToString();
                        DataTable dt = new DataTable();
                        dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid FROM view_examQuestions where questid = " + Qdata[0].ToString() + " AND instituteId=" + lblInsId.Text + "");
                        Repeater2.DataSource = dt;
                        Repeater2.DataBind();


                        sourceQuestions.SelectCommand = "SELECT questid, question, option1, option2, option3, option4,option5, answer, difflevel, userid FROM   dbo.view_examQuestions WHERE (subid = " + ddlSub.SelectedValue + ")  and (instituteId=" + lblInsId.Text + ")";
                        sourceQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                        gridQuestions.DataBind();
                        DataTable dt1 = new DataTable();
                        dt1 = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, answer, difflevel, userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber] FROM view_examQuestions  WHERE (subid = " + ddlSub.SelectedValue + ")  and (instituteId=" + lblInsId.Text + ") ORDER BY questid");
                        Repeater1.DataSource = dt1;
                        Repeater1.DataBind();
                        ViewState["queTable"] = dt1;
                        Messages11.setMessage(1, "Question updated Successfully !!!");
                        Messages11.Visible = true;
                        Session.Remove("cancleQueEdit");
                    }
                }
                catch { }
            }
        }
        protected void ddlCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Messages11.Visible = false;
                sourceSub.SelectCommand = "SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where courseid=" + ddlCourse.SelectedValue + " and subid is not null";
                sourceSub.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                ddlSub.DataBind();
                sourceQuestions.SelectCommand = "SELECT questid, question, option1, option2, option3, option4,option5, answer, difflevel, userid FROM   dbo.view_examQuestions WHERE (subid = " + ddlSub.SelectedValue + ")  and (instituteId=" + lblInsId.Text + ")";
                sourceQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                gridQuestions.DataBind();


            }
            catch { }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            sourceQuestions.SelectCommand = "SELECT questid, question, option1, option2, option3, option4,option5, answer, difflevel, userid FROM   dbo.view_examQuestions WHERE (subid = " + ddlSub.SelectedValue + ")  and (instituteId=" + lblInsId.Text + ")";
            sourceQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            gridQuestions.DataBind();
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, answer, difflevel, userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber] FROM view_examQuestions  WHERE (subid = " + ddlSub.SelectedValue + ")  and (instituteId=" + lblInsId.Text + ") ORDER BY questid");
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            ViewState["queTable"] = dt;
            if (dt.Rows.Count == 0)
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "No Questions Added!!!");
            }
            else
            {
                Messages11.Visible = false;
            }
        }

        protected void btnRep_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            lblQueID.Text = "Que " + btn.Text + "";
            lblQueID.Visible = true;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            LinkButton lbtn = (LinkButton)sender;
            string qID = lbtn.CommandArgument;
            Session["QData"] = qID + "," + ddlCourse.SelectedValue + "," + ddlSub.SelectedValue;
            Session["editQuestion"] = "Yes";
            Response.Redirect("ManageQuestion.aspx");
        }
    }
}
