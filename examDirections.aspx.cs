﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class examDirections : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    btnStart.Text = "Start";
                    lblExamID.Text = Session["examID"].ToString();
                    lbluserID.Text = Session["userID"].ToString();

                    lblInstructions.Text = ob.getValue("SELECT description from exam_startPages where coursePreExamID = (select preexamid from exm_newexam where newexm=  " + lblExamID.Text + ") ");
                    try
                    {
                        if (Session["ShowDir"].ToString() == "True")
                        {
                            Session.Remove("ShowDir");
                            btnStart.Text = "Resume";
                        }
                    }
                    catch { }
                }
            }
            catch { }
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            Session["CloseDir"] = "Yes";
            string script = "<script type=\"text/javascript\">callfun();</script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }
    }
}
