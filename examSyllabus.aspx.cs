﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class examSyllabus : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {


            ob.addSyllabus(txtHeading.Text, fckEditorDesc.Value);
            gridDetails.DataBind();
            fckEditorDesc.Value = "";
            txtHeading.Text = "";

        }

        protected void gridDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //int row = 0;
            //if (e.CommandName == "info")
            //{
            //    row = Convert.ToInt16(e.CommandArgument);
            //    Session["SMaterial"] = gridDetails.Rows[row].Cells[5].Text;
            //    ModalPopupExtender3.Show();

            //}

        }

        protected void gridDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Cells[4].Visible = false;
            //    e.Row.Cells[5].Visible = false;
            //}
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    e.Row.Cells[4].Visible = false;
            //    e.Row.Cells[5].Visible = false;
            //}
        }
    }
}
