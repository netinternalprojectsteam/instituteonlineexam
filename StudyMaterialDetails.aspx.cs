﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class StudyMaterialDetails : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {//Mheading, Mdetails, Mcategory
            try
            {
                string id = Session["SMaterial"].ToString();
                DataTable dt = new DataTable();
                dt = ob.getdataByID(id);
                lblHeading.Text = dt.Rows[0]["Mheading"].ToString();
                lblDesc.Text = dt.Rows[0]["Mdetails"].ToString();
                lbldate.Text = dt.Rows[0]["MAddDate"].ToString();
                LblCategory.Text = dt.Rows[0]["Mcategory"].ToString();
            }
            catch { }
        }
    }
}
