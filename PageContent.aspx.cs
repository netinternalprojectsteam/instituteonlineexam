﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class PageContent : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ddlInfoHead.Items.Insert(0, new ListItem("--Select--"));
                //ddlInfoHead.DataBind();
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            if (CKEditorControl1.Value.Length > 0 )
            {
                if (ddlInfoHead.SelectedItem.Text != "--Select--")
                {
                    ob.updatePageContent(lblID.Text, CKEditorControl1.Value);
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Page Containt Updated Successfully !");
                    CKEditorControl1.Value = "";
                    ddlInfoHead.Items.Clear();
                    ddlInfoHead.Items.Insert(0, new ListItem("--Select--"));
                    //
                    ddlInfoHead.DataBind();
                    ddlInfoHead.SelectedValue = lblID.Text;
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Select Page Name !");
                }
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Enter Proper Text !");
            }
        }

        protected void ddlInfoHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            DataTable dt = new DataTable();
            lblID.Text = ddlInfoHead.SelectedValue;
            dt = ob.getPageContent(ddlInfoHead.SelectedValue);
            CKEditorControl1.Value = dt.Rows[0]["PageDetails"].ToString();
            lblcontent.Text = dt.Rows[0]["PageDetails"].ToString();
            Panel2.Visible = true;
        }

        protected void lnkedit_Click(object sender, EventArgs e)
        {
            try
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
            }
            catch (Exception)
            {
            }
        }
    }
}
