﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class uData : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            rptMaterials.DataBind();
            rptRec.DataBind();
            rptAwareness.DataBind();
            rptLinks.DataBind();
            try
            {
                if (!IsPostBack)
                {
                    string Mcategory = Request.QueryString["Mcategory"];
                    if (Mcategory == "FREE_STUDY_MATERIAL")
                    {
                        rptLinks.Visible = false;
                        rptMaterials.Visible = true;
                        rptRec.Visible = false;
                        rptAwareness.Visible = false;
                        this.Page.Title = "FREE STUDY MATERIAL";
                    }
                    if (Mcategory == "CURRENT_JOBS")
                    {
                        rptLinks.Visible = false;
                        rptRec.Visible = true;
                        rptMaterials.Visible = false;
                        rptAwareness.Visible = false;
                        this.Page.Title = "CURRENT JOBS & RESULTS";
                    }
                    if (Mcategory == "BANKING_AWARENESS")
                    {
                        rptLinks.Visible = false;
                        rptAwareness.Visible = true;
                        rptMaterials.Visible = false;
                        rptRec.Visible = false;
                        this.Page.Title = "BANKING AWARENESS";
                    }
                    if (Mcategory == "IMPORTANT_LINKS")
                    {
                        rptLinks.Visible = true;
                        rptAwareness.Visible = false;
                        rptMaterials.Visible = false;
                        rptRec.Visible = false;
                        this.Page.Title = "IMPORTANT LINKS";
                    }
                }
            }
            catch { }
        }
    }
}
