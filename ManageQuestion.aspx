﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="ManageQuestion.aspx.cs" Inherits="OnlineExam.ManageQuestion" Title="Manage Questions" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="RichTextEditor" Namespace="AjaxControls" TagPrefix="cc2" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 16px;
        }
    </style>

    <script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
            document.getElementById("ctl00_ContentPlaceHolder1_Button2").click();     
       }
   
      function showMsg(value)
       {
            var msg="Question Number : "+value;
            alert(msg);
       } 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" /></div>
            <table class="style1">
                <tr>
                    <td align="center">
                        <h3>
                            Manage Questions</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lbluserId" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Label ID="lblInsId" runat="server" Text="" Visible="false"></asp:Label>
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Course :"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                        DataValueField="courseid" AutoPostBack="True" OnSelectedIndexChanged="ddlCourse_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                        SelectCommand="SELECT Distinct [courseid], [catid], [courseinfo] FROM [view_catCourseSub] where instituteId=@instituteId AND courseid is not null and subid is not null ">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                                <td align="right">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Subject :"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSubject" runat="server" DataSourceID="SqlDataSource1" DataTextField="subjectinfo"
                                        DataValueField="subid">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                        SelectCommand="SELECT Distinct [subid], [subjectinfo] FROM [view_catCourseSub] where subid is not null">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                        <asp:Accordion ID="Accordion1" runat="server" SelectedIndex="0" FadeTransitions="true"
                            TransitionDuration="250" FramesPerSecond="40" RequireOpenedPane="false" AutoSize="None"
                            HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                            ContentCssClass="accordionContent">
                            <Panes>
                                <asp:AccordionPane ID="AccordionPane6" runat="server">
                                    <Header>
                                        Question Information</Header>
                                    <Content>
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <asp:SqlDataSource ID="sourceHeading" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                                        SelectCommand="SELECT Distinct [infID], [infHeading] FROM [exm_queInfo] where instituteId=@instituteId">
                                                        <SelectParameters>
                                                            <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                    <asp:Label ID="Label3" runat="server" Text="Information Heading" Font-Bold="true"></asp:Label>
                                                    <asp:DropDownList ID="ddlHeading" runat="server" DataSourceID="sourceHeading" OnSelectedIndexChanged="ddlHeading_SelectedIndexChanged1"
                                                        AutoPostBack="true" DataTextField="infHeading" DataValueField="infID">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="Panel1" runat="server" Style="max-height: 350px;" ScrollBars="Auto">
                                                        <asp:Repeater ID="repQueInfo" runat="server">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("infContents") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccPan1" runat="server">
                                    <Header>
                                        Question</Header>
                                    <Content>
                                        <br />
                                        <asp:SqlDataSource ID="sourceInfoHead" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                            SelectCommand="SELECT Distinct [infID], [infHeading] FROM [exm_queInfo] where instituteId=@instituteId">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                        <asp:Label ID="Label5" runat="server" Text="Select Information Heading" Font-Bold="true"></asp:Label>
                                        <asp:DropDownList ID="ddlInfoHead" runat="server" DataSourceID="sourceInfoHead" DataTextField="infHeading"
                                            DataValueField="infID" ToolTip="Left blank if question is without information.">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:ImageButton ID="imgSearchInfo" ImageUrl="~/images/search.png" runat="server" />
                                        <br />
                                        <br />
                                        <FCKeditorV2:FCKeditor ID="CKEditorControl1" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                        <asp:ModalPopupExtender ID="modalSearchContents" runat="server" TargetControlID="imgSearchInfo"
                                            BackgroundCssClass="ModalPopupBG" PopupControlID="searchContents" Drag="true"
                                            CancelControlID="btnCanSearchInfo">
                                        </asp:ModalPopupExtender>
                                        <div id="searchContents" style="display: none;" class="popupConfirmation">
                                            <iframe id="Iframe2" frameborder="0" src="searchInfoContents.aspx" height="450px"
                                                width="800px"></iframe>
                                            <div class="popup_Buttons" style="display: none">
                                                <input id="btnOkSearchInfo" value="Done" type="button" />
                                                <input id="btnCanSearchInfo" value="Cancel" type="button" />
                                            </div>
                                        </div>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccPan2" runat="server">
                                    <Header>
                                        Option A</Header>
                                    <Content>
                                        <FCKeditorV2:FCKeditor ID="CKEOptionA" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                        <%-- <CKEditor:CKEditorControl ID="CKEOptionA" runat="server"></CKEditor:CKEditorControl>--%>
                                        <%-- <cc1:Editor ID="edOptionA" runat="server" />--%>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane1" runat="server">
                                    <Header>
                                        Option B</Header>
                                    <Content>
                                        <FCKeditorV2:FCKeditor ID="CKEOptionB" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                        <%--   <CKEditor:CKEditorControl ID="CKEOptionB" runat="server"></CKEditor:CKEditorControl>--%>
                                        <%--   <cc1:Editor ID="edOptionB" runat="server" />--%>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane2" runat="server">
                                    <Header>
                                        Option C</Header>
                                    <Content>
                                        <FCKeditorV2:FCKeditor ID="CKEOptionC" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                        <%-- <CKEditor:CKEditorControl ID="CKEOptionC" runat="server"></CKEditor:CKEditorControl>--%>
                                        <%--  <cc1:Editor ID="edOptionC" runat="server" />--%>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane3" runat="server">
                                    <Header>
                                        Option D</Header>
                                    <Content>
                                        <FCKeditorV2:FCKeditor ID="CKEOptionD" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                        <%-- <CKEditor:CKEditorControl ID="CKEOptionD" runat="server"></CKEditor:CKEditorControl>--%>
                                        <%-- <cc1:Editor ID="edOptionD" runat="server" />--%>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane7" runat="server">
                                    <Header>
                                        Option E</Header>
                                    <Content>
                                        <FCKeditorV2:FCKeditor ID="CKEOptionE" runat="server">
                                        </FCKeditorV2:FCKeditor>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane8" runat="server">
                                    <Header>
                                        Marks</Header>
                                    <Content>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" style="width: 300px">
                                                    <asp:Label ID="Label6" runat="server" Text="For Correct Answer"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtMarks" runat="server"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMarks"
                                                        FilterMode="ValidChars" ValidChars="0123456789.">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="Label7" runat="server" Text="Negative for Incorrect"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtNegMarks" runat="server" Text="0"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNegMarks"
                                                        FilterMode="ValidChars" ValidChars="0123456789.">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane4" runat="server">
                                    <Header>
                                        Answer</Header>
                                    <Content>
                                        <asp:DropDownList ID="edAnswer" runat="server">
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane ID="AccordionPane5" runat="server">
                                    <Header>
                                        Difficulty Level</Header>
                                    <Content>
                                        <asp:DropDownList ID="txtDiffLevel" runat="server">
                                            <asp:ListItem>Basic</asp:ListItem>
                                            <asp:ListItem>Normal</asp:ListItem>
                                            <asp:ListItem>Medium</asp:ListItem>
                                            <asp:ListItem>Hard</asp:ListItem>
                                        </asp:DropDownList>
                                    </Content>
                                </asp:AccordionPane>
                            </Panes>
                        </asp:Accordion>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add New" CssClass="simplebtn" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                            CssClass="simplebtn" OnClick="btnCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="questid"
                            DataSourceID="SqlDataSource2" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated"
                            Visible="False" CssClass="gridview">
                            <Columns>
                                <asp:BoundField DataField="questid" HeaderText="questid" InsertVisible="False" ReadOnly="True"
                                    SortExpression="questid" />
                                <asp:BoundField DataField="question" HeaderText="Question" HtmlEncode="False" SortExpression="question" />
                                <asp:BoundField DataField="option1" HeaderText="Option A" HtmlEncode="False" HtmlEncodeFormatString="False"
                                    SortExpression="option1" />
                                <asp:BoundField DataField="option2" HeaderText="Option B" HtmlEncode="False" HtmlEncodeFormatString="False"
                                    SortExpression="option2" />
                                <asp:BoundField DataField="option3" HeaderText="Option C" HtmlEncode="False" HtmlEncodeFormatString="False"
                                    SortExpression="option3" />
                                <asp:BoundField DataField="option4" HeaderText="Option D" HtmlEncode="False" HtmlEncodeFormatString="False"
                                    SortExpression="option4" />
                                <asp:BoundField DataField="answer" HeaderText="Answer" HtmlEncode="False" HtmlEncodeFormatString="False"
                                    SortExpression="answer" />
                                <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT questid, question, option1, option2, option3, option4, answer, subid, difflevel, userid, instituteId FROM view_examQuestions where instituteId=@instituteId">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:Label ID="lblQid" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
