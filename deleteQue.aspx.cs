﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class deleteQue : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            sourceGridQuestions.SelectCommand = "SELECT questid, CASE WHEN LEN(question)>80 THEN LEFT(question,80)+'...' ELSE question END as question, subjectinfo, courseinfo FROM view_QuestionDetails WHERE (question LIKE '%" + txtData.Text + "%' and instituteId=" + lblInsId.Text + ")";
            sourceGridQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
            // gridQuestions.DataBind();
            Repeater2.DataBind();
            //if (gridQuestions.Rows.Count > 0)
            //{
            //    for (int i = 0; i < gridQuestions.Rows.Count; i++)
            //    {
            //        Label lbl = (Label)gridQuestions.Rows[i].FindControl("lblGridLabel");
            //        lbl.Text = gridQuestions.Rows[i].Cells[2].Text;
            //    }
            //}
        }

        protected void gridQuestions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[2].Visible = false;
                // e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        protected void gridQuestions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //int row = 0;
            //if (e.CommandName == "Info")
            //{
            //    row = Convert.ToInt32(e.CommandArgument);
            //    string qID = gridQuestions.Rows[row].Cells[1].Text;
            //    queHolder.Visible = true;
            //    DataTable dt = new DataTable();
            //    dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext, mark, negativeDeduction FROM exm_questions where questid = " + qID + "");

            //    Repeater1.DataSource = dt;
            //    Repeater1.DataBind();

            //    if (dt.Rows[0]["Info"].ToString().Length > 0)
            //    {
            //        repQueInfo.DataSource = dt;
            //        repQueInfo.DataBind();
            //        Panel3.Visible = true;
            //    }
            //    else { Panel3.Visible = false; }
            //    ModalPopupExtender2.Show();
            //}
        }

        protected void btnInfo_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            queHolder.Visible = true;
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid,question, option1, option2, option3, option4,option5, infoID, case when infoID != '0' then (select infContents from exm_queInfo where infID = infoID) end as Info,'Add to Exam' as btntext, mark, negativeDeduction FROM view_examQuestions where questid = " + qID + " and instituteId=" + lblInsId.Text + "");

            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            if (dt.Rows[0]["Info"].ToString().Length > 0)
            {
                repQueInfo.DataSource = dt;
                repQueInfo.DataBind();
                Panel3.Visible = true;
            }
            else { Panel3.Visible = false; }
            ModalPopupExtender2.Show();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            DataTable dt = new DataTable();

            dt = ob.getData("SELECT  examID, examName, isnull((questionNos),'') + isnull((questionList),'') as list FROM view_examQuestionList WHERE (questionNos LIKE '%," + qID + ",%') OR (questionNos LIKE '%" + qID + ",%') OR (questionNos LIKE '%," + qID + "%') OR (questionList LIKE '%," + qID + ",%') OR (questionList LIKE '%" + qID + ",%') OR (questionList LIKE '%," + qID + "%')");
            gridExams.DataSource = dt;
            gridExams.DataBind();
            lblID.Text = qID;
            if (dt.Rows.Count > 0)
            {
                ModalPopupExtender1.Show();
            }
            else
            {
                ob.DeleteQuestion(lblID.Text);
                sourceGridQuestions.SelectCommand = "SELECT questid, CASE WHEN LEN(question)>80 THEN LEFT(question,80)+'...' ELSE question END as question, subjectinfo, courseinfo FROM view_QuestionDetails WHERE (question LIKE '%" + txtData.Text + "%' and instituteId=" + lblInsId.Text + ")";
                sourceGridQuestions.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                Repeater2.DataBind();

                string script = "<script>alert('Question Deleted Successfully!!!')</script>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }

        }

        protected void gridExams_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[0].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ob.DeleteQuestion(lblID.Text);
        }



    }
}
