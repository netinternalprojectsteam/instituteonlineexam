﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="ManageSubject.aspx.cs" Inherits="OnlineExam.ManageSubject" Title="Manage Subject" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table class="style1">
                <tr>
                    <td align="center" colspan="2">
                        <h3>
                            Manage Subject</h3>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Course :"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="SqlDataSource2" DataTextField="courseinfo"
                            DataValueField="courseid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT Distinct [courseid], [courseinfo] FROM [view_catCourseSub] where instituteId=@instituteId and courseid is not null">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Subject :"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSubject"
                            ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSubID" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="lblInsId" runat="server" Visible="False"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add New" CssClass="simplebtn" />
                        <asp:Button ID="btnCan" runat="server" Text="Cancel" CssClass="simplebtn" CausesValidation="false"
                            OnClick="btnCan_Click" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="subid"
                            DataSourceID="SqlDataSource1" OnRowCommand="GridView1_RowCommand" CssClass="gridview">
                            <Columns>
                                <asp:BoundField DataField="subid" HeaderText="subid" InsertVisible="False" ReadOnly="True"
                                    SortExpression="subid" />
                                <asp:BoundField DataField="courseinfo" HeaderText="courseinfo" SortExpression="courseinfo" />
                                <asp:BoundField DataField="subjectinfo" HeaderText="subjectinfo" SortExpression="subjectinfo" />
                                <asp:ButtonField ButtonType="Button" CommandName="remove" HeaderText="Delete" Text="Delete">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                    <ControlStyle CssClass="simplebtn" />
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                            SelectCommand="SELECT Distinct subid, courseinfo, subjectinfo FROM view_catCourseSub where instituteId=@instituteId and subid is not null">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lblInsId" Name="instituteId" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
