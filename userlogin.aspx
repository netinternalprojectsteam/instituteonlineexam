﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userlogin.aspx.cs" Inherits="OnlineExam.userlogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/jquery.fancybox-1.3.1.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="inline1" style="width: 400px;">
        <h2>
            Login Here</h2>
        <div class="clear">
        </div>
        <p>
            Dont Have an account? <a href="#" class="colr">Create one</a>, It’s Simple and free.</p>
        <div class="clear">
        </div>
        <a href="#" class="left">
            <img src="images/signup.gif" alt="" /></a> &nbsp;<a href="#" class="left"><img src="images/forgot.gif"
                alt="" /></a>
        <div class="clear marg_bot">
            &nbsp;</div>
       
        <ul class="forms">
            <li class="txt">Email ID</li>
            <li class="inputfield">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="bar"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email ID !"
                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </li>
        </ul>
        <ul class="forms">
            <li class="txt">Password</li>
            <li class="inputfield">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="bar"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
            </li>
        </ul>
        <ul class="forms marg_top">
            <li class="txt">
                <asp:CheckBox ID="chkRem" runat="server" Text="Remember Me" Visible="false" />
                <asp:Button ID="Button1" CssClass="simplebtn" runat="server" Text="Login" OnClick="Button1_Click" />
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
