﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="examInfo.aspx.cs" Inherits="OnlineExam.examInfo" Title="Exam Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
  function openNewWindow()
        {      
        window.open("Exam.aspx", "Exam", "location=1, width=auto, height=auto, resizable=no");
        }
    </script>

    <style type="text/css">
        .imgClass
        {
            border-radius: 20px;
            border: solid 2px black;
        }
        .OhLink
        {
            font-family: 'Times New Roman' , Times, serif;
            color: #46b1e1;
            font-weight: bold;
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblExamID" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
    <table width="100%">
        <tr>
            <td align="left">
                <div class="pageHeading">
                    Exam Information
                </div>
            </td>
            <td align="right">
                <a href='/myAccount.aspx' class="OhLink">My Profile</a> &nbsp;&nbsp; <a class="OhLink"
                    href='/testsList.aspx'>Test List</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Text="Sorry, the selected exam is not available !"
                    Font-Bold="true" Font-Size="Large" ForeColor="Red" Visible="false"></asp:Label>
                <asp:Label ID="lblError" runat="server" Text="Please select Exam Package to Continue!!!"
                    Font-Bold="true" Font-Size="Large" ForeColor="Red" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel1" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Repeater ID="rptExamInfo" runat="server" OnItemDataBound="rptExamInfo_ItemDataBound">
                                    <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td align="left" valign="bottom" colspan="3">
                                                    <asp:Label ID="lblTitle" Font-Bold="true" Font-Size="Large" runat="server" Text='<%# Eval("examName")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    &nbsp;
                                                </td>
                                                <td rowspan="8" align="right" style="border-left: solid  1px black">
                                                    <table>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                ( Insure that POP UP has been allowed to start the exam )
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <a href="abtSpecification.aspx">Click here for Browser Setting Information</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                            <td align="right">
                                                                <a href="HowTo.aspx">Click here for Exam Solving Directions</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Button ID="btnStart" runat="server" Text="Start Exam" Font-Bold="true" Font-Size="Large"
                                                                    Width="150px" Height="40px" CssClass="simplebtn" OnClick="btnStart_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlPaid" runat="server" Visible="false">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        Select Package
                                                    </td>
                                                    <td align="left">
                                                        <asp:RadioButtonList ID="rdbPaid" runat="server" DataTextField="exmPackageName" DataValueField="totID"
                                                            RepeatDirection="Vertical">
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select Exam Package to Continue!!!"
                                                            ControlToValidate="rdbPaid" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator><br />
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td align="left" style="width: 150px;">
                                                    Total Questions
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblDesc" runat="server" Text='<%# Eval("queNos")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Total Time
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("maxTime")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    No of Section
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("noOfSections")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Exam Category
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("coursrPackageID")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Negative Marking
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("negativeMarking")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="3">
                                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("eDesc")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
