﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class addMainMenu : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                    GridView1.DataBind();
                    ddmainMenu1.DataBind();
                    ddmainMenu1.Items.Insert(0, new ListItem(string.Empty, "0"));
                    if (ddmainMenu1.SelectedItem.Text.Length > 0)
                    {
                        DataTable dt = new DataTable();
                        dt = ob.getLink(Convert.ToInt32(ddmainMenu1.SelectedValue));
                        string link = dt.Rows[0][0].ToString();
                        int maxID = ob.getMaxID() + 1;
                        txtLink.Text = link + "?id=" + maxID.ToString();
                    }
                    else
                    {
                        txtLink.Text = "#";
                    }

                
            }
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            if (txtMenuName.Text.Length > 0)
            {
                string parentID = ddmainMenu1.SelectedValue;

                ob.insertFinalMenu(txtMenuName.Text, chkParent.Checked, FCKeditor1.Value, parentID, txtPreference.Text, txtLink.Text, chkvisible.Checked, chkfooter.Checked);
                messages1.Visible = true;
                messages1.setMessage(1, "Details added successfully !");
                Panel2.Visible = true;
                Panel1.Visible = false;
                btnNew.Visible = true;
                GridView1.DataBind();
                txtMenuName.Text = "";
                FCKeditor1.Value = "";
                chkParent.Checked = false;
                ddmainMenu1.ClearSelection();
                ddmainMenu1.DataBind();
                ddmainMenu1.Items.Insert(0, new ListItem(string.Empty, "0"));
                txtPreference.Text = "";
            }
            else
            {
                messages1.Visible = true;
                messages1.setMessage(0, "Enter proper menu name & page contents !");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtMenuName.Text.Length > 0)
            {
                string parentID = ddmainMenu1.SelectedValue;

                ob.updateFinalMenu(txtMenuName.Text, chkParent.Checked, FCKeditor1.Value, parentID, lblID.Text, txtPreference.Text, txtLink.Text, chkvisible.Checked, chkfooter.Checked);
                messages1.Visible = true;
                messages1.setMessage(1, "Details updated successfully !");
                Panel2.Visible = true;
                Panel1.Visible = false;
                btnNew.Visible = true;
                GridView1.DataBind();
                txtMenuName.Text = "";
                FCKeditor1.Value = "";
                chkParent.Checked = false;
                ddmainMenu1.ClearSelection();
                ddmainMenu1.DataBind();
                ddmainMenu1.Items.Insert(0, new ListItem(string.Empty, "0"));
                txtPreference.Text = "";
            }
            else
            {
                messages1.Visible = true;
                messages1.setMessage(0, "Enter proper menu name !");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Panel1.Visible = false; Panel2.Visible = true;
            messages1.Visible = false; btnNew.Visible = true;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            messages1.Visible = false;
            btnadd.Visible = true;
            btnCancel.Visible = true;
            btnUpdate.Visible = false;
            txtMenuName.Text = "";
            FCKeditor1.Value = "";
            txtPreference.Text = "";
            btnNew.Visible = false;
            ddmainMenu1.ClearSelection();
            ddmainMenu1.DataBind();
            ddmainMenu1.Items.Insert(0, new ListItem(string.Empty, "0"));
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt32(e.CommandArgument);
                lblID.Text = GridView1.Rows[row].Cells[1].Text;
                ddmainMenu1.ClearSelection();
                ddmainMenu1.DataBind();
                ddmainMenu1.Items.Insert(0, new ListItem(string.Empty, "0"));
                DataTable dt = new DataTable();
                dt = ob.getFinalMenu(Convert.ToInt32(lblID.Text));
                txtMenuName.Text = dt.Rows[0]["menuName"].ToString();
                FCKeditor1.Value = dt.Rows[0]["pageContents"].ToString();
                ddmainMenu1.SelectedValue = dt.Rows[0]["parentID"].ToString();
                chkParent.Checked = Convert.ToBoolean(dt.Rows[0]["isParent"].ToString());
                txtPreference.Text = dt.Rows[0]["preference"].ToString();
                txtLink.Text = dt.Rows[0]["link"].ToString();
                chkvisible.Checked = Convert.ToBoolean(dt.Rows[0]["notvisibleheader"].ToString());
                chkfooter.Checked = Convert.ToBoolean(dt.Rows[0]["displayinfooter"].ToString());
                Panel1.Visible = true;
                Panel2.Visible = false;
                btnUpdate.Visible = true;
                btnCancel.Visible = true;
                btnadd.Visible = false;
                messages1.Visible = false;
                btnNew.Visible = false;
            }
            if (e.CommandName == "remove")
            {

                lblID.Text = Convert.ToInt32(e.CommandArgument).ToString();
                int res = ob.deleteFinalMenu(Convert.ToInt32(lblID.Text));
                if (res == 1)
                {
                    messages1.setMessage(1, "Menu deleted successfully !");
                    messages1.Visible = true;
                }
                else
                {
                    messages1.setMessage(0, "Menu Already In Use !");
                    messages1.Visible = true;
                }
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
        }

        protected void ddMainMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddmainMenu1.SelectedItem.Text.Length > 0)
            {
                DataTable dt = new DataTable();
                dt=ob.getLink(Convert.ToInt32(ddmainMenu1.SelectedValue));
                string link = dt.Rows[0][0].ToString();
                int maxID = ob.getMaxID() + 1;
                txtLink.Text = link + "?id=" + maxID.ToString();
            }
            else
            {
                txtLink.Text = "#";
            }
        }
    }
}
