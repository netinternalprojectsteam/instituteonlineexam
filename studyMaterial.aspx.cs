﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class studyMaterial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rptMaterials.DataBind();
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                this.Page.MasterPageFile = "~/userMaster.master";
            }
            else
            {
                //this.Page.MasterPageFile = "~/site1.master";
                this.Page.MasterPageFile = "~/userMaster.master";
            }
        }
    }
}
