﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace OnlineExam
{
    public partial class getXML : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { ddlExam.DataBind(); }
        }

        protected void btnXml_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid,ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber],CorrectAns,infoID, negativeDeduction, mark, yourAns, selectedOption,  questioninfo,  infHeading, infContents,noOfSections,Ismarked FROM view_examQuestionsDetails where exmid = " + ddlExam.SelectedValue + "");
            DataSet ds = new DataSet("Question");
            ds.Tables.Add(dt);
            txtXML.Text = ds.GetXml();

           // txtXML.Text = txtXML.Text + "<br><br>" + ds.GetXmlSchema();
            StreamWriter xmlDoc = new StreamWriter(Server.MapPath("~/XMLFile1.xml"), false);
            // Apply the WriteXml method to write an XML document
            ds.WriteXml(xmlDoc);
            xmlDoc.Close();
        }
    }
}
