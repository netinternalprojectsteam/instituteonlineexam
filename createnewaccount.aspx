﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="createnewaccount.aspx.cs"
    Inherits="OnlineExam.createnewaccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div id="inline2" style="width: 400px;">
        <h2>
            Create New Account</h2>
        <div class="clear">
        </div>
        <p>
            Dont Have an account? <a href="#" class="colr">Create one</a>, It’s Simple and free.</p>
        <div class="clear">
        </div>
        <a href="#" class="left">
            <img src="images/signup.gif" alt="" /></a> &nbsp;<a href="#" class="left"><img src="images/forgot.gif"
                alt="" /></a>
        <div class="clear marg_bot">
            &nbsp;</div>
        <table width="100%">
            <tr>
                <td align="right">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                    <uc1:Messages1 ID="Messages11" runat="server" Visible="False" />
                </td>
            </tr>
            <%--<tr>
                <td align="right">
                    Course Name :
                </td>
                <td>
                    <asp:DropDownList ID="ddlCourse" runat="server" CssClass="bar" DataSourceID="SqlDataSource1"
                        DataTextField="courseinfo" DataValueField="courseid">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                        SelectCommand="SELECT [courseid], [courseinfo] FROM [exm_course]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Number Of Question :
                </td>
                <td>
                    <asp:DropDownList ID="ddlQNO" runat="server">
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td align="right">
                    Name :
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="bar" Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Email ID :
                </td>
                <td>
                    <asp:TextBox ID="txtEmailID" runat="server" CssClass="bar" Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailID"
                        ErrorMessage="*"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            runat="server" ErrorMessage="Invalid email !" 
                        ControlToValidate="txtEmailID" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Mobile No :
                </td>
                <td>
                    <asp:TextBox ID="txtMo" runat="server" CssClass="bar" Width="250px" MaxLength="12"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMo"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td align="right">
                    Password :
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="txtbox" Width="250px" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                </td>
                <td>
                    <asp:Button ID="Button1" CssClass="simplebtn" runat="server" Text="Sign Up Now" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
