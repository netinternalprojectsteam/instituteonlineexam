﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="getXML.aspx.cs" Inherits="OnlineExam.getXML" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT [examID], [examName] FROM [exm_existingExams]"></asp:SqlDataSource>
    <table width="100%">
        <tr>
            <td>
                <asp:DropDownList ID="ddlExam" runat="server" DataSourceID="sourceExams" DataTextField="examName"
                    DataValueField="examID">
                </asp:DropDownList>
                <asp:Button ID="btnXml" runat="server" Text="Get XML" OnClick="btnXml_Click" />
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="XMLFile1.xml" Target="_blank">HyperLink</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtXML" runat="server" TextMode="MultiLine" Height="500px" Width="700px"></asp:TextBox>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
