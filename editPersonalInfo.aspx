﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editPersonalInfo.aspx.cs"
    Inherits="OnlineExam.editPersonalInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Queston Details</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btn3').click();
        }
        
         function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
    </script>

    <style type="text/css">
        .completionList
        {
            border: solid 1px blue;
            margin: 0px;
            padding: 3px;
            height: 120px;
            overflow: auto;
            background-color: #f9f9ff; ;}
        .listItem
        {
            color: #000;
            list-style: none;
        }
        .itemHighlighted
        {
            background-color: #e5e5ff;
            cursor: pointer;
            list-style: none;
        }
    </style>
    <%-- <link href="css/style.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Personal Information
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:Panel ID="Panel1" runat="server">
            <center>
                <table>
                    <tr>
                        <td colspan="2" align="center">
                            <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label5" runat="server" Text="Institute" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlInstitute" runat="server" DataTextField="Institute" DataValueField="InstituteId"
                                Enabled="false">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlInstitute"
                                Display="Dynamic" ErrorMessage="Please Select Institute!" ForeColor="Red" InitialValue="0"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblImage" runat="server" Visible="false" Text="Label"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Name" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server" CssClass="txtbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtName"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="Email" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" Enabled="false" runat="server" CssClass="txtbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" Text="Mobile No" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMo" runat="server" MaxLength="12" CssClass="txtbox"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMo"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" Text="Choose Image" CssClass="myLabel"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
                            <asp:Button ID="btnUpdate" CssClass="simplebtn" runat="server" Text="Update Info"
                                OnClick="btnUpdate_Click" />
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
