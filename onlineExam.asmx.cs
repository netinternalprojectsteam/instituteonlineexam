﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Services;
using System.Net;
using System.Net.Mail;
using Newtonsoft.Json;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;


namespace OnlineExam
{
    /// <summary>
    /// Summary description for onlineExam
    /// </summary>
    [WebService(Namespace = "http://pvkexams.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class onlineExam : System.Web.Services.WebService
    {
        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString);

        public void openConnection()
        {
            try { cn.Open(); }
            catch { cn.Close(); cn.Open(); }
        }

        public string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void CreateUserAccount(string uname, string uemail, string mobileNo, string Add1, string Add2, string country, string state, string city, string Institute)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                string linkID1 = "0";
                string uid = "0";
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd2 = new SqlCommand();
                //cmd2.CommandText = "SELECT uid, uemail, password, IsActive,linkID FROM exam_users WHERE uemail='" + uemail + "'";
                cmd2.CommandText = "SELECT uid, uemail, password, IsActive,linkID, uname FROM exam_users WHERE uemail=@uemail and instituteId=@instituteId";
                cmd2.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                cmd2.Parameters.AddWithValue("uemail", uemail);
                cmd2.Parameters.AddWithValue("Institute", Institute);
                da.Fill(dt);
                cn.Close();
                if (dt.Rows.Count == 0)
                {
                    Random random = new Random();

                    string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    for (int i = 0; i < 6; i++)
                    {
                        int x = random.Next(0, 53);
                        sb.Append(array[x]);
                    }
                    linkID1 = sb.ToString();
                    openConnection();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "INSERT INTO exam_users (uname, uemail, password, mobileNo, Add1, Add2, country, state, city, imageName, linkID, instituteId) VALUES (@uname, @uemail, @password, @mobileNo, @Add1, @Add2, @country, @state, @city, @imageName, @linkID1, @Institute)";
                    cmd.Parameters.AddWithValue("uname", uname);
                    cmd.Parameters.AddWithValue("uemail", uemail);
                    cmd.Parameters.AddWithValue("password", "");
                    cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                    cmd.Parameters.AddWithValue("Add1", Add1);
                    cmd.Parameters.AddWithValue("Add2", Add2);
                    cmd.Parameters.AddWithValue("country", country);
                    cmd.Parameters.AddWithValue("state", state);
                    cmd.Parameters.AddWithValue("city", city);
                    cmd.Parameters.AddWithValue("imageName", "");
                    cmd.Parameters.AddWithValue("linkID1", linkID1);
                    cmd.Parameters.AddWithValue("Institute", Institute);
                    cmd.Connection = cn;
                    string j = cmd.ExecuteNonQuery().ToString();

                    cn.Close();
                    openConnection();
                    SqlCommand cmd1 = new SqlCommand("SELECT MAX(uid) FROM exam_users", cn);
                    uid = cmd1.ExecuteScalar().ToString();
                    cn.Close();

                    if (j == "1")
                    {
                        string result = sendmail(uname, uemail, linkID1, uid);
                        if (result == "true")
                        {
                            DataTable dtResponse = new DataTable();
                            dtResponse.Columns.Add("response", typeof(string));

                            DataRow dt2row = dtResponse.NewRow();
                            dt2row["response"] = "Success";

                            dtResponse.Rows.Add(dt2row);

                            List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                            Dictionary<string, object> rowRes = null;
                            foreach (DataRow rs in dtResponse.Rows)
                            {
                                rowRes = new Dictionary<string, object>();
                                foreach (DataColumn col in dtResponse.Columns)
                                {
                                    rowRes.Add(col.ColumnName, rs[col]);
                                }
                                rowsResponse.Add(rowRes);
                            }

                            DataTable dtResponse1 = new DataTable();
                            dtResponse1.Columns.Add("uid", typeof(string));

                            DataRow dt2row1 = dtResponse1.NewRow();
                            dt2row1["uid"] = uid;

                            dtResponse1.Rows.Add(dt2row1);

                            List<Dictionary<string, object>> rowsResponse1 = new List<Dictionary<string, object>>();
                            Dictionary<string, object> rowRes1 = null;
                            foreach (DataRow rs in dtResponse1.Rows)
                            {
                                rowRes1 = new Dictionary<string, object>();
                                foreach (DataColumn col in dtResponse1.Columns)
                                {
                                    rowRes1.Add(col.ColumnName, rs[col]);
                                }
                                rowsResponse1.Add(rowRes1);
                            }


                            var jobj1 = serializer.Serialize(new { rowsResponse });
                            var jobj2 = serializer.Serialize(new { CreateUserAccount = (rowsResponse1) });
                            var k = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(j);
                        }
                        else
                        {
                            DataTable dtResponse = new DataTable();
                            dtResponse.Columns.Add("response", typeof(string));

                            DataRow dt2row = dtResponse.NewRow();
                            dt2row["response"] = "Fail";

                            dtResponse.Rows.Add(dt2row);

                            List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                            Dictionary<string, object> rowRes = null;
                            foreach (DataRow rs in dtResponse.Rows)
                            {
                                rowRes = new Dictionary<string, object>();
                                foreach (DataColumn col in dtResponse.Columns)
                                {
                                    rowRes.Add(col.ColumnName, rs[col]);
                                }
                                rowsResponse.Add(rowRes);
                            }

                            DataTable dtRes = new DataTable();
                            dtRes.Columns.Add("result", typeof(string));
                            DataRow dt1row = dtRes.NewRow();
                            dt1row["result"] = "No details";

                            dtRes.Rows.Add(dt1row);

                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row = null;
                            foreach (DataRow rs in dtRes.Rows)
                            {
                                row = new Dictionary<string, object>();
                                foreach (DataColumn col in dtRes.Columns)
                                {
                                    row.Add(col.ColumnName, rs[col]);
                                }
                                rows.Add(row);
                            }

                            var jobj1 = serializer.Serialize(new { rowsResponse });
                            var jobj2 = serializer.Serialize(new { CreateUserAccount = (j) });
                            var k = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(j);
                        }



                    }
                }
                else
                {
                    if (dt.Rows[0]["IsActive"].ToString() == "False")
                    {
                        string result = sendmail(uname, uemail, linkID1, uid);
                        if (result == "true")
                        {
                            DataTable dtResponse = new DataTable();
                            dtResponse.Columns.Add("response", typeof(string));

                            DataRow dt2row = dtResponse.NewRow();
                            dt2row["response"] = "Success";

                            dtResponse.Rows.Add(dt2row);

                            List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                            Dictionary<string, object> rowRes = null;
                            foreach (DataRow rs in dtResponse.Rows)
                            {
                                rowRes = new Dictionary<string, object>();
                                foreach (DataColumn col in dtResponse.Columns)
                                {
                                    rowRes.Add(col.ColumnName, rs[col]);
                                }
                                rowsResponse.Add(rowRes);
                            }
                            var jobj1 = serializer.Serialize(new { rowsResponse });
                            var jobj2 = serializer.Serialize(new { CreateUserAccount = (rowRes) });
                            var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(j);
                        }
                        else
                        {
                            DataTable dtResponse = new DataTable();
                            dtResponse.Columns.Add("response", typeof(string));

                            DataRow dt2row = dtResponse.NewRow();
                            dt2row["response"] = "Fail";

                            dtResponse.Rows.Add(dt2row);

                            List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                            Dictionary<string, object> rowRes = null;
                            foreach (DataRow rs in dtResponse.Rows)
                            {
                                rowRes = new Dictionary<string, object>();
                                foreach (DataColumn col in dtResponse.Columns)
                                {
                                    rowRes.Add(col.ColumnName, rs[col]);
                                }
                                rowsResponse.Add(rowRes);
                            }

                            DataTable dtRes = new DataTable();
                            dtRes.Columns.Add("result", typeof(string));
                            DataRow dt1row = dtRes.NewRow();
                            dt1row["result"] = result;

                            dtRes.Rows.Add(dt1row);

                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row = null;
                            foreach (DataRow rs in dtRes.Rows)
                            {
                                row = new Dictionary<string, object>();
                                foreach (DataColumn col in dtRes.Columns)
                                {
                                    row.Add(col.ColumnName, rs[col]);
                                }
                                rows.Add(row);
                            }

                            var jobj1 = serializer.Serialize(new { rowsResponse });
                            var jobj2 = serializer.Serialize(new { CreateUserAccount = (rows) });
                            var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(j);
                        }


                    }

                    else
                    {

                        DataTable dtResponse = new DataTable();
                        dtResponse.Columns.Add("response", typeof(string));

                        DataRow dt2row = dtResponse.NewRow();
                        dt2row["response"] = "Fail";

                        dtResponse.Rows.Add(dt2row);

                        List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes = null;
                        foreach (DataRow rs in dtResponse.Rows)
                        {
                            rowRes = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse.Columns)
                            {
                                rowRes.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse.Add(rowRes);
                        }

                        DataTable dtRes = new DataTable();
                        dtRes.Columns.Add("result", typeof(string));
                        DataRow dt1row = dtRes.NewRow();
                        dt1row["result"] = "No details";

                        dtRes.Rows.Add(dt1row);

                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row = null;
                        foreach (DataRow rs in dtRes.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dtRes.Columns)
                            {
                                row.Add(col.ColumnName, rs[col]);
                            }
                            rows.Add(row);
                        }

                        var jobj1 = serializer.Serialize(new { rowsResponse });
                        var jobj2 = serializer.Serialize(new { CreateUserAccount = (rows) });
                        var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(j);

                    }
                }
            }
            catch (Exception ee)
            {
                DataTable dtResponse = new DataTable();
                dtResponse.Columns.Add("response", typeof(string));

                DataRow dt2row = dtResponse.NewRow();
                dt2row["response"] = "Fail";

                dtResponse.Rows.Add(dt2row);

                List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                Dictionary<string, object> rowRes = null;
                foreach (DataRow rs in dtResponse.Rows)
                {
                    rowRes = new Dictionary<string, object>();
                    foreach (DataColumn col in dtResponse.Columns)
                    {
                        rowRes.Add(col.ColumnName, rs[col]);
                    }
                    rowsResponse.Add(rowRes);
                }

                DataTable dtRes = new DataTable();
                dtRes.Columns.Add("result", typeof(string));
                DataRow dt1row = dtRes.NewRow();
                dt1row["result"] = ee.Message;

                dtRes.Rows.Add(dt1row);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;
                foreach (DataRow rs in dtRes.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtRes.Columns)
                    {
                        row.Add(col.ColumnName, rs[col]);
                    }
                    rows.Add(row);
                }

                var jobj1 = serializer.Serialize(new { rowsResponse });
                var jobj2 = serializer.Serialize(new { CreateUserAccount = rows });
                var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(j);
            }
        }

        public string sendmail(string uname, string uemail, string linkID1, string uid)
        {
            try
            {
                string link = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/setPassword.aspx";
                string mail = "Hello " + uname + "<br> Thank you for using the pvkexams.com.<br>To complete the registration process, please <a href='" + link + "?regID=" + linkID1 + "@" + uid + "'>Click Here</a><br><br>Regards<br><a href='pvkexams.com'>Team PVK Exams</a>'<br><br><b>ALL THE BEST</b>";


                //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"].ToString();
                //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"].ToString();
                //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                //SmtpClient Sc = new SmtpClient(smtpHost);
                //string port = ConfigurationManager.AppSettings["smtpPort"];
                //Sc.Port = Convert.ToInt32(port);

                //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(infoEID, uemail, "Profile Details at pgfunda.com", mail);
                //mm.IsBodyHtml = true;
                //System.Net.Mail.SmtpClient client = new SmtpClient();
                //client.Credentials = new System.Net.NetworkCredential(infoEID, infoPass);
                //client.Send(mm);
                System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                SmtpClient Sc = new SmtpClient(smtpHost);
                string port = ConfigurationManager.AppSettings["smtpPort"];
                Sc.Port = Convert.ToInt32(port);
                string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                Sc.Credentials = new NetworkCredential(infoEID, infoPass);
                Sc.EnableSsl = true;
                ms.From = new MailAddress(infoEID);
                ms.To.Add(uemail);

                ms.Subject = "Profile Details at pvkexams.com";
                ms.Body = mail;
                ms.IsBodyHtml = true;
                Sc.Send(ms);
                return "true";


            }
            catch (Exception ee)
            {
                return ee.Message;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetActiveUserLoginInfo(string uemail, string passwrdnew)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();
                string passwrd = base64Encode(passwrdnew);
                SqlCommand cmd = new SqlCommand();
                //cmd.CommandText = "SELECT uid, uemail, password, IsActive, instituteId FROM exam_users WHERE uemail=@uemail AND password=@passwrd AND IsActive = 'True'";

                cmd.CommandText = "SELECT uid, uname,uemail, password, IsActive, instituteId, institute as instituteName From view_userByInstitute where uemail=@uemail and password=@passwrd and isActive='True'";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uemail", uemail);
                cmd.Parameters.AddWithValue("passwrd", passwrd);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetActiveUserLoginInfo = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetActiveUserLoginInfo = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetUserInfoByID(string uid)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT  uid,  uemail, uname, mobileNo,  imageName, password, city, edate, country, state, extraEmail, Add1, Add2, aboutU, exam_Institute.instituteId, institute FROM exam_users INNER JOIN exam_Institute on exam_users.InstituteId=exam_Institute.InstituteId WHERE uid = @uid ";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("uid", uid);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetUserInfoByID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetUserInfoByID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updatePersonalInfo(string uname, string mobileNo, string imageName, string uid)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE exam_users SET uname = @uname, mobileNo = @mobileNo, imageName = @imageName where uid = @uid";
                cmd.Parameters.AddWithValue("uname", uname);
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("imageName", imageName);
                cmd.Parameters.AddWithValue("uid", uid);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updatePersonalInfo = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updatePersonalInfo = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateContactDetails(string extraEmail, string Add1, string Add2, string city, string state, string country, string uid)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE exam_users SET extraEmail = @extraEmail, Add1 = @Add1, Add2 = @Add2, city = @city, state = @state, country = @country where uid = @uid";
                cmd.Parameters.AddWithValue("extraEmail", extraEmail);
                cmd.Parameters.AddWithValue("Add1", Add1);
                cmd.Parameters.AddWithValue("Add2", Add2);
                cmd.Parameters.AddWithValue("city", city);
                cmd.Parameters.AddWithValue("state", state);
                cmd.Parameters.AddWithValue("country", country);
                cmd.Parameters.AddWithValue("uid", uid);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateContactDetails = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateContactDetails = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updatePassword(string uid, string oldpassword, string password)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {

                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = "SELECT uid, password, uname FROM exam_users where uid =@uid ";
                cmd2.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                cmd2.Parameters.AddWithValue("uid", uid);
                da.Fill(dt);
                cn.Close();
                if (dt.Rows[0]["password"].ToString() == base64Encode(oldpassword))
                {
                    openConnection();
                    SqlCommand cmd = new SqlCommand();
                    string newpassword = base64Encode(password);
                    cmd.CommandText = "UPDATE exam_users SET password = @newpassword where uid = @uid ";
                    cmd.Parameters.AddWithValue("newpassword", newpassword);
                    cmd.Parameters.AddWithValue("uid", uid);
                    cmd.Connection = cn;
                    string i = cmd.ExecuteNonQuery().ToString();
                    cn.Close();
                    if (i == "1")
                    {

                        DataTable dtResponse = new DataTable();
                        dtResponse.Columns.Add("response", typeof(string));

                        DataRow dt2row = dtResponse.NewRow();
                        dt2row["response"] = "Success";

                        dtResponse.Rows.Add(dt2row);

                        List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes = null;
                        foreach (DataRow rs in dtResponse.Rows)
                        {
                            rowRes = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse.Columns)
                            {
                                rowRes.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse.Add(rowRes);
                        }


                        var jobj1 = serializer.Serialize(new { rowsResponse });
                        var jobj2 = serializer.Serialize(new { updatePassword = (i) });
                        var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(j);


                    }
                    else
                    {
                        DataTable dtResponse = new DataTable();
                        dtResponse.Columns.Add("response", typeof(string));

                        DataRow dt2row = dtResponse.NewRow();
                        dt2row["response"] = "Fail";

                        dtResponse.Rows.Add(dt2row);

                        List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes = null;
                        foreach (DataRow rs in dtResponse.Rows)
                        {
                            rowRes = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse.Columns)
                            {
                                rowRes.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse.Add(rowRes);
                        }

                        DataTable dtRes = new DataTable();
                        dtRes.Columns.Add("result", typeof(string));
                        DataRow dt1row = dtRes.NewRow();
                        dt1row["result"] = "No details";

                        dtRes.Rows.Add(dt1row);

                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row = null;
                        foreach (DataRow rs in dtRes.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dtRes.Columns)
                            {
                                row.Add(col.ColumnName, rs[col]);
                            }
                            rows.Add(row);
                        }

                        var jobj1 = serializer.Serialize(new { rowsResponse });
                        var jobj2 = serializer.Serialize(new { updatePassword = (i) });
                        var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(j);
                    }
                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "Old password not match !";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updatePassword = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExams(string instituteId, string examType)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                //cmd.CommandText = "SELECT examID, examName FROM exm_existingExams WHERE (isAvailable = 1) AND (section1Ques IS NOT NULL) and (instituteId=@instituteId) and (courseID=@courseID)";
                cmd.CommandText = "SELECT examID, examName FROM view_examDetails WHERE (isAvailable = 1) AND (section1Ques IS NOT NULL) AND (len(section1Ques)>0) and (instituteId=@instituteId) and (examType=@examType) and (paidID is not NULL)";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("instituteId", instituteId);

                cmd.Parameters.AddWithValue("examType", examType);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getExamDetailsByID(string examID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT examID, examName, 'Total Questions : ' + CONVERT(varchar(5), noofQuestions) AS noofQuestions,CASE WHEN negativeMarking = 'True' THEN 'Yes' Else 'No' end as  negativeMarking, coursrPackageID, noOfSections,CASE WHEN coursrPackageID = 'Computer' THEN 'comp.jpg' WHEN coursrPackageID = 'English' THEN 'english.jpg' WHEN coursrPackageID = 'General Awareness' THEN 'GenAwareness.jpg' WHEN coursrPackageID = 'Model Paper' THEN 'modal.jpg' WHEN coursrPackageID = 'Numerical' THEN 'numerical.jpg' WHEN coursrPackageID = 'Reasoning' THEN 'reasoning.jpg' END AS ImageName, eDesc, noofQuestions as queNos, coursrPackageID, maxTime FROM exm_existingExams WHERE IsAvailable = 'true' and examID = @examID";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("examID", examID);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getExamDetailsByID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getExamDetailsByID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void InsertUserQuestionList(string userID, string examID)//string billno, string MenuName, string Qty, string Rate, string tasteType)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT examID, examName,isnull(section1Ques,'') +','+ isnull(section2Ques,'') +','+ isnull(section3Ques,'') +','+ isnull(section4Ques,'' ) +','+isnull(section5Ques,'' ) as questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, examType FROM exm_existingExams where examID = " + examID + " ";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("examID", examID);

                da.Fill(dt);
                cn.Close();
                if (dt.Rows.Count > 0)
                {


                    openConnection();
                    SqlCommand cmd1 = new SqlCommand("select max(newexm)+1 from exm_newexam", cn);
                    string MaxBillNo = cmd1.ExecuteScalar().ToString();
                    cn.Close();
                    
                    string category = "Pre Exam";
                    openConnection();
                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.CommandText = "INSERT INTO exm_newExam (newexm, noquestion, questionlist, userID, onDate, maxTime, courseID, category, noOfSections, preexamID) VALUES (@MaxBillNo, @noquestion, @questionlist, @userID, getdate(), @maxTime, @courseID ,@category, @noOfSections, @examID)";
                    cmd2.Parameters.AddWithValue("MaxBillNo", MaxBillNo);
                    cmd2.Parameters.AddWithValue("noquestion", dt.Rows[0]["noofQuestions"].ToString());
                    cmd2.Parameters.AddWithValue("questionlist", dt.Rows[0]["questionNos"].ToString());
                    cmd2.Parameters.AddWithValue("userID", userID);
                    cmd2.Parameters.AddWithValue("maxTime", dt.Rows[0]["maxTime"].ToString());
                    cmd2.Parameters.AddWithValue("courseID", dt.Rows[0]["courseID"].ToString());
                    cmd2.Parameters.AddWithValue("category", category);
                    cmd2.Parameters.AddWithValue("noOfSections", dt.Rows[0]["noOfSections"].ToString());
                    cmd2.Parameters.AddWithValue("examID", examID);
                    cmd2.Connection = cn;
                    string i = cmd2.ExecuteNonQuery().ToString();

                    cn.Close();

                    if (i == "1")
                    {


                        string[] queNos = dt.Rows[0]["questionNos"].ToString().Split(',');
                        int noOfQuestions = Convert.ToInt32(dt.Rows[0]["noofQuestions"].ToString());
                        string answer = "0";
                        for (int ii = 0; ii < noOfQuestions; ii++)
                        {
                            if (queNos[ii].Length > 0)
                            {
                                openConnection();
                                SqlCommand cmd3 = new SqlCommand();
                                cmd3.CommandText = "INSERT INTO exm_userAnswers (userid, exmid, questno, answer, remainingTime) VALUES  (@userID, @MaxBillNo, @questno, @answer, @remainingTime)";
                                cmd3.Parameters.AddWithValue("userID", userID);
                                cmd3.Parameters.AddWithValue("MaxBillNo", MaxBillNo);
                                cmd3.Parameters.AddWithValue("questno", queNos[ii].ToString());
                                cmd3.Parameters.AddWithValue("answer", answer);
                                cmd3.Parameters.AddWithValue("remainingTime", dt.Rows[0]["maxTime"].ToString());

                                cmd3.Connection = cn;
                                string m = cmd3.ExecuteNonQuery().ToString();

                                cn.Close();
                            }
                        }


                        DataTable dtResponse = new DataTable();
                        dtResponse.Columns.Add("response", typeof(string));

                        DataRow dt2row = dtResponse.NewRow();
                        dt2row["response"] = "Success";

                        dtResponse.Rows.Add(dt2row);

                        List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes = null;
                        foreach (DataRow rs in dtResponse.Rows)
                        {
                            rowRes = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse.Columns)
                            {
                                rowRes.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse.Add(rowRes);
                        }

                        DataTable dtResponse1 = new DataTable();
                        dtResponse1.Columns.Add("MaxBillNo", typeof(string));

                        DataRow dt2row1 = dtResponse1.NewRow();
                        dt2row1["MaxBillNo"] = MaxBillNo;

                        dtResponse1.Rows.Add(dt2row1);

                        List<Dictionary<string, object>> rowsResponse1 = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes1 = null;
                        foreach (DataRow rs in dtResponse1.Rows)
                        {
                            rowRes1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse1.Columns)
                            {
                                rowRes1.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse1.Add(rowRes1);
                        }


                        var jobj1 = serializer.Serialize(new { rowsResponse });
                        var jobj2 = serializer.Serialize(new { InsertUserQuestionList = (rowsResponse1) });
                        var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(j);
                    }
                    else
                    {
                        DataTable dtResponse = new DataTable();
                        dtResponse.Columns.Add("response", typeof(string));

                        DataRow dt2row = dtResponse.NewRow();
                        dt2row["response"] = "Fail";

                        dtResponse.Rows.Add(dt2row);

                        List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                        Dictionary<string, object> rowRes = null;
                        foreach (DataRow rs in dtResponse.Rows)
                        {
                            rowRes = new Dictionary<string, object>();
                            foreach (DataColumn col in dtResponse.Columns)
                            {
                                rowRes.Add(col.ColumnName, rs[col]);
                            }
                            rowsResponse.Add(rowRes);
                        }

                        DataTable dtRes = new DataTable();
                        dtRes.Columns.Add("result", typeof(string));
                        DataRow dt1row = dtRes.NewRow();
                        dt1row["result"] = "No details";

                        dtRes.Rows.Add(dt1row);

                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row = null;
                        foreach (DataRow rs in dtRes.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dtRes.Columns)
                            {
                                row.Add(col.ColumnName, rs[col]);
                            }
                            rows.Add(row);
                        }

                        var jobj1 = serializer.Serialize(new { rowsResponse });
                        var jobj2 = serializer.Serialize(new { InsertUserQuestionList = (rows) });
                        var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(j);
                    }
                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { InsertUserQuestionList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee)
            {
                DataTable dtResponse = new DataTable();
                dtResponse.Columns.Add("response", typeof(string));

                DataRow dt2row = dtResponse.NewRow();
                dt2row["response"] = "Fail";

                dtResponse.Rows.Add(dt2row);

                List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                Dictionary<string, object> rowRes = null;
                foreach (DataRow rs in dtResponse.Rows)
                {
                    rowRes = new Dictionary<string, object>();
                    foreach (DataColumn col in dtResponse.Columns)
                    {
                        rowRes.Add(col.ColumnName, rs[col]);
                    }
                    rowsResponse.Add(rowRes);
                }

                DataTable dtRes = new DataTable();
                dtRes.Columns.Add("result", typeof(string));
                DataRow dt1row = dtRes.NewRow();
                dt1row["result"] = ee.Message;

                dtRes.Rows.Add(dt1row);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;
                foreach (DataRow rs in dtRes.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtRes.Columns)
                    {
                        row.Add(col.ColumnName, rs[col]);
                    }
                    rows.Add(row);
                }

                var jobj1 = serializer.Serialize(new { rowsResponse });
                var jobj2 = serializer.Serialize(new { InsertUserQuestionList = rows });
                var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(j);

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExamDescription(string maxExamID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT description from exam_startPages where coursePreExamID = (select preexamid from exm_newexam where newexm=@maxExamID)";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("maxExamID", maxExamID);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamDescription = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamDescription = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExamQue(string ExamID, string uid)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT questid, question, option1, option2, option3, option4, option5,infoID,infContents,selectedOption,Uname,noOfSections,isFinish,Ismarked,examName,section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM view_examQuestionsDetails where exmid =@ExamID and uid =@uid order by questid asc";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("ExamID", ExamID);
                cmd.Parameters.AddWithValue("uid", uid);
                da.Fill(dt);
                cn.Close();

                DataTable dt1 = new DataTable();
                if (dt.Rows.Count > 0)
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    dt1.Columns.Add("srno", typeof(string));
                    dt1.Columns.Add("questid", typeof(string));
                    dt1.Columns.Add("question", typeof(string));
                    dt1.Columns.Add("option1", typeof(string));
                    dt1.Columns.Add("option2", typeof(string));
                    dt1.Columns.Add("option3", typeof(string));
                    dt1.Columns.Add("option4", typeof(string));
                    dt1.Columns.Add("option5", typeof(string));
                    dt1.Columns.Add("infoID", typeof(string));
                    dt1.Columns.Add("infContents", typeof(string));
                    dt1.Columns.Add("selectedOption", typeof(string));
                    dt1.Columns.Add("Uname", typeof(string));
                    dt1.Columns.Add("noOfSections", typeof(string));
                    dt1.Columns.Add("isFinish", typeof(string));
                    dt1.Columns.Add("Ismarked", typeof(string));
                    dt1.Columns.Add("examName", typeof(string));
                    dt1.Columns.Add("section1Ques", typeof(string));
                    dt1.Columns.Add("section2Ques", typeof(string));
                    dt1.Columns.Add("section3Ques", typeof(string));
                    dt1.Columns.Add("section4Ques", typeof(string));
                    dt1.Columns.Add("section5Ques", typeof(string));
                    dt1.Columns.Add("isSection1", typeof(string));
                    dt1.Columns.Add("isSection2", typeof(string));
                    dt1.Columns.Add("isSection3", typeof(string));
                    dt1.Columns.Add("isSection4", typeof(string));
                    dt1.Columns.Add("isSection5", typeof(string));

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dt1Row = dt1.NewRow();
                        string srno = (i + 1).ToString();
                        dt1Row["srno"] = srno;
                        dt1Row["questid"] = dt.Rows[i]["questid"].ToString();
                        dt1Row["question"] = dt.Rows[i]["question"].ToString();
                        dt1Row["option1"] = dt.Rows[i]["option1"].ToString();
                        dt1Row["option2"] = dt.Rows[i]["option2"].ToString();
                        dt1Row["option3"] = dt.Rows[i]["option3"].ToString();
                        dt1Row["option4"] = dt.Rows[i]["option4"].ToString();
                        dt1Row["option5"] = dt.Rows[i]["option5"].ToString();
                        dt1Row["infoID"] = dt.Rows[i]["infoID"].ToString();
                        dt1Row["infContents"] = dt.Rows[i]["infContents"].ToString();
                        dt1Row["selectedOption"] = dt.Rows[i]["selectedOption"].ToString();
                        dt1Row["Uname"] = dt.Rows[i]["Uname"].ToString();
                        dt1Row["noOfSections"] = dt.Rows[i]["noOfSections"].ToString();
                        dt1Row["isFinish"] = dt.Rows[i]["isFinish"].ToString();
                        dt1Row["Ismarked"] = dt.Rows[i]["Ismarked"].ToString();
                        dt1Row["examName"] = dt.Rows[i]["examName"].ToString();
                        dt1Row["section1Ques"] = dt.Rows[i]["section1Ques"].ToString();
                        dt1Row["section2Ques"] = dt.Rows[i]["section2Ques"].ToString();
                        dt1Row["section3Ques"] = dt.Rows[i]["section3Ques"].ToString();
                        dt1Row["section4Ques"] = dt.Rows[i]["section4Ques"].ToString();
                        dt1Row["section5Ques"] = dt.Rows[i]["section5Ques"].ToString();

                        int sectionLength = 0;
                        string isLoad = "No";

                        string[] section1Ques = null;
                        if (dt.Rows[i]["section1Ques"].ToString().Length > 0)
                        {
                            section1Ques = dt.Rows[i]["section1Ques"].ToString().Split(',');
                            sectionLength = section1Ques.Length;
                        }
                        else
                        {
                            sectionLength = 0;
                        }

                        for (int j1 = 0; j1 < section1Ques.Length; j1++)
                        {
                            if (section1Ques[j1].Trim().Length > 0)
                            {
                                if (section1Ques[j1] == dt.Rows[i]["questid"].ToString())
                                {
                                    dt1Row["isSection1"] = "" + dt.Rows[i]["questid"].ToString() + ",Yes";
                                    dt1Row["isSection2"] = "No";
                                    dt1Row["isSection3"] = "No";
                                    dt1Row["isSection4"] = "No";
                                    dt1Row["isSection5"] = "No";

                                    isLoad = "No";
                                    break;
                                }
                                else
                                {
                                    isLoad = "Yes";
                                }
                            }
                        }

                        sectionLength = 0;
                        if (isLoad == "Yes")
                        {
                            string[] section2Ques = null;
                            if (dt.Rows[i]["section2Ques"].ToString().Length > 0)
                            {
                                section2Ques = dt.Rows[i]["section2Ques"].ToString().Split(',');
                                sectionLength = section2Ques.Length;
                            }
                            else
                            {
                                sectionLength = 0;
                            }

                            for (int k = 0; k < sectionLength; k++)
                            {
                                if (section2Ques[k].Trim().Length > 0)
                                {
                                    if (section2Ques[k] == dt.Rows[i]["questid"].ToString())
                                    {
                                        dt1Row["isSection1"] = "No";
                                        dt1Row["isSection2"] = "" + dt.Rows[i]["questid"].ToString() + ",Yes";
                                        dt1Row["isSection3"] = "No";
                                        dt1Row["isSection4"] = "No";
                                        dt1Row["isSection5"] = "No";

                                        isLoad = "No";
                                        break;
                                    }
                                    else
                                    {
                                        isLoad = "Yes";
                                    }
                                }
                            }

                            sectionLength = 0;

                            if (isLoad == "Yes")
                            {
                                string[] section3Ques = null;
                                if (dt.Rows[i]["section3Ques"].ToString().Length > 0)
                                {
                                    section3Ques = dt.Rows[i]["section3Ques"].ToString().Split(',');
                                    sectionLength = section3Ques.Length;
                                }
                                else
                                {
                                    sectionLength = 0;
                                }

                                for (int m = 0; m < sectionLength; m++)
                                {
                                    if (section3Ques[m].Trim().Length > 0)
                                    {
                                        if (section3Ques[m] == dt.Rows[i]["questid"].ToString())
                                        {
                                            dt1Row["isSection1"] = "No";
                                            dt1Row["isSection2"] = "No";
                                            dt1Row["isSection3"] = "" + dt.Rows[i]["questid"].ToString() + ",Yes";
                                            dt1Row["isSection4"] = "No";
                                            dt1Row["isSection5"] = "No";

                                            isLoad = "No";
                                            break;
                                        }
                                        else
                                        {
                                            isLoad = "Yes";
                                        }
                                    }

                                }

                                sectionLength = 0;

                                if (isLoad == "Yes")
                                {
                                    string[] section4Ques = null;
                                    if (dt.Rows[i]["section4Ques"].ToString().Length > 0)
                                    {
                                        section4Ques = dt.Rows[i]["section4Ques"].ToString().Split(',');
                                        sectionLength = section4Ques.Length;
                                    }
                                    else
                                    {
                                        sectionLength = 0;
                                    }

                                    for (int n = 0; n < sectionLength; n++)
                                    {
                                        if (section4Ques[n].Trim().Length > 0)
                                        {
                                            if (section4Ques[n] == dt.Rows[i]["questid"].ToString())
                                            {
                                                dt1Row["isSection1"] = "No";
                                                dt1Row["isSection2"] = "No";
                                                dt1Row["isSection3"] = "No";
                                                dt1Row["isSection4"] = "" + dt.Rows[i]["questid"].ToString() + ",Yes";
                                                dt1Row["isSection5"] = "No";

                                                isLoad = "No";
                                                break;
                                            }
                                            else
                                            {
                                                isLoad = "Yes";
                                            }
                                        }

                                    }

                                    sectionLength = 0;

                                    if (isLoad == "Yes")
                                    {
                                        string[] section5Ques = null;
                                        if (dt.Rows[i]["section5Ques"].ToString().Length > 0)
                                        {
                                            section5Ques = dt.Rows[i]["section5Ques"].ToString().Split(',');
                                            sectionLength = section5Ques.Length;
                                        }
                                        else
                                        {
                                            sectionLength = 0;
                                        }

                                        for (int p = 0; p < sectionLength; p++)
                                        {
                                            if (section5Ques[p].Trim().Length > 0)
                                            {
                                                if (section5Ques[p] == dt.Rows[i]["questid"].ToString())
                                                {
                                                    dt1Row["isSection1"] = "No";
                                                    dt1Row["isSection2"] = "No";
                                                    dt1Row["isSection3"] = "No";
                                                    dt1Row["isSection4"] = "No";
                                                    dt1Row["isSection5"] = "" + dt.Rows[i]["questid"].ToString() + ",Yes";

                                                    isLoad = "No";
                                                    break;
                                                }
                                                else
                                                {
                                                    isLoad = "Yes";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        dt1.Rows.Add(dt1Row);
                    }

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";
                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt1.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt1.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamQue = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamQue = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getExamQueAns(string ExamID, string uid)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT questid, answer,CorrectAns FROM view_examQuestionsDetails where exmid =@ExamID and uid =@uid ";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("ExamID", ExamID);
                cmd.Parameters.AddWithValue("uid", uid);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getExamQueAns = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getExamQueAns = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateExamStatus(string newexm)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE exm_newExam SET  isFinish = '1' WHERE (newexm = @newexm)";
                cmd.Parameters.AddWithValue("newexm", newexm);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateExamStatus = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateExamStatus = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateQueAnswer(string questNo, string exmid, string userid, string answer, string remainingTime, string timeSpent)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Update exm_userAnswers set answer = @answer , remainingTime = @remainingTime, timeSpent=timeSpent+@timeSpent where questNo = @questNo and userid = @userid and exmid = @exmid";
                cmd.Parameters.AddWithValue("questNo", questNo);
                cmd.Parameters.AddWithValue("exmid", exmid);
                cmd.Parameters.AddWithValue("userid", userid);
                cmd.Parameters.AddWithValue("answer", answer);
                cmd.Parameters.AddWithValue("remainingTime", remainingTime);
                cmd.Parameters.AddWithValue("timeSpent", timeSpent);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateQueAnswer = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateQueAnswer = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateRemTime(string exmid, string userid, string remainingTime)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd1 = new SqlCommand("select max(questNo) from exm_userAnswers where userid =" + userid + " and exmid = " + exmid + "", cn);
                string MaxqueNo = cmd1.ExecuteScalar().ToString();
                cn.Close();
                // string MaxqueNo = (Convert.ToInt32(billNo) + 1).ToString();


                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Update exm_userAnswers set remainingTime = @remainingTime, timeSpent=@timeSpent where questNo = @MaxqueNo and userid = @userid and exmid = @exmid";
                cmd.Parameters.AddWithValue("MaxqueNo", MaxqueNo);
                cmd.Parameters.AddWithValue("exmid", exmid);
                cmd.Parameters.AddWithValue("userid", userid);
                cmd.Parameters.AddWithValue("remainingTime", remainingTime);
                cmd.Parameters.AddWithValue("timeSpent", 0);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateRemTime = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateRemTime = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getCountryList()
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT DISTINCT [ID], [countryName] FROM [countryList] order by countryName ASC";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getCountryList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getCountryList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getStateList(string Country)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT DISTINCT [ID], [stateName], [countryID] FROM [stateList] where countryID = '" + Country + "' order by statename asc";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("Country", Country);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getStateList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getStateList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getInstituteList()
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select InstituteId, Institute from exam_Institute where isActive='true' order by Institute asc";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getInstituteList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getInstituteList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getCourseList(string InstituteId)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select * from view_catCourseSub where InstituteId=@InstituteId";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("InstituteId", InstituteId);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getCourseList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getCourseList = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExamResult(string uid, string newExamID)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dtUserDetails = new DataTable();
                openConnection();
                SqlCommand cmd0000 = new SqlCommand();
                cmd0000.CommandText = "SELECT uname, uemail, password, mobileNo, imageName FROM exam_users WHERE (uid = @uid)";
                cmd0000.Connection = cn;
                cmd0000.Parameters.AddWithValue("uid", uid);
                SqlDataAdapter sd = new SqlDataAdapter(cmd0000);
                sd.Fill(dtUserDetails);
                cn.Close();

                //get questions
                DataTable questions = new DataTable();
                questions.Columns.Add("questid", typeof(string));
                questions.Columns.Add("question", typeof(string));
                questions.Columns.Add("option1", typeof(string));
                questions.Columns.Add("option2", typeof(string));
                questions.Columns.Add("option3", typeof(string));
                questions.Columns.Add("option4", typeof(string));
                questions.Columns.Add("option5", typeof(string));
                questions.Columns.Add("answer", typeof(string));
                questions.Columns.Add("serialnumber", typeof(string));
                questions.Columns.Add("CorectAns", typeof(string));
                questions.Columns.Add("yourAns", typeof(string));
                questions.Columns.Add("IsCorrect", typeof(string));
                questions.Columns.Add("Answer1", typeof(string));
                questions.Columns.Add("timeSpent", typeof(string));
                questions.Columns.Add("questionNo", typeof(string));
                questions.Columns.Add("mark", typeof(string));
                questions.Columns.Add("negativeDeduction", typeof(string));
                questions.Columns.Add("timespentinSecond", typeof(string));

                DataTable dtGivenAns = new DataTable();
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT userid, exmid, questno, answer,CASE WHEN answer = '1' THEN (SELECT option1 FROM exm_questions WHERE questid = questno) WHEN answer = '2' THEN (SELECT option2 FROM exm_questions WHERE questid = questno) WHEN answer = '3' THEN (SELECT option3 FROM exm_questions WHERE questid = questno) WHEN answer = '4' THEN (SELECT option4 FROM exm_questions WHERE questid = questno) WHEN answer = '5' THEN (SELECT option5 FROM exm_questions WHERE questid = questno) WHEN answer = '0' THEN 'Not Solved' END AS Answer1,case when timespent > 60 then  convert(varchar(300), timespent/60) + ' min ' + convert(varchar(300), timespent%60)+' sec' else  convert(varchar(300), timespent) + ' sec' end as timeSpent, timespent as timespentinSecond FROM exm_userAnswers where exmid = @exmid and userid =@userid";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("exmid", newExamID);
                cmd.Parameters.AddWithValue("userid", uid);
                da.Fill(dtGivenAns);
                cn.Close();

                double totMarksreal = 0;

                for (int i = 0; i < dtGivenAns.Rows.Count; i++)
                {
                    DataTable dtQueDetails = new DataTable();
                    DataRow dtrow = questions.NewRow();
                    openConnection();
                    SqlCommand cmd1 = new SqlCommand();
                    cmd1.CommandText = "SELECT questid, question, option1, option2, option3, option4,option5, answer, case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '4' then exm_questions.option4  when exm_questions.answer = '5' then exm_questions.option5 end as CorectAns,mark, negativeDeduction FROM exm_questions where questid = " + dtGivenAns.Rows[i]["questno"].ToString() + "";
                    cmd1.Connection = cn;
                    SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                    cmd1.Parameters.AddWithValue("exmid", newExamID);
                    cmd1.Parameters.AddWithValue("userid", uid);
                    da1.Fill(dtQueDetails);
                    cn.Close();

                    try
                    {
                        dtrow["questid"] = dtQueDetails.Rows[0]["questid"].ToString();
                        dtrow["question"] = dtQueDetails.Rows[0]["question"].ToString();
                        dtrow["option1"] = dtQueDetails.Rows[0]["option1"].ToString();
                        dtrow["option2"] = dtQueDetails.Rows[0]["option2"].ToString();
                        dtrow["option3"] = dtQueDetails.Rows[0]["option3"].ToString();
                        dtrow["option4"] = dtQueDetails.Rows[0]["option4"].ToString();
                        dtrow["option5"] = dtQueDetails.Rows[0]["option5"].ToString();
                        dtrow["answer"] = dtQueDetails.Rows[0]["answer"].ToString();
                        dtrow["serialnumber"] = (i + 1).ToString();
                        dtrow["CorectAns"] = dtQueDetails.Rows[0]["CorectAns"].ToString();
                        dtrow["yourAns"] = dtGivenAns.Rows[i]["answer"].ToString();
                        dtrow["timeSpent"] = dtGivenAns.Rows[i]["timeSpent"].ToString();
                        dtrow["timespentinSecond"] = dtGivenAns.Rows[i]["timespentinSecond"].ToString();

                        if (dtQueDetails.Rows[0]["answer"].ToString() == dtGivenAns.Rows[i]["answer"].ToString())
                        {
                            dtrow["IsCorrect"] = "Correct";
                        }
                        else
                        {
                            dtrow["IsCorrect"] = "InCorrect";
                        }
                        if (dtGivenAns.Rows[i]["answer"].ToString() == "0")
                        {
                            dtrow["IsCorrect"] = "Not Solved";
                        }
                        dtrow["Answer1"] = dtGivenAns.Rows[i]["Answer1"].ToString();
                        dtrow["questionNo"] = "Question " + (i + 1).ToString();
                        dtrow["mark"] = dtQueDetails.Rows[0]["mark"].ToString();
                        dtrow["negativeDeduction"] = dtQueDetails.Rows[0]["negativeDeduction"].ToString();
                        questions.Rows.Add(dtrow);

                        totMarksreal += Convert.ToDouble(dtQueDetails.Rows[0]["mark"].ToString());
                    }
                    catch
                    { }
                }

                string IsSolved = "No";

                DataTable dtExamDetails = new DataTable();
                openConnection();
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = "Select preexamid from exm_newexam where newexm=@newexm union all select negativemarking from exm_existingexams where examid = (Select preexamid from exm_newexam where newexm=@newexm)";
                cmd2.Connection = cn;
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                cmd2.Parameters.AddWithValue("newexm", newExamID);
                da2.Fill(dtExamDetails);
                cn.Close();

                DataTable dtexamInfo = new DataTable();
                openConnection();
                SqlCommand cmd3 = new SqlCommand();
                cmd3.CommandText = "SELECT examID, examName, questionNos, maxTime, userID, noofQuestions, createdDate, courseID, isAvailable, noOfSections, negativeMarking, section1Ques,section2Ques,section3Ques,section4Ques,section5Ques FROM  exm_existingExams WHERE (examID =@examID)";
                cmd3.Connection = cn;
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                cmd3.Parameters.AddWithValue("examID", dtExamDetails.Rows[0][0].ToString());
                da3.Fill(dtexamInfo);
                cn.Close();
                string examName = "Exam Name : " + dtexamInfo.Rows[0]["examName"].ToString();

                double marks = 0;
                int correct = 0;
                int incorrect = 0;
                int totalTimeSpent = 0;

                for (int item = 0; item < questions.Rows.Count; item++)
                {
                    if (questions.Rows[item][11].ToString() != "Not Solved")
                    {
                        IsSolved = "Yes";
                    }
                    else
                    { }

                    if (questions.Rows[item]["IsCorrect"].ToString() == "Correct")
                    {
                        correct++;
                        marks = marks + Convert.ToDouble(questions.Rows[item][15].ToString());
                        totalTimeSpent = totalTimeSpent + Convert.ToInt32(questions.Rows[item][17].ToString());
                    }
                    if (questions.Rows[item]["IsCorrect"].ToString() == "InCorrect")
                    {
                        incorrect++;
                        totalTimeSpent = totalTimeSpent + Convert.ToInt32(questions.Rows[item][17].ToString());
                        if (dtExamDetails.Rows[1][0].ToString() == "true" || dtExamDetails.Rows[1][0].ToString() == "1")
                        {
                            marks = marks - Convert.ToDouble(questions.Rows[item][16].ToString());
                        }

                    }
                    if (questions.Rows[item]["IsCorrect"].ToString() == "Not Solved")
                    {
                        totalTimeSpent = totalTimeSpent + Convert.ToInt32(questions.Rows[item][17].ToString());
                    }

                }

                string timeTotal = "";
                if (totalTimeSpent > 3600)
                {
                    timeTotal = totalTimeSpent / 3600 + "Hrs";
                    int timespent1 = totalTimeSpent - 3600;
                    if (timespent1 > 60)
                    {
                        timeTotal = timeTotal + (timespent1 / 60).ToString() + " Mins  " + (timespent1 % 60).ToString() + " Sec";
                    }
                }
                if (totalTimeSpent > 60 && totalTimeSpent < 3600)
                {
                    timeTotal = timeTotal + (totalTimeSpent / 60).ToString() + " Mins  " + (totalTimeSpent % 60).ToString() + " Sec";
                }
                if (totalTimeSpent < 60)
                {
                    timeTotal = totalTimeSpent.ToString() + " Sec";
                }

                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("uname", typeof(string));
                dtResult.Columns.Add("examName", typeof(string));
                dtResult.Columns.Add("TotalQue", typeof(string));
                dtResult.Columns.Add("CorrectQue", typeof(string));
                dtResult.Columns.Add("IncorrectQue", typeof(string));
                dtResult.Columns.Add("SolvedQue", typeof(string));
                dtResult.Columns.Add("totMarks", typeof(string));
                dtResult.Columns.Add("TimeSpent", typeof(string));
                //dtResult.Columns.Add("answer", typeof(string));
                //dtResult.Columns.Add("serialnumber", typeof(string));
                //dtResult.Columns.Add("CorectAns", typeof(string));
                //dtResult.Columns.Add("yourAns", typeof(string));
                //dtResult.Columns.Add("IsCorrect", typeof(string));
                dtResult.Columns.Add("Percentile", typeof(string));
                dtResult.Columns.Add("accuracy", typeof(string));
                dtResult.Columns.Add("Percentage", typeof(string));

                DataRow dtrow1 = dtResult.NewRow();
                dtrow1["uname"] = dtUserDetails.Rows[0]["uname"].ToString();
                dtrow1["examName"] = examName;
                dtrow1["TotalQue"] = questions.Rows.Count.ToString();
                dtrow1["CorrectQue"] = correct.ToString();
                dtrow1["IncorrectQue"] = incorrect.ToString();
                dtrow1["SolvedQue"] = (correct + incorrect).ToString();
                dtrow1["totMarks"] = marks.ToString();
                dtrow1["TimeSpent"] = timeTotal;
                //dtrow1["serialnumber"] = "";
                //dtrow1["CorectAns"] = "";
                //dtrow1["yourAns"] = "";
                //dtrow1["IsCorrect"] = "";
                //dtResult.Rows.Add(dtrow1);

                DataTable dtExamResult = new DataTable();
                openConnection();
                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandText = "Select * from exam_userResult where userid =@userid and examid =@examID";
                cmd4.Connection = cn;
                SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
                cmd4.Parameters.AddWithValue("userid", uid);
                cmd4.Parameters.AddWithValue("examID", newExamID);
                da4.Fill(dtExamResult);
                cn.Close();

                if (dtExamResult.Rows.Count == 0)
                {
                    openConnection();
                    SqlCommand cmd5 = new SqlCommand();
                    cmd5.CommandText = "INSERT INTO exam_userResult (userid, examid, totalMarks, totalQue, solvedQue, correctQue, preexamID) VALUES (@userid, @examid, @totalMarks, @totalQue, @solvedQue, @correctQue, @preexamID)";
                    cmd5.Connection = cn;
                    SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                    cmd5.Parameters.AddWithValue("userid", uid);
                    cmd5.Parameters.AddWithValue("examid", newExamID);
                    cmd5.Parameters.AddWithValue("totalMarks", marks.ToString());
                    cmd5.Parameters.AddWithValue("totalQue", questions.Rows.Count.ToString());
                    cmd5.Parameters.AddWithValue("solvedQue", (correct + incorrect).ToString());
                    cmd5.Parameters.AddWithValue("correctQue", correct.ToString());
                    cmd5.Parameters.AddWithValue("preexamID", dtExamDetails.Rows[0][0].ToString());
                    int i1 = cmd5.ExecuteNonQuery();
                    cn.Close();

                    openConnection();
                    SqlCommand cmd6 = new SqlCommand();
                    cmd6.CommandText = "select max(id) from exam_userResult";
                    cmd6.Connection = cn;
                    string id = cmd6.ExecuteScalar().ToString();
                    cn.Close();


                    DataTable dtDetails = new DataTable();
                    openConnection();
                    SqlCommand cmd7 = new SqlCommand();
                    cmd7.CommandText = "select count(*) from exam_userresult where preexamid=@preexamid union all select count(id) from exam_userresult where totalmarks <= @totalmarks and preexamid=@preexamid and id !=@id";
                    cmd7.Connection = cn;
                    SqlDataAdapter da6 = new SqlDataAdapter(cmd7);
                    cmd7.Parameters.AddWithValue("preexamid", dtExamDetails.Rows[0][0].ToString());
                    cmd7.Parameters.AddWithValue("totalmarks", marks.ToString());
                    cmd7.Parameters.AddWithValue("id", id);
                    da6.Fill(dtDetails);
                    cn.Close();

                    double percentile = 0;
                    double percentage = 0;
                    string s1 = dtDetails.Rows[1][0].ToString();
                    string s2 = dtDetails.Rows[0][0].ToString();

                    if (IsSolved == "No") { percentile = 0; }
                    else
                    {
                        percentile = (Convert.ToDouble(s1) / Convert.ToDouble(s2)) * 100;
                    }
                    string totPercentile = string.Format("{0:n2}", percentile);

                    double accuracy = 0;
                    string totAccuracy = "0";
                    try
                    {
                        if (Convert.ToInt16(correct) > 0 && Convert.ToInt16((correct + incorrect).ToString()) > 0)
                        {
                            accuracy = (Convert.ToDouble(correct) / Convert.ToDouble((correct + incorrect).ToString())) * 100;
                            totAccuracy = string.Format("{0:n2}", accuracy);
                        }
                        else { totAccuracy = "0.0"; accuracy = 0; }
                    }
                    catch { accuracy = 0; }

                    try
                    {
                        percentage = (Convert.ToDouble(marks) / Convert.ToDouble(totMarksreal)) * 100;
                    }
                    catch { percentage = 0; }
                    if (percentage < 0)
                    {
                        percentage = 0;
                    }
                    else
                    { }
                    string totPercentage = string.Format("{0:n2}", percentage);

                    dtrow1["Percentile"] = totPercentile;
                    dtrow1["accuracy"] = totAccuracy;
                    dtrow1["Percentage"] = totPercentage;

                    openConnection();
                    SqlCommand cmd8 = new SqlCommand();
                    cmd8.CommandText = "UPDATE exam_userResult set percentile = @percentile , accuracy = @accuracy, totalTimeSpent=@totalTimeSpent where id = @id";
                    cmd8.Connection = cn;
                    cmd8.Parameters.AddWithValue("percentile", percentile);
                    cmd8.Parameters.AddWithValue("accuracy", accuracy);
                    cmd8.Parameters.AddWithValue("totalTimeSpent", totalTimeSpent);
                    cmd8.Parameters.AddWithValue("id", id);
                    int i2 = cmd8.ExecuteNonQuery();
                    cn.Close();

                    //to generate section wise details
                    try
                    {


                        if (Convert.ToInt32(dtexamInfo.Rows[0]["noOfSections"].ToString()) > 1)
                        {

                            DataTable dtSectionwiseDetails = new DataTable();
                            dtSectionwiseDetails.Columns.Add("sectionNo", typeof(string));
                            dtSectionwiseDetails.Columns.Add("totalQue", typeof(string));
                            dtSectionwiseDetails.Columns.Add("solvedQue", typeof(string));
                            dtSectionwiseDetails.Columns.Add("correctQue", typeof(string));
                            dtSectionwiseDetails.Columns.Add("incorrectQue", typeof(string));
                            dtSectionwiseDetails.Columns.Add("marks", typeof(string));
                            dtSectionwiseDetails.Columns.Add("accuracy", typeof(string));
                            dtSectionwiseDetails.Columns.Add("percentile", typeof(string));
                            dtSectionwiseDetails.Columns.Add("sessionTime", typeof(string));
                            dtSectionwiseDetails.Columns.Add("unSolved", typeof(string));
                            dtSectionwiseDetails.Columns.Add("topper1", typeof(string));
                            dtSectionwiseDetails.Columns.Add("topper2", typeof(string));

                            int questionInSection = 0;
                            int sectionCorrect = 0; int sectionIncorrect = 0; int sectionNotSolved = 0;
                            double sectionMarks = 0; int totalSectionQues = 0;

                            int queSum = 0;
                            int ques = 0;

                            for (int x = 1; x <= Convert.ToInt32(dtexamInfo.Rows[0]["noOfSections"].ToString()); x++)
                            {
                                questionInSection = 0;
                                ques = 0;
                                int fromQue = 0; int toQue = 0;
                                if (x == 1)//for section 1
                                {
                                    string section1Ques = dtexamInfo.Rows[0]["section1Ques"].ToString();
                                    string[] section1QuesNo = section1Ques.Split(',');
                                    for (int i = 0; i < section1QuesNo.Length; i++)
                                    {
                                        if (section1QuesNo[i].ToString().Length > 0)
                                        { ques = ques + 1; }

                                    }
                                    queSum = ques;
                                    fromQue = 0; toQue = ques;
                                    questionInSection = section1QuesNo.Length;
                                }
                                if (x == 2)//for section 2
                                {
                                    string section2Ques = dtexamInfo.Rows[0]["section2Ques"].ToString();
                                    string[] section2QuesNo = section2Ques.Split(',');
                                    for (int i = 0; i < section2QuesNo.Length; i++)
                                    {
                                        if (section2QuesNo[i].ToString().Length > 0)
                                        { ques = ques + 1; }

                                    }

                                    fromQue = queSum;
                                    toQue = queSum + ques; queSum = queSum + ques;
                                    questionInSection = section2QuesNo.Length;
                                }
                                if (x == 3)//for section 3
                                {
                                    string section3Ques = dtexamInfo.Rows[0]["section3Ques"].ToString();
                                    string[] section3QuesNo = section3Ques.Split(',');
                                    for (int i = 0; i < section3QuesNo.Length; i++)
                                    {
                                        if (section3QuesNo[i].ToString().Length > 0)
                                        { ques = ques + 1; }

                                    }

                                    fromQue = queSum; toQue = queSum + ques; queSum = queSum + ques;
                                    questionInSection = section3QuesNo.Length;
                                }
                                if (x == 4)//for section 4
                                {
                                    string section4Ques = dtexamInfo.Rows[0]["section4Ques"].ToString();
                                    string[] section4QuesNo = section4Ques.Split(',');
                                    for (int i = 0; i < section4QuesNo.Length; i++)
                                    {
                                        if (section4QuesNo[i].ToString().Length > 0)
                                        { ques = ques + 1; }

                                    }

                                    fromQue = queSum; toQue = queSum + ques; queSum = queSum + ques;
                                    questionInSection = section4QuesNo.Length;
                                }
                                if (x == 5)//for section 5
                                {
                                    string section5Ques = dtexamInfo.Rows[0]["section5Ques"].ToString();
                                    string[] section5QuesNo = section5Ques.Split(',');
                                    for (int i = 0; i < section5QuesNo.Length; i++)
                                    {
                                        if (section5QuesNo[i].ToString().Length > 0)
                                        { ques = ques + 1; }

                                    }

                                    fromQue = queSum; toQue = queSum + ques;
                                    queSum = queSum + ques;
                                    questionInSection = section5QuesNo.Length;
                                }

                                sectionCorrect = 0; sectionIncorrect = 0; sectionNotSolved = 0;
                                sectionMarks = 0; int timespent = 0; totalSectionQues = 0;

                                for (int z = fromQue; z < toQue; z++)
                                {
                                    if (questions.Rows[z]["IsCorrect"].ToString() == "Correct")
                                    {
                                        string abcd = questions.Rows[z][17].ToString();
                                        timespent = timespent + Convert.ToInt32(questions.Rows[z][17].ToString());
                                        sectionCorrect++;
                                        sectionMarks = sectionMarks + Convert.ToDouble(questions.Rows[z][15].ToString());
                                    }
                                    if (questions.Rows[z]["IsCorrect"].ToString() == "InCorrect")
                                    {
                                        string abcd = questions.Rows[z][17].ToString();
                                        timespent = timespent + Convert.ToInt32(questions.Rows[z][17].ToString());
                                        sectionIncorrect++;
                                        if (dtExamDetails.Rows[1][0].ToString() == "true" || dtExamDetails.Rows[1][0].ToString() == "1")
                                        {
                                            sectionMarks = sectionMarks - Convert.ToDouble(questions.Rows[z][16].ToString());
                                        }
                                    }
                                    if (questions.Rows[z]["IsCorrect"].ToString() == "Not Solved")
                                    {
                                        string abcd = questions.Rows[z][17].ToString();
                                        timespent = timespent + Convert.ToInt32(questions.Rows[z][17].ToString());
                                        sectionNotSolved = sectionNotSolved + 1;
                                    }

                                }

                                openConnection();
                                SqlCommand cmd9 = new SqlCommand();
                                cmd9.CommandText = "INSERT INTO exam_sectionWiseDetails (examID, userID, sectionNo, totalQue, solvedQue, correctQue, incorrectQue, marks, preExamID ) VALUES(@examID, @userID, @sectionNo, @totalQue, @solvedQue, @correctQue, @incorrectQue, @marks, @preExamID)";
                                cmd9.Connection = cn;
                                cmd9.Parameters.AddWithValue("examID", newExamID);
                                cmd9.Parameters.AddWithValue("userID", uid);
                                cmd9.Parameters.AddWithValue("sectionNo", x);
                                cmd9.Parameters.AddWithValue("totalQue", questionInSection);
                                cmd9.Parameters.AddWithValue("solvedQue", sectionCorrect + sectionIncorrect);
                                cmd9.Parameters.AddWithValue("correctQue", sectionCorrect);
                                cmd9.Parameters.AddWithValue("incorrectQue", sectionIncorrect);
                                cmd9.Parameters.AddWithValue("marks", sectionMarks);
                                cmd9.Parameters.AddWithValue("preExamID", dtExamDetails.Rows[0][0].ToString());
                                int i3 = cmd9.ExecuteNonQuery();
                                cn.Close();

                                openConnection();
                                SqlCommand cmd10 = new SqlCommand();
                                cmd10.CommandText = "select max(id) from exam_sectionWiseDetails";
                                cmd10.Connection = cn;
                                string id2 = cmd10.ExecuteScalar().ToString();
                                cn.Close();

                                DataTable dtEDetails = new DataTable();
                                openConnection();
                                SqlCommand cmd11 = new SqlCommand();
                                cmd11.CommandText = "select count(*) from exam_sectionWiseDetails where sectionNo=@sectionNo and preExamID=@preExamID union all select count(id) from exam_sectionWiseDetails where marks <= @marks and sectionNo=@sectionNo and preExamID=@preExamID and id !=@id";
                                cmd11.Connection = cn;
                                cmd11.Parameters.AddWithValue("sectionNo", x);
                                cmd11.Parameters.AddWithValue("preExamID", dtExamDetails.Rows[0][0].ToString());
                                cmd11.Parameters.AddWithValue("marks", sectionMarks);
                                cmd11.Parameters.AddWithValue("id", id2);
                                SqlDataAdapter da7 = new SqlDataAdapter(cmd11);
                                da7.Fill(dtEDetails);
                                cn.Close();

                                string s12 = dtEDetails.Rows[1][0].ToString();
                                string s13 = dtEDetails.Rows[0][0].ToString();
                                double percentile1 = 0; double accuracy1 = 0;

                                if (questionInSection == sectionNotSolved)
                                {
                                    percentile1 = 0;
                                    accuracy1 = 0;
                                }
                                else
                                {
                                    percentile1 = (Convert.ToDouble(s12) / Convert.ToDouble(s13)) * 100;
                                    accuracy1 = (Convert.ToDouble(sectionCorrect) / Convert.ToDouble(sectionCorrect + sectionIncorrect)) * 100;
                                }

                                string sessionTime = "";
                                if (timespent > 3600)
                                {
                                    sessionTime = timespent / 3600 + "Hrs";
                                    int timespent1 = timespent - 3600;
                                    if (timespent1 > 60)
                                    {
                                        sessionTime = sessionTime + (timespent1 / 60).ToString() + " Mins  " + (timespent1 % 60).ToString() + " Sec";
                                    }
                                }
                                if (timespent > 60 && timespent < 3600)
                                {
                                    sessionTime = sessionTime + (timespent / 60).ToString() + " Mins  " + (timespent % 60).ToString() + " Sec";
                                }
                                if (timespent < 60)
                                {
                                    sessionTime = timespent.ToString() + " Sec";
                                }
                                try
                                {
                                    //if (accuracy1 == Convert.ToDouble("NaN"))
                                    //{ accuracy1 = 0; }
                                    openConnection();
                                    SqlCommand cmd12 = new SqlCommand();
                                    cmd12.CommandText = "UPDATE exam_sectionWiseDetails set percentile = @percentile , accuracy = @accuracy, sessionTime = @sessionTime where id = @id";
                                    cmd12.Connection = cn;
                                    cmd12.Parameters.AddWithValue("percentile", percentile1);
                                    cmd12.Parameters.AddWithValue("accuracy", accuracy1);
                                    cmd12.Parameters.AddWithValue("sessionTime", Convert.ToInt32(timespent));
                                    cmd12.Parameters.AddWithValue("id", id2);
                                    int i4 = cmd12.ExecuteNonQuery();
                                    cn.Close();
                                }
                                catch
                                {
                                    openConnection();
                                    SqlCommand cmd121 = new SqlCommand();
                                    cmd121.CommandText = "UPDATE exam_sectionWiseDetails set percentile = @percentile , accuracy = @accuracy, sessionTime = @sessionTime where id = @id";
                                    cmd121.Connection = cn;
                                    cmd121.Parameters.AddWithValue("percentile", percentile1);
                                    cmd121.Parameters.AddWithValue("accuracy", 0);
                                    cmd121.Parameters.AddWithValue("sessionTime", Convert.ToInt32(timespent));
                                    cmd121.Parameters.AddWithValue("id", id2);
                                    int i41 = cmd121.ExecuteNonQuery();
                                    cn.Close();
                                }

                                DataTable dtsectiondetails = new DataTable();
                                openConnection();
                                SqlCommand cmd13 = new SqlCommand();
                                cmd13.CommandText = "select id,examid,isnull((select max(percentile) from exam_sectionwisedetails where sectionno=@sectionno and preexamid=@preexamid),0) as toper1,isnull((select max(percentile) from exam_sectionwisedetails where sectionno= @sectionno and preexamid=@preexamid and percentile<(select max(percentile) from exam_sectionwisedetails where sectionno=@sectionno and preexamid=@preexamid)),0) as toper2 from exam_sectionwisedetails where examid=@examid and userid=@userid";
                                cmd13.Connection = cn;
                                cmd13.Parameters.AddWithValue("sectionno", x);
                                cmd13.Parameters.AddWithValue("preexamid", dtexamInfo.Rows[0]["examID"].ToString());
                                cmd13.Parameters.AddWithValue("examid", newExamID);
                                cmd13.Parameters.AddWithValue("userid", uid);
                                SqlDataAdapter da8 = new SqlDataAdapter(cmd13);
                                da8.Fill(dtsectiondetails);
                                cn.Close();

                                DataRow dtrow = dtSectionwiseDetails.NewRow();
                                dtrow["sectionNo"] = "Section " + x.ToString();
                                dtrow["totalQue"] = questionInSection.ToString();
                                dtrow["solvedQue"] = (sectionCorrect + sectionIncorrect).ToString();
                                dtrow["correctQue"] = sectionCorrect.ToString();
                                dtrow["incorrectQue"] = sectionIncorrect.ToString();
                                dtrow["marks"] = sectionMarks.ToString();
                                dtrow["accuracy"] = string.Format("{0:n2}", accuracy1);//accuracy1.ToString();
                                dtrow["percentile"] = string.Format("{0:n2}", percentile1); //percentile1.ToString();
                                dtrow["sessionTime"] = sessionTime;
                                dtrow["unSolved"] = (questionInSection - (correct + incorrect)).ToString();
                                dtrow["topper1"] = dtsectiondetails.Rows[0]["toper1"].ToString();
                                dtrow["topper2"] = dtsectiondetails.Rows[0]["toper2"].ToString();
                                dtSectionwiseDetails.Rows.Add(dtrow);
                            }

                        }
                        else
                        {

                        }
                        DataTable dtExamResult1 = new DataTable();
                        openConnection();
                        SqlCommand cmd41 = new SqlCommand();
                        cmd41.CommandText = "Select * from exam_userResult where userid =@userid and examid =@examID";
                        cmd41.Connection = cn;
                        SqlDataAdapter da41 = new SqlDataAdapter(cmd41);
                        cmd41.Parameters.AddWithValue("userid", uid);
                        cmd41.Parameters.AddWithValue("examID", newExamID);
                        da41.Fill(dtExamResult1);
                        cn.Close();
                        string messagebody = "<table border='1' width='100%'><tr><td align='center'><span class='myLabelHead'>Total Questions</span></td><td align='center'><span class='myLabelHead'>Questions Attempted</span></td><td align='center'><span class='myLabelHead'>Correct Answers</span></td><td align='center'><span class='myLabelHead'>Incorrect Answers</span></td></tr><tr><td align='center'><span class='myLabel'>" + questions.Rows.Count.ToString() + "</span></td><td align='center'><span class='myLabel'>" + (correct + incorrect).ToString() + "</span></td><td align='center'><span class='myLabel'>" + correct + "</span></td><td align='center'><span class='myLabel'>" + incorrect + "</span></td></tr><tr><td colspan='4' style='height: 2px; background-color: Blue'></td></tr><tr><td align='center'><span class='myLabelHead'>Total Marks</span></td><td align='center'><span class='myLabelHead'>Percentile</span></td><td align='center'><span class='myLabelHead'>Accuracy</span></td><td  align='center'><span class='myLabelHead'>Total Time</span></td></tr><tr><td align='center'><span class='myLabel'>" + marks.ToString() + "</span></td><td align='center'><span class='myLabel'>" + dtExamResult1.Rows[0]["percentile"].ToString() + "</span></td><td align='center'><span class='myLabel'>" + dtExamResult1.Rows[0]["accuracy"].ToString() + "</span></td><td align='center'><span class='myLabel'>" + totalTimeSpent + "</span></td></tr></table>";

                        string mailBody = "Hello " + dtUserDetails.Rows[0]["uname"].ToString() + "<br> Your score for <b>" + dtexamInfo.Rows[0]["examName"].ToString() + "</b> is as follows<br><br>";
                        //mailBody = mailBody + messagebody;

                        //string resultEID = ConfigurationManager.AppSettings["ResultEmailID"].ToString();
                        //string resultPass = ConfigurationManager.AppSettings["ResultEmailPass"].ToString();

                        //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(resultEID, dtUserDetails.Rows[0]["uemail"].ToString(), "Result Details at pgfunda.com", mailBody);
                        //mm.IsBodyHtml = true;
                        //System.Net.Mail.SmtpClient client = new SmtpClient();
                        //client.Credentials = new System.Net.NetworkCredential(resultEID, resultPass);
                        //client.Send(mm);

                        System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                        string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                        SmtpClient Sc = new SmtpClient(smtpHost);
                        string port = ConfigurationManager.AppSettings["smtpPort"];
                        Sc.Port = Convert.ToInt32(port);
                        string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                        string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                        Sc.Credentials = new NetworkCredential(infoEID, infoPass);
                        Sc.EnableSsl = true;
                        ms.From = new MailAddress(infoEID);
                        ms.To.Add(dtUserDetails.Rows[0]["uemail"].ToString());

                        ms.Subject = "Result Details at pvkexams.com";
                        ms.Body = mailBody + messagebody;
                        ms.IsBodyHtml = true;
                        Sc.Send(ms);


                    }
                    catch { }



                }
                else
                {
                    dtrow1["Percentile"] = dtExamResult.Rows[0]["percentile"].ToString();
                    dtrow1["accuracy"] = dtExamResult.Rows[0]["accuracy"].ToString();
                    double percentage1 = Convert.ToDouble(marks) / Convert.ToDouble(totMarksreal) * 100;
                    if (percentage1 < 0)
                    {
                        percentage1 = 0;
                    }
                    else
                    { }
                    string totPercentage = string.Format("{0:n2}", percentage1);
                    dtrow1["Percentage"] = totPercentage;

                    dtrow1["CorrectQue"] = dtExamResult.Rows[0]["correctQue"].ToString();
                    dtrow1["InCorrectQue"] = (Convert.ToInt16(dtExamResult.Rows[0]["solvedQue"].ToString()) - Convert.ToInt16(dtExamResult.Rows[0]["correctQue"].ToString())).ToString();
                    dtrow1["SolvedQue"] = dtExamResult.Rows[0]["solvedQue"].ToString();
                    dtrow1["totMarks"] = dtExamResult.Rows[0]["totalMarks"].ToString();
                    dtrow1["timeSpent"] = dtExamResult.Rows[0]["totalTimeSpent"].ToString();



                }
                dtResult.Rows.Add(dtrow1);

                if (dtResult.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtResult.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResult.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamResult = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { GetExamResult = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

                DataTable dtResponse = new DataTable();
                dtResponse.Columns.Add("response", typeof(string));

                DataRow dt2row = dtResponse.NewRow();
                dt2row["response"] = "Fail";

                dtResponse.Rows.Add(dt2row);

                List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                Dictionary<string, object> rowRes = null;
                foreach (DataRow rs in dtResponse.Rows)
                {
                    rowRes = new Dictionary<string, object>();
                    foreach (DataColumn col in dtResponse.Columns)
                    {
                        rowRes.Add(col.ColumnName, rs[col]);
                    }
                    rowsResponse.Add(rowRes);
                }

                DataTable dtRes = new DataTable();
                dtRes.Columns.Add("result", typeof(string));
                DataRow dt1row = dtRes.NewRow();
                dt1row["result"] = ee.Message;

                dtRes.Rows.Add(dt1row);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;
                foreach (DataRow rs in dtRes.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtRes.Columns)
                    {
                        row.Add(col.ColumnName, rs[col]);
                    }
                    rows.Add(row);
                }

                var jobj1 = serializer.Serialize(new { rowsResponse });
                var jobj2 = serializer.Serialize(new { GetExamResult = rows });
                var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(j);

            }



        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserExamRemTime(string newExamID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select min(remainingtime) from exm_userAnswers where exmid =@newExamID";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("newExamID", newExamID);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserExamRemTime = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserExamRemTime = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserExamMaxTime(string newExamID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select maxTime from exm_newExam where newExm =@newExamID";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("newExamID", newExamID);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserExamMaxTime = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserExamMaxTime = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserwiseInstitude(string uemail, string passwrdnew)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();
                string password = base64Encode(passwrdnew);
                SqlCommand cmd = new SqlCommand();
                // cmd.CommandText = "SELECT uid, uemail,instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail and IsActive='True'";

                cmd.CommandText = "SELECT uid, uemail, instituteId, institute as instituteName from view_userByInstitute where uemail=@uemail and password=@password and isActive='true'";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uemail", uemail);
                cmd.Parameters.AddWithValue("password", password);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserwiseInstitude = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserwiseInstitude = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserInstitudeforgotpass(string uemail)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                // cmd.CommandText = "SELECT uid, uemail,instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail and IsActive='True'";

                cmd.CommandText = "SELECT uid, uemail, instituteId, institute as instituteName from view_userByInstitute where uemail=@uemail and isActive='true'";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uemail", uemail);

                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserInstitudeforgotpass = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserInstitudeforgotpass = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        //User Answer sheet

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserAnsSheet(string uid, string examID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                // cmd.CommandText = "SELECT uid, uemail,instituteId, (Select institute from exam_Institute where exam_Institute.InstituteId=exam_users.instituteId) as instituteName FROM exam_users WHERE uemail=@uemail and IsActive='True'";

                cmd.CommandText = "select questid,selectedOption,Case when selectedOption=1 then 'Selected' else 'UnSelected' end as Option1,Case when selectedOption=2 then 'Selected' else 'UnSelected' end as Option2,Case when selectedOption=3 then 'Selected' else 'UnSelected' end as Option3,Case when selectedOption=4 then 'Selected' else 'UnSelected' end as Option4,Case when selectedOption=5 then 'Selected' else 'UnSelected' end as Option5,case when Ismarked='marked' then 'yes' else 'No' end as Ismarked from view_examQuestionsDetails where exmid=" + examID + " and uid=" + uid
 + "";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uid", uid);
                cmd.Parameters.AddWithValue("examID", examID);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserAnsSheet = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserAnsSheet = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserFinishedExams(string uid)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, convert(varchar(20),exm_newExam.onDate,103) as onDate, exm_newExam.maxTime,  exm_existingExams.examName FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid INNER JOIN exm_existingExams ON exm_newExam.preexamID = exm_existingExams.examID WHERE (exm_newExam.userID = @uid) AND (exm_newExam.isFinish = 1) ORDER BY exm_newExam.newexm DESC";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uid", uid);
                da.Fill(dt);
                cn.Close();

                if (dt.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserFinishedExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserFinishedExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserUnFinishedExams(string uid)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt1 = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                //     cmd.CommandText = "SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, exm_newExam.onDate, exm_newExam.maxTime, (SELECT     MIN(remainingTime) AS Expr1 FROM exm_userAnswers WHERE  (exmid = exm_newExam.newexm)) AS remTime, exm_existingExams.examName FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid INNER JOIN exm_existingExams ON exm_newExam.preexamID = exm_existingExams.examID WHERE (exm_newExam.userID = " + uid + ") AND (exm_newExam.isFinish = 0) ORDER BY exm_newExam.newexm DESC";
                cmd.CommandText = "SELECT exm_newExam.newexm, exm_course.courseinfo, exm_newExam.noquestion, exm_newExam.onDate, exm_newExam.maxTime, isnull((SELECT     MIN(remainingTime) AS Expr1 FROM exm_userAnswers WHERE  (exmid = exm_newExam.newexm)),0) AS remTime,exm_existingExams.examName,exm_existingExams.noOfSections FROM exm_newExam INNER JOIN exm_course ON exm_newExam.courseID = exm_course.courseid INNER JOIN exm_existingExams ON exm_newExam.preexamID = exm_existingExams.examID WHERE (exm_newExam.userID = " + uid + ") AND (exm_newExam.isFinish = 0) ORDER BY exm_newExam.newexm DESC";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uid", uid);
                da.Fill(dt1);
                cn.Close();

                DataTable dtUnfinished = new DataTable();
                dtUnfinished.Columns.Add("newexm", typeof(string));
                dtUnfinished.Columns.Add("examName", typeof(string));
                dtUnfinished.Columns.Add("noquestion", typeof(string));
                dtUnfinished.Columns.Add("onDate", typeof(string));
                dtUnfinished.Columns.Add("maxTime", typeof(string));
                dtUnfinished.Columns.Add("remTime", typeof(string));
                dtUnfinished.Columns.Add("noOfSections", typeof(string));
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow dtrow = dtUnfinished.NewRow();
                    dtrow["newexm"] = dt1.Rows[i]["newexm"].ToString();
                    dtrow["examName"] = dt1.Rows[i]["examName"].ToString();
                    dtrow["noquestion"] = dt1.Rows[i]["noquestion"].ToString();
                    dtrow["onDate"] = dt1.Rows[i]["onDate"].ToString();
                    dtrow["maxTime"] = dt1.Rows[i]["maxTime"].ToString();
                    dtrow["noOfSections"] = dt1.Rows[i]["noOfSections"].ToString();
                    string s11111 = dt1.Rows[i]["remTime"].ToString();

                    //int remTime = 3600000;
                    DateTime remTime1 = DateTime.Now.AddMilliseconds(Convert.ToInt32(dt1.Rows[i]["remTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)remTime1) - DateTime.Now;
                    string r1 = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    dtrow["remTime"] = r1;
                    dtUnfinished.Rows.Add(dtrow);
                }

                if (dtUnfinished.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtUnfinished.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtUnfinished.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserUnFinishedExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getUserUnFinishedExams = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getForgotPass(string uemail, string instituteId)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt1 = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT uid, uemail, password, IsActive,linkID, uname FROM exam_users WHERE uemail=@uemail and instituteId=@instituteId";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("uemail", uemail);
                cmd.Parameters.AddWithValue("instituteId", instituteId);
                da.Fill(dt1);
                cn.Close();



                if (dt1.Rows.Count > 0)
                {
                    string mail = "Hello " + dt1.Rows[0]["uname"].ToString() + "<br> <br> Your Login details <br><b>Email : </b>" + dt1.Rows[0]["uemail"].ToString() + "<br><b>Pasword : </b>" + base64Decode(dt1.Rows[0]["password"].ToString()) + "<br><br>Regards<br><a href='http://pvkexams.com'>Team PVK Exams</a>'";

                    System.Net.Mail.MailMessage ms = new System.Net.Mail.MailMessage();
                    string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                    SmtpClient Sc = new SmtpClient(smtpHost);
                    string port = ConfigurationManager.AppSettings["smtpPort"];
                    Sc.Port = Convert.ToInt32(port);
                    string infoEID = ConfigurationManager.AppSettings["InfoEmailID"];
                    string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"];
                    Sc.Credentials = new NetworkCredential(infoEID, infoPass);
                    Sc.EnableSsl = true;
                    ms.From = new MailAddress(infoEID);
                    ms.To.Add(dt1.Rows[0]["uemail"].ToString());

                    ms.Subject = "Profile Details at pvkexams.com";
                    ms.Body = mail;
                    ms.IsBodyHtml = true;
                    Sc.Send(ms);


                    //string infoEID = ConfigurationManager.AppSettings["InfoEmailID"].ToString();
                    //string infoPass = ConfigurationManager.AppSettings["InfoEmailPass"].ToString();
                    //string smtpHost = ConfigurationManager.AppSettings["smtpServer"];
                    //SmtpClient Sc = new SmtpClient(smtpHost);
                    //string port = ConfigurationManager.AppSettings["smtpPort"];
                    //Sc.Port = Convert.ToInt32(port);

                    //System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(infoEID, dt1.Rows[0]["uemail"].ToString(), "Profile Details at pgfunda.com", mail)
                    //mm.IsBodyHtml = true;
                    //System.Net.Mail.SmtpClient client = new SmtpClient();
                    //client.Credentials = new System.Net.NetworkCredential(infoEID, infoPass);
                    //client.Send(mm);






                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Result", typeof(string));

                    DataRow dt3row = dtResult.NewRow();
                    dt3row["Result"] = "Please check your email...!";

                    dtResult.Rows.Add(dt3row);
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtResult.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResult.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getForgotPass = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { dtResponse = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        //For Paid User

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getPaidExamDetailsByExamID(string examID)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                DataTable dt1 = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = "Select ID, examID, examName, examFees, allowedAttempt, userID, ' Rs.'+ convert(varchar(20),examFees)+' (Allowed attempts:'+convert(varchar(20),allowedAttempt)+')' as exmPackageName,  (convert(varchar(10),ID))+':'+(convert(varchar(10),allowedAttempt))+':'+(convert(varchar(10),examFees)) as totID from view_paidExams where examID=@examID";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("examID", examID);
                da.Fill(dt1);
                cn.Close();




                if (dt1.Rows.Count > 0)
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dt1.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt1.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getPaidExamDetailsByExamID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getPaidExamDetailsByExamID = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void insertPaidExmUserDetails(string userId, string examId, string paidExmID, string exmAmt, string exmAttempt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {


                openConnection();
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = "INSERT into exm_paidExmUsers(userId, examId, paidExmID, exmAmt, exmAttempt) VALUES(@userId, @examId, @paidExmID, @exmAmt, @exmAttempt)";
                cmd2.Parameters.AddWithValue("userId", userId);
                cmd2.Parameters.AddWithValue("examId", examId);
                cmd2.Parameters.AddWithValue("paidExmID", paidExmID);
                cmd2.Parameters.AddWithValue("exmAmt", exmAmt);
                cmd2.Parameters.AddWithValue("exmAttempt", exmAttempt);

                cmd2.Connection = cn;
                string i = cmd2.ExecuteNonQuery().ToString();

                cn.Close();

                if (i == "1")
                {

                    openConnection();
                    SqlCommand cmd1 = new SqlCommand("Select max(transID) from exm_paidExmUsers", cn);
                    string MaxtranID = cmd1.ExecuteScalar().ToString();
                    cn.Close();

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtResponse1 = new DataTable();
                    dtResponse1.Columns.Add("MaxtranID", typeof(string));

                    DataRow dt2row1 = dtResponse1.NewRow();
                    dt2row1["MaxtranID"] = MaxtranID;

                    dtResponse1.Rows.Add(dt2row1);

                    List<Dictionary<string, object>> rowsResponse1 = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes1 = null;
                    foreach (DataRow rs in dtResponse1.Rows)
                    {
                        rowRes1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse1.Columns)
                        {
                            rowRes1.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse1.Add(rowRes1);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { insertPaidExmUserDetails = (rowsResponse1) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { insertPaidExmUserDetails = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }


            }
            catch (Exception ee)
            {
                DataTable dtResponse = new DataTable();
                dtResponse.Columns.Add("response", typeof(string));

                DataRow dt2row = dtResponse.NewRow();
                dt2row["response"] = "Fail";

                dtResponse.Rows.Add(dt2row);

                List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                Dictionary<string, object> rowRes = null;
                foreach (DataRow rs in dtResponse.Rows)
                {
                    rowRes = new Dictionary<string, object>();
                    foreach (DataColumn col in dtResponse.Columns)
                    {
                        rowRes.Add(col.ColumnName, rs[col]);
                    }
                    rowsResponse.Add(rowRes);
                }

                DataTable dtRes = new DataTable();
                dtRes.Columns.Add("result", typeof(string));
                DataRow dt1row = dtRes.NewRow();
                dt1row["result"] = ee.Message;

                dtRes.Rows.Add(dt1row);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = null;
                foreach (DataRow rs in dtRes.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtRes.Columns)
                    {
                        row.Add(col.ColumnName, rs[col]);
                    }
                    rows.Add(row);
                }

                var jobj1 = serializer.Serialize(new { rowsResponse });
                var jobj2 = serializer.Serialize(new { insertPaidExmUserDetails = rows });
                var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(j);

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getPaidExamStatus(string uid, string examId)
        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {


                //cmd.CommandText = "Select max(transId) as transId from exm_paidExmUsers where userId=@uid ";
                openConnection();
                SqlCommand cmd1 = new SqlCommand("Select max(transId) as transId from exm_paidExmUsers where userId=" + uid + " and examId=" + examId + "", cn);
                string MaxtranID = cmd1.ExecuteScalar().ToString();
                cn.Close();
                // string MaxqueNo = (Convert.ToInt32(billNo) + 1).ToString();


                DataTable dt1 = new DataTable();
                openConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select tStatus from exm_paidExmUsers where transId=" + MaxtranID + "";
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt1);
                cn.Close();




                if (dt1.Rows.Count > 0)
                {
                    string MaxNewExmID = "";
                    if (dt1.Rows[0]["tStatus"].ToString() == "Completed")
                    {
                        openConnection();
                        SqlCommand cmd112 = new SqlCommand("Select max(newexm) as newExmID from exm_newexam where userID=" + uid + " and preexamID=" + examId + "", cn);
                        MaxNewExmID = cmd112.ExecuteScalar().ToString();
                        cn.Close();

                    }
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtResponse1 = new DataTable();
                    dtResponse1.Columns.Add("tStatus", typeof(string));
                    dtResponse1.Columns.Add("MaxNewExmID", typeof(string));

                    DataRow dt2row1 = dtResponse1.NewRow();
                    dt2row1["tStatus"] = dt1.Rows[0]["tStatus"].ToString();
                    dt2row1["MaxNewExmID"] = MaxNewExmID;

                    dtResponse1.Rows.Add(dt2row1);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtResponse1.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse1.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }
                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getPaidExamStatus = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { getPaidExamStatus = (rows) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);

                }
            }
            catch (Exception ee)
            {

            }
        }


        [WebMethod]
        public void updateGCMId(string uid, string GCMId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                openConnection();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE exam_users SET  GCMId = @GCMId WHERE (uid = @uid)";
                cmd.Parameters.AddWithValue("GCMId", GCMId);
                cmd.Parameters.AddWithValue("uid", uid);
                cmd.Connection = cn;
                string i = cmd.ExecuteNonQuery().ToString();
                cn.Close();
                if (i == "1")
                {

                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Success";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }


                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateExamStatus = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);


                }
                else
                {
                    DataTable dtResponse = new DataTable();
                    dtResponse.Columns.Add("response", typeof(string));

                    DataRow dt2row = dtResponse.NewRow();
                    dt2row["response"] = "Fail";

                    dtResponse.Rows.Add(dt2row);

                    List<Dictionary<string, object>> rowsResponse = new List<Dictionary<string, object>>();
                    Dictionary<string, object> rowRes = null;
                    foreach (DataRow rs in dtResponse.Rows)
                    {
                        rowRes = new Dictionary<string, object>();
                        foreach (DataColumn col in dtResponse.Columns)
                        {
                            rowRes.Add(col.ColumnName, rs[col]);
                        }
                        rowsResponse.Add(rowRes);
                    }

                    DataTable dtRes = new DataTable();
                    dtRes.Columns.Add("result", typeof(string));
                    DataRow dt1row = dtRes.NewRow();
                    dt1row["result"] = "No details";

                    dtRes.Rows.Add(dt1row);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = null;
                    foreach (DataRow rs in dtRes.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dtRes.Columns)
                        {
                            row.Add(col.ColumnName, rs[col]);
                        }
                        rows.Add(row);
                    }

                    var jobj1 = serializer.Serialize(new { rowsResponse });
                    var jobj2 = serializer.Serialize(new { updateExamStatus = (i) });
                    var j = JsonConvert.SerializeObject(new[] { JsonConvert.DeserializeObject(jobj1), JsonConvert.DeserializeObject(jobj2) });
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(j);
                }
            }
            catch (Exception ee) { }


        }
    }
}
