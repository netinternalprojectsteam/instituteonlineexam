﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="uploadResult.aspx.cs" Inherits="OnlineExam.uploadResult" Title="Upload Offline Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel7" runat="server">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <h3 align="center">
                                Upload Offline Result</h3>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Panel ID="Panel8" runat="server">
                                <center>
                                    <table>
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:Label ID="lblMsg" Visible="false" runat="server" Text="Label"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="Label2" runat="server" Text="Class/Course" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlClass" runat="server">
                                                    <asp:ListItem>-SELECT-</asp:ListItem>
                                                    <asp:ListItem>FIRST YEAR</asp:ListItem>
                                                    <asp:ListItem>SECOND YEAR</asp:ListItem>
                                                    <asp:ListItem>THIRD YEAR MINOR</asp:ListItem>
                                                    <asp:ListItem>THIRD YEAR MAJOR</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="Label3" runat="server" Text="Exam Name" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtName" runat="server" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                    ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="Label1" runat="server" Text="Select file" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                            </td>
                                            <td align="left">
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="simplebtn" OnClick="btnUpload_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete Selected" OnClick="btnDelete_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                OnSelecting="sourceDetails_Selecting" SelectCommand="SELECT [id], [fileName], [forClass] FROM [site_offlineResult]">
                            </asp:SqlDataSource>
                            <br />
                            <asp:GridView ID="gridFiles" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                DataKeyNames="id" DataSourceID="sourceDetails" OnRowCreated="gridFiles_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                                        SortExpression="id" />
                                    <asp:BoundField DataField="fileName" HeaderText="Result File" SortExpression="fileName" />
                                    <asp:BoundField DataField="forClass" HeaderText="Class/Course" SortExpression="forClass" />
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
