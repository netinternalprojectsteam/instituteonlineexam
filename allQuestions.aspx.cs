﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class allQuestions : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblExamID.Text = Session["examID"].ToString();
                    lblUserID.Text = Session["userID"].ToString();
                    Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)Session["time"]) - DateTime.Now;
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    Timer1.Enabled = true;

                    DataTable dt = new DataTable();
                    dt = ob.GetUserInfoByID(lblUserID.Text);
                    lblName.Text = dt.Rows[0]["uname"].ToString();


                    DataTable dt1 = (DataTable)Session["queData"];
                    GridView1.DataSource = dt1;
                    GridView1.DataBind();

                    repQuestins.DataSource = dt1;
                    repQuestins.DataBind();

                    lblMsg.Text = "All Questions : " + repQuestins.Items.Count + "";
                }
            }
            catch { }
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                TimeSpan time1 = new TimeSpan();
                time1 = ((DateTime)Session["time"]) - DateTime.Now;
                if (time1.Minutes <= 0 && time1.Seconds <= 0)
                {
                    Label2.Text = "TimeOut!";
                    DataTable dt = (DataTable)ViewState["myTable"];
                    Session["dtable"] = dt;
                    //  Session["ans"] = lblGivenAns.Text;


                    Session["examID"] = lblExamID.Text;
                    Session["userID"] = lblUserID.Text;

                    ob.updateExamStatus(lblExamID.Text);
                    Response.Redirect("exmResult.aspx");
                }
                else
                {
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours, time1.Minutes.ToString(), time1.Seconds.ToString());
                }
            }
            catch { }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            Session["rTime"] = rTimeinMill;
            Timer1.Enabled = false;
            string script = "<script type=\"text/javascript\">callfun();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void linkGotoQue_Click(object sender, EventArgs e)
        {

            LinkButton btn = (LinkButton)sender;
            Session["qID"] = btn.CommandArgument;

            Session["JumpTo"] = "Yes";
            // string x = gridReview.Rows[row].Cells[0].Text;

            // Session["qNo"] = (row + 1).ToString();

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (GridView1.Rows[i].Cells[0].Text == btn.CommandArgument)
                {
                    Session["qNo"] = GridView1.Rows[i].Cells[11].Text;
                }
            }


            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            Session["rTime"] = rTimeinMill;
            Timer1.Enabled = false;


            //Session.Remove("JumpTo"); Session.Remove("qID"); Session.Remove("qNo");
            string script = "<script type=\"text/javascript\">callfun();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }

        protected void btnAnswered_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT  row_number() over(order by uid) as serialnumber, uid, questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid, infoID, negativeDeduction, mark, yourAns, selectedOption FROM view_examQuestionsDetails WHERE (exmid = " + lblExamID.Text + ") AND (selectedOption <> 0)");
            repAnswered.DataSource = dt;
            repAnswered.DataBind();
            lblMsg.Text = "Answered Questions : " + repAnswered.Items.Count + "";
            Panel1.Visible = false;
            Panel3.Visible = true;
        }

        protected void btnUnanswered_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT  row_number() over(order by uid) as serialnumber, uid, questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid, infoID, negativeDeduction, mark, yourAns,selectedOption   FROM view_examQuestionsDetails WHERE (exmid = " + lblExamID.Text + ") AND (selectedOption = 0)");
            repQuestins.DataSource = dt;
            repQuestins.DataBind();
            lblMsg.Text = "Unanswered Questions : " + repQuestins.Items.Count + "";
            Panel1.Visible = true;
            Panel3.Visible = false;
        }

        protected void btnMarked_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT row_number() over(order by uid) as serialnumber, questid, question FROM view_examMarkedQuestions WHERE (examid = " + lblExamID.Text + ")");
            repQuestins.DataSource = dt;
            repQuestins.DataBind();
            lblMsg.Text = "Marked Questions : " + repQuestins.Items.Count + "";
            Panel1.Visible = true;
            Panel3.Visible = false;
        }

        protected void btnAllQuestions_Click(object sender, EventArgs e)
        {
            DataTable dt1 = (DataTable)Session["queData"];
            repQuestins.DataSource = dt1;
            repQuestins.DataBind();
            lblMsg.Text = "All Questions : " + repQuestins.Items.Count + "";
            Panel1.Visible = true;
            Panel3.Visible = false;
        }
    }
}
