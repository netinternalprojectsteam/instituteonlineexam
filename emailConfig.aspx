﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="emailConfig.aspx.cs" Inherits="OnlineExam.emailConfig" Title="Email Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
function ShowDiv()
{
var formdiv = document.getElementById('editData');
formdiv.style.display='block';
var dispinf = document.getElementById('showOnly');
dispinf.style.display = 'none';
}
    </script>

    <script type="text/javascript">
//function HideDiv()
//{
//var formdiv = document.getElementById('editData');
//formdiv.style.display='block';
//var dispinf = document.getElementById('showOnly');
//dispinf.style.display = 'none';
//}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblID" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="lblInstituteId" runat="server" Text="Label" Visible="false"></asp:Label>
    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    Email Configuration
                </h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel1" runat="server" Visible="false">
                    <center>
                        <table>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Email Address"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtAdd" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAdd"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid ID !"
                                        ControlToValidate="txtAdd" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Password"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPass"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="SMTP Host"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtHost" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtHost"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="SMTP Port"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPort" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPort"
                                        ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="SMTP Enable"></asp:Label>
                                </td>
                                <td align="left" valign="top">
                                    <asp:CheckBox ID="chkEnable" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="btnCan" runat="server" Text="Cancel" CssClass="simplebtn" CausesValidation="false"
                                        OnClick="btnCan_Click" />
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
                <asp:Panel ID="Panel3" runat="server" Visible="true">
                    <center>
                        <table>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Email Address"></asp:Label>
                                </td>
                                <td rowspan="5" style="width: 15px">
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblEmail" runat="server" Font-Bold="True" Text="Email Address"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="SMTP Host"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblHost" runat="server" Font-Bold="True" Text="Email Address"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="SMTP Port"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblPort" runat="server" Font-Bold="True" Text="Email Address"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="SMTP Enable"></asp:Label>
                                </td>
                                <td align="left" valign="top">
                                    <asp:CheckBox ID="chkEnabled" runat="server" Enabled="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Style="text-decoration: underline;
                                        color: Blue" OnClick="Button1_Click">Edit</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </center>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel2" runat="server">
                    <asp:GridView ID="gridCnfig" runat="server" DataSourceID="sourceConfig" CssClass="gridview">
                    </asp:GridView>
                    <asp:SqlDataSource ID="sourceConfig" runat="server"></asp:SqlDataSource>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
