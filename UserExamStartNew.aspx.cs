﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class UserExamStartNew : System.Web.UI.Page
    {
        string[,] queAns;
        int i = 1;
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Timer1.Enabled = false;
                try
                {
                    if (Session["resExam"].ToString() == "Yes" || Session["resPreExam"].ToString() == "Yes")
                    {
                        try
                        {
                            Session.Remove("resExam");
                        }
                        catch { }
                        try { Session.Remove("resPreExam"); }
                        catch { }


                        lblPreviousTime.Text = DateTime.Now.ToString();
                        lblExamID.Text = Session["examID"].ToString();
                        lbluserID.Text = Session["userID"].ToString();

                        DataTable dtUserDetails = new DataTable();
                        dtUserDetails = obj.GetUserInfoByID(lbluserID.Text);
                        lblYourName.Text = dtUserDetails.Rows[0]["uname"].ToString();


                        Panel3.Visible = true;
                        //Panel2.Visible = false;

                        DataTable questions = new DataTable();
                        questions = obj.getData("SELECT questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid,ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber],CorrectAns,infoID, negativeDeduction, mark, yourAns, selectedOption,  questioninfo,  infHeading, infContents,noOfSections,Ismarked FROM view_examQuestionsDetails where exmid = " + lblExamID.Text + " and uid = " + lbluserID.Text + "");

                        int remTime = Convert.ToInt32(obj.getValue("select min(remainingtime) from exm_userAnswers where exmID= " + lblExamID.Text + ""));


                        Session["time"] = DateTime.Now.AddMilliseconds(remTime);
                        Label3.Text = "Started at &nbsp;" + DateTime.Now.ToString("h:mm tt");
                        Label3.Visible = false;
                        //Response.Write((DateTime)Session["time"]);
                        TimeSpan time1 = new TimeSpan();
                        time1 = ((DateTime)Session["time"]) - DateTime.Now;
                        Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                        Timer1.Enabled = true;
                        lblSelectedQuesNo.Text = "1";
                        lblQuestionNo.Text = questions.Rows[0][0].ToString();
                        Repeater1.DataSource = questions;
                        Repeater1.DataBind();

                        ViewState["myTable"] = questions;
                        loadQuestion();
                        ViewState["in"] = 1;

                        setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);




                        // GridView1.DataSource = questions;
                        // GridView1.DataBind();

                        //to show direction page first..
                        Panel7.Visible = false;
                        Panel6.Visible = false;
                        //Label2.Visible = false;
                        Label3.Visible = false;
                        //Label4.Visible = false;
                        Timer1.Enabled = false;
                        Session["rTime2"] = remTime;
                        modalShowDirections.Show();


                        string sections = questions.Rows[0]["noOfSections"].ToString();//obj.getValue("select noOfSections from exm_newExam where newExm = " + lblExamID.Text + "");

                        if (Convert.ToInt16(sections) > 1)
                        {
                            lblSections.Text = sections;
                            DataTable dtSections = new DataTable();
                            dtSections.Columns.Add("serialNo", typeof(string));
                            dtSections.Columns.Add("SectioNo", typeof(string));
                            dtSections.Columns.Add("number", typeof(string));

                            for (int z = 1; z <= Convert.ToInt16(sections); z++)
                            {
                                DataRow dtrow = dtSections.NewRow();
                                dtrow["serialNo"] = z.ToString();
                                dtrow["SectioNo"] = "Section " + z.ToString();
                                if (z == 1) { dtrow["number"] = "I"; }
                                if (z == 2) { dtrow["number"] = "II"; }
                                if (z == 3) { dtrow["number"] = "III"; }
                                if (z == 4) { dtrow["number"] = "IV"; }
                                if (z == 5) { dtrow["number"] = "V"; }
                                if (z == 6) { dtrow["number"] = "VI"; }
                                if (z == 7) { dtrow["number"] = "VII"; }
                                if (z == 8) { dtrow["number"] = "VIII"; }
                                if (z == 9) { dtrow["number"] = "IX"; }
                                if (z == 10) { dtrow["number"] = "X"; }
                                dtSections.Rows.Add(dtrow);
                            }
                            Repeater2.DataSource = dtSections;
                            Repeater2.DataBind();

                            int totalQuestions = questions.Rows.Count;
                            int questionInSection = totalQuestions / Convert.ToInt16(sections);

                            DataTable dtLoadQuestionNos = new DataTable();
                            dtLoadQuestionNos.Columns.Add("serialnumber", typeof(string));
                            dtLoadQuestionNos.Columns.Add("questid", typeof(string));

                            for (int x = 0; x < questionInSection; x++)
                            {
                                DataRow dtrow = dtLoadQuestionNos.NewRow();
                                dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                                dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                                dtLoadQuestionNos.Rows.Add(dtrow);

                                string s1 = questions.Rows[x]["serialnumber"].ToString();
                                string s2 = questions.Rows[x]["questid"].ToString();
                            }

                            Repeater1.DataSource = dtLoadQuestionNos;
                            Repeater1.DataBind();


                            selectSection("1");
                            lblSectionNo.Text = "1";
                            lblSectionsLabel.Visible = true;

                        }
                        else { lblSectionsLabel.Visible = false; }

                    }
                }
                catch (Exception ee)
                {
                    if (ee.Message.Contains("Object reference not set to an instance of an object."))
                    {
                        lblYourName.Text = "Session Expired !";
                        Panel10.Visible = false;
                        Panel7.Visible = false;
                    }
                }

            }
            try
            {

                if (!IsPostBack)
                {
                    //ddlCourse.DataBind();
                    // Session.Remove("ques");
                    //rb1.Attributes.Add("onmousedown", "clickCheck(this)");
                    //string[] st = HttpContext.Current.User.Identity.Name.Split(',');
                    //lbluserID.Text = st[0].ToString();
                    generateAnswerSheet();
                    selectQue();
                }

            }
            catch { }

        }

        //to get proper action after any popup is closed...
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["rTime"].ToString().Length > 0)
                {
                    Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime"].ToString()));
                    TimeSpan time1 = new TimeSpan();
                    time1 = ((DateTime)Session["time"]) - DateTime.Now;
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    Timer1.Enabled = true;
                    Session.Remove("rTime");
                }
            }
            catch { }

            try
            {

                if (Session["JumpTo"].ToString() == "Yes")
                {

                    //Button btn = (Button)sender;
                    string qID = Session["qID"].ToString();
                    int z = Convert.ToInt16(Session["qNo"].ToString());


                    Session.Remove("JumpTo"); Session.Remove("qID"); Session.Remove("qNo");


                    ViewState["in"] = z;

                    lblSelectedQuesNo.Text = z.ToString();

                    lblQuestionNo.Text = qID;
                    loadQuestion();

                    //DataTable dt = new DataTable();
                    //dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,option5,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 when answer = 5 then option5 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + qID + "");
                    //lblQue.Text = dt.Rows[0][1].ToString();
                    //lblOptionA.Text = dt.Rows[0][2].ToString();
                    //lblOptionB.Text = dt.Rows[0][3].ToString();
                    //lblOptionC.Text = dt.Rows[0][4].ToString();
                    //lblOptionD.Text = dt.Rows[0][5].ToString();
                    //lblOptionE.Text = dt.Rows[0][6].ToString();

                    //setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

                    //clearAllRadio();
                    //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
                    //if (givenAnswer != "0")
                    //{
                    //    if (givenAnswer == "1")
                    //    { rb1.Checked = true; }
                    //    if (givenAnswer == "2")
                    //    { rb2.Checked = true; }
                    //    if (givenAnswer == "3")
                    //    { rb3.Checked = true; }
                    //    if (givenAnswer == "4")
                    //    { rb4.Checked = true; }
                    //    if (givenAnswer == "5")
                    //    { rb5.Checked = true; }
                    //    System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "document.getElementById('" + linkCleraOptions.ClientID + "').innerHTML='Clear';", true);
                    //}

                    //try
                    //{
                    //    if (dt.Rows[0]["infoID"].ToString() != "0")
                    //    {
                    //        Panel5.Visible = true; hrli.Visible = true;//to show common data heading...
                    //        DataTable dtqueData = new DataTable();
                    //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
                    //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
                    //        {
                    //            repQueInfo.DataSource = dtqueData;
                    //            repQueInfo.DataBind();
                    //        }
                    //        else
                    //        {
                    //            Panel5.Visible = false; hrli.Visible = false; ;//to hide common data heading...
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Panel5.Visible = false; hrli.Visible = false;//to hide common data heading...
                    //    }
                    //}
                    //catch { }

                    //DataTable markedQuestion = new DataTable();
                    //markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
                    //if (markedQuestion.Rows.Count > 0)
                    //{
                    //    btnMark.Text = "Unmark";
                    //    iBtnUnMark.Visible = true;
                    //    iBtnMark.Visible = false;
                    //}
                    //else
                    //{
                    //    btnMark.Text = "Mark";
                    //    iBtnMark.Visible = true;
                    //    iBtnUnMark.Visible = false;
                    //}


                    if (lblSections.Text.Length > 0)
                    {
                        DataTable dt1 = (DataTable)ViewState["myTable"];
                        int TotalSections = Repeater2.Items.Count;
                        int totalQuestions = dt1.Rows.Count;
                        int questionInSection = totalQuestions / Convert.ToInt16(TotalSections);
                        int queNos = 0;
                        string secFound = "No";
                        for (int i = 1; i < 10; i++)
                        {
                            if (secFound == "No")
                            {
                                queNos = questionInSection * i;
                                if (z <= queNos)
                                {
                                    secFound = i.ToString();
                                }
                            }
                        }

                        generateQuestions(secFound);
                    }
                    //selectQue();
                    //setNextPrevious();

                }
            }
            catch { }


            try
            {
                if (Session["restartExam"].ToString() == "Yes")
                {
                    if (Session["rTime1"].ToString().Length > 0)
                    {
                        Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime1"].ToString()));
                        TimeSpan time1 = new TimeSpan();
                        time1 = ((DateTime)Session["time"]) - DateTime.Now;
                        Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                        Timer1.Enabled = true;
                        Session.Remove("rTime1");
                    }

                    Panel7.Visible = true;
                    Panel6.Visible = true;
                    //Label2.Visible = true;
                    Label3.Visible = false; ;
                    //Label4.Visible = true;
                    Session.Remove("restartExam");



                }
            }
            catch { }


            try
            {
                if (Session["CloseDir"].ToString() == "Yes")
                {

                    Session.Remove("CloseDir");
                    //if (Session["rTime2"].ToString().Length > 0)
                    //{
                    //    Session["time"] = DateTime.Now.AddMilliseconds(Convert.ToInt32(Session["rTime2"].ToString()));
                    //    TimeSpan time1 = new TimeSpan();
                    //    time1 = ((DateTime)Session["time"]) - DateTime.Now;
                    //    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
                    //    Timer1.Enabled = true;
                    //    Session.Remove("rTime2");
                    //}
                    Panel7.Visible = true;
                    Timer1.Enabled = true;
                    Panel6.Visible = true;
                    //Label2.Visible = true;
                    Label3.Visible = false;
                    //Label4.Visible = true;
                    //Session.Remove("restartExam");

                }
            }
            catch { }


        }

        protected void btnSub_Click(object sender, EventArgs e)
        {
            //lblPreviousTime.Text = DateTime.Now.ToString();
            //string maxExamID = obj.getValue("select max(newexm)+1 from exm_newexam");
            //lblExamID.Text = maxExamID;
            //Panel3.Visible = true;
            //Panel2.Visible = false;
            //DataTable dt = new DataTable();
            //dt = obj.getData("SELECT top " + ddlno.Text + " exm_questions.questid, exm_questions.question, exm_questions.option1, exm_questions.option2, exm_questions.option3, exm_questions.option4,  exm_questions.answer, exm_questions.subid, exm_questions.difflevel, exm_questions.userid, ROW_NUMBER() OVER (ORDER BY questid) AS [serialnumber], case when exm_questions.answer = '1' then exm_questions.option1 when exm_questions.answer = '2' then  exm_questions.option2 when exm_questions.answer = '3' then exm_questions.option3 when exm_questions.answer = '2' then exm_questions.option4 end as CorectAns,exm_questions.infoID  FROM exm_questions INNER JOIN exm_subject ON exm_questions.subid = exm_subject.subid INNER JOIN exm_course ON exm_subject.courseid = exm_course.courseid WHERE (exm_course.courseid = " + ddlCourse.SelectedValue + ") order by questid, newid() ");
            //Timer1.Enabled = true;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            //Session["time"] = DateTime.Now.AddMilliseconds(3600000);
            //Label3.Text = "Started at &nbsp;" + DateTime.Now.ToString("h:mm tt");
            //Label3.Visible = true;
            ////Response.Write((DateTime)Session["time"]);
            //TimeSpan time1 = new TimeSpan();
            //time1 = ((DateTime)Session["time"]) - DateTime.Now;
            //Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours.ToString(), time1.Minutes.ToString(), time1.Seconds.ToString());
            //Timer1.Enabled = true;


            //int rTimeinMill = 0;
            //string[] rTime = Label2.Text.Split(':');

            //if (rTime[0].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[0].ToString()) > 0)
            //    {
            //        rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
            //    }
            //}

            //if (rTime[1].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[1].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
            //    }
            //}

            //if (rTime[2].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[2].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
            //    }
            //}





            //string queNos = "";
            //for (int n = 0; n < dt.Rows.Count; n++)
            //{
            //    if (n == 0)
            //    {
            //        queNos = dt.Rows[0][0].ToString();
            //        obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, dt.Rows[0][0].ToString(), "0", rTimeinMill);
            //    }
            //    else
            //    {
            //        queNos = queNos + "," + dt.Rows[n][0].ToString();
            //        obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, dt.Rows[n][0].ToString(), "0", rTimeinMill);
            //    }
            //}
            //obj.InsertUserQuestionList(maxExamID, ddlno.Text, queNos, lbluserID.Text, rTimeinMill, ddlCourse.SelectedValue);

            //ViewState["myTable"] = dt;
            //Session.Add("localQuest", Convert.ToInt16(Session["questno"]) + 1);
            //ViewState["in"] = i;
            //Session["ques"] = ddlno.Text;
            //lblQuestionNo.Text = dt.Rows[0][0].ToString();
            //lblQue.Text = dt.Rows[0][1].ToString();
            //lblOptionA.Text = dt.Rows[0][2].ToString();
            //lblOptionB.Text = dt.Rows[0][3].ToString();
            //lblOptionC.Text = dt.Rows[0][4].ToString();
            //lblOptionD.Text = dt.Rows[0][5].ToString();
            //Repeater1.DataSource = dt;
            //Repeater1.DataBind();

            //lblSelectedQuesNo.Text = "1";
            //selectQue();

            ////if (dt.Rows[0]["infoID"].ToString() != "0")
            ////{
            ////    Panel5.Visible = true;
            ////    Panel5.Width = new Unit("50%");
            ////    DataTable dtqueData = new DataTable();
            ////    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
            ////    repQueInfo.DataSource = dtqueData;
            ////    repQueInfo.DataBind();
            ////}
            ////else
            ////{
            ////    Panel5.Visible = false;
            ////    Panel5.Width = new Unit("0%");
            ////}

            ////Timer1.Enabled = false;
            //try
            //{
            //    if (dt.Rows[0]["infoID"].ToString() != "0")
            //    {
            //        Panel5.Visible = true;
            //        DataTable dtqueData = new DataTable();
            //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
            //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //        {
            //            repQueInfo.DataSource = dtqueData;
            //            repQueInfo.DataBind();
            //        }
            //        else { Panel5.Visible = false; }
            //    }
            //    else
            //    {
            //        Panel5.Visible = false;
            //    }
            //}
            //catch { }

        }

        //for sunbmit and Next button (button is visible false)
        //protected void Button2_Click(object sender, EventArgs e)
        //{
        //    //try
        //    //{

        //    if (rb1.Checked == true || rb2.Checked == true || rb3.Checked == true || rb4.Checked == true || rb5.Checked == true)
        //    {
        //        setTimeSpent();
        //        string ans = "0";
        //        if (rb1.Checked == true)
        //        {
        //            ans = "1";
        //        }
        //        if (rb2.Checked == true)
        //        {
        //            ans = "2";
        //        }
        //        if (rb3.Checked == true)
        //        {
        //            ans = "3";
        //        }
        //        if (rb4.Checked == true)
        //        {
        //            ans = "4";
        //        }
        //        if (rb5.Checked == true)
        //        {
        //            ans = "5";
        //        }

        //        // = (rblOptions.SelectedIndex + 1).ToString();
        //        if (lblGivenAns.Text.Length > 0)
        //        {
        //            lblGivenAns.Text = lblGivenAns.Text + "," + ans;
        //        }
        //        else { lblGivenAns.Text = ans; }
        //        DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //        int rTimeinMill = 0;
        //        string[] rTime = Label2.Text.Split(':');

        //        if (rTime[0].Length > 0)
        //        {
        //            if (Convert.ToInt32(rTime[0].ToString()) > 0)
        //            {
        //                rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
        //            }
        //        }

        //        if (rTime[1].Length > 0)
        //        {
        //            if (Convert.ToInt32(rTime[1].ToString()) > 0)
        //            {
        //                rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
        //            }
        //        }

        //        if (rTime[2].Length > 0)
        //        {
        //            if (Convert.ToInt32(rTime[2].ToString()) > 0)
        //            {
        //                rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
        //            }
        //        }


        //        if (dt2.Rows.Count > 0)
        //        {
        //            obj.updateUserAnswer(dt2.Rows[0]["id"].ToString(), ans, rTimeinMill);
        //        }
        //        else
        //        {
        //            obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text, ans, rTimeinMill);
        //        }

        //        //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");

        //        //DataTable questionList = obj.getData("select questionlist from exm_newexam where newexm = " + lblExamID.Text + " ");




        //        // Access dt back from viewstate
        //        DataTable dt = (DataTable)ViewState["myTable"];
        //        int j = (int)ViewState["in"];
        //        if (dt.Rows.Count > j)
        //        {
        //            lblQuestionNo.Text = dt.Rows[j][0].ToString();
        //            lblQue.Text = dt.Rows[j][1].ToString();
        //            lblOptionA.Text = dt.Rows[j][2].ToString();
        //            lblOptionB.Text = dt.Rows[j][3].ToString();
        //            lblOptionC.Text = dt.Rows[j][4].ToString();
        //            lblOptionD.Text = dt.Rows[j][5].ToString();
        //            lblOptionE.Text = dt.Rows[j][6].ToString();

        //            setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

        //            //if (dt.Rows[j]["infoID"].ToString() != "0")
        //            //{
        //            //    Panel5.Visible = true;
        //            //    Panel5.Width = new Unit("50%");
        //            //    DataTable dtqueData = new DataTable();
        //            //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //            //    repQueInfo.DataSource = dtqueData;
        //            //    repQueInfo.DataBind();
        //            //}
        //            //else
        //            //{
        //            //    Panel5.Visible = false;
        //            //    Panel5.Width = new Unit("0%");
        //            //}

        //            try
        //            {
        //                if (dt.Rows[0]["infoID"].ToString() != "0")
        //                {
        //                    Panel5.Visible = true; hrli.Visible = true;//to show common data heading... 
        //                    DataTable dtqueData = new DataTable();
        //                    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
        //                    if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //                    {
        //                        repQueInfo.DataSource = dtqueData;
        //                        repQueInfo.DataBind();
        //                    }
        //                    else
        //                    {
        //                        Panel5.Visible = false; hrli.Visible = false;//to hide common data heading... 
        //                    }
        //                }
        //                else
        //                {
        //                    Panel5.Visible = false; hrli.Visible = false;//to hide common data heading... 
        //                }
        //            }
        //            catch { }

        //            j++;
        //            ViewState["in"] = j;
        //        }
        //        else
        //        {
        //            //Session["dtable"] = dt;
        //            //Session["ans"] = lblGivenAns.Text;
        //            ////Response.Redirect("ExamResult.aspx");
        //            j = 0;

        //            lblQuestionNo.Text = dt.Rows[j][0].ToString();
        //            lblQue.Text = dt.Rows[j][1].ToString();
        //            lblOptionA.Text = dt.Rows[j][2].ToString();
        //            lblOptionB.Text = dt.Rows[j][3].ToString();
        //            lblOptionC.Text = dt.Rows[j][4].ToString();
        //            lblOptionD.Text = dt.Rows[j][5].ToString();
        //            lblOptionE.Text = dt.Rows[j][6].ToString();

        //            setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);
        //            //if (dt.Rows[j]["infoID"].ToString() != "0")
        //            //{
        //            //    Panel5.Visible = true;
        //            //    DataTable dtqueData = new DataTable();
        //            //    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //            //    repQueInfo.DataSource = dtqueData;
        //            //    repQueInfo.DataBind();
        //            //}
        //            //else
        //            //{
        //            //    Panel5.Visible = false;
        //            //}

        //            try
        //            {
        //                if (dt.Rows[0]["infoID"].ToString() != "0")
        //                {
        //                    Panel5.Visible = true; hrli.Visible = true;//to show common data heading... 
        //                    DataTable dtqueData = new DataTable();
        //                    dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
        //                    if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //                    {
        //                        repQueInfo.DataSource = dtqueData;
        //                        repQueInfo.DataBind();
        //                    }
        //                    else
        //                    {
        //                        Panel5.Visible = false; hrli.Visible = false;//to show common data heading... 
        //                    }
        //                }
        //                else
        //                {
        //                    Panel5.Visible = false;
        //                    hrli.Visible = false;//to show common data heading... 
        //                }
        //            }
        //            catch { }
        //            j++;
        //            ViewState["in"] = j;

        //            string script = "<script type=\"text/javascript\">showMessage();</script>";
        //            //Page page = HttpContext.Current.CurrentHandler as Page;
        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        //        }

        //        //   j = (int)ViewState["in"];




        //        clearAllRadio();

        //        Repeater1.DataSource = dt;
        //        Repeater1.DataBind();
        //        int z = (int)ViewState["in"];
        //        lblSelectedQuesNo.Text = z.ToString();
        //        selectQue();

        //        string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //        if (givenAnswer != "0")
        //        {
        //            if (givenAnswer == "1")
        //            { rb1.Checked = true; }
        //            if (givenAnswer == "2")
        //            { rb2.Checked = true; }
        //            if (givenAnswer == "3")
        //            { rb3.Checked = true; }
        //            if (givenAnswer == "4")
        //            { rb4.Checked = true; }
        //            if (givenAnswer == "5")
        //            { rb5.Checked = true; }
        //        }

        //        DataTable markedQuestion = new DataTable();
        //        markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
        //        if (markedQuestion.Rows.Count > 0)
        //        {
        //            btnMark.Text = "Unmark";
        //            iBtnUnMark.Visible = true;
        //            iBtnMark.Visible = false;
        //        }
        //        else
        //        {
        //            btnMark.Text = "Mark";
        //            iBtnMark.Visible = true;
        //            iBtnUnMark.Visible = false;
        //        }
        //        //}
        //        //catch { }
        //    }
        //    else
        //    {
        //        string script = "<script type=\"text/javascript\">showErrorMsg();</script>";
        //        //Page page = HttpContext.Current.CurrentHandler as Page;
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        //    }
        //}

        protected void btnRep_Click(object sender, EventArgs e)
        {
            //setTimeSpent();
            ////setremainingTime();
            //saveUserAnswer();

            setTimeSpentandUserAnswer();
            // System.Threading.Thread.Sleep(2000);
            Button btn = (Button)sender;
            string qID = btn.CommandArgument;
            int z = Convert.ToInt16(btn.Text);
            lblSelectedQuesNo.Text = z.ToString();

            ViewState["in"] = z;

            lblQuestionNo.Text = qID;
            loadQuestion();
            //DataTable dt = new DataTable();
            //dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,option5,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 when answer = 5 then option5 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + qID + "");
            //lblQue.Text = dt.Rows[0][1].ToString();
            //lblOptionA.Text = dt.Rows[0][2].ToString();
            //lblOptionB.Text = dt.Rows[0][3].ToString();
            //lblOptionC.Text = dt.Rows[0][4].ToString();
            //lblOptionD.Text = dt.Rows[0][5].ToString();
            //lblOptionE.Text = dt.Rows[0][6].ToString();

            //setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

            //clearAllRadio();
            //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //if (givenAnswer != "0")
            //{
            //    if (givenAnswer == "1")
            //    { rb1.Checked = true; }
            //    if (givenAnswer == "2")
            //    { rb2.Checked = true; }
            //    if (givenAnswer == "3")
            //    { rb3.Checked = true; }
            //    if (givenAnswer == "4")
            //    { rb4.Checked = true; }
            //    if (givenAnswer == "5")
            //    { rb5.Checked = true; }
            //}

            //try
            //{
            //    if (dt.Rows[0]["infoID"].ToString() != "0")
            //    {
            //        Panel5.Visible = true; hrli.Visible = true;//to show common data heading... 
            //        DataTable dtqueData = new DataTable();
            //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
            //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //        {
            //            repQueInfo.DataSource = dtqueData;
            //            repQueInfo.DataBind();
            //        }
            //        else
            //        {
            //            Panel5.Visible = false; hrli.Visible = false;//to show common data heading... 
            //        }
            //    }
            //    else
            //    {
            //        Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
            //    }
            //}
            //catch { }



            //setNextPrevious();
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void clearAllRadio()
        {
            rb1.Checked = false;
            rb2.Checked = false;
            rb3.Checked = false;
            rb4.Checked = false;
            rb5.Checked = false;
        }

        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    //setTimeSpent();
        //    //setremainingTime();


        //    //string ans = "0";
        //    //if (rb1.Checked == true)
        //    //{
        //    //    ans = "1";
        //    //}
        //    //if (rb2.Checked == true)
        //    //{
        //    //    ans = "2";
        //    //}
        //    //if (rb3.Checked == true)
        //    //{
        //    //    ans = "3";
        //    //}
        //    //if (rb4.Checked == true)
        //    //{
        //    //    ans = "4";
        //    //}
        //    //// = (rblOptions.SelectedIndex + 1).ToString();
        //    //if (lblGivenAns.Text.Length > 0)
        //    //{
        //    //    lblGivenAns.Text = lblGivenAns.Text + "," + ans;
        //    //}
        //    //else { lblGivenAns.Text = ans; }
        //    //DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //    //int rTimeinMill = 0;
        //    //string[] rTime = Label2.Text.Split(':');

        //    //if (rTime[0].Length > 0)
        //    //{
        //    //    if (Convert.ToInt32(rTime[0].ToString()) > 0)
        //    //    {
        //    //        rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
        //    //    }
        //    //}

        //    //if (rTime[1].Length > 0)
        //    //{
        //    //    if (Convert.ToInt32(rTime[1].ToString()) > 0)
        //    //    {
        //    //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
        //    //    }
        //    //}

        //    //if (rTime[2].Length > 0)
        //    //{
        //    //    if (Convert.ToInt32(rTime[2].ToString()) > 0)
        //    //    {
        //    //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
        //    //    }
        //    //}


        //    //if (dt2.Rows.Count > 0)
        //    //{
        //    //    obj.updateUserAnswer(dt2.Rows[0]["id"].ToString(), ans, rTimeinMill);
        //    //}
        //    //else
        //    //{
        //    //    obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text, ans, rTimeinMill);
        //    //}







        //    //DataTable dt = (DataTable)ViewState["myTable"];
        //    //int j = (int)ViewState["in"];

        //    //if (dt.Rows.Count > j)
        //    //{

        //    //    lblSelectedQuesNo.Text = j.ToString();

        //    //    ViewState["in"] = j;

        //    //    lblQuestionNo.Text = dt.Rows[j][0].ToString();
        //    //    DataTable dt1 = new DataTable();
        //    //    dt1 = obj.getData("SELECT questid,question, option1, option2, option3, option4,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + lblQuestionNo.Text + "");
        //    //    lblQue.Text = dt1.Rows[0][1].ToString();
        //    //    lblOptionA.Text = dt1.Rows[0][2].ToString();
        //    //    lblOptionB.Text = dt1.Rows[0][3].ToString();
        //    //    lblOptionC.Text = dt1.Rows[0][4].ToString();
        //    //    lblOptionD.Text = dt1.Rows[0][5].ToString();

        //    //    clearAllRadio();
        //    //    string givenAnswer1 = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //    //    if (givenAnswer1 != "0")
        //    //    {
        //    //        if (givenAnswer1 == "1")
        //    //        { rb1.Checked = true; }
        //    //        if (givenAnswer1 == "2")
        //    //        { rb2.Checked = true; }
        //    //        if (givenAnswer1 == "3")
        //    //        { rb3.Checked = true; }
        //    //        if (givenAnswer1 == "4")
        //    //        { rb4.Checked = true; }
        //    //    }

        //    //    try
        //    //    {
        //    //        if (dt.Rows[j]["infoID"].ToString() != "0")
        //    //        {
        //    //            Panel5.Visible = true;
        //    //            DataTable dtqueData = new DataTable();
        //    //            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //    //            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //    //            {
        //    //                repQueInfo.DataSource = dtqueData;
        //    //                repQueInfo.DataBind();
        //    //            }
        //    //            else { Panel5.Visible = false; }
        //    //        }
        //    //        else
        //    //        {
        //    //            Panel5.Visible = false;
        //    //        }
        //    //    }
        //    //    catch { }

        //    //}


























        //    ////if (dt.Rows.Count > j)
        //    ////{

        //    ////    lblQuestionNo.Text = j.ToString();
        //    ////    DataTable dt1 = new DataTable();
        //    ////    dt1 = obj.getData("SELECT questid,question, option1, option2, option3, option4,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + j.ToString() + "");
        //    ////    lblQue.Text = dt1.Rows[0][1].ToString();
        //    ////    lblOptionA.Text = dt1.Rows[0][2].ToString();
        //    ////    lblOptionB.Text = dt1.Rows[0][3].ToString();
        //    ////    lblOptionC.Text = dt1.Rows[0][4].ToString();
        //    ////    lblOptionD.Text = dt1.Rows[0][5].ToString();
        //    ////    try
        //    ////    {
        //    ////        if (dt.Rows[j]["infoID"].ToString() != "0")
        //    ////        {
        //    ////            Panel5.Visible = true;
        //    ////            DataTable dtqueData = new DataTable();
        //    ////            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //    ////            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //    ////            {
        //    ////                repQueInfo.DataSource = dtqueData;
        //    ////                repQueInfo.DataBind();
        //    ////            }
        //    ////            else { Panel5.Visible = false; }
        //    ////        }
        //    ////        else
        //    ////        {
        //    ////            Panel5.Visible = false;
        //    ////        }
        //    ////    }
        //    ////    catch { }


        //    ////    if (Repeater2.Items.Count > 0)
        //    ////    {

        //    ////        string lastqueNo = "";
        //    ////        int repItems = Repeater1.Items.Count;
        //    ////        for (int x = 0; x < repItems; x++)
        //    ////        {
        //    ////            Button btn = (Button)Repeater1.Items[x].FindControl("Button1");
        //    ////            string qID = btn.Text;
        //    ////            if (x == repItems - 1)
        //    ////            {
        //    ////                if (Convert.ToInt16(qID) < j + 1)
        //    ////                {
        //    ////                    int sectionNo = Convert.ToInt16(lblSectionNo.Text) + 1;
        //    ////                    lblSectionNo.Text = sectionNo.ToString();
        //    ////                    setSectionAndQue(sectionNo.ToString());
        //    ////                    selectSection("Section " + sectionNo.ToString());
        //    ////                }
        //    ////            }

        //    ////        }
        //    ////    }

        //    ////    j++;
        //    ////    ViewState["in"] = j;


        //    ////}
        //    //else
        //    //{
        //    //    Session["dtable"] = dt;
        //    //    Session["ans"] = lblGivenAns.Text;
        //    //    //Response.Redirect("ExamResult.aspx");

        //    //    j = 0;

        //    //    lblQuestionNo.Text = dt.Rows[j][0].ToString();
        //    //    lblQue.Text = dt.Rows[j][1].ToString();
        //    //    lblOptionA.Text = dt.Rows[j][2].ToString();
        //    //    lblOptionB.Text = dt.Rows[j][3].ToString();
        //    //    lblOptionC.Text = dt.Rows[j][4].ToString();
        //    //    lblOptionD.Text = dt.Rows[j][5].ToString();

        //    //    try
        //    //    {
        //    //        if (dt.Rows[j]["infoID"].ToString() != "0")
        //    //        {
        //    //            Panel5.Visible = true;
        //    //            DataTable dtqueData = new DataTable();
        //    //            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //    //            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //    //            {
        //    //                repQueInfo.DataSource = dtqueData;
        //    //                repQueInfo.DataBind();
        //    //            }
        //    //            else { Panel5.Visible = false; }
        //    //        }
        //    //        else
        //    //        {
        //    //            Panel5.Visible = false;
        //    //        }
        //    //    }
        //    //    catch { }


        //    //    if (Repeater2.Items.Count > 0)
        //    //    {
        //    //        lblSectionNo.Text = "1";
        //    //        setSectionAndQue(lblSectionNo.Text);
        //    //        selectSection("Section " + lblSectionNo.Text);
        //    //    }
        //    //    j++;
        //    //    ViewState["in"] = j;

        //    //    string script = "<script type=\"text/javascript\">showMessage();</script>";
        //    //    //Page page = HttpContext.Current.CurrentHandler as Page;
        //    //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        //    //}

        //    //clearAllRadio();
        //    //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //    //if (givenAnswer != "0")
        //    //{
        //    //    if (givenAnswer == "1")
        //    //    { rb1.Checked = true; }
        //    //    if (givenAnswer == "2")
        //    //    { rb2.Checked = true; }
        //    //    if (givenAnswer == "3")
        //    //    { rb3.Checked = true; }
        //    //    if (givenAnswer == "4")
        //    //    { rb4.Checked = true; }
        //    //}

        //    //DataTable markedQuestion = new DataTable();
        //    //markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
        //    //if (markedQuestion.Rows.Count > 0)
        //    //{
        //    //    btnMark.Text = "Unmark";
        //    //}
        //    //else { btnMark.Text = "Mark"; }
        //    //lblSelectedQuesNo.Text = j.ToString();
        //    //selectQue();
        //    //generateAnswerSheet();
        //}








        // Next button click action
        protected void btnNext_Click(object sender, EventArgs e)
        {

            // setTimeSpent();
            //setremainingTime();

            //saveUserAnswer();
            setTimeSpentandUserAnswer();
            // System.Threading.Thread.Sleep(2000);
            //string ans = "0";
            //if (rb1.Checked == true)
            //{
            //    ans = "1";
            //}
            //if (rb2.Checked == true)
            //{
            //    ans = "2";
            //}
            //if (rb3.Checked == true)
            //{
            //    ans = "3";
            //}
            //if (rb4.Checked == true)
            //{
            //    ans = "4";
            //}
            //if (rb5.Checked == true)
            //{
            //    ans = "5";
            //}
            //// = (rblOptions.SelectedIndex + 1).ToString();
            //if (lblGivenAns.Text.Length > 0)
            //{
            //    lblGivenAns.Text = lblGivenAns.Text + "," + ans;
            //}
            //else { lblGivenAns.Text = ans; }
            //DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //int rTimeinMill = 0;
            //string[] rTime = Label2.Text.Split(':');

            //if (rTime[0].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[0].ToString()) > 0)
            //    {
            //        rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
            //    }
            //}

            //if (rTime[1].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[1].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
            //    }
            //}

            //if (rTime[2].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[2].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
            //    }
            //}


            //if (dt2.Rows.Count > 0)
            //{
            //    obj.updateUserAnswer(dt2.Rows[0]["id"].ToString(), ans, rTimeinMill);
            //}
            //else
            //{
            //    obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text, ans, rTimeinMill);
            //}



            DataTable dt = (DataTable)ViewState["myTable"];
            int j = (int)ViewState["in"];
            lblQuestionNo.Text = dt.Rows[j][0].ToString();
            loadQuestion();

            //lblQue.Text = dt.Rows[j][1].ToString();
            //lblOptionA.Text = dt.Rows[j][2].ToString();
            //lblOptionB.Text = dt.Rows[j][3].ToString();
            //lblOptionC.Text = dt.Rows[j][4].ToString();
            //lblOptionD.Text = dt.Rows[j][5].ToString();
            //lblOptionE.Text = dt.Rows[j][6].ToString();

            //setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

            //clearAllRadio();
            //string givenAnswer1 = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //if (givenAnswer1 != "0")
            //{
            //    if (givenAnswer1 == "1")
            //    { rb1.Checked = true; }
            //    if (givenAnswer1 == "2")
            //    { rb2.Checked = true; }
            //    if (givenAnswer1 == "3")
            //    { rb3.Checked = true; }
            //    if (givenAnswer1 == "4")
            //    { rb4.Checked = true; }
            //    if (givenAnswer1 == "5")
            //    { rb5.Checked = true; }
            //}

            //try
            //{
            //    if (dt.Rows[j]["infoID"].ToString() != "0")
            //    {
            //        Panel5.Visible = true;
            //        hrli.Visible = true;//to show/hide common data heading... 
            //        DataTable dtqueData = new DataTable();
            //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
            //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //        {
            //            repQueInfo.DataSource = dtqueData;
            //            repQueInfo.DataBind();
            //        }
            //        else
            //        {
            //            Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading...  
            //        }
            //    }
            //    else
            //    {
            //        Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
            //    }
            //}
            //catch { }





            //DataTable markedQuestion = new DataTable();
            //markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            //if (markedQuestion.Rows.Count > 0)
            //{
            //    btnMark.Text = "Unmark";
            //    iBtnUnMark.Visible = true;
            //    iBtnMark.Visible = false;
            //}
            //else
            //{
            //    btnMark.Text = "Mark";
            //    iBtnMark.Visible = true;
            //    iBtnUnMark.Visible = false;
            //}



            //if (Repeater2.Items.Count > 0)
            //{

            //    //string lastqueNo = "";
            //    int repItems = Repeater1.Items.Count;
            //    for (int x = 0; x < repItems; x++)
            //    {
            //        Button btn = (Button)Repeater1.Items[x].FindControl("Button1");
            //        string qID = btn.Text;
            //        if (x == repItems - 1)
            //        {
            //            if (Convert.ToInt16(qID) <= j)
            //            {
            //                int sectionNo = Convert.ToInt16(lblSectionNo.Text) + 1;
            //                lblSectionNo.Text = sectionNo.ToString();
            //                setSectionAndQue(sectionNo.ToString());
            //                selectSection("Section " + sectionNo.ToString());
            //            }
            //        }

            //    }

            //}
            if (lblSections.Text.Length > 0)
            {
                int TotalSections = Repeater2.Items.Count;
                int totalQuestions = dt.Rows.Count;
                int questionInSection = totalQuestions / Convert.ToInt16(TotalSections);
                int queNos = 0;
                string secFound = "No";
                for (int i = 1; i < 10; i++)
                {
                    if (secFound == "No")
                    {
                        queNos = questionInSection * i;
                        if (j + 1 <= queNos)
                        {
                            secFound = i.ToString();
                        }
                    }
                }

                generateQuestions(secFound);
            }
            j++;
            ViewState["in"] = j;
            lblSelectedQuesNo.Text = j.ToString();
            selectQue();

            setNextPrevious();
        }
        //else
        //{
        //    Session["dtable"] = dt;
        //    Session["ans"] = lblGivenAns.Text;
        //    //Response.Redirect("ExamResult.aspx");

        //    j = 0;

        //    lblQuestionNo.Text = dt.Rows[j][0].ToString();
        //    lblQue.Text = dt.Rows[j][1].ToString();
        //    lblOptionA.Text = dt.Rows[j][2].ToString();
        //    lblOptionB.Text = dt.Rows[j][3].ToString();
        //    lblOptionC.Text = dt.Rows[j][4].ToString();
        //    lblOptionD.Text = dt.Rows[j][5].ToString();
        //    lblOptionE.Text = dt.Rows[j][6].ToString();

        //    setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

        //    clearAllRadio();
        //    string givenAnswer1 = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
        //    if (givenAnswer1 != "0")
        //    {
        //        if (givenAnswer1 == "1")
        //        { rb1.Checked = true; }
        //        if (givenAnswer1 == "2")
        //        { rb2.Checked = true; }
        //        if (givenAnswer1 == "3")
        //        { rb3.Checked = true; }
        //        if (givenAnswer1 == "4")
        //        { rb4.Checked = true; }
        //        if (givenAnswer1 == "5")
        //        { rb5.Checked = true; }
        //    }
        //    if (Repeater2.Items.Count > 0)
        //    {
        //        setSectionAndQue("1");
        //        selectSection("1");
        //    }
        //    try
        //    {
        //        if (dt.Rows[j]["infoID"].ToString() != "0")
        //        {
        //            Panel5.Visible = true;
        //            hrli.Visible = true;//to show/hide common data heading... 
        //            DataTable dtqueData = new DataTable();
        //            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
        //            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
        //            {
        //                repQueInfo.DataSource = dtqueData;
        //                repQueInfo.DataBind();
        //            }
        //            else
        //            {
        //                Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
        //            }
        //        }
        //        else
        //        {
        //            Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
        //        }
        //    }
        //    catch { }


        //    DataTable markedQuestion = new DataTable();
        //    markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
        //    if (markedQuestion.Rows.Count > 0)
        //    {
        //        btnMark.Text = "Unmark";
        //        iBtnUnMark.Visible = true;
        //        iBtnMark.Visible = false;
        //    }
        //    else
        //    {
        //        btnMark.Text = "Mark";
        //        iBtnMark.Visible = true;
        //        iBtnUnMark.Visible = false;
        //    }





        //    j++;
        //    ViewState["in"] = j;
        //    lblSelectedQuesNo.Text = j.ToString();

        //    string script = "<script type=\"text/javascript\">showMessage();</script>";
        //    //Page page = HttpContext.Current.CurrentHandler as Page;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

        //}
        //selectQue();
        //generateAnswerSheet();
        //setNextPrevious();











        //protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (Repeater1.Items.Count > 0)
        //    {
        //        DataTable dt = new DataTable();
        //        dt = obj.getData("SELECT  questno, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ")");
        //        if (dt.Rows.Count > 0)
        //        {
        //            //Label lbl = (Label)Repeater1.FindControl("");
        //            Button btn = (Button)Repeater1.FindControl("Button1");
        //            string qID = btn.Text;
        //            //for (int i = 0; i < dt.Rows.Count; i++)
        //            //{
        //            //    if (Convert.ToInt16(qID) == Convert.ToInt16(dt.Rows[0]["questno"].ToString()))
        //            //    {
        //            //        btn.BackColor = System.Drawing.Color.Green;
        //            //    }
        //            //}
        //        }
        //    }
        //}

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            //if (rb1.Checked == true || rb2.Checked == true || rb3.Checked == true || rb4.Checked == true || rb5.Checked == true)
            //{
            // saveUserAnswer();
            //}
            setTimeSpentandUserAnswer();
            DataTable dt = (DataTable)ViewState["myTable"];
            Session["dtable"] = dt;
            Session["ans"] = lblGivenAns.Text;


            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;

            obj.updateExamStatus(lblExamID.Text);
            Response.Redirect("exmResult.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                TimeSpan time1 = new TimeSpan();
                time1 = ((DateTime)Session["time"]) - DateTime.Now;
                if (time1.Minutes <= 0 && time1.Seconds <= 0)
                {
                    Label2.Text = "TimeOut!";
                    DataTable dt = (DataTable)ViewState["myTable"];
                    Session["dtable"] = dt;
                    Session["ans"] = lblGivenAns.Text;


                    Session["examID"] = lblExamID.Text;
                    Session["userID"] = lbluserID.Text;

                    obj.updateExamStatus(lblExamID.Text);
                    Response.Redirect("exmResult.aspx");
                }
                else
                {
                    Label2.Text = String.Format("{0}:{1}:{2}", time1.Hours, time1.Minutes.ToString(), time1.Seconds.ToString());
                }
            }
            catch { }

        }

        //link button action for mark question
        protected void btnMark_Click(object sender, EventArgs e)
        {
            //int i = 0;
            //if (btnMark.Text == "Mark")
            //{
            //    obj.insertMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
            //    btnMark.Text = "Unmark";
            //    i = i + 1;

            //}
            //if (btnMark.Text == "Unmark" && i == 0)
            //{
            //    obj.deleteMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
            //    btnMark.Text = "Mark";
            //}
            //generateAnswerSheet();
        }


        //code to show Review Screen to user. 'btnReview' is visible false
        protected void btnReview_Click(object sender, EventArgs e)
        {
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;

            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            //int x = rTime;
            Session["rTime"] = rTimeinMill.ToString();
            Timer1.Enabled = false;
            modalPopupReview.Show();
        }


        //to change the color of selected question.
        public void selectQue()
        {
            for (int i = 0; i < Repeater1.Items.Count; i++)
            {
                Button btn = (Button)Repeater1.Items[i].FindControl("Button1");
                //CheckBox chb = (CheckBox)repQuestions.Items[i].FindControl("chkSelect");
                if (btn.Text == lblSelectedQuesNo.Text)
                {
                    btn.BackColor = System.Drawing.Color.Green;
                    btn.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    btn.BackColor = System.Drawing.Color.WhiteSmoke;
                    btn.ForeColor = System.Drawing.Color.Black;
                }
            }

            DataTable dt = (DataTable)ViewState["myTable"];
            queNo.Text = lblSelectedQuesNo.Text + "/" + dt.Rows.Count.ToString();


        }


        //to change the color of selected section ....
        public void selectSection(string sectionNO)
        {
            for (int i = 0; i < Repeater2.Items.Count; i++)
            {
                Button btn = (Button)Repeater2.Items[i].FindControl("Button1");
                //CheckBox chb = (CheckBox)repQuestions.Items[i].FindControl("chkSelect");
                if (btn.CommandArgument == sectionNO)
                {
                    btn.BackColor = System.Drawing.Color.Green;
                    btn.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    btn.BackColor = System.Drawing.Color.WhiteSmoke;
                    btn.ForeColor = System.Drawing.Color.Black;
                }
            }
        }





        //to set the remaining time to database after solving each question which will be useful for restarting the exam from remaining time
        public void setremainingTime()
        {
            DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }


            if (dt2.Rows.Count > 0)
            {
                obj.updateTimeRem(dt2.Rows[0]["id"].ToString(), rTimeinMill.ToString());
            }

        }

        //to pause the exam and show resume button to user..
        protected void btnPause_Click(object sender, EventArgs e)
        {
            //if (rb1.Checked == true || rb2.Checked == true || rb3.Checked == true || rb4.Checked == true || rb5.Checked == true)
            //{
            //    saveUserAnswer();
            //}
            setTimeSpentandUserAnswer();
            Timer1.Enabled = false;
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            //int x = rTime;
            Session["rTime1"] = rTimeinMill.ToString();
            Panel6.Visible = false;
            //Label2.Visible = false;
            Label3.Visible = false;
            //Label4.Visible = false;
            Panel7.Visible = false;



            modalPauseExam.Show();
        }


        //to save the exam for later restarting....
        protected void btnsaveForLater_Click(object sender, EventArgs e)
        {

            //saveUserAnswer();
            //setTimeSpent();
            setTimeSpentandUserAnswer();
            //setremainingTime();
            string script = "<script type=\"text/javascript\">window.close();</script>";
            //Page page = HttpContext.Current.CurrentHandler as Page;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }


        //to show directions page....
        protected void linkShowDirections_Click(object sender, EventArgs e)
        {
            //if (rb1.Checked == true || rb2.Checked == true || rb3.Checked == true || rb4.Checked == true || rb5.Checked == true)
            //{
            //    saveUserAnswer();
            //}
            setTimeSpentandUserAnswer();
            Panel7.Visible = false;
            Panel6.Visible = false;
            //Label2.Visible = false;
            Label3.Visible = false;
            //Label4.Visible = false;
            Timer1.Enabled = false;
            //int rTimeinMill = 0;
            //string[] rTime = Label2.Text.Split(':');

            //if (rTime[0].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[0].ToString()) > 0)
            //    {
            //        rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
            //    }
            //}

            //if (rTime[1].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[1].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
            //    }
            //}

            //if (rTime[2].Length > 0)
            //{
            //    if (Convert.ToInt32(rTime[2].ToString()) > 0)
            //    {
            //        rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
            //    }
            //}
            //Session["rTime2"] = rTimeinMill;
            Session["ShowDir"] = "True";
            modalShowDirections.Show();
        }


        //image button to show clock
        protected void imgShowClock_Click(object sender, ImageClickEventArgs e)
        {

            Label2.Visible = true;
            Label4.Visible = true;
            imgShowClock.Visible = false;
            lnkShowClock.Visible = false;
            imgHideClock.Visible = true;
            lnkHideClock.Visible = true;
        }


        //imagebutton to hide clock
        protected void imgHideClock_Click(object sender, ImageClickEventArgs e)
        {
            Label2.Visible = false;
            Label4.Visible = false;
            imgShowClock.Visible = true;
            lnkShowClock.Visible = true;
            imgHideClock.Visible = false;
            lnkHideClock.Visible = false;
        }


        // link button to hide clock
        protected void lnkHideClock_Click(object sender, EventArgs e)
        {
            Label2.Visible = false;
            Label4.Visible = false;
            imgShowClock.Visible = true;
            lnkShowClock.Visible = true;
            imgHideClock.Visible = false;
            lnkHideClock.Visible = false;
        }


        //link button to show clock
        protected void lnkShowClock_Click(object sender, EventArgs e)
        {


            Label2.Visible = true;
            Label4.Visible = true;
            imgShowClock.Visible = false;
            lnkShowClock.Visible = false;
            imgHideClock.Visible = true;
            lnkHideClock.Visible = true;
        }



        //to get proper action after click on Section buttons...
        protected void btnRep2_Click(object sender, EventArgs e)
        {
            //setTimeSpent();
            // setremainingTime();
            //if (rb1.Checked == true || rb2.Checked == true || rb3.Checked == true || rb4.Checked == true || rb5.Checked == true)
            //{
            //saveUserAnswer();
            //}
            setTimeSpentandUserAnswer();
            Button btn = (Button)sender;
            string sectionNo = btn.CommandArgument;

            lblSectionNo.Text = sectionNo;
            setSectionAndQue(sectionNo);


            selectQue(); setNextPrevious();
            selectSection(sectionNo);
            //queNo.Text = lblSelectedQuesNo.Text + "/" + questions.Rows.Count.ToString();
        }




        //to set Section number and load proper question numbers buttons in repeater....
        public void setSectionAndQue(string sectionNo)
        {
            try
            {
                DataTable questions = (DataTable)ViewState["myTable"];

                int totalQuestions = questions.Rows.Count;
                int questionInSection = totalQuestions / Convert.ToInt16(lblSections.Text);

                DataTable dtLoadQuestionNos = new DataTable();
                dtLoadQuestionNos.Columns.Add("serialnumber", typeof(string));
                dtLoadQuestionNos.Columns.Add("questid", typeof(string));
                int z = 0;
                int fromQue = 0; int toQue = 0;
                if (sectionNo == "1")
                {
                    fromQue = 0;
                    toQue = questionInSection;
                }

                if (sectionNo == "2")
                {
                    fromQue = questionInSection;
                    toQue = questionInSection * 2;
                }

                if (sectionNo == "3")
                {
                    fromQue = (questionInSection * 2);
                    toQue = questionInSection * 3;

                }
                if (sectionNo == "4")
                {
                    fromQue = (questionInSection * 3);
                    toQue = questionInSection * 4;
                }

                if (sectionNo == "5")
                {
                    fromQue = (questionInSection * 4);
                    toQue = questionInSection * 5;

                }

                if (sectionNo == "6")
                {
                    fromQue = questionInSection * 5;
                    toQue = questionInSection * 6;
                }
                if (sectionNo == "7")
                {
                    fromQue = questionInSection * 6;
                    toQue = questionInSection * 7;
                }
                if (sectionNo == "8")
                {
                    fromQue = questionInSection * 7;
                    toQue = questionInSection * 8;
                }

                if (sectionNo == "9")
                {
                    fromQue = questionInSection * 8;
                    toQue = questionInSection * 9;
                }
                if (sectionNo == "10")
                {
                    fromQue = questionInSection * 9;
                    toQue = questionInSection * 10;
                }

                lblSelectedQuesNo.Text = (fromQue + 1).ToString();
                lblQuestionNo.Text = questions.Rows[fromQue][0].ToString();
                ViewState["in"] = fromQue + 1;
                loadQuestion();
                for (int x = fromQue; x < toQue; x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);

                    string s1 = questions.Rows[x]["serialnumber"].ToString();
                    string s2 = questions.Rows[x]["questid"].ToString();
                }
                Repeater1.DataSource = dtLoadQuestionNos;
                Repeater1.DataBind();
            }
            catch { }
        }

        //to load questions..
        public void loadQuestion()
        {
            //DataTable dt = new DataTable();
            //dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,option5,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 when answer = 5 then option5 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + lblQuestionNo.Text + "");
            // dt = obj.getData("SELECT questid, question, option1, option2, option3, option4, option5, answer, subid, difflevel, userid, negativeDeduction, mark, yourAns, selectedOption, CorrectAns, questioninfo, infoID, infHeading, infContents , Ismarked FROM view_examQuestionsDetails WHERE (questid = " + lblQuestionNo.Text + ") AND (uid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ")");

            DataTable dt = (DataTable)ViewState["myTable"];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["questid"].ToString() == lblQuestionNo.Text)
                {
                    lblQue.Text = dt.Rows[i]["question"].ToString();
                    lblOptionA.Text = dt.Rows[i]["option1"].ToString();
                    lblOptionB.Text = dt.Rows[i]["option2"].ToString();
                    lblOptionC.Text = dt.Rows[i]["option3"].ToString();
                    lblOptionD.Text = dt.Rows[i]["option4"].ToString();
                    lblOptionE.Text = dt.Rows[i]["option5"].ToString();

                    clearAllRadio();


                    //                   DataTable dt2 = obj.getData("Select id from exm_markedQuestions  where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + ") ");
                    DataTable markedQuestion = new DataTable();
                    markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");

                    if (markedQuestion.Rows.Count > 0)
                    {
                        btnMark.Text = "Unmark";
                        iBtnUnMark.Visible = true;
                        iBtnMark.Visible = false;
                    }
                    else
                    {
                        btnMark.Text = "Mark";
                        iBtnMark.Visible = true;
                        iBtnUnMark.Visible = false;
                    }


                    try
                    {
                        if (dt.Rows[i]["infoID"].ToString() != "0")
                        {
                            Panel5.Visible = true;
                            hrli.Visible = true;//to show/hide common data heading... 
                            DataTable dtqueData = new DataTable();
                            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[i]["infoID"].ToString() + " ");
                            if (dt.Rows[i]["infContents"].ToString().Trim().Length > 0)
                            {
                                repQueInfo.DataSource = dtqueData;
                                repQueInfo.DataBind();
                            }
                            else
                            {
                                Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
                            }
                        }
                        else
                        {
                            Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
                        }
                    }
                    catch { }

                    string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
                    if (givenAnswer != "0")
                    {
                        if (givenAnswer == "1")
                        { rb1.Checked = true; }
                        if (givenAnswer == "2")
                        { rb2.Checked = true; }
                        if (givenAnswer == "3")
                        { rb3.Checked = true; }
                        if (givenAnswer == "4")
                        { rb4.Checked = true; }
                        if (givenAnswer == "5")
                        { rb5.Checked = true; }
                    }
                }
            }
            setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);
            setNextPrevious();
            selectQue();

            //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");








        }

        //to show answersheet to user...
        public void generateAnswerSheet()
        {

            DataTable dtAnsewers = new DataTable();
            dtAnsewers = obj.getData("SELECT id,questno, answer,case when (select id from exm_markedQuestions where questionno = questno and examid=" + lblExamID.Text + " and userid= " + lbluserID.Text + ") is not null then 'Marked' else 'Not Marked' end as IsMarked FROM exm_userAnswers WHERE userid =" + lbluserID.Text + " AND exmid = " + lblExamID.Text + "");

            DataTable questions = (DataTable)ViewState["myTable"];
            DataTable dt = new DataTable();

            dt.Columns.Add("questid", typeof(string));

            dt.Columns.Add("serialnumber", typeof(string));
            dt.Columns.Add("IsMarked", typeof(string));

            for (int i = 0; i < dtAnsewers.Rows.Count; i++)
            {
                if (dtAnsewers.Rows[i]["questno"].ToString() == questions.Rows[i][0].ToString())
                {
                    DataRow dtrow = dt.NewRow();

                    dtrow["questid"] = dtAnsewers.Rows[i]["questno"].ToString();
                    dtrow["serialnumber"] = (i + 1).ToString();

                    if (dtAnsewers.Rows[i]["IsMarked"].ToString() == "Marked")
                    {
                        dtrow["IsMarked"] = "<img src='/images/apply2.png'   alt='Marked' />";
                    }
                    else
                    { dtrow["IsMarked"] = ""; }




                    dt.Rows.Add(dtrow);

                }


            }
            //lblAnswerSheet.Text = dt.ToString();
            // GridView2.DataSource = dt;
            // GridView2.DataBind();
            //GridView2.Visible = false;
            repAnsSheet.DataSource = dt;
            repAnsSheet.DataBind();

            for (int j = 0; j < repAnsSheet.Items.Count; j++)
            {
                LinkButton lbn = (LinkButton)repAnsSheet.Items[j].FindControl("LinkButton3");
                if (dtAnsewers.Rows[j]["questno"].ToString() == lbn.CommandArgument)
                {
                    if (dtAnsewers.Rows[j]["answer"].ToString() == "1")
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption1");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption1");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (dtAnsewers.Rows[j]["answer"].ToString() == "2")
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption2");
                        lbl.Text = "<img src='/images/radioAns.png' alt=''  width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption2");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (dtAnsewers.Rows[j]["answer"].ToString() == "3")
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption3");
                        lbl.Text = "<img src='/images/radioAns.png' alt=''  width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption3");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (dtAnsewers.Rows[j]["answer"].ToString() == "4")
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption4");
                        lbl.Text = "<img src='/images/radioAns.png' alt=''  width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption4");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (dtAnsewers.Rows[j]["answer"].ToString() == "5")
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption5");
                        lbl.Text = "<img src='/images/radioAns.png' alt=''  width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[j].FindControl("lblOption5");
                        lbl.Text = "<img src='/images/radio.png' alt=''   width='12px' height='12px'/>";
                    }

                }
            }

            UpdatePanel5.Update();
        }

        public void updateAnswerSheet(string ans)
        {
            for (int i = 0; i < repAnsSheet.Items.Count; i++)
            {
                Label lbl1 = (Label)repAnsSheet.Items[i].FindControl("lblQid");
                if (lbl1.Text == lblQuestionNo.Text)
                {
                    if (ans == "1")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption1");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption1");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (ans == "2")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption2");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption2");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (ans == "3")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption3");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption3");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (ans == "4")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption4");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption4");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (ans == "5")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption5");
                        lbl.Text = "<img src='/images/radioAns.png' alt='' width='12px' height='12px' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("lblOption5");
                        lbl.Text = "<img src='/images/radio.png' alt=''  width='12px' height='12px' />";
                    }
                    if (btnMark.Text == "Unmark")
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("Label1");
                        lbl.Text = "<img src='/images/apply2.png'   alt='Marked' />";
                    }
                    else
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("Label1");
                        lbl.Text = "";
                    }
                }

            }
            UpdatePanel5.Update();
        }


        //to jump to selected question number from answer sheet..
        protected void btnToQue_Click(object sender, EventArgs e)
        {

            // saveUserAnswer();

            setTimeSpentandUserAnswer();
            DataTable questions = (DataTable)ViewState["myTable"];
            LinkButton btn = (LinkButton)sender;
            string qID = btn.CommandArgument;
            int z = Convert.ToInt16(btn.Text);
            lblSelectedQuesNo.Text = z.ToString();

            ViewState["in"] = z;

            lblQuestionNo.Text = qID;
            loadQuestion();
            //DataTable dt = new DataTable();
            //dt = obj.getData("SELECT questid,question, option1, option2, option3, option4,option5,case when answer = 1 then option1 when answer = 3 then option3 when answer = 2 then option2 when answer = 4 then option4 when answer = 5 then option5 end as Answer, difflevel, userid, infoID FROM exm_questions where questid = " + qID + "");
            //lblQue.Text = dt.Rows[0][1].ToString();
            //lblOptionA.Text = dt.Rows[0][2].ToString();
            //lblOptionB.Text = dt.Rows[0][3].ToString();
            //lblOptionC.Text = dt.Rows[0][4].ToString();
            //lblOptionD.Text = dt.Rows[0][5].ToString();
            //lblOptionE.Text = dt.Rows[0][6].ToString();

            //setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

            //clearAllRadio();
            //string givenAnswer = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //if (givenAnswer != "0")
            //{
            //    if (givenAnswer == "1")
            //    { rb1.Checked = true; }
            //    if (givenAnswer == "2")
            //    { rb2.Checked = true; }
            //    if (givenAnswer == "3")
            //    { rb3.Checked = true; }
            //    if (givenAnswer == "4")
            //    { rb4.Checked = true; }
            //    if (givenAnswer == "5")
            //    { rb5.Checked = true; }
            //}


            //try
            //{
            //    if (dt.Rows[0]["infoID"].ToString() != "0")
            //    {
            //        Panel5.Visible = true;
            //        hrli.Visible = true;//to show/hide common data heading... 
            //        DataTable dtqueData = new DataTable();
            //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[0]["infoID"].ToString() + " ");
            //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //        {
            //            repQueInfo.DataSource = dtqueData;
            //            repQueInfo.DataBind();
            //        }
            //        else
            //        {
            //            Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading...
            //        }
            //    }
            //    else
            //    {
            //        Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
            //    }
            //}
            //catch { }

            //DataTable markedQuestion = new DataTable();
            //markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            //if (markedQuestion.Rows.Count > 0)
            //{
            //    btnMark.Text = "Unmark";
            //    iBtnUnMark.Visible = true;
            //    iBtnMark.Visible = false;
            //}
            //else
            //{
            //    btnMark.Text = "Mark";
            //    iBtnMark.Visible = true;
            //    iBtnUnMark.Visible = false;
            //}



            if (lblSections.Text.Length > 0)
            {
                int TotalSections = Repeater2.Items.Count;
                int totalQuestions = questions.Rows.Count;
                int questionInSection = totalQuestions / Convert.ToInt16(TotalSections);
                int queNos = 0;
                string secFound = "No";
                for (int i = 1; i < 10; i++)
                {
                    if (secFound == "No")
                    {
                        queNos = questionInSection * i;
                        if (z <= queNos)
                        {
                            secFound = i.ToString();
                        }
                    }
                }

                generateQuestions(secFound);
                //selectQue();


            }

            selectQue();
            UpdatePanel1.Update();
        }

        //to show proper question numbers button depending upon section selected....
        public void generateQuestions(string secFound)
        {
            DataTable questions = (DataTable)ViewState["myTable"];
            DataTable dtLoadQuestionNos = new DataTable();
            dtLoadQuestionNos.Columns.Add("serialnumber", typeof(string));
            dtLoadQuestionNos.Columns.Add("questid", typeof(string));
            int TotalSections = Repeater2.Items.Count;
            int totalQuestions = questions.Rows.Count;
            int questionInSection = totalQuestions / Convert.ToInt16(TotalSections);
            if (secFound == "1")
            {
                for (int x = 0; x < questionInSection; x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }
            if (secFound == "2")
            {
                for (int x = questionInSection; x < (questionInSection * 2); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }

            if (secFound == "3")
            {
                for (int x = questionInSection * 2; x < questionInSection * 3; x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }
            if (secFound == "4")
            {
                for (int x = questionInSection * 3; x < (questionInSection * 4); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }

            if (secFound == "5")
            {
                for (int x = questionInSection * 4; x < (questionInSection * 5); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);

                }
            }

            if (secFound == "6")
            {
                for (int x = questionInSection * 5; x < questionInSection * 6; x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);

                }
            }
            if (secFound == "7")
            {
                for (int x = questionInSection * 6; x < (questionInSection * 7); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }
            if (secFound == "8")
            {
                for (int x = questionInSection * 7; x < (questionInSection * 8); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }

            if (secFound == "9")
            {
                for (int x = questionInSection * 8; x < (questionInSection * 9); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }
            if (secFound == "10")
            {
                for (int x = questionInSection * 9; x < (questionInSection * 10); x++)
                {
                    DataRow dtrow = dtLoadQuestionNos.NewRow();
                    dtrow["serialnumber"] = questions.Rows[x]["serialnumber"].ToString();
                    dtrow["questid"] = questions.Rows[x]["questid"].ToString();
                    dtLoadQuestionNos.Rows.Add(dtrow);
                }
            }

            Repeater1.DataSource = dtLoadQuestionNos;
            Repeater1.DataBind();

            selectSection(secFound);
            lblSectionNo.Text = secFound;
        }


        // to show all questions to user..
        protected void btnSwitchView_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["myTable"];
            Session["queData"] = dt;
            Session["examID"] = lblExamID.Text;
            Session["userID"] = lbluserID.Text;

            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }
            //int x = rTime;
            Session["rTime"] = rTimeinMill.ToString();
            Timer1.Enabled = false;
            ModalPopupSwitchView.Show();
        }

        // to hide/show options for questions...
        public void setQueOptions(string option1, string option2, string option3, string option4, string option5)
        {
            if (option1.Trim().Length > 0)
            {
                rb1.Visible = true;
                lblOptionA.Visible = true;
                lblQue1.Visible = true;
            }
            else
            {
                rb1.Visible = false;
                lblOptionA.Visible = false;
                lblQue1.Visible = false;
            }

            if (option2.Trim().Length > 0)
            {
                rb2.Visible = true;
                lblOptionB.Visible = true;
                lblQue2.Visible = true;
            }
            else
            {
                rb2.Visible = false;
                lblOptionB.Visible = false;
                lblQue2.Visible = false;
            }

            if (option3.Trim().Length > 0)
            {
                rb3.Visible = true;
                lblOptionC.Visible = true;
                lblQue3.Visible = true;
            }
            else
            {
                rb3.Visible = false;
                lblOptionC.Visible = false;
                lblQue4.Visible = false;
            }

            if (option4.Trim().Length > 0)
            {
                rb4.Visible = true;
                lblOptionD.Visible = true;
                lblQue4.Visible = true;
            }
            else
            {
                rb4.Visible = false;
                lblOptionD.Visible = false;
                lblQue4.Visible = false;
            }

            if (option5.Trim().Length > 0)
            {
                rb5.Visible = true;
                lblOptionE.Visible = true;
                lblQue5.Visible = true;
            }
            else
            {
                rb5.Visible = false;
                lblOptionE.Visible = false;
                lblQue5.Visible = false;
            }
        }


        //to save the timespent on each question to database...
        public void setTimeSpent()
        {
            TimeSpan time1 = new TimeSpan();
            time1 = ((DateTime)Session["time"]) - DateTime.Now;
            ////string dddd = DateTime.Now.ToString();
            ////string ptttt = lblPreviousTime.Text;
            time1 = DateTime.Now - Convert.ToDateTime(lblPreviousTime.Text);
            int tMin = 0;
            if (time1.Minutes > 0)
            {
                tMin = time1.Minutes * 60;
            }
            int totalTime = tMin + time1.Seconds;

            lblPreviousTime.Text = DateTime.Now.ToString();

            int preTime = Convert.ToInt32(obj.getValue("Select Case when timeSpent is null then 0 else timeSpent end as timeSpent from exm_userAnswers where userid = " + lbluserID.Text + " and exmid= " + lblExamID.Text + " and questno=" + lblQuestionNo.Text + ""));

            obj.setTimeSpent((totalTime + preTime).ToString(), lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
        }

        //to save answer for question..
        public void saveUserAnswer()
        {
            string ans = "0";
            if (rb1.Checked == true)
            {
                ans = "1";
            }
            if (rb2.Checked == true)
            {
                ans = "2";
            }
            if (rb3.Checked == true)
            {
                ans = "3";
            }
            if (rb4.Checked == true)
            {
                ans = "4";
            }
            if (rb5.Checked == true)
            {
                ans = "5";
            }
            // = (rblOptions.SelectedIndex + 1).ToString();
            if (lblGivenAns.Text.Length > 0)
            {
                lblGivenAns.Text = lblGivenAns.Text + "," + ans;
            }
            else { lblGivenAns.Text = ans; }
            DataTable dt2 = obj.getData("SELECT id, answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }


            if (dt2.Rows.Count > 0)
            {
                obj.updateUserAnswer(dt2.Rows[0]["id"].ToString(), ans, rTimeinMill);
            }
            else
            {
                obj.insertUseAnswer(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text, ans, rTimeinMill);
            }
            //  generateAnswerSheet();
        }



        public void setTimeSpentandUserAnswer()
        {
            TimeSpan time1 = new TimeSpan();
            time1 = ((DateTime)Session["time"]) - DateTime.Now;
            time1 = DateTime.Now - Convert.ToDateTime(lblPreviousTime.Text);
            int tMin = 0;
            if (time1.Minutes > 0)
            {
                tMin = time1.Minutes * 60;
            }
            int totalTime = tMin + time1.Seconds;

            lblPreviousTime.Text = DateTime.Now.ToString();

            //int preTime = Convert.ToInt32(obj.getValue("Select Case when timeSpent is null then 0 else timeSpent end as timeSpent from exm_userAnswers where userid = " + lbluserID.Text + " and exmid= " + lblExamID.Text + " and questno=" + lblQuestionNo.Text + ""));

            string ans = "0";
            if (rb1.Checked == true)
            {
                ans = "1";
            }
            if (rb2.Checked == true)
            {
                ans = "2";
            }
            if (rb3.Checked == true)
            {
                ans = "3";
            }
            if (rb4.Checked == true)
            {
                ans = "4";
            }
            if (rb5.Checked == true)
            {
                ans = "5";
            }

            int rTimeinMill = 0;
            string[] rTime = Label2.Text.Split(':');

            if (rTime[0].Length > 0)
            {
                if (Convert.ToInt32(rTime[0].ToString()) > 0)
                {
                    rTimeinMill = Convert.ToInt32(rTime[0].ToString()) * 3600000;
                }
            }

            if (rTime[1].Length > 0)
            {
                if (Convert.ToInt32(rTime[1].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[1].ToString()) * 60000;
                }
            }

            if (rTime[2].Length > 0)
            {
                if (Convert.ToInt32(rTime[2].ToString()) > 0)
                {
                    rTimeinMill = rTimeinMill + Convert.ToInt32(rTime[2].ToString()) * 1000;
                }
            }

            obj.updateTimespentAndUserAnswer(ans, rTimeinMill.ToString(), totalTime, lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);

            // generateAnswerSheet();
            updateAnswerSheet(ans);

        }


        // to show previous question...
        protected void btnPre_Click(object sender, EventArgs e)
        {
            setTimeSpentandUserAnswer();

            //setTimeSpent();
            //setremainingTime();
            // System.Threading.Thread.Sleep(2000);

            DataTable dt = (DataTable)ViewState["myTable"];
            int j = (int)ViewState["in"];
            j = j - 2;

            //if (dt.Rows.Count > j)
            //{
            lblQuestionNo.Text = dt.Rows[j][0].ToString();
            loadQuestion();

            //lblQue.Text = dt.Rows[j][1].ToString();
            //lblOptionA.Text = dt.Rows[j][2].ToString();
            //lblOptionB.Text = dt.Rows[j][3].ToString();
            //lblOptionC.Text = dt.Rows[j][4].ToString();
            //lblOptionD.Text = dt.Rows[j][5].ToString();
            //lblOptionE.Text = dt.Rows[j][6].ToString();

            //setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

            //clearAllRadio();
            //string givenAnswer1 = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //if (givenAnswer1 != "0")
            //{
            //    if (givenAnswer1 == "1")
            //    { rb1.Checked = true; }
            //    if (givenAnswer1 == "2")
            //    { rb2.Checked = true; }
            //    if (givenAnswer1 == "3")
            //    { rb3.Checked = true; }
            //    if (givenAnswer1 == "4")
            //    { rb4.Checked = true; }
            //    if (givenAnswer1 == "5")
            //    { rb5.Checked = true; }
            //}

            //try
            //{
            //    if (dt.Rows[j]["infoID"].ToString() != "0")
            //    {
            //        hrli.Visible = true;//to show/hide common data heading... 
            //        Panel5.Visible = true;
            //        DataTable dtqueData = new DataTable();
            //        dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
            //        if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //        {
            //            repQueInfo.DataSource = dtqueData;
            //            repQueInfo.DataBind();
            //        }
            //        else
            //        {
            //            Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
            //        }
            //    }
            //    else
            //    {
            //        Panel5.Visible = false; hrli.Visible = false;//to show/hide common data heading... 
            //    }
            //}
            //catch { }





            //DataTable markedQuestion = new DataTable();
            //markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            //if (markedQuestion.Rows.Count > 0)
            //{
            //    btnMark.Text = "Unmark";
            //    iBtnUnMark.Visible = true;
            //    iBtnMark.Visible = false;
            //}
            //else
            //{
            //    btnMark.Text = "Mark";
            //    iBtnMark.Visible = true;
            //    iBtnUnMark.Visible = false;
            //}

            if (lblSections.Text.Length > 0)
            {
                int TotalSections = Repeater2.Items.Count;
                int totalQuestions = dt.Rows.Count;
                int questionInSection = totalQuestions / Convert.ToInt16(TotalSections);
                int queNos = 0;
                string secFound = "No";
                for (int i = 1; i < 10; i++)
                {
                    if (secFound == "No")
                    {
                        queNos = questionInSection * i;
                        if (j + 1 <= queNos)
                        {
                            secFound = i.ToString();
                        }
                    }
                }

                generateQuestions(secFound);
            }
            j++;
            ViewState["in"] = j;
            lblSelectedQuesNo.Text = j.ToString();
            selectQue();
            setNextPrevious();
            //generateAnswerSheet();
            //}
            //else
            //{
            //    Session["dtable"] = dt;
            //    Session["ans"] = lblGivenAns.Text;
            //    Response.Redirect("ExamResult.aspx");

            //    j = 0;

            //    lblQuestionNo.Text = dt.Rows[j][0].ToString();
            //    lblQue.Text = dt.Rows[j][1].ToString();
            //    lblOptionA.Text = dt.Rows[j][2].ToString();
            //    lblOptionB.Text = dt.Rows[j][3].ToString();
            //    lblOptionC.Text = dt.Rows[j][4].ToString();
            //    lblOptionD.Text = dt.Rows[j][5].ToString();
            //    lblOptionE.Text = dt.Rows[j][6].ToString();

            //    setQueOptions(lblOptionA.Text, lblOptionB.Text, lblOptionC.Text, lblOptionD.Text, lblOptionE.Text);

            //    clearAllRadio();
            //    string givenAnswer1 = obj.getValue("SELECT answer FROM exm_userAnswers WHERE (userid = " + lbluserID.Text + ") AND (exmid = " + lblExamID.Text + ") AND (questno = " + lblQuestionNo.Text + ")");
            //    if (givenAnswer1 != "0")
            //    {
            //        if (givenAnswer1 == "1")
            //        { rb1.Checked = true; }
            //        if (givenAnswer1 == "2")
            //        { rb2.Checked = true; }
            //        if (givenAnswer1 == "3")
            //        { rb3.Checked = true; }
            //        if (givenAnswer1 == "4")
            //        { rb4.Checked = true; }
            //        if (givenAnswer1 == "5")
            //        { rb5.Checked = true; }
            //    }
            //    if (Repeater2.Items.Count > 0)
            //    {
            //        setSectionAndQue("1");
            //        selectSection("Section 1");
            //    }
            //    try
            //    {
            //        if (dt.Rows[j]["infoID"].ToString() != "0")
            //        {
            //            Panel5.Visible = true;
            //            DataTable dtqueData = new DataTable();
            //            dtqueData = obj.getData("Select infContents from exm_queInfo where infID = " + dt.Rows[j]["infoID"].ToString() + " ");
            //            if (dtqueData.Rows[0][0].ToString().Trim().Length > 0)
            //            {
            //                repQueInfo.DataSource = dtqueData;
            //                repQueInfo.DataBind();
            //            }
            //            else { Panel5.Visible = false; }
            //        }
            //        else
            //        {
            //            Panel5.Visible = false;
            //        }
            //    }
            //    catch { }


            //    DataTable markedQuestion = new DataTable();
            //    markedQuestion = obj.getData("Select * from exm_markedQuestions where userid = " + lbluserID.Text + " and examid = " + lblExamID.Text + " and questionNo = " + lblQuestionNo.Text + "");
            //    if (markedQuestion.Rows.Count > 0)
            //    {
            //        btnMark.Text = "Unmark";
            //    }
            //    else { btnMark.Text = "Mark"; }





            //    j++;
            //    ViewState["in"] = j;
            //    lblSelectedQuesNo.Text = j.ToString();

            //    string script = "<script type=\"text/javascript\">showMessage();</script>";
            //    Page page = HttpContext.Current.CurrentHandler as Page;
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);

            //}


        }

        //to hide/show Next and Previous buttons depending upon question number...
        public void setNextPrevious()
        {
            DataTable dt = (DataTable)ViewState["myTable"];
            if (lblSelectedQuesNo.Text == "1")
            {
                lBtnNext.Enabled = true;
                lBtnPre.Enabled = false;

                lBtnPre.Visible = false;
                lBtnNext.Visible = true;
            }
            if (lblSelectedQuesNo.Text == dt.Rows.Count.ToString())
            {
                lBtnNext.Enabled = false;
                lBtnPre.Enabled = true;

                lBtnNext.Visible = false;
                lBtnPre.Visible = true;
            }
            if (lblSelectedQuesNo.Text != "1" && lblSelectedQuesNo.Text != dt.Rows.Count.ToString())
            {
                lBtnNext.Enabled = true;
                lBtnPre.Enabled = true;
                lBtnNext.Visible = true;
                lBtnPre.Visible = true;
            }

        }

        //to mark the selected question
        protected void iBtnMark_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = obj.getData("Select id from exm_markedQuestions where userid = " + lbluserID.Text + " and examid=" + lblExamID.Text + " and questionNo= " + lblQuestionNo.Text + "");
            if (dt.Rows.Count == 0)
            {
                obj.insertMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
                btnMark.Text = "Unmark";
                iBtnMark.Visible = false;
                iBtnUnMark.Visible = true;
                //generateAnswerSheet();

                for (int i = 0; i < repAnsSheet.Items.Count; i++)
                {
                    Label lbl1 = (Label)repAnsSheet.Items[i].FindControl("lblQid");
                    if (lbl1.Text == lblQuestionNo.Text)
                    {
                        Label lbl = (Label)repAnsSheet.Items[i].FindControl("Label1");
                        lbl.Text = "<img src='/images/apply2.png'   alt='Marked' />";
                    }

                }
                UpdatePanel5.Update();
            }
        }

        //to unmark the selected question... 
        protected void iBtnUnMark_Click(object sender, ImageClickEventArgs e)
        {
            obj.deleteMarked(lbluserID.Text, lblExamID.Text, lblQuestionNo.Text);
            btnMark.Text = "Mark";
            iBtnMark.Visible = true;
            iBtnUnMark.Visible = false;
            //generateAnswerSheet();

            for (int i = 0; i < repAnsSheet.Items.Count; i++)
            {
                Label lbl1 = (Label)repAnsSheet.Items[i].FindControl("lblQid");
                if (lbl1.Text == lblQuestionNo.Text)
                {
                    Label lbl = (Label)repAnsSheet.Items[i].FindControl("Label1");
                    lbl.Text = "";
                }

            }
            UpdatePanel5.Update();
        }
    }
}
