﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="exmResult.aspx.cs" Inherits="OnlineExam.exmResult" Title="Exam Result" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/accordion.css" rel="stylesheet" type="text/css" />
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblExamID" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblUserID" Visible="false" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblInstituteId" Visible="false" runat="server" Text="Label"></asp:Label>
            <table width="950px">
                <tr>
                    <td align="left">
                    </td>
                    <td align="right">
                    </td>
                </tr>
            </table>
            <table width="950px">
                <tr>
                    <td align="left">
                        <div class="pageHeading">
                            Exam Result</div>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel3" runat="server">
                            <table width="90%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblName" runat="server" Text="Label" CssClass="myLabelHead"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblExamName" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <asp:Label ID="Label8" runat="server" Text="Label" Visible="false"></asp:Label>
                <tr>
                    <td align="center">
                        <asp:Accordion ID="Accordion1" runat="server" FadeTransitions="true" TransitionDuration="250"
                            FramesPerSecond="40" AutoSize="None" HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
                            ContentCssClass="accordionContent" RequireOpenedPane="false">
                            <Panes>
                                <%--Result Summary--%>
                                <asp:AccordionPane ID="AccPan1" runat="server">
                                    <Header>
                                        Result Summary</Header>
                                    <Content>
                                        <asp:Panel ID="Panel2" runat="server">
                                            <table border="1" width="100%">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="Label2" runat="server" Text="Total Questions" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label3" runat="server" Text="Questions Attempted" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label4" runat="server" Text="Correct Answers" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label5" runat="server" Text="Incorrect Answers" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblTotalQue" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblSolvedQuestions" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblCorrect" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblIncorrect" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="height: 2px; background-color: Blue">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="Label7" runat="server" Text="Total Marks" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label9" runat="server" Text="Percentile" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label14" runat="server" Text="Accuracy" CssClass="myLabelHead"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="Label15" runat="server" Text="Total Time" CssClass="myLabelHead" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblTMarks" runat="server" Text="" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblPercentile" runat="server" Text="" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblAccuracy" runat="server" Text="" CssClass="myLabel"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="lblTimeSpent" runat="server" Text="" CssClass="myLabel" Visible="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </Content>
                                </asp:AccordionPane>
                                <%--Section wise Details--%>
                                <asp:AccordionPane runat="server" ID="AccordionPane2">
                                    <Header>
                                        Sectionwise Details
                                    </Header>
                                    <Content>
                                        <asp:GridView ID="GridView2" runat="server" CssClass="gridview" Visible="false" RowStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Center">
                                        </asp:GridView>
                                        <asp:Repeater ID="repSectionWiseDetails" runat="server">
                                            <ItemTemplate>
                                                <table border="1" width="100%">
                                                    <tr style="background-color: #1A70F0;">
                                                        <td colspan="4" align="left">
                                                            <asp:Label ID="Label16" runat="server" Text='<%# Eval("sectionNo") %>' Font-Bold="true"
                                                                Font-Size="Medium" ForeColor="White"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="Label2" runat="server" Text="Total Questions" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label3" runat="server" Text="Questions Attempted" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label4" runat="server" Text="Correct Answers" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label5" runat="server" Text="Incorrect Answers" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblTotalQue" runat="server" Text='<%# Eval("totalQue") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblSolvedQuestions" runat="server" Text='<%# Eval("solvedQue") %>'
                                                                CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblCorrect" runat="server" Text='<%# Eval("correctQue") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblIncorrect" runat="server" Text='<%# Eval("incorrectQue") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="height: 2px; background-color: Blue">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="Label7" runat="server" Text="Total Marks" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label9" runat="server" Text="Percentile" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label14" runat="server" Text="Accuracy" CssClass="myLabelHead"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label15" runat="server" Text="Time Spent" CssClass="myLabelHead" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblTMarks" runat="server" Text='<%# Eval("marks") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblPercentile" runat="server" Text='<%# Eval("percentile") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblAccuracy" runat="server" Text='<%# Eval("accuracy") %>' CssClass="myLabel"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="Label20" runat="server" Text='<%# Eval("sessionTime") %>' CssClass="myLabel"
                                                                Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </Content>
                                </asp:AccordionPane>
                                <%--  Result Details--%>
                                <asp:AccordionPane runat="server" ID="AccPane2">
                                    <Header>
                                        Result Details
                                    </Header>
                                    <Content>
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td align="right">
                                                                <asp:LinkButton ID="linkQueAns" runat="server" CausesValidation="false" OnClick="linkQueAns_Click"
                                                                    Style="text-decoration: underline; color: Blue">Questions with Answers</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Label ID="Label6" runat="server" Text="Click question number to view question details"
                                                                    CssClass="myLabel"></asp:Label>
                                                            </td>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" style="width: 15px; background-color: Whitesmoke">
                                                                        </td>
                                                                        <td align="left">
                                                                            Unsolved Questions
                                                                        </td>
                                                                        <td align="left" style="width: 15px; background-color: Red">
                                                                        </td>
                                                                        <td align="left">
                                                                            Incorrect Questions
                                                                        </td>
                                                                        <td align="left" style="width: 15px; background-color: Green">
                                                                        </td>
                                                                        <td align="left">
                                                                            Correct Questions
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="Repeater2_ItemCommand">
                                                        <ItemTemplate>
                                                            <div style="float: left; width: 40px">
                                                                <asp:Button ID="Button1" runat="server" Text='<%# Eval("serialnumber") %>' CommandArgument='<%#Eval("questid") %>'
                                                                    OnClick="btnRep_Click" Width="35px" CausesValidation="false" />
                                                                <asp:Label ID="lblInfo" Visible="false" runat="server" Text='<%# Eval("IsCorrect") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="Repeater1" runat="server">
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:Panel ID="Panel1" runat="server">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td valign="top" style="width: 100px" align="left">
                                                                                        <asp:Label ID="lblQuestionNo" runat="server" Text="Question" CssClass="myLabelHead"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="left">
                                                                                        <asp:Label ID="lblQuestion" runat="server" CssClass="myLabel" Text='<%# Eval("Question") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="Label11" runat="server" Text="Correct Answer" CssClass="myLabelHead"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="left">
                                                                                        <asp:Label ID="lblYourAns" runat="server" Text='<%# Eval("correctAns") %>' CssClass="myLabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="Label12" runat="server" Text="Your Answer" CssClass="myLabelHead"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="left">
                                                                                        <asp:Label ID="lblCorrect" runat="server" CssClass="myLabel" Text='<%# Eval("Answer") %>'></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="Label13" runat="server" Text=" Explanation" CssClass="myLabelHead"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="left">
                                                                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("explanation") %>' CssClass="myLabel"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <%--  <asp:Panel ID="Panel1" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td valign="top" style="width: 100px" align="left">
                                                                    <asp:Label ID="Label10" runat="server" Text="Question" CssClass="myLabelHead"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <asp:Label ID="lblQuestion" runat="server" CssClass="myLabel" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label11" runat="server" Text="Correct Answer" CssClass="myLabelHead"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <asp:Label ID="lblYourAns" runat="server" Text="" CssClass="myLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label12" runat="server" Text="Your Answer" CssClass="myLabelHead"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <asp:Label ID="Label7" runat="server" CssClass="myLabel" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label13" runat="server" Text="Explanation" CssClass="myLabelHead"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <asp:Label ID="Label1" runat="server" Text="" CssClass="myLabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </Content>
                                </asp:AccordionPane>
                                <%--   Questionwise Details--%>
                                <asp:AccordionPane runat="server" ID="AccordionPane1">
                                    <Header>
                                        Questionwise Details
                                    </Header>
                                    <Content>
                                        <asp:GridView ID="gridQueDetails" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                <asp:BoundField HeaderText="Question Nos" DataField="questionNo" />
                                                <asp:BoundField HeaderText="Status" DataField="IsCorrect" />
                                                <%--  <asp:BoundField HeaderText="Time Spent" DataField="timeSpent" />--%>
                                            </Columns>
                                        </asp:GridView>
                                    </Content>
                                </asp:AccordionPane>
                                <asp:AccordionPane runat="server" ID="AccordionPane3">
                                    <Header>
                                        Graphical Analysis
                                    </Header>
                                    <Content>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Chart ID="queDetailsChart" runat="server" Width="500px">
                                                                <Series>
                                                                    <asp:Series Name="Total Questions" ChartArea="ChartArea1" XValueMember="sectionNo"
                                                                        YValueMembers="totalQue" Legend="Legend1">
                                                                    </asp:Series>
                                                                    <asp:Series Name="Correct" ChartArea="ChartArea1" XValueMember="sectionNo" YValueMembers="correctQue"
                                                                        Legend="Legend1">
                                                                    </asp:Series>
                                                                    <asp:Series Name="Incorrect" ChartArea="ChartArea1" XValueMember="sectionNo" YValueMembers="incorrectQue"
                                                                        Legend="Legend1">
                                                                    </asp:Series>
                                                                    <asp:Series Name="Unsolved" ChartArea="ChartArea1" XValueMember="sectionNo" YValueMembers="unSolved"
                                                                        Legend="Legend1">
                                                                    </asp:Series>
                                                                </Series>
                                                                <ChartAreas>
                                                                    <asp:ChartArea Name="ChartArea1">
                                                                        <AxisY Title="Question Nos">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisY>
                                                                        <AxisX Title="Sections" Interval="1">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisX>
                                                                    </asp:ChartArea>
                                                                </ChartAreas>
                                                                <Legends>
                                                                    <asp:Legend Name="Legend1">
                                                                    </asp:Legend>
                                                                </Legends>
                                                                <Titles>
                                                                    <asp:Title Name="Title1" Text="Question Summary">
                                                                    </asp:Title>
                                                                </Titles>
                                                                <BorderSkin BackColor="#D3DEEF" />
                                                            </asp:Chart>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label17" runat="server" Text="Graphical Details for Total, Correct, Incorrect, Solved Questions "
                                                                Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Chart ID="accuracyChart" runat="server" Width="500px">
                                                                <Series>
                                                                    <asp:Series Name="Accuracy" ChartArea="ChartArea1" XValueMember="sectionNo" YValueMembers="accuracy"
                                                                        Legend="Legend1">
                                                                    </asp:Series>
                                                                </Series>
                                                                <ChartAreas>
                                                                    <asp:ChartArea Name="ChartArea1">
                                                                        <AxisY Title="Percentage">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisY>
                                                                        <AxisX Title="Sections" Interval="1">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisX>
                                                                    </asp:ChartArea>
                                                                </ChartAreas>
                                                                <Legends>
                                                                    <asp:Legend Name="Legend1">
                                                                    </asp:Legend>
                                                                </Legends>
                                                                <Titles>
                                                                    <asp:Title Name="Title1" Text="Accuracy Details">
                                                                    </asp:Title>
                                                                </Titles>
                                                                <BorderSkin BackColor="#D3DEEF" />
                                                            </asp:Chart>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label18" runat="server" Text="Accuracy represents how many questions you get correct, out of ones you atempt."
                                                                Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Chart ID="Chart1" runat="server" Width="500px">
                                                                <Series>
                                                                    <asp:Series Name="First Topper Percentile" ChartArea="ChartArea1" XValueMember="sectionNo"
                                                                        YValueMembers="topper1" Legend="Legend1">
                                                                    </asp:Series>
                                                                    <asp:Series Name="Second Topper Percentile" ChartArea="ChartArea1" XValueMember="sectionNo"
                                                                        YValueMembers="topper2" Legend="Legend1">
                                                                    </asp:Series>
                                                                    <asp:Series Name="Your Percentile" ChartArea="ChartArea1" XValueMember="sectionNo"
                                                                        YValueMembers="percentile" Legend="Legend1">
                                                                    </asp:Series>
                                                                </Series>
                                                                <ChartAreas>
                                                                    <asp:ChartArea Name="ChartArea1">
                                                                        <AxisY Title="Percentage">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisY>
                                                                        <AxisX Title="Sections" Interval="1">
                                                                            <MajorGrid Enabled="False" />
                                                                        </AxisX>
                                                                    </asp:ChartArea>
                                                                </ChartAreas>
                                                                <Legends>
                                                                    <asp:Legend Name="Legend1">
                                                                    </asp:Legend>
                                                                </Legends>
                                                                <Titles>
                                                                    <asp:Title Name="Title1" Text="Percentile Details">
                                                                    </asp:Title>
                                                                </Titles>
                                                                <BorderSkin BackColor="#D3DEEF" />
                                                            </asp:Chart>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label19" runat="server" Text="Your position for the exam." Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </Content>
                                </asp:AccordionPane>
                            </Panes>
                        </asp:Accordion>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" runat="server" Visible="false">
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <asp:ModalPopupExtender ID="modalShowQueAns" runat="server" TargetControlID="LinkButton2"
                BackgroundCssClass="ModalPopupBG" PopupControlID="queAns" Drag="true" CancelControlID="btnCanQueAns12">
            </asp:ModalPopupExtender>
            <div id="queAns" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe1" frameborder="0" src="viewAndPrintQuestion.aspx" height="600px"
                    width="800px"></iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="btnOkQueAns12" value="Done" type="button" />
                    <input id="btnCanQueAns12" value="Cancel" type="button" />
                </div>
            </div>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
