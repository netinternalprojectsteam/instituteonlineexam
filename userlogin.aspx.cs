﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class userlogin : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                DataTable dt = new DataTable();
                dt = obj.GetUserLoginInfo(txtEmail.Text, txtPassword.Text);
                if (dt.Rows.Count > 0)
                {
                    FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "User" + "," + "NXGOnlineExam", chkRem.Checked);
                }
                else
                {

                    Response.Redirect("/Default.aspx");
                }

            }
            catch { }
        }
    }
}
