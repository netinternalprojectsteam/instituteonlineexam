﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class loginAdmin : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            //System.Threading.Thread.Sleep(2000);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            DataTable dt = new DataTable();
            dt = ob.GetUsernameAndPassword(txtUName.Text, txtPass.Text);
            if (dt.Rows.Count > 0)
            {
                //FormsAuthentication.RedirectFromLoginPage(dt.Rows[0][0].ToString() + "," + "Admin" + "," + "NXGOnlineExam", chkRem.Checked);
                Session["LoginDetails"] = dt.Rows[0][0].ToString() + "#" + "Admin" + "#" + chkRem.Checked;
                string script = "<script type=\"text/javascript\">callfun();</script>";
                //Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
            else
            {
                Messages11.Visible = true;
                Messages11.setMessage(0, "Invalid Login Details !");
            }

        }
    }
}
