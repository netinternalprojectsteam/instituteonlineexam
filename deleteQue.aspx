﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="deleteQue.aspx.cs" Inherits="OnlineExam.deleteQue" Title="Delete Question" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript"> 
        function ConfirmOnDelete()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Delete Questions
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label1" runat="server" Text="Question Contains"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtData" runat="server" MaxLength="100" Width="450px"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnShow" runat="server" Text="Search" CssClass="simplebtn" OnClick="btnShow_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblInsId" runat="server" Text="Label" Visible="false"></asp:Label>
                        <asp:SqlDataSource ID="sourceGridQuestions" runat="server"></asp:SqlDataSource>
                        <%-- <asp:GridView ID="gridQuestions" DataSourceID="sourceGridQuestions" runat="server"
                            CssClass="gridview" OnRowCreated="gridQuestions_RowCreated" AutoGenerateColumns="False"
                            OnRowCommand="gridQuestions_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="Sr. No.">
                                    <ItemStyle Width="20px" />
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="questid" HeaderText="Unique ID" />
                                <asp:BoundField DataField="question" HeaderText="question" />
                                <asp:TemplateField HeaderText="Question">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridLabel" runat="server" Text="Label"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="courseinfo" HeaderText="Course" />
                                <asp:BoundField DataField="subjectinfo" HeaderText="Subject" />
                                <asp:ButtonField ButtonType="Image" CommandName="Info" HeaderText="Details" ImageUrl="~/images/Details.png"
                                    Text="Button">
                                    <ItemStyle HorizontalAlign="Center" Width="25px" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Image" CommandName="remove" HeaderText="Delete" ImageUrl="~/images/delete.gif"
                                    Text="Button">
                                    <ItemStyle HorizontalAlign="Center" Width="25px" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>--%>
                        <asp:Repeater ID="Repeater2" runat="server" DataSourceID="sourceGridQuestions">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td style="width: 60px">
                                            Unique ID
                                        </td>
                                        <td>
                                            Question
                                        </td>
                                        <td style="width: 100px">
                                            Course
                                        </td>
                                        <td style="width: 100px">
                                            Subject
                                        </td>
                                        <td style="width: 60px">
                                            Info
                                        </td>
                                        <td style="width: 60px">
                                            Delete
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%" style="border: solid 1px black">
                                    <tr>
                                        <td align="left" style="width: 60px">
                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("questid") %>'></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("question") %>'></asp:Label>
                                        </td>
                                        <td align="left" style="width: 100px">
                                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("courseinfo") %>'></asp:Label>
                                        </td>
                                        <td align="left" style="width: 100px">
                                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("subjectinfo") %>'></asp:Label>
                                        </td>
                                        <td align="left" style="width: 60px">
                                            <asp:Button ID="Button1" runat="server" Text="Info" CssClass="simplebtn" CommandArgument='<%#Eval("questid") %>'
                                                OnClick="btnInfo_Click" />
                                        </td>
                                        <td align="left" style="width: 60px">
                                            <asp:Button ID="Button2" runat="server" Text="Delete" CssClass="simplebtn" OnClick="btnDelete_Click"
                                                CommandArgument='<%#Eval("questid") %>' OnClientClick="javascript:return ConfirmOnDelete();" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            <div style="display: none">
                <asp:LinkButton ID="LinkButton1" runat="server">LinkButton</asp:LinkButton></div>
            <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="LinkButton1"
                BackgroundCssClass="ModalPopupBG" PopupControlID="Panel1" Drag="true" CancelControlID="btn3123">
            </asp:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" Width="650" Height="450" ScrollBars="Auto">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Question Informations
                        </div>
                        <div class="TitlebarRight" onclick="cancel();">
                            <b><a id="btn3123" href="#" onclick="cancel();" style="text-decoration: none; color: White">
                                X</a></b>
                        </div>
                    </div>
                    <asp:Panel ID="Panel2" runat="server">
                        <center>
                            <div style="border: solid 2px black" runat="server" id="queHolder" visible="false">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top">
                                            <div style="max-width: 300px; position: relative">
                                                <br />
                                                <br />
                                                <br />
                                                <asp:Panel ID="Panel3" runat="server" Visible="false" Height="300" ScrollBars="Auto">
                                                    <asp:Repeater ID="repQueInfo" runat="server">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("Info") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:Panel>
                                            </div>
                                        </td>
                                        <td align="left">
                                            <div style="width: 99%; min-width: 300px; float: left; vertical-align: top">
                                                <asp:Label ID="lblQueAdded" runat="server" Text="" Visible="false"></asp:Label>
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:Repeater ID="Repeater1" runat="server">
                                                                <ItemTemplate>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:Panel ID="Panel1" runat="server">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td valign="top" style="width: 100px" align="right">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="Label17" runat="server" Text="Correct Marks :"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblMarks" runat="server" Text='<%#Eval("mark") %>'></asp:Label>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            &nbsp;
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="Label18" runat="server" Text="Incorrect Deduction :"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblDeduction" runat="server" Text='<%#Eval("negativeDeduction") %>'></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" style="width: 100px" align="left">
                                                                                                <asp:Label ID="Label10" runat="server" Text="Question" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="lblQuestion" runat="server" CssClass="myLabel" Text='<%# Eval("Question") %>'></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label11" runat="server" Text="Option 1" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="lblYourAns" runat="server" Text='<%# Eval("option1") %>' CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label12" runat="server" Text="Option 2" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="lblCorrect" runat="server" CssClass="myLabel" Text='<%# Eval("option2") %>'></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label13" runat="server" Text="Option 3" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("option3") %>' CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label5" runat="server" Text="Option 4" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("option4") %>' CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label14" runat="server" Text="Option 5" CssClass="myLabelHead"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" align="left">
                                                                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("option5") %>' CssClass="myLabel"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </center>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel4" runat="server" Width="500px" Height="400px">
                <center>
                    <div class="popup_Container">
                        <div class="popup_Titlebar" id="Div1">
                            <div class="TitlebarLeft">
                                Warning
                            </div>
                            <div class="TitlebarRight" onclick="cancel();">
                            </div>
                        </div>
                        <asp:Panel ID="Panel5" runat="server">
                            <center>
                                <b>Following Exam Contains this Question</b>
                                <asp:GridView ID="gridExams" runat="server" CssClass="gridview" Width="450px" OnRowCreated="gridExams_RowCreated">
                                </asp:GridView>
                                <br />
                                <asp:Button ID="Button3" runat="server" Text="Want To Continue" CssClass="simplebtn"
                                    OnClick="Button3_Click" />
                                <asp:Button ID="btn31232" runat="server" Text="Cancel" CssClass="simplebtn" />
                            </center>
                        </asp:Panel>
                    </div>
                </center>
            </asp:Panel>
            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="LinkButton2"
                BackgroundCssClass="ModalPopupBG" PopupControlID="Panel4" Drag="true" CancelControlID="btn31232">
            </asp:ModalPopupExtender>
            <div style="display: none">
                <asp:Label ID="lblID" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>
                <asp:LinkButton ID="LinkButton2" runat="server">LinkButton</asp:LinkButton></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
