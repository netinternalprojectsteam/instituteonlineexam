﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class queData : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInst.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInst.Text = val[3];
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Messages11.Visible = false;
            try
            {
                if (FCKeditor1.Value.Length > 0)
                {
                    ob.insertQuestionInformation(txtHeading.Text, FCKeditor1.Value, lblInst.Text);
                    txtHeading.Text = "";
                    FCKeditor1.Value = "";
                    Messages11.Visible = true;
                    Messages11.setMessage(1, "Information Saved Successfully !");

                }
                else
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Enter proper question information !");
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("Violation of PRIMARY KEY constraint 'PK_exm_queInfo_1'. Cannot insert duplicate key in object 'dbo.exm_queInfo'.The statement has been terminated."))
                {
                    Messages11.Visible = true;
                    Messages11.setMessage(0, "Information Heading already exists !");
                }
            }
        }

        protected void txtHeading_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
