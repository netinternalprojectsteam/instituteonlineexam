﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true" CodeBehind="addMainMenu.aspx.cs" Inherits="OnlineExam.addMainMenu" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    Add Menu</h3>
                
            </td>
        </tr>
       
    </table>
    <uc1:Messages1 ID="messages1" runat="server" Visible="false"/>
               
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label3" runat="server" Text="Parent"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddmainMenu1" runat="server" DataTextField="menuName" DataValueField="id"
                                                    DataSourceID="sourceMainMenu" OnSelectedIndexChanged="ddMainMenu_SelectedIndexChanged"
                                                    AutoPostBack="true">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 100px">
                                                <asp:Label ID="Label2" runat="server" Text="Menu Name"></asp:Label>
                                                <asp:Label ID="lblID" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lblMenuID" runat="server" Text="Label" Visible="False"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMenuName" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMenuName"
                                                    ForeColor="Red" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label4" runat="server" Text="Is Parent"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkParent" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label5" runat="server" Text="Preference"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtPreference" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPreference"
                                                    ForeColor="Red" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPreference"
                                                    ValidChars="1234567890">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label6" runat="server" Text="Link"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtLink" runat="server" Enabled="false"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLink"
                                                    ForeColor="Red" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label7" runat="server" Text="Not For Header"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkvisible" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label8" runat="server" Text="For Footer"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkfooter" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:Label ID="Label1" runat="server" Text="Page Contents"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <FCKeditorV2:FCKeditor ID="FCKeditor1" runat="server" Width="99%" Height="350px">
                                                </FCKeditorV2:FCKeditor>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" CssClass="simplebtn"/>
                                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Visible="false" OnClick="btnUpdate_Click" CssClass="simplebtn"/>
                                                <asp:Button ID="btnCancel" CausesValidation="false" runat="server" Text="Cancel"
                                                    Visible="false" OnClick="btnCancel_Click" CssClass="simplebtn"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server">
                        <table width="100%">
                            <tr>
                                <td style ="padding-right:8px;">
                                    <asp:Button ID="btnNew" runat="server" Text="New Menu" OnClick="btnNew_Click" Style="float: right;color:#fff;" CssClass="simplebtn" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridView1" runat="server" CssClass="gridview" AutoGenerateColumns="False"
                                        DataKeyNames="id" DataSourceID="sourceMenu" OnRowCommand="GridView1_RowCommand"
                                        EmptyDataText="No Menu To Show !!" OnRowCreated="GridView1_RowCreated">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No.">
                                                <ItemStyle HorizontalAlign="Center" Width="25px" />
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %></ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
                                            <asp:BoundField DataField="menuName" HeaderText="Menu Name" SortExpression="menuName" />
                                            <asp:BoundField DataField="parentID" HeaderText="parentID" SortExpression="parentID" />
                                            <asp:BoundField DataField="isParent" HeaderText="isParent" SortExpression="isParent" />
                                            <asp:BoundField DataField="pageContents" HeaderText="pageContents" SortExpression="pageContents" />
                                            <asp:BoundField DataField="preference" HeaderText="Preference" SortExpression="preference" />
                                            <asp:BoundField DataField="link" HeaderText="Link" SortExpression="link" />
                                            <asp:BoundField DataField="notvisibleheader" HeaderText="Not For Header" SortExpression="notvisibleheader" />
                                            <asp:BoundField DataField="displayinfooter" HeaderText="For Footer" SortExpression="displayinfooter" />
                                            <asp:ButtonField ButtonType="Button" CommandName="change" HeaderText="Edit" Text="Edit">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Eval("id") %>' Text="X"
                                                        CommandName="remove" OnClientClick="return confirm('Are you sure you want to delete this Record?');" CssClass="dt-sc-button small red" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
 
    


    <asp:SqlDataSource ID="sourceMainMenu" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT id, menuName FROM tblMenu where isParent='true' order by menuName">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceMenu" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
        SelectCommand="SELECT * FROM tblMenu order by menuName"></asp:SqlDataSource>
</asp:Content>
