﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ManageCourse : System.Web.UI.Page
    {
        classes.DataLogic obj = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["InstituteID"] != null)
                    {
                        lblInsId.Text = Session["InstituteID"].ToString();
                    }
                    else
                    {
                        lblInsId.Text = val[3];
                    }
                }
            }
            catch { }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Button1.Text == "Add New")
                {
                    obj.AddNewCourse(ddlCategory.SelectedValue, txtCourse.Text);
                    Messages11.setMessage(1, "Course Info Added Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    txtCourse.Text = "";
                }

                if (Button1.Text == "Update")
                {
                    obj.UpdateCourseInfo(ddlCategory.SelectedValue, txtCourse.Text, lblCourseID.Text);
                    Messages11.setMessage(1, "Course Info Updated Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                    Button1.Text = "Add New";
                    txtCourse.Text = "";
                    btnCan.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Messages11.setMessage(0, ex.Message);
                Messages11.Visible = true;
            }

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowno = Convert.ToInt16(e.CommandArgument);
            if (e.CommandName == "change")
            {
                lblCourseID.Text = GridView1.Rows[rowno].Cells[0].Text;
                DataTable dt = new DataTable();
                dt = obj.GetCourseInfoByID(lblCourseID.Text);
                ddlCategory.SelectedValue = dt.Rows[0][0].ToString();
                txtCourse.Text = dt.Rows[0][1].ToString();
                Button1.Text = "Update";
                btnCan.Visible = true;
            }

            if (e.CommandName == "remove")
            {
                try
                {
                    obj.DeleteCourseInfo(GridView1.Rows[rowno].Cells[0].Text);
                    Messages11.setMessage(1, "Course Info Deleted Successfully !!!");
                    Messages11.Visible = true;
                    GridView1.DataBind();
                }
                catch (Exception ex)
                {
                    Messages11.setMessage(0, ex.Message);
                    Messages11.Visible = true;
                }
            }
        }

        protected void btnCan_Click(object sender, EventArgs e)
        {
            Button1.Text = "Add New";
            txtCourse.Text = "";
            btnCan.Visible = false;
        }
    }
}
