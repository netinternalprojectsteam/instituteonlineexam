﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class studyMaterials : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string ext = "";
        string extToUpper = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "Save")
            {
                int maxID = ob.getMaxSMaterialID();
                lblID.Text = (maxID + 1).ToString();
            }
            string submit = "Yes";
            if (fileUploadIcon.HasFile)
            {
                ext = System.IO.Path.GetExtension(this.fileUploadIcon.PostedFile.FileName);
                extToUpper = ext.ToUpper();
                if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG")
                {
                    if (fileUploadIcon.FileBytes.Length >= 4194304)
                    {
                        Messages11.setMessage(0, "Cannot Upload Icon Image ! File Size is Larger than 4 MB.");
                        Messages11.Visible = true;
                        submit = "No";
                    }
                    else
                    {
                        lblIcon.Text = lblID.Text + "icon" + ext;
                        fileUploadIcon.SaveAs(Server.MapPath("~\\UploadedDocs\\" + lblIcon.Text));

                    }
                }
                else
                {
                    Messages11.setMessage(0, "Allowed Files for IconImage are .JPG, .JPEG, .PNG");
                    Messages11.Visible = true;
                    submit = "No";
                }
            }
            if (fileUploadDoc.HasFile)
            {
                ext = System.IO.Path.GetExtension(this.fileUploadDoc.PostedFile.FileName);
                extToUpper = ext.ToUpper();
                if (fileUploadDoc.FileBytes.Length >= 4194304)
                {
                    Messages11.setMessage(0, "Cannot Upload Document ! File Size is Larger than 4 MB.");
                    Messages11.Visible = true;
                    submit = "No";
                }
                else
                {
                    lblDoc.Text = lblID.Text + "doc" + ext;
                    fileUploadDoc.SaveAs(Server.MapPath("~\\UploadedDocs\\" + lblDoc.Text));

                }
            }
            if (submit == "Yes")
            {

                if (btnSave.Text == "Save")
                {
                    ob.insertStudyMaterial(txtTitle.Text, txtDesc.Text, lblIcon.Text, lblDoc.Text, chkIsActive.Checked);
                }
                if (btnSave.Text == "Update")
                {
                    ob.updateStudyMaterial(txtTitle.Text, txtDesc.Text, lblIcon.Text, lblDoc.Text, chkIsActive.Checked, lblID.Text);
                }
                gridMaterials.DataBind();
                txtDesc.Text = "";
                txtTitle.Text = "";
                btnSave.Text = "Save";
                btnCancel.Visible = false;
            }
        }

        protected void gridMaterials_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "change")
            {
                row = Convert.ToInt32(e.CommandArgument);
                lblID.Text = gridMaterials.Rows[row].Cells[1].Text;
                DataTable dt = new DataTable();
                dt = ob.getStudyMaterialDetails(lblID.Text);

                txtTitle.Text = dt.Rows[0]["title"].ToString();
                txtDesc.Text = dt.Rows[0]["description"].ToString();
                lblDoc.Text = dt.Rows[0]["downloadFile"].ToString();
                lblIcon.Text = dt.Rows[0]["iconImage"].ToString();
                chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["isActive"].ToString());
                btnSave.Text = "Update";
                btnCancel.Visible = true;

            }
        }

        protected void gridMaterials_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[5].Visible = false;
                    e.Row.Cells[4].Visible = false;
                }
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[5].Visible = false;
                    e.Row.Cells[4].Visible = false;
                }
            }
            catch { }
        }
    }
}
