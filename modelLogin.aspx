﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="modelLogin.aspx.cs" Inherits="OnlineExam.modelLogin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="inline1" style="width: 400px;">
        <h2>
            Login Here</h2>
        <div class="clear">
        </div>
        <p>
            Dont Have an account? <a href="register.html" class="colr">Create one</a>, It’s
            Simple and free.</p>
        <div class="clear">
        </div>
        <a href="#" class="left">
            <img src="images/signup.gif" alt="" /></a> &nbsp;<a href="#" class="left"><img src="images/forgot.gif"
                alt="" /></a>
        <div class="clear marg_bot">
            &nbsp;</div>
        <ul class="forms">
            <li class="txt">User Name</li>
            <li class="inputfield">
                <%-- <input type="text" value="" class="bar" />--%>
                <asp:TextBox ID="txtUName" runat="server" CssClass="bar"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtUName"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email ID !"
                    ControlToValidate="txtUName" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                <%--<asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator1">
                </asp:ValidatorCalloutExtender>--%>
            </li>
        </ul>
        <ul class="forms">
            <li class="txt">Password</li>
            <li class="inputfield">
                <%--<input type="text" value="" class="bar" />--%>
                <asp:TextBox ID="txtPass" TextMode="Password" runat="server" CssClass="bar"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtPass"></asp:RequiredFieldValidator>
            </li>
        </ul>
        <ul class="forms marg_top">
            <li class="txt">
                <%--<asp:Button ID="Button1" CssClass="simplebtn" runat="server" Text="Login" />
             &nbsp;&nbsp;  
                <asp:Button ID="Button2" CssClass="simplebtn" runat="server" Text="Cancel" />--%>
                <asp:CheckBox ID="chkRem" runat="server" Text="Remember Me" Visible="false" />
                <asp:Button ID="Button1" runat="server" Text="Login" CssClass="simplebtn" OnClick="Button1_Click" />
            </li>
        </ul>
    </div>
    </form>
</body>
</html>
