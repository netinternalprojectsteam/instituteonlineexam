﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class ccavCancel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string hostURL = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + "/testsList.aspx";
                string script = "<script type=\"text/javascript\"> var seconds = 8; alert('Payment Cancelled! Please wait...');  setTimeout(function () { window.location='" + hostURL + "'; }, seconds * 500); </script>";
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
            }
        }
    }
}
