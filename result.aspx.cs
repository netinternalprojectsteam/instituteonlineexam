﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.IO;
using iTextSharp.text.html.simpleparser;
using System.Collections.Generic;
using System.Text;
namespace OnlineExam
{
    public partial class result : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }
                ddlExams.DataBind();
                if (ddlExams.Text != "-Select-")
                {

                    DataTable dt = new DataTable();
                    dt = ob.getData("SELECT convert(varchar(10),onDate,103) as onDate, uname, uemail, mobileNo, totalMarks, percentile, accuracy, solvedQue, correctQue,totalQue,examName,NegativeMarking FROM view_examResult WHERE (preexamID = " + ddlExams.SelectedValue + ") ORDER BY totalMarks DESC");

                    if (dt.Rows.Count > 0)
                    {
                        gridDetails.DataSource = dt;
                        gridDetails.DataBind();
                        lblExamName.Text = "Exam Name : " + dt.Rows[0]["examName"].ToString();
                        lblDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                        lblIsNegative.Text = "Negative Marking : " + dt.Rows[0]["NegativeMarking"].ToString();
                        lblTotalQue.Text = "Total Questions : " + dt.Rows[0]["totalQue"].ToString();
                        Label1.Visible = false;
                        Panel1.Visible = true;
                        btnToPDF.Visible = true;

                    }
                    else
                    {
                        Panel1.Visible = false;
                        Label1.Visible = true;
                        btnToPDF.Visible = false;
                    }
                }
            }
        }
        protected void ddlExams_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlExams.Text != "-Select-")
            {
                //sourceDetails.ConnectionString = ConfigurationManager.ConnectionStrings["onlineexamdbConnectionString"].ConnectionString;
                //sourceDetails.SelectCommand = "SELECT convert(varchar(10),onDate,103) as onDate, uname, uemail, mobileNo, totalMarks, percentile, accuracy, solvedQue, correctQue, FROM view_examResult WHERE (preexamID = " + ddlExams.SelectedValue + ") ORDER BY totalMarks DESC";

                DataTable dt = new DataTable();
                dt = ob.getData("SELECT convert(varchar(10),onDate,103) as onDate, uname, uemail, mobileNo, totalMarks, percentile, accuracy, solvedQue, correctQue,totalQue,examName,NegativeMarking FROM view_examResult WHERE (preexamID = " + ddlExams.SelectedValue + ") ORDER BY totalMarks DESC");

                if (dt.Rows.Count > 0)
                {
                    gridDetails.DataSource = dt;
                    gridDetails.DataBind();
                    lblExamName.Text = "Exam Name : " + dt.Rows[0]["examName"].ToString();
                    lblDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    lblIsNegative.Text = "Negative Marking : " + dt.Rows[0]["NegativeMarking"].ToString();
                    lblTotalQue.Text = "Total Questions : " + dt.Rows[0]["totalQue"].ToString();
                    Label1.Visible = false;
                    Panel1.Visible = true;
                    btnToPDF.Visible = true;

                }
                else
                {
                    Panel1.Visible = false;
                    Label1.Visible = true;
                    btnToPDF.Visible = false;
                }
            }
        }

        protected void btnToPDF_Click(object sender, EventArgs e)
        {
            string attachment = "attachment; filename=Result For" + ddlExams.SelectedItem.Text + " " + DateTime.Now.Date.ToString("dd/MM/yyyy") + ".pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            Panel1.RenderControl(htextw);
            Document document = new Document();
            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

            /* Verifies that the control is rendered */

        }
    }
}
