﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="examStartPage.aspx.cs" Inherits="OnlineExam.examStartPage" Title="Manage Exam Start Pages" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <br />
    <asp:Label ID="lblID" Visible="false" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="lblCategory" Visible="false" runat="server" Text="Label"></asp:Label>
    <asp:Label ID="lblInst" Visible="false" runat="server" Text="Label"></asp:Label>
    <table width="100%">
        <tr>
            <td colspan="4" align="center">
                <h3>
                    Manage Start Pages</h3>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="4">
                <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <asp:Label ID="Label2" runat="server" Text="Exam Name" CssClass="myLabel"></asp:Label>
            </td>
            <td align="left" valign="top">
                <%--<asp:DropDownList ID="ddlExam" runat="server" DataValueField="ID" DataTextField="EName"
                            DataSourceID="sourceExam" AutoPostBack="True" 
                            OnSelectedIndexChanged="ddlExam_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sourceExam" runat="server"></asp:SqlDataSource>--%>
                <asp:DropDownList ID="ddlExams" runat="server" DataSourceID="sourceExams" AutoPostBack="True"
                    DataTextField="examName" DataValueField="examID" OnSelectedIndexChanged="ddlExams_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                    SelectCommand="SELECT DISTINCT [examID], [examName] FROM [view_examDetails] where instituteId=@instituteId">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lblInst" Name="instituteId" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                    SelectCommand="SELECT dbo.exam_startPages.id, dbo.view_examDetails.examID, dbo.exam_startPages.category, dbo.exam_startPages.description,  dbo.view_examDetails.examName FROM dbo.exam_startPages INNER JOIN dbo.view_examDetails ON dbo.exam_startPages.coursePreExamID = dbo.view_examDetails.examID where instituteId=@instituteId">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lblInst" Name="instituteId" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td align="left" valign="top">
                <asp:Label ID="Label1" runat="server" Text="Select As" CssClass="myLabel"></asp:Label>
            </td>
            <td align="left" valign="top">
                <asp:DropDownList ID="ddlSelectAs" runat="server" DataSourceID="SqlDataSource1" DataTextField="examName"
                    DataValueField="examID">
                </asp:DropDownList>
                &nbsp;
                <asp:Button ID="btnAdd" runat="server" Text="ADD" CssClass="simplebtn" OnClick="btnAdd_Click" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <asp:Label ID="Label3" runat="server" Text="Description" CssClass="myLabel"></asp:Label>
            </td>
            <td align="left" valign="top">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <%--<CKEditor:CKEditorControl ID="CKEditorControl1" runat="server"></CKEditor:CKEditorControl>--%>
                <FCKeditorV2:FCKeditor ID="FCKeditor1" runat="server">
                </FCKeditorV2:FCKeditor>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
