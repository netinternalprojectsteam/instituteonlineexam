<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="OnlineExam.Default" Title="Home Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/TSlider/global.css" rel="stylesheet" type="text/css" />
    <link href="/css/rolloverImages.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .divBlock
        {
            height: 300px;
            background-image: url('images/np1.png' );
            background-repeat: no-repeat;
            width: 305px;
            padding-top: 25px;
        }
        table.Shovertable
        {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
            width: 100%;
            border-radius: 5px;
        }
        table.Shovertable th
        {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
        }
        table.Shovertable tr
        {
        }
        table.Shovertable td
        {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #a9c6c9;
            text-align: center;
        }
    </style>
    <style type="text/css">
        .tLink
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-style: normal;
            letter-spacing: 2;
            color: Blue;
            font-family: Georgia;
            text-decoration: none;
        }
        .tLink1
        {
            padding: 2px;
            color: #fff;
            cursor: pointer;
            background-color: #46b1e1;
            display: inline-block;
            font-size: 12px;
            text-decoration: none;
        }
        .tLink1:hover
        {
            color: #fff;
            background-color: #143857;
            background-image: none;
        }
        #intro
        {
            background: #ffffffurl(images/bg-texture.png)repeat left top;
            border-bottom: 1px solid #e1e1e1;
            font: normal 1em/1.3 "Signika" ,sans-serif,sans-serif;
            font-weight: 300;
            color: #585858;
        }
        .divPara
        {
            color: Blue;
            line-height: 1.8em;
            padding: 5px 5px;
            margin: 0;
            text-align: left;
            vertical-align: top;
            font-family: Georgia;
        }
    </style>
    <style type="text/css" media="screen">
        #slider1
        {
            width: 960px; /* important to be same as image width */
            height: 370px; /* important to be same as image height */
            position: relative; /* important */
            overflow: hidden; /* important */
            border: solid 2px black;
        }
        #slider1Content
        {
            width: 960px; /* important to be same as image width or wider */
            position: absolute;
            top: 0;
            margin-left: 0;
        }
        .slider1Image
        {
            float: left;
            position: relative;
            display: none;
        }
        .slider1Image span
        {
            position: absolute;
            font: 10px/15px Arial, Helvetica, sans-serif;
            width: 0px;
            background-color: #000;
            filter: alpha(opacity=70);
            -moz-opacity: 0.7;
            -khtml-opacity: 0.7;
            opacity: 0.7;
            color: #fff;
            display: none;
        }
        .clear
        {
            clear: both;
        }
        .slider1Image span strong
        {
            font-size: 14px;
        }
        .left
        {
            top: 0;
            left: 0;
            width: 110px !important;
            height: 350px;
        }
        .right
        {
            right: 0;
            bottom: 0;
            width: 90px !important;
            height: 355px;
        }
        ul
        {
            list-style-type: none;
        }
        .blockHead
        {
            text-align: center;
            color: fa6c04;
            font-size: large;
            font-family: Microsoft Sans Serif;
        }
    </style>
    <!-- JavaScripts-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel2" runat="server">
        <div id="slider1">
            <ul id="slider1Content">
                <li class="slider1Image"><a href="">
                    <img src="Slider/1.png" alt="1" /></a> <span></span></li>
                <li class="slider1Image"><a href="">
                    <img src="Slider/2.png" alt="2" /></a> <span></span></li>
                <li class="slider1Image"><a href="">
                    <img src="Slider/3.png" alt="3" /></a> <span></span></li>
                <li class="slider1Image"><a href="">
                    <img src="Slider/4.png" alt="4" /></a> <span></span></li>
                <li class="slider1Image"><a href="">
                    <img src="Slider/5.png" alt="4" /></a> <span></span></li>
                <div class="clear slider1Image">
                </div>
            </ul>
        </div>
        <div class="clear">
        </div>
        <br />
        <br />
        <div style="float: left; text-align: center; width: 100%" id="intro">
            Why go for question papers in PDF, Texts when you can practice tests real time online
            !<br />
            9000 MCQ, affordable fees..last 5 years paper..2800 registered members...check now...
            <br />
            <br />
        </div>
    </asp:Panel>
    <table width="100%">
        <tr>
            <td>
                <div style="width: 940px; float: left">
                    <div style="width: 315px; float: left; text-align: center; vertical-align: top;">
                        <img src="/images/studyMat.gif" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            CURRENT AFFAIRS<br />
                        </div>
                        <div class="divBlock" style="padding-left: 10px; line-height: 150%;">
                            <asp:Label ID="lblStudyMaterial" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 310px; float: left; vertical-align: top; text-align: center;">
                        <img src="/images/news2.png" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            CURRENT JOBS & RESULTS</div>
                        <div class="divBlock" style="padding-left: 10px; line-height: 150%;">
                            <asp:Label ID="lblCurrentJobs" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 315px; float: left; vertical-align: top; text-align: center;">
                        <img src="/images/ba.jpg" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            BANKING AWARENESS</div>
                        <div class="divBlock" style="padding-left: 10px; line-height: 150%;">
                            <asp:Label ID="lblBAwareness" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                </div>
                <hr />
                <div style="width: 940px; float: left; text-align: center;">
                    <div style="width: 315px; float: left; vertical-align: top;">
                        <img src="/images/help.png" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            EXAM SYLLABUS
                        </div>
                        <div class="divBlock" style="padding-left: 10px; line-height: 150%;">
                            <asp:Label ID="lblSyllabus" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 310px; float: left; vertical-align: top;">
                        <img src="/images/links.png" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            IMPORTANT LINKS
                        </div>
                        <div class="divBlock" style="padding-left: 10px; line-height: 150%;">
                            <asp:Label ID="lblImpLinks" runat="server" Text="Label"></asp:Label>
                        </div>
                    </div>
                    <div style="width: 315px; float: left; vertical-align: top;">
                        <img src="/images/help.png" alt="" width="30px" height="30px" /><br />
                        <div class="blockHead">
                            SYSTEM REQUIRMENTS
                        </div>
                        <div class="divBlock">
                            <center>
                                <div style="width: 90%; text-align: center;">
                                    <table class="Shovertable">
                                        <tr>
                                            <td>
                                                1
                                            </td>
                                            <td>
                                                JavaScript Support
                                            </td>
                                            <td>
                                                Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2
                                            </td>
                                            <td>
                                                Cookies Enabled
                                            </td>
                                            <td>
                                                Not Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3
                                            </td>
                                            <td>
                                                Flash Support
                                            </td>
                                            <td>
                                                Not Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4
                                            </td>
                                            <td>
                                                Ajax
                                            </td>
                                            <td>
                                                Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5
                                            </td>
                                            <td>
                                                Java Support
                                            </td>
                                            <td>
                                                Not Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6
                                            </td>
                                            <td>
                                                Enable Popup
                                            </td>
                                            <td>
                                                Required
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <a href='abtSpecification.aspx' class='tLink1'>Read More>></a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
