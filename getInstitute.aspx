﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="getInstitute.aspx.cs" Inherits="OnlineExam.getInstitute" Title="Select Institute" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .vsmaint
        {
            width: 100%;
            min-height: 500px;
        }
        .vstablebox
        {
            text-decoration: none;
            width: 250px;
            padding: 15px;
            background: #fff;
            border: 1px solid #eee;
            margin: 15px;
            color: #46b1e1;
            float: left;
            text-align: justify;
        }
        .vstablebox a
        {
            text-decoration: none;
            background: #fff;
            color: #46b1e1;
        }
        .vstablebox a:hover
        {
            text-decoration: none;
            background: #fff;
            color: blue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="font-size: 17px; padding: 10px;">
        Please select Institute to continue Login...</div>
    <div class="vsmaint">
        <asp:Repeater ID="rptInstitute" runat="server" OnItemCommand="rptInstitute_ItemCommand">
            <ItemTemplate>
                <div class="vstablebox">
                    <asp:LinkButton ID="linkInstitute" runat="server" CommandArgument='<%#Eval("InstituteId")+","+Eval("uid") %>'
                        CommandName="select"><%#Eval("instituteName")%></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
