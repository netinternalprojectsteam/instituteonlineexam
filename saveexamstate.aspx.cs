﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class saveexamstate : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string Uid = "";
                string Eid = "";
                try
                {
                    Uid = Session["userID"].ToString();
                    Eid = Session["examID"].ToString();
                }
                catch { }

                try
                {
                    Uid = Request.QueryString["uid"].ToString();
                    Eid = Request.QueryString["examID"].ToString();
                }
                catch { }


                if (Request.QueryString["act"].ToString() == "saveAns")
                {
                    int rTime = Convert.ToInt32(Request.QueryString["rTime"].ToString());
                    int timeSpent = Convert.ToInt32(Request.QueryString["spentTime"].ToString());

                    string ans = Request.QueryString["ans"].ToString();
                    string queID = Request.QueryString["q"].ToString();
                    ob.updateQueAnswer(queID, Eid, Uid, ans, rTime, timeSpent);
                }
                if (Request.QueryString["act"].ToString() == "mark")
                {

                    string queID = Request.QueryString["q"].ToString();
                    ob.insertMarked(Uid, Eid, queID);
                }
                if (Request.QueryString["act"].ToString() == "unmark")
                {

                    string queID = Request.QueryString["q"].ToString();
                    ob.deleteMarked(Uid, Eid, queID);
                }
                if (Request.QueryString["act"].ToString() == "finishExam")
                {

                    string Ansque = Request.QueryString["answered"].ToString();
                    if (Ansque.Length > 0)
                    {
                        string[] saveQuestions = Ansque.Split('@');
                        //questid+","+value+","+totalTime
                        for (int i = 1; i < saveQuestions.Length; i++)
                        {
                            string[] queAns = saveQuestions[i].ToString().Split('X');
                            string questid = queAns[0].ToString();
                            string ans = queAns[1].ToString();
                            string time = queAns[2].ToString();
                            //time.Remove(time.Length - 1, 1);
                            int Ttime = Convert.ToInt32(time);
                            ob.updateQueAnswer(questid, Eid, Uid, ans, Ttime, 0);
                        }
                    }

                    ob.updateExamStatus(Eid);

                }
                if (Request.QueryString["act"].ToString() == "addFeedback")
                {

                    string feedback = Request.QueryString["answered"].ToString();
                    ob.insertFeedback(Uid, Eid, feedback);

                }

                Session["userID"] = Uid;
                Session["examID"] = Eid;

            }
            catch (Exception) { }
        }
    }
}
