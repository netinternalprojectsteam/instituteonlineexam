﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="examSyllabus.aspx.cs" Inherits="OnlineExam.examSyllabus" Title="Exam Syllabus" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Exam Syllabus
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Heading" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHeading" runat="server" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtHeading"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label4" runat="server" Text="Syllabus" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <FCKeditorV2:FCKeditor ID="fckEditorDesc" runat="server" Width="700px" Height="400px">
                                        </FCKeditorV2:FCKeditor>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT id, sHeading, sDetails, sDate FROM site_syllabus order by id desc">
                            </asp:SqlDataSource>
                            <asp:GridView ID="gridDetails" runat="server" DataSourceID="sourceDetails" CssClass="gridview"
                                AutoGenerateColumns="False" OnRowCommand="gridDetails_RowCommand" OnRowCreated="gridDetails_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No.">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="sHeading" HeaderText="Heading" SortExpression="sHeading" />
                                    <asp:BoundField DataField="sDate" HeaderText="Published Date" SortExpression="sDate" />
                                    <asp:BoundField DataField="sDetails" HeaderText="sDetails" SortExpression="sDetails" />
                                    <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                                    <%-- <asp:ButtonField ButtonType="Image" HeaderText="Delete" ImageUrl="~/images/delete.gif"
                                        Text="Button" CommandName="remove" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:ButtonField ButtonType="Image" HeaderText="Info" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ImageUrl="~/images/Details.png" Text="Button"
                                        CommandName="info" />--%>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
