﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="UserExamStart.aspx.cs" Inherits="OnlineExam.UserExamStart" Title="User Exam Start" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">



function CheckOnOff()
{
if(document.getElementById("<%= rb1.ClientID %>").checked == true)
{
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
}

}


function CheckOnOff1()
{
if(document.getElementById("<%= rb2.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
}

}


function CheckOnOff2()
{
if(document.getElementById("<%= rb3.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb4.ClientID %>").checked = false;
}

}


function CheckOnOff3()
{
if(document.getElementById("<%= rb4.ClientID %>").checked == true)
{
document.getElementById("<%= rb1.ClientID %>").checked = false;
document.getElementById("<%= rb2.ClientID %>").checked = false;
document.getElementById("<%= rb3.ClientID %>").checked = false;
}

}

    </script>

    <%--alert('Call JavaScript function from codebehind');--%>

    <script type="text/javascript" language="javascript"> 
        function showMessage()
        {
//        if (confirm("Exam completed ! Displaying Question No. 1...")==true)
//        return true;
//        else 
//        return false;
alert('Exam completed ! Displaying Question No. 1...');
        }
    </script>

    <script language="Javascript" type="text/javascript">
      function CallAlert()
       {       
        document.getElementById("ctl00_ContentPlaceHolder1_Button3").click();     
       }
   
 
    </script>

    <script type="text/javascript" language="javascript"> 
        function ConfirmForFinish()
        {
        if (confirm("Are you sure?")==true)
        return true;
        else 
        return false;
        }
        
        function openNewWindow()
        {
        window.open("userexamstartNew.aspx", "Exam", "location=1, width=auto, height=auto");
        }
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel4" runat="server">
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label4" runat="server" Text="Remaining Time :" Visible="false"></asp:Label>&nbsp;&nbsp;<asp:Label
                    ID="Label2" runat="server" />
                <asp:Timer ID="Timer1" runat="server" Interval="3600" OnTick="Timer1_Tick">
                </asp:Timer>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button3" runat="server" Text="Button" CausesValidation="false" OnClick="Button1_Click" /></div>
            <asp:Panel ID="Panel2" runat="server">
                <center>
                    <table>
                        <tr>
                            <td>
                                <h4>
                                    Course Name :</h4>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCourse" runat="server" DataSourceID="sourceCourse" DataTextField="courseinfo"
                                    DataValueField="courseid">
                                </asp:DropDownList>
                                <asp:Label ID="lblSolvedQuestions" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="lblQueAns" runat="server" Visible="False"></asp:Label>
                                <asp:SqlDataSource ID="sourceCourse" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                    SelectCommand="SELECT [courseid], [catid], [courseinfo] FROM [exm_course]"></asp:SqlDataSource>
                            </td>
                            <td>
                                <h4>
                                    No of Questions :</h4>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlno" runat="server">
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnSub" runat="server" Text="Submit" OnClick="btnSub_Click" />
                                <%--     <asp:Button ID="Button4" runat="server" Text="Button" OnClientClick="openNewWindow();" />--%>
                            </td>
                        </tr>
                    </table>
                </center>
            </asp:Panel>
            <asp:Label ID="lblQuestionList" runat="server" Text="Label" Visible="False"></asp:Label>
            <asp:Label ID="lblGivenAns" Visible="false" runat="server" Text=""></asp:Label>
            <asp:Panel ID="Panel3" runat="server" Visible="false">
                <asp:Label ID="lblQuestionNo" Visible="false" runat="server" Text="Label"></asp:Label>
                <br />
                <br />
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                                <ItemTemplate>
                                    <div style="float: left; width: 40px">
                                        <asp:Button ID="Button1" runat="server" Text='<%# Eval("serialnumber") %>' CommandArgument='<%#Eval("questid") %>'
                                            Width="35px" OnClick="btnRep_Click" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr style="border-bottom: solid 1px black" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server" Height="450px" ScrollBars="Auto">
                    <table>
                        <tr>
                            <td align="left" valign="top">
                                <div style="max-width: 500px; position: absolute">
                                    <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Visible="false" Height="350px">
                                        <asp:Repeater ID="repQueInfo" runat="server">
                                            <ItemTemplate>
                                                <table width="500px">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("infContents") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </div>
                            </td>
                            <td align="left" style="float: left" valign="top">
                                <div style="width: 99%; min-width: 500px; float: left">
                                    <table width="100%" style="float: left">
                                        <tr>
                                            <td rowspan="18" style="width: 5%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 95%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblQue" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="width: 10px" valign="top">
                                                            <asp:RadioButton ID="rb1" runat="server" OnClick="javascript:CheckOnOff();" />
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblOptionA" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                    <td align="left">
                                    </td>
                                </tr>--%>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="width: 10px" valign="top">
                                                            <asp:RadioButton ID="rb2" runat="server" OnClick="javascript:CheckOnOff1();" />
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblOptionB" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                    <td align="left">
                                       
                                    </td>
                                </tr>--%>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="width: 10px" valign="top">
                                                            <asp:RadioButton ID="rb3" runat="server" OnClick="javascript:CheckOnOff2();" />
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblOptionC" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                    <td align="left">
                                      
                                    </td>
                                </tr>--%>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="width: 10px" valign="top">
                                                            <asp:RadioButton ID="rb4" runat="server" OnClick="javascript:CheckOnOff3();" />
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="lblOptionD" runat="server" Text="Label" CssClass="myLabel"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                    <td align="left">
                                      
                                    </td>
                                </tr>--%>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table width="100%">
                    <tr>
                        <td>
                            <hr style="border-bottom: solid 1px black" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="Button2" runat="server" Text="Submit And Next" CausesValidation="False"
                                OnClick="Button2_Click" CssClass="simplebtn" />
                            <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="simplebtn" />
                            <asp:Button ID="btnMark" runat="server" Text="Mark" CssClass="simplebtn" OnClick="btnMark_Click" />
                            <asp:Button ID="btnFinish" runat="server" Text="Finish Exam" OnClick="btnFinish_Click"
                                CssClass="simplebtn" OnClientClick="return ConfirmForFinish();" />
                            <asp:Button ID="btnReview" runat="server" Text="Exam Review" CssClass="simplebtn"
                                OnClick="btnReview_Click" />
                            <asp:GridView ID="GridView1" runat="server" Visible="false">
                            </asp:GridView>
                            <asp:Label ID="lblExamID" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lbluserID" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <%--  <asp:SqlDataSource ID="sourceAttempts" runat="server"></asp:SqlDataSource>
               <asp:GridView ID="gridAttempts" CssClass="gridview" DataSourceID="sourceAttempts"
                    Width="250px" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="attemptDate" HeaderText="Date Attempted" />
                    </Columns>
                </asp:GridView>--%></asp:Panel>
            <div style="display: none">
                <asp:LinkButton ID="linkReview" runat="server">LinkButton</asp:LinkButton>
            </div>
            <asp:ModalPopupExtender ID="modalPopupReview" runat="server" TargetControlID="linkReview"
                BackgroundCssClass="ModalPopupBG" PopupControlID="examReview" Drag="true" CancelControlID="btnReviewCan">
            </asp:ModalPopupExtender>
            <div id="examReview" style="display: none;" class="popupConfirmation">
                <iframe id="Iframe4" frameborder="0" src="examReview.aspx" height="600px" width="600px">
                </iframe>
                <div class="popup_Buttons" style="display: none">
                    <input id="btnreviewOk" value="Done" type="button" />
                    <input id="btnReviewCan" value="Cancel" type="button" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
