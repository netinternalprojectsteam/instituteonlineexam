﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudyMaterialDetails.aspx.cs"
    Inherits="OnlineExam.StudyMaterialDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkayDetails').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnDetailsCancel').click();
        }
    
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Study Material Details
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();" style="text-decoration: none; color: White">X</a></b>
            </div>
        </div>
        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">
            <table>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 150px">
                        <asp:Label ID="Label1" runat="server" Text="Heading"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblHeading" runat="server" Text="Heading"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label2" runat="server" Text="Category"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="LblCategory" runat="server" Text="Heading"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label3" runat="server" Text="Publish Date"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lbldate" runat="server" Text="Heading"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label>
                    </td>
                    <td align="left">
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="lblDesc" runat="server" Text="Heading"></asp:Label>
                    </td>
                </tr>
            </table>
          
        </asp:Panel>
    </div>
    </form>
</body>
</html>
