﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="usefulData.aspx.cs" Inherits="OnlineExam.usefulData" Title="Useful Links and News" %>

<%@ Register Src="Messages1.ascx" TagName="Messages1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        Useful Links & Recruitment News
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                        <uc1:Messages1 ID="Messages11" runat="server" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel1" runat="server">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" Text="Heading" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHeading" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtHeading"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" Text="Link" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLink" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtLink"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" Text="Link Category" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCategory" runat="server">
                                            <asp:ListItem>Useful Link</asp:ListItem>
                                            <asp:ListItem>Recruitment News</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="simplebtn" OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT Linkheader, redirectLink, linkcategory, id FROM exam_usefulLinksAndNews">
                            </asp:SqlDataSource>
                            <asp:GridView ID="gridDetails" runat="server" DataSourceID="sourceDetails" CssClass="gridview"
                                AutoGenerateColumns="False" OnRowCommand="gridDetails_RowCommand" OnRowCreated="gridDetails_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No.">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Linkheader" HeaderText="Link header" SortExpression="Linkheader" />
                                    <asp:BoundField DataField="redirectLink" HeaderText="Redirect Link" SortExpression="redirectLink" />
                                    <asp:BoundField DataField="linkcategory" HeaderText="Category" SortExpression="linkcategory" />
                                    <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                                    <asp:ButtonField ButtonType="Image" HeaderText="Delete" ImageUrl="~/images/delete.gif"
                                        Text="Button" CommandName="remove" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
