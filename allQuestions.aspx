﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="allQuestions.aspx.cs" Inherits="OnlineExam.allQuestions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Switch View Page</title>
    <link href="css/popup.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    
        function okay() {
            window.parent.document.getElementById('btnOkSView').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCanSView').click();
        }
        
           function callfun()
    {
      parent.CallAlert();
        return false;
    }
        
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Switch View
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel4" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="left" valign="top">
                                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                            </td>
                            <td align="right" valign="top">
                                <asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="Label4" runat="server" Text="Remaining Time :"></asp:Label>&nbsp;&nbsp;<asp:Label
                                    ID="Label2" runat="server" />
                                <asp:Timer ID="Timer1" runat="server" Interval="3600" OnTick="Timer1_Tick">
                                </asp:Timer>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
            DynamicLayout="true">
            <ProgressTemplate>
                <center>
                    <div class="LockBackground">
                        <div class="LockPane">
                            <div>
                                <img src="ajax-loader2.gif" alt="" />
                            </div>
                        </div>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblUserID" runat="server" Text="Label" Visible="false"></asp:Label>
                <asp:Label ID="lblExamID" runat="server" Text="Label" Visible="false"></asp:Label>
                <table width="650px">
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnAnswered" runat="server" Text="Answered" ToolTip="Show Answered Questions"
                                OnClick="btnAnswered_Click" CssClass="simplebtn" />
                            <asp:Button ID="btnUnanswered" runat="server" Text="Unanswered" ToolTip="Show Unanswered Questions"
                                OnClick="btnUnanswered_Click" CssClass="simplebtn" />
                            <asp:Button ID="btnMarked" runat="server" Text="Marked" ToolTip="Show Marked Questions"
                                OnClick="btnMarked_Click" CssClass="simplebtn" />
                            <asp:Button ID="btnAllQuestions" runat="server" Text="All" ToolTip="Show All Questions"
                                OnClick="btnAllQuestions_Click" CssClass="simplebtn" />
                            <asp:Button ID="Button1" runat="server" Text="Close" OnClick="Button1_Click" CssClass="simplebtn" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" Text="Label" ForeColor="White"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel1" runat="server" Height="400px" ScrollBars="auto">
                                <center>
                                    <asp:GridView ID="GridView1" runat="server" Visible="false">
                                    </asp:GridView>
                                    <asp:Repeater ID="repQuestins" runat="server">
                                        <HeaderTemplate>
                                            <table style="background-color: White; width: 100%">
                                                <tr>
                                                    <td align="left" style="width: 40px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <asp:Label ID="Label5" runat="server" Text="Questions"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto">
                                                <table style="background-color: White; width: 100%">
                                                    <tr>
                                                        <td align="left" style="width: 40px" valign="top">
                                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("serialnumber") %>'></asp:Label>)
                                                        </td>
                                                        <td valign="top">
                                                            <asp:LinkButton ID="linkGotoQue" runat="server" OnClick="linkGotoQue_Click" Text='<%#Eval("question")%>'
                                                                CommandArgument='<%#Eval("questid")%>' ForeColor="Black" Style="text-decoration: none"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="top">
                                                            <hr style="border-bottom: dotted 1px blue; background-color: white" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </center>
                            </asp:Panel>
                            <asp:Panel ID="Panel3" runat="server" Height="400px" ScrollBars="auto" Visible="false">
                                <center>
                                    <asp:Repeater ID="repAnswered" runat="server">
                                        <HeaderTemplate>
                                            <table style="background-color: White; width: 100%">
                                                <tr>
                                                    <td align="left" style="width: 40px" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top">
                                                        <asp:Label ID="Label5" runat="server" Text="Questions"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto">
                                                <table style="background-color: White; width: 100%">
                                                    <tr>
                                                        <td align="left" style="width: 40px" valign="top">
                                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("serialnumber") %>'></asp:Label>)
                                                        </td>
                                                        <td valign="top">
                                                            <asp:LinkButton ID="linkGotoQue" runat="server" OnClick="linkGotoQue_Click" Text='<%#Eval("question")%>'
                                                                CommandArgument='<%#Eval("questid")%>' ForeColor="Black" Style="text-decoration: none"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="top">
                                                            &nbsp; &nbsp; &nbsp;Your Answer : &nbsp;
                                                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("selectedOption") %>'></asp:Label>)&nbsp;
                                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("yourAns") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="top">
                                                            <hr style="border-bottom: dotted 1px blue; background-color: white" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </center>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
