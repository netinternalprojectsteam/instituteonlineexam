﻿<%@ Page Language="C#" MasterPageFile="~/adminMaster.Master" AutoEventWireup="true"
    CodeBehind="expertAns.aspx.cs" Inherits="OnlineExam.expertAns" Title="Expert Answers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Expert Answers</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblID" runat="server" Text="" Visible="false"></asp:Label>
                         <asp:Label ID="lblEID" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label1" runat="server" Text="Question : " Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblQue" runat="server" Text="Question" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="Label2" runat="server" Text="Answer : " Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAns" runat="server" TextMode="MultiLine" Height="80px" Width="500px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required !"
                                            ControlToValidate="txtAns"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="simplebtn" OnClick="btnUpdate_Click" />
                                        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="simplebtn" CausesValidation="false"
                                            OnClick="btncancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:SqlDataSource ID="sourceDetails" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>"
                                SelectCommand="SELECT id, question, CASE WHEN isnull(answer, 0) = 0 THEN 'Not Answered !' ELSE answer END AS answer, uname, uemail, mobileNo FROM  view_expertQueAns order by id desc">
                            </asp:SqlDataSource>
                            <asp:GridView ID="gridDetails" runat="server" DataSourceID="sourceDetails" CssClass="gridview"
                                AutoGenerateColumns="False" OnRowCommand="gridDetails_RowCommand" OnRowCreated="gridDetails_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No.">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %></ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="question" HeaderText="Question" SortExpression="question" />
                                    <asp:BoundField DataField="answer" HeaderText="Answer" SortExpression="answer" />
                                    <asp:BoundField DataField="uname" HeaderText="Name" SortExpression="uname" />
                                    <asp:BoundField DataField="uemail" HeaderText="Email" SortExpression="uemail" />
                                    <asp:BoundField DataField="mobileNo" HeaderText="Mobile No" SortExpression="mobileNo" />
                                    <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" />
                                    <asp:ButtonField ButtonType="Image" HeaderText="Answer" ImageUrl="~/images/delete.gif"
                                        Text="Button" CommandName="change" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
