﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class logDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string qString = Request.QueryString["id"].ToString();
                    if (qString == "NXG1234")
                    {
                        lblMsg.Visible = false;
                        getFiles();
                        getDir();
                    }
                    else
                    {
                        lblMsg.Visible = true;
                    }
                }
                catch { lblMsg.Visible = true; }
            }
        }
        protected void gridFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string aPath = Server.MapPath("");
            aPath = aPath + "\\";
            int row = 0;
            if (e.CommandName == "remove")
            {
                row = Convert.ToInt16(e.CommandArgument);
                string fName = gridFiles.Rows[row].Cells[1].Text;
                System.IO.File.Delete(aPath + fName);
                getFiles();
            }

        }

        protected void gridDirs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int row = 0;
            if (e.CommandName == "remove")
            {
                row = Convert.ToInt16(e.CommandArgument);
                string DName = gridDirs.Rows[row].Cells[1].Text;
                System.IO.Directory.Delete(DName);
                getDir();
            }
        }


        public void getFiles()
        {
            string aPath = Server.MapPath("");
            string[] aFiles = System.IO.Directory.GetFiles(aPath, "*.*");
            DataTable dtFiles = new DataTable();
            dtFiles.Columns.Add("fileName", typeof(string));
            for (int i = 0; i < aFiles.Length; i++)
            {
                string just_file = System.IO.Path.GetFileName((string)aFiles[i]);
                DataRow dtRow = dtFiles.NewRow();
                dtRow["fileName"] = just_file;
                dtFiles.Rows.Add(dtRow);
            }
            gridFiles.DataSource = dtFiles;
            gridFiles.DataBind();
        }


        public void getDir()
        {
            string aPath = Server.MapPath("");
            string[] aDirectory = System.IO.Directory.GetDirectories(aPath);
            DataTable dtDirs = new DataTable();
            dtDirs.Columns.Add("dirName", typeof(string));
            for (int j = 0; j < aDirectory.Length; j++)
            {

                DataRow dtRow = dtDirs.NewRow();
                dtRow["dirName"] = aDirectory[j].ToString();
                dtDirs.Rows.Add(dtRow);
            }
            gridDirs.DataSource = dtDirs;
            gridDirs.DataBind();
        }
    }
}
