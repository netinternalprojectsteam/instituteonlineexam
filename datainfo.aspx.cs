﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class datainfo : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();

                dt = ob.getData("SELECT distinct category, catid FROM exm_category");
                int rowCount = dt.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    TreeNode nodeCategory = new TreeNode();
                    nodeCategory.Expanded = false;

                    nodeCategory.Collapse();
                    nodeCategory.SelectAction = TreeNodeSelectAction.Expand;
                    nodeCategory.Text = dt.Rows[i][0].ToString();
                    TreeView1.Nodes.Add(nodeCategory);

                    DataTable dTab = new DataTable();
                    dTab = ob.getData("SELECT courseid, catid, courseinfo FROM exm_course where catid=" + dt.Rows[i][1].ToString() + "");
                    int TabCount = dTab.Rows.Count;

                    for (int j = 0; j < TabCount; j++)
                    {
                        //ID, CityID, boardingPoint, Location, Landmark, Contact, Address
                        TreeNode nodeCourse = new TreeNode();
                        nodeCourse.Expanded = false;
                        nodeCourse.Text = dTab.Rows[j][2].ToString();
                        TreeView1.Nodes[i].ChildNodes.Add(nodeCourse);


                        //DataTable dTab1 = new DataTable();
                        //dTab1 = ob.getData("SELECT subid, courseid, subjectinfo FROM exm_subject where courseid = " + dTab.Rows[j][0].ToString() + "");
                        //int TabCount1 = dTab1.Rows.Count;

                        //for (int k = 0; k < TabCount1; k++)
                        //{
                        //    //ID, CityID, boardingPoint, Location, Landmark, Contact, Address
                        //    TreeNode nodeSub = new TreeNode();
                        //    nodeSub.Expanded = false;
                        //    nodeSub.Text = dTab1.Rows[k][2].ToString();
                        //    //TreeView1.Nodes[j].ChildNodes.Add(nodeSub);
                        //    TreeView1.Nodes[i].ChildNodes[j].ChildNodes.Add(nodeSub);
                        //}
                    }

                }
            }
        }


        protected void TreeView1_TreeNodeExpanded1(object sender, TreeNodeEventArgs e)
        {
            string x = e.Node.Text;
            btnCourse.Text = "Add New Course to " + x + "";
            btnSubject.Visible = false;
            btnCourse.Visible = true;
            try
            {
                foreach (TreeNode treenode in TreeView1.Nodes)
                {

                    // If node is expanded

                    if (treenode != e.Node)
                    {

                        // Collapse all other nodes

                        treenode.Collapse();
                        btnSubject.Visible = false;
                    }

                }
            }
            catch { }
        }

        protected void TreeView1_DataBinding(object sender, EventArgs e)
        {

        }

        protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
        {//Response.Write(TreeView1.SelectedNode.Depth.ToString());
            //Response.Write(TreeView1.SelectedNode.Value);
            TreeNode node = this.TreeView1.SelectedNode;
            String pathStr = node.Text;
            string separator = "\\";



            TreeView1.PathSeparator = Convert.ToChar(separator);


            while (node.Parent != null)
            {
                pathStr = node.Parent.Text + this.TreeView1.PathSeparator + pathStr;

                node = node.Parent;
            }

            string path = pathStr;
            btnSubject.Visible = true;
            btnSubject.Text = "Add New Subject to " + path + "";
            //lbtvpath.Text = pathStr;

            pathStr = pathStr.Replace("\\", ",");

            string[] pData = pathStr.Split(',');
            DataTable dt = new DataTable();
            dt = ob.getData("SELECT  exm_subject.subid, exm_subject.subjectinfo, exm_subject.courseid, exm_course.courseinfo FROM exm_course INNER JOIN exm_subject ON exm_course.courseid = exm_subject.courseid WHERE (exm_course.courseinfo ='" + pData[1].ToString() + "')");
            gridsub.DataSource = dt;
            gridsub.DataBind();


        }

        protected void btnCourse_Click(object sender, EventArgs e)
        {
            pnlCourse.Visible = true;
            btnCourse.Visible = false;
            pnlSubject.Visible = false;
            btnSubject.Visible = false;
        }

        protected void btnCancelCourse_Click(object sender, EventArgs e)
        {
            pnlCourse.Visible = false;
            btnCourse.Visible = true;

        }

        protected void btnSubject_Click(object sender, EventArgs e)
        {
            pnlSubject.Visible = true;
            btnSubject.Visible = false;
            pnlCourse.Visible = false;
            btnCourse.Visible = true;
        }

        protected void btnCancelSub_Click(object sender, EventArgs e)
        {
            pnlSubject.Visible = false;
            btnSubject.Visible = true;
        }
    }
}
