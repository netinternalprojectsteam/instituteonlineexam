﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="contactus.aspx.cs" Inherits="OnlineExam.contactus" Title="Contact Us" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="left" colspan="3">
                        <div class="pageHeading">
                            Contact Us
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMail" Visible="false" runat="server" Text="Label"></asp:Label>
                        <asp:Label ID="lblInstituteId" Visible="false" runat="server" Text="Label"></asp:Label>
                        <table>
                            <tr>
                                <td colspan="3" align="left">
                                    <asp:Label ID="lblContactusMsg" runat="server" Text="Your details saved !" ForeColor="Green"
                                        Visible="false" Font-Bold="true"></asp:Label>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label1" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtName" runat="server" CssClass="txtbox1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label2" runat="server" Text="Email" Font-Bold="true"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email"
                                        ControlToValidate="txtMail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    <br />
                                    <asp:TextBox ID="txtMail" runat="server" CssClass="txtbox1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label4" runat="server" Text="Mobile" Font-Bold="true"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="txtbox1"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMobile"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="Label3" runat="server" Text="Query&nbsp;" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="Label5" runat="server" Text="(if any)" Font-Italic="true" Font-Size="Small"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtQuery" runat="server" TextMode="MultiLine" Height="125px" Width="400px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Send" CssClass="simplebtn" OnClick="btnSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
