﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class howTo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["examInfo"].ToString().Length > 0)
                    {
                        hyperBack.NavigateUrl = "/examInfo.aspx?eid=" + Session["examInfo"].ToString() + "";
                        Session.Remove("examInfo");
                    }
                    else { hyperBack.Visible = false; }
                }
            }
            catch { }
        }
    }
}
