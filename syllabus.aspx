﻿<%@ Page Language="C#" MasterPageFile="~/userMaster.Master" AutoEventWireup="true"
    CodeBehind="syllabus.aspx.cs" Inherits="OnlineExam.syllabus" Title="Exam Syllabus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <center>
                <div class="LockBackground">
                    <div class="LockPane">
                        <div>
                            <img src="ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMsg" ForeColor="Red" Font-Bold="true" Font-Size="Large" runat="server" Text="No Details Found !"></asp:Label>
                        <asp:SqlDataSource ID="sourceSyllabus" runat="server" ConnectionString="<%$ ConnectionStrings:onlineexamdbConnectionString %>">
                        </asp:SqlDataSource>
                        <asp:Repeater ID="rptSyllabus" runat="server" DataSourceID="sourceSyllabus">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align='left'>
                                            <table width='100%'>
                                                <tr>
                                                    <td align="left">
                                                        <div class="pageHeading">
                                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("sHeading") %>'></asp:Label>
                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <%#Eval("sDetails") %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
