﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace OnlineExam
{
    public partial class toppers : System.Web.UI.Page
    {
        classes.DataLogic ob = new OnlineExam.classes.DataLogic();
        string[] val = HttpContext.Current.User.Identity.Name.Split(',');

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["InstituteID"] != null)
                {
                    lblInsId.Text = Session["InstituteID"].ToString();
                }
                else
                {
                    lblInsId.Text = val[3];
                }

                DataTable dtToppers = new DataTable();
                dtToppers.Columns.Add("examName", typeof(string));
                dtToppers.Columns.Add("uname", typeof(string));

                DataTable dt = new DataTable();
                dt = ob.getData("select max(totalMarks) as totalMarks,examName,examID,ondate from view_examSummary where instituteId=" + lblInsId.Text + " group by examName,examID ,ondate order by onDate desc");
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataTable dt1 = new DataTable();
                        dt1 = ob.getData("select examName,uname from view_examSummary where totalMarks='" + dt.Rows[i]["totalMarks"].ToString() + "' and examID='" + dt.Rows[i]["examID"].ToString() + "'");
                        if (dt1.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt1.Rows.Count; j++)
                            {
                                DataRow dtrow = dtToppers.NewRow();
                                dtrow["examName"] = dt1.Rows[j]["examName"].ToString();
                                dtrow["uname"] = dt1.Rows[j]["uname"].ToString();
                                dtToppers.Rows.Add(dtrow);
                            }
                        }
                    }

                }
                gridToppers.DataSource = dtToppers;
                gridToppers.DataBind();
            }
        }
    }
}
